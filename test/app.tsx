import * as React from "react";
import { Component } from "react";
import { render } from "react-dom";
import { AppBase } from '../src/components/app-base'
import { MasterPage } from '../src/components/masterpage/masterpage'
// import { AkProjectPicker } from '../src/components/identity/ak-project-picker'
import { AppKeys } from "../src/index";

// import { AkCostCenterPicker } from '../src/components/identity/AkCostCenterPicker';
interface AppProps {
}
interface AppStates {
}

export class App extends Component<AppProps,
    AppStates> {

    constructor(props, context) {
        super(props, context);

    }

    render() {
        return <AppBase appKey={AppKeys.YeeOfficeSettings}
            onLoaded={(appLocale, theme) => {
                return new Promise((resolve, reject) => {
                    switch (appLocale.locale) {
                        case "en":
                            resolve(require("../src/locales/en.json"));
                            break;
                        default:
                            resolve(require("../src/locales/zh.json"));
                            break;
                    }
                });
            }}>
            {/*<MasterPage></MasterPage>*/}
{/* 
            <AkCostCenterPicker onChange={(v) => {
                console.log(v)
            }} /> */}
        </AppBase>;
    }
}

render(
    <App />, document.getElementById("app_content"));
