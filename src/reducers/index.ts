import { combineReducers, ReducersMapObject, Reducer } from "redux";
import { identity } from "./Identity";
import { metadata } from "./Metadata";
import { masterpage } from "./MasterPage";
import { contentList } from "./ContentList";
import { admin } from "./AppAdmin";
import { notification } from './notification';
import { addAssisign } from './AddAssisign';
import { file } from "./file";
import { appCenter } from "./AppCenter";
import { commentReducer } from "./commentReducer";
import { commonBaseInfo } from "./CommonBaseInfoReducer";
import { listSets } from "./listSet";

export function rootCombineReducers<S>(reducers?: ReducersMapObject): Reducer<S> {
    const re = Object.assign({}, { file, identity, masterpage, metadata, contentList, admin, notification: notification, addAssisign, appCenter, commentReducer,commonBaseInfo,listSets }, reducers);
    return combineReducers<S>(re);
}