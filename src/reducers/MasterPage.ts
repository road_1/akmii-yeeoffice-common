import {
    MASTERPAGEHEADER_TRIGGER,
    MASTERPAGEFOOTER_TRIGGER,
    MASTERPAGECONTENT_MIN_HEIGHT,
    MASTERPAGE_SHOW_SIDER_ICON,
    MASTERPAGE_SHOW_SIDER,
    SHOW_CREATE_WORKFLOW_MODAL
} from "../actions/ActionTypes";

const initialState = {
    showHeader: true,
    showFooter: true,
    minHeight: 80,
    showSiderIcon: false,
    showSider: true,
    showCreateWorkFlowModal:false
};

export const masterpage = (state = initialState, action) => {
    switch (action.type) {
        case MASTERPAGEHEADER_TRIGGER:
            return { ...state, showHeader: action.payload };
        case MASTERPAGEFOOTER_TRIGGER:
            return { ...state, showFooter: action.payload };
        case MASTERPAGECONTENT_MIN_HEIGHT:
            return { ...state, minHeight: action.payload };
        case MASTERPAGE_SHOW_SIDER_ICON:
            return { ...state, showSiderIcon: action.payload };
        case MASTERPAGE_SHOW_SIDER:
            return { ...state, showSider: action.payload };
        case SHOW_CREATE_WORKFLOW_MODAL:
            return {...state,showCreateWorkFlowModal:action.payload};
        default:
            return state;
    }
};