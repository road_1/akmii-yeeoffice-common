import { AkIdentity, AkIdentityType, AkUser } from "./../api/identity/identity";
import { AkUtil } from "./../util/util";
import {
    REQUEST_USERS_BEGIN,
    IDENTITY_USER_LOADED,
    IDENTITY_ORGANIZATION_LOADED,
    IDENTITY_ORGANIZATION_SHOULD_LOAD,
    IDENTITY_LOCATION_LOADED,
    IDENTITY_LOCATION_SHOULD_LOAD,
    IDENTITY_POSITION_LOADED,
    IDENTITY_POSITION_SHOULD_LOAD,
    IDENTITY_GROUP_LOADED,
    IDENTITY_GROUP_SHOULD_LOAD,
} from "../actions/ActionTypes";
import { AkDictionaryString, AkGlobal } from "../index";

export class IdentityUtil {
    // static getRequestIDs(payload: string | string[] | AkIdentity | AkIdentity[]): string[] {
    //     const ids: string[] = [];

    //     AkUtil.each(AkUtil.toArray(payload), i => {
    //         if (typeof i === "string") {
    //             ids.push(i);
    //         } else {
    //             ids.push(i.ID);
    //         }
    //     });

    //     return ids;
    // }

    // static getResolvedIDs(dict: AkDictionaryString<IdentityUserHolder>): string[] {
    //     return Object.keys(dict);
    // }

    static diffForRequest(dict: AkDictionaryString<IdentityUserHolder>, payload: string | string[] | AkIdentity | AkIdentity[]): AkIdentity[] {
        //避免多次循环
        let rs = [];
        AkUtil.each(AkUtil.toArray(payload), i => {
            let id = (typeof i === "string") ? i: i.ID;
            if (!dict.hasOwnProperty(id)) {
                rs.push({ID:id, Type: AkIdentityType.User});
            }
        });
        // const reqIDs = IdentityUtil.getRequestIDs(payload);
                
        return rs;

        // const resIDs = IdentityUtil.getResolvedIDs(dict);
        // return reqIDs.filter(i => resIDs.indexOf(i) === -1).map(i => ({ ID: i, Type: AkIdentityType.User }));
    }

    static getOrSet(dict: AkDictionaryString<IdentityUserHolder>, id: string): IdentityUserHolder {
        let holder = dict[id];
        if (!holder) {
            holder = { loading: false, data: null };
            dict[id] = holder;
        }
        return holder;
    }

    static setLoading(dict: AkDictionaryString<IdentityUserHolder>, identities: AkIdentity[]) {
        const newDict = {};
        identities.forEach(identity => {
            const holder = IdentityUtil.getOrSet(dict, identity.ID);
            holder.loading = true;
            newDict[identity.ID] = holder;
        });

        return { ...dict, ...newDict };
    }

    static setData(dict: AkDictionaryString<IdentityUserHolder>, data: AkUser[]) {
        const newDict = {};
        data.forEach(user => {
            const holder = IdentityUtil.getOrSet(dict, user.ID);
            holder.loading = false;
            holder.data = user;
            newDict[user.ID] = holder;
        });

        return { ...dict, ...newDict };
    }

    /**
     * 是否是完整的identity对象
     * @param p 
     */
    static isValidIdentity(p) {
        if (p && p instanceof Object && ("ID" in p && "Type" in p && "Name" in p && "Attr" in p)) {
            return true;
        } else {
            return false;
        }
    }

    // /**
    //  * 直接更新store的user cache，不使用redux的reducer，用于用户搜索结果的更新避免触发render（是否有问题，需要持续跟踪）
    //  * @param users 
    //  */
    // static updateUserCacheDirectly(users: AkUser[]) {        
    //     let { identity: { identityDict } } = AkGlobal.store.getState();
    //     this.setData(identityDict, users);
    // }
}

export interface IdentityUserHolder {
    loading: boolean;
    data?: AkUser;
}

const initialState = {
    identityDict: {},
    locations: [],
    locationLoaded: false,
    organizations: [],
    organizationLoaded: false,
    groups: [],
    groupLoaded: false,
    positions: [],
    positionLoaded: false
};

export const identity = (state = initialState, action) => {
    let identityDict;

    switch (action.type) {
        case REQUEST_USERS_BEGIN:
            identityDict = state.identityDict;
            identityDict = IdentityUtil.setLoading(identityDict, action.payload);
            return { ...state, identityDict };
        case IDENTITY_USER_LOADED:
            identityDict = state.identityDict;
            identityDict = IdentityUtil.setData(identityDict, action.payload);
            return { ...state, identityDict };
        case IDENTITY_ORGANIZATION_LOADED:
            return { ...state, organizations: action.payload, organizationLoaded: true };
        case IDENTITY_LOCATION_LOADED:
            return { ...state, locations: action.payload, locationLoaded: true };
        case IDENTITY_POSITION_LOADED:
            return { ...state, positions: action.payload, positionLoaded: true };
        case IDENTITY_GROUP_LOADED:
            return { ...state, groups: action.payload, groupLoaded: true };
        case IDENTITY_ORGANIZATION_SHOULD_LOAD:
            return { ...state, organizations: [], organizationLoaded: false };
        case IDENTITY_LOCATION_SHOULD_LOAD:
            return { ...state, locations: [], locationLoaded: false };
        case IDENTITY_POSITION_SHOULD_LOAD:
            return { ...state, positions: [], positionLoaded: false };
        case IDENTITY_GROUP_SHOULD_LOAD:
            return { ...state, groups: [], groupLoaded: false };
        default:
            return state;
    }
}