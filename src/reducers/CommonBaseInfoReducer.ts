import { COMMON_USERBASEINFO } from "../actions/ActionTypes";
import moment = require("moment");

const initialState = {
    baseInfo: {
        UserID: 0,
        TenantID: 0,
        TimeZone: null,
        TimeZoneFe: moment.tz.guess(),
        ServerTimeZone: moment.tz.guess(),
        DateFormat: "YYYY-MM-DD",
        LanguageCode: "",
        isSuccess: null
    }
}

export const commonBaseInfo = (state = initialState, action) => {
    switch (action.type) {
        case COMMON_USERBASEINFO:
            const baseInfo = action.payload;
            return { ...state, baseInfo };
        default:
            return state;
    }
}