import { RECEIVE_CHAT_LIST, SAVE_CHAT_ITEM, SHOW_CHAT_ITEM, SHOW_CHAT_INFO, SHOW_CHAT_LIST, SHOW_CHAT_DETAIL } from '../actions/ActionTypes';
import { ChatPullModel } from '../api/masterpage/socketmodel';
export interface Notification {
    accountChatList?: ChatPullModel[];
    //chatMessagePullList?: ChatMessagePullModel[];
    chatItem?: ChatPullModel;
    showChatItem?: boolean;
    showChatInfo?: boolean;
    chatPerson?: string[];
}

const initialState: Notification = {
    accountChatList: [],
    //chatMessagePullList: [],
    chatItem: undefined,
    showChatItem: false,
    showChatInfo: false,
    chatPerson: []
};

export const notification = (state: Notification = initialState, action) => {
    switch (action.type) {
        case RECEIVE_CHAT_LIST:
            const accountChatList = action.payload;
            return { ...state, accountChatList };
        case SAVE_CHAT_ITEM:
            const chatItem = action.payload;
            return { ...state, chatItem };
        case SHOW_CHAT_ITEM:
            const showChatItem = action.payload;
            return { ...state, showChatItem };
        case SHOW_CHAT_INFO:
            const showChatInfo = action.payload;
            return { ...state, showChatInfo };
        case SHOW_CHAT_LIST:
            const showChatList = action.payload;
            return { ...state, showChatList };
        case SHOW_CHAT_DETAIL:
            const chatPerson = action.payload;
            return { ...state, chatPerson };
        default:
            return state;
    }
}