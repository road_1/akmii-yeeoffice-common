import { COMMENT_ADD, COMMENT_LOADED } from '../actions/ActionTypes';
import { CommentModel, CommentModelRequest, GetCommentRequest } from '../api/comment/commentmodel';

export interface CommentHolder {
    status?: "done" | "error" | "loading";
    errorCount?: number;
    appKey?: string;
    dataID?: string;
    dataType?: string;
    pageSize?: number;
    data?: CommentModel[]
}

export type CommentCacheDict = {
    [key: string]: CommentHolder;
};

export interface CommentReducer {
    commentDict?: CommentCacheDict;
}

const initialState: CommentReducer = {
    commentDict: {}
};

export class CommentReducerUtil {
    static getKey(appKey, dataType, dataID) {
        return `${appKey}-${dataType}-${dataID}`;
    }

    static getCacheKey(request: GetCommentRequest) {
        return this.getKey(request.appKey, request.dataType, request.dataID);
    }

    static getFromCacheByKey(key, cache: CommentCacheDict): CommentHolder {
        if (key && cache) {
            return cache[key];
        }
        return null;
    }

    static getFromCache(request: GetCommentRequest, cache: CommentCacheDict): CommentHolder {
        if (cache && request && request.appKey && request.dataID && request.dataType) {
            let key = this.getCacheKey(request);
            return this.getFromCacheByKey(key, cache);
        }
        return null;
    }
}

export const commentReducer = (state: CommentReducer = initialState, action) => {
    switch (action.type) {
        case COMMENT_LOADED:
            const holder: CommentHolder = action.payload;
            let key = CommentReducerUtil.getKey(holder.appKey, holder.dataType, holder.dataID);
            let commentDict = { ...state.commentDict, [key]: holder };
            return { ...state, commentDict };
        default:
            return state;
    }
}
