import { ListSetDefModal } from "../api/listSet-model";
import { AkDictionaryString } from "../util/common";
import { UPDATE_LISTSET_DEF, UPDATE_LISTSET_LISTS, UPDATE_LISTSET_PAGE, UPDATE_LISTSET_LOADING } from "../actions/listSetActionTypes";

const inititalState: ListSetDefModal = {
    listset: null,
    lists: [],
    loading: true,
    pages: [],
};

export const listSets = (states: AkDictionaryString<ListSetDefModal> = {}, action) => {
    const { payload } = action;
    const customType = payload && payload.customType;
    let state = (customType && states[customType]) || inititalState;

    switch (action.type) {
        case UPDATE_LISTSET_DEF:
            state = { ...state, listset: payload.listDef };
            return { ...states, [customType]: state };
        case UPDATE_LISTSET_LISTS:
            state = { ...state, lists: payload.lists };
            return { ...states, [customType]: state };
        case UPDATE_LISTSET_PAGE:
            state = { ...state, pages: payload.pages };
            return { ...states, [customType]: state };
        case UPDATE_LISTSET_LOADING:
            state = { ...state, loading: payload.loading };
            return { ...states, [customType]: state };
        default:
            return states;
    }
};
