import { UPDATE_APPCENTER_LIST, UPDATA_SELECT_APPCENTER, UPDATE_PER, UPDATE_SYSTEM_APP_LIST, UPDATE_PORTAL_LIST, UPDATE_NAVIGATION_LIST } from "../actions/ActionTypes";
import { AppCenterListResponse } from "../api/appcenter/appcentermodal";
import { AppCenterPermissionEnum, NavigationModel } from "..";
import { PortalViewModel } from "../components/masterpage/navigator";

export interface AppCenterState {
    systemAppList?: NavigationModel[];
    appCenterList?: AppCenterListResponse[];
    portalTreeData?: PortalViewModel;
    selectApp?: AppCenterListResponse;
    per?: AppCenterPermissionEnum;
    navigationList?: any[];
}

const initialState: AppCenterState = {
    systemAppList: null,
    appCenterList: null,
    selectApp: null,
    portalTreeData: null,
    per: AppCenterPermissionEnum.Manage,
    navigationList: null
};

export const appCenter = (state: AppCenterState = initialState, action) => {
    switch (action.type) {
        case UPDATE_SYSTEM_APP_LIST:
            const systemAppList = action.payload;
            return { ...state, systemAppList };
        case UPDATE_PORTAL_LIST:
        const portalTreeData =  action.payload;
            return { ...state, portalTreeData }
        case UPDATE_APPCENTER_LIST:
            const appCenterList = action.payload;
            return { ...state, appCenterList };
        case UPDATA_SELECT_APPCENTER:
            const selectApp = action.payload;
            return { ...state, selectApp };
        case UPDATE_PER:
            const per = action.payload;
            return { ...state, per }
        case UPDATE_NAVIGATION_LIST: 
        const navigationList = action.payload;
            return {...state, navigationList}
        default:
            return state;
    }
}