import { FILE_LIBRARY_LOADED } from "../actions/ActionTypes";

const initialState = {
    libraryDict: {},
};

export const file = (state = initialState, action) => {
    switch (action.type) {
        case FILE_LIBRARY_LOADED:
            let libraryDict = state.libraryDict;
            libraryDict[action.payload.code] = action.payload.data;
            return { ...state, libraryDict };
        default:
            return state;
    }
}