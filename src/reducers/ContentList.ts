import { ContentListItem, ContentListField } from "./../api/content-list/content-list-model";
import { CONTENT_LIST_LOADED, CONTENT_FIELD_LOADED } from "../actions/ActionTypes";
import { AkDictionaryString } from "../index";

export interface ContentListsHolder {
    lists?: ContentListItem[];
    listFieldDict?: AkDictionaryString<ContentListField[]>;
}

const initialState: ContentListsHolder = {
    lists: [],
    listFieldDict: {}
};

export const contentList = (state: ContentListsHolder = initialState, action) => {
    switch (action.type) {
        case CONTENT_LIST_LOADED:
            const lists = action.payload;
            return { ...state, lists };
        case CONTENT_FIELD_LOADED:
            const newDict = {};
            const { fields, listID } = action.payload;
            newDict[listID] = fields;
            const listFieldDict = { ...state.listFieldDict, ...newDict };
            return { ...state, listFieldDict };
        default:
            return state;
    }
}