import { APPINFO_LOADED } from "../actions/ActionTypes";
const initialState = {
    isAppAdmin: undefined
};

export const admin = (state = initialState, action) => {
    switch (action.type) {
        case APPINFO_LOADED:
            return { ...state, isAppAdmin: action.payload };
        default:
            return state;
    }
};
