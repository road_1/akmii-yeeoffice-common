import {HISTORYLOG_LOADED} from "../actions/ActionTypes";
const initialState = {
    addCount: 0
}

export const addAssisign = (state = initialState, action) => {
    switch (action.type) {
        case HISTORYLOG_LOADED:
            let count=state.addCount+1;
            return {...state, addCount: count}
        default:
            return state;
    }
}
