import {
    METADATA_CATEGORY_LOADED,
    METADATA_LOADED,
    METADATA_REQUEST_BEGIN
} from "../actions/ActionTypes";
import { MetadataInfo } from "../api/flowcraft/metadata";

export class MetadataDictUtil {
    static contains(dict, categoryCode: string, parentCode?: string): boolean {
        const key = getDictKey(categoryCode, parentCode);
        return dict.hasOwnProperty(key);
    }

    static requireFetch(dict, categoryCode: string, parentCode?: string): boolean {
        if (parentCode === "") return false;
        const metaholder = MetadataDictUtil.get(dict, categoryCode, parentCode);
        if (metaholder && (metaholder.loading || metaholder.data)) {
            return false;
        }
        return true;
    }

    static get(dict, categoryCode: string, parentCode?: string): MetadataHolder {
        const key = getDictKey(categoryCode, parentCode);
        return dict[key];
    }

    static getMetadataByID(dict, id: string | string[], categoryCode: string, parentCode?: string): MetadataInfo[] {
        const holder = MetadataDictUtil.get(dict, categoryCode, parentCode);
        if (holder && holder.data) {
            let ids: string[] = [];
            let result = [];
            if (id instanceof Array) {
                ids = id;
            } else {
                ids.push(id);
            }

            holder.data.forEach(item => {
                 if(ids.indexOf(item.ID)!==-1){
                    result.push(item)
                 }
            })

            return result;
            // return holder.data.find(item => item.ID === id);
        }
        return null;
    }

    /**
     * 不存在则创建新的对象并返回
     * @param categoryCode
     * @param parentCode
     */
    static getOrSet(dict, categoryCode: string, parentCode?: string): MetadataHolder {
        const key = getDictKey(categoryCode, parentCode);
        let holder = dict[key];
        if (!holder) {
            holder = { loading: false, data: null }
            dict[key] = holder;
        }
        return holder;
    }

    static setLoading(dict, categoryCode: string, parentCode?: string) {
        const key = getDictKey(categoryCode, parentCode);
        let holder = MetadataDictUtil.getOrSet(dict, categoryCode, parentCode);
        holder.loading = true;
        let newState = {};
        newState[key] = holder;
        return { ...dict, ...newState };
    }

    static setData(dict, data: MetadataInfo[], categoryCode: string, parentCode?: string) {
        const key = getDictKey(categoryCode, parentCode);
        let holder = MetadataDictUtil.getOrSet(dict, categoryCode, parentCode);
        holder.loading = false;
        holder.data = data;
        let newState = {};
        newState[key] = holder;
        return { ...dict, ...newState };
    }
}

const initialState = {
    metadataCategories: [],
    metadataDict: {}
}

function getDictKey(categoryCode: string, parentCode?: string) {
    if (parentCode && parentCode !== categoryCode && parentCode !== "0" && parentCode !== "") {
        return `${categoryCode}-${parentCode}`;
    } else {
        return categoryCode;
    }
}

export interface MetadataHolder {
    loading: boolean;
    data?: MetadataInfo[]
}

export const metadata = (state = initialState, action) => {
    let metadataDict;

    switch (action.type) {
        case METADATA_REQUEST_BEGIN:
            metadataDict = state.metadataDict;
            metadataDict = MetadataDictUtil.setLoading(metadataDict, action.payload.categoryCode, action.payload.parentCode);
            return { ...state, metadataDict };
        case METADATA_LOADED:
            metadataDict = state.metadataDict;
            metadataDict = MetadataDictUtil.setData(metadataDict, action.payload.items, action.payload.categoryCode, action.payload.parentCode);
            return { ...state, metadataDict };
        case METADATA_CATEGORY_LOADED:
            const metadataCategories = action.payload;
            return { ...state, metadataCategories };
        default:
            return state;
    }
}