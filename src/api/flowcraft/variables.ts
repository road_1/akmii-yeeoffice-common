import { AkRequest, AkResponse, Request, AppKeys, AkContext } from "../../util/";

export interface GetVariablesRequest extends AkRequest {
    applicationID?: string;
}

export interface GetVariablesResponse extends AkResponse {
    Variables?: {};
}

export class ProcVariables {
    static getVariables(data: GetVariablesRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.Flowcraft) + "/api/variables";
        return new Request<GetVariablesRequest, GetVariablesResponse>().get(url, data);
    }
}