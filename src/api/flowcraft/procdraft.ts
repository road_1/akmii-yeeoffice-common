import { Request, AkResponse, AkRequest, AkBase } from "../../util/";
import { AppKeys, AkContext } from "../../util/common";

export interface ProcDraftModel extends AkBase {
    ApplicantID: string;
    /** ProcDraftID */
    ProcDraftID: string;
    /** ProcDefID */
    ProcDefID: string;
    DefKey: string;
    /** 流程名称 */
    FlowName: string;
    /** 表单数据 */
    FormDATA: string;
    /** 创建草稿人 */
    CreatedByName: string;
    FormUrl: string;
    /** 租户ID Index */
    TenantID?: string;
    /** 创建时间 */
    Created?: Date;
    /** 创建时间对应的字符串形式 */
    CreatedStr?: string;
    /** 创建人 */
    CreatedBy?: string;
    /** 修改时间 */
    Modified?: Date;
    /** 修改时间对应的字符串形式 */
    ModifiedStr?: string;
    /** 修改人 */
    ModifiedBy?: string;
}
/**保存草稿 */
export interface PostProcDraftResponse extends AkResponse { }
export interface PostProcDraftRequest extends AkRequest {
    ProcDraftID?: string;
    ProcDefID?: string;
    ApplicantID?: string;
    DefKey: string;
    FormData?: string;
}
export interface GetProcDraftRequest extends AkRequest {
    procDraftID: string;
}
export interface GetProcDraftResponse extends AkResponse {
    Data?: ProcDraftModel;
}

export class ProcDraftsAPI {
    /**保存草稿 */
    static postProcDrafts(data?: PostProcDraftRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.Flowcraft) + "/api/procdrafts";
        return new Request<PostProcDraftRequest,
            PostProcDraftResponse>().post(url, data);
    }

    static putProcDrafts(data?: PostProcDraftRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.Flowcraft) + "/api/procdrafts";
        return new Request<PostProcDraftRequest,
            PostProcDraftResponse>().put(url, data);
    }

    static getProcDraftByID(data?: GetProcDraftRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.Flowcraft) + "/api/procdrafts/id";
        return new Request<GetProcDraftRequest,
            GetProcDraftResponse>().get(url, data);
    }
}