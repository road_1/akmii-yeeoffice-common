import { AppKeys, AkRequest, AkResponse, Request } from "../../util/";
import { AkContext } from "../../util/common";

export interface GetAttachmentUrlRequest extends AkRequest {
    /**
     * 文件名
     *
     * @type {string}
     * @memberOf GetAttachmentUrl
     */
    fileName: string;
    /**
     * 扩展名
     *
     * @type {string}
     * @memberOf GetAttachmentUrl
     */
    fileExt: string;
    /**
     * 文件长度
     *
     * @type {string}
     * @memberOf GetAttachmentUrl
     */
    fileLength: string;
    /**
     * 文件MD5
     *
     * @type {string}
     * @memberOf GetAttachmentUrl
     */
    md5: string;
    /**
     * 备注
     *
     * @type {string}
     * @memberOf GetAttachmentUrl
     */
    comment?: string;
    appKey?: string;
}

export interface GetAttachmentUrlResponse extends AkResponse {
    Data?: {
        Url: string;
        FileKey: string;
    };
}

export interface DownAttachmentRequest extends AkRequest {
    FileKey: string;
}

export interface DownAttachmentResponse extends AkResponse { }

export const enum AttachmentStatus {
    Uploading = 1,
    Success = 2,
    Error = 3,
    Remove = 4,
    Cancel = 5
}

export enum AttachmentFileExt {
    doc,
    docx,
    xls,
    xlsx,
    ppt,
    pptx,
    png,
    jpg,
    jpge
}

export interface PutAttachementStatusRequest extends AkRequest {
    FileKey: string;
    Status: AttachmentStatus;
}

export interface PutAttachementStatusResponse extends AkResponse { }

export interface GetIconsUrlUploadRequest extends AkRequest {
    appKey?: string;
    md5: string;
}

export interface GetIconsUrlUploadResposne extends AkResponse {
    Data?: IconsUploadModel;
}

export interface IconsUploadModel {
    Url: string;
    DownloadUrl: string;
}

export class AttachmentAPI {
    /**
     * 获取上传附件的URL
     *
     * @static
     * @param {GetAttachmentUrlRequest} data
     * @returns
     *
     * @memberOf AttachmentAPI
     */
    static GetAttachmentUrl(data: GetAttachmentUrlRequest) {
        data.appKey = AkContext.getAppKey();
        let url =  AkContext.getAppInfoAPI_URL(AppKeys.FileServer) + "/api/attachments/url/upload";
        return new Request<GetAttachmentUrlRequest, GetAttachmentUrlResponse>().post(url, data);
    }

    /**
     * 修改文件状态
     *
     * @static
     * @param {PutAttachementStatusRequest} data
     * @returns
     *
     * @memberOf AttachmentAPI
     */
    static PutAttachementStatus(data: PutAttachementStatusRequest) {
        let url = AkContext.getAppInfoAPI_URL(AppKeys.FileServer) + "/api/attachments/editStatus";
        return new Request<PutAttachementStatusRequest, PutAttachementStatusResponse>().put(url, data);
    }

    static DownAttachment(data: DownAttachmentRequest) {
        let url = AkContext.getAppInfoAPI_URL(AppKeys.FileServer) + "/api/attachments/url/download";
        return new Request<DownAttachmentRequest, DownAttachmentResponse>().get(url, data);
    }
}