import { Request, AkResponse, AkRequest, AkBase } from "../../util/";
import { BaseModel } from "./BaseModel";

/**委托 获取流程名称列表*/
export interface GetFlowListRequest extends AkRequest {
    categoryID?: string;
    flowName?:string;
}
/**委托 获取aiib流程名称列表*/
export interface GetAiibFlowListRequest extends AkRequest {
    flowName?:string;
}
export interface ProcDefBrief extends BaseModel {
    DefKey : string;
    DefName : string;
}

/**委托流程名称列表*/
export interface ProcDefBriefListResponse extends AkResponse {
    Data?: ProcDefBrief[];
}
export interface ProcDefDelegate extends BaseModel{
    Key :string;
    ProcDefName  :string;
    ListID  :string;
}

/**委托Aiib流程名称列表*/
export interface ProcDefDelegateResponse extends AkResponse {
    Data?: ProcDefDelegate[];
}

export class ProcDefsAPI {
  
    /**委托 获取流程名称列表*/
    static async getFlowList(data?: GetFlowListRequest) {
        let url: string = "/api/procdefs/brief";
        return new Request<GetFlowListRequest, ProcDefBriefListResponse>().get(url, data);
    }
     /**委托 aiib获取流程名称列表*/
     static async getAiibFlowList(data?: GetAiibFlowListRequest) {
        let url: string = "api/procdefs/flowdelegates";
        return new Request<GetAiibFlowListRequest,  ProcDefDelegateResponse>().get(url, data);
    }
}
