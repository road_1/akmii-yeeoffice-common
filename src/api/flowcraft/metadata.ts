import { Request } from "../../util/request";
import { AppKeys, AkContext, AkBase, AkResponse, AkRequest } from "../../util/common";

export interface MetadataModel {
    /** 类别 */
    CategoryID: string;
    /** 编号 */
    Code: string;
    /** 描述 */
    Description: string;
    /** 扩展 */
    Ext: string;
    /** PK */
    ID: string;
    /** 级别 */
    Level: number;
    /** 多语言 */
    Localization: string;
    /** mapping */
    Mapping: string;
    /** 名字 */
    Name: string;
    /** 顺序 */
    Order: number;
    /** 上级 */
    ParentID: string;
    /** 租户ID Index */
    TenantID?: string;
    /** 创建时间 */
    Created?: Date;
    /** 创建时间对应的字符串形式 */
    CreatedStr?: string;
    /** 创建人 */
    CreatedBy?: string;
    /** 修改时间 */
    Modified?: Date;
    /** 修改时间对应的字符串形式 */
    ModifiedStr?: string;
    /** 修改人 */
    ModifiedBy?: string;
}
export interface MetadataModelResponse extends AkResponse {
    Data?: MetadataModel[];
}
export interface GetMetadataModelRequest extends AkRequest {
    categoryCode: string;
    parentCode: string;
}

export interface GetMetaDataByID {
    id: string;
}
/**MetaData 基础信息 */
export interface MetadataInfo extends AkBase {
    CategoryID?: string;
    Code?: string;
    Description?: string;
    HasChild?: boolean;
    Ext?: string;
    ID?: string;
    Level?: number;
    Localization?: string;
    Mapping?: string;
    Name?: string;
    Order?: number;
    ParentID?: string;
    Status?: number;
    TenantID?: string;
    CreatedStr?: string;
    CreatedBy?: string;
    ModifiedStr?: string;
    ModifiedBy?: string;
}
/**MetaData 分类信息 */
export interface MetadataCategoryInfo extends AkBase {
    CategoryID: string;
    Code: string;
    Description: string;
    Ext: string;
    Localization: string;
    Name: string;
    Status: number;
    TenantID: string;
    CreatedStr: string;
    CreatedBy: string;
    ModifiedStr: string;
    ModifiedBy: string;
}
export interface GetMetadataCategoryResponse extends AkResponse {
    Data?: MetadataCategoryInfo[];
}
/**获取metadata**/
export interface GetMetadataRequest extends AkRequest {
    categoryID: string;
    parentID?: string;
    isChild?: boolean;
    status?: number;//0禁用 1启用 -1全部
}
/**参数管理列表 */
export interface GetMetadataResponse extends AkResponse {
    Data?: MetadataInfo[];
}

export interface GetMetadataByIDResponse extends AkResponse {
    Data?: MetadataInfo;
}

export class MetadataAPI {

    /**
     * 查询metadata的分类
     * @returns {Promise<GetCategoryResponse>}
     */
    static getCategorys() {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/metadata/categories";
        return new Request<null,
            GetMetadataCategoryResponse>().get(url, undefined, []);
    }

    /**
     * 查询metadata，返回一级的数据
     * @param request
     * @returns {Promise<GetCategoryResponse>}
     */
    static get(request: GetMetadataRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/metadata";
        return new Request<GetMetadataRequest,
            GetMetadataResponse>().get(url, request, []);
    }
    /**
     * 根据parentcode查询子级
     * @param request
     * @returns {Promise<MetadataModelResponse>}
     */
    static getByCode(request: GetMetadataModelRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/metadata/parentcode";
        return new Request<GetMetadataModelRequest,
            MetadataModelResponse>().get(url, request, []);
    }

    /**
     * 根据ID查询自己
     */
    static getByID(request: GetMetaDataByID) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/metadata/metadataid";
        return new Request<GetMetaDataByID,
            GetMetadataByIDResponse>().get(url, request, []);
    }
}
