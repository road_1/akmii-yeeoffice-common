import { AppKeys, AkContext } from "../../util/common";
import { AkRequest, AkResponse, Request } from "../../util/";
import { TaskDetailInfo } from "./index";
import { GetProcessLogResponse } from "./task";
import { IdentityAPI } from "../identity/identity";

export let ApplicationStatusLocale = "model.application.status.";
export enum ApplicationStatusEnum {
    /**
     * 草稿
     */
    Draft = -2,
    Any = -1,
    /**开始 */
    Start = 0,
    /** 运行中 */
    Running = 1,
    /** 已结束 */
    Complete = 2,
    /** 拒绝 */
    Rejected = 3,
    /** 出错 */
    Error = 4,
    /** 撤回流程 */
    Revoked = 5,
    /** 流程撤回中 */
    Revoking = 51,
    /** 取消流程 */
    Canceled = 6,
    /**流程取消中 */
    Cancelling = 61
}

export const ApplicationStatusColor = {
    /** 运行中 */
    Running: "rgb(255, 191, 0)",
    /** 已结束 */
    Complete: "#18ba9b",
    /** 拒绝 */
    Rejected: "#f04134",
    /** 出错 */
    Error: "#f04134",
    /** 撤回流程 */
    Revoked: "#bbb",
    /** 取消流程 */
    Cancelled: "#bbb"
}

/**开启一个流程实例 By Key*/
export interface PostStartProcInstByKeyResponse extends AkResponse { }
export interface PostStartProcInstByKeyRequest extends AkRequest {
    Key?: string;
    Variables?: {};
    ApplicationID?: string;
    ProcDraftID?: string;
    ApplicantID?: string;
    applicationext2?: string;
}
export interface GetApplicantRequest extends AkRequest {
    userID?: string;
}
export interface GetApplicantResponse extends AkResponse {
    Data?: TaskDetailInfo;
}
export interface GetApplicantDetailRequest extends AkRequest {
    applicationID?: string;
}
export interface GetApplicantDetailResponse extends AkResponse {
    Data?: TaskDetailInfo;
}
export class ProcInstAPI {
    /**开启一个流程By Key */
    static postStartProcInstByKey(data: PostStartProcInstByKeyRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.Flowcraft) + "/api/procinsts/Start/key";
        return new Request<PostStartProcInstByKeyRequest,
            PostStartProcInstByKeyResponse>().post(url, data);
    }

    /** 获取流程实例详情 */
    static getApplicantByUserID(data: GetApplicantRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.Flowcraft) + "/api/applications/applicant";
        return new Request<GetApplicantRequest,
            GetApplicantResponse>().get(url, data);
    }
    /**
     * 获取流程信息
     * @param data
     */
    static getApplicationByID(data: GetApplicantDetailRequest) {
        let url = AkContext.getAppInfoAPI_URL(AppKeys.Flowcraft) + "/api/applications/detail";
        return new Request<GetApplicantDetailRequest,
            GetApplicantDetailResponse>().get(url, data);
    }

    /**
     * 申请取消 取消之后不允许再提交
     * 
     * @static
     * @param {GetApplicantDetailRequest} data 
     * @returns 
     * 
     * @memberOf ProcInstAPI
     */
    static async putApplicationCancel(data: GetApplicantDetailRequest) {
        const url = "/api/applications/cancel";
        return new Request<GetApplicantDetailRequest, AkResponse>().put(url, data);
    }

    /**
     * 申请撤回
     * 
     * @static
     * @param {GetApplicantDetailRequest} data 
     * @returns 
     * 
     * @memberOf ProcInstAPI
     */
    static async putApplicationRevoke(data: GetApplicantDetailRequest) {
        const url = "/api/applications/revoke";
        return new Request<GetApplicantDetailRequest, AkResponse>().put(url, data);
    }
    /**
     *查找流程日志
     * @param data APPID
     */
    static getProcessLog(data: GetApplicantDetailRequest) {
        let url = AkContext.getAppInfoAPI_URL(AppKeys.Flowcraft) + "/api/applications/processlog";
        return new Request<GetApplicantDetailRequest,
            GetProcessLogResponse>().get(url, data);
    }

    /**
     * 获取流程pending的任务处理人
     * @param flowNos 
     */
    static activeAssignees(flowNos: string[]) {
        const url = "/api/tasks/assignees";
        return new Request<any, AkResponse>().post(url, { FlowNos: flowNos });
    }
}

export class GetAppUserAPI {
    static getAppAdmin = IdentityAPI.getAppAdmin
}