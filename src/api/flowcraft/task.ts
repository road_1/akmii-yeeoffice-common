import {ContentListField} from '../content-list/content-list-model';
import { AkRequest, AkResponse, AkItemResponse, AkContext, AppKeys } from "../../util/common";
import { Request } from "../../util/request";
import { BaseModel } from "./BaseModel";

export interface TaskInfo extends BaseModel {
    /** TenantID */
    TenantID?: string;
    /** TaskID */
    TaskID?: string;
    /** 流程实例ID */
    ProcInstID?: string;
    /** 流程定义ID */
    ProcDefID?: string;
    /** 流程名称 */
    ProcDefName?: string;
    /** 类别ID */
    CategoryID?: string;
    /** 类别名称 */
    CategoryName?: string;
    /** 执行ID */
    ExecutionID?: string;
    /** 任务对应的ActivityID */
    ActivityID?: string;
    /** Name */
    Name?: string;
    /** ParentTaskID */
    ParentTaskID?: string;
    /** Description */
    Description?: string;
    /** OwnerID */
    OwnerID?: string;
    /** 任务的分配对象 */
    AssigneeID?: string;
    /** 分配人名字 */
    AssigneeName?: string;
    /** 分配人头像 */
    AssigneePhoto?: string;
    /** 任务的代理人 */
    DelegateID?: string;
    /** 代理人名字 */
    DelegateName?: string;
    /** 优先级 */
    Priority?: number;
    /** on update CURRENT_TIMESTAMP(3) */
    StartTimeStr?: string;
    /** ClaimTime */
    ClaimTimeStr?: string;
    /** EndTime */
    EndTimeStr?: string;
    /** Duration */
    Duration?: string;
    /** DueDate */
    DueDateStr?: string;
    /** DeleteReason */
    DeleteReason?: string;
    /** Outcome */
    Outcome?: string;
    /** Comment */
    Comment?: string;
    /** TaskURL */
    TaskURL?: string;
    /** 是否存储表单数据 */
    IsSaveFormData?: boolean;
    /** FormDataID */
    FormDataID?: string;
    /** Ext */
    Ext?: string;
    /**Status */
    Status?: number;
    /**
     * 是否允许加签
     */
    IsAllowSign?: boolean;

    FlowNo?: string;
    CallProcInstID?: string;
    Child?: [any];
    Flags?: number;
    RecalledID?: string;
    BeRecalledID?: string;
}

export const enum TaskStatusEnum {
    /**  待办 */
    Pending = 1,
    /**  已完成 */
    Complete = 2,
    /**  待认领 */
    Candidate = 3
}

export const enum TaskFlagsEnum {
    Default = 0,

    /// <summary>
    /// 多人任务
    /// </summary>
    MultiTask = 1,

    /// <summary>
    /// 领用任务
    /// </summary>
    CandidateTask = 2,

    /// <summary>
    /// 是否可以召回任务
    /// </summary>
    RecallTask = 4,
    /// <summary>
    /// 该任务已不能召回了
    /// </summary>
    NoCanRecallTask=8,
    /// <summary>
    /// 任务可以转办
    /// </summary>
    AllowReassign = 16,
    /// <summary>
    /// 任务审批
    /// </summary>
    Approve = 32,
    /// <summary>
    /// 任务完成
    /// </summary>
    Complete = 64,
}

/**任务审批 */
export interface TaskDetailInfo {
    /**流程编号 */
    FlowNo?: string;
    /**状态。0:发起，1:运行中，2已结束,3:Reject 注意:只有reject的才可以重新发起 */
    AppStatus?: number;
    /**创建人ID */
    CreatedBy?: string;
    /**创建人名称 */
    CreatedByName?: string;
    /**CreateDateStr */
    ApplyDateStr?: string;
    /**申请人ID */
    ApplicantID?: string;
    /**申请人名称 */
    ApplicantName?: string;
    /**员工编号 */
    EmployeeNo?: string;
    /**职位 */
    JobTitle?: string;
    /**位置编码 */
    LocationID?: string;
    /**位置名称 */
    LocationName?: string;
    /**汇报经理ID */
    LineManagerID?: string;
    /**汇报经理名称 */
    LineManagerName?: string;
    /**部门ID */
    OrgID?: string;
    /**部门名称 */
    OrgName?: string;
    /**变量对象 */
    Variables?: any;
    /**流程日志列表 */
    ProcessLogList?: ProcessLog[];
    /**是否允许加签 */
    CurrentTask?: TaskInfo;
    /**申请单ID */
    ApplicationID?: string;
}

export interface ApplicantInfo {
    ApplicantID?: string;
    ApplicantName?: string;
    EmployeeNo?: string;
    JobTitle?: string;
    LocationID?: string;
    LocationName?: string;
    LineManagerID?: string;
    LineManagerName?: any;
    OrgID?: string;
    OrgName?: string;
    CreatedByName?: string;
    ListID?:string;
}

export interface ApplicationInfo {
    ApplicationID: string;
    CurrentProcInstID: string;
    HistoryProcInstIDs?: any;
    ApplicantID: string;
    Status: number;
    FlowNo: string;
    DefKey: string;
    FormUrl?: any;
    FlowName?: any;
    CategoryID?: any;
    CategoryName?: any;
    CreatedByName?: any;
    Version: number;
    Comment?: any;
    TenantID: string;
    CreatedStr: string;
    CreatedBy: string;
    ModifiedStr: string;
    ModifiedBy: string;
}

export interface TaskDetailV2Info {
    ApplicantInfo?: ApplicantInfo;
    Variables?: any;
    ApplicationInfo?: ApplicationInfo;
    ProcInstInfo?: ProcInstInfo;
    TaskInfo?: TaskInfo;
    ListFields?: ContentListField[];
}

export interface AttachmentFileInfo {
    fileName: string;
    fileSize: number;
    fileKey: string;
    fileInfo?: any;
}
/**审批日志信息信息 */
export interface ProcessLog {
    DefResourceID?: string;
    ImgResourceID?: string;
    ProcInstModel?: ProcInstInfo;
    TaskList?: TaskInfo[];
}
/**新建申请日志*/
export interface NewProcessLog {
    ResourceID: string;
    Name: string;
    Resource: string;
    TenantID: string;
    CreatedStr: string;
    CreatedBy: string;
    ModifiedStr: string;
    ModifiedBy: string;
}
export interface GetProcessLogResponse extends AkResponse {
    Data?: ProcessLog[];
}

export interface ProcInstInfo {
    /**流程实例ID */
    ProcInstID?: string;
    Name?: string;
    /**父级流程实例ID */
    ParentInstID?: string;
    /**流程定义ID */
    ProcDefID?: string;
    /**版本号 */
    Version?: string;
    /**分类ID */
    CategoryID?: string;
    /**分类名称 */
    CategoryName?: string;
    /** 流程启动时间*/
    StartTimeStr?: string;
    /**流程结束时间 */
    EndTimeStr?: string;
    /**流程运行毫秒数 */
    DURATION?: string;
    /**发起人ID */
    StartUserID?: string;
    /**流程开始ActivityID */
    StartActID?: string;
    /**流程结束ActivityID */
    EndActID?: string;
    /**0:未启动，1:运行中，2:结束，3:已删除，4:出错	 */
    Status?: string;
    /** 备注，如删除原因等*/
    Comment?: string;
    /**租户ID */
    TenantID?: string;
    CreatedStr?: string;
    CreatedBy?: string;
    ModifiedStr?: string;
    ModifiedBy?: string;
}
/**获取待审批内容 */
export interface GetApproveInfoRequest extends AkRequest {
    taskID?: string;
    /**流程定义ID */
    procInstID?: string;
}
export const enum TaskRuntimeOrderByEnum {
    /**默认*/
    Default = 0,
    /**流程编号*/
    FlowNoUp = 11,
    /**流程编号*/
    FlowNoDown = 12,
    /**创建时间*/
    CreatedUp = 61,
    /**创建时间*/
    CreatedDown = 62
}
export interface GetTaskRequest extends AkRequest {
    type?: number;
    /**流程编号 */
    flowNo?: string;
    /**流程名称 */
    flowName?: string;
    /**申请人 */
    applicantID?: string;
    startTimeStr?: string;
    endTimeStr?: string;
    pageIndex?: number;
    pageSize?: number;
    /**排序 */
    orderbyIndex?: TaskRuntimeOrderByEnum;
}
export interface GetApproveInfoResponse extends AkResponse {
    // Data?: TaskApproval
    Data?: TaskDetailInfo;
}
/** 获取新建申请的流程日志 */
export interface GetNewProcessLogRequest extends AkRequest {
    defID?: string;
}
export interface GetNewProcessLogResponse extends AkResponse {
    Data?: NewProcessLog
}
/**处理任务 同意、拒绝*/
export interface PutTaskHandleRequest extends AkRequest {
    /**任务ID */
    TaskID?: string;
    /**审批结果 同意 拒绝 */
    Outcome?: "Approved" | "Rejected" | "Completed" | "SaveVariable";
    /**评论意见 */
    Comment?: string;
    /**描述 */
    Description?: string;
    /**变量 */
    Variables?: Object;
    applicationext2?:string;
}
export interface PutTaskHandleResponse extends AkResponse { }
/**签收任务 */
export interface PutTaskClaimRequest extends AkRequest {
    /**任务ID */
    TaskID?: string;
    /**任务的分配对象 */
    AssigneeID?: string;
}
export interface PutTaskClaimResponse extends AkResponse { }
/**签收任务 */
export interface PutTaskEndorseRequest extends AkRequest {
    /**任务ID */
    TaskID?: string;
    /**任务的分配对象 */
    AssigneeIDs?: string[];
    /**是否验证用户已存在 */
    isCheckUser?:boolean;
}

export interface PutTaskEndorseResponse extends AkResponse { }

/**转办任务 */
export interface PutTaskChangeRequest extends AkRequest {
    /**任务ID */
    TaskID?: string;
    /**任务的分配对象 */
    AssigneeID?: string;
}
export interface PutTaskChangeResponse extends AkResponse {
}

export interface GetTaskCountResponse extends AkResponse {
    Data?: {
        PendingCount: number;
        CandidateCount: number;
    };
}
export interface TaskModelListResponse extends AkResponse {
    Data?: TaskInfo[];
}

export interface PutCancelClaimRequest extends AkRequest {
    /**任务ID */
    TaskID?: string;
}

export interface GetPrintDefByPageIDRequest extends AkRequest {
    pageID?: string;
    urlPageID?: string;
}
//任务召回
export interface PutRecallRequest extends AkRequest {
    /**任务ID */
    TaskID?: string;
}
export class TaskAPI {
    /** 获取任务列表(我的) */
    static async getTask(data: GetTaskRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.Flowcraft) + "/api/tasks";
        return new Request<GetTaskRequest,
            TaskModelListResponse>().get(url, data);
    }
    /**
     * 获取对应的任务数量
     * @param data
     */
    static getTaskCount() {
        let url = AkContext.getAppInfoAPI_URL(AppKeys.Flowcraft) + "/api/tasks/counts";
        var request = new Request<any,
            GetTaskCountResponse>();
        request.defaultParam.IgnoreError = true;
        return request.get(url);
    }

    /**获取任务审批详情 */
    static getTaskDetail(data: GetApproveInfoRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.Flowcraft) + "/api/tasks/detail";
        return new Request<GetApproveInfoRequest,
            GetApproveInfoResponse>().get(url, data);
    }

    /** 申请页面获取流程日志 */
    static async getNewProcessLog(data: GetNewProcessLogRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.Flowcraft) + "/api/procdefs/defid";
        return new Request<GetNewProcessLogRequest,
            GetNewProcessLogResponse>().get(url, data);
    }

    /**获取任务日志 */
    static async getProcessLog(data: GetApproveInfoRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.Flowcraft) + "/api/tasks/processlog";
        return new Request<GetApproveInfoRequest,
            GetProcessLogResponse>().get(url, data);
    }
    /**审批结果 */
    static putTaskHandle(data: PutTaskHandleRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.Flowcraft) + "/api/tasks/handle";
        return new Request<PutTaskHandleRequest,
            PutTaskHandleResponse>().put(url, data);
    }
    /**签收任务 */
    static putClaimTask(data: PutTaskClaimRequest) {
        const url: string = AkContext.getAppInfoAPI_URL(AppKeys.Flowcraft) + "/api/tasks/claim";
        return new Request<PutTaskClaimRequest, PutTaskClaimResponse>().put(url, data);
    }
    /**加签 */
    static putEndorseTask(data: PutTaskEndorseRequest) {
        const url: string = AkContext.getAppInfoAPI_URL(AppKeys.Flowcraft) + "/api/tasks/endorse";
        return new Request<PutTaskEndorseRequest, PutTaskEndorseResponse>().put(url, data);
    }
    /**转办任务 */
    static putChangeTask(data: PutTaskChangeRequest) {
        const url: string = AkContext.getAppInfoAPI_URL(AppKeys.Flowcraft) + "/api/tasks/change";
        return new Request<PutTaskChangeRequest, PutTaskChangeResponse>().put(url, data);
    }
    /**取消领用 */
    static putCancelClaimTask(data: PutCancelClaimRequest) {
        const url: string = AkContext.getAppInfoAPI_URL(AppKeys.Flowcraft) + "/api/tasks/cancel/claim";
        return new Request<PutCancelClaimRequest, AkResponse>().put(url, data);
    }
    /**根据PageID获取表单定义 */
    static async getPrintDefByPageID(data?: GetPrintDefByPageIDRequest) {
        let url: string = "/api/procdefs/printtext";
        return new Request<GetPrintDefByPageIDRequest, AkItemResponse<string>>().get(url, data);
    }
    /**任务召回 */
    static async putRecallTask(data?: PutRecallRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.Flowcraft) + "/api/tasks/recall";
        return new Request<PutRecallRequest, AkResponse>().put(url, data);
    }
}