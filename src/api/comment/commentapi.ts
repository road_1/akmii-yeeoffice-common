import { AkResponse, AppKeys, AkContext } from "../../util/common";
import { Request } from "../../util/request";
import {
    GetCommentRequest,
    CommentListResponse,
    CommentModelRequest,
    GetCommentCountRequest,
    PutCommentStateRequest,
    PutCommentStateByIDRequest,
    PostCommentsByCounts
} from "./commentmodel";
import { AkRequest } from "../../index";
export class CommentAPI {

    static apiurl = process.env.commentapi;

    /**获取所有的评论，分页*/
    static async getComment(data?: GetCommentRequest) {

        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeComment) + "/api/content";
        return new Request<GetCommentRequest, CommentListResponse>().get(url, data);
    }

    /**添加评论*/
    static async postComment(data?: CommentModelRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeComment) + "/api/content";
        return new Request<CommentModelRequest, AkResponse>().post(url, data);
    }

    /**获取评论总数 */
    static async getCommentCount(data?: GetCommentCountRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeComment) + "/api/content/count";
        return new Request<GetCommentCountRequest, AkResponse>().get(url, data);
    }

    /**删除帖子的时候禁用评论 */
    static async putCommentState(data?: PutCommentStateRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeComment) + "/api/admin/content/editbydataid";
        return new Request<PutCommentStateRequest, AkResponse>().put(url, data);
    }

    /**禁用评论 */
    static async putCommentByIDState(data?: PutCommentStateByIDRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeComment) + "/api/content/editbyid";
        return new Request<PutCommentStateByIDRequest, AkResponse>().put(url, data);
    }

    static async PostCommentsByCounts(data?: PostCommentsByCounts) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeComment) + "/api/content/dataid/counts";
        return new Request<PostCommentsByCounts, AkResponse>().post(url, data);
    }
    static async getSupportInfobyCode(code:string) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/partnerchannel?code="+code;
        return new Request<AkRequest, AkResponse>().get(url);
    }
}