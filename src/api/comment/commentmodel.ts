import { AkRequest, AkResponse } from "../../util/common";

/**
 * saga delete request model
 */
export interface CommentDeleteRequest {
    ID: string,
    DataID: string,
    DataType: string,
    appKey: string
}

/**
 * saga add comment model
 */
export interface CommentAddRequest extends CommentModelRequest {
    onComplete?: (success: boolean) => void;
}

/**
 * saga more comment model
 */
export interface CommentMoreRequest extends GetCommentRequest {
    onComplete?: (success: boolean) => void;
}

export interface CommentModelRequest extends AkRequest {
    /**CategoryID */
    DataType?: string,
    /** 文章id */
    DataID: string,
    /** 多级id 1级0 */
    ParentID?: number,
    /** 评论内容 */
    ContentText: string,
    /** 回复给某人的id */
    TargetID?: number,
    appKey: string
}

export interface GetCommentRequest extends AkRequest {
    /**CategoryID */
    dataType?: string,
    /**文章id */
    dataID?: string,

    lastID?: string,
    pageSize?: number,
    /**上级评论id 没有不需要输入 */
    parentID?: number,
    appKey?: string
}

export interface GetCommentCountRequest extends AkRequest {
    /**
     只输入dataType查询的是整个知识库的回复数量
     */
    dataType: string,
    /**
     查询文章下的回复数量
     */
    dataID?: string,
    parentID?: number,
    appKey: string
}

export interface PutCommentStateRequest extends AkRequest {
    DataID: string,
    DataType: string,
    Status: boolean,
    appKey: string
}
export interface PutCommentStateByIDRequest extends AkRequest {
    ID: string,
    DataType: string,
    Status: boolean,
    appKey: string
}

export interface PostCommentsByCounts extends AkRequest {
    DataIDs: string[],
    AppKey: string,
    DataType: string,
    ParentID: string
}

export interface CommentModel {
    ID?: string,
    AppKey?: string,
    DataType?: string,
    ParentID?: string,
    DataID?: string,
    ContentText?: string,
    UserID?: string,
    TargetID?: number,
    CreatedStr?: string,
    CreatedBy?: string
    ModifiedStr?: string
    ModifiedBy?: string,
    User: CommentUser,
    TenantID?: string;
}

export interface CommentResponse extends AkResponse {
    Data?: CommentModel;
}
export interface CommentListResponse extends AkResponse {
    Data?: CommentModel[];
}
export interface CommentUser extends CommentResponse {
    Name?: string,
    Photo?: string,
    UserID?: string
}
