import { AkBaseModel } from "../../util/common";

export interface ListSetDefModal {
    listset?: ContentListItem;
    lists?: ContentListItem[];
    pages?: ContentListLayout[];
    loading?: boolean;
}

export interface ContentListItem extends AkBaseModel {
    AppID?: number;
    ListID?: string;
    Title?: string;
    Description?: string;
    Status?: ContentListStatusEnum;
    IconUrl?: string;
    Ext1?: string;
    Ext2?: string;
    Ext3?: string;
    MaxRole?: ContentListRightTypeEnum;
    Type?: ContentListType;
    LayoutView?: string;
    AdvanceList?: any;
    IsVerRecord?: boolean;
    WorkspaceID?: string;
    CustomType?: string;
}

export declare enum ContentListStatusEnum {
    Disabled = 0,
    Enabled = 1
}

export declare enum ContentListRightTypeEnum {
    Empty = 0,
    Read = 1,
    Write = 2,
    Manage = 4,
    Create = 8,
    Edit = 16,
    Remove = 32,
    Export = 64,
    Import = 128
}

export declare enum ContentListType {
    List = 1,
    Library = 2,
    Custom = 4,
    Calendar = 8,
    CalendarList = 9,
    LibraryList = 16,
    ListSet = 1024
}

export interface ContentListLayout {
    AppID?: number;
    ListID?: string;
    LayoutID?: string;
    Type?: ContentListLayoutType;
    Title?: string;
    LayoutView?: string | any;
    IsDefault?: boolean;
    IsItemPerm?: boolean;
    Read?: AkIdentity[];
    Ext1?: any;
    Ext2?: string;
    Ext3?: string;
}

export declare enum ContentListLayoutType {
    List = 0,
    Form = 1,
    CalenDar = 100,
    News = 999,
    MindMap = 101,
    TimeLine = 102,
    CustomPage = 103
}

export declare class AkIdentity {
    constructor(identity?: AkIdentity);
    ID: string;
    Name?: string;
    Type: number;
    Attr?: Object;
}