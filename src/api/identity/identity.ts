import {Request, AkRequest, AkResponse} from "../../util/";
import {AkUtil} from "../../util/util";
import {AppKeys} from "../../util/common";
import {AkContext} from "../../index";
import {simpleApiRequestCacheMiddleware} from "../common/middleware";
// import PrincipalType = SP.Utilities.PrincipalType;
// import PrincipalSource = SP.Utilities.PrincipalSource;

// export const AkIdentityType_User = 1;
// export const AkIdentityType_Organization = 2;
// export const AkIdentityType_Group = 3;
// export const AkIdentityType_Role = 4;
// export const AkIdentityType_Location = 5;
// export const AkIdentityType_SPSecurityGroup = 6;
// export const AkIdentityType_SPSiteGroup = 7;
// export const AkIdentityType_SPUser = 8;

export enum AkIdentityType {
    Unknown = 0,
    User = 1,
    Organization = 2,
    Group = 3,
    Role = 4,
    Location = 5,
    SPSecurityGroup = 6,
    SPSiteGroup = 7,
    SPUser = 8,
    CostCenter = 9,
    Project = 10,
    Field = 999
}

// export type AkIdentityType = AkIdentityType_User|AkIdentityType_Organization|AkIdentityType_Group|AkIdentityType_Role|AkIdentityType_Location;

export class AkIdentity {
    constructor(identity?: AkIdentity) {
        if (identity) {
            this.ID = identity.ID;
            this.Name = identity.Name;
            this.Type = identity.Type;
            this.Attr = identity.Attr;
        }
    }

    ID: string; //标示
    Name?: string; //显示名
    Type: number;
    Attr?: Object; //其它属性
}

export class AkUser extends AkIdentity {

    constructor(identity?: AkIdentity) {
        super(identity);
        if (identity) {
            this.Login = this.Attr['Login'];
            this.Email = this.Attr['Email'];
            this.Photo = this.Attr['Photo'];
            this.PersonalSite = this.Attr['PersonalSite'];
            this.LineManager = this.Attr['LineManager'];
            this.JobGrade = this.Attr['JobGrade'];
            /*            this.UserNo = this.Attr['UserNo'];*/
            this.Org = this.Attr['Org'];
            this.JobTitle = this.Attr['JobTitle'];
            this.EmployeeNo = this.Attr['EmployeeNo'];
            //Bug 49305: 表单设计器-计算值表达式getUserAttr无法显示创建时间与部门
            this.Created=identity["Created"];
            this.CreatedBy=identity["CreatedBy"];
            this.Modified=identity["Modified"];
            this.ModifiedBy=identity["ModifiedBy"];
        }
    }

    UserNo: string;
    Org: string;
    Login: string;
    Email: string;
    Photo: string;
    PersonalSite: string;
    LineManager: string;
    JobGrade: string;
    JobTitle: string;
    EmployeeNo: string;
    Created: string;
    CreatedBy: string;
    Modified: string;
    ModifiedBy: string;
}

export class AkOrganization extends AkIdentity {

    constructor(identity?: AkIdentity) {
        super(identity);
        if (identity) {
            this.Parent = this.Attr['ParentID'];
            this.Manager = this.Attr['Manager'];
            this.UserCount = Number(this.Attr["UserCount"]);    // Add User Count
        }
    }

    Parent: string;
    Manager: string;
    UserCount: number;
}
export class AkProject extends AkIdentity {
}
export class AkCostCenter extends AkIdentity {

}
export class AkPosition extends AkIdentity {
}

export class AkGroup extends AkIdentity {
    Code:string;
    constructor(identity?: AkIdentity){
        super(identity);
        if(identity){
            this.Code = this.Attr['Code'];
        }
    }
}

export class AkField extends AkIdentity {
    DisplayName?: string;
}

export class AkLocation extends AkIdentity {
}

export class AkSPPrincipal extends AkIdentity {
    SPEntityType: string;
    SPEntityData: any;
}

export interface IdentityRequest {
    ID?: string,
    Type?: number
}

export interface ResolveIdentityRequest extends AkRequest {
    identities: IdentityRequest[]
}

export interface SearchIdentitiesRequest extends AkRequest {
    ParentOrgID?: string,
    IsChild?: boolean,
    keyword: string,
    types: number[],
    rowCount: number
}
export interface SearchCostCenterRequest extends AkRequest {
    keyword?: string,
    orgId?: string,
    pageIndex?: number,
    pageSize?: number
}

export interface SearchUserRequest extends AkRequest {
    keyword?: string,
    orgId?: string,
    pageIndex?: number,
    pageSize?: number
}

export interface IdentitiesResponse extends AkResponse {
    Data?: AkIdentity[];
}

export interface IdentitiesGroupResponse extends AkResponse {
    Data?: AkGroup[];
}

export interface GetOrganizationsResponse extends AkResponse {
    Data?: AkIdentity[];
}

export interface SearchProjectColumnRequest extends AkRequest {
    ProjectListID?: string;
    ProjectAppID?: string;
}

export interface SearchProjectRequest extends AkRequest {
    AppID?: number,
    FilterValue?: string,
    IsExport?: boolean,
    ListID?: string,
    pageIndex: number,
    pageSize: number,
    columns: Array<string>,
    Wheres?: any[]
}

export class IdentityAPI {
    /**
     * 将SharePoint返回的principal结果转化为AkSPPrincipal对象
     * @param source
     * @returns AkSPPrincipal[]
     */
    static parseSPPrinciplal(source: any | any[]): AkSPPrincipal[] {
        function parseSingle(s: any): AkSPPrincipal {
            let basic = {
                ID: s.Key,
                Name: s.DisplayText,
                Type: AkIdentityType.Unknown,
                SPEntityType: s.EntityType,
                SPEntityData: s.EntityData
            }
            switch (s.EntityType) {
                case "":
                    basic = {...basic, ...{ID: s.EntityData.SPGroupID, Type: AkIdentityType.SPSiteGroup}}
                    break;
                case "User":
                    basic = {...basic, ...{Type: AkIdentityType.SPUser}}
                    break;
                case "FormsRole":
                    basic = {...basic, ...{Type: AkIdentityType.SPSecurityGroup}}
                    break;
            }
            return basic;
        }

        if (AkUtil.isArray(source)) {
            return source.map(d => parseSingle(d));
        } else {
            return [parseSingle(source)];
        }
    }

    static parseProject(source: any) {
        source.Attr = {
            Code: source.Code,
            OrgID: source.ProjectDepartment,
            OrgName: "",
            Remark: source.Description,
            Managers: []
        };
        if (source.Manager)
            source.Attr.Managers.push({
                CostCenterID: source.CostCenter,
                ManagerID: source.Manager,
                ManagerName: ""
            });
        if (source.ManagerL2)
            source.Attr.Managers.push({
                CostCenterID: source.CostCenter,
                ManagerID: source.ManagerL2,
                ManagerName: ""
            });
        if (source.ManagerL3)
            source.Attr.Managers.push({
                CostCenterID: source.CostCenter,
                ManagerID: source.ManagerL3,
                ManagerName: ""
            });
        return source;
    }

    // /**
    //  * 查询SharePoint的Principal信息
    //  * @param keyword 搜索关键字，不可为空
    //  * @param count 返回记录数
    //  * @param type principal类型
    //  * @param source principal来源
    //  * @returns {Promise<T>}
    //  */
    // getSPPrincipal(keyword, count?: number, type?: PrincipalType, source?: PrincipalSource) {
    //     return new Promise((resolve, reject) => {
    //         var context = new SP.ClientContext();
    //         var query = new SP.UI.ApplicationPages.ClientPeoplePickerQueryParameters();
    //         query.set_allowMultipleEntities(false);
    //         query.set_maximumEntitySuggestions(count ? count : 50);
    //         query.set_principalType(type ? type : PrincipalType.all);
    //         query.set_principalSource(source ? source : PrincipalSource.all);
    //         query.set_queryString(keyword);
    //         var searchResult = SP.UI.ApplicationPages.ClientPeoplePickerWebServiceInterface.clientPeoplePickerSearchUser(context, query);
    //         context.executeQueryAsync(function () {
    //             var results = context.parseObjectFromJsonString(searchResult.get_value());
    //             resolve(IdentityAPI.parseSPPrinciplal(results));
    //         }, function (sender, args) {
    //             reject(args.get_message());
    //             // Akmii.UI.executeQueryError(sender, args);
    //             //alert('Error message:' + args.get_message() + 'Error code:' + args.get_errorCode() + 'Error details:' + args.get_errorDetails() + 'Error stackTrace:' + args.get_stackTrace());
    //         });
    //     });
    //
    // }

    /**
     * 根据Identity的key解析对象
     * todo: 需要同时传入Identiyt的Type
     * @param vals
     * @returns {Promise<IdentitiesResponse>}
     */
    @simpleApiRequestCacheMiddleware
    static async resolveIdentities(data: ResolveIdentityRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/identities/resolve";
        // let url: string = "https://uatyeeofficesettings.yeeoffice.com/_api/ver(3.0)" + "/api/identities/resolve";
        // return new Promise((resolve, reject)=>{
        //     new Request < ResolveIdentityRequest,
        //         IdentitiesResponse >().post(url, data).then(d=>{
        //         resolve(d);
        //     }, err=>{
        //         if (!err) {
        //             reject({Data:[]});
        //         } else {
        //             reject(err);
        //         }
        //     });
        // });

        return new Request<ResolveIdentityRequest, IdentitiesResponse>().post(url, data, []);
    }

    /**
     * 根据Keyword查询Identity
     * @param data
     * @returns {Promise<IdentitiesResponse>}
     */
    static searchIdentities(data: SearchIdentitiesRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/identities/search";
        return new Request<SearchIdentitiesRequest, IdentitiesResponse>().post(url, data, []);
    }

    /**
     * 根据条件搜索用户
     * @param data
     * @returns {Promise<IdentitiesResponse>}
     */
    static searchUser(data: SearchUserRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/identities/user/search";
        return new Request<SearchUserRequest, IdentitiesResponse>().get(url, data, []);
    }


    /**
     * 根据条件搜索用户
     * @param data
     * @returns {Promise<IdentitiesResponse>}
     */
    static searchCostCenter(data: SearchCostCenterRequest) {
        //window[`${AppKeys.YeeOfficeSettings}_APIUrl`]
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/identities/costcenter/search";
        //let url: string = "https://uatyeeofficesettings.yeeoffice.com/_api/ver(3.0)" + "/api/identities/costcenter/search";
        return new Request<SearchCostCenterRequest, IdentitiesResponse>().get(url, data, []);
    }

    /*项目列表字段请求*/
    static async getProjectColumn(data: SearchProjectColumnRequest) {
        //`${window[AppKeys.YeeOfficeSettings + '_APIUrl']}/api/crafts/defs/listid?ListID=${data.ProjectListID}&AppID=${data.ProjectAppID}`
        const url = `${AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings)}/api/crafts/defs/listid?ListID=${data.ProjectListID}&AppID=${data.ProjectAppID}`;
        return new Request<AkRequest, AkResponse>().get(url, null);
    }

    static searchProject(data: SearchProjectRequest) {
        //`${window[AppKeys.YeeOfficeSettings + '_APIUrl']}/api/crafts/datas/listid`
        const url = `${AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings)}/api/crafts/datas/listid`;
        return new Request<SearchProjectRequest, AkResponse>().post(url, data);
    }

    /**
     * 将identity的数组，转换成字典对象
     * @param identities
     * @returns {{}}
     */
    static identityArray2Dict(identities: AkIdentity[]): Object {
        return AkUtil.arrayToObject(identities, 'ID');
    }

    /**
     * 将identity字典对象，转成数组
     * @param dict
     * @returns {AkIdentity[]}
     */
    static identityDict2Array(dict: Object): AkIdentity[] {
        return Object.keys(dict).map(k => {
            return dict[k];
        });
    }

    /**
     * 获取所有的组织
     * @returns {Promise<IdentitiesResponse>}
     */
    static getOrganizations() {
        // let mock: IdentitiesResponse = {
        //     Status: "0",
        //     Message: "",
        //     Data: [
        //         {ID: "1", Type: "ORGANIZATION", Name: "部门根结点blablabla", Attr: {Parent: "", Manager: ""}},
        //         {ID: "2", Type: "ORGANIZATION", Name: "子结点1blablabla", Attr: {Parent: "1", Manager: ""}},
        //         {ID: "3", Type: "ORGANIZATION", Name: "子节点2blablabla", Attr: {Parent: "1", Manager: ""}},
        //         {ID: "4", Type: "ORGANIZATION", Name: "子节点11blablabla", Attr: {Parent: "2", Manager: ""}},
        //         {ID: "5", Type: "ORGANIZATION", Name: "子节点111blablabla", Attr: {Parent: "4", Manager: ""}},
        //         {ID: "6", Type: "ORGANIZATION", Name: "子节点112blablabla", Attr: {Parent: "4", Manager: ""}},
        //         {ID: "7", Type: "ORGANIZATION", Name: "子节点113blablabla", Attr: {Parent: "4", Manager: ""}},
        //         {ID: "8", Type: "ORGANIZATION", Name: "子节点114blablabla", Attr: {Parent: "4", Manager: ""}},
        //         {ID: "9", Type: "ORGANIZATION", Name: "子节点115blablabla", Attr: {Parent: "4", Manager: ""}},
        //         {ID: "10", Type: "ORGANIZATION", Name: "子节点162blablabla", Attr: {Parent: "4", Manager: ""}},
        //         {ID: "11", Type: "ORGANIZATION", Name: "子节点163blablabla", Attr: {Parent: "4", Manager: ""}},
        //         {ID: "12", Type: "ORGANIZATION", Name: "子节点164blablabla", Attr: {Parent: "4", Manager: ""}},
        //         {ID: "13", Type: "ORGANIZATION", Name: "子节点165blablabla", Attr: {Parent: "4", Manager: ""}},
        //         {ID: "14", Type: "ORGANIZATION", Name: "子节点166blablabla", Attr: {Parent: "4", Manager: ""}},
        //         {ID: "15", Type: "ORGANIZATION", Name: "子节点167blablabla", Attr: {Parent: "4", Manager: ""}},
        //         {ID: "16", Type: "ORGANIZATION", Name: "子节点168blablabla", Attr: {Parent: "4", Manager: ""}},
        //
        //     ]
        // }
        // return Promise.resolve<IdentitiesResponse>(mock);
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/identities/organizations";
        //let url: string = "https://uatyeeofficesettings.yeeoffice.com/_api/ver(3.0)" + "/api/identities/organizations";
        return new Request<null, IdentitiesResponse>().get(url, undefined, []);
    }

    /**
     * 获取所有地区信息
     * @returns {Promise<IdentitiesResponse>}
     */
    static getLocations() {
        // let mock: IdentitiesResponse = {
        //     Status: "0",
        //     Message: "",
        //     Data: [
        //         {ID: "1", Type: "LOCATION", Name: "北京"},
        //         {ID: "2", Type: "LOCATION", Name: "上海"},
        //         {ID: "3", Type: "LOCATION", Name: "西安"},
        //     ]
        // }
        //
        // return Promise.resolve<IdentitiesResponse>(mock);
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/identities/locations";
        return new Request<null, IdentitiesResponse>().get(url, undefined, []);
    }

    /**
     * 获取所有岗位信息
     * @returns {Promise<IdentitiesResponse>}
     */
    static getJobPositions() {
        // let mock: IdentitiesResponse = {
        //     Status: "0",
        //     Message: "",
        //     Data: [
        //         {ID: "1", Type: "ROLE", Name: "角色1"},
        //         {ID: "2", Type: "ROLE", Name: "角色2"},
        //         {ID: "3", Type: "ROLE", Name: "角色3"},
        //         {ID: "4", Type: "ROLE", Name: "角色4"},
        //         {ID: "5", Type: "ROLE", Name: "角色5"},
        //     ]
        // }
        //
        // return Promise.resolve<IdentitiesResponse>(mock);
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.Flowcraft) + "/api/identities/positions";
        return new Request<null, IdentitiesResponse>().get(url, undefined, []);
    }

    /**
     * 获取所有组信息
     * @returns {Promise<IdentitiesResponse>}
     */
    static getGroups(data?:any) {
        // let mock: IdentitiesResponse = {
        //     Status: "0",
        //     Message: "",
        //     Data: [
        //         {ID: "1", Type: "GROUP", Name: "组1"},
        //         {ID: "2", Type: "GROUP", Name: "组2"},
        //         {ID: "3", Type: "GROUP", Name: "组3"},
        //         {ID: "5", Type: "GROUP", Name: "组5"},
        //     ]
        // }
        //
        // return Promise.resolve<IdentitiesResponse>(mock);
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/identities/usergroups";
        return new Request<object, IdentitiesGroupResponse>().get(url, data, []);
    }

    static getAppAdmin(appID?) {
        appID = appID ? appID : AkContext.getAppInfoID();
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + `/appinforole/hasmodule/appinfoid?AppInfoID=${appID}`;
        return new Request<AkRequest, AkResponse>().get(url);
    }
}
