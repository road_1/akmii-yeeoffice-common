

import { SocketTokenModel, ChatMessageAddRequest, ChatAddRequest, SocketResponse, InstructionEnum, SocketRequest, ChatPullRequest, ChatPullModel, ChatMessagePullRequest, AccountChatUpdateLastReadRequest, ChatMessagePullModel, AccountChatUpdateFavoriteRequest, AccountBindDeviceIDRequest, ChatMemberBaseRequest, ChatMembersRequest, ChatMemberUpdateAliasRequest } from "./socketmodel";
import { AkGlobal, NotificationAction } from "../../index";
import { AkUtil } from '../../util/util';
export class SocketClient {

    /**对象实例 */
    private static instance: SocketClient;
    private static tokenModel: SocketTokenModel;
    private static deviceID: string;
    public static getInstance(): SocketClient {
        if (!this.instance) {
            this.instance = new SocketClient();
        }
        return this.instance;
    }
    /**当前连接 */
    private connection: WebSocket;
    /**实例化，使用类之前必须初始化 */
    init(item: SocketTokenModel, deviceID?: string) {
        SocketClient.tokenModel = item;
        SocketClient.deviceID = deviceID;
        if (SocketClient.instance
            && SocketClient.instance.connection
            && SocketClient.instance.connection.readyState !== SocketClient.instance.connection.CLOSED
        ) {
            SocketClient.instance.connection.close();
        }
        SocketClient.instance.connection = new WebSocket(`${item.WsUrl}?token=${(item.AccessToken)}&deviceID=${deviceID}&type=${3}`);
        SocketClient.instance.connection.onopen = this.onSocketOpen;
        SocketClient.instance.connection.onmessage = this.onSocketMessage;
        SocketClient.instance.connection.onerror = this.onSocketError;
        SocketClient.instance.connection.onclose = this.onSocketClose;
    }

    private onSocketOpen() {
        console.log("已连接");
        setTimeout(() => {
            SocketClient.getInstance().sendHeartBeat();
        }, 20000);
    }
    /**接收到消息的处理 */
    private onSocketMessage(message) {
        //心跳包
        if (message.data === "h&b") {
            setTimeout(() => {
                SocketClient.getInstance().sendHeartBeat();
            }, 20000);
            return;
        }
        var response = JSON.parse(message.data) as SocketResponse;
        //手机端如果是402的话，需要踢出登录
        if (response.Status === 402) {
            // AkGlobal.store.dispatch(SocketAction.requestPlaySignin());
            return;
        }
        switch (response.Op) {
            case InstructionEnum.SESSION_CONNECT://连接上服务器，去拉取常用会话 
                console.log('success');
                AkGlobal.store.dispatch(NotificationAction.requestChatPullAction());
                break;
            case InstructionEnum.CHAT_PULL_S://拉取到会话(常用聊天)
                let chatList = response.Data as ChatPullModel[];
                AkGlobal.store.dispatch(NotificationAction.requestChatList(chatList));
                break;
            case InstructionEnum.CHAT_CREATE_S://收到创建会话
                let chat = response.Data as ChatPullModel;
                AkGlobal.store.dispatch(NotificationAction.requestChat(chat));
                break;
            case InstructionEnum.CHAT_MSG_SEND_S://收到发送的消息
                let messageResponse = JSON.parse(response.Data);
                AkGlobal.store.dispatch(NotificationAction.requestChatMessage(messageResponse));
                break;
            case InstructionEnum.CHAT_MSG_PULL_S://消息拉取
                var requestmessage = ((response.Data || {}) as ChatMessagePullModel);
                requestmessage.UniqueID = AkUtil.guid();
                AkGlobal.store.dispatch(NotificationAction.requestChatMessageList(requestmessage));
                break;
            case InstructionEnum.ACCOUNT_CHAT_UPDATE_LAST_READ_S://更新消息已读
                var readMessage = JSON.parse(response.Data);
                AkGlobal.store.dispatch(NotificationAction.updataMessage(readMessage))
                break;
            case InstructionEnum.CHAT_MEMBER_PULL_S://成员拉取
                AkGlobal.store.dispatch(NotificationAction.receiveChatPerson(response.Data));
                break;
            case InstructionEnum.CHAT_MEMBER_EXIT_S://退出群组
                AkGlobal.store.dispatch(NotificationAction.receiveRemoveGroupPerson(response.Data));
                break;
            case InstructionEnum.CHAT_UPDATE_NAME_S://更改群名称
                AkGlobal.store.dispatch(NotificationAction.requestUpdateGroupInfo(response.Data));
                break;
            case InstructionEnum.PLAY_SIGNIN_S://踢出登录
                // AkGlobal.store.dispatch(SocketAction.requestPlaySignin());
                break;
            case InstructionEnum.CHAT_UPDATE_PHOTO_S://更新群组头像
                AkGlobal.store.dispatch(NotificationAction.requestUpdateGroupInfo(response.Data));
                break;
            case InstructionEnum.CHAT_MEMBER_REMOVE_S://移除成员
                AkGlobal.store.dispatch(NotificationAction.receiveRemoveGroupPerson(response.Data));
                break;
            case InstructionEnum.CHAT_MEMBER_ADD_S://添加成员
                AkGlobal.store.dispatch(NotificationAction.receiveAddGroupPerson(response.Data));
                break;
            case InstructionEnum.ACCOUNT_CHAT_UPDATE_FAVORITE_S://更新置顶
                AkGlobal.store.dispatch(NotificationAction.receiveIsTop(response.Data));
                break;
            default:
                console.log(response.Data);
                break;
        }
    }

    private onSocketError() {
        console.log("出错了");
    }
    private onSocketClose() {
        console.log("已关闭");
        setTimeout(() => {
            // let account = AkGlobal.store.getState().account;
            // if (account && account.isLogon) {
            //     SocketClient.reStart();
            // }
            SocketClient.reStart();
        }, 10000);
    }
    /**重启 */
    static reStart() {
        if (SocketClient.getInstance().connection
            && SocketClient.getInstance().connection.readyState !== SocketClient.getInstance().connection.OPEN
            && SocketClient.tokenModel) {
            console.log('restart');
            SocketClient.getInstance().init(SocketClient.tokenModel, SocketClient.deviceID);
        }
    }
    /**关闭连接 */
    closeConn() {
        if (this.connection
            && this.connection.readyState === this.connection.OPEN) {
            SocketClient.tokenModel = null;
            this.connection.close();
        }
    }
    /**拉取会话消息 */
    pullChatList(data: ChatPullRequest) {
        if (this.connection && this.connection.readyState === this.connection.OPEN) {
            var request = new SocketRequest<ChatPullRequest>(InstructionEnum.CHAT_PULL_R, data);
            this.connection.send(JSON.stringify(request));
        }
    }

    /**创建对话 */
    createChat(data: ChatAddRequest, uniqueID?: string) {
        if (this.connection && this.connection.readyState === this.connection.OPEN) {
            var request = new SocketRequest<ChatAddRequest>(InstructionEnum.CHAT_CREATE_R, data, uniqueID);
            this.connection.send(JSON.stringify(request));
        }
    }

    /**发送消息 */
    sendMessage(data: ChatMessageAddRequest | string, uniqueID?: string) {
        if (this.connection && this.connection.readyState === this.connection.OPEN) {
            var request = new SocketRequest<ChatMessageAddRequest | string>(InstructionEnum.CHAT_MSG_SEND_R, data, uniqueID);
            this.connection.send(JSON.stringify(request));
        }
    }

    /**发送心跳包 */
    sendHeartBeat() {
        if (this.connection && this.connection.readyState === this.connection.OPEN) {
            this.connection.send("h&b");
        }
    }

    /**拉取聊天信息 */
    pullMessage(data: ChatMessagePullRequest, uniqueID?: string) {
        if (this.connection && this.connection.readyState === this.connection.OPEN) {
            var request = new SocketRequest<ChatMessagePullRequest>(InstructionEnum.CHAT_MSG_PULL_R, data, uniqueID);
            this.connection.send(JSON.stringify(request));
        }
    }
    /**更新消息已读 */
    readMessage(data: AccountChatUpdateLastReadRequest) {
        if (this.connection && this.connection.readyState === this.connection.OPEN) {
            var request = new SocketRequest<AccountChatUpdateLastReadRequest>(InstructionEnum.ACCOUNT_CHAT_UPDATE_LAST_READ_R, data);
            this.connection.send(JSON.stringify(request));
        }
    }
    /**设置置顶 */
    setChatFavorite(data: AccountChatUpdateFavoriteRequest) {
        if (this.connection && this.connection.readyState === this.connection.OPEN) {
            var request = new SocketRequest<AccountChatUpdateFavoriteRequest>(InstructionEnum.ACCOUNT_CHAT_UPDATE_FAVORITE_R, data);
            this.connection.send(JSON.stringify(request));
        }
    }
    /**绑定设备号 */
    bindDevice(data: AccountBindDeviceIDRequest) {
        if (this.connection && this.connection.readyState === this.connection.OPEN) {
            var request = new SocketRequest<AccountBindDeviceIDRequest>(InstructionEnum.ACCOUNT_DEVICE_BIND_R, data);
            this.connection.send(JSON.stringify(request));
        }
    }
    /**解绑设备号 */
    unBindDevice(data: AccountBindDeviceIDRequest) {
        if (this.connection && this.connection.readyState === this.connection.OPEN) {
            var request = new SocketRequest<AccountBindDeviceIDRequest>(InstructionEnum.ACCOUNT_DEVICE_UNBIND_R, data);
            this.connection.send(JSON.stringify(request));
        }
    }
    /**获取聊天成员 */
    getMember(data: ChatMemberBaseRequest) {
        if (this.connection && this.connection.readyState === this.connection.OPEN) {
            var request = new SocketRequest<ChatMemberBaseRequest>(InstructionEnum.CHAT_MEMBER_PULL_R, data);
            this.connection.send(JSON.stringify(request));
        }
    }
    /**添加成员 */
    addMember(data: ChatMembersRequest) {
        if (this.connection && this.connection.readyState === this.connection.OPEN) {
            var request = new SocketRequest<ChatMemberBaseRequest>(InstructionEnum.CHAT_MEMBER_ADD_R, data);
            this.connection.send(JSON.stringify(request));
        }
    }
    /**删除成员 */
    removeMember(data: ChatMembersRequest) {
        if (this.connection && this.connection.readyState === this.connection.OPEN) {
            var request = new SocketRequest<ChatMemberBaseRequest>(InstructionEnum.CHAT_MEMBER_REMOVE_R, data);
            this.connection.send(JSON.stringify(request));
        }
    }
    /**退出群组 */
    quitGroup(data: ChatMemberBaseRequest, uniqueID?: string) {
        if (this.connection && this.connection.readyState === this.connection.OPEN) {
            var request = new SocketRequest<ChatMemberBaseRequest>(InstructionEnum.CHAT_MEMBER_EXIT_R, data, uniqueID);
            this.connection.send(JSON.stringify(request));
        }
    }
    editGroupName(data: ChatMemberUpdateAliasRequest, uniqueID?: string) {
        if (this.connection && this.connection.readyState === this.connection.OPEN) {
            var request = new SocketRequest<ChatMemberUpdateAliasRequest>(InstructionEnum.CHAT_UPDATE_NAME_R, data, uniqueID);
            this.connection.send(JSON.stringify(request));
        }
    }
    sendHeadBeat() {
        if (this.connection
            && this.connection.readyState === this.connection.OPEN
            && SocketClient.tokenModel) {
            this.connection.send("h&b");
        }
    }
}