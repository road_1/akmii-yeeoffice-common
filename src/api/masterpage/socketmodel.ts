import { AkBase, AkUtil, AkBaseModel } from "../../index";



export interface SocketTokenModel extends AkBase {
    AccessToken: string;
    WsUrl: string;
}
export class SocketRequest<T> implements AkBase {
    Op: InstructionEnum;
    Data: T | string;
    UniqueID: string;
    constructor(op: InstructionEnum, data: T, uniqueID?: string) {
        this.Op = op;
        this.Data = JSON.stringify(data);
        this.UniqueID = uniqueID || AkUtil.guid();
    }
}

export interface SocketResponse {
    Op: InstructionEnum;
    Data: string | any;
    Status: number;
    Message: string;
    TotalCount: string;
    IsSuccess: boolean;
    CurrentDate: string;
    UniqueID: string;
}
/** 消息接收到 */
export interface ChatMessageModel extends AkBaseModel {
    /** ChatID */
    ChatID?: string;
    /** MessageID */
    MessageID?: string;
    /** Type */
    Type: MessageTypeEnum;
    /** Body */
    Body?: string;
    /** RevCount */
    RevCount?: number;
    /** ReadCount */
    ReadCount?: number;
    /** Author */
    Author?: string;
    /**消息的唯一标识，标记消息 */
    UniqueID?: string;
    /**状态:
     * 对方:0:未读，1:已读
     * 自己:0:发送中，1:已发送,2:发送失败,
     */
    Status?: number;
    /** Ext1 */
    Ext1?: string;
    /** Ext2 */
    Ext2?: string;
    /** Ext3 */
    Ext3?: string;
    /** 重发次数 */
    RetryTimes?: number;
}
/**创建会话 */
export interface ChatPullModel extends AkBase {
    AccountChat: AccountChatModel;
    Chat: ChatModel;
    ChatID?: string;
    MessagesPull?: ChatMessagePullModel;
    LastMessageTime?: string;
    /**未读消息数量 */
    UnReadCount?: number;
}
export interface AccountChatModel extends AkBaseModel {
    /** ChatID */
    ChatID: string;
    /** AccountID */
    AccountID: string;
    /** Alias */
    Alias: string;
    /** IsFavorite */
    IsFavorite: boolean;
    /** IsIgnore */
    IsIgnore: boolean;
    /** LastReadID */
    LastReadID: string;
    /** Customize */
    Customize: string;
    /** Comment */
    Comment: string;
    /** 状态 0: 删除 1: 正常 */
    Status: number;
    /** Ext1 */
    Ext1: string;
    /** Ext2 */
    Ext2: string;
    /** Ext3 */
    Ext3: string;
}
export interface ChatModel extends AkBaseModel {
    UniqueID: string;
    /** ChatID */
    ChatID: string;
    /** Name */
    Name: string;
    /** Type */
    Type: ChatTypeEnum;
    /** MemberCount */
    MemberCount: number;
    /** Owner */
    Owner: string;
    /** LastMessage */
    LastMessage: string;
    /** Announcement */
    Announcement: string;

    /** QRCode */
    QRCode: string;
    /** Photo */
    Photo: string;
    /** Ext1 */
    Ext1: string;
    /** Ext2 */
    Ext2: string;
    /** Ext3 */
    Ext3: string;
}

export interface GroupUserModel extends AkBaseModel {
    /** ChatID */
    ChatID: string;
    /** Seq */
    Seq: number;
    /** AccountID */
    AccountID: string;
    /** Alias */
    Alias: string;
    /** Ext1 */
    Ext1: string;
    /** Ext2 */
    Ext2: string;
    /** Ext3 */
    Ext3: string;
}
export interface BaseYeeChat {

}
export interface ChatPullRequest extends BaseYeeChat {
    LastTime?: string;
    ChatID?: string;
}
/** 添加聊天/群组 */
export interface ChatAddRequest extends BaseYeeChat {
    /** 组名称 */
    Name?: string;
    Ext1?: string;
    ChatType: ChatTypeEnum;
    MemberIDs: string[];
}
/** 添加会话 */
export interface ChatMessageAddRequest extends BaseYeeChat {
    /** ChatID */
    ChatID: string;
    /** Type */
    Type: MessageTypeEnum;
    /** Body */
    Body: string;
}
export interface ChatMessagePullRequest extends BaseYeeChat {
    LastMsgID: string;
    ChatID: string;
    /**0/1 获取消息方向，正常是加载最新，向上获取历史消息时传1，此时lastmsgid是本地最后一条消息 */
    Direction: number;
    Self?:boolean;//是否拉取最新的一条消息
}
/**消息标记已读请求 */
export interface AccountChatUpdateLastReadRequest extends BaseYeeChat {
    ChatID: string;
    LastMessageID: string;
}
export interface AccountChatUpdateFavoriteRequest extends BaseYeeChat {
    ChatID: string;
    Favorite: boolean;
}
export interface AccountBindDeviceIDRequest extends BaseYeeChat {
    DeviceID: string;
    DeviceType: DeviceTypeEnum
}
export interface ChatMemberBaseRequest extends BaseYeeChat {
    ChatID: string;
}
export interface ChatMembersRequest extends ChatMemberBaseRequest {
    MemberIDs: string[]
    Ext1?: string;
}
export interface ChatMemberUpdateAliasRequest extends ChatMemberBaseRequest {
    Name: string;
}

export interface ChatMessagePullModel {
    ChatID?: string;
    /**是否还有更多消息 */
    More?: boolean;
    Direction?: MessagePullDirectionEnum;
    UniqueID?: string;
    Messages: ChatMessageModel[];
}
export interface ChatUpdateAnnouncementRequest extends BaseYeeChat {
    ChatID?: string;
    Announcement?: string;
}
export interface ChatUpdateNameRequest extends BaseYeeChat {
    ChatID?: string;
    Name?: string;
}

export const enum ChatTypeEnum {
    /** 私聊 */
    OneToOneChat = 1,
    /** 群组聊天 */
    GroupChat = 2,
    /**服务通知 */
    NoticeChat = 3
}
/**群组用户类型 */
export const enum GroupUserSelectTypeEnum {
    /**添加 */
    Add = 0,
    /**删除 */
    Remove = 1,
    /** 清空 */
    Clear = 2
}

export const enum MessagePullDirectionEnum {
    /**向下拉取 */
    Down = 0,
    /**向上拉取历史消息 */
    Up = 1,
}
/** 
 * R:客户端的request
 * S:服务器端的send
 */
export const enum InstructionEnum {
    /**不认识的指令*/
    DEFAULT = 0,
    /**创建会话*/
    CHAT_CREATE_R = 100,
    /**创建会话*/
    CHAT_CREATE_S = 1100,
    /**更新会话*/
    CHAT_UPDATE_R = 101,
    /**更新会话*/
    CHAT_UPDATE_S = 1101,
    /**拉取会话*/
    CHAT_PULL_R = 102,
    /**拉取会话*/
    CHAT_PULL_S = 1102,
    /**更新群组名称*/
    CHAT_UPDATE_NAME_R = 105,
    /**更新群组名称*/
    CHAT_UPDATE_NAME_S = 1105,
    /**更新群组头像*/
    CHAT_UPDATE_PHOTO_R = 106,
    /**更新群组头像*/
    CHAT_UPDATE_PHOTO_S = 1106,
    /**移除成员*/
    CHAT_MEMBER_REMOVE_R = 200,
    /**移除成员*/
    CHAT_MEMBER_REMOVE_S = 1200,
    /**添加成员*/
    CHAT_MEMBER_ADD_R = 201,
    /**添加成员*/
    CHAT_MEMBER_ADD_S = 1201,
    /**退出群组*/
    CHAT_MEMBER_EXIT_R = 202,
    /**退出群组*/
    CHAT_MEMBER_EXIT_S = 1202,
    /**成员拉取*/
    CHAT_MEMBER_PULL_R = 203,
    /**成员拉取*/
    CHAT_MEMBER_PULL_S = 1203,
    /**更新组的个人昵称*/
    CHAT_MEMBER_UPDATE_ALIAS_R = 204,
    /**更新组的个人昵称*/
    CHAT_MEMBER_UPDATE_ALIAS_S = 1204,
    /**消息拉取*/
    CHAT_MSG_PULL_R = 300,
    /**消息拉取*/
    CHAT_MSG_PULL_S = 1300,
    /**消息发送*/
    CHAT_MSG_SEND_R = 301,
    /**消息发送*/
    CHAT_MSG_SEND_S = 1301,
    /**消息阅读*/
    CHAT_MSG_READ_R = 302,
    /**消息阅读*/
    CHAT_MSG_READ_S = 1302,
    /**会话置顶更新*/
    ACCOUNT_CHAT_UPDATE_FAVORITE_R = 400,
    /**会话置顶更新*/
    ACCOUNT_CHAT_UPDATE_FAVORITE_S = 1400,
    /**会话更新最新读取ID*/
    ACCOUNT_CHAT_UPDATE_LAST_READ_R = 401,
    /**会话更新最新读取ID*/
    ACCOUNT_CHAT_UPDATE_LAST_READ_S = 1401,

    /** 绑定设备号 */
    ACCOUNT_DEVICE_BIND_R = 501,
    /** 绑定设备号 */
    ACCOUNT_DEVICE_BIND_S = 1501,
    /** 解绑设备号 */
    ACCOUNT_DEVICE_UNBIND_R = 502,
    /** 解绑设备号 */
    ACCOUNT_DEVICE_UNBIND_S = 1502,
    /**踢出登录 */
    PLAY_SIGNIN_S = 900,
    /**关闭连接*/
    SESSION_CLOSE = 999,
    /**建立连接*/
    SESSION_CONNECT = 1000,
}
export const enum MessageTypeEnum {
    /** Text消息*/
    Text = 0,
    /** 图片*/
    Image = 1,
    /** 文件*/
    File = 2,
    /** 音频*/
    Audio = 3,
    /** 视频*/
    Video = 4,
    /** 模板消息*/
    Template = 9,
    /** 群名称修改*/
    ChatRename = 100,
    /** 添加群成员*/
    ChatAddMember = 101,
    /** 移除群成员*/
    ChatRemoveMember = 102,
    /** 群公告更新*/
    AnnouncementEdit = 103,
    /** 退出群*/
    ChatExit = 104,
    /**系统消息(时间) */
    Notification = 999,
}

export const enum DeviceTypeEnum {
    ALL = 0,
    /**Android */
    ANDROID = 1,
    /**IOS */
    IOS = 2,
    PC = 3,
    Server = 4,
}

export class TemplateMsgModel {
    Title?: string;
    Created?: string;
    IconUrl?: string;
    Data?: string;
    TemplateCode?: string;
    Btns?: IBtn[];
}
export interface IBtn {
    Type?: string;
    Code?: string;
    Name?: string;
    Url?: string;
}
