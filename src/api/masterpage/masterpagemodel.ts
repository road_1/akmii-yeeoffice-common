import { AkResponse, AkRequest, AkBase } from '../../util/';
import { AkIdentity } from "../index";

export interface SPGetUserRequest extends AkRequest {
}
export interface SPUserInfo {
    LoginName: string;
}
export interface SPGetUserResponse extends AkResponse {
    d: SPUserInfo;
}
export interface MerchatLoginRequest extends AkRequest {
    MerchatToken: string;
    LoginName: string;
}
export class UserModel {
    ID: number;
    Name_CN: string;
    Name_EN: string;
    SPAccount: string;
    SPAccount_Short: string;
    Phone: string;
    DepartmentID: string;
    LineManager: number;
    LocationID: number;
    LocationName?: any;
    Photo: string;
    JobTitle: string;
    Telephone: string;
    OfficeAddress: string;
    Email: string;
    EmployeeNo: string;
    Mobile: string;
    Remark: string;
    Deleted: boolean;
    AccountID: string;
    PassWord: string;
    IsAdmin: boolean;
    TenantID: string;
    ReadDBConnection?: any;
}
export class CompanyInfo {
    ID: number;
    CompanyName: string;
    Addresses: string;
    Province: string;
    City: string;
    TenantID: string;
    ZipCode: string;
    PhoneNumber: string;
    TechnicalContact: string;
    CountryCode: string;
    CountryName: string;
    LanguageCode: string;
    LanguageName: string;
    MainAdmin: string;
    SecondAdmin: string;
    Logo: string;
    NavBarColour: string;
    NavFontColour: string;
    ClientInfoList?: ClientInfo[];
    Attr?: any;
}
export class ClientInfo {
    ClientID: string;
    ClientType: number;
    Status: number;
}
export class AccountType {
    AccountKey: string;
    AccountType: number;
}
export interface MerchatLoginModel {
    LiveSeconds: number;
    UserModel: UserModel;
    CompanyInfo: CompanyInfo;
    Secret: string;
    EnterpriseInfo?: EnterpriseInfo;
    AccountType?: number;
}
export interface EnterpriseInfo {
    Created: string;
    ExpireDate: string;
    Flag: number;
    RemainingDay: number;
    Level: number;
    Status: number;
    InviteCode: string;
    SubscribeDay: number;
    UserMaxCount: number;
}
export const enum EnterpriseFlag {
    /**暂无 */
    Nomal = 0,
    /**试用期 */
    Free = 2,
    /** 试用期结束 */
    FreeComplete = -2,
    /**
     *  已购买订阅
     */
    Subscribe = 3,
    /**
    *  购买订阅过期
    */
    SubscriptionExpired = -3
}
export interface MerchatLoginResponse extends AkResponse {
    Data: MerchatLoginModel;
    DataCount: number;
    TotalPages: number;
    CurrentPageIndex: number;
    CurrentPageSize: number;
    BeginIndex: number;
    EndIndex: number;
    ErrorMSG: string;
    TimeStamp: string;
    TipMSG: string;
    OperationState: boolean;
    State: number;
}
export interface MerchatNavigationRequest extends AkRequest {
    CurrentIndex?: number;
    PageSize?: number;
}
export interface NavigationModel {
    YunGalaxyNavigationID: number;
    YunGalaxyNavigationName_CN: string;
    YunGalaxyNavigationName_EN: string;
    YunGalaxyNavigationSummary: string;
    YunGalaxyNavigationImage: string;
    YunGalaxyNavigationUrl: string;
    YunGalaxyNavigationModule: number;
    YunGalaxyNavigationType: number;
    Created: Date;
    Modified: Date;
    YunGalaxyNavigationParentID: number;
    YunGalaxyNavigationSortID: number;
    Extends1?: any;
    Extends2?: any;
    Extends3?: any;
    AppID: number;
}

export interface MerchatNavigationResponse extends AkResponse {
    Data: NavigationModel[];
    DataCount: number;
    TotalPages: number;
    CurrentPageIndex: number;
    CurrentPageSize: number;
    BeginIndex: number;
    EndIndex: number;
    ErrorMSG: string;
    TimeStamp: string;
    TipMSG: string;
    OperationState: boolean;
    State: number;
}

export interface PortalModel {
    Admins: AkIdentity[];
    PortalID?: string;
    ApplicationID?: string;
    Title?: string;
    Description?: string;
    WebRelativeUrl?: string;
    ParentID?: string;
    SupperManagerID?: string;
    Configs?: string;
    DraftConfigs?: string;
    Status?: string;
    Ext1?: string;
    Ext2?: string;
    Ext3?: string;
    Image?: string;
}

export interface MerchatPortalResponse extends AkResponse {
    Data: PortalModel[];
}

export interface APIMapping extends AppInfoList {
    // MerchantAPIUrl: string;
    // AppInfoID: number;
}

export interface MyAppInfo extends AppInfoList {
    // AppInfoID: number;
    // AppName: string;
    // AppVersion: string;
    // AppKey: string;
    // AppInfoOrder: number;
    // DefaultSubSiteName: string;
    // DefaultPageUrl: string;
}

export interface MyAppInstallInfo extends AppInfoList {
    // MerchantKey: number;
    // AppInfoID: number;
    // InstallUrl: string;
}

export interface MerchatAPIModel {
    MerchantName: string;
    AppInfoList: AppInfoList[];
}
export interface MerchatModelResponse extends AkBase {
    Data: MerchatAPIModel;
}


export class AppInfoList {
    MerchantAPIUrl?: string;
    AppInfoID?: number;
    AppInfoOrder?: number;
    AppName?: string;
    AppVersion?: string;
    AppKey?: string;
    DefaultSubSiteName?: string;
    DefaultPageUrl?: string;
    InstallUrl?: string;
    MerchantKey?: string;
    /**
     *  1 是开启  2 是禁用
     */
    Status?: number;
    /**
     * 购买版本信息
     */
    AppExt?: AppExt;
}
/**
*  购买版本信息
**/
export class AppExt {
    Level?: MerchantLevelEnum
}

export enum MerchantLevelEnum {
    /**
    *  终极版
    **/
    Ultimate = 999,
    /**
    *  免费版
    **/
    Free = 0,

    /**yeeflow(暂时) */
    YeeFlow = 20,

    /**
    *  标准版
    **/
    Standard = 10,
    /**
    *  企业版
    **/
    Enterprise = 100,
}

export class SupportModel {
    Mail: string;
    Telephone: string;
    WorkHours: string;
    InviteCode: string;
}

export interface UpdatePasswordRequest {
    OldPwd: string;
    NewPwd: string;
}

export enum LoginType {
    YeeOffice = 1,
    Office365 = 2,
    Wechat = 3,
    DingTalk = 4,
    WechatQY = 5,
    Office365_21V = 6,
    NETEASE = 7,
    SharepointOnline = 999,
    none = 0
}

export interface MenuData {
    /**
     * 文本ID
     */
    Key?: string;
    /**
     * 文本默认值
     */
    Title?: string;
    /**
     * 路径
     */
    Path?: string;
    /** 是否展示在导航中 */
    NotShow?: boolean;
    /**
     * 图标
     */
    Icon?: string;
    /**
     * 角标数量
     */
    Badge?: number;
    /**自定义点击事件 */
    OnClick?: () => void;
    /**
     * 子节点
     */
    Children?: MenuData[];
    /**
 * 是否分割线
 *
 * @type {boolean}
 * @memberOf MenuData
 */
    IsDivider?: boolean;

    /**
     * 是否是标题
     * */
    IsTitle?: boolean;

}
