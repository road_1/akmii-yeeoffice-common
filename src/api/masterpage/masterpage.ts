import { AkItemResponse } from '../../util/common';
import { AppKeys, AkContext } from './../../util/common';
import { Request } from "../../util/";
import {
    MerchatLoginResponse,
    MerchatLoginRequest,
    MerchatNavigationRequest,
    MerchatNavigationResponse,
    MerchatModelResponse,
    MerchatPortalResponse,
    AppInfoList,
    UpdatePasswordRequest,
} from "./masterpagemodel";
import { AkUtil, AkRequest, AkResponse } from "../../index";
import { MerchatAPIModel, } from "../index";
import { SocketTokenModel } from "./socketmodel";
import { AkNotification } from '../../components/controls/ak-notification';
import { AkGlobal } from '../../util/common';
import { CommonLocale } from '../../locales/localeid';
import { CacheHelper } from '../../util/cache';

export class MerchatAPI {
    /**
     * 商户Token
     */
    static LoginToken: string;
    /**
     * 登录名
     */
    static LoginName: string;
    /**
    * 用于强制刷新缓存
    */
    static ForcedRefreshCache: Boolean;
    /**
     * 获取对应的商户Token
     */
    static getMerchatToken() {
        var toekn = null;
        var request = new Request<any, any>();
        request.defaultParam.Prefix = "";
        request.defaultParam.RequireToken = false;
        request.sendXMLHttpRequest("get",
            AkContext.getRootWebUrl() + "/style%20library/loginhelper.js?"
            + AkUtil.guid(),
            null,
            false,
            (data) => {
                const regex = /=([\s\S]*?);/gm;
                let m;
                let variables: string[] = [];
                while ((m = regex.exec(data)) !== null) {
                    if (m.index === regex.lastIndex) {
                        regex.lastIndex++;
                    }
                    variables.push(m[1].replace(/'|"/g, '').trim().replace("/ver(2.0)/", "/ver(3.0)/"));
                }
                MerchatAPI.LoginToken = variables[0];
                // window["LoginUrl"] = variables[1];
                window["YeeOffice_JSVersion"] = variables[2];
                CacheHelper.setLocalStorage(AkContext.Keys.LoginHerperKey, variables);
                toekn = MerchatAPI.LoginToken;
            },
            (data) => {
            }, false);

        return toekn;
    }
    /** 获取SocketToken */
    static getSocketToken() {
        const url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/token";
        var request = new Request<null, AkItemResponse<SocketTokenModel>>()
        request.defaultParam.IgnoreError = true;
        return request.get(url, null)
    }
    /**
     * 获取SP用户名
     */
    static getUserName() {
        AkContext.setCurrentBranch(AkContext.SPOnline);
        if (MerchatAPI.LoginName) {
            return MerchatAPI.LoginName;
        }
        var request = new Request<any, any>();
        let url = AkContext.getRootWebUrl() + "/_api/Web/GetUserById(" + global["_spPageContextInfo"].userId + ")";
        request.sendXMLHttpRequest("get",
            url,
            null,
            false,
            (data) => {
                MerchatAPI.LoginName = data.d.LoginName;
            },
            (data) => {
            }, true, [{ key: "Accept", value: "application/json;odata=verbose" }]);
        return MerchatAPI.LoginName;
    }
    /**
     * 默认登陆登录  YeeOfficeBranch.SPOnline
     */
    static merchatLogin(callback?: (logainfail: boolean) => void) {
        if (AkContext.getBranch() === AkContext.YeeOffice) {
            return MerchatAPI.LoginByYeeOffice();
        } else {
            return MerchatAPI.LoginBySPOnline(callback);
        }
    }
    /**
     *   
     *  YeeOfficeNet版本登陆
     */
    static LoginByYeeOffice() {


        const loginData = CacheHelper.getLocalStorage(AkContext.Keys.TokenDataKey);
        //const loginhelper = MerchatAPI.getLoginHelperLocalStorage();
        if (CacheHelper.isAuthCookiesExpiration() &&
            loginData
            && AkContext.getUser() &&
            AkContext.getUser().AccountID
            === window["AKUserID"]) {
            window["YeeOffice_JSVersion"] = AkContext.YeeOffice;
            return loginData;
        } else {
            window.location.href = AkContext.getSignOutURI();
        }

    }
    /**
    *  老版本登陆
    */
    static LoginBySPOnline(callback: (logainfail: boolean) => void) {
        var merchatLoginResponse = null;
        CacheHelper.isRefreshCache();
        var spuserId = MerchatAPI.getUserName();
        //判断信息是否过期
        const loginData = CacheHelper.getLocalStorage(AkContext.Keys.TokenDataKey);
        const loginhelper = CacheHelper.getLocalStorage(AkContext.Keys.LoginHerperKey);
        if (CacheHelper.isAuthCookiesExpiration() &&
            loginData && loginhelper
            && loginData.UserModel &&
            loginData.UserModel.SPAccount.toLowerCase().trim()
            === spuserId.toLowerCase().trim()) {
            MerchatAPI.LoginToken = loginhelper[0];
            window["YeeOffice_JSVersion"] = loginhelper[2];
            merchatLoginResponse = loginData;
        } else {
            var token = MerchatAPI.getMerchatToken()
            MerchatAPI.ForcedRefreshCache = true;
            let url = CacheHelper.getLocalStorage(AkContext.Keys.LoginHerperKey)[1];
            // Cookies.set("LoginUrl", window["LoginUrl"].replace(")/login", ")"), {
            //     path: '/'
            // });
            let request = new Request<MerchatLoginRequest, MerchatLoginResponse>();
            request.sendXMLHttpRequest("post",
                url,
                {
                    MerchatToken: token,
                    LoginName: spuserId,
                },
                false,
                (data) => {
                    if (data.Data && data.Data.Secret) {
                        CacheHelper.setAuthCookies(data.Data);
                        merchatLoginResponse = data.Data; 
                    } else if (data.Data && data.Data.UserModel) {
                        if (data.Data.UserModel.Status.toString() === '10000' || data.Data.UserModel.Status.toString() === '10017') {
                            callback && callback(true)
                        }
                    } else {
                        if (AkGlobal.intl) {
                            AkNotification.error({
                                message: AkGlobal.intl.formatMessage({ id: CommonLocale.Tip }),
                                description: AkGlobal.intl.formatMessage({ id: CommonLocale.SPLoginResponseUnauthorized })
                            });
                        }

                    }
                },
                (data) => {
                    if (AkGlobal.intl) {
                        AkNotification.error({
                            message: AkGlobal.intl.formatMessage({ id: CommonLocale.Tip }),
                            description: AkGlobal.intl.formatMessage({ id: CommonLocale.SPLoginResponseUnauthorized })
                        });
                    }
                    return null;
                }, true);
        }
        return merchatLoginResponse;
    }
    /**
   * 获取商户具体API信息
   */
    static getMerchatInfo() {
        if (AkContext.getBranch() === AkContext.YeeOffice) {
            return MerchatAPI.getMerchatInfoYeeOffice();
        } else {
            return MerchatAPI.getMerchatInfoSPOnline();
        }
    }
    static RefreshAKTokenData() {
        let request = new Request<MerchatLoginRequest, MerchatLoginResponse>();
        request.sendXMLHttpRequest("post",
            "/account/refreshAKToken",
            null,
            false,
            (s) => {
                if (s.Data) {
                    CacheHelper.setAuthCookies(JSON.parse(s.Data));
                    return s.Data;
                }
            },
            (d) => {
                console.log(d);
                return null;
            }, true);
    }
    static getMerchatInfoYeeOffice() {
        let merchatData = CacheHelper.getLocalStorage(AkContext.Keys.AppInfoKey);
        if (merchatData.length > 0) {
            let data: MerchatAPIModel = {
                MerchantName: "",
                AppInfoList: merchatData
            };
            return data;
        } else {
            window.location.href = window.location.origin + window["SignOutURI"];
        }
    }

    static getMerchatInfoSPOnline() {
        var d = null;
        if (MerchatAPI.ForcedRefreshCache) {
            CacheHelper.clearAppinfoList();
            MerchatAPI.ForcedRefreshCache = false;
        }
        let merchatData = CacheHelper.getLocalStorage(AkContext.Keys.AppInfoKey);
        if (CacheHelper.isAuthCookiesExpiration() && merchatData && merchatData.length > 0) {
            return { Data: merchatData } as MerchatModelResponse;
        } else {
            let url = CacheHelper.getLocalStorage(AkContext.Keys.LoginHerperKey)[1].replace(")/login", ")") + "/GetMerchantInfo";
            var request = new Request<any, MerchatModelResponse>();
            request.sendXMLHttpRequest("post",
                url,
                null,
                false,
                (data) => {
                    if (data.Data) {
                        CacheHelper.setLocalStorage(AkContext.Keys.AppInfoKey, data.Data)
                        d = data;
                    }
                },
                (data) => {
                }, true, null, true);
        }
        return d;
    }
    //同步刷新
    static refreshAPIMapping() {
        let url = CacheHelper.getLocalStorage(AkContext.Keys.LoginHerperKey)[0].replace(")/login", ")") + "/GetMerchantInfo";
        var xmlhttp = new XMLHttpRequest;
        xmlhttp.onreadystatechange = () => {
            if (xmlhttp.readyState === 4 && ((xmlhttp.status >= 200 && xmlhttp.status < 300) || xmlhttp.status === 304)) {
                var data = JSON.parse(xmlhttp.responseText) as MerchatModelResponse;
                CacheHelper.setLocalStorage(AkContext.Keys.AppInfoKey, data.Data);
            }
        };
        xmlhttp.open("POST", url, false);
        xmlhttp.setRequestHeader("Content-Type", "application/json");
        xmlhttp.setRequestHeader(AkContext.Keys.LoginSecretKey, AkContext.getToken());
        xmlhttp.send(null);
    }

    /**
     * 获取导航数据
     * @param data 分页参数
     */
    static getMechatNavigation(data: MerchatNavigationRequest) {
        const url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/YunGalaxyNavigation/Items";
        var request = new Request<AkRequest, MerchatNavigationResponse>();
        request.defaultParam.Url = url;
        return request.get(url, null);
    }

    static getMechatPortal() {
        const url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficePortal) + "/api/portal/items";
        var request = new Request<null, MerchatPortalResponse>()
        request.defaultParam.IgnoreError = true;
        return request.get(url, null)

    }

    static UpdatePassword(data: UpdatePasswordRequest) {
        const url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/userinfo/V4/password";
        return new Request<UpdatePasswordRequest, AkResponse>().put(url, data);
    }

    /**获取该用户是否为新用户第一次进入 */
    static async getNewUserForGuideModal() {
        let url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "api/tenantext";
        return new Request<AkRequest, AkResponse>().get(url, null);
    }

    /**写入已阅读引导页记录 */
    static async putUserGuideViewData() {
        let url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "api/tenantext";
        return new Request<AkRequest, AkResponse>().post(url, null);
    }

}

