import { AkContext, AppKeys, AkResponse } from "../..";
import { Request } from "../../util/request";
import { AppCenterListRequest, AppCenterListResponse, Response, ListResponse, AppCenterRequest, AppCenterPermissionResponse, UpdateAppCenterRequest, UpdataNavigationRequest, UpdateWelcomePageRequest, GetAppNavigationByIdRequest, GetAppNavigationListRequest, NavigationListRequest, NavigationResponse } from "./appcentermodal";

export class AppCenterApi {
    /**获取自定义应用列表 */
    static async getAppCenterList(data?: AppCenterListRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/AppCenter/apps";
        return new Request<AppCenterListRequest, ListResponse<AppCenterListResponse>>().get(url, data);
    }

    /**获取导航数据列表 */
    static async getNavigationListSort(data?:NavigationListRequest) {
        data = {...data, isManage: false}
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/YunGalaxyNavigation/menu";
        return new Request<NavigationListRequest, Response<NavigationResponse>>().get(url, data)
    }

    /**获取单个自定义应用信息 */
    static async getAppCenter(data?: AppCenterRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/AppCenter/appcenterid";
        return new Request<AppCenterRequest, Response<AppCenterListResponse>>().get(url, data);
    }

    /**根据appCenterID获取权限列表 */
    static async getAppCenterPermission(data?: AppCenterRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/AppCenter/permission";
        return new Request<AppCenterRequest, Response<AppCenterPermissionResponse>>().get(url, data);
    }

    /**修改应用基本信息 */
    static async updateAppCenter(data?: UpdateAppCenterRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/AppCenter";
        return new Request<UpdateAppCenterRequest, AkResponse>().put(url, data);
    }

    /**设置是否显示导航 */
    static async updateNavigation(data?: UpdataNavigationRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/AppCenter/ShowNavigation";
        return new Request<UpdataNavigationRequest, AkResponse>().put(url, data);
    }

    /**设置欢迎页 */
    static async updateWelcomePage(data?: UpdateWelcomePageRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/AppCenter/WelcomePage";
        return new Request<UpdateWelcomePageRequest, AkResponse>().put(url, data);
    }
    /**根据ID获取导航信息 */
    static async getNavigationById(data: GetAppNavigationByIdRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/AppCenter/Navigation/id";
        return new Request<GetAppNavigationByIdRequest, AkResponse>().get(url, data);
    }
    /**获取Appd导航列表 */
    static async getNavigationList(data: GetAppNavigationListRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/AppCenter/Navigation/user";
        return new Request<GetAppNavigationListRequest, AkResponse>().get(url, data);
    }
}