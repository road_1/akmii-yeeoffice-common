import { AkRequest, AkResponse } from "../../util/common";
import { AkIdentity } from "..";

export interface AppCenterListRequest extends AkRequest {
    title?: string;
}

export interface AppCenterRequest extends AkRequest {
    appCenterID?: number;
}

export interface NavigationListRequest extends AkRequest {
    title?: string;
    isManage?: boolean;
}

export interface UpdateAppCenterRequest extends AkRequest {
    AppCenterID?: number;
    Title?: string;
    IconUrl?: string;
    Description?: string;
    IsItemPerm?: boolean;
    Settings?: string;
    Manage?: AkIdentity[];
    Read?: AkIdentity[];
    IsReadEveryone?: boolean;
    PermTypeIDs?: any;
    WelcomePage?: string;
}

export interface UpdataNavigationRequest extends AkRequest {
    AppCenterID?: number;
    ShowNavigation?: boolean;
}

export interface UpdateWelcomePageRequest extends AkRequest {
    AppCenterID?: number;
    WelcomePage?: string;
}


export interface ListResponse<T> extends AkResponse {
    Data?: T[];
}

export interface Response<T> extends AkResponse {
    Data?: T;
}

export interface AppCenterListResponse {
    TenantID?: string;
    AppCenterID?: number;
    Title?: string;
    RelativeUrl?: string;
    IconUrl?: string;
    Description?: string;
    Status?: number;
    IsItemPerm?: boolean;
    ShowNavigation?: boolean;
    Settings?: string;
    WelcomePage?: string;
    SortIndex?: number;
    Created?: string;
    Modified?: string;
    CreatedBy?: string;
    ModifiedBy?: string;
    Perm?: number;
}

export interface AppCenterPermissionResponse {
    Manage?: AkIdentity[];
    Read?: AkIdentity[];
    IsReadEveryone?: boolean;
}
export interface GetAppNavigationByIdRequest {
    AppCenterID?: string;
    ID?: string;
}
export interface GetAppNavigationListRequest {
    AppCenterID?: string;
}

export interface NavigationResponse {
    AppCenterID?: number;
    BigImage?: string;
    Description?: string;
    IconUrl?: string;
    IsItemPerm?: boolean;
    IsSystem?: boolean;
    Perm?: number;
    RelativeUrl?: string;
    Settings?: string;
    ShowNavigation?: boolean;
    SmallImage?: string;
    SortIndex?: boolean;
    Status?: boolean;
    Title?: string;
    WelcomePage?: string;
}

