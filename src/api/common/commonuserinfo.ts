import { AkContext, AppKeys, Request, AkResponse } from "../../util";

export class CommonBaseInfoApi {
    static CompanyBaseInfo() {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/CompanyBaseInfo/Item";
        return new Request<any, AkResponse>().get(url);
    }

    static GetPeifileInfo() {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/userinfo/V4/profile";
        return new Request<any, AkResponse>().get(url);
    }

    static GetTimeZoneData() {
        let url: string = `https:${process.env.CDN}timezone.json`;
        return new Request<any, AkResponse>().get(url);
    }
}