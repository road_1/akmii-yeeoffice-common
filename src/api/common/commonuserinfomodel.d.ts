interface CommonUserBaseInfo {
    UserID: number;
    TenantID: number;
    TimeZone: string;
    TimeZoneFe: string;
    DateFormat: string;
    LanguageCode: string;
    ServerTimeZone: string;
    isSuccess?: boolean;
}

interface CompanyBaseInfoModel {
    ID: number;
    TenantID: string;
    Addresses?: string;
    Attr?: string;
    City?: string;
    ClientInfoList?: ClientInfoListModel[];
    CompanyName?: string;
    Contacts?: string
    CountryCode?: string;
    CountryName?: string;
    Currency?: string;
    DateFormat?: string;
    ExpireDate?: string;
    LanguageCode?: string;
    LanguageName?: string;
    Logo?: string;
    MainAdmin?: string;
    NavBarColour?: string;
    NavFontColour?: string;
    PhoneNumber?: string;
    Province?: string;
    SecondAdmin?: number;
    TechnicalContact?: string;
    TimeZone?: string;
    UserCount?: number
    UserMaxCount?: number;
    ZipCode?: string;
}

interface ClientInfoListModel {
    ClientID?: string;
    ClientType?: number;
    Status: number;
}

interface UserProfileInfo {
    TenantID: number;
    UserID: number;
    LanguageCode?: string;
    TimeZone?: string;
    DateFormat?: string;
    ServerTimeZone?: string;
    Attr?: string;
    Created?: string;
    Modified?: string;
    CreatedBy?: string;
    ModifiedBy?: string;
}

interface ToCSTValueRes {
    dateString: string;
    date: moment.Moment;
}

interface CustomListMgrAddRequest extends ContentListRequest {
    Title?: string;
    Description?: string;
    IconUrl?: string;
    Ext1?: string;
    Ext2?: string;
    Ext3?: string;
    Manage?: AkIdentity[];
    Write?: AkIdentity[];
    Read?: AkIdentity[];
    CustomType?: string;
}