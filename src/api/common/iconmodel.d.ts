interface GetIconsRequest extends AkRequest {
    md5: string;
    appKey?: string;
    isAbsoluteUrl?: boolean;
}
interface IconsModel extends AkBase {
    Url: string;
    DownloadUrl: string;
}
interface GetIconsResponse extends AkResponse {
    Data?: IconsModel;
}
