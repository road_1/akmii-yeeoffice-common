import { AkContext, AppKeys, AkResponse } from "../..";
import { Request } from "../../util/request";
export class LocalizationAPI{
    /**获取全部国家 */
    static async getAllCountrys() {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/localizations";
        return new Request<any,AkResponse>().get(url,null);
    }
    
    /**获取公司信息 */
    static async getCompanyInfo(){
        const url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings)+'/companybaseinfo/item';
        return new Request<any,AkResponse>().get(url);
    }
}