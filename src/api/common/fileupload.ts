import { Request } from "../../util/";
import { AppKeys } from "../../index";
import {AkResponse, AkContext} from '../../util/common';
import { GetLibraryByCode, LibraryResponse, PostFileBegin, FileResponse, FileRequest, PostDcoument,DeleteDocument, DeleteDocumentRequest } from './fileuploadmodel';

export class FileUpLoadAPI {
    /**
     * 获取Library 信息
     * 
     * @static
     * @param {GetLibraryByCode} [data] 
     * @returns 
     * @memberof FileUpLoadAPI
     */
    static async getLibraryByCode(data?:GetLibraryByCode){
        let url:string =AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net)+"/api/Library/code";
        // https://uat.yeeoffice.cn/YeeOfficeDocument_Net/_API/Ver(3.0)//api/Library/code
        // data.AppID = data.AppID ||  AkContext.getAppInfoID(AppKeys.YeeOfficeDocument_Net);
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<GetLibraryByCode,LibraryResponse>().get(url,data);
    }
    /**
     * 文件开始上传
     *
     * @static
     * @param {PostFileBegin} [data]
     * @returns
     * @memberof fileAPI
     */
    static async postFileBegin(data?:PostFileBegin){
        let url:string =AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net)+"/api/file/upload/begin";
        // data.AppID = data.AppID || AkContext.getAppInfoID(AppKeys.YeeOfficeDocument_Net);
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<PostFileBegin,FileResponse>().post(url,data);
    }
    /**
     * 文件分块上传
     *
     * @static
     * @param {FileRequest} [data]
     * @returns
     * @memberof fileAPI
     */
    static async postFileChunk(data?:FileRequest){
        let url:string =AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net)+"/api/file/upload/chunk";
        return new Request<FileRequest,AkResponse>().post(url,data);
    }
    
    /**
     * 文件上传结束
     *
     * @static
     * @param {FileRequest} [data]
     * @returns
     * @memberof fileAPI
     */
    static async putFileEnd(data?:FileRequest){
        let url:string =AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net)+"/api/file/upload/end";
        return new Request<FileRequest,AkResponse>().put(url,data);
    }

    /**
     * 文件上传取消
     *
     * @static
     * @param {FileRequest} [data]
     * @returns
     * @memberof fileAPI
     */
    static async putFileCancel(data?:FileRequest){
        let url:string =AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net)+"/api/file/upload/cancel";
        return new Request<FileRequest,AkResponse>().put(url,data);
    }

    /**
     * 小文件上传
     *
     * @static
     * @param {FileRequest} [data]
     * @returns
     * @memberof fileAPI
     */
    static async postSmallFile(data?:FileRequest){
        let url:string =AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net)+"/api/file/upload/small";
        return new Request<FileRequest,AkResponse>().post(url,data);
    }

    /**
     * 新增
     *
     * @static
     * @param {PostDcoument} [data]
     * @returns
     * @memberof documentAPI
     */
    static async postDocument(data?:PostDcoument){
        let url:string =AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net)+"/api/document";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<PostDcoument,AkResponse>().post(url,data);
    }

     
    /**
     * 删除
     *
     * @static
     * @param {DeleteDocument} [data]
     * @returns
     * @memberof documentAPI
     */
    static async deleteDocument(data?:DeleteDocument){
        let url:string =AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net)+"/api/document";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<DeleteDocument,AkResponse>().del(url,data);
    }

    /**
     * 获取上传进度
     *
     * @static
     * @param {FileRequest} [data]
     * @returns
     * @memberof fileAPI
     */
    static async getFileProgress(data?:FileRequest){
        let url:string =AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net)+"/api/file/progress";
        return new Request<FileRequest,AkResponse>().get(url,data);
    }
    
    // static async getDocumentByName(data?:GetDocumentByName){
    //     let url:string =AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net)+"/api/document/name";
    //     data.AppID = data.AppID || AkContext.getAppInfoID();
    //     return new Request<GetDocumentByName,DocumentResponse>().get(url,data);
    // }

    /**
     * 真删除
     *
     * @static
     * @param {DeleteDocument} [data]
     * @returns
     * @memberof documentAPI
     */
    static async deleteDocumentFile(data?:DeleteDocumentRequest){
        
        let url:string =AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net)+"/api/document/batchDelete/physical";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<DeleteDocumentRequest,AkResponse>().put(url,data);
    }

}