import { Request } from "../../util/";
import { AppKeys } from "../../index";
import { AkContext } from "../../util/common";

export class IconsAPI {
    /**
     * 获取上传接口
     */
    static GetIcons(data?: GetIconsRequest) {
        data.appKey = AkContext.getAppKey();
        let url = AkContext.getAppInfoAPI_URL(AppKeys.FileServer) + "/api/icons/url/upload";
        return new Request<GetIconsRequest, GetIconsResponse>().get(url, data);
    }
}