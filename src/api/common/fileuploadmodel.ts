import { BaseModel } from '../flowcraft/BaseModel';
import { AkResponse, AkRequest } from '../../util/common';
import { AkFile, AkFileOptions } from "../../index";
/** 获取Library 信息*/
export interface LibraryRequest extends AkRequest {
    AppID?: number;
}
export interface GetLibraryByCode extends LibraryRequest {
    Code?: string;
}

/** 开始上传 */
export interface PostFileBegin extends AkRequest {
    AppID?: number;
    MD5?: string;
    FileName?: string;
    FileExtension?: string;
    Length?: number;
    ChunkSize?: number
}
export interface FileResponse extends AkResponse {
    Data?: FileModel;
}
export interface FileModel extends BaseModel {
    FileID?: string;
    Progress?: string;
}

/** 分块上传 */
export interface FileRequest extends AkRequest {
    fileID?: string;
}

/**新增到文档库 */
export interface PostDcoument extends DocumentRequest {
    ParentID?: string;
    ContentLength?: number;
    Name?: string;
    Extension?: string;
    MD5?: string;
    Type?: number;
    OverWrite?: boolean;
    FieldValues?: object;
}

/**删除文件*/
export interface DocumentRequest extends AkRequest {
    AppID?: number;
    LibraryID?: string;
}
export interface DeleteDocument extends DocumentRequest {
    listDataID?: string;
}

export interface DeleteDocumentRequest extends DocumentRequest {
    ListDataIDs?: string[];
}

/**上传文件*/
export class AkUploadFile {
    file: AkFile;
    folderPath: string;
    overwrite: boolean;
    status: FileUploadStatus;
    error: FileUploadError;
    errorMessage?: string;
    progress: number;
    id: string;
    canceledUpload: boolean;
    fileID?: string;
    MD5?: string;
    currentChunk?: number;
}

export interface DocumentBase extends BaseModel {
    ListDataID?: string;
    MD5?: string;
    Name: string;
    Extension?: string;
}
export interface DocumentModel extends DocumentBase {
    ContentLength?: number;
    Extension?: string;
    IsFavourite?: boolean;
    LibraryID?: string;
    ListDataID?: string;
    MD5?: string;
    Name: string;
    Reference?: string;
    RowVersion?: number;
    Type?: number;
    VirtualPath?: string;
    ParentID?: string;
    FieldValues?: FileInfo;
    ModifiedByName?: string;
    CreatedByName?: string;
}
export interface FileInfo {
    BatchNo: string;
    IsItemPerm: number;
    ListID: string;
    ParentID: string;
    Status: number;
    UniqueName: string;
}

export interface LibraryResponse extends AkResponse {
    Data?: LibraryModel;
}
export interface DocumentUploadRequest extends AkRequest {
    item: AkUploadFile;
}

export interface DocumentCancelUploadRequest extends DocumentUploadRequest {
    item: AkUploadFile;
    uploaded: boolean;
}
export interface DocumentChunkUploadRequest extends DocumentUploadRequest {
    uploadedFileSize: number;
}

export interface LibraryModel extends BaseModel {
    LibraryID?: string;
    CategoryID?: string;
    IconUrl?: string;
    Name?: string;
    Localization?: string;
    Description?: string;
    ShowFileIcon?: boolean;
    Ext?: string;
    IsItemPerm?: boolean;
    Status?: number;
    ItemCount?: string;
    CategoryName?: string;
    PermissionResponse?: any;
    Perm?: number
}

export enum FileUploadStatus {
    Start = 1,
    Error = 2,
    Uploading = 90,
    Cancelling = 96,
    Canceled = 97,
    Failed = 99,
    Uploaded = 100,
}

export enum FileUploadError {
    None = 0,
    FileSizeMoreThan2GB = 1,
    FileNameHasInvalidateChars = 2,
    FileNameMoreThan200Chars = 3,
    FileAlreadyExists = 4,
    Unknown = 5,
    FileEmpty = 6
}
export class AkFileUploadItem {
    fileKey: string;
    fileSize: number;
    fileName: string;
    fileInfo: object;
    uid?: string;
}
