import { AkContext, AppKeys, AkResponse, Request } from "../../util";

export interface AdminIntlSearchRequest {
    key?: string;
    locales?: string;
    pageIndex?: number;
    pageSize?: number;
}

export interface AdminIntlSearchModel {
    Key?: string;
    Desc?: string;
    Created?: string;
    CreatedBy?: string;
    Modified?: string;
    ModifiedBy?: string;
    Locales?: { [key: string]: string };
}

export interface AdminIntlSearchResult extends AkResponse {
    Data?: AdminIntlSearchModel[]
}

export interface AdminSetRequest {
    locale?: string;
    key?: string;
    desc?: string;
    value?: string;
}
export class CustomIntlAdminAPI {
    static async search(request: AdminIntlSearchRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/admin/intl";
        return new Request<AdminIntlSearchRequest, AdminIntlSearchResult>().get(url, request);
    }

    static async set(request: AdminSetRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/admin/intl";
        return new Request<AdminSetRequest, AkResponse>().post(url, request);
    }

    static async delete(key: string) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/admin/intl/" + key;
        return new Request<void, AkResponse>().del(url);
    }
}

export class CustomIntlAPI {
    // static async put(request) {
    //     let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/admin/intl";
    //     return new Request<AdminIntlRequest, AkResponse>().get(url, request);
    // }


    static async getVersion(locale) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/intl/version";
        return new Promise((resolve, reject) => {
            resolve({ Status: 0, Data: 1024 });
        });
    }

    static async get(locale) {
        return new Promise((resolve, reject) => {
            resolve({ Status: 0, Data: {} });
        });
    }
}