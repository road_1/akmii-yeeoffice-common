//请求对象参数相同的，从缓存中取结果。此方法适用于仅包含单个请求对象的API函数。
export function simpleApiRequestCacheMiddleware(target: any, methodName: string, descriptor: PropertyDescriptor) {
    let method = descriptor.value;
    let getCacheObject = () => {
        let instance = target as { _simpleApiRequestCache };
        if (!instance._simpleApiRequestCache) {
            instance._simpleApiRequestCache = {};
        }
        if (!instance._simpleApiRequestCache[methodName]) {
            instance._simpleApiRequestCache[methodName] = {};
        }
        return instance._simpleApiRequestCache[methodName];
    };
    descriptor.value = async (req) => {
        let cache = getCacheObject();
        let requestString = JSON.stringify(req);
        if (cache[requestString]) {
            return Promise.resolve(cache[requestString]);
        }
        else {
            let promise = new Promise(async (resolve, reject) => {
                let result = await method.apply(this, [req]);
                cache[requestString] = result;
                resolve(result);
            });
            cache[requestString] = promise;
            return promise;
        }
    };
}