import { AkBaseModel } from "../../util/common";
import { exec } from "child_process";
import { AkIdentity } from "..";

export enum ContentListLayoutType {
    List = 0,
    Form = 1,
    CalenDar = 100,
    News = 999,
    MindMap = 101,
    TimeLine = 102,
    CustomPage = 103
}

export enum ContentListStatusEnum {
    Disabled = 0,
    Enabled = 1
}

export enum ContentListRightTypeEnum {
    Empty = 0,
    Read = 1,
    Write = 2,
    Manage = 4,
    Create = 8,
    Edit = 16,
    Remove = 32,
    Export = 64,
    Import = 128
}

export const  enum ContentListModalOpenType {
    Current = "current",
    New = "new",
    Target = "target"
}

export enum AppCenterPermissionEnum {
    NotPermission = 0,
    Read = 1,
    Write = 2,
    Manage = 4
}

export enum ContentListCategoryType {
    Normal = 0,
    Customer = 1
}

export enum ContentListWhereType {
    Eq, // =
    Leq, // <=
    Geq, // >=
    Gt, // >
    Lt, // <
    Neq, // !=
    IsNull,
    IsNotNull,
    LikeLast,
    In,
    Contains,
    Me,
    ContainsMe,
    //区间(前端定义，慎用)
    Between,
    EndsWith,
    InRelation
}

export enum ContentListType {
    List = 1,
    Library = 2,
    Custom = 4,
    Calendar = 8,
    CalendarList = 9,
    LibraryList = 16,
    ListSet = 1024
}

export enum RequestListSetPerm {
    ALL = 0,//可以查所有权限的列表
    NoRead = 1,//查除读权限以外的所有列表
    OnlyManage = 2,//查只有管理权限的列表
}
export interface ContentListItem extends AkBaseModel {
    AppID?: number;
    ListID?: string;
    Title?: string;
    Description?: string;
    Status?: ContentListStatusEnum;
    IconUrl?: string;
    Ext1?: string;
    Ext2?: string;
    Ext3?: string;
    MaxRole?: ContentListRightTypeEnum;
    Type?: ContentListType;
    LayoutView?: string;
    AdvanceList?: any;
    IsVerRecord?: boolean;
    WorkspaceID?: string;
    CustomType?: string;
}

export interface ContentListField extends AkBaseModel {
    AppID?: number;
    FieldID?: string;
    ListID?: string;
    FieldName?: string;
    DisplayName?: string;
    FieldType?: string;
    FieldIndex?: number;
    Type?: string;
    Category?: ContentListCategoryType;
    DefaultValue?: string;
    Rules?: string;
    InternalName?: string;
    IsSort?: boolean;
    IsIndex?: boolean;
    IsFilter?: boolean;
    IsSystem?: boolean;
    IsUnique?: boolean;
    IsIndexCreated?: boolean;
    Status?: number; // Default = 1, Readonly = 2, NotForForms = 4, NotForList = 8
    Ext1?: string;
    Ext2?: string;
    Ext3?: string;
}

export enum ContentListFieldStatus {
    /**默认 */
    Default = 1,
    /**只读 */
    Readonly = 2,
    /**不用于表单 */
    NotForForm = 4,
    /**不用于list */
    NotForList = 8,
    /**不用于查阅项 */
    NotForLookup = 16,
    /**不能被删除 */
    CanNotBeDeleted = 32,
    /**索引不能删 */
    IndexCanNotBeDeleted = 64,
    /// 类型只读
    TypeReadOnly = 128,
}

export interface ContentListRequest {
    AppID?: number;
    type?: number;//默认可不传
    title?: string;//默认可不传
    CustomType?: string; //-1:全部列表;空:没有列表集的列表;ListSite_{ListID}:listset的列表;
    workspaceID?: string;//工作区id
    perm?: RequestListSetPerm;
}

export const LIST_SET_PREFIX = "ListSite_";

export interface GetListIDByTitle {
    AppID?: number;
    Title?: string;
}

export interface ContentListItemRequest extends ContentListRequest {
    ListID?: string;
    Title?: string;
    verification?: boolean;//是否验证权限
    DataID?: string;
}

export interface CustomDataGetByListIDRequest extends ContentListRequest {
    FilterValue?: string;
    Wheres?: string;
    Sorts?: string;
    ClientHour?: number;
    ListID: string;
    Columns?: string;
    ColumnType?: number;
    ViewUrl?: string;
    // 当前语言
    lanCode?: string;
}

export interface CustomDataExportDownliadRequest {
    exportID: string;
}
export interface CustomDefinitionEditRequest extends ContentListItemRequest {
    FieldID?: number;
    DisplayName?: string;
    Type?: string;
    Category?: number;
    DefaultValue?: string;
    Rules?: string;
    IsSort?: boolean;
    IsFilter?: boolean;
    IsUnique?: boolean;
    Ext1?: string;
    Ext2?: string;
    Ext3?: string;
}
export interface CustomDefinitionEditBatchRequest extends ContentListItemRequest {
    FieldIDs?: number[];
    Type?: any;
    Status?: boolean;
}
export interface CustomDefinitionRemoveRequest extends ContentListItemRequest {
    FieldID?: number;
}
export interface CustomDefinitionAddRequest extends ContentListItemRequest {
    /** public string FieldName { get; set; } */
    FieldType?: any;
    /** public int FieldIndex { get; set; } */
    DisplayName?: string;
    /** 短文本 | 长文本 |  数字 |  时间 |  Metadata  |  Lookup  | User  |  Choice | Currency | YesNo |  hyperlink | Picture */
    Type?: string;
    /** 0:标准字段  1:自定义字段 */
    Category?: number;
    IsUnique?: boolean;
    /** DefaultValue */
    DefaultValue?: string;
    /** 根据字段类型使用Json存储规则 */
    Rules?: string;
    /** IsSort */
    IsSort?: boolean;
    /** IsFilter */
    IsFilter?: boolean;
    /** Ext1 */
    Ext1?: string;
    /** Ext2 */
    Ext2?: string;
    /** Ext3 */
    Ext3?: string;
}
export interface CustomListMgrAddRequest extends ContentListRequest {
    Title?: string;
    Description?: string;
    IconUrl?: string;
    Ext1?: string;
    Ext2?: string;
    Ext3?: string;
    Manage?: AkIdentity[];
    Write?: AkIdentity[];
    Read?: AkIdentity[];
    CustomType?: string;
}

export interface ListSetSortModal {
    AppID: number;
    ListID: string;
    ListSetID: string;
    Type: "list" | "page";
    IsHidden: boolean;
    Title: string;
}

export interface CustomListMgrEditRequest extends ContentListItemRequest {
    Title?: string;
    IconUrl?: string;
    /** 说明 */
    Description?: string;
    Ext1?: string;
    Ext2?: string;
    Ext3?: string;
    Manage?: AkIdentity[];
    Write?: AkIdentity[];
    Read?: AkIdentity[];
}
export interface CustomListEditStatusRequest extends ContentListItemRequest {
    Status?: number;
}
export interface CustomListEditExtRequest extends ContentListItemRequest {
    Ext1?: string;
    Ext2?: string;
    Ext3?: string;
}
export interface ContentListDatasRequest extends ContentListItemRequest {
    ListDataID?: string;
}


export interface ContentListDatasByIdsRequest extends ContentListItemRequest {
    ListDataIDs?: string[];
    Columns?: string[];
    Verification?: boolean;
}

export interface ContentListAddDatasRequest extends ContentListItemRequest {
    Dic?: {};
}
export interface CustonDataGetTitleRequest extends ContentListRequest {
    Title?: string;
    Columns?: string[];
    Wheres?: any[];
    Sorts?: ContentSort;
    SortsList?: ContentSort[];
    FilterValue?: string;
    PageIndex?: number;
    PageSize?: number;
    Verification?: boolean;
}
export interface ContentSort {
    SortName?: string;
    SortByDesc?: boolean;
}
export interface ContentListEditDatasRequest extends ContentListAddDatasRequest {
    ListDataID?: string;
    RowVersion?: string;
    Title?: string;
}

export interface ContentListEditBatchRequest extends ContentListAddDatasRequest {
    Title?: string;
    ParList?: ContentListEditBatchParlist[];
}

export interface ContentListEditBatchParlist {
    ListDataID: string;
    RowVersion?: string;
    Dic: {};
}
export interface ContentListDatasByQueryRequest extends ContentListItemRequest {
    Columns?: string[];
    Wheres?: ContentListWhereModel[];
    PageIndex?: number;
    PageSize?: number;
    IsExport?: boolean;
    FilterValue?: string;
    Sorts?: {};
    Verification?: boolean;
}

export interface OneValueContentListQueryRequest extends ContentListItemRequest {
    Column?: string;
    Wheres?: ContentListWhereModel[];
}

export interface ContentListDatasByIdRequest extends ContentListDatasRequest {
    Columns?: string[];
    DefaultColumns?: boolean;
    Verification?: boolean;
}

export interface ContentListWhereModel {
    WhereName: string;
    Pre?: string;
    Value?: string;
    Type: ContentListWhereType;
    Child?: any;
}

export interface ContentListAdditionModel {
    FieldID?: string;
    FieldName?: string;
    RelationName?: string;
    IsShow?: boolean;
    DisplayName?: string;
    Type?: string;
}

export interface CustomListLayoutsRequest extends ContentListRequest {
    listID: string;
    type: number;
}

export interface LayoutViewRequest extends ContentListRequest {
    ListID: string;
    LayoutView: string;
}

export interface LayoutTitlesRquest extends ContentListRequest {
    ListID: string;
    DicList: any;
    Verification?: boolean;
}

export interface ContentListCopyRquest {
    ListID: string;
    AppID: number;
    ListDataID: string;
    Dic?: any;
}

export interface ContentListCheckRquest {
    ListID: string;
    AppID: number;
    ParentID: string;
    Name: string;
    Extension: string;
}

export interface LayoutRequest extends ContentListRequest {
    layoutID?: string;
    listID?: string;
    title?: string;
}

export interface ListDataIDRequest extends ContentListRequest {
    Title?: string;
    ListID?: string;
    ListDataID?: string;
    Columns?: string[];
    DefaultColumns?: boolean;
}

// export interface AkListItem extends AkDictionaryString<string> {
//     AppID?: string;
//     ListID?: string;
//     ListDataID?: string;
//     TenantID?: string;
//     Created?: string;
//     CreatedBy?: string;
//     Modified?: string;
//     ModifiedBy?: string;
//     RowVersion?: string;
// }
export interface SearchTagsRequest {
    CategoryID?: string;
    TagName?: string;
    Limit?: number;
}
export interface GetTagsByItemIdRequest {
    ItemID?: string;
    IsGetTagName?: Boolean;
}
export interface GetTagsByIdsRequest {
    TagIDs?: string[];
}
export interface TagAddRequest {
    CategoryID?: string;
    TagID?: string;
    TagName?: string;
    Ext?: string;
}

export interface CheckPathRequest {
    AppID: number;
    ListID: string;
    path: string;
    isCreate?: boolean;
}

export interface AddLibraryDocsRequest {
    AppID: number;
    ListID: string;
    ParentID?: string;
    DicList?: any[];
    Type: AddLibraryDocsType;
}

export enum AddLibraryDocsType {
    Ignore = 0,//已存在忽略
    Rename = 1,//已存在重命名
    Cover = 2//已存在修改
}
export interface SearchEnginesRequest extends ContentListItemRequest {
    Columns?: string[];
    Wheres?: ContentListWhereModel[];
    PageIndex?: number;
    PageSize?: number;
    IsExport?: boolean;
    FilterValue?: string;
    Sorts?: {};
    Verification?: boolean;
}

export interface ExportSearchRequest extends ContentListItemRequest {

}

