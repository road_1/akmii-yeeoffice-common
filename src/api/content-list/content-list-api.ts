import { Request } from "../../util/request";
import { AkResponse, AppKeys, AkListResponse, AkItemResponse, AkContext } from "../../util/common";
import { CheckPathRequest, AddLibraryDocsRequest, GetListIDByTitle, OneValueContentListQueryRequest, SearchTagsRequest, GetTagsByItemIdRequest, GetTagsByIdsRequest, ContentListCopyRquest, ContentListCheckRquest, CustomDataExportDownliadRequest, TagAddRequest, SearchEnginesRequest, ExportSearchRequest } from './content-list-model';
import {
    ContentListItemRequest,
    ContentListItem,
    ContentListRequest,
    ContentListDatasByQueryRequest,
    ContentListDatasRequest,
    ContentListAddDatasRequest,
    ContentListEditDatasRequest,
    ContentListDatasByIdRequest,
    ContentListDatasByIdsRequest,
    ContentListField,
    CustomListMgrAddRequest,
    CustomListMgrEditRequest,
    CustomListEditStatusRequest,
    CustomListEditExtRequest,
    CustomDefinitionEditRequest,
    CustomDefinitionEditBatchRequest,
    CustomDefinitionRemoveRequest,
    CustomDefinitionAddRequest,
    CustonDataGetTitleRequest,
    CustomDataGetByListIDRequest,
    CustomListLayoutsRequest,
    LayoutViewRequest,
    LayoutTitlesRquest,
    LayoutRequest,
    ListDataIDRequest
} from "./content-list-model";
import { ContentListEditBatchRequest } from "..";
import { ContentListLayout } from "../listSet-model";

export class ContentListApi {
    static async GetLists(data: ContentListRequest = {}) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/list/user";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<ContentListRequest, AkListResponse<ContentListItem>>().get(url, data);
    }

    static async GetAllContentList(data: ContentListRequest = {}) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/list/all";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<ContentListRequest, AkListResponse<ContentListItem>>().get(url, data);
    }

    // @simpleApiRequestCacheMiddleware
    static async GetList(data: ContentListItemRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/list";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        let response = await new Request<ContentListItemRequest, AkItemResponse<ContentListItem>>().get(url, data);
        if (response.Status === 0 && !response.Data.LayoutView) {
            response.Data.LayoutView = JSON.stringify({
                add: "default",
                edit: "default",
                view: "default",
            });
        }
        return response;
    }

    /**添加表 */
    static async AddList(data: CustomListMgrAddRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/list";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<CustomListMgrAddRequest, AkResponse>().post(url, data);
    }

    static async EditList(data: CustomListMgrEditRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/list";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<CustomListMgrEditRequest, AkResponse>().put(url, data);
    }

    static async EditListStatus(data: CustomListEditStatusRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/list/status";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<CustomListEditStatusRequest, AkResponse>().put(url, data);
    }

    static async EditListExt(data: CustomListEditExtRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/list/ext";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<CustomListEditExtRequest, AkResponse>().put(url, data);
    }


    // @simpleApiRequestCacheMiddleware
    static async GetFields(data: ContentListItemRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/defs/listid";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<ContentListItemRequest, AkListResponse<ContentListField>>().get(url, data);
    }

    static async EditFields(data: CustomDefinitionEditRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/defs";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<CustomDefinitionEditRequest, AkResponse>().put(url, data);
    }

    static async EditFieldsBatch(data: CustomDefinitionEditBatchRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/defs/batch";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<CustomDefinitionEditBatchRequest, AkResponse>().put(url, data);
    }

    static async DeleteFields(data: CustomDefinitionRemoveRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/defs/fieldid";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<CustomDefinitionRemoveRequest, AkResponse>().del(url, data);
    }

    static async AddFields(data: CustomDefinitionAddRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/defs";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<CustomDefinitionAddRequest, AkResponse>().post(url, data);
    }

    /**单条列表查询 */
    static async GetDataByID(data: ContentListDatasByIdRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas/id";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<ContentListDatasByIdRequest, AkItemResponse<any>>().post(url, data);
    }

    /**
     * 查询单列单条数据
     * @param data
     */
    // @simpleApiRequestCacheMiddleware
    static async GetOneValue(data: OneValueContentListQueryRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas/default";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<OneValueContentListQueryRequest, AkResponse>().post(url, data);
    }

    /**查询多条数据 */
    // @simpleApiRequestCacheMiddleware
    static async GetDataByIDs(data: ContentListDatasByIdsRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas/ids";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<ContentListDatasByIdsRequest, AkListResponse<any>>().post(url, data);
    }

    /**列表的数据加载 */
    static async GetDataByListID(data: ContentListDatasByQueryRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas/listid";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<ContentListDatasByQueryRequest, AkListResponse<any>>().post(url, data);
    }

    /**列表的数据加载 */
    static async GetDataByListIDs(data: ContentListDatasByQueryRequest[]) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas/listids";
        // data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<ContentListDatasByQueryRequest[], AkListResponse<any>>().post(url, data);
    }

    /**添加单条数据 */
    static async AddData(data: ContentListAddDatasRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<ContentListAddDatasRequest, AkResponse>().post(url, data);
    }

    /**批量添加数据 */
    static async AddBatchData(data: LayoutTitlesRquest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas/addbatch";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<LayoutTitlesRquest, AkResponse>().post(url, data);
    }

    /**复制数据 */
    static async CopyData(data: ContentListCopyRquest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/document/copy";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<ContentListCopyRquest, AkResponse>().put(url, data);
    }

    /**文档库列表删除数据 */
    static async LibraryListDeleteData(data: ContentListCopyRquest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/document/id";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<ContentListCopyRquest, AkResponse>().del(url, data);
    }

    /**检查文件或者文件夹是否存在 */
    static async LibraryListCheck(data: ContentListCheckRquest) {
        data.AppID = data.AppID || AkContext.getAppInfoID();

        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/document/checkdoc?AppID=" + data.AppID + "&ListID=" + data.ListID + "&ParentID=" + data.ParentID + "&Name=" + data.Name + "&Extension=" + data.Extension;
        return new Request<ContentListCheckRquest, AkResponse>().get(url);
    }

    static async GetDataByTitle(data: CustonDataGetTitleRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas/title";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<CustonDataGetTitleRequest, AkResponse>().post(url, data);
    }

    /**编辑单条数据 */
    static async EditData(data: ContentListEditDatasRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<ContentListEditDatasRequest, AkResponse>().put(url, data);
    }

    /**删除单条数据 */
    static async DeleteData(data: ContentListDatasRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas/id";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<ContentListDatasRequest, AkResponse>().del(url, data);
    }

    /**删除整个列表的数据 */
    // static async DeleteDataByListID(data: ContentListDatasRequest) {
    //     const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas/listid";
    //     data.AppID = data.AppID || AkContext.getAppInfoID();
    //     return new Request<ContentListDatasRequest, AkResponse>().del(url, data);
    // }

    /**导入列表数据 */
    static async ImportData(data: ContentListItemRequest, file: FormData) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas/import?AppID=" + data.AppID + "&ListID=" + data.ListID;
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<ContentListItemRequest, AkResponse>().send(url, file);
    }

    static async GetDatasTemplate(data: ContentListItemRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas/template";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<ContentListItemRequest, AkResponse>().get(url, data);
    }

    static async GetDatasExport(data: CustomDataGetByListIDRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas/export";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<CustomDataGetByListIDRequest, AkResponse>().get(url, data);
    }


    static getDatasExportCreate(data: CustomDataGetByListIDRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas/exportcreate";
        return new Request<CustomDataGetByListIDRequest, AkResponse>().put(url, data);
    }

    static DataBaseExportCreate(data: CustomDataExportDownliadRequest) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas/exportdownload";
        return new Request<CustomDataExportDownliadRequest, AkResponse>().get(url, data);
    }

    static async GetCustomListLayouts(data: CustomListLayoutsRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/layouts/type";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<CustomListLayoutsRequest, AkResponse>().get(url, data);
    }

    static async SaveLayoutView(data: LayoutViewRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/list/layoutview";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<LayoutViewRequest, AkResponse>().put(url, data);
    }

    static async SaveLayoutTitles(data: LayoutTitlesRquest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/layouts/titles";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<LayoutTitlesRquest, AkResponse>().put(url, data);
    }

    // @simpleApiRequestCacheMiddleware
    static async GetLayouts(data: LayoutRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/layouts";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<LayoutRequest, AkResponse>().get(url, data);
    }

    static async DeleteLayout(data: LayoutRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/layouts/layoutid";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<LayoutRequest, AkResponse>().del(url, data);
    }

    // 根据listdataid 获取一条数据
    static async GetListByListDataId(data: ListDataIDRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas/id";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<ListDataIDRequest, AkResponse>().post(url, data);
    }


    //   根据title添加单条数据
    static async AddDataByTitle(data: ContentListAddDatasRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas/title/add";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<ContentListAddDatasRequest, AkResponse>().post(url, data);
    }

    //   根据title编辑单条数据
    static async EditDataByTitle(data: ContentListEditDatasRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas/title/edit/id";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<ContentListEditDatasRequest, AkResponse>().put(url, data);
    }

    //   根据title批量编辑单条数据
    static async EditBatchByTitle(data: ContentListEditBatchRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas/title/editbatch/id";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<ContentListEditBatchRequest, AkResponse>().put(url, data);
    }

    //   根据title获取单条数据
    static async GetListByListDataIdAndTitle(data: ListDataIDRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas/title/id";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<ListDataIDRequest, AkResponse>().post(url, data);
    }

    //   根据title删除单条数据
    static async DelListDataByTitle(data: ContentListDatasRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas/title/id";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<ListDataIDRequest, AkResponse>().del(url, data);
    }

    // 根据title查询相关列表数据
    static async GetListData(data: GetListIDByTitle) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/list/title";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<GetListIDByTitle, AkResponse>().get(url, data);
    }
    //搜索标签及相关标签
    static async SearchTags(data: SearchTagsRequest) {

        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/tags/startwith";
        return new Request<SearchTagsRequest, AkResponse>().get(url, data);
    }
    //根据ItemID获取标签
    static async GetTagsByItemID(data: GetTagsByItemIdRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/tags/refs/" + data.ItemID;
        return new Request<GetTagsByItemIdRequest, AkResponse>().get(url, data);
    }
    //根据tagIDs批量获取标签
    static async GetTagsByIDs(ids?: GetTagsByIdsRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/tags/taginfos";
        return new Request<GetTagsByIdsRequest, AkResponse>().post(url, ids);
    }
    //新增标签
    static async AddTag(data: TagAddRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/tags";
        return new Request<TagAddRequest, AkResponse>().post(url, data);
    }

    /**检测path是否存在返回最后一个id 注:isCreate为true会创建对应文件夹*/
    static async checkOrAddLibraryPath(data: CheckPathRequest) {
        data.AppID = data.AppID || AkContext.getAppInfoID();
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/document/path/exists";
        return new Request<CheckPathRequest, AkResponse>().get(url, data);
    }

    /**批量添加文档 */
    static async AddLibraryDocs(data: AddLibraryDocsRequest) {
        data.AppID = data.AppID || AkContext.getAppInfoID();
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/document/docs";
        return new Request<AddLibraryDocsRequest, AkResponse>().post(url, data);
    }
    /**批量添加数据 不走事物*/
    static async ProjectDatasAdd(data: LayoutTitlesRquest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas/adds";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<LayoutTitlesRquest, AkResponse>().post(url, data);
    }

    /** 内容列表模糊搜索 */
    static async SearchEngines(data: SearchEnginesRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/searchengines/keywords";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<SearchEnginesRequest, AkResponse>().post(url, data);
    }

    /** 内容列表模糊搜索导出*/
    static async ExportSearchList(data: ExportSearchRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/searchengines/exportcreate";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<ExportSearchRequest, AkResponse>().put(url, data);
    }

    static async GetLayoutByListID(data: ContentListItemRequest) {
        const url = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/layouts/listid";
        data.AppID = data.AppID || AkContext.getAppInfoID();
        return new Request<ContentListItemRequest, AkListResponse<ContentListLayout>>().get(url, data);
    }
}