import * as React from "react";
import { CommonLocale } from "../locales/localeid";
import { AkRowProps } from "../components/controls/ak-row";
import { AkColProps, AkCol } from "../components/controls/ak-col";
import { AkGlobal } from "./common";
import { AkForm } from "../components/controls/ak-form";
import * as classNames from "classnames";
import { getValuePropValue } from '../components/controls/ak-select';

/**
 * 比较操作运算符
 * 
 * @export
 * @enum {number}
 */
export enum ComparisonOperator {
    Equals,               //等于
    GreaterThan,          //大于
    GreaterOrEqualsThan,  //大于等于
    LessThan,             //小于
    LessOrEqualsThan      //小于等于
}

/**
 * Form Helper functions
 *
 * @export
 * @class FormHelper
 */
export class FormHelper {
    /**
     * 设置表单值为Int类型
     *
     * @static
     * @param {any} event on blur event
     * @param {any} props
     * @param {string} id 表单Key
     *
     * @memberof FormHelper
     */
    public static setIntegerFieldValue(event, props, id: string) {
        const value = new Object();
        let intValue = parseInt(event.target.value);
        if (isNaN(intValue)) intValue = null;
        value[id] = intValue;
        props.form.setFieldsValue(value);
        props.form.validateFields([id], { force: true });
    }

    /**
     * 设置表单值为Float类型
     *
     * @static
     * @param {any} event on blur event
     * @param {any} props
     * @param {string} id 表单Key
     * @param {number} [fix=null] 保留几位小数
     *
     * @memberof FormHelper
     */
    public static setFloatFieldValue(event, props, id: string, fix: number = null) {
        const value = new Object();
        let floatValue = parseFloat(event.target.value);
        if (isNaN(floatValue)) floatValue = null;
        if (floatValue !== null && fix && fix > 0) value[id] = floatValue.toFixed(fix);
        else value[id] = floatValue;
        props.form.setFieldsValue(value);
        props.form.validateFields([id], { force: true });
    }

    /**
     * 设置表单值为0.5或1类型
     *
     * @param {any} event on blur event
     * @param {any} props
     * @param {string} id 表单Key
     *
     * @memberof FormHelper
     */
    public static setOneOrHalfValue(event, props, id: string) {
        const value = new Object();
        let floatValue = parseFloat(event.target.value);
        if (isNaN(floatValue)) floatValue = null;
        if (floatValue !== null) {
            let mod = floatValue % 1;
            if (mod > 0.5) {
                floatValue = floatValue - mod + 1
            } else if (mod > 0) {
                floatValue = floatValue - mod + 0.5
            }
            value[id] = floatValue.toFixed(1);
        } else {
            value[id] = null;
        }
        props.form.setFieldsValue(value);
        props.form.validateFields([id], { force: true });
    }

    /**
     * 设置Select框Placeholder
     *
     * @static
     * @param {*} props
     * @param {string} name 多语言Key
     * @returns {string}
     *
     * @memberof FormHelper
     */
    public static pleaseSelectMessage(props: any, name: string): string {
        return AkGlobal.intl.formatMessage({ id: CommonLocale.FormValidatePleaseSelect }) + AkGlobal.intl.formatMessage({ id: name });
    }

    /**
     * 设置Input框Placeholder
     *
     * @static
     * @param {*} props
     * @param {string} name 多语言Key
     * @returns {string}
     *
     * @memberof FormHelper
     */
    public static pleaseInputMessage(props: any, name: string): string {
        return AkGlobal.intl.formatMessage({ id: CommonLocale.FormValidatePleaseInput }) + AkGlobal.intl.formatMessage({ id: name });
    }

    /**
     * 必填验证
     *
     * @static
     * @param {any} props
     * @param {string} name 多语言Key
     * @returns
     *
     * @memberof FormHelper
     */
    public static ruleForRequire(props: any, name: string) {
        return {
            required: true,
            message: AkGlobal.intl.formatMessage({ id: CommonLocale.FormValidatePleaseInput }) + AkGlobal.intl.formatMessage({ id: name })
        };
    }

    /**
     * 
     * 
     * @static
     * @param {*} props 
     * @param {string} name 
     * @returns 
     * @memberof FormHelper
     */
    public static ruleForRequireMultiple(props: any, name: string) {
        const { formatMessage } = AkGlobal.intl;
        return {
            validator: (rule, value, callback) => {
                if (!value || value.length === 0) {
                    callback(formatMessage({ id: CommonLocale.FormValidatePleaseInput }) + formatMessage({ id: name }));
                }
                callback();
            }
        }
    }

    /**
     * 
     * 
     * @static
     * @param {*} props 
     * @param {string} name 多语言Key
     * @param {string} format Format Key
     * @param {RegExp} regex 
     * 
     * @memberof FormHelper
     */
    public static ruleForRegExp(props: any, name: string, format: string, regex: RegExp) {
        const { formatMessage } = AkGlobal.intl;
        return {
            validator: (rule, value, callback) => {
                if (value && !regex.test(value)) {
                    callback(formatMessage({ id: format }, { name: formatMessage({ id: name }) }));
                }
                callback();
            }
        };
    }

    /**
     * 整数验证
     * 
     * @static
     * @param {*} props 
     * @param {string} name 多语言Key
     * @returns 
     * 
     * @memberof FormHelper
     */
    public static ruleForInteger(props: any, name: string) {
        return FormHelper.ruleForRegExp(props, name, CommonLocale.FormInteger, /^(0|[1-9][0-9]*|-[1-9][0-9]*)$/);
    }

    /**
     * 整数或浮点数验证
     * 
     * @static
     * @param {*} props 
     * @param {string} name 多语言Key
     * @returns 
     * 
     * @memberof FormHelper
     */
    public static ruleForNumber(props: any, name: string) {
        return FormHelper.ruleForRegExp(props, name, CommonLocale.FormNumber, /^-*[0-9,\.]+$/);
    }

    /**
     * 邮箱验证
     * 
     * @static
     * @param {*} props 
     * @param {string} name 多语言Key
     * @returns 
     * 
     * @memberof FormHelper
     */
    public static ruleForEmail(props: any, name: string) {
        return FormHelper.ruleForRegExp(props, name, CommonLocale.FormEmail, /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/);
    }

    /**
     * 银行账号验证
     * 
     * @static
     * @param {*} props 
     * @param {string} name 多语言Key
     * @returns 
     * 
     * @memberof FormHelper
     */
    public static ruleForBankAccount(props: any, name: string) {
        return FormHelper.ruleForRegExp(props, name, CommonLocale.FormBankAccount, /^(\d{16}|\d{19})$/);
    }

    /**
     * 网站地址验证
     * 
     * @static
     * @param {*} props 
     * @param {string} name 多语言Key
     * @returns 
     * 
     * @memberof FormHelper
     */
    public static ruleForURL(props: any, name: string) {
        return FormHelper.ruleForRegExp(props, name, CommonLocale.FormUrl, /^((ht|f)tps?):\/\/[\w\-]+(\.[\w\-]+)+([\w\-\.,@?^=%&:\/~\+#]*[\w\-\@?^=%&\/~\+#])?$/);
    }

    /**
     * 数值比较验证
     *
     * @static
     * @param {any} props
     * @param {string} name 多语言Key
     * @param {ComparisonOperator} op 操作符
     * @param {number} rightValue 比较值
     * @returns
     *
     * @memberof FormHelper
     */
    public static ruleForComparisonNumber(props: any, name: string, op: ComparisonOperator, rightValue: number) {
        const { formatMessage } = AkGlobal.intl;
        return {
            validator: (rule, value, callback) => {
                let leftValue = parseFloat(value);
                if (!isNaN(leftValue)) {
                    switch (op) {
                        case ComparisonOperator.Equals:
                            if (leftValue === rightValue) {
                                callback();
                            } else {
                                callback(formatMessage({ id: CommonLocale.FormEqualsRule }, { name: formatMessage({ id: name }), value: rightValue.toString() }))
                            }
                            break;
                        case ComparisonOperator.GreaterOrEqualsThan:
                            if (leftValue >= rightValue) {
                                callback();
                            } else {
                                callback(formatMessage({ id: CommonLocale.FormGreaterOrEqualsRule }, { name: formatMessage({ id: name }), value: rightValue.toString() }));
                            }
                            break;
                        case ComparisonOperator.GreaterThan:
                            if (leftValue > rightValue) {
                                callback();
                            } else {
                                callback(formatMessage({ id: CommonLocale.FormGreaterThanRule }, { name: formatMessage({ id: name }), value: rightValue.toString() }));
                            }
                            break;
                        case ComparisonOperator.LessOrEqualsThan:
                            if (leftValue <= rightValue) {
                                callback();
                            } else {
                                callback(formatMessage({ id: CommonLocale.FormLessOrEqualsThanRule }, { name: formatMessage({ id: name }), value: rightValue.toString() }));
                            }
                            break;
                        case ComparisonOperator.LessThan:
                            if (leftValue < rightValue) {
                                callback();
                            } else {
                                callback(formatMessage({ id: CommonLocale.FormLessThanRule }, { name: formatMessage({ id: name }), value: rightValue.toString() }));
                            }
                            break;
                    }
                } else {
                    callback();
                }
            }
        }
    }

    /**
     * 长度比较验证
     *
     * @static
     * @param {*} props
     * @param {string} name
     * @param {ComparisonOperator} op
     * @param {number} rightValue
     * @returns
     *
     * @memberof FormHelper
     */
    public static ruleForComparisonLength(props: any, name: string, op: ComparisonOperator, rightValue: number) {
        const { formatMessage } = AkGlobal.intl;
        return {
            validator: (rule, value, callback) => {
                if (value !== null && value !== undefined) {
                    const leftValue: number = value.toString().length;
                    switch (op) {
                        case ComparisonOperator.Equals:
                            if (leftValue === rightValue) {
                                callback();
                            } else {
                                callback(formatMessage({ id: CommonLocale.FormEqualsRule }, {
                                    name: formatMessage({ id: CommonLocale.FormLength }, { name: formatMessage({ id: name }) }), value: rightValue.toString()
                                }));
                            }
                            break;
                        case ComparisonOperator.GreaterOrEqualsThan:
                            if (leftValue >= rightValue) {
                                callback();
                            } else {
                                callback(formatMessage({ id: CommonLocale.FormGreaterOrEqualsRule }, {
                                    name: formatMessage({ id: CommonLocale.FormLength }, { name: formatMessage({ id: name }) }), value: rightValue.toString()
                                }));
                            }
                            break;
                        case ComparisonOperator.GreaterThan:
                            if (leftValue > rightValue) {
                                callback();
                            } else {
                                callback(formatMessage({ id: CommonLocale.FormGreaterThanRule }, {
                                    name: formatMessage({ id: CommonLocale.FormLength }, { name: formatMessage({ id: name }) }), value: rightValue.toString()
                                }));
                            }
                            break;
                        case ComparisonOperator.LessOrEqualsThan:
                            if (leftValue <= rightValue) {
                                callback();
                            } else {
                                callback(formatMessage({ id: CommonLocale.FormLessOrEqualsThanRule }, {
                                    name: formatMessage({ id: CommonLocale.FormLength }, { name: formatMessage({ id: name }) }), value: rightValue.toString()
                                }));
                            }
                            break;
                        case ComparisonOperator.LessThan:
                            if (leftValue < rightValue) {
                                callback();
                            } else {
                                callback(formatMessage({ id: CommonLocale.FormLessThanRule }, {
                                    name: formatMessage({ id: CommonLocale.FormLength }, { name: formatMessage({ id: name }) }), value: rightValue.toString()
                                }));
                            }
                            break;
                    }
                } else {
                    callback();
                }
            }
        }
    }

    /**
     * 范围验证（开始）
     *
     * @static
     * @param {*} props
     * @param {string} relatedId 相关结束值，表单Key
     * @returns
     *
     * @memberof FormHelper
     */
    public static ruleForRangeFrom(props: any, relatedId: string) {
        const form = props.form;
        return {
            validator: (rule, value, callback) => {
                if (value && form.isFieldTouched(relatedId)) {
                    form.validateFields([relatedId], { force: true });
                }
                callback();
            }
        };
    }

    /**
     * 范围验证（结束）
     *
     * @static
     * @param {*} props
     * @param {string} relatedId 相关开始值，表单Key
     * @param {string} fromName 相关开始值，多语言Key
     * @param {string} toName 相关结束值，多语言Key
     * @returns
     *
     * @memberof FormHelper
     */
    public static ruleForRangeTo(props: any, relatedId: string, fromName: string, toName: string) {
        const { intl: { formatMessage } } = AkGlobal;
        return {
            validator: (rule, value, callback) => {
                if (value && value < props.form.getFieldValue(relatedId)) {
                    callback(formatMessage({ id: CommonLocale.FormRangeMessage }, {
                        left: formatMessage({ id: fromName }), right: formatMessage({ id: toName })
                    }));
                } else {
                    callback();
                }
            }
        }
    }

    /**
     * row layout
     *
     * @static
     * @type {AkRowProps}
     * @memberof FormHelper
     */
    public static rowLayout: AkRowProps = {
        gutter: 15,
        type: "flex",
        align: 'top',
        justify: 'start'
    }

    /**
     * col layour for label
     *
     * @static
     * @type {AkColProps}
     * @memberof FormHelper
     */
    public static colLabelLayout: AkColProps = {
        lg: 4,
        md: 4,
        sm: 4,
        xs: 24,
        className: "ant-form-item-label"
    }

    /**
     * col control layout
     *
     * @static
     * @type {AkColProps}
     * @memberof FormHelper
     */
    public static colControlLayout: AkColProps = {
        lg: 8,
        md: 8,
        sm: 8,
        xs: 24,
        className: ""
    }

    /**
     * 大Control栅格Props
     *
     * @static
     * @type {AkColProps}
     * @memberof FormHelper
     */
    public static largeControlLayout: AkColProps = {
        lg: 20,
        md: 20,
        sm: 20,
        xs: 24,
        className: ""
    }

    /**
     * Form Item Layout
     *
     * @static
     *
     * @memberof FormHelper
     * */
    public static formItemLayout = {
        labelCol: { xs: { span: 24 }, sm: { span: 8 }, className: "" },
        wrapperCol: { xs: { span: 24 }, sm: { span: 16 }, className: "" }
    }

    /**
     * 渲染局部标题
     *
     * @static
     * @param {*} title 标题
     * @returns
     *
     * @memberof FormHelper
     */
    public static renderSectionTitle(title: any) {
        return <div className="ak-form-title mb15">
            <span className="title-bluespan"></span>
            {title}
        </div>
    }

    /**
     * 渲染局部小标题
     *
     * @static
     * @param {*} title  标题
     * @returns
     *
     * @memberof FormHelper
     */
    public static renderSubSectionTitle(title: any) {
        return <div className="ak-table-title mb15">
            <label>{title}</label>
        </div>
    }

    /**Modal框表单
     * 
     * 
     * @static
     * @param {*} props
     * @param {string} id  表单控件的多语言id和标签的多语言id
     * @param {string} key   表单控件的key值
     * @param {*} formControl  表单Control Element
     * @param {boolean} [required=false]  该表单是否必填？默认为否  如果为true的话，标签需要会出现红色星号，表单Control为必填项
     * @param {*} [rules=null]  验证规则，为null时自动生成必填验证以及长度验证
     * @param {AkColProps} [labelLayout=FormHelper.colLabelLayout]  表单标签栅格Props
     * @param {(string | React.ReactNode)} [extra=null] 额外的提示信息，和 help 类似，当需要错误信息和提示文案同时出现时，可以使用这个。
     * @returns 
     * 
     * @memberof FormHelper
     */
    public static renderFormItemInModal(props: any,
        id: string,
        key: string,
        formControl: any,
        required: boolean = false,
        rules: any = null,
        formItemLayout: any = FormHelper.formItemLayout,
        extra: string | React.ReactNode = null) {
        const { getFieldDecorator } = props.form;

        if (rules === null) {
            rules = [];
            rules.push(FormHelper.ruleForComparisonLength(props, id, ComparisonOperator.LessOrEqualsThan, 50))
            if (required) rules.push(FormHelper.ruleForRequire(props, id));
        }
        return <AkForm.Item label={AkGlobal.intl.formatMessage({ id: id })} {...formItemLayout}>
            {getFieldDecorator(key, { rules: rules })(formControl)}
        </AkForm.Item>
    }

    /**完整表单Item
     *
     *
     * @static
     * @param {*} props
     * @param {string} id  表单控件的多语言id和标签的多语言id
     * @param {string} key   表单控件的key值
     * @param {*} formControl  表单Control Element
     * @param {boolean} [required=false]  该表单是否必填？默认为否  如果为true的话，标签需要会出现红色星号，表单Control为必填项
     * @param {*} [rules=null]  验证规则，为null时自动生成必填验证以及长度验证
     * @param {AkColProps} [labelLayout=FormHelper.colLabelLayout]  表单标签栅格Props
     * @param {AkColProps} [controlLayout=FormHelper.colControlLayout] 表单Control栅格Props
     * @param {(string | React.ReactNode)} [extra=null] 额外的提示信息，和 help 类似，当需要错误信息和提示文案同时出现时，可以使用这个。
     * @returns
     *
     * @memberof FormHelper
     */
    public static renderFormItem(
        props: any,
        id: string,
        key: string,
        formControl: any,
        required: boolean = false,
        rules: any = null,
        labelLayout: AkColProps = FormHelper.colLabelLayout,
        controlLayout: AkColProps = FormHelper.colControlLayout,
        extra: string | React.ReactNode = null) {
        const label = FormHelper.renderFormItemLabel(props, id, required, labelLayout);
        const control = FormHelper.renderFormItemControl(props, id, key, formControl, required, rules, controlLayout, extra);
        return [label, control];
    }

    /**
     * 表单标签
     *
     * @static
     * @param {*} props
     * @param {string} id  标签的多语言id
     * @param {boolean} [required=false]  是否必需？需要会出现红色星号
     * @param {AkColProps} [labelLayout=FormHelper.colLabelLayout]标签栅格Props
     * @returns
     *
     * @memberof FormHelper
     */
    public static renderFormItemLabel(
        props: any,
        id: string,
        required: boolean = false,
        labelLayout: AkColProps = FormHelper.colLabelLayout) {
        const { formatMessage } = AkGlobal.intl;
        return <AkCol {...labelLayout} key="label">
            <label className={classNames({ "ant-form-item-required": required })}>
                {formatMessage({ id: id })}
            </label>
        </AkCol>
    }

    /**
     * 表单Control元素
     *
     * @static
     * @param {*} props
     * @param {string} id 表单控件的多语言id
     * @param {string} key 表单控件key值
     * @param {*} formControl   表单Control元素
     * @param {boolean} [required=false]  该表单是否必填？默认为否
     * @param {*} [rules=null]验证规则，为null时自动生成必填验证以及长度验证
     * @param {AkColProps} [controlLayout=FormHelper.colControlLayout]表单Control栅格Props
     * @param {(string | React.ReactNode)} [extra=null] 额外的提示信息，和 help 类似，当需要错误信息和提示文案同时出现时，可以使用这个。
     * @returns
     *
     * @memberof FormHelper
     */
    public static renderFormItemControl(
        props: any,
        id: string,
        key: string,
        formControl: any,
        required: boolean = false,
        rules: any = null,
        controlLayout: AkColProps = FormHelper.colControlLayout,
        extra: string | React.ReactNode = null) {
        const { getFieldDecorator } = props.form;

        if (rules === null) {
            rules = [];
            rules.push(FormHelper.ruleForComparisonLength(props, id, ComparisonOperator.LessOrEqualsThan, 50))
            if (required) rules.push(FormHelper.ruleForRequire(props, id));
        }

        return <AkCol {...controlLayout} key="control">
            <AkForm.Item extra={extra}>
                {getFieldDecorator(key, { rules: rules })(formControl)}
            </AkForm.Item>
        </AkCol>
    }

    /**
     * Approval页面表单
     *
     * @static
     * @param {*} props
     * @param {string} id  表单的多语言id
     * @param {*} value  表单的value值
     * @param {AkColProps} [labelLayout=FormHelper.colLabelLayout]表单标签栅格Props
     * @param {AkColProps} [controlLayout=FormHelper.colControlLayout]表单Control栅格Props
     * @returns
     *
     * @memberof FormHelper
     */
    public static renderApprovalItem(
        props: any,
        id: string,
        value: any,
        labelLayout: AkColProps = FormHelper.colLabelLayout,
        controlLayout: AkColProps = FormHelper.colControlLayout) {
        const { formatMessage } = AkGlobal.intl;
        // controlLayout.className += ' text-overflow line line-3';
        const label = <AkCol {...labelLayout} key="label">{formatMessage({ id: id })}</AkCol>
        const control = <AkCol {...controlLayout} key="value">
            <div className="ant-form-item-control-wrapper">
                {value}
            </div>
        </AkCol>
        return [label, control];
    }
}