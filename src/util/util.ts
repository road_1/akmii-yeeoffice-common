import { AkGlobal, AkContext } from './common';
import {
    remove,
    each,
    before,
    isFunction,
    isEqual,
    padStart,
    padEnd,
    isArray,
    round,
    fill,
    difference,
    differenceWith,
    slice,
    concat,
    isNumber,
    groupBy,
    defaultsDeep,
    uniqBy,
    sumBy,
    endsWith
} from "lodash";
import * as scrollIntoView from "dom-scroll-into-view";
import { AkFileIcon } from "./ak-file-icon";
import * as moment from 'moment'
import { NoticeLocale } from '../locales/localeid';
import { URLHelper } from './URLHelper';
import { AkDatetimeUtil } from './datetime';

export class AkUtil {
    /**获取聊天时间 */
    static getChatTime(date: moment.Moment) {
        const baseInfo = AkDatetimeUtil.getBaseInfo();

        if (typeof date === 'string') {
            date = moment(date);
        }

        // 时区转换
        if (baseInfo.ServerTimeZone !== "UTC") {
            date = date.tz("UTC");
        }
        date = date.tz(baseInfo.TimeZoneFe || baseInfo.ServerTimeZone)

        var now = moment().endOf('day');
        var dayDiff = now.diff(date, 'd');
        if (dayDiff === 0) {
            return date.format("HH:mm");
            // return AkGlobal.intl.formatTime(date.toDate());
        }
        if (dayDiff === 1) {
            return AkGlobal.intl.formatMessage({ id: NoticeLocale.Yesterday }, { 'date': date.format("HH:mm") });
        }
        return date.format(baseInfo.DateFormat + " HH:mm");
    }
    /**
     * 是否包含节点
     * @param root 根节点
     * @param el 要包含的节点
     */
    static contains(root, el) {
        if (root.compareDocumentPosition)
            return root === el || !!(root.compareDocumentPosition(el) & 16);
        if (root.contains && el.nodeType === 1) {
            return root.contains(el) && root !== el;
        }
        while ((el = el.parentNode))
            if (el === root) return true;
        return false;
    }
    /**
     * 生成guid
     * @returns {string}
     */
    static guid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0;
            // tslint:disable-next-line:triple-equals
            var v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
    static parseURL(url) {
        var parser = document.createElement("a"),
            searchObj = {},
            queries, key, value

        parser.href = url
        queries = parser.search.replace(/^\?/, "").split("&")
        queries.forEach((query) => {
            key = query.split("=")[0]
            value = query.split("=")[1]
            searchObj[key] = value
        })

        return {
            protocol: parser.protocol,
            host: parser.host,
            hostname: parser.hostname,
            port: parser.port,
            pathname: parser.pathname,
            searchObj: searchObj,
            hash: parser.hash
        }
    }

    /**
     * 将给定参数转换成字符串，如果不是字符串，则返回默认值
     * @param param 
     * @param defaultValue 没有默认值则返回undefined
     */
    static parseStr(param, defaultValue?: string): string {
        if (param !== null) {
            let type = typeof param;
            if (type !== "object" && type !== "function") {
                //不是object和函数，则直接返回字符串
                return param + "";
            }
        }
        return defaultValue;
    }

    static round(n: number, precision?: number): number {
        return round(n, precision);
    }

    /**
     * 解决float精度问题
     * @param value 
     * @param defaultValue 如果对象不可处理，则返回默认值，没有默认值则返回undefined
     * @param precision  精度，默认10
     */
    static floatPrecision(value, defaultValue?: number, precision?: number): number {
        // tslint:disable-next-line:triple-equals
        if (value != null) {
            if (typeof value !== "number") {
                value = parseFloat(value);
            }
            if (!isNaN(value)) {
                //#10715 浮点数偏差，修改精度从8位到6位
                return Number(value.toFixed(precision ? precision : 6));
            }
        }
        return defaultValue;
    }

    /**
     * 格式化数字显示，如果不是数字返回undefined
     * @param n 
     * @param digit 小数点后几位
     * @param thousandth 是否显示千分位
     */
    static formatNumber(n, digit, thousandth?) {
        let rs;
        if (n === null || n === undefined) {
            n = 0;
        }
        if (AkUtil.isNumber(Number(n))) {
            let temp = Number(n) + "";
            let parts = temp.split(".");
            let part1 = parts[0] || "0";
            let part2 = parts.length > 1 ? parts[1] : "0";
            rs = part1;
            if (thousandth) {
                rs = rs.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            }
            if (digit === undefined || digit === null) {
                if (parts.length > 1) {
                    rs += "." + part2;
                }
            } else if (digit === 0) {
                if (parts.length > 1) {
                    rs += "." + part2;
                    rs = Math.round(rs);
                }
            } else if (digit > 0) {
                if (part2.length > digit) {
                    //#1023 需要四舍五入截断
                    let temp = round("0." + part2, digit) + "";
                    rs += "." + temp.substr(2);
                } else {
                    part2 = AkUtil.padEnd(part2, digit, "0");
                    rs += "." + part2;
                }
            }
        }
        return rs;
    }

    /**
     * 将给定参数转换成数字，如果不能转换，则返回默认值
     * @param param 
     * @param defaultValue 没有默认值则返回undefined
     */
    static parseNumber(param, defaultValue?): number {
        // tslint:disable-next-line:triple-equals
        if (param != null) {
            let type = typeof param;
            // tslint:disable-next-line:triple-equals
            if (type != "object" && type != "function" && !isNaN(param)) {
                //不是object和函数并且可转成数字，转换成数字返回
                return Number(param);
            }
        }
        return defaultValue;
    }

    static isNumber(value) {
        return isNumber(value);
    }

    static isPercentage(v) {
        let temp = (v + "").trim();
        if (AkUtil.endsWith(temp, '%')) {
            temp = temp.substr(0, temp.length - 1);
            return AkUtil.isNumber(Number(temp));
        } else {
            return false;
        }
    }

    /**
     * 判断值是否为真（考虑后端的返回结果和bit）
     */
    static isTrue(v) {
        return v === "true" || v === "True" || v === true || v === 1 || v === "1"
    }

    /**
     * 尝试把percentage转成number，如果不是数字或百分制则返回defaultvalue
     * @param v 
     * @param defaultValue 
     */
    static percentageToNumber(v, defaultValue?) {
        let result = defaultValue;
        if (v) {
            let temp = (v + "").trim();
            if (AkUtil.endsWith(temp, '%')) {
                temp = temp.substr(0, temp.length - 1);
                let num = AkUtil.parseNumber(temp, null);
                if (num === null) {
                    //不是数字
                    result = defaultValue;
                } else {
                    result = num / 100;
                }
            } else {
                //直接尝试用数字处理
                result = AkUtil.parseNumber(temp, defaultValue);
            }
        }
        return result;
    }

    static ScrollToTop() {
        scrollIntoView(document.body, document, { alignWithTop: true });
    }

    /**
     * 获取QueryString中的值
     * @param key
     * @returns {any}
     */
    static getQueryString(key): string {
        var reg = new RegExp("(^|&|\\?)" + key + "=([^&]*)(&|$)", "i");
        //先从URL参数中取
        //不能直接从全部里边取，因为这样会把前端Router的hash也认为是参数的值
        var r = window.location.search.substr(1).match(reg);
        if (r) {
            return decodeURIComponent(r[2]);
        }
        //如果URL中参数没有，从前端router参数中取
        r = window.location.hash.substr(1).match(reg);
        if (r) {
            return decodeURIComponent(r[2]);
        }
        return null;
    }

    static getFileExt(fileName: string) {
        return ((/[.]/.exec(fileName)) ? /[^.]+$/.exec(fileName)[0] : "").toLowerCase();
    }

    static getThumbUrl(fileName: string) {
        switch (this.getFileExt(fileName)) {
            case "ppt":
            case "pptx":
                return AkFileIcon.PPT;
            case "doc":
            case "docx":
                return AkFileIcon.Word;
            case "xls":
            case "xlsx":
                return AkFileIcon.Excel;
            case "vsdx":
            case "vsd":
                return AkFileIcon.VISO;
            case "pdf":
                return AkFileIcon.PDF;
            case "txt":
                return AkFileIcon.TXT;
            case "png":
            case "jpg":
            case "gif":
            case "jpeg":
            case "icon":
                return AkFileIcon.IMG;
            default:
                return AkFileIcon.FILE;
        }
    }

    /**
     * 深度对比两个对象，但是同一个对象相当于===
     * @param value
     * @param other
     * @returns {boolean}
     */
    static isEqual(value, other) {
        return isEqual(value, other);
    }

    /**
     * 深度复制两个对象
     * @param obj
     * @returns {any}
     */
    static deepClone(obj) {
        if (obj === null || typeof obj !== 'object') {
            return obj;
        }

        return JSON.parse(JSON.stringify(obj));
    }

    /**
     * 深度合并对象的值
     * @param object
     * @param sources
     * @returns {TResult}
     */
    static defaultsDeep(object, ...sources: any[]) {
        return defaultsDeep(object, sources);
    }

    /**
     * 根据传入的解释器，用来获取对象的属性
     * @param obj
     * @param desc
     * @returns {any}
     */
    static getPropByKeyFunc(obj: any, desc: string | ((record) => any)) {
        if (obj && obj !== null) {
            if (typeof desc === "string") {
                return obj[desc];
            } else {
                return desc(obj);
            }
        } else {
            return null;
        }
    }

    /**
     * 从数组中移除一个满足条件的对象
     * @param array
     * @param predicate
     * @returns {T[]} 返回删除的数组
     */
    static remove(array: any[], predicate: (value: any, index?, arr?) => boolean): any[] {
        return remove(array, predicate);
    }

    /**
     * insert in position of array
     * @param array
     * @param value
     * @param index
     * @returns {any} new created array with inserted
     */
    static insert(array: any[], value: any, index: number) {
        if (array) {
            if (index > 0) {
                const length = array.length;
                if (index >= length) {
                    return concat(array, value);
                } else {
                    const part1 = slice(array, 0, index);
                    const part2 = slice(array, index, length);
                    return concat(part1, value, part2);
                }
            } else {
                return concat(value, array);
            }
        } else {
            return null;
        }
    }

    /**
     * Converts value to an array
     * 
     * @static
     * @param {*} value 
     * @param {boolean} [removeStrEmpty=true] String.Empty是否作为空值
     * @returns 
     * @memberof AkUtil
     */
    static toArray(value: any, removeStrEmpty: boolean = true) {
        let ret = value;
        if (value === undefined) {
            ret = undefined;
        } else if (value === null) {
            ret = null;
        } else if (removeStrEmpty && value === "") {
            ret = null;
        } else if (!Array.isArray(value)) {
            ret = [value];
        }
        return ret;
    }

    /**
     * 和insert区别是插入到当前列表而不是返回新列表
     * @param array
     * @param value
     * @param index
     */
    static insertInto(array: any[], value: any, index: number) {
        if (array) {
            const length = array.length;
            if (index >= length) {
                array.push(value);
            } else {
                var start = index > 0 ? index : 0;
                let toReplace = value;
                var temp;
                while (start < length) {
                    temp = array[start];
                    array[start] = toReplace;
                    toReplace = temp;
                    start++;
                }
                array.push(toReplace);
            }
        }
        return array;
    }

    /**
     * 在指定位位置覆盖元素（会修改传入的数组）
     * @param array 原始数组
     * @param value 插入对象
     * @param start 开始位置
     * @param end 结束位置
     * @returns {T[]}
     */
    static fill(array: any[], value: any, start?: number, end?: number) {
        return fill(array, value, start, end);
    }

    /**
     * 遍历对象或数组
     * @param obj
     * @param iteratee
     * @returns {any}
     */
    static each(obj, iteratee: (value, idx) => any): any {
        return each(obj, iteratee);
    }

    /**
     * 根据条件将数组分组，生成以key分组的dictionary
     * @param collection
     * @param iteratee 返回分组key
     * @returns {Dictionary<T[]>}
     */
    static groupBy(collection, iteratee: (value, idx) => any) {
        return groupBy(collection, iteratee as any);
    }

    /**
     * 创建一个差异化的数组
     * @param array
     * @param values
     */
    static difference<T>(array: T[], ...values: Array<T[]>) {
        return difference(array, ...values);
    }

    /**
     * 根据传入的对比函数创建一个差异化的数组
     * @param array
     * @param values
     * @param comparator
     * @returns {any[]}
     */
    static differenceWith(array, values, comparator: (v1, v2) => boolean) {
        return differenceWith(array, values, comparator);
    }

    /**
     * 延迟执行方法
     * @param func
     * @param args
     * @returns {number}
     */
    static defer(func: Function, ...args) {
        return setTimeout(func, 1, ...args)
    }

    static once(func) {
        return AkUtil.before(2, func);
    }

    static before(n: number, func: any) {
        return before(n, func);
    }

    static stringSize(str: string) {
        return str ? str.length : 0;
    }

    static padStart(string: string, length: number, chars: string) {
        return padStart(string, length, chars);
    }

    static padEnd(string: string, length: number, chars: string) {
        return padEnd(string, length, chars);
    }

    static isFunction(value) {
        return isFunction(value)
    }

    static isArray(value) {
        return isArray(value)
    }

    static distinct(arr: any[]) {
        return Array.from(new Set(arr));
    }
    static uniqBy(arr: any[], iteratee?: any) {
        return uniqBy(arr, iteratee);
    }
    static sumBy(arr: any[], iteratee?: any) {
        return sumBy(arr, iteratee);
    }
    static top(arr: any[], item: any) {
        let index = arr.indexOf(item);
        var temp = item;
        arr.unshift(temp);
        arr.splice(index + 1, 1)
        return arr;
    }
    /**
     * 将数组转成对象，key为指定的属性
     * @param arr
     * @param keyProp 指定数组对象中的属性为结果Object的key
     * @returns {{}}
     */
    static arrayToObject(arr: any[], keyProp?: string): any {
        let rs = {};
        if (arr && arr.length > 0) {
            this.each(arr, v => {
                if (keyProp) {
                    const key = v[keyProp];
                    if (key !== undefined) {
                        rs[key] = v;
                    }
                } else {
                    rs[v] = v;
                }
            });
        }
        return rs;
    }

    /**
     * copy text to clipboard
     * @param text
     * @param options
     * @returns {boolean}
     */
    static copyToClipboard(text, options?) {

        var defaultMessage = 'Copy to clipboard: #{key}, Enter';

        function format(message) {
            var copyKey = (/mac os x/i.test(navigator.userAgent) ? '⌘' : 'Ctrl') + '+C';
            return message.replace(/#{\s*key\s*}/g, copyKey);
        }

        var debug, message, reselectPrevious, range, selection, mark, success = false;
        if (!options) {
            options = {};
        }
        debug = options.debug || false;
        try {
            reselectPrevious = AkUtil.toggleSelection();

            range = document.createRange();
            selection = document.getSelection();

            mark = document.createElement('span');
            mark.textContent = text;
            mark.setAttribute('style', [
                // reset user styles for span element
                'all: unset',
                // prevents scrolling to the end of the page
                'position: fixed',
                'top: 0',
                'clip: rect(0, 0, 0, 0)',
                // used to preserve spaces and line breaks
                'white-space: pre',
                // do not inherit user-select (it may be `none`)
                '-webkit-user-select: text',
                '-moz-user-select: text',
                '-ms-user-select: text',
                'user-select: text',
            ].join(';'));

            document.body.appendChild(mark);

            range.selectNode(mark);
            selection.addRange(range);

            var successful = document.execCommand('copy');
            if (!successful) {
                throw new Error('copy command was unsuccessful');
            }
            success = true;
        } catch (err) {
            debug && console.error('unable to copy using execCommand: ', err);
            debug && console.warn('trying IE specific stuff');
            try {
                window["clipboardData"].setData('text', text);
                success = true;
            } catch (err) {
                debug && console.error('unable to copy using clipboardData: ', err);
                debug && console.error('falling back to prompt');
                message = format('message' in options ? options.message : defaultMessage);
                window.prompt(message, text);
            }
        } finally {
            if (selection) {
                if (typeof selection.removeRange === 'function') {
                    selection.removeRange(range);
                } else {
                    selection.removeAllRanges();
                }
            }

            if (mark) {
                document.body.removeChild(mark);
            }
            reselectPrevious();
        }

        return success;
    }

    static toggleSelection() {
        var selection = document.getSelection();
        if (!selection.rangeCount) {
            return function () {
            };
        }
        var active = document.activeElement;

        var ranges = [];
        for (var i = 0; i < selection.rangeCount; i++) {
            ranges.push(selection.getRangeAt(i));
        }

        switch (active.tagName.toUpperCase()) { // .toUpperCase handles XHTML
            case 'INPUT':
            case 'TEXTAREA':
                let blur = active["blur"];
                blur();
                break;

            default:
                active = null;
                break;
        }

        selection.removeAllRanges();
        return function () {
            selection.type === 'Caret' &&
                selection.removeAllRanges();

            if (!selection.rangeCount) {
                ranges.forEach(function (range) {
                    selection.addRange(range);
                });
            }

            if (active) {
                let focus = active["focus"];
                focus();
            }
        };
    }

    /**
     * AntD Form的field根据传入的Props变化，该方法用于根据props中的具体对象转成form fields要求格式
     * @param obj
     */
    static mapPropsToFields(obj) {
        let fields = {};
        if (obj) {
            AkUtil.each(Object.keys(obj), p => {
                fields[p] = { value: obj[p] }
                // fields[p] = Form.createFormField({ value: obj[p] })
            });
        }
        return fields;
    }

    static noContentIcon = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA9CAYAAADxoArXAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKTWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVN3WJP3Fj7f92UPVkLY8LGXbIEAIiOsCMgQWaIQkgBhhBASQMWFiApWFBURnEhVxILVCkidiOKgKLhnQYqIWotVXDjuH9yntX167+3t+9f7vOec5/zOec8PgBESJpHmomoAOVKFPDrYH49PSMTJvYACFUjgBCAQ5svCZwXFAADwA3l4fnSwP/wBr28AAgBw1S4kEsfh/4O6UCZXACCRAOAiEucLAZBSAMguVMgUAMgYALBTs2QKAJQAAGx5fEIiAKoNAOz0ST4FANipk9wXANiiHKkIAI0BAJkoRyQCQLsAYFWBUiwCwMIAoKxAIi4EwK4BgFm2MkcCgL0FAHaOWJAPQGAAgJlCLMwAIDgCAEMeE80DIEwDoDDSv+CpX3CFuEgBAMDLlc2XS9IzFLiV0Bp38vDg4iHiwmyxQmEXKRBmCeQinJebIxNI5wNMzgwAABr50cH+OD+Q5+bk4eZm52zv9MWi/mvwbyI+IfHf/ryMAgQAEE7P79pf5eXWA3DHAbB1v2upWwDaVgBo3/ldM9sJoFoK0Hr5i3k4/EAenqFQyDwdHAoLC+0lYqG9MOOLPv8z4W/gi372/EAe/tt68ABxmkCZrcCjg/1xYW52rlKO58sEQjFu9+cj/seFf/2OKdHiNLFcLBWK8ViJuFAiTcd5uVKRRCHJleIS6X8y8R+W/QmTdw0ArIZPwE62B7XLbMB+7gECiw5Y0nYAQH7zLYwaC5EAEGc0Mnn3AACTv/mPQCsBAM2XpOMAALzoGFyolBdMxggAAESggSqwQQcMwRSswA6cwR28wBcCYQZEQAwkwDwQQgbkgBwKoRiWQRlUwDrYBLWwAxqgEZrhELTBMTgN5+ASXIHrcBcGYBiewhi8hgkEQcgIE2EhOogRYo7YIs4IF5mOBCJhSDSSgKQg6YgUUSLFyHKkAqlCapFdSCPyLXIUOY1cQPqQ28ggMor8irxHMZSBslED1AJ1QLmoHxqKxqBz0XQ0D12AlqJr0Rq0Hj2AtqKn0UvodXQAfYqOY4DRMQ5mjNlhXIyHRWCJWBomxxZj5Vg1Vo81Yx1YN3YVG8CeYe8IJAKLgBPsCF6EEMJsgpCQR1hMWEOoJewjtBK6CFcJg4Qxwicik6hPtCV6EvnEeGI6sZBYRqwm7iEeIZ4lXicOE1+TSCQOyZLkTgohJZAySQtJa0jbSC2kU6Q+0hBpnEwm65Btyd7kCLKArCCXkbeQD5BPkvvJw+S3FDrFiOJMCaIkUqSUEko1ZT/lBKWfMkKZoKpRzame1AiqiDqfWkltoHZQL1OHqRM0dZolzZsWQ8ukLaPV0JppZ2n3aC/pdLoJ3YMeRZfQl9Jr6Afp5+mD9HcMDYYNg8dIYigZaxl7GacYtxkvmUymBdOXmchUMNcyG5lnmA+Yb1VYKvYqfBWRyhKVOpVWlX6V56pUVXNVP9V5qgtUq1UPq15WfaZGVbNQ46kJ1Bar1akdVbupNq7OUndSj1DPUV+jvl/9gvpjDbKGhUaghkijVGO3xhmNIRbGMmXxWELWclYD6yxrmE1iW7L57Ex2Bfsbdi97TFNDc6pmrGaRZp3mcc0BDsax4PA52ZxKziHODc57LQMtPy2x1mqtZq1+rTfaetq+2mLtcu0W7eva73VwnUCdLJ31Om0693UJuja6UbqFutt1z+o+02PreekJ9cr1Dund0Uf1bfSj9Rfq79bv0R83MDQINpAZbDE4Y/DMkGPoa5hpuNHwhOGoEctoupHEaKPRSaMnuCbuh2fjNXgXPmasbxxirDTeZdxrPGFiaTLbpMSkxeS+Kc2Ua5pmutG003TMzMgs3KzYrMnsjjnVnGueYb7ZvNv8jYWlRZzFSos2i8eW2pZ8ywWWTZb3rJhWPlZ5VvVW16xJ1lzrLOtt1ldsUBtXmwybOpvLtqitm63Edptt3xTiFI8p0in1U27aMez87ArsmuwG7Tn2YfYl9m32zx3MHBId1jt0O3xydHXMdmxwvOuk4TTDqcSpw+lXZxtnoXOd8zUXpkuQyxKXdpcXU22niqdun3rLleUa7rrStdP1o5u7m9yt2W3U3cw9xX2r+00umxvJXcM970H08PdY4nHM452nm6fC85DnL152Xlle+70eT7OcJp7WMG3I28Rb4L3Le2A6Pj1l+s7pAz7GPgKfep+Hvqa+It89viN+1n6Zfgf8nvs7+sv9j/i/4XnyFvFOBWABwQHlAb2BGoGzA2sDHwSZBKUHNQWNBbsGLww+FUIMCQ1ZH3KTb8AX8hv5YzPcZyya0RXKCJ0VWhv6MMwmTB7WEY6GzwjfEH5vpvlM6cy2CIjgR2yIuB9pGZkX+X0UKSoyqi7qUbRTdHF09yzWrORZ+2e9jvGPqYy5O9tqtnJ2Z6xqbFJsY+ybuIC4qriBeIf4RfGXEnQTJAntieTE2MQ9ieNzAudsmjOc5JpUlnRjruXcorkX5unOy553PFk1WZB8OIWYEpeyP+WDIEJQLxhP5aduTR0T8oSbhU9FvqKNolGxt7hKPJLmnVaV9jjdO31D+miGT0Z1xjMJT1IreZEZkrkj801WRNberM/ZcdktOZSclJyjUg1plrQr1zC3KLdPZisrkw3keeZtyhuTh8r35CP5c/PbFWyFTNGjtFKuUA4WTC+oK3hbGFt4uEi9SFrUM99m/ur5IwuCFny9kLBQuLCz2Lh4WfHgIr9FuxYji1MXdy4xXVK6ZHhp8NJ9y2jLspb9UOJYUlXyannc8o5Sg9KlpUMrglc0lamUycturvRauWMVYZVkVe9ql9VbVn8qF5VfrHCsqK74sEa45uJXTl/VfPV5bdra3kq3yu3rSOuk626s91m/r0q9akHV0IbwDa0b8Y3lG19tSt50oXpq9Y7NtM3KzQM1YTXtW8y2rNvyoTaj9nqdf13LVv2tq7e+2Sba1r/dd3vzDoMdFTve75TsvLUreFdrvUV99W7S7oLdjxpiG7q/5n7duEd3T8Wej3ulewf2Re/ranRvbNyvv7+yCW1SNo0eSDpw5ZuAb9qb7Zp3tXBaKg7CQeXBJ9+mfHvjUOihzsPcw83fmX+39QjrSHkr0jq/dawto22gPaG97+iMo50dXh1Hvrf/fu8x42N1xzWPV56gnSg98fnkgpPjp2Snnp1OPz3Umdx590z8mWtdUV29Z0PPnj8XdO5Mt1/3yfPe549d8Lxw9CL3Ytslt0utPa49R35w/eFIr1tv62X3y+1XPK509E3rO9Hv03/6asDVc9f41y5dn3m978bsG7duJt0cuCW69fh29u0XdwruTNxdeo94r/y+2v3qB/oP6n+0/rFlwG3g+GDAYM/DWQ/vDgmHnv6U/9OH4dJHzEfVI0YjjY+dHx8bDRq98mTOk+GnsqcTz8p+Vv9563Or59/94vtLz1j82PAL+YvPv655qfNy76uprzrHI8cfvM55PfGm/K3O233vuO+638e9H5ko/ED+UPPR+mPHp9BP9z7nfP78L/eE8/sl0p8zAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAANESURBVHja7NpNiFVlGMDx34xXy9TM0k1JhBmhaR+IVpuMJGkRRKQZ1MZF4CJbVNCqTbQLokiRgiQmiJCCIQkUJcGsBjLxYxqsqGisjIKa1FBQ57a4z8FD4My9d84958zc88Dhhcv78fzP857n431vT19fn5xlNbZjBUbyXrxX/rIAizCrgLULAT4X7cVuAS5UWgW+DVeXSPc7Og38Cf7CtSWAHcbhTgM/gWk4WSB0L77DDXik08ADsa1nFAg9iJvxAD7Ow2kNpaCHc4TuDdglWIN9eXrpBHoWfsMVLYyttbn2t7Hmw/i0iLA0hHsD9tcWLJ3E3wst6HgMi/FoOM7C4vAAluM6nMA1TYzZF+HkzybXOI5leBD9ZUg8BgP6KvyNOeP0P4OjqI/Tbzb+xS0Bu7dMmdYg7sNXqW80C92GsCErWBkqB59hVYbzncLKKpeugCvgTL/hhZiH0RIZ7GI4t8yBl0c4KaO8i41ZAl8fsL/gufhtWglAz+EevBil4htZAR9NHQCcKpl1+3ETXsdBfD5Rp7U3UsdlJYRN1+kncQDTJwL8cpRiG/FNyR3wndEeaxd4A17Ca+EUyi5/RPV2Kz5oFXhpDNqP5ydRmB3AM2GsZ5sFnh7b9x+NW4LJJlvxYXjsu5sBPhjtkkmcUK3Hz2Hx2WMBv43b8VB4vcksiRM7cjngTXg6XPzuKZA2j0TCtAjv/T/xWIVtsQ2+xv3oGWfCnji1OKtxpnWiwwDzwgNfGYaqNzHmd3yEpzSOlrbXMB9fRocb8X0byrwVO6STsnascDOGnI/2HRyqYWe8sSfjqGZGkxPVo+8XmJvDFp0Z7eP4oYXS9nxUef3YX4vkewfeL3ldnayxC6dbHHsEm7EtmeSnNpVIdkMed72jqcqtHRlOv7VCbuNzljldecRTAVfA4ycfIvnotJzJQueJAifeeX4OwAtS8b9tmehVy4VI3R7TuCcelf3h3mg8C/FjpL+FfsPrsCXg65HZZPkk8XeHxjXr2SItnMjmeCovXQFXwBVwBVwBV8AVcAVcAVfAUxy43gWs9TTw6S4AHkkD39UFwCuTevjNqGVfdelueKrJYryCAzWNvwbMxQtT3MJ7sPa/AQCmb68uqxzhmQAAAABJRU5ErkJggg=="
    static noDataIcon = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJkAAAB+CAYAAAA3HbOAAAAABGdBTUEAALGPC/xhBQAAEsRJREFUeAHtXQtUVVUa3ryStyQ+eCpgOZSKGggqPlARNWssX6GOtdKUGRVSJzUdJ5nEytUix8c0o4k6Nr4yTRNdjaJiig8Uy9FKWUVJPlAx5aEgCMz337h0udx77r3nnnO4HPZZ63DO2Y9/7/2d7/5773//+8AYP5oEAhEREfuio6M9mkRl9SrpqPfcJB9TU1MHFhcXjyktLX0KDbBrhEZUeXp6/s/Dw2PbnDlzsuUov6ampkd5ebk7ZJfIIV9OmY3xQiRrz+rVq70vX758GGfXHj162Pn5+TE7O+WbVF1dzX7++Wd2/vz5mrCwsGMBAQHDQLYyyRoKQeHh4ddwicjJybkhpVwlZDVpTYaXesbLyyt406ZNzNXVVQm8BMsoKiqyS0lJ6Z+fn5+JhFGCiS2MdHBwmHT69OmCxvgRWVjVBsmV/9k3qIK4gKVLl84+c+bMB2lpaczJyUmcEBlylZSUsMmTJ9fExsaOnDt37l4ZiqgTGRkZ6V1VVTUI2m1HXaAN3tjbYJ3MqtKdO3dGxcXF2RTBqOIYl7G+ffvaQatNMKshIhOh+5yDbjoX2aNFilAsm6Ld5fDhw1sUFhaOxCA2Ai3shKuD2Jbevn07AtpCbHZZ8wUFBbHMzMxnQQTRmgzdYjXwuYnr4eDg4B04qrSVTk5Otk9PT2+H5wHQYhe14bZ6VYxkUO0RIMZGAJGPMxNnFsYZdcDh2aLDxcWlB7omf4syKZSYukyMEa+ifWvEFolu0BEE64z8f8rLy5sF/F7Ozs4mzcVAsmpc5tN9UzgUIVnPnj1DoNrTAdp0jKN2SQEMuspjFy5ciId2lEKcpDJQL+bj47MHpEi3UvBu5F8KjfgaSHcE127QXIVWylQ8uyJjMqj9DWjZEqkIRij5+vr+FS+x6ptvvlEcNKECjx8/zq5evVoBki0TSmdJHIi1Dj/QjThXWpLPVtLKPruEpboLGnsQBPMDSDVSNvydd95ZeOjQoSXx8fH2GGwzf//G6T3xI9LYyTIyMhjGStXQsq/Omzdvk5RtjYmJcUY3fDskJMQb47OK2pnlRG9v77QDBw7cl7IsqWUpQbLxeAkT8Gt8XurKk7xVq1ZFX7x4cRO0R0BBQQHZMmRvk3476McDzVoRGBiYFxoaOiYpKelb/TRSPKO7POvo6JgAe1lOVFSUJ7rQ9cC2j729/Uwpewkp6qorQ/YxGV4Arbdd1y1UyvvExMQsyOsopUwblnUDpPKl+oFoxbiMwXi3D64BFGarh+wks9WGq6Ve0GAnbL0tigz8bR0EXj95EeAkkxdfLh0IcJJxGsiOACeZ7BDzAjjJOAdkR4CTTHaIeQGcZJwDsiPASSY7xLwATjLOAdkR4CSTHWJeACcZ54DsCHCSyQ4xL4CTjHNAdgQ4yWSHmBfAScY5IDsCnGSyQ8wL4CTjHJAdAU4y2SHmBajS/Ro7pH6AL3wIvV7sMcg7e/Zsc9kDYJOMViXJtAQjxHXv6RkEvISw39G9FAdIfBkkDpVCllplqJJkQi+LE0IIHXniVDkmoy5SC5fuvTaMX5VFQJWajI/BlCWRqdJUqclMNZrHK4sAJ5myeDfL0jjJmuVrV7bRnGTK4t0sS1PlwJ8bY22Ly6rUZLoGWN1724K++dRGlZpM6PVxi78QOvLEqZJkZIDVajB9Yyy3+MtDJCGpqiQZN8YKvXLl41Q5JlMeRl6iEAKcZELo8DhJEOAkkwRGLkQIAU4yIXR4nCQIqHLgz42xknBDMiGq1GRa8wWhpHsvGWpckEUIqFKTCSHAjbFC6MgTp0qScWOsPGQRK1WVJOPGWLF0kCefKsdk8kDFpYpFgJNMLHI8n9kIcJKZDRVPKBYBTjKxyPF8ZiPASWY2VDyhWAQ4ycQix/OZjQAnmdlQ8YRiEeAkE4scz2c2ApxkZkPFE4pFgJNMLHKNkA/LZeVY8C9vhKKtKpKTzCr4lM0cHBw8Ef9zPEPZUq0vjZPMegwVk7Bjx44KxQqTsCBOMgnB5KIMI6BKLwzDTW16ocnJyfZ79+7tbm9vH4Ta+2BM5teiRYu2Dx8+vIax2XWEFeCaC6+Ty7bcOpslWXl5+ZNVVVWSfdu1sV6Cq6vrMZCjyNzyY2Ji3EtLS4ch37gDBw4M9fPzq8HJfHx8nNq1a+fi5uZmd/fu3eqbN2+W4azMz8937N+//31g9RnIt8vd3f1IZmbmI3PLUyKdzZKssrJyAn6lyUqAIGcZDx48iID8HFNlgFzOSJuIdi/q1q1bzaBBg1r27t2btW7d2lBWGua4aSN++ukn9xMnTkw9ePBgPIj3AN6/f8YEYTvIXaNN05hXmyVZY4KidNkgxRSQa1nPnj2dp0+f7hYYGGhRFYKCghhOhwkTJnhduHDBa+XKlWug3d6G3BnoSg9aJEyGxJxkMoBqrkhoKhcnJ6dt0FaD586d69a5c2dzsxpN17VrV/bRRx95QrN5vv/++zujo6P/fvz48cWNqdX47NLo65I3Alor0MHB4avu3bvHrVmzRhKC6da4T58+bOPGjR6wrc0aMGBAelxcXF33qptOiXs7uQsBmNMwtgqH2k6wpCzkcccYxcOSPLaYFgP4QmiRSt26AZOQxx57LGfs2LEekydPdtCNk/r+0aNHbPny5WVHjhy5grpEYDJxX+oyTMmz2e4SL6YUladTVQe6Lw+YJA5NnDjRE2Mo2XsSR0dHhq7YBXgGHT169BOAOUJpQGVvpNINsuXyoJ3t0EXuhCbzVYJguljMmjXLOSAgYAAmBMm64Urc26wmU6LxSpeBF/zXtm3bRi9YsKCFqbKXLFnCvv76a1PJ6uJHjx7NQNy6Z/0b0mjvvfeeG7rnNyIjI7/Mzs4+rJ9GrmdOMrmQ1ZPbr1+/NjBTLIAV3xlWe73Y+o+5ubns8OHD7JVXXiHTRP1IA0+nTp1i27ZtYyNHjmQw1hpI8WtQy5Yt2fz5891SUlLWQqs+qdSMU1UkKy4ufhHAtTGKsswReHFfenp6XjJSzN9gcLXr0KGDkejfgjEr1JBr5syZ9K8Uf4swcoeZIwOJ2e7duxnGekZS/RoMLcawguCD61iE0BhN9kNVJMML+QtedLjsqBkpAOXTDLoBychcgVne5ClTpgirMGQmLXby5EmWlpZmFsGoKqQZk5KS2IYNG9ioUaOYi4sLBRs9ZsyY4YYuezlmtzvh2VFlNKFEEXzgLxGQQmJgrpg6cOBAe6w9CiXTxJEWwwCdhYWFmUyrm2DMmDGsqKiI7dmzRzfY4D0ZbFGGZ15e3mCDCSQO5CSTGFBD4kCyl2AQdTIUpxum1WKLFy82W4tp8zs7O7PExES2fft2BucCbbDR6+DBg90xRos3mkDCCE4yCcE0JCoqKqpdWVlZ8DPPPGMoul4YaTFat8QCeb1wcx/GjRvH7t27xz7//HOTWXr16sXgufG8yYQSJFDVmMzDw4M8HmzqqK6uHh4eHl6JcZOgJtNqsbVr1zIYa0W1gcZiWGDXzDRfeOEFBg1qVA6RGeldULfQnJycBuNIoxlFRIhrjYiCmmsWLOWEhYaGuppqP2kxf39/hrVMU0kF4+Pj49kvv/zC0tPTBdNR5BNPPGEHQj9tMqGVCTjJrATQVHYYQYO8vb0Fk2m12KJFi0RrMW0BIDVLSEhgW7duZbDLaYMNXuEISbNdH4OREgZykkkIpiFR0BT+pkhGWgwvnKHrMiTC4jCylRUWFrL9+/cL5m3VqpUDjLfBgokkiOQkkwBEIREYXLf28vIymkSrxcjOhfEbI68JOunekkM3L43FYANjW7Zs0cgyJofqhbFie2PxUoWrauAvFShSykF3WYLZpVGRRDI6Fi5cWC9N+/btGQylDAvq9cINPVRUVLBhw4YxrHg0iCbbmTFNir0ENMO81yCTxAGcZBIDakDc9Tt37pi0SezcuZPReIoO+NGx8ePHa2aJppaJsMLBVqxYwcir9q233qor/vvvv9fYzeoCDNxgQ0oNiPaDgShJgzjJJIWzoTBoinya7Zk6aGZJnhLaIzU1VUOS2NhYJrRSQJqQNN6hQ4cYTDja7GZdb926VYH6XTMrsRWJ+JjMCvDMyQpN8d3169ctXh8kYylcgzRrksbGZzR7JCv/smXLLCYY1R27nKqw3ppvTjusSfPbT8caKTyvUQSgKb7Apo53kUB41VpPAnlfkE8Zln/YvHnzyKall4Ix7ExiHTt2ZPDuaBBnKoBWBm7cuOEAr5DT8C0zldyqeK7JrILPdGba3Q2NU4rFaNOJ9VKQb9i6desMEoyS0kI3aTEipKUH+aChez2pxPc1uCaz9O2ISI9tb+nQZq+GhIRYnLtLly6MTqmPrKyscgz8/yO1XEPyOMkMoSJxGMwI/8SiNVZ84l10B/e6xbz55psavzDdMGvub9++bTR7QUEBww5zit9tNJGEEZxkEoJpTBRe6Bl4r57dt29fP3KR1j1o5kifI8B3LDSnbpw19+TISHINLZKvX7++AvEfQrvesaYMc/NKRjIsibTEor49divfNbfw5pQOtrIZ8FzNHjp0KFy/nOuaDq9ZRqdSBz7Qwo4dO1YJn7O3tWXC1mYHd2xaKO+BmSxt2XNDGG0WoMFeHgzCuQjLhceuaVuMVqjO1fIRIzIToTDY/D1uR+N8ChXyx7UGYdW4p/UQGuWeQ9eQhkqHIczizb3Ir7oDNq+9w3FMnTrVtBlfhtaTKWT27NnlMNQuwSpBGpav/oB31h9F9cU7KsP9WVxpBeA+7u/jniaGHXF2qr3exPUTxG3BhOYr3Jt1WKTJ8AGPSEhdhDMGFfgS1x1gOPnVX4VPkubzSH379n2cnPQoDRrxMa7OqNQ5XJv9ATwmYWx2qVOnTu3gKas4HvgcwiPYxi5jRSEY7yQXJ3k37sQ7nAUzxo9CFcIuK/pWGqnc8Tj3gwtFyP/BiBEj1iGOFIvRwyxNhu8qdADz3wWZBkPwElw/1pLKqGREIJ0duoJUXAORnnbHNPsDnrJPo7vMxqcD3AzZvuQCKCMjQ/O5AhCMfLP/hXMV3skNMeVh8d3hypUrsdCMZP+jIxGaLevX24Z/SR0KHiDJGxiUnkeiqzg7QdhqcwhGQkFI6kIv4RTVl5MMtR2nT5/+Fj/YSfh0QNmPPwoqD8maTrufQOr76GESYBtrj/e3UCzBqFK0wwma77/PPfdcBBQI7eHcDc22DuQz6IprVJNh3OUKcqRBQHfIfRGVEuWiC5KK+uCKZAjbqCB8E+MldFPrsTXNFUMM2WoJd5+qzZs3F4HYcSB4jhwFYRbbCvJ3kFLBJGEUyqnnDmJQk4Fg7ZGB1J8rBu9RYglGDYKcKyCq6R2tcrTehmXCGLodM7xofDqgEE6LVeRDJuVRUlLCMFZ6iN1L34EAT8tFMKozzTphaB6O20Isox0Ff3x129JAk9HuGjQ4C7+yT6ESFxA7dTNYeo8CWyPPd1DTvrb2LVNL2yJHevp8AX7Iu7AqED5t2jSXIUOGiFom0taNtsPt2rWrGtqrAnI34wN705VYOqLyoUxoDL4CtwPRnkiQT+NIV49kIJgnmJiJRFkYeyXiKsmB/no3KnAWGjFFEoEqFIKX0x8/xH/g7IiNui70Ebs2bdqY3VLMGhm0I42XyBRxGAvgrwNv2X3F9CtYSzTy+84HhxIovo5kNGjDIu4XqGABDNQTrdVguoUDQB8UTnaVFMj+UErZuuWo4R4/yOHwvU+Ci1B/Wg2ApnOlr1+Tdyud+Lq1Zm8ljLuaXUkgVzW+O4a52cMHsO6nw3ctFeS60JhY4AfSFl30ebzn1/G+P6kjGRq3DBXrBTLEopLC21xEtAAW5WBMeTciqyfOTJRzGV2yoH1FRDFNMUsGXgQZr+sdtWaCPiDVSJg8QtHD+GEYQ18GcsOSUBG6wpvA8Bq6x3OYNX5Gs9Z6Ahr5AXwagipsxQy0rYZkIEBvNGI/Kt8d61lX5KofGfSwfhcHcGg6RWO1OpLLVaaty8Wv/d8g2Qlbr6eY+pFGA59u2dEXmGtV2zL0oWlihPE8HAEhBOyhfhPwa7rECSYEE4+zBgFHdJFpmG7SMgM/OAIcAY4AR4AjwBHgCHAEOAIcAY4AR4AjwBHgCHAEOALNFYFG2dDQXMFuSu3GfpcWvr6+rbC76YG19bZoI4m1hfH8jY8APGI6Y+34jzgDUJtzWO35DKs9F/Vrhi/+rEbckwiP0Y+z9LnZL1BbClhTTg/PiC4g10m0wV2vHafwnAZftk/x8ZZifNT4RaTbCpK9BgJu0ktr8SMnmcWQNd0MIBltgRsCAq2FL34mPG+i8RyP07+2VeQFTfsuH6+9+sPty+rukpOsFl21X2r3wxbChy+RHEe17SW/NeyaGornl3E+CwJ6QIPRN0YnQYvJ+00pbSX4VR0IYHeUH/ZbJAn9L3IQzI5c8KVu8f8BlXi3/G0ghtIAAAAASUVORK5CYII=";
    //
    //
    // static each<T extends {}>(obj: T, iteratee?: (element: any, key?: string)=>boolean): T {
    //     if (obj) {
    //         Object.keys(obj).some((key)=>{
    //             var value = obj[key];
    //             return iteratee(value, key) === false;
    //         });
    //     }
    //     return obj;
    // }
    //
    // static each<T>(obj:T[], iteratee:(v:T, idx:number)=>boolean):T[] {
    //     obj.some((o,i)=>iteratee(o, i) === false);
    //     return obj;
    // }

    static bytesToSize(bytes) {
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes === 0) return '0 Byte';
        const i: number = Math.floor(Math.log(bytes) / Math.log(1024));
        const num: number = Math.round(bytes / Math.pow(1024, i));
        return `${num} ${sizes[i]}`;
    }

    static JSONParse(value: string) {
        if (!value) return undefined;
        if (typeof value === "string") {
            if ((value.startsWith("{") && value.endsWith("}")) ||
                (value.startsWith("[") && value.endsWith("]")) ||
                (value.startsWith("\"") && value.endsWith("\""))) {
                try {
                    return JSON.parse(value);
                } catch (error) {

                }
            } else {
                return value
            }
        } else {
            return value;
        }
    }

    /**
     * Check flag in enum or not
     * 
     * @static
     * @param {number} value 
     * @param {number} flag 
     * @returns 
     * @memberof AkUtil
     */
    static enumFlagContians(value: number, flag: number) {
        if (!value) return false;
        return (value & flag) === flag;
    }

    /**
     * To Boolean
     * 
     * @static
     * @param {any} value 
     * @returns 
     * @memberof AkUtil
     */
    static toBoolean(value) {
        if (typeof value === "string") {
            switch (value.toLowerCase().trim()) {
                case "true":
                case "1":
                    return true;
                case "false":
                case "0":
                case null:
                    return false;
                default:
                    return Boolean(value);
            }
        } else if (typeof value === "number") {
            return value > 0
        } else {
            return value;
        }
    }

    static endsWith(string, target, position?) {
        return endsWith(string, target, position);
    }


    static intersperse(arr, sep) {
        if (arr.length === 0) {
            return [];
        }

        return arr.slice(1).reduce((xs, x, i) => xs.concat([sep, x]), [arr[0]]);
    }

    static getPopupContainer(trigger: Element): HTMLElement {
        return (trigger.closest(".ant-modal-content") || document.body) as HTMLElement;
    };
    /**
     * 将html内容转换为纯文本
     * @param html 
     */
    static htmlToText(html) {
        if (!html) {
            return "";
        }
        return html.replace(/<\/?.+?>/g, "")
            .replace(/ /g, "")
            .replace(/&lt;/g, "<")
            .replace(/&gt;/g, ">")
            .replace(/&amp;/g, "&")
            .replace(/&quot;/g, '"')
            .replace(/&apos;/g, "'")
            .replace(/&nbsp;/g, " ");
    }

    /**
     * 将timeout方法转化成异步执行
     * @param func 
     * @param duration 
     */
    static async timeoutToPromise(func, duration) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                try {
                    if (func && typeof func === "function") {
                        resolve(func());
                    } else {
                        resolve(func);
                    }
                } catch (ex) {
                    reject(ex);
                }
            }, duration);
        });
    }

    /**
     * 转化数字成千分位显示
     * @param num 
     */
    static parseThousands(num: number) {
        // let newstr = num + "";
        // return newstr.replace(/(?<=\d)(?<!\.\d*)(?=(\d{3})+(\.|$))/g, ',');
        let src = num + "";
        let part1 = src;
        let part2 = "";
        let pos = src.indexOf('.');
        if (pos > 0) {
            part1 = src.substr(0, pos);
            part2 = src.substr(pos);
        }
        let result = "";
        while (part1.length > 3) {
            result = ',' + part1.slice(-3) + result;
            part1 = part1.slice(0, part1.length - 3);
        }
        if (part1) { result = part1 + result; }
        result += part2;
        return result;

        //return part1.replace(/(\d)(?=(\d{3}))/g, "$1,") + part2;
        // if (newstr.indexOf('.') > 0) {
        //     return newstr.replace(/(?<=\d)(?<!\.\d*)(?=(\d{3})+(\.|$))/g, '$1,');
        // } else {
        //     return newstr.replace(/(\d)(?=(\d{3}))/g, ",");
        // }
    }

    static isHomePage() {
        return window.location.hash === "/" || window.location.hash === "" || window.location.hash === "#/";
    }

    static isCustomHomePage() {
        if (AkUtil.getQueryString("hideSystemActions") || AkUtil.getQueryString("hidesystemactions")) {
            return true
        } else {
            return false
        }
    }
}