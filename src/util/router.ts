import * as React from "react";
import { LinkProps, RouteProps, RouterProps, IndexRouteProps, IndexRoute } from 'react-router';
import {
    Router,
    Route,
    Link
} from "react-router";

export var AkRouter: React.Component<RouterProps, any> | any = Router;

export var AkRoute: React.Component<RouteProps, any> | any = Route;

export var AkIndexRoute: React.Component<IndexRouteProps, any> | any = IndexRoute;

export var AkLink: React.Component<LinkProps, any> | any = Link;