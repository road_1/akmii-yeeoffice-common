import { AkUtil } from './util';
import { AkResponse, AkContext, AppKeys } from './common';
import { AkFileIcon } from './ak-file-icon';
import * as SparkMD5 from 'spark-md5';
import * as superagent from 'superagent';
import { PostFileBegin, FileUploadStatus, FileRequest, AkUploadFile, DocumentModel, DocumentCancelUploadRequest, DocumentChunkUploadRequest } from '../api/common/fileuploadmodel';
import { FileUpLoadAPI } from '../api/common/fileupload';
import { Progress, message } from 'antd';

export class FileUpLoadCommon {
    static DefaultPageSize = 20;
    static DefaultFirstPageIndex = 1;
    public static KbSize: number = 1024;
    public static MbSize: number = FileUpLoadCommon.KbSize * 1024;
    public static GbSize: number = FileUpLoadCommon.MbSize * 1024;
    public static _defaultChunkFileSize: number = 2 * 1024 * 1024;

    static isMobile(): boolean {
        return window.document.body.clientWidth < 768;
    }
    static generateGuid(): string {
        let S4 = () => {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        }
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    }
    static getFileSize(size: any): string {
        if (size == null || size == "") return "";

        let sizeNumber = parseInt(size);
        if (sizeNumber >= FileUpLoadCommon.GbSize) {
            return (Number(sizeNumber / FileUpLoadCommon.GbSize)).toFixed(2) + 'GB';
        } else if (sizeNumber >= FileUpLoadCommon.MbSize) {
            return (Number(sizeNumber / FileUpLoadCommon.MbSize)).toFixed(2) + 'MB';
        } else if (sizeNumber >= FileUpLoadCommon.KbSize) {
            return (Number(sizeNumber / FileUpLoadCommon.KbSize)).toFixed(2) + 'KB';
        } else {
            return (Number(sizeNumber)).toFixed(2) + 'B';
        }
    }
    static getIconUrl(Extension: string, isFolder: boolean) {
        if (isFolder) return AkFileIcon.FOLDER;
        switch (Extension) {
            case "":
                return AkFileIcon.FOLDER;
            case "ppt":
            case "pptx":
                return AkFileIcon.PPT;
            case "doc":
            case "docx":
                return AkFileIcon.Word;
            case "xls":
            case "xlsx":
                return AkFileIcon.Excel;
            case "vsdx":
            case "vsd":
                return AkFileIcon.VISO;
            case "pdf":
                return AkFileIcon.PDF;
            case "txt":
                return AkFileIcon.TXT;
            case "zip":
            case "rar":
            case "7z":
            case "cab":
            case "iso":
                return AkFileIcon.ZIP;
            case "mp4":
            case "webm":
            case "ogg":
                return AkFileIcon.Meida;
            case "bmp":
            case "png":
            case "jpg":
            case "gif":
            case "jpeg":
            case "icon":
                return AkFileIcon.IMG;
            default:
                return AkFileIcon.FILE;
        }
    }
    static getDownloadUrl(item: DocumentModel) {
        let url: string = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net) + "/api/document/download";
        let appid = item.AppID;
        url = url + `?appID=${appid}&libraryID=${item.LibraryID}&listDataID=${item.ListDataID}&mD5=${item.MD5}&name=${item.Name}&extension=${item.Extension}`;
        return url;
    }
    static isMediaFile(extension: string) {
        let canViewInBrowserFiles = ["png", "jpg", "gif", "jpeg", "icon", "bmp", "mp3", "mp4", "WebM", "Ogg"];
        return canViewInBrowserFiles.indexOf(extension.toLowerCase()) >= 0;
    }
    static isImageFile(extension: string) {
        let canViewInBrowserFiles = ["png", "jpg", "gif", "jpeg", "icon", "bmp"];
        return canViewInBrowserFiles.indexOf(extension.toLowerCase()) >= 0;
    }
    static isPdfFile(extension: string) {
        let canViewInBrowserFiles = ["pdf"];
        return canViewInBrowserFiles.indexOf(extension.toLowerCase()) >= 0;
    }
    static isTextFile(extension: string) {
        let canViewInBrowserFiles = ["txt"];
        return canViewInBrowserFiles.indexOf(extension.toLowerCase()) >= 0;
    }
    static isOfficeFile(extension: string) {
        let canViewInBrowserFiles = ["doc", "xls", "ppt", "docx", "xlsx", "pptx"];
        return canViewInBrowserFiles.indexOf(extension.toLowerCase()) >= 0;
    }
    static validateFileName(fileName: string): boolean {
        let hasInvalidChars = false;
        if (fileName.indexOf("~") >= 0 ||
            fileName.indexOf("#") >= 0 ||
            fileName.indexOf("%") >= 0 ||
            fileName.indexOf("&") >= 0 ||
            fileName.indexOf("*") >= 0 ||
            fileName.indexOf("{") >= 0 ||
            fileName.indexOf("}") >= 0 ||
            fileName.indexOf("'") >= 0 ||
            fileName.indexOf("\\") >= 0 ||
            fileName.indexOf(":") >= 0 ||
            fileName.indexOf("<") >= 0 ||
            fileName.indexOf(">") >= 0 ||
            fileName.indexOf("?") >= 0 ||
            fileName.indexOf("/") >= 0 ||
            fileName.indexOf("|") >= 0 ||
            fileName.indexOf("\"") >= 0) {
            hasInvalidChars = true;
        }

        let validatePassed = hasInvalidChars == false;
        return validatePassed;
    }
    static validateFileSize(options: AkUploadFile): boolean {
        let lessThen2Gb = options.file.size < (FileUpLoadCommon.GbSize * 2);
        let validatePassed = lessThen2Gb === true;
        return validatePassed;
    }
    static getFileExtension(fileName: string): string {
        return '.' + fileName.split('.').pop();
    }
    static getFileNameWithOutExtension(fileName: string): string {
        let extension = this.getFileExtension(fileName);
        return fileName.replace(extension, "");
    }
    static validateFileNameSize(fileName: string): boolean {
        let moreThan200Chars = false;
        if (this.getFileNameWithOutExtension(fileName).length > 200) {
            moreThan200Chars = true;
        }
        let validatePassed = moreThan200Chars === false;
        return validatePassed;
    }
    static async getUpload(options: AkUploadFile): Promise<AkResponse> {
        return new Promise((resolve, reject) => {
            try {
                let file = options.file;
                let name = file.name; //文件名
                let size = file.size; //总大小
                let shardCount = Math.ceil(size / this._defaultChunkFileSize); //总片数
                let currentChunk = 0;//计算MD5的index
                const fileReader = new FileReader();
                const blobSlice = File.prototype.slice;
                const spark = new SparkMD5.ArrayBuffer();
                let response: AkResponse = {};

                fileReader.onload = e => {
                    spark.append(e.target["result"]);
                    currentChunk++;
                    if (currentChunk < shardCount) {
                        loadNext();
                    } else {
                        var md5 = spark.end();
                        let fileRequest: PostFileBegin = {
                            MD5: md5,
                            FileName: name,
                            FileExtension: AkUtil.getFileExt(name),
                            Length: size,
                            ChunkSize: shardCount
                        }
                        FileUpLoadAPI.postFileBegin(fileRequest).then(d => {
                            if (d.Status === 0) {
                                options.MD5 = md5;
                                options.fileID = d.Data.FileID;
                                options.currentChunk = shardCount;

                                response.Status = 0;
                                response.Data = options;
                                response.Message = d.Data.Progress;
                                resolve(response);
                            } else {
                                response.Status = d.Status;
                                response.Data = options;
                                response.Message = d.Message;
                                reject(d.Message);
                            }
                        })
                    }
                }
                let loadNext = () => {
                    var start = currentChunk * FileUpLoadCommon._defaultChunkFileSize,
                        end = start + this._defaultChunkFileSize >= file.size ? file.size : start + this._defaultChunkFileSize;
                    fileReader.readAsArrayBuffer(blobSlice.call(file, start, end));
                }
                loadNext();
            } catch (ex) {
                reject(ex.message);
            }
        })
    }
    static upload(options: AkUploadFile): Promise<AkResponse> {
        let response: AkResponse = {};
        return new Promise((resolve, reject) => {
            try {
                this.getUpload(options).then((d: AkResponse) => {
                    if (d.Message === "Success") {
                        response.Status = FileUploadStatus.Uploaded;
                        response.Message = "uploaded success";
                        response.TotalCount = 100;
                        resolve(response);
                    }
                    else if (d.Message === "Ready") {
                        //小于20M 为 小文件
                        if (options.file.size <= 1024 * 1024 * 20) {
                            this.uploadSmallFile(options).then(onfulfilled => {
                                resolve(onfulfilled);
                            }, onrejected => {
                                reject(onrejected);
                            });
                        } else {
                            this.uploadLargeFile(options).then(onfulfilled => {
                                resolve(onfulfilled);
                            }, onrejected => {
                                reject(onrejected);
                            });
                        }
                    } else {
                        this.directfinishUpload(options).then(onfulfilled => { resolve(onfulfilled); }, onrejected => {
                            reject(onrejected);
                        });
                    }
                });
            }
            catch (err) {
                if (err instanceof CancelUploadingError) {
                    let cancelUploadingError = err as CancelUploadingError;
                    return this.handCancelUploadingError(cancelUploadingError);
                }
                return err as AkResponse;
            }
        });
    }
    static handCancelUploadingError(cancelUploadingError: CancelUploadingError) {
        let cancelRequest: DocumentCancelUploadRequest = {
            item: cancelUploadingError.item,
            uploaded: cancelUploadingError.uploaded
        }
        return this.cancelUploadFile(cancelRequest);
    }
    //小文件上传
    static uploadSmallFile(options: AkUploadFile): Promise<AkResponse> {
        let response: AkResponse = {};
        return new Promise((resolve, reject) => {
            let start = 0;
            let end = options.file.size;
            const blobSlice = File.prototype.slice;
            var fileForm = new FormData();
            fileForm.append("file", blobSlice.call(options.file, start, end));
            const requestFiles = superagent.post(AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net) + "/api/file/upload/small?fileID=" + options.fileID);
            requestFiles.send(fileForm).on("progress", function (e) {
                let progress = (options as any).onProgress;
                if (progress) {
                    progress({ percent: e.percent });
                }
            });
            if (AkContext.getBranch() !== AkContext.YeeOffice) {
                requestFiles.set("AkmiiSecret", AkContext.getToken());
            }
            requestFiles.end((err, res) => {
                if (res.ok && res.body.Status === 0) {
                    options.progress = 100;
                    response.Status = FileUploadStatus.Uploaded;
                    response.Message = "uploaded success";
                    response.TotalCount = 100;
                    resolve(response);
                } else {
                    response.Status = FileUploadStatus.Error;
                    // response.Message = "upload error:" + err;
                    response.Data = "upload error:" + err;
                    reject(response);
                }
            });
        })
    }

    static async uploadLargeFile(options: AkUploadFile): Promise<AkResponse> {

        let chunkUploadRequest: DocumentChunkUploadRequest = {
            item: options,
            uploadedFileSize: 0,
        };
        await this.beginUploadLargeFile(chunkUploadRequest);

        while ((chunkUploadRequest.uploadedFileSize + this._defaultChunkFileSize) < options.file.size) {
            await this.continueUploadLargeFile(chunkUploadRequest);
        }
        return this.finishUploadLargeFile(chunkUploadRequest);
    }
    static beginUploadLargeFile(request: DocumentChunkUploadRequest): Promise<AkResponse> {
        return new Promise((resolve, reject) => {
            let start = 0;
            let end = request.uploadedFileSize + this._defaultChunkFileSize;
            request.uploadedFileSize = end;
            const blobSlice = File.prototype.slice;
            var fileForm = new FormData();
            fileForm.append("file", blobSlice.call(request.item.file, start, end));
            const requestFiles = superagent.post(AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net) + "/api/file/upload/chunk?fileID=" + request.item.fileID);
            if (AkContext.getBranch() !== AkContext.YeeOffice) {
                requestFiles.set("AkmiiSecret", AkContext.getToken());
            }
            requestFiles.send(fileForm);
            let response: AkResponse = {};
            requestFiles.end((err, res) => {
                if (res.ok && res.body.Status === 0) {
                    if (request.item.canceledUpload === true) {
                        response.Status = FileUploadStatus.Canceled;
                        response.Message = "canceled Upload";
                        response.Data = request.item;
                        reject(response);
                    }
                    response.Status = FileUploadStatus.Start;
                    response.Message = "begin upload large file";
                    response.Data = request.item;
                    response.TotalCount = (request.uploadedFileSize / request.item.file.size) * 100;
                    resolve(response);
                } else {
                    response.Status = FileUploadStatus.Error;
                    // response.Message = "upload error";
                    response.Data = "upload error:" + request.item;
                    reject(response);
                }
            });
        });
    }
    static continueUploadLargeFile(request: DocumentChunkUploadRequest): Promise<AkResponse> {
        return new Promise((resolve, reject) => {
            let start = request.uploadedFileSize;
            let end = request.uploadedFileSize + this._defaultChunkFileSize;
            request.uploadedFileSize = end;
            const blobSlice = File.prototype.slice;
            var fileForm = new FormData();
            fileForm.append("file", blobSlice.call(request.item.file, start, end));
            const requestFiles = superagent.post(AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net) + "/api/file/upload/chunk?fileID=" + request.item.fileID);
            requestFiles.send(fileForm).on("progress", function (e) {
                let progress = (request.item as any).onProgress;
                if (progress) {
                    progress({ percent: e.percent });
                }
            });
            if (AkContext.getBranch() !== AkContext.YeeOffice) {
                requestFiles.set("AkmiiSecret", AkContext.getToken());
            }

            let response: AkResponse = {};

            requestFiles.end((err, res) => {
                if (res.ok && res.body.Status === 0) {
                    if (request.item.canceledUpload === true) {
                        response.Status = FileUploadStatus.Canceled;
                        response.Message = "upload cancled";
                        response.Data = request.item;
                        reject(response);
                    }
                    response.Status = FileUploadStatus.Uploading;
                    response.Message = "uploading";
                    response.Data = request.item;
                    response.TotalCount = (request.uploadedFileSize / request.item.file.size) * 100;
                    resolve(response);
                } else {
                    response.Status = FileUploadStatus.Error;
                    // response.Message = "upload error:" + err;
                    response.Data = "upload error:" + err + "; item :" + request.item;
                    reject(response);
                }
            });
        });
    }
    /** 结束上传*/
    static directfinishUpload(options: AkUploadFile): Promise<AkResponse> {
        let response: AkResponse = {};
        return new Promise((resolve, reject) => {
            response.Status = FileUploadStatus.Uploaded;
            response.Message = "uploaded success";
            response.TotalCount = 100;
            resolve(response);
        })
    }
    static finishUploadLargeFile(request: DocumentChunkUploadRequest): Promise<AkResponse> {
        let response: AkResponse = {};

        return new Promise((resolve, reject) => {

            if (request.item.currentChunk === 1) {
                let FileID: FileRequest = {
                    fileID: request.item.fileID
                }
                FileUpLoadAPI.putFileEnd(FileID).then(d => {
                    if (d.Status === 0) {
                        response.Status = FileUploadStatus.Uploaded;
                        response.Message = "uploaded success";
                        response.TotalCount = 100;
                        resolve(response);
                    } else {
                        response.Status = FileUploadStatus.Error;
                        // response.Message = "upload error:" + d.Message;
                        response.Data = "upload error:" + d.Message;
                        reject(response);
                    }
                })
            } else {
                let start = request.uploadedFileSize;
                let end = request.item.file.size;
                request.uploadedFileSize = end;
                const blobSlice = File.prototype.slice;
                var fileForm = new FormData();
                fileForm.append("file", blobSlice.call(request.item.file, start, end));
                const requestFiles = superagent.post(AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net) + "/api/file/upload/chunk?fileID=" + request.item.fileID);
                if (AkContext.getBranch() !== AkContext.YeeOffice) {
                    requestFiles.set("AkmiiSecret", AkContext.getToken());
                }
                requestFiles.send(fileForm);
                requestFiles.end((err, res) => {
                    if (res.ok && res.body.Status === 0) {
                        if (request.item.canceledUpload === true) {
                            reject(new CancelUploadingError(request.item, true));
                        }
                        request.item.progress = 100;
                        let FileID: FileRequest = {
                            fileID: request.item.fileID
                        }
                        FileUpLoadAPI.putFileEnd(FileID).then(d => {
                            if (d.Status === 0) {
                                response.Status = FileUploadStatus.Uploaded;
                                response.Message = "uploaded success";
                                resolve(response);
                            } else {
                                response.Status = FileUploadStatus.Error;
                                // response.Message = "upload error";
                                response.Data = "upload error:" + d.Message;
                                reject(response);
                            }
                        })
                    } else {
                        response.Status = FileUploadStatus.Error;
                        // response.Message = "upload error:" + err;
                        response.Data = "upload error:" + err;
                        reject(Response);
                    }
                });
            }
        });
    }

    static cancelUploadFile(request: DocumentCancelUploadRequest): Promise<AkResponse> {
        let response: AkResponse = {};

        request.item.progress = request.item.progress - (request.item.progress * 0.3);
        return new Promise((resolve) => {
            let fileID: FileRequest = {
                fileID: request.item.fileID
            }
            FileUpLoadAPI.putFileCancel(fileID).then(d => {
                if (d.Status === 0) {
                    request.item.progress = 0;
                    response.Status = FileUploadStatus.Canceled;
                    response.TotalCount = 0;
                    response.Message = "canceled upload file";
                    resolve(response);
                }
            })
        });
    }
}

export class CancelUploadingError {
    item: AkUploadFile;
    uploaded: boolean;
    stack?: string;
    constructor(item: AkUploadFile, uploaded: boolean) {
        this.item = item;
        this.uploaded = uploaded;
    }
}
// CancelUploadingError.prototype = Object.create(Error.prototype);

export class UploadError {
    status: FileUploadStatus;
    message: string;
    stack?: string;
    constructor(status: FileUploadStatus, message: string) {
        this.status = status;
        this.message = message;
    }
}
// UploadError.prototype = Object.create(Error.prototype);
