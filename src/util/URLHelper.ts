import { AkContext, NavigationHelper, AppKeys } from './common';
/**
 * URL 管理类，管理所有2.0版本的URL
 */
export class URLHelper {
    /**
     * 初始化所有的URL，都被记录在window下
     */
    static init() {
        // window["FileServer_APIUrl"]
        window["CurrentAPIDome"] = "yeeoffice.com";
        window["MerchantBaseApiUrl"] = "https://merchantBaseAPI." + window["CurrentAPIDome"];
        window["ManagementApiUrl"] = "https://management." + window["CurrentAPIDome"];
        window["NewsApiUrl"] = "https://Newsapi." + window["CurrentAPIDome"];
        window["DocApiUrl"] = "https://document." + window["CurrentAPIDome"];
        window["WorkflowUrl"] = "Https://yeeofficewfapi." + window["CurrentAPIDome"] + "/_API/GetRole/";
        window["SurveyApiUrl"] = "https://survey." + window["CurrentAPIDome"];
        window["ExamApiUrl"] = "https://examapi." + window["CurrentAPIDome"];
        window["onlinevoteUrl"] = "https://onlinevote." + window["CurrentAPIDome"];
        window["ContactsUrl"] = "https://addressbook." + window["CurrentAPIDome"];
        window["QuickLinkUrl"] = "https://quicklinks." + window["CurrentAPIDome"];
        window["LoginURL"] = "https://login." + window["CurrentAPIDome"];
        window["KnowlegdeApiUrl"] = "https://knowlegde." + window["CurrentAPIDome"];
        window["WorktaskApiUrl"] = "https://worktaskapi." + window["CurrentAPIDome"];
        window["MemoApiUrl"] = "https://memo." + window["CurrentAPIDome"];
        window["ScheduleUrl"] = "https://schedule." + window["CurrentAPIDome"];
        window["FilecenterUrl"] = "https://filecenter." + window["CurrentAPIDome"] + "/";
        window["CorpChatHubUrl"] = "https://test1." + window["CurrentAPIDome"] + "/chatMessageHub/hubs";
        window["PortalApiUrl"] = "https://portalbase." + window["CurrentAPIDome"];

        window["CDNBackUp"] = "https://cdn.yungalaxy.com/";
        window["ResourceUrl"] = "https://cdn.yungalaxy.com/";
        
    }
    /**
     * 
     * @param appID APP的ID
     * @param pageUrl 页面URL
     */
    static GetDomainUrl(appID: number, pageUrl: string) {
        if (AkContext.getBranch() == AkContext.YeeOffice) {
            return pageUrl;
        }
        let domain = NavigationHelper.getInstallDomainByID(appID)
        return domain + "/" + pageUrl;
    }
    /**
     * 获取上传文件的路径
     * @param file 文件名
     */
    static GetFileUrl(file: string) {
        if (!file) {
            return file;
        }
        if (file.toLowerCase().startsWith("http") || file.toLowerCase().startsWith("//")) {
            return file;
        }
        if (file.toLowerCase().startsWith("uploads/")) {
            return window["FilecenterUrl"] + file;
        }
        if (file.toLowerCase().startsWith("images/")) {
            return window["CDN"] + file;
        }
        return file;
    }

    /**
 * 
 * @param welcomepage APP的ID
 * @param RelativeUrl 页面URL
 */
    static getNewAppJumpUrl(welcomepage: string, RelativeUrl: string) {
        let url = "";
        if (AkContext.getBranch() === AkContext.YeeOffice) {
            if (AkContext.getNetSPToken()) {
                url = window.location.origin + NavigationHelper.getNavigationUrl(AppKeys.YeeOfficeBase, "");
            } else {
                url = AkContext.getRootWebUrl() + NavigationHelper.getNavigationUrl(AppKeys.YeeOfficeBase, "");
            }
        } else {
            url = AkContext.getRootWebUrl() + '/SitePages/Pages/apps.aspx';
        }
        if (welcomepage) {
            if (welcomepage.startsWith("http") || welcomepage.startsWith("https")) {
                return NavigationHelper.parseURL(welcomepage)
            } else {
                if (welcomepage.startsWith('/')) welcomepage.slice(1, welcomepage.length);
                return url + `#/app/${RelativeUrl}/pages/${welcomepage}`
            }
        } else {
            return url + `#/app/${RelativeUrl}/pages/home`;
        }
    }
}
