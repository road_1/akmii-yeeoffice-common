
import * as moment from "moment-timezone";
import { AkDatetimeUtil } from "./datetime";

export class AkDateTimeZoneUtil {
    static getUserNowDate(): string {
        const baseInfo = AkDatetimeUtil.getBaseInfo();
        const format = baseInfo.DateFormat + " HH:mm";
        const userDate = moment().format(format);
        return userDate;
    }

    static getSeverNowDate(format?: string): string {
        const baseInfo = AkDatetimeUtil.getBaseInfo();
        const serverDate = moment().tz(baseInfo.ServerTimeZone).format(format || AkDatetimeUtil.defaultFormat)
        return serverDate;
    }
}