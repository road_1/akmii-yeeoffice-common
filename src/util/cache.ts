import { AkContext } from './common';
import { AppInfoList, MerchatLoginModel, UserModel, CompanyInfo, EnterpriseInfo, ClientInfo } from '../api/masterpage/masterpagemodel';
import { Cookies } from './cookie';
import { SSL_OP_NETSCAPE_CHALLENGE_BUG } from 'constants';
import { SupportModel } from '../index';
/**
 * 当前内存缓存对象模型
 */
export interface IAkLocalStorageData {
    /**
     * APP信息列表
     */
    AKAppInfo: AppInfoList[];
    /**
     * 登陆大缓存
     */
    AKTokenData: MerchatLoginModel;
    /**
     * SPOnline版本 登陆数据缓存
     */
    AKLoginHelperData: string[];
    AKCurrentBranch: string
    AKSupport:SupportModel
}

/**
 * 当前内存缓存对象模型
 */
export interface IAkCooikeData {

}
/**
 * 用于缓存处理
 */
export class CacheHelper {

    private static CacheTypeOf =null;
    static LocalStorageData: IAkLocalStorageData = {
        /**
         * APP信息列表
        */
        AKAppInfo: null,
        /**
         * 登陆大缓存
         */
        AKTokenData: null,
        /**
         * 支持缓存
         */
        AKSupport:null,
        /**
         * SPOnline版本 登陆数据缓存
         */
        AKLoginHelperData: null,
        AKCurrentBranch: null,
    }

    static CooikeData: IAkCooikeData = {
       

    }

    /**
     * 初始化缓存类型
     */
    static getCacheTypeOf() {
        this.CacheTypeOf = {
            AKAppInfo: "object",
            AKTokenData: "object",
            AKLoginHelperData: "object",
            AKCurrentBranch: "string",
            AkSiteAbsoluteUrl: "string",
            AKSupport: "object",
        };
        try {   
            var a = localStorage.getItem(AkContext.Keys.CacheType);
            this.CacheTypeOf = a?JSON.stringify(a):this.CacheTypeOf;    
        } catch (error) { 
        }
    }
    /**
     * 更新字段信息
     * @param key 
     * @param value 
     */
    static setCacheTypeOf(key: string, value: string) {
        if (!(key in this.CacheTypeOf) && this.CacheTypeOf[key] != value) {
            try {
                this.CacheTypeOf[key] = value;
                this.setLocalStorage(AkContext.Keys.CacheType,JSON.stringify(this.CacheTypeOf));
            } catch (error) {
                console.log("CacheHelper_setCacheTypeOf_error:" + error);
                console.info("浏览器不支持LocalStorage")
            }
        }
    }
    /**
     * 设置LocalStorage缓存
     * @param key key
     * @param value  value
     * @param cache  默认写本地缓存，true 不写入本地缓存
     */
    static setLocalStorage(key: string, value: any, nocache?: boolean) {
        !nocache?this.getCacheTypeOf():null;
        let data = value;
        let type = typeof value;
        try {
            if (type == "object") {
                data = JSON.stringify(value);
            }
            if (!nocache) {
                this.LocalStorageData[key] = value;
            }
            localStorage.setItem(key, data);
        } catch (error) {
            console.log("CacheHelper_setLocalStorage_error:" + error);
        }
        !nocache?this.setCacheTypeOf(key, type):null; 
    }
    /**
    * 设置Cooike
    * @param key key
    * @param value  value
    * @param options  options
    * @param cache  默认写本地缓存，true 不写入本地缓存
    */
    static setCooikes(key: string, value: any, options?: any, nocache?: boolean) {
        if(key==AkContext.Keys.LanguageKey|| key==AkContext.Keys.LoginSecretKey )
        {
            Cookies.set(key, value, options ? options : null);
        }
        !nocache?this.getCacheTypeOf():null;
        let data = value;
        let type = typeof value;
        try {
            if (type == "object") {
                data = JSON.stringify(value);
            }
            if (!nocache) {
                this.CooikeData[key] = value;
            }
            Cookies.set(key, data, options ? options : null);
        } catch (error) {
            console.log("CacheHelper_setCooikes_error:" + error);
        }
        !nocache?this.setCacheTypeOf(key, type):null;
    }

    /**
    * 读取LocalStorage
    * @param key key 
    * @param cache  默认走本地缓存 true 直接读取LocalStorage
    */
    static getLocalStorage(key: string, cache?: boolean): any {
        !cache?this.getCacheTypeOf():null;
        let r;
        try {
            if (!cache && key in this.LocalStorageData && this.LocalStorageData[key]) {
                return this.LocalStorageData[key];
            }
            var data = localStorage.getItem(key);
            //number，string，boolean，underfine，null，object
            switch (this.CacheTypeOf[key]) {
                case "string":
                    this.LocalStorageData[key] = data;
                    r = data;
                    break;
                case "object":
                    r = JSON.parse(data);
                    this.LocalStorageData[key] = r;
                    break;
                case "number":
                    r = Number(data);
                    this.LocalStorageData[key] = r;
                    break;
                case "boolean":
                    r = Boolean(data);
                    this.LocalStorageData[key] = r;
                    break;
                default:
                    r = data;
                    this.LocalStorageData[key] = r;
                    break;
            }
        } catch (error) {
            console.log("CacheHelper_getLocalStorage_error:" + error);
        }
        return r;
    }
    /**
    * 清除LocalStorage
    * @param key key 
    */
    static clearLocalStorage(key: string) {

        try {
            if (key in this.CacheTypeOf && this.CacheTypeOf[key]) {
                this.CacheTypeOf[key] = null;
            }
            if (key in this.LocalStorageData && this.LocalStorageData[key]) {
                this.LocalStorageData[key] = null;
            }
            localStorage.removeItem(key);
        } catch (error) {
            console.log("CacheHelper_clearLocalStorage_error:" + error);
        }
    }

    /**
    * 读取Cooike
    * @param key key 
    * @param cache  默认读取本地缓存 true 直接读取cooike
    */
    static getCooike(key: string, cache?: boolean): any {
        if(key==AkContext.Keys.LanguageKey|| key==AkContext.Keys.LoginSecretKey )
        {
           return Cookies.get(key);
        }
        !cache?this.getCacheTypeOf():null;
        let r;
        try {
            if (!cache && key in this.CooikeData && this.CooikeData[key]) {
                return this.CooikeData[key];
            }
            var data = Cookies.get(key);
            //number，string，boolean，underfine，null，object
            switch (this.CacheTypeOf[key]) {
                case "string":
                    this.CooikeData[key] = data;
                    r = data;
                    break;
                case "object":
                    r = JSON.parse(data);
                    this.CooikeData[key] = r;
                    break;
                case "number":
                    r = Number(data);
                    this.CooikeData[key] = r;
                    break;
                case "boolean":
                    r = Boolean(data);
                    this.CooikeData[key] = r;
                    break;
                default:
                    r = data;
                    this.CooikeData[key] = r;
                    break;
            }
        } catch (error) {
            console.log("CacheHelper_getCooike_error:" + error);
        }
        return r;
    }
    /**
   * 清楚cooike以及缓存
   * @param key key 
   */
    static clearCooike(key: string) {
        try {
            if (key in this.CooikeData && this.CooikeData[key]) {
                this.CooikeData[key] = null;
            }
            if (key in this.CacheTypeOf && this.CacheTypeOf[key]) {
                this.CacheTypeOf[key] = null;
            }
            Cookies.remove(key);
        } catch (error) {
            console.log("CacheHelper_clearCooike_error:" + error);
        }
    }

    /**
   * 是否要在切换站点集刷新缓存,只能用户SPOnline版本
   */
    static isRefreshCache() {
        var url = window["_spPageContextInfo"].siteAbsoluteUrl;
        var siteUrl = this.getLocalStorage(AkContext.Keys.SiteAbsoluteUrl);
        if (!siteUrl || siteUrl != url) {
            this.clearAll();
            this.setLocalStorage(AkContext.Keys.SiteAbsoluteUrl, url);
        }
    }
    /**
    * 清除所有缓存
    */
    static clearAll() {
        this.clearCooike(AkContext.Keys.LoginSecretKey);
        this.LocalStorageData.AKLoginHelperData = null;
        this.LocalStorageData.AKCurrentBranch = null;
        this.clearLocalStorage(AkContext.Keys.LoginHerperKey);
        this.clearLocalStorage(AkContext.Keys.BranchKey);
        this.clearAppinfoList();
        this.clearMersetchatData(); 
    }
    /**
     * 清除APPInfo缓存
     */
    static clearAppinfoList() {
        this.LocalStorageData.AKAppInfo = null;
        this.clearLocalStorage(AkContext.Keys.AppInfoKey);
    }
    /**
    * 清除登录缓存以及用户、公司缓存
    */
    static clearMersetchatData() {
        this.LocalStorageData.AKTokenData = null;
        this.clearLocalStorage(AkContext.Keys.TokenDataKey);
        this.clearCooike(AkContext.Keys.LoginSecretKey);
    }
    /**
     * 判断Token是否存在
     */
    static isAuthCookiesExpiration() {
        if (AkContext.getBranch() === AkContext.YeeOffice) {
            return true;
        }
        let LoginSecret = this.getCooike(AkContext.Keys.LoginSecretKey, true);
        if (LoginSecret) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * 写入Cooike 信息，Token 不需要走缓存
     * @param data 
     */
    static setAuthCookies(data: MerchatLoginModel) {
        var secretExpiresDate = new Date();
        secretExpiresDate.setTime(secretExpiresDate.getTime() + ((data.LiveSeconds || 300) * 1000));
        this.setLocalStorage(AkContext.Keys.TokenDataKey, data);
        if (data.Secret) {
            this.setCooikes(AkContext.Keys.LoginSecretKey, data.Secret, {
                path: '/',
                expires: secretExpiresDate
            }, true);
        }
        if (data.CompanyInfo) {
            if (data.CompanyInfo.LanguageCode) {
                if (!this.getCooike(AkContext.Keys.LanguageKey,true)) {
                    this.setCooikes(AkContext.Keys.LanguageKey, data.CompanyInfo.LanguageCode.toLocaleLowerCase(),{
                        path: '/',
                        expires: secretExpiresDate
                    },true);
                }
            }
        }
    }
}