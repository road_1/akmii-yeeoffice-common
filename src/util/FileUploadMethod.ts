import { AkResponse, AppKeys, AkContext, AkGlobal } from './common';
import { AkUtil, DeleteDocumentRequest, ApplyContentLocale } from "../index";
import { FileUpLoadAPI } from '../api/common/fileupload';
import { LibraryResponse, LibraryModel, FileUploadError, PostDcoument, FileUploadStatus, AkUploadFile } from '../api/common/fileuploadmodel';
import {FileUpLoadCommon} from './FileUpLoadCommon';
import { UploadLocale } from '../locales/localeid';

export class FileUploadMethod {

    static library: LibraryModel = {};

    static UpLoadFile(code: string, options: AkUploadFile): Promise<AkResponse> {

        return new Promise((res, rej) => {
            this.getLibraryByCode(code, options).then((onfullfiled: AkResponse) => {
                res(onfullfiled);
            }, (onrejected: AkResponse) => {
                rej(onrejected);
            });
        });
    }

    /**  获取library 信息 */
    static getLibraryByCode(code: string, options: AkUploadFile): Promise<AkResponse> {
        return new Promise((res, rej) => {
            FileUpLoadAPI.getLibraryByCode({ Code: code }).then((data: LibraryResponse) => {
                this.library = data.Data;
                this.validate(options).then(() => {
                    this.beginUpload(options,this.library).then((onfulfilled: AkResponse) => {
                        res(onfulfilled);
                    }, (onreject: AkResponse) => {
                        rej(onreject);
                    });
                }, (onReject: AkResponse) => {
                    rej(onReject);
                });
            });
        });
    }

    static validate(options: AkUploadFile): Promise<AkResponse> {
        let response: AkResponse = {};
        const {formatMessage}=AkGlobal.intl;

        if (options.file.size === 0) {
            response.Status = FileUploadError.FileEmpty;
            response.Message = formatMessage({id:UploadLocale.MsgNoContentError},{name:options.file.name});
            return Promise.reject(response);
        }
        if (FileUpLoadCommon.validateFileSize(options) === false) {
            response.Status = FileUploadError.FileSizeMoreThan2GB;
            response.Message = formatMessage({id:UploadLocale.MsgFileSizeError},{name:options.file.name,value:'2gb'});
            return Promise.reject(response);
        }
        if (FileUpLoadCommon.validateFileNameSize(options.file.name) === false) {
            response.Status = FileUploadError.FileNameMoreThan200Chars;
            response.Message = formatMessage({id:UploadLocale.FileNameMoreLarge},{name:options.file.name});
            return Promise.reject(response);
        }
        if (FileUpLoadCommon.validateFileName(options.file.name) === false) {
            response.Status = FileUploadError.FileNameHasInvalidateChars;
            response.Message =formatMessage({id:UploadLocale.FileNameInvalidate},{name:options.file.name});
            return Promise.reject(response);
        }
        response.Status = 100;
        response.Message = "validate success";
        return Promise.resolve(response);
    }

    static beginUpload(options: AkUploadFile,library:LibraryModel): Promise<AkResponse> {
        const {formatMessage}=AkGlobal.intl;
        return new Promise((resolve, reject) => {
            if (options.file.size !== 0 && library) {
                FileUpLoadCommon.upload(options).then((res: AkResponse) => {
                    if (res.Status === FileUploadStatus.Uploaded) {
                        this.postFile(options,library).then((onfulfiled: AkResponse) => {
                            resolve(onfulfiled);
                        }, (onrejected: AkResponse) => {
                            reject(onrejected);
                        });
                    } else {
                        reject(res);
                    }
                }, (onredected => {
                    reject(onredected)
                }));
            } else {
                let respons: AkResponse = {};
                respons.Status = FileUploadError.FileEmpty;
                respons.Message =formatMessage({id:ApplyContentLocale.UploadFail});//  "file empty"
                reject(respons);
            }
        })
    };

    static async postFile(options: AkUploadFile,library:LibraryModel): Promise<AkResponse> {
        let filename = options.file.name;
        let index = filename.lastIndexOf('.');
        let name = filename.substring(0, index);
        let extension = filename.substring(index + 1, filename.length).toLowerCase();
        let newName = AkUtil.guid();
        let response: AkResponse = {};

        return new Promise((res, rej) => {
            const postRequest: PostDcoument = {
                AppID: library.AppID,
                LibraryID: library.LibraryID,
                ParentID: "0",
                ContentLength: options.file.size,
                Name: newName,// name,
                Extension: extension,
                MD5: options.MD5,
                Type: 1,
                OverWrite: false
            }
            FileUpLoadAPI.postDocument(postRequest).then(d => {
                response = d;
                if (d.Status === 0) {
                    response.Message=newName;
                    if (AkContext.getBranch() === AkContext.YeeOffice) {
                        response.Data = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net) + d.Data + "&name=" + encodeURIComponent(name);
                    } else {
                        response.Data = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net) + d.Data + "&name=" + encodeURIComponent(name) + `&akmiisecret=temp`;
                    }
                    res(response);
                } else {
                    rej(response);
                }
            });
        });
    }
    /** 删除文件 */
    static deleteFiles( deleteFiles?:DeleteDocumentRequest): Promise<AkResponse> {
        return new Promise((res, rej) => {
            FileUpLoadAPI.deleteDocumentFile(deleteFiles).then(d => {
                d.Status === 0? res(d):rej(d);
            });
        });
    }
}
