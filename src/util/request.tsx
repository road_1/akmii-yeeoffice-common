import * as request from "superagent";
import * as React from "react";
import * as Mock from "mockjs";
import * as moment from "moment";
import { Cookies } from "../util/cookie";
import { AkRequestParam, AkResponse, AkGlobal, AkConstants, AppKeys, AkContext, NavigationHelper } from './common';
import { CommonLocale } from "../locales";
import { AkNotification } from "../components/controls/ak-notification";
import { MerchatAPI } from "../api/masterpage/masterpage";
import { CacheHelper } from "./cache";

export class Request<TRequest, TResponse extends AkResponse> {
    /**
     * 默认参数
     *
     * @type {AkRequestParam < TRequest >}
     * @memberOf Request
     */
    defaultParam: AkRequestParam<TRequest> = {
        Url: '',
        Data: null,
        Querys: null,
        RequireToken: true,
        IgnoreError: false,
        Headers: [],
        Prefix: null
    }
    /**
     * 处理Http请求
     *
     * @param {(url : string) => request.SuperAgentRequest} func
     * @param {AkRequestParam < TRequest >} [param]
     * @returns
     *
     * @memberOf Request
     */
    async processResponse(func: (url: string) => request.SuperAgentRequest, param?: AkRequestParam<TRequest>, emptyData?: any) {
        //初始化 商户API URL
        if (!param.Url.startsWith("http") && !param.Prefix) {
            param.Prefix = AkContext.getAppKey() ? AkContext.getAppInfoAPI_URL(AkContext.getAppKey()) : null;
        }

        let request = func(param.Prefix && !param.Url.startsWith("http")
            ? param.Prefix + param.Url
            : '' + param.Url);

        /**跨域请求带Cookie传递 */
        // request.withCredentials();
        if (param.Data) {
            request.send(JSON.stringify(param.Data));
        }

        if (param.Querys) {
            request.query(param.Querys);
        }

        if (param.FormData) {
            request.send(param.FormData);
        } else {
            request.type("json");
            request.set("Content-Type", "application/json; charset=utf-8");
        }

        if (param.RequireToken) {
            //.net 版本不需要附带
            if (AkContext.getBranch() !== AkContext.YeeOffice) {
                /**获取用户Login信息 */
                let token = AkContext.getToken();
                if (!token || token.length <= 0) {
                    //token 不存在，需要重新获取 
                    if (!this.isSPRequestAndFirst(param.Url)) {
                        const rs = MerchatAPI.merchatLogin();
                        token = AkContext.getToken();
                    }
                }
                request.set("AkmiiSecret", token);
            } else {
                request.withCredentials();
            }
        }

        request.set("ClientTimeZone", (moment().utcOffset() / 60).toString());
        if (param.Headers) {
            param.Headers.forEach((entry) => {
                request.set(entry);
            });
        }
        return new Promise<TResponse>((resolve: (response: TResponse) => void, reject: (errorResponse: TResponse) => void) => {
            request
                .end(function (error, response: request.Response): void {
                    if (!response) {
                        if (!param.IgnoreError && AkGlobal.intl) {
                            AkNotification.error({
                                message: AkGlobal.intl.formatMessage({ id: CommonLocale.Tip }),
                                description: AkGlobal.intl.formatMessage({ id: CommonLocale.ResponseError })
                            });
                        }
                        resolve(({
                            Status: 1,
                            Data: emptyData
                                ? emptyData
                                : undefined
                        }) as TResponse);
                        return;
                    }
                    if (response.ok) {
                        if (response.body) {
                            if (response.body.Message === "mock") {
                                resolve(Mock.mock(response.body))
                            }
                            if (response.body.Status > 0) {
                                const messageId = AkConstants.CommonServerStatusLocale + response.body.Status;
                                let message = AkGlobal.intl && AkGlobal.intl.formatMessage({ id: messageId });
                                if (message !== messageId) {
                                    //如果定义了status对应的消息，替换消息内容
                                    response.body.ServerMessage = response.body.Message;
                                    response.body.Message = message;
                                }
                            }
                            if (AkContext.getBranch() == AkContext.YeeOffice && response.body.Status == 5) {
                                //app未授权或未启用界面
                                if (!AkContext.isYeeFlow()) {
                                    window.location.href = "/Error/Disable";
                                }
                            }
                            if (response.body.ServerMessage === "secretStr error") {
                                if (AkContext.getBranch() == AkContext.YeeOffice) {
                                    window.location.href = AkContext.getSignOutURI();
                                }
                            }
                            resolve(response.body);
                        } else {
                            resolve({
                                Status: 1,
                                Data: response.text
                            } as TResponse);
                        }
                    } else {
                        if (!param.IgnoreError && AkGlobal.intl) {
                            if (response.unauthorized) {
                                AkNotification.error({
                                    message: AkGlobal.intl.formatMessage({ id: CommonLocale.Error }),
                                    description: AkGlobal.intl.formatMessage({ id: CommonLocale.ResponseUnauthorized })
                                });
                                if (AkContext.getBranch() == AkContext.YeeOffice) {
                                    window.location.href = AkContext.getSignOutURI();
                                }
                            } else if (response.notFound) {
                                AkNotification.error({
                                    message: AkGlobal.intl.formatMessage({ id: CommonLocale.Tip }),
                                    description: AkGlobal.intl.formatMessage({ id: CommonLocale.ResponseNotFound })
                                });

                            } else if (response.badRequest) {
                                AkNotification.error({
                                    message: AkGlobal.intl.formatMessage({ id: CommonLocale.Tip }),
                                    description: AkGlobal.intl.formatMessage({ id: CommonLocale.ResponseBadRequest })
                                });

                            } else {
                                AkNotification.error({
                                    message: AkGlobal.intl.formatMessage({ id: CommonLocale.Tip }),
                                    description: AkGlobal.intl.formatMessage({ id: CommonLocale.ResponseError })
                                });
                            }
                        }
                        resolve(({
                            Status: 1,
                            Data: emptyData
                                ? emptyData
                                : undefined
                        }) as TResponse);
                    }
                });
        });
    }
    /**
     * 构造请求数据
     *
     * @param {IArguments} args
     * @returns {AkRequestParam < TRequest >}
     *
     * @memberOf Request
     */
    buildData(args: IArguments | any[]): AkRequestParam<TRequest> {

        if (typeof (args[0]) === "string") {
            return Object.assign({}, this.defaultParam, {
                Url: args[0],
                Data: args[1]
            })
        } else {
            return Object.assign({}, this.defaultParam, args[0])
        }
    }
    /**
    * 判断是否SharePoint请求或第一次请求Token,
    * @memberOf Request
    */
    private isSPRequestAndFirst(url) {
        let lowerUrl = url.toLowerCase();
        let loginURL = CacheHelper.getLocalStorage(AkContext.Keys.LoginHerperKey) ? CacheHelper.getLocalStorage(AkContext.Keys.LoginHerperKey)[1].toLowerCase() : null;
        if (!loginURL || lowerUrl.indexOf("sharepoint.cn") >= 0 || lowerUrl.indexOf("sharepoint.com") >= 0 || lowerUrl.indexOf(loginURL) >= 0) {
            return true;
        }
        return false;
    }

    /**
     * 生成服务器请求
     * @param url
     * @param data 
     * @param addRandom 是否增加随机数避免加载旧数据
     */
    buildQueryParam(url, data, addRandom?) {
        if (data) {
            url += "?";
            Object.getOwnPropertyNames(data)
                .forEach(name => {
                    // if (data[name] !== undefined) 
                    {
                        url += name + "=" + encodeURI(data[name]) + "&";
                    }
                });
            if (addRandom) {
                url += Date.now() + "&";
            }

            url = url.slice(0, url.length - 1);
        }
        return url;
    }

    /**
     * Get method
     * @param url
     * @param data
     * @param emptyData response中的空数据，在服务器返回错误时保证前端功能
     * @returns {Promise<TResponse>}
     */
    get(url: string, data?: TRequest, emptyData?: any): Promise<TResponse> {
        return this.processResponse(request.get, this.buildData([
            this.buildQueryParam(url, data, true),
            data
        ]), emptyData)
    }

    /**
     * Post method
     * @param url
     * @param data
     * @param emptyData response中的空数据，在系统错误时保证前端功能
     * @returns {Promise<TResponse>}
     */
    post(url: string, data?: TRequest, emptyData?: any): Promise<TResponse> {
        return this.processResponse(request.post, this.buildData(arguments), emptyData);
    }

    /**
     * Put method
     * @param url
     * @param data
     * @param emptyData response中的空数据，在系统错误时保证前端功能
     * @returns {Promise<TResponse>}
     */
    put(url: string, data?: TRequest, emptyData?: any): Promise<TResponse> {
        return this.processResponse(request.put, this.buildData(arguments), emptyData);
    }

    /**
     * Delete method
     * 
     * @param {string} url 
     * @param {TRequest} [data] 
     * @param {*} [emptyData] 
     * @returns {Promise<TResponse>} 
     * @memberof Request
     */
    del(url: string, data?: TRequest, emptyData?: any): Promise<TResponse> {
        return this.processResponse(request.delete, this.buildData([
            this.buildQueryParam(url, data),
            data
        ]), emptyData);
    }

    /**
     * FormData post
     * 
     * @param {string} url 
     * @param {FormData} [formData] 
     * @returns 
     * @memberof Request
     */
    send(url: string, formData?: FormData, emptyData?: any) {
        return this.processResponse(request.post, {
            Url: url,
            FormData: formData,
            RequireToken: true
        }, emptyData || {});
    }
    /**
     * 
     * @param type  post、get
     * @param url  地址
     * @param data  参数
     * @param async true 异步，false 同步
     * @param sfuc  成功方法
     * @param error  失败方法
     */
    sendXMLHttpRequest(type: string, url: string, data: any, async: boolean, sfuc: (data) => void, error: (data) => void, isjson: boolean = true, header: any[] = null, hasToken: boolean = false) {
        var xmlhttp = new XMLHttpRequest;
        xmlhttp.onreadystatechange = () => {
            if (xmlhttp.readyState === 4) {
                var d;
                if (isjson) {
                    try {
                        d = JSON.parse(xmlhttp.responseText)
                    } catch (error) {
                        d = xmlhttp.responseText;
                    }
                } else {
                    d = xmlhttp.responseText;
                }
                if ((xmlhttp.status >= 200 && xmlhttp.status < 300) || xmlhttp.status === 304) {
                    sfuc(d);
                } else {
                    error(d);
                }

            }
        };
        xmlhttp.open(type, url, async);
        xmlhttp.setRequestHeader("content-type", "application/json");
        if (header) {
            header.forEach(function (element) {
                xmlhttp.setRequestHeader(element.key, element.value);
            });
        }
        if (hasToken) {
            xmlhttp.setRequestHeader(AkContext.Keys.LoginSecretKey, AkContext.getToken());
        }
        if (type.toLocaleLowerCase() === "get") {//大小写
            xmlhttp.send(data);
        }
        else if (type.toLocaleLowerCase() === "post") {
            xmlhttp.send(JSON.stringify(data));
        }
    }

}