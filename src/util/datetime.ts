import * as moment from 'moment';
import 'moment-timezone';
import { AkGlobal, AkContext } from './common';
import { AkDateTimeZoneUtil } from './date-timezone';

export class AkDatetimeUtil {
    /**
     * Minutes
     *
     * @static
     * @type {Array}
     * @memberof AkDatetimeUtil
     */
    static MINUTES = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59];

    /**
     * CST offset
     * 
     * @static
     * @type {number}
     * @memberof AkDatetimeUtil
     */
    static CSTOffset: number = 60 * 8;

    /**
     * Default Time Zone
     * 
     * @static
     * @type {string}
     * @memberof AkDatetimeUtil
     */
    static defaultTimeZone: string = 'Asia/Shanghai';

    /**
     * Default format
     * 
     * @static
     * @type {string}
     * @memberof AkDatetimeUtil
     */
    static defaultFormat: string = 'YYYY-MM-DD HH:mm:ss';

    static defaultFormatShort = 'YYYY-MM-DD';

    /**
     * Is a moment object or not
     * 
     * @static
     * @param {any} value 
     * @returns {boolean} 
     * @memberof AkDatetimeUtil
     */
    static isValidMoment(value): boolean {
        return moment.isMoment(value) && value.isValid();
    }

    /**
     * Convert to moment
     * 
     * @static
     * @param {any} date 
     * @param {string} format 
     * @returns {moment.Moment} 
     * @memberof AkDatetimeUtil
     */
    static toMoment(date): moment.Moment {
        const arr = (date + "").split(":");
        const second = arr[arr.length - 1];
        if (second === "0") {
            //兼容 ie  2019-05-02 15：00：0这种格式
            arr[arr.length - 1] = "00";
            date = arr.join(":");
        }
        const momentDate = moment(date);
        if (!AkDatetimeUtil.isValidMoment(momentDate))
            return undefined;
        return momentDate;
    }

    /**
     * Parse to string value
     * 
     * @static
     * @param {any} momentObj 
     * @param {string} [format='YYYY-MM-DD'] 
     * @returns {string} 
     * @memberof AkDatetimeUtil
     */
    static toValue(momentObj, format: string = AkDatetimeUtil.defaultFormat): string {
        if (!AkDatetimeUtil.isValidMoment(momentObj))
            return undefined;
        return momentObj.format(format);
    }


    /**
     * Is China Standard Time or not
     * 
     * @static
     * @param {any} momentObj 
     * @returns {boolean} 
     * @memberof AkDatetimeUtil
     */
    static isCST(momentObj): boolean {
        if (!AkDatetimeUtil.isValidMoment(momentObj))
            return false;
        return momentObj.utcOffset() === AkDatetimeUtil.CSTOffset;
    }

    /**
     * Has time in format or not
     * 
     * @static
     * @param {string} format 
     * @returns {boolean} 
     * @memberof AkDatetimeUtil
     */
    static hasTime(format: string, value?: string): boolean {
        const hourRegExp = new RegExp('([Hh]{1,2}|LTS)(?![^\[]*\])', 'g');
        if (value) {
            return !!format.match(hourRegExp) || value.length > 10;
        }
        return !!format.match(hourRegExp);
    }

    /**
     * Parse to CST Value
     * 
     * @static
     * @param {any} momentObj 
     * @param {string} [format='YYYY-MM-DD'] 
     * @returns {string} 
     * @memberof AkDatetimeUtil
     */
    static toServerTime(momentObj, format: string = AkDatetimeUtil.defaultFormat): ToCSTValueRes {
        const baseInfo = AkDatetimeUtil.getBaseInfo();
        // let timeZone = baseInfo.ServerTimeZone;
        // if (AkContext.isYeeFlow()) {
        let timeZone = baseInfo.TimeZoneFe || baseInfo.ServerTimeZone;
        // }
        if (!AkDatetimeUtil.isValidMoment(momentObj))
            return undefined;
        // const tempString = momentObj.clone().format(format);
        // let tempMoment = moment.tz(tempString, format, timeZone);

        let formatArr = format.split(" ");


        let serverFormat = "YYYY-MM-DD";
        if (formatArr && formatArr[0].length === 4) {
            serverFormat = "YYYY";
        } else if (formatArr && formatArr[0].length === 5) {
            serverFormat = "MM-DD";
        } else if (formatArr && formatArr[0].length === 7) {
            serverFormat = "YYYY-MM";
        }

        if (formatArr.length > 1) {
            serverFormat = serverFormat + " " + formatArr[1];
        }

        return {
            dateString: momentObj.tz(baseInfo.ServerTimeZone).format(serverFormat),
            date: momentObj.tz(baseInfo.ServerTimeZone)
        }

        // if (tempMoment.utcOffset() === momentObj.utcOffset()) {
        //     return {
        //         dateString: tempMoment.tz(baseInfo.ServerTimeZone).format(AkDatetimeUtil.defaultFormat),
        //         date: tempMoment.tz(baseInfo.ServerTimeZone)
        //     }
        // }
        // else {
        //     // datepicker 选中此刻 的时间处理
        //     return {
        //         dateString: AkDateTimeZoneUtil.getSeverNowDate(),
        //         date: moment(AkDateTimeZoneUtil.getSeverNowDate(), AkDatetimeUtil.defaultFormat)
        //     }
        // }
    }

    /**
     * Parse to CST Moment
     * 
     * @static
     * @param {any} date 
     * @param {string} [timezone] 
     * @returns {moment.Moment} 
     * @memberof AkDatetimeUtil
     */
    static toUserTime(date): moment.Moment {
        const baseInfo = AkDatetimeUtil.getBaseInfo();
        let timeZone = baseInfo.TimeZoneFe;
        // if (!AkContext.isYeeFlow()) {
        //     timeZone = baseInfo.ServerTimeZone;
        // }

        let showDateMoment;
        if (moment.isMoment(date)) {
            showDateMoment = date.tz(timeZone || AkDatetimeUtil.getBaseInfo().ServerTimeZone);
        } else {

            const arr = (date + "").split(":");
            const second = arr[arr.length - 1];

            if (date.indexOf(" ") === -1) {
                //没有时间参数
            } else if ((date + "").indexOf(":") === -1) {
                // 解决 new Date("2018-05-04 16")解析为Invalid Date
                date += ":00"
            } else if (second === "0") {
                //兼容 ie  2019-05-02 15：00：0这种格式
                arr[arr.length - 1] = "00";
                date = arr.join(":");
            }

            const serverDateMoment = moment.tz(date, AkDatetimeUtil.defaultFormat, AkDatetimeUtil.getBaseInfo().ServerTimeZone);
            showDateMoment = serverDateMoment.tz(timeZone || AkDatetimeUtil.getBaseInfo().ServerTimeZone);
        }
        if (!AkDatetimeUtil.isValidMoment(showDateMoment))
            return undefined;
        return showDateMoment;

        //  // 解决 IE 下 获取"2018-03-28 15:06:02" 的值为Nan
        // let tempDate=(date+"").replace(/-/g,'/');
        // // 解决 new Date("2018-05-04 16")解析为Invalid Date
        // if((date+"").indexOf(":") === -1) {
        //     tempDate += ":00"
        // }
        // let times=(new Date(tempDate)).valueOf();
        // const momentDate = moment.tz(times, AkDatetimeUtil.defaultTimeZone).tz(moment.tz.guess());


    }

    static getBaseInfoTimeZone(): string {
        const reduces = AkGlobal.store.getState();
        const baseInfo = reduces.commonBaseInfo.baseInfo;
        // if (!AkContext.isYeeFlow()) {
        //     return baseInfo.ServerTimeZone;
        // }
        return baseInfo.TimeZoneFe || baseInfo.ServerTimeZone;
    }

    static getBaseInfo(): CommonUserBaseInfo {
        const reduces = AkGlobal.store.getState();
        const baseInfo = reduces.commonBaseInfo.baseInfo;
        return baseInfo;
    }

    // 将时间类型的数据做时区转换
    static parseDateToCSTMoment(date: Date, format: string = AkDatetimeUtil.defaultFormat): Date {
        // let dateString = moment(AkDatetimeUtil.parseDate(date)).format(AkDatetimeUtil.defaultFormat);
        let dateString = moment(date).format(AkDatetimeUtil.defaultFormat);
        const userDate = AkDatetimeUtil.toUserTime(dateString).format(AkDatetimeUtil.defaultFormat).replace(/-/g, "/")
        return new Date(userDate);
    }

    static setDateZero(v: number) {
        return v < 10 ? "0" + v : v + "";
    }

    static parseDate(date: Date, second?: boolean) {
        let dateStr = `${date.getFullYear()}-${AkDatetimeUtil.setDateZero(date.getMonth() + 1)}-${AkDatetimeUtil.setDateZero(date.getDate())} ${AkDatetimeUtil.setDateZero(date.getHours())}:${AkDatetimeUtil.setDateZero(date.getMinutes())}:${second ? AkDatetimeUtil.setDateZero(date.getSeconds()) : '00'}`;
        return dateStr;
    }

}