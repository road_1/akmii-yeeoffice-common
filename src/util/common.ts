import { URLHelper } from './URLHelper';
import { SagaMiddleware } from "redux-saga";
import { Store } from "redux";
import { InjectedIntlProps } from "react-intl";
import { AkUtil } from "./util";
import {
    MerchatLoginModel,
    CompanyInfo,
    UserModel,
    AppInfoList,
    ClientInfo,
    MerchantLevelEnum
} from '../api/masterpage/masterpagemodel';
import { MerchatAPI } from '../api/masterpage/masterpage';
import { AkNotification, AkArgsProps } from '../components/controls/ak-notification';
import { CommonLocale } from '../locales/localeid';
import { AkMessage, AkConfigOnClose, AkDatetimeUtil } from '../index';
import { AkUser, IdentityAPI, AkIdentityType } from "../api/identity/identity";
import { IdentityAction } from "../actions";
import { CacheHelper } from './cache';
import { InjectedRouter, PlainRoute, WithRouterProps, withRouter, hashHistory } from 'react-router';
import { Location } from "history";
import * as moment from "moment";
import { Params } from 'react-router/lib/Router';

//获取组织，地点，组等请求重试时间和次数 AkContext中使用
let REFRESS_COUNT_ORG = 10;
let REFRESS_COUNT_LOC = 10;
let REFRESS_COUNT_GRP = 10;
let REFRESS_COUNT_POS = 10;
const REFRESS_TIMEOUT = 500;

export interface AkBase {
}

export interface AkRequest extends AkBase {
}

export interface AkResponse extends AkBase {
    /**
     * 数据
     *
     * @type {任意类型}
     * @memberOf AkResponse
     */
    Data?: any;
    /**
     * 状态
     */
    Status?: number;
    /**
     * 消息
     */
    Message?: string;
    /**服务器错误消息 */
    ServerMessage?: string;
    /**
     * 总记录数
     */
    TotalCount?: number;
}

export interface AkListResponse<T> extends AkBase {
    Data?: T[];
    /**za
     * 状态
     */
    Status?: number;
    /**
     * 消息
     */
    Message?: string;
    ServerMessage?: string;
    /**
     * 总记录数
     */
    TotalCount?: number;
}

export interface AkItemResponse<T> extends AkBase {
    Data?: T;
    /**za
     * 状态
     */
    Status?: number;
    /**
     * 消息
     */
    Message?: string;
    ServerMessage?: string;
    /**
     * 总记录数
     */
    TotalCount?: number;
}

export interface AkBaseModel extends AkBase {
    /** 租户ID Index */
    TenantID?: string;
    /** 创建时间 */
    Created?: string;
    /** 创建时间对应的字符串形式 */
    CreatedStr?: string;
    /** 创建人名称 */
    CreatedByName?: string;
    /** 创建人 */
    CreatedBy?: string;
    /** 修改时间 */
    Modified?: string;
    /** 修改时间对应的字符串形式 */
    ModifiedStr?: string;
    /** 修改人名称 */
    ModifiedByName?: string;
    /** 修改人 */
    ModifiedBy?: string;
}

export interface AkRequestParam<T> {
    /**
     * URL 请求访问的路径
     *
     * @type {string}
     * @memberOf RequestParam
     */
    Url?: string;
    /**
     * 是否需要Token
     */
    RequireToken?: boolean;
    /**
     * 头信息
     */
    Headers?: {
        [key: string]: string
    }[];
    /**
     * 请求的数据
     *
     * @type {WObject}
     * @memberOf RequestParam
     */
    Data?: T;
    /**
     * 查询参数
     *
     * @type {WObject}
     * @memberOf RequestParam
     */
    Querys?: AkBase;
    /**
     * 访问接口前缀
     *
     * @type {string}
     * @memberOf RequestParam
     */
    Prefix?: string;
    /**
     * 是否需要错误消息
     */
    IgnoreError?: boolean;
    /**
     * FormData
     */
    FormData?: FormData;
}


/**翻译基类 */
export interface IntlProps extends InjectedIntlProps {
}

export interface RouterProps {
    location?: Location;
    params?: Params;
    router?: InjectedRouter;
    routes?: PlainRoute[];
}

/**
 * 国际化
 */
export class AppLocaleStatic {
    constructor(options: AppLocaleStatic) {
        this.locale = options.locale;
        this.formats = options.formats;
        this.defaultFormats = options.defaultFormats;
        this.defaultLocale = options.defaultLocale;
        this.messages = options.messages;
        this.antd = options.antd;
    }

    antd?: any;
    locale?: string;
    formats?: Object;
    messages?: Object;
    defaultLocale?: string;
    defaultFormats?: Object;
}

export class AkConstants {
    static RegExpFieldKey = /[^_a-zA-Z0-9]/g;
    /**是否为中文 */
    static RegExpIsChiness = /[^\u4e00-\u9fa5]/;
    /**手机号验证 */
    static RegExpMobile = /^(13[0-9]|14[579]|15[0-3,5-9]|17[0135678]|18[0-9])\d{8}$/;
    /**座机号验证 */
    static RegExpPhone = /^((\d{3,4}-)|\d{3,4}-)?\d{7,8}$/;
    /**
     * 服务器状态
     */
    static CommonServerStatusLocale = "common.server.status."

    static DefualtHttps = "https://file.yeeoffice.com";
    /** 用户默认图片 */
    static DefaultUserIcon = AkConstants.DefualtHttps + "/common/user.jpg";// "images/user.jpg";
    /** 分割线图片 */
    static DefaultFlowBlinkIcon = AkConstants.DefualtHttps + "/common/flow-blink.gif";
    /** 签名图片 */
    static DefaultSingerIcon = AkConstants.DefualtHttps + "/common/singer.png";
    /** 流程默认图片 */
    static DefaultFlowDefaultIcon = AkConstants.DefualtHttps + "/common/flow-default.png";
    /** 新建流程默认图片 */
    static DefaultNewFlowIcon = AkConstants.DefualtHttps + "/common/newflow.png";
    /** 通知图标 */
    static DefaultNoticeIcon = AkConstants.DefualtHttps + "/common/notice.png";

    static minXS = window.innerWidth < 480;
    static minSM = window.innerWidth < 768;

    /**
     * 安全的整数最大值，大于该值js由于精度问题会自动取整
     * @type {number}
     */
    static NUMBER_MAX_SAFE = 9999999999999998;
    static NUMBER_MIN_SAFE = -999999999999998;


    static xs = 480;
    static sm = 768;
    static md = 992;
    static lg = 1200;
    static xl = 1600;
    static minMD = window.innerWidth < 992;
    static minLG = window.innerWidth < 1200;
    static minXL = window.innerWidth < 1600;

}

/**
 * 所有项目AppKeys
 */
export class AppKeys {
    /** 当前Key */
    static CurrentAppKey = "CurrentAppKey";
    /** 评论 */
    static YeeOfficeComment = "YeeOfficeComment";
    /** 通讯录*/
    static YeeOfficeContacts = "YeeOfficeContacts";
    /** 知识管理*/
    static YeeOfficeKnowledge = "YeeOfficeKnowledge";
    /** 文档中心*/
    static YeeOfficeDocument = "YeeOfficeDocument";
    /** 文档中心.net*/
    static YeeOfficeDocument_Net = "YeeOfficeDocument_Net";
    /** 一起办基础包*/
    static YeeOfficeBase = "YeeOfficeBase";
    /** 系统管理*/
    static YeeOfficeSettings = "YeeOfficeSettings";
    /** 项目管理*/
    static ProjectManagement = "ProjectManagement";
    /** 销售管理*/
    static SalesManagement = "SalesManagement";
    /** 文件服务*/
    static FileServer = "FileServer";
    /** HoribaKPI*/
    static SalesManagementHoriba = "SalesManagement-Horiba";
    /** 企业门户*/
    static YeeOfficePortal = "YeeOfficePortal";
    /** 流程中心*/
    static Flowcraft = "Flowcraft";
    /** 消息中心*/
    static YeeOfficeWebChat = "YeeOfficeWebChat";
    /** 堀场项目*/
    static HoribaFlowcraft = "HoribaFlowcraft";

    static isAppkey(appkey: string) {
        let falg = false;
        AppKeys.AppkeyList.forEach(element => {
            if (element === appkey) {
                falg = true;
            }
        });
        if (falg) {
            return true;
        } else {
            console.log(appkey + "不是合法的appkey");
            return false;
        }
    }

    ///用与项目APPKey处理
    static AddPust(appkey: string) {
        AppKeys.AppkeyList.push(appkey);
    }

    static AppkeyList: string[] = [
        "YeeOfficeComment",
        "YeeOfficeContacts",
        "YeeOfficeKnowledge",
        "YeeOfficeDocument",
        "YeeOfficeDocument_Net",
        "YeeOfficeBase",
        "YeeOfficeSettings",
        "ProjectManagement",
        "SalesManagement",
        "FileServer",
        "SalesManagement-Horiba",
        "YeeOfficePortal",
        "Flowcraft",
        "YeeOfficeWebChat",
        "HoribaFlowcraft"];
}

export class AkGlobal {
    static store: Store<any>;
    static intl: ReactIntl.InjectedIntl;
    static saga: SagaMiddleware<any>;
    static reloadMasterpage: (language?: any) => void;
    // static user: SagaMiddleware;
}

export interface AkDictionaryString<TValue> {
    [index: string]: TValue
}

export interface AkDictionaryNumber<TValue> {
    [index: number]: TValue
}

export abstract class AkEntity {
    ID: string = AkUtil.guid();
}

export class FlowcraftCommon {
    /**隐藏顶部工具栏 */
    static hiddenTB() {
        let top = document.getElementById("masterPageTop");
        let bottom = document.getElementsByClassName("placeholder-bottom")[1];
        if (top) {
            top.classList.add("hidden");
            document.getElementsByTagName("body")[0].setAttribute("style", "");
        }
        if (bottom) {
            bottom.classList.add("hidden");
        }
    }

    static hiddenAutoflex() {
        let content = document.getElementById("content");
        if (content) {
            content.classList.remove("content");
            content.classList.remove("container-fluid");
        }
    }

    static goBack() {
        if (hashHistory.getCurrentLocation().query["ismail"]) {
            window.location.href = window.location.pathname + "?_hash_=/todo";
            return;
        }

        if (hashHistory.getCurrentLocation().query["goHistory"]) {
            if (window.history.length > 1) {
                window.history.back();
            } else {
                if (AkContext.getBranch() === AkContext.SPOnline) {
                    window.location.href = AkContext.getRootWebUrl() + "/Workflow/";
                }
            }
            return;
        }

        if (document.referrer === "") {
            if (AkContext.getBranch() === AkContext.YeeOffice) {
                window.location.href = AkContext.getRootWebUrl() + "/Home/Workflow/";
            } else {
                window.location.href = AkContext.getRootWebUrl() + "/Workflow/";
            }
            return;
        }

        if (!document.referrer) {
            window.location.href = window.location.pathname;
            return;
        }
        if (document.referrer.indexOf("_hash_=/todo/transfer") > 0) {
            window.location.href = window.location.pathname + "#/todo";
            return;
        }
        (window.history && window.history.length && window.history.length > 1) ? window.history.back() : window.location.href = window.location.pathname;
    }

    static newGuid() {
        return AkUtil.guid();
        // var guid = "";
        // for (var i = 1; i <= 32; i++) {
        //     var n = Math.floor(Math.random() * 16.0).toString(16);
        //     guid += n;
        //     if ((i === 8) || (i === 12) || (i === 16) || (i === 20))
        //         guid += "-";
        // }
        // return guid;
    }

    static xs = 480;
    static sm = 768;
    static md = 992;
    static lg = 1200;
    static xl = 1600;
    static minXS = window.innerWidth < 480;
    static minSM = window.innerWidth < 768;
    static minMD = window.innerWidth < 992;
    static minLG = window.innerWidth < 1200;
    static minXL = window.innerWidth < 1600;

    static getSiderWidth() {
        let width = window.innerWidth;
        if (width < this.sm) {
            return document.body.clientWidth - 2;
        } else {
            return 250;
        }
    }

    static noContentIcon = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA9CAYAAADxoArXAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKTWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVN3WJP3Fj7f92UPVkLY8LGXbIEAIiOsCMgQWaIQkgBhhBASQMWFiApWFBURnEhVxILVCkidiOKgKLhnQYqIWotVXDjuH9yntX167+3t+9f7vOec5/zOec8PgBESJpHmomoAOVKFPDrYH49PSMTJvYACFUjgBCAQ5svCZwXFAADwA3l4fnSwP/wBr28AAgBw1S4kEsfh/4O6UCZXACCRAOAiEucLAZBSAMguVMgUAMgYALBTs2QKAJQAAGx5fEIiAKoNAOz0ST4FANipk9wXANiiHKkIAI0BAJkoRyQCQLsAYFWBUiwCwMIAoKxAIi4EwK4BgFm2MkcCgL0FAHaOWJAPQGAAgJlCLMwAIDgCAEMeE80DIEwDoDDSv+CpX3CFuEgBAMDLlc2XS9IzFLiV0Bp38vDg4iHiwmyxQmEXKRBmCeQinJebIxNI5wNMzgwAABr50cH+OD+Q5+bk4eZm52zv9MWi/mvwbyI+IfHf/ryMAgQAEE7P79pf5eXWA3DHAbB1v2upWwDaVgBo3/ldM9sJoFoK0Hr5i3k4/EAenqFQyDwdHAoLC+0lYqG9MOOLPv8z4W/gi372/EAe/tt68ABxmkCZrcCjg/1xYW52rlKO58sEQjFu9+cj/seFf/2OKdHiNLFcLBWK8ViJuFAiTcd5uVKRRCHJleIS6X8y8R+W/QmTdw0ArIZPwE62B7XLbMB+7gECiw5Y0nYAQH7zLYwaC5EAEGc0Mnn3AACTv/mPQCsBAM2XpOMAALzoGFyolBdMxggAAESggSqwQQcMwRSswA6cwR28wBcCYQZEQAwkwDwQQgbkgBwKoRiWQRlUwDrYBLWwAxqgEZrhELTBMTgN5+ASXIHrcBcGYBiewhi8hgkEQcgIE2EhOogRYo7YIs4IF5mOBCJhSDSSgKQg6YgUUSLFyHKkAqlCapFdSCPyLXIUOY1cQPqQ28ggMor8irxHMZSBslED1AJ1QLmoHxqKxqBz0XQ0D12AlqJr0Rq0Hj2AtqKn0UvodXQAfYqOY4DRMQ5mjNlhXIyHRWCJWBomxxZj5Vg1Vo81Yx1YN3YVG8CeYe8IJAKLgBPsCF6EEMJsgpCQR1hMWEOoJewjtBK6CFcJg4Qxwicik6hPtCV6EvnEeGI6sZBYRqwm7iEeIZ4lXicOE1+TSCQOyZLkTgohJZAySQtJa0jbSC2kU6Q+0hBpnEwm65Btyd7kCLKArCCXkbeQD5BPkvvJw+S3FDrFiOJMCaIkUqSUEko1ZT/lBKWfMkKZoKpRzame1AiqiDqfWkltoHZQL1OHqRM0dZolzZsWQ8ukLaPV0JppZ2n3aC/pdLoJ3YMeRZfQl9Jr6Afp5+mD9HcMDYYNg8dIYigZaxl7GacYtxkvmUymBdOXmchUMNcyG5lnmA+Yb1VYKvYqfBWRyhKVOpVWlX6V56pUVXNVP9V5qgtUq1UPq15WfaZGVbNQ46kJ1Bar1akdVbupNq7OUndSj1DPUV+jvl/9gvpjDbKGhUaghkijVGO3xhmNIRbGMmXxWELWclYD6yxrmE1iW7L57Ex2Bfsbdi97TFNDc6pmrGaRZp3mcc0BDsax4PA52ZxKziHODc57LQMtPy2x1mqtZq1+rTfaetq+2mLtcu0W7eva73VwnUCdLJ31Om0693UJuja6UbqFutt1z+o+02PreekJ9cr1Dund0Uf1bfSj9Rfq79bv0R83MDQINpAZbDE4Y/DMkGPoa5hpuNHwhOGoEctoupHEaKPRSaMnuCbuh2fjNXgXPmasbxxirDTeZdxrPGFiaTLbpMSkxeS+Kc2Ua5pmutG003TMzMgs3KzYrMnsjjnVnGueYb7ZvNv8jYWlRZzFSos2i8eW2pZ8ywWWTZb3rJhWPlZ5VvVW16xJ1lzrLOtt1ldsUBtXmwybOpvLtqitm63Edptt3xTiFI8p0in1U27aMez87ArsmuwG7Tn2YfYl9m32zx3MHBId1jt0O3xydHXMdmxwvOuk4TTDqcSpw+lXZxtnoXOd8zUXpkuQyxKXdpcXU22niqdun3rLleUa7rrStdP1o5u7m9yt2W3U3cw9xX2r+00umxvJXcM970H08PdY4nHM452nm6fC85DnL152Xlle+70eT7OcJp7WMG3I28Rb4L3Le2A6Pj1l+s7pAz7GPgKfep+Hvqa+It89viN+1n6Zfgf8nvs7+sv9j/i/4XnyFvFOBWABwQHlAb2BGoGzA2sDHwSZBKUHNQWNBbsGLww+FUIMCQ1ZH3KTb8AX8hv5YzPcZyya0RXKCJ0VWhv6MMwmTB7WEY6GzwjfEH5vpvlM6cy2CIjgR2yIuB9pGZkX+X0UKSoyqi7qUbRTdHF09yzWrORZ+2e9jvGPqYy5O9tqtnJ2Z6xqbFJsY+ybuIC4qriBeIf4RfGXEnQTJAntieTE2MQ9ieNzAudsmjOc5JpUlnRjruXcorkX5unOy553PFk1WZB8OIWYEpeyP+WDIEJQLxhP5aduTR0T8oSbhU9FvqKNolGxt7hKPJLmnVaV9jjdO31D+miGT0Z1xjMJT1IreZEZkrkj801WRNberM/ZcdktOZSclJyjUg1plrQr1zC3KLdPZisrkw3keeZtyhuTh8r35CP5c/PbFWyFTNGjtFKuUA4WTC+oK3hbGFt4uEi9SFrUM99m/ur5IwuCFny9kLBQuLCz2Lh4WfHgIr9FuxYji1MXdy4xXVK6ZHhp8NJ9y2jLspb9UOJYUlXyannc8o5Sg9KlpUMrglc0lamUycturvRauWMVYZVkVe9ql9VbVn8qF5VfrHCsqK74sEa45uJXTl/VfPV5bdra3kq3yu3rSOuk626s91m/r0q9akHV0IbwDa0b8Y3lG19tSt50oXpq9Y7NtM3KzQM1YTXtW8y2rNvyoTaj9nqdf13LVv2tq7e+2Sba1r/dd3vzDoMdFTve75TsvLUreFdrvUV99W7S7oLdjxpiG7q/5n7duEd3T8Wej3ulewf2Re/ranRvbNyvv7+yCW1SNo0eSDpw5ZuAb9qb7Zp3tXBaKg7CQeXBJ9+mfHvjUOihzsPcw83fmX+39QjrSHkr0jq/dawto22gPaG97+iMo50dXh1Hvrf/fu8x42N1xzWPV56gnSg98fnkgpPjp2Snnp1OPz3Umdx590z8mWtdUV29Z0PPnj8XdO5Mt1/3yfPe549d8Lxw9CL3Ytslt0utPa49R35w/eFIr1tv62X3y+1XPK509E3rO9Hv03/6asDVc9f41y5dn3m978bsG7duJt0cuCW69fh29u0XdwruTNxdeo94r/y+2v3qB/oP6n+0/rFlwG3g+GDAYM/DWQ/vDgmHnv6U/9OH4dJHzEfVI0YjjY+dHx8bDRq98mTOk+GnsqcTz8p+Vv9563Or59/94vtLz1j82PAL+YvPv655qfNy76uprzrHI8cfvM55PfGm/K3O233vuO+638e9H5ko/ED+UPPR+mPHp9BP9z7nfP78L/eE8/sl0p8zAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAANESURBVHja7NpNiFVlGMDx34xXy9TM0k1JhBmhaR+IVpuMJGkRRKQZ1MZF4CJbVNCqTbQLokiRgiQmiJCCIQkUJcGsBjLxYxqsqGisjIKa1FBQ57a4z8FD4My9d84958zc88Dhhcv78fzP857n431vT19fn5xlNbZjBUbyXrxX/rIAizCrgLULAT4X7cVuAS5UWgW+DVeXSPc7Og38Cf7CtSWAHcbhTgM/gWk4WSB0L77DDXik08ADsa1nFAg9iJvxAD7Ow2kNpaCHc4TuDdglWIN9eXrpBHoWfsMVLYyttbn2t7Hmw/i0iLA0hHsD9tcWLJ3E3wst6HgMi/FoOM7C4vAAluM6nMA1TYzZF+HkzybXOI5leBD9ZUg8BgP6KvyNOeP0P4OjqI/Tbzb+xS0Bu7dMmdYg7sNXqW80C92GsCErWBkqB59hVYbzncLKKpeugCvgTL/hhZiH0RIZ7GI4t8yBl0c4KaO8i41ZAl8fsL/gufhtWglAz+EevBil4htZAR9NHQCcKpl1+3ETXsdBfD5Rp7U3UsdlJYRN1+kncQDTJwL8cpRiG/FNyR3wndEeaxd4A17Ca+EUyi5/RPV2Kz5oFXhpDNqP5ydRmB3AM2GsZ5sFnh7b9x+NW4LJJlvxYXjsu5sBPhjtkkmcUK3Hz2Hx2WMBv43b8VB4vcksiRM7cjngTXg6XPzuKZA2j0TCtAjv/T/xWIVtsQ2+xv3oGWfCnji1OKtxpnWiwwDzwgNfGYaqNzHmd3yEpzSOlrbXMB9fRocb8X0byrwVO6STsnascDOGnI/2HRyqYWe8sSfjqGZGkxPVo+8XmJvDFp0Z7eP4oYXS9nxUef3YX4vkewfeL3ldnayxC6dbHHsEm7EtmeSnNpVIdkMed72jqcqtHRlOv7VCbuNzljldecRTAVfA4ycfIvnotJzJQueJAifeeX4OwAtS8b9tmehVy4VI3R7TuCcelf3h3mg8C/FjpL+FfsPrsCXg65HZZPkk8XeHxjXr2SItnMjmeCovXQFXwBVwBVwBV8AVcAVcAVfAUxy43gWs9TTw6S4AHkkD39UFwCuTevjNqGVfdelueKrJYryCAzWNvwbMxQtT3MJ7sPa/AQCmb68uqxzhmQAAAABJRU5ErkJggg=="
}
/*
 * 用于多版本的分支兼容处理
 */
export class AkContext {
    /**判断是否在mobile app界面下访问 */
    static isMobileApp() {
        let isPhonse = AkUtil.getQueryString("isPhone");
        return isPhonse && isPhonse.length > 0;
    }

    /**
     * 系统App状态枚举
     */
    static AppInfoEnum = {
        NoInstall: -1,
        Enable: 1,
        Disable: 2,
    };
    /**
     * 系统App信息
     */
    static AppInfos: AppInfoList[];
    /**
     * 登录后的用户信息
     */
    static MerchantData: MerchatLoginModel;
    /**
     * 当前APPkey
     */
    private static _currentAppKey: string;

    /**
     * 用于刷新Token的状态
     */
    static RefreshCount = 3;

    /**
     * 目前的SharepointOnline版本
     */
    static SPOnline = "SPOnline";
    /**
     * YeeOffice Net 版本
     */
    static YeeOffice = "YeeOffice";
    /**
     * 网易.net版本
     */
    private static Netease = "Netease";
    /**
     * Office365商店版本
     */
    private static Office365 = "Office365";
    /**
     *  用户存储 前端缓存的LocalStorage的key
     */
    static Keys = {
        /**
        * LocalStorage的存储key
        */
        SupportInfo: "AKSupport",
        /**
         * LocalStorage的存储key
         */
        TokenDataKey: "AKTokenData",
        /**
         * Cookie的存储key
         */
        LoginSecretKey: "AkmiiSecret",
        /**
         * LocalStorage的存储key
         */
        AppInfoKey: "AKAppInfo",
        /**
         * 用于存储当前版本分支
         */
        BranchKey: "AKCurrentBranch",
        /**
         * 用于多语言key
         */
        LanguageKey: "ak-language",
        /**
         * 老版本使用key 后续将会抛弃
         */
        LoginHerperKey: "AKLoginHelperData",
        /**
         * 认证类型
         */
        AuthenticationType: "AKAuthenticationType",
        /**
         * 多设备标识    Mobile=手机端
         */
        MobileDevice: "MobileDevice",
        SiteAbsoluteUrl: "AkSiteAbsoluteUrl",
        CacheType: "AkCacheTypeOf",
        SPOToken: "SPOToken",
        RootWebUrl: "RootWebUrl",

    };
    /**
   * SharePoint 默认权限组名称
   */
    static SharePointGroupNames = {
        Members: "YeeOfficeMembers",//参与者
        Admins: "YeeOfficeAdmins",//完全控制
        AppDocumentOwner: "AppDocumentOwner",//
        AppPortalOwner: "AppPortalOwner",
        AppSalesOwner: "AppSalesOwner",
        AppProjectOwner: "AppProjectOwner",
    }
    /**yeeoffice默认主题色 */
    static yeeofficeBG = "#63A8EB";
    /**yeeflow默认主题色 */
    static yeeflowBG = "#1ab394";
    /**获取默认主题色 */
    static defaultBG = AkContext.isYeeFlow() ? AkContext.yeeflowBG : AkContext.yeeofficeBG;
    /**
    *判断是否 获取站点Root web URl
    */
    static getRootWebUrl() {
        var url = window[AkContext.Keys.RootWebUrl];
        if (!url) {
            url = _spPageContextInfo.siteAbsoluteUrl;
        }
        return url;
    }
    /**
    *判断是否是自定义Rootweb
    */
    static getIsCustomRootWeb() {
        var url = window[AkContext.Keys.RootWebUrl];
        if (url) {
            return true;
        }
        return false;
    }
    static setAppKey(key: string) {
        this._currentAppKey = key;
    }

    /**
     *判断是否 是否移动端登陆
     */
    static isMobileLoginIn() {
        if (CacheHelper.getCooike(AkContext.Keys.MobileDevice) === "Mobile" ||
            AkUtil.getQueryString("isDlg") === "true" ||
            AkUtil.getQueryString("isDlg") === "1" ||
            AkUtil.getQueryString("isdlg") === "1" ||
            AkUtil.getQueryString("isdlg") === "true" ||
            AkUtil.getQueryString("system") === "teams"
        ) {
            return true;
        } else {
            return false;
        }
    }

    /**判断是否是Teams进入 */
    // static isTeamsLoginIn() {
    //     if (AkUtil.getQueryString("system") === "teams") {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

    static getAppKey() {
        return this._currentAppKey;
    }

    /**
     * 获取.net版本当前认证类型
     */
    static getAuthenticationType() {
        let type = CacheHelper.getCooike(AkContext.Keys.AuthenticationType);
        if (type) {
            type = "0";
        }
        return type;
    }

    /**
     * .net版本SP文档库是否可以用
     */
    static isNetSPDocument() {
        return window[AkContext.Keys.SPOToken] ? true : false && this.getBranch() === this.YeeOffice;
    }

    /**
     * 获取.net版本SPToken
     */
    static getNetSPToken() {
        return window[AkContext.Keys.SPOToken];
    }

    /**
     * 判断是否是网易版本
     */
    static isNeteaseVersion() {
        var version = window["JSVersion"];
        if (version === this.Netease) {
            return true;
        }
        return false;
    }

    /**
     * 判断是否是Office365商店版本
     */
    static isOffice365Version() {
        var version = window["JSVersion"];
        if (version === this.Office365) {
            return true;
        }
        return false;
    }

    /**
     * 获取当前版本分支,默认SPOnline
     */
    static getBranch(): string {
        var version = window["YeeOffice_JSVersion"];
        if (version && version.toLowerCase().startsWith(this.Netease.toLowerCase())) {
            return this.Netease;
        }
        let _CurrentBranch = window[AkContext.Keys.BranchKey];
        if (_CurrentBranch) {
            return _CurrentBranch;
        } else {
            try {
                _CurrentBranch = window.localStorage.getItem(AkContext.Keys.BranchKey);
            } catch (e) {
                _CurrentBranch = CacheHelper.getCooike(AkContext.Keys.BranchKey, true);
            }
            if (_CurrentBranch) {
                return _CurrentBranch;
            }
            if (window.location.href.toLocaleLowerCase().indexOf(".sharepoint.")) {
                return this.SPOnline;
            } else {
                return this.YeeOffice;
            }
        }
    }

    /**
     * 设置当前版本分支
     */
    static setCurrentBranch(branch: string) {
        window[AkContext.Keys.BranchKey] = branch;
        try {
            CacheHelper.setLocalStorage(AkContext.Keys.BranchKey, branch);
        } catch (e) {
            CacheHelper.setCooikes(AkContext.Keys.BranchKey, branch)
        }
    }

    /**
     * 获取appurl
     * @param appkey 为空默认获取当前的appkey
     */
    static getAppInfoAPI_URL(appkey?: string): string {
        var app = AkContext.getAppinfoByAppKey(appkey)
        if (app) {
            return app.MerchantAPIUrl;
        }
        return "";
    }

    /**
     * 根据AppKey获取对应的App
     * @param appkey AppKey
     */
    static getAppinfoByAppKey(appkey?: string): AppInfoList {
        if (!appkey) {
            appkey = this.getAppKey();
        }
        if (!AppKeys.isAppkey(appkey)) {
            return null;
        }
        let _app: AppInfoList = null;
        let list: AppInfoList[] = CacheHelper.getLocalStorage(AkContext.Keys.AppInfoKey);
        if (!list) {
            list = [];
        } else if (typeof list === "string") {
            list = JSON.parse(list);
        }

        list.forEach(app => {
            if (app.AppKey === appkey) {
                _app = app;
                ;
                return _app;
            }
        })
        if (!_app) {
            //判断app 是否安装,如果没有就代表未true id=-1.代表未安装 
            if (list.length > 0) {
                console.log(appkey + " not install");
                return _app;
            } else {
                if (AkContext.RefreshCount <= 0) {
                    return _app;
                } else {
                    if (AkContext.refreshAPIMappings()) {
                        _app = AkContext.getAppinfoByAppKey(appkey)
                    }
                }


            }
        }
        return _app;
    }
    /**
     * 刷新当前企业以及用户缓存数据
     */
    static RefreshAKTokenData(): MerchatLoginModel {
        MerchatAPI.RefreshAKTokenData();
        return CacheHelper.getLocalStorage(AkContext.Keys.TokenDataKey, true)
    }
    /**
     * 获取appid
     * @param appkey 为空默认获取当前的appkey
     */
    static getAppInfoID(appkey?: string): number {
        var app = AkContext.getAppinfoByAppKey(appkey)
        if (app) {
            return app.AppInfoID;
        }
        return -1;
    }

    /** 获取登录信息 */
    static getMerchantData(): MerchatLoginModel {
        return CacheHelper.getLocalStorage(AkContext.Keys.TokenDataKey);
    }

    /**
     * 判断sharepoint版本 .net登陆
     */
    static IsSPLoginByNet(): boolean {
        var falg = false;
        if (this.getAccountType() === 999) {
            return false;
        }
        var clientInfo: ClientInfo[] = [];
        let model = AkContext.getMerchantData();
        //如果数据不存在了，重新请求
        if (model && model.CompanyInfo) {
            clientInfo = model.CompanyInfo.ClientInfoList;
        }
        clientInfo.forEach(element => {
            if (element.ClientType === 999) {
                falg = true;
            }
        });
        return falg;
    }

    /**
     * 获取 当前用户信息 ,兼容两个版版本
     */
    static getUser(): UserModel {
        let model = AkContext.getMerchantData();
        //如果数据不存在了，重新请求
        if (model && model.UserModel) {
            return model.UserModel;
        }
        if (AkContext.RefreshCount <= 0) {
            return null;
        } else {
            if (AkContext.refreshAPIMappings()) {
                return AkContext.getUser();
            };
        }
        return null;
    }

    /**
     * 获取 当前用户登陆类型
     */
    static getAccountType(): Number {
        let model = AkContext.getMerchantData();
        //如果数据不存在了，重新请求
        if (model && model.AccountType) {
            return model.AccountType;
        }
        if (AkContext.RefreshCount <= 0) {
            return null;
        } else {
            if (AkContext.refreshAPIMappings()) {
                return AkContext.getAccountType()
            };
        }
        return null;
    }


    /**
     * 获取 当前公司信息 ,兼容两个版版本
     */
    static getCompanyInfo(): CompanyInfo {
        let model = AkContext.getMerchantData();
        //如果数据不存在了，重新请求
        if (model && model.CompanyInfo) {
            return model.CompanyInfo;
        }
        if (AkContext.RefreshCount <= 0) {
            return null;
        } else {
            if (AkContext.refreshAPIMappings()) {
                return AkContext.getCompanyInfo()
            };
        }
        return null;
    }

    /**
     * 获取当前语言,如果用户没有选择，默认走公司语言,兼容两个版版本
     */
    static getLanguage(): string {
        let language = CacheHelper.getCooike(AkContext.Keys.LanguageKey, true) === "undefined" ? undefined : CacheHelper.getCooike(AkContext.Keys.LanguageKey, true);
        if (!language) {
            let data = AkContext.getMerchantData();
            if (data) {
                language = data.CompanyInfo.LanguageCode;
            }
        }
        return language;
    }

    /**
     * 设置多语言
     * @param language  多语言
     */
    static setLanguage(language: string) {
        var secretExpiresDate = new Date();
        secretExpiresDate.setTime(secretExpiresDate.getTime() + (3600 * 24 * 365 * 1000));
        if (AkContext.isYeeFlow()) {
            //yeeflow 登陆语言cookie写到了根站点
            var _domainstsr = location.host.split(".");
            var _domainurl = '.' + _domainstsr[_domainstsr.length - 2] + "." + _domainstsr[_domainstsr.length - 1];
            CacheHelper.setCooikes(AkContext.Keys.LanguageKey, language.toLowerCase(), {
                path: '/',
                expires: secretExpiresDate,
                domain: _domainurl
            }, true);
        } else {
            //yeeoffice 登陆语言cookie写在当前站点下
            CacheHelper.setCooikes(AkContext.Keys.LanguageKey, language.toLowerCase(), {
                path: '/',
                expires: secretExpiresDate
            }, true);
        }
    }

    /**
     * 刷新前台缓存
     */
    static refreshAPIMappings() {
        if (AkContext.YeeOffice === AkContext.getBranch()) {
            if (AkGlobal.intl) {
                AkNotification.error({
                    message: AkGlobal.intl.formatMessage({ id: CommonLocale.Tip }),
                    description: AkGlobal.intl.formatMessage({ id: CommonLocale.CacheDataBroken })
                });
            }
            window.location.href = AkContext.getCloseConnectionURI();
            throw 'not find cache';
        } else {
            AkContext.RefreshCount--;
            MerchatAPI.merchatLogin();
            if (AkContext.getMerchantData()) {
                MerchatAPI.getMerchatInfo();
                if (AkContext.getAppinfobyLocalStorage().length > 0) {
                    AkContext.RefreshCount = 3;
                    return true;
                }
            }

        }
    }

    /**
     * 获取当前Token
     */
    static getToken() {
        if (AkContext.getBranch() === AkContext.YeeOffice) {
            return null;
        } else {
            return CacheHelper.getCooike(AkContext.Keys.LoginSecretKey, true);
        }
    }

    /**
     * 获取app列表
     */
    static getAppinfobyLocalStorage(): AppInfoList[] {
        var apps = CacheHelper.getLocalStorage(AkContext.Keys.AppInfoKey);
        return apps ? apps : [];
    }

    /**
     * 获取注销URl
     */
    static getSignOutURI() {
        CacheHelper.clearAll();
        if (AkContext.getBranch() === AkContext.YeeOffice) {

            return window["SignOutURI"];
        } else {
            return window["_spPageContextInfo"].siteAbsoluteUrl + '/_layouts/15/SignOut.aspx';
        }
    }

    /**
     * 获取用户默认头像
     */
    static getUserDefaultPhoto() {
        return URLHelper.GetFileUrl(AkConstants.DefaultUserIcon);
    }
    /**
     * 获取系统通知图标
     */
    static getSystemNoticeIcon() {
        return URLHelper.GetFileUrl(AkConstants.DefaultNoticeIcon);
    }

    /**
     * 获取分割线图片
     */
    static getUserFlowBlinkIcon() {
        return URLHelper.GetFileUrl(AkConstants.DefaultFlowBlinkIcon);
    }

    /**
     * 获取签名图片
     */
    static getUserSingerIcon() {
        return URLHelper.GetFileUrl(AkConstants.DefaultSingerIcon);
    }

    /**
     * 获取流程默认图片
     */
    static getUserFlowDefaultIcon() {
        return URLHelper.GetFileUrl(AkConstants.DefaultFlowDefaultIcon);
    }

    /**
     * 新建流程程默认图片
     */
    static getUserNewFlowIcon() {
        return URLHelper.GetFileUrl(AkConstants.DefaultNewFlowIcon);
    }

    /**
     * 获取切换用户URl
     */
    static getCloseConnectionURI() {
        CacheHelper.clearAll();
        if (AkContext.getBranch() === AkContext.YeeOffice) {
            return window["SignOutURI"];
        } else {
            return window["_spPageContextInfo"].siteAbsoluteUrl + '/_layouts/closeConnection.aspx?loginasanotheruser=true&Source=' + encodeURIComponent(window.location.href);
        }
    }

    /**
     * 获取当前版本
     */
    static getAppLevel(appKey?: string) {
        const appInfo = AkContext.getAppinfoByAppKey(appKey);
        const appExt = (appInfo && appInfo.AppExt) ? JSON.parse(appInfo.AppExt.toString()) : null;
        if (appExt && appExt.Level !== null && appExt.Level !== undefined && appExt.Level !== "") {
            return appExt.Level;
        }
        else {
            return MerchantLevelEnum.Ultimate;
        }
    }

    static async getOrganizations() {
        const { identity: { organizationLoaded, organizations } } = AkGlobal.store.getState();
        if (!organizationLoaded) {
            AkGlobal.store.dispatch(IdentityAction.requestOrganizations());
            REFRESS_COUNT_ORG--;
            if (REFRESS_COUNT_ORG > 0) {
                return await AkUtil.timeoutToPromise(AkContext.getOrganizations, REFRESS_TIMEOUT);
            } else {
                if (AkGlobal.intl) {
                    const { formatMessage } = AkGlobal.intl;
                    //超过重试次数，提示错误
                    AkNotification.error({
                        message: formatMessage({ id: CommonLocale.Tip }),
                        description: formatMessage({ id: CommonLocale.GetInfoError })
                    })
                }
                return [];
            }
        } else {
            return organizations;
        }
    }

    static async getLocations() {
        const { identity: { locationLoaded, locations } } = AkGlobal.store.getState();
        if (!locationLoaded) {
            AkGlobal.store.dispatch(IdentityAction.requestLocations());
            REFRESS_COUNT_LOC--;
            if (REFRESS_COUNT_LOC > 0) {
                return await AkUtil.timeoutToPromise(AkContext.getLocations, REFRESS_TIMEOUT);
            } else {
                if (AkGlobal.intl) {
                    const { formatMessage } = AkGlobal.intl;
                    //超过重试次数，提示错误
                    AkNotification.error({
                        message: formatMessage({ id: CommonLocale.Tip }),
                        description: formatMessage({ id: CommonLocale.GetInfoError })
                    })
                }
                return [];
            }
        } else {
            return locations;
        }
    }

    static async getGroups() {
        const { identity: { groupLoaded, groups } } = AkGlobal.store.getState();
        if (!groupLoaded) {
            AkGlobal.store.dispatch(IdentityAction.requestGroups());
            REFRESS_COUNT_GRP--;
            if (REFRESS_COUNT_GRP > 0) {
                return await AkUtil.timeoutToPromise(AkContext.getGroups, REFRESS_TIMEOUT);
            } else {
                if (AkGlobal.intl) {
                    const { formatMessage } = AkGlobal.intl;
                    //超过重试次数，提示错误
                    AkNotification.error({
                        message: formatMessage({ id: CommonLocale.Tip }),
                        description: formatMessage({ id: CommonLocale.GetInfoError })
                    })
                }
                return [];
            }
        } else {
            return groups;
        }
    }

    static async getPositions() {
        const { identity: { positionLoaded, positions } } = AkGlobal.store.getState();
        if (!positionLoaded) {
            AkGlobal.store.dispatch(IdentityAction.requestPositions());
            REFRESS_COUNT_POS--;
            if (REFRESS_COUNT_POS > 0) {
                return await AkUtil.timeoutToPromise(AkContext.getPositions, REFRESS_TIMEOUT);
            } else {
                if (AkGlobal.intl) {
                    const { formatMessage } = AkGlobal.intl;
                    //超过重试次数，提示错误
                    AkNotification.error({
                        message: formatMessage({ id: CommonLocale.Tip }),
                        description: formatMessage({ id: CommonLocale.GetInfoError })
                    })
                }

                return [];
            }
        } else {
            return positions;
        }
    }

    /**
     * 获取用户信息
     */
    static async getUsersInfo(users: string[]) {
        const { identity: identityDict } = AkGlobal.store.getState();
        let usersInfo: AkUser[] = [];
        const getUsersID = () => {
            let reqIDs = [];
            users.forEach(item => {
                const identity = identityDict[item];
                if (identity && !identity.loading)
                    usersInfo.push(identityDict.data);
                else
                    reqIDs.push({ ID: item, Type: AkIdentityType.User });
            });
            return reqIDs;
        }
        const reqIDs = getUsersID();
        if (reqIDs.length !== 0) {
            const rs = await IdentityAPI.resolveIdentities({ identities: reqIDs });
            let requireUpdateStore = [];
            if (rs.Status === 0) {
                AkUtil.each(rs.Data, r => {
                    const u = new AkUser(r);
                    usersInfo.push(u);
                    requireUpdateStore.push(u)
                });
                AkGlobal.store.dispatch(IdentityAction.identityUserLoaded(requireUpdateStore));
            } else {
                throw "user not loaded:" + rs.Message;
            }

        }
        return usersInfo;
    }

    /**获取是否是yeeflow版本 */
    static isYeeFlow() {
        return process.env.Version === "yeeflow"
    }

    /**获取是否是新版本商户 */
    static isNewVersion() {
        const loginData = AkContext.getMerchantData();
        //是否是新商户
        const Attr = loginData && loginData.CompanyInfo && loginData.CompanyInfo.Attr;
        let isNewVersion = false;
        if (Attr && JSON.parse(Attr) && JSON.parse(Attr).IsNewVersion) {
            isNewVersion = true;
        }
        return isNewVersion
        // return true
    }

    /** 计算剩余订阅天数*/
    static diffDays(startdate?: string, enddate?: string) {
        //不传时间默认当前时间
        //四舍五入
        const start = startdate ? moment(startdate, AkDatetimeUtil.defaultFormat) : moment();
        const end = enddate ? moment(enddate, AkDatetimeUtil.defaultFormat) : moment();
        return end.diff(start, "days");
    }
}
/*
 * 导航处理
 */
export class NavigationHelper {


    /**
     * SP版本 自动切换.net版本导航地址(如果是需要跨模块或页面跳转，请使用改方法包装)
     * 例如:   entry.YunGalaxyNavigationUrl = NavigationHelper.getYeeOfficeNavigationUrl(app.AppKey, entry.YunGalaxyNavigationUrl)
     *  这样就会自动兼容 .net版本域SP版本跳转了
     * @param appkey  需要跳转的模块key
     * @param url   默认需要跳转的url  例如  sitepages/page/index.apsx 或 home/index
     * @param url  ?号附带的参数 例如: ?token=xxxxx
     */
    static getNavigationUrl(appkey: string, url: string, param: string = "") {
        if (AkContext.getBranch() === AkContext.YeeOffice) {
            switch (appkey) {
                case AppKeys.Flowcraft:
                    url = "/Home/workflow";
                    break;
                case AppKeys.YeeOfficePortal:
                    url = "/Home/portal";
                    break;
                case AppKeys.ProjectManagement:
                    url = "/Home/project";
                    break;
                case AppKeys.SalesManagement:
                    url = "/Home/crm";
                    break;
                case AppKeys.YeeOfficeContacts:
                    url = "/Home/address";
                    break;
                case AppKeys.YeeOfficeDocument_Net:
                    url = "/Home/document";
                    break;
                case AppKeys.YeeOfficeDocument:
                    url = "/Home/document";
                    break;
                case AppKeys.YeeOfficeSettings:
                    url = "/Home/settings";
                    break;
                case AppKeys.YeeOfficeKnowledge:
                    url = "/Home/knowledge";
                    break;
                default:
                    url = "/Home/Index";
                    break;
            }
        } else {
            let domain = AkContext.getAppinfoByAppKey(appkey).InstallUrl;
            url = domain + "/" + (url ? url : "SitePages/Pages/index.aspx");
        }
        return url + param;
    }



    /**
     * 获取当前APP的安装域名
     * @param appkey  appkey
     */
    static getInstallDomain(appkey: string): string {
        var url = "";
        let _app = AkContext.getAppinfoByAppKey(appkey);
        if (_app) {
            url = _app.InstallUrl
        }
        return url;
    }

    /**
     * 获取当前APP的安装域名
     * @param appkey  appkey
     */
    static getInstallDomainByID(appid: number): string {
        let url = "";
        let list = AkContext.getAppinfobyLocalStorage();
        if (!list) {
            list = [];
        }
        list.forEach(app => {
            if (app.AppInfoID === appid) {
                url = app.InstallUrl;
                return app.InstallUrl;
            }
        })
        if (url === "") {
            //判断app 是否安装,如果没有就代表未true id=-1.代表未安装 
            if (list.length > 0) {
                console.log(appid + " not install");
                return url;
            } else {
                if (AkContext.RefreshCount <= 0) {
                    return url;
                } else {
                    if (AkContext.refreshAPIMappings()) {
                        url = NavigationHelper.getInstallDomainByID(appid);
                    };
                }
            }
        }
        return url;
    }

    /**
     * 拼接当前app的页面地址
     * @param appkey  appkey
     * @param pageUrl  页面地址
     */
    static getAppPageUrl(appkey: string, pageUrl: string): string {
        if (!AppKeys.isAppkey(appkey)) {
            return "";
        }
        let url = "";
        let list = AkContext.getAppinfobyLocalStorage();
        if (!list) {
            list = [];
        }
        list.forEach(app => {
            if (app.AppKey === appkey) {
                url = app.InstallUrl;
                return app.InstallUrl;
            }
        })
        if (url === "") {
            //判断app 是否安装,如果没有就代表未true id=-1.代表未安装 
            if (list.length > 0) {
                console.log(appkey + " not install");
                return url;
            } else {
                if (AkContext.RefreshCount <= 0) {
                    return url;
                } else {
                    if (AkContext.refreshAPIMappings()) {
                        url = NavigationHelper.getAppPageUrl(appkey, pageUrl);
                    };
                }
            }

        }
        return url + "/" + pageUrl;
    }

    /**
     * 获取欢迎首页
     */
    static getWelcomePage() {
        if (AkContext.getBranch() === AkContext.YeeOffice) {
            return NavigationHelper.getNavigationUrl("", "");
        } else {
            return NavigationHelper.getAppPageUrl(AppKeys.YeeOfficeBase, "SitePages/Pages/index.aspx")
        }
    }

    /**
     * 获取接口domain
     */
    static getAPIDomain(AppKey?) {
        if (!AppKey) {
            AppKey = AkContext.getAppKey();
        }

        const APIURL = AkContext.getAppInfoAPI_URL(AppKey);
        let Domain;

        if (APIURL.startsWith("http://") || APIURL.startsWith("https://")) {
            Domain = APIURL.split("/").slice(0, 3);
        } else {
            Domain = APIURL.split("/").slice(0, 1);
        }

        return Domain.join("/");
    }


    /**获取地址list */
    static getInstallList() {
        let list: AppInfoList[] = CacheHelper.getLocalStorage(AkContext.Keys.AppInfoKey);
        if (!list) {
            list = [];
        } else if (typeof list === "string") {
            list = JSON.parse(list);
        }
        const filterArrary = [AppKeys.Flowcraft, AppKeys.YeeOfficePortal, AppKeys.ProjectManagement, AppKeys.SalesManagement,
        AppKeys.YeeOfficeContacts, AppKeys.YeeOfficeDocument, AppKeys.YeeOfficeSettings, AppKeys.YeeOfficeKnowledge, AppKeys.YeeOfficeBase];
        return list.filter(i => filterArrary.indexOf(i.AppKey) > -1);
    }

    /**
     * 根据地址获取AppKey
     * 文档中心获取的是sp版本的AppKey
     **/
    static getNetAppKeybyUrl(url) {
        const appUrlList = ["/Home/workflow", "/Home/portal", "/Home/project", "/Home/crm", "/Home/address", "/Home/document", "/Home/settings", "/Home/knowledge"];
        const appUrl = appUrlList.find(i => url.toLocaleLowerCase().indexOf(i.toLocaleLowerCase()) > -1);
        let key;
        switch (appUrl) {
            case "/Home/workflow":
                key = AppKeys.Flowcraft;
                break;
            case "/Home/portal":
                key = AppKeys.YeeOfficePortal;
                break;
            case "/Home/project":
                key = AppKeys.ProjectManagement;
                break;
            case "/Home/crm":
                key = AppKeys.SalesManagement;
                break;
            case "/Home/address":
                key = AppKeys.YeeOfficeContacts;
                break;
            case "/Home/document":
                key = AppKeys.YeeOfficeDocument;
                break;
            case "/Home/settings":
                key = AppKeys.YeeOfficeSettings;
                break;
            case "/Home/knowledge":
                key = AppKeys.YeeOfficeKnowledge;
                break;
            default:
                key = AppKeys.YeeOfficeBase;
                break;
        }
        return key;
    }
    /**
     * 处理项目自定义页面上移动端支持
     */
    static ProjectCustomURL(url: string, rs, pos) {
        if (url.toLocaleLowerCase().indexOf("cgtn.aspx") > -1 || url.toLocaleLowerCase().indexOf("cgtv-process.aspx") > -1) {
            rs = NavigationHelper.getAPIDomain() + "/home/cgtn#" + url.substr(pos);
        }
        return rs;
    }
    static parseURL(url: string) {
        let rs = url;
        if (!url) return url;
        //sp转net
        if (AkContext.IsSPLoginByNet()) {
            const reg = /(http|ftp|https):\/\/.*?(sharepoint.com|sharepoint.cn).*?(#|\?\_hash\_\=)/i;
            if (reg.test(url)) {
                let pos = url.indexOf('?_hash_=');
                if (pos < 0) {
                    pos = url.indexOf('#') + 1;
                } else {
                    pos += 8;
                }
                rs = NavigationHelper.ProjectCustomURL(url, rs, pos)
                if (url.toLocaleLowerCase().indexOf("apps.aspx") > -1) {
                    //app页面
                    rs = NavigationHelper.getAPIDomain() + NavigationHelper.getNavigationUrl(AppKeys.YeeOfficeBase, "", "") + "#" + url.substr(pos);
                } else {
                    const list = NavigationHelper.getInstallList();
                    list.forEach(item => {
                        console.log(url.toLocaleLowerCase().indexOf(item.InstallUrl.toLocaleLowerCase()) > -1, url, item);
                        if (url.toLocaleLowerCase().indexOf(item.InstallUrl.toLocaleLowerCase()) > -1) {
                            rs = NavigationHelper.getAPIDomain() + NavigationHelper.getNavigationUrl(item.AppKey, "", "") + "#" + url.substr(pos);
                        }
                    });
                }
            }
        }
        //net转sp
        if (AkContext.getBranch() === AkContext.SPOnline) {
            if (url.indexOf(NavigationHelper.getAPIDomain()) > -1) {
                let pos = url.indexOf('?_hash_=');
                if (pos < 0) {
                    pos = url.indexOf('#') + 1;
                } else {
                    pos += 8;
                }
                const list = NavigationHelper.getInstallList();
                const installApp = list.find(i => i.AppKey === NavigationHelper.getNetAppKeybyUrl(url));
                let DefaultPageUrl = "SitePages/Pages/Apps.aspx";
                if (installApp.DefaultPageUrl.toLocaleLowerCase() !== DefaultPageUrl.toLocaleLowerCase()) {
                    DefaultPageUrl = "SitePages/Pages/index.aspx";
                }
                rs = installApp.InstallUrl + "/" + DefaultPageUrl + "#" + url.substr(pos);
            }
        }
        return rs;
    }
}

//统一 报错警告提示消息，成功消息
export class AkCommonMsg {
    static Warning(args: AkArgsProps) {
        AkNotification.warning(args);
    }

    static Success(content: React.ReactNode, duration?: number | undefined, onClose?: AkConfigOnClose | undefined) {
        AkMessage.success(content, duration, onClose);
    }
}

export class AppUtil {
    /**添加新属性至扩展字段
     *object为源字段，支持对象和对象字符串
     *props为新增的属性
     *value为新增的值
     *nostringifyObject为是否不stringify值为object
     */
    static SetValueInObject(object: any, props: string, value: any, nostringifyObject?: boolean): string {
        let set;

        if (typeof object === "object") {
            //object and null
            set = object || {}
        } else if (typeof object === "string") {
            //object string
            set = JSON.parse(object)
        } else {
            //undefind
            set = {}
        }

        if (typeof value === "object") {
            if (nostringifyObject) {
                set[props] = value;
            } else {
                set[props] = JSON.stringify(value);
            }
        } else {
            set[props] = value;
        }
        return JSON.stringify(set);
    }
}
//富文本扩展
export class FuWenBenExpand {
    /**计算两指间的距离 */
    static getDistance(p1, p2) {
        var x = p2.pageX - p1.pageX,
            y = p2.pageY - p1.pageY;
        return Math.sqrt((x * x) + (y * y));
    };
    /**监听移动端滑动缩放事件，屏蔽整体页面缩放 */
    static imgView() {
        window["mobilefangdatupianClose"] = function () {
            document.getElementById("fangdatupian").setAttribute("src", "");
            document.getElementById("mobilefangdatupian").style.setProperty("display", "none");

        };
        window["badingimgClick"] = function (className?: string) {
            //if(window.innerWidth>768){return;}
            var divs = document.getElementsByClassName(className ? className : "ak-main-content-textalign");
            for (let index = 0; index < divs.length; index++) {
                const element = divs[index];
                const imgs = element.getElementsByTagName("img");
                for (let imgindex = 0; imgindex < imgs.length; imgindex++) {
                    const imgelement = imgs[imgindex];
                    imgelement.onclick = function () {
                        document.getElementById("mobilefangdatupian").style.setProperty("display", "block");
                        var src = imgelement.getAttribute("src")
                        document.getElementById("fangdatupian").setAttribute("src", src);
                        //添加监听事件
                        var pageX, pageY, initX, initY, isTouch = false, enbledOne = false, scale = 1;
                        var start = [];
                        var _oldDistance;
                        document.getElementById("fangdatupian").addEventListener("touchstart", function (e: any) {
                            pageX = e.targetTouches[0].pageX;
                            pageY = e.targetTouches[0].pageY;
                            initX = e.target.offsetLeft;
                            initY = e.target.offsetTop;
                            if (e.touches.length >= 2) {
                                start = e.touches;
                            };
                            isTouch = true;
                        }, false);
                        //监听 touchmove 事件 手指 移动时 做的事情
                        document.getElementById("fangdatupian").addEventListener("touchmove", function (e: any) {
                            e.preventDefault();
                            if (e.touches.length === 1 && isTouch && !enbledOne && scale > 1) {
                                var touchMoveX = e.targetTouches[0].pageX,
                                    touchMoveY = e.targetTouches[0].pageY;
                                e.target.style.left = parseInt(touchMoveX) - parseInt(pageX) + parseInt(initX) + "px";
                                e.target.style.top = parseInt(touchMoveY) - parseInt(pageY) + parseInt(initY) + "px";
                            };
                            if (e.touches.length >= 2) {
                                var now = e.touches;
                                var nowDistance = (FuWenBenExpand.getDistance(now[0], now[1]) / FuWenBenExpand.getDistance(start[0], start[1]));
                                if (_oldDistance > nowDistance) {
                                    if (scale <= 1) {
                                        e.target.style.left = "50%";
                                        e.target.style.top = "50%";
                                        e.target.style.transform = "translate(-50%, -50%) scale(1)";
                                        return;
                                    }
                                    scale = scale - 0.2;
                                } else {
                                    scale = scale + 0.2;
                                }
                                _oldDistance = nowDistance;
                                e.scale = scale.toFixed(2);
                                if (scale <= 1) {
                                    scale = 1;
                                    enbledOne = true;
                                    e.target.style.left = "50%";
                                    e.target.style.top = "50%";
                                } else {
                                    if (scale > 4.5) { scale = 4.5; }
                                    enbledOne = false;
                                }
                                e.target.style.transform = "translate(-50%, -50%) scale(" + scale + ")";
                            };
                        }, false);
                        document.getElementById("fangdatupian").addEventListener("touchend", function (e) {
                            if (isTouch) { isTouch = false; }
                        }, false);
                    };
                }
            }
        }
    }
}