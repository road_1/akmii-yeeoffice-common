import * as request from "superagent";
import { AkContext } from "../index";

export interface SpRequestParam {
    Url?: string;
    Query?: Object;
    Data?: any;
    Headers?: {
        [key: string]: string
    }[];
    IgnoreError?: boolean;
}

export class SpRequest {
    /**
     * 默认参数
     *
     * @type {SpRequestParam < TRequest >}
     * @memberOf Request
     */
    defaultParam: SpRequestParam = {
        Url: '',
        Query: null,
        Data: null,
        //判断.net版本 SP访问
        Headers:[],
        IgnoreError: false,
    }

    /**
     * 处理Http请求
     *
     * @param {(url : string) => request.SuperAgentRequest} func
     * @param {AkRequestParam < TRequest >} [param]
     * @returns
     *
     * @memberOf Request
     */
    async processResponse(func: (url: string) => request.SuperAgentRequest, param?: SpRequestParam, emptyData?: any) {
        let request = func(param.Url);
        if (param.Query) {
            request.query(param.Query);
        }
        if (param.Data) {
            request.send(param.Data);
        }
        param.Headers.push({"accept":"application/json;odata=verbose"});
        if(AkContext.isNetSPDocument())
        {
            param.Headers.push({"Authorization":"Bearer " + AkContext.getNetSPToken()});
        
        } else{
            param.Headers.push({"x-requestdigest": (document.getElementById("__REQUESTDIGEST") as HTMLInputElement).value}); 
        }
        if (param.Headers) {
            param.Headers.forEach((entry) => {
                request.set(entry);
            });
        }

        return new Promise((resolve: (result: any) => void, reject: (error: any) => void) => {
            request.end((error: any, response: request.Response) => {
                if (response.status === 200)
                    resolve(response.body);
                else
                    reject(error);
            });
        });
    }

    /**
     * 构造请求数据
     *
     * @param {IArguments} args
     * @returns {AkRequestParam < TRequest >}
     *
     * @memberOf Request
     */
    buildData(args: IArguments | any[]): SpRequestParam {
        if (typeof (args[0]) === "string") {
            return Object.assign({}, this.defaultParam, {
                Url: args[0],
                Data: args[1]
            })
        } else {
            return Object.assign({}, this.defaultParam, args[0])
        }
    }

    buildQueryParam(url, data) {
        if (data) {
            url += "?";
            Object.getOwnPropertyNames(data).forEach(name => {
                {
                    url += name + "=" + encodeURI(data[name]) + "&";
                }
            });
            url = url.slice(0, url.length - 1);
        }
        return url;
    }

    /**
     * Get method
     * @param url
     * @param data
     * @param emptyData response中的空数据，在服务器返回错误时保证前端功能
     * @returns {Promise<any>}
     */
    get(url: string, data?: any, emptyData?: any): Promise<any> {
        return this.processResponse(request.get, this.buildData([
            this.buildQueryParam(url, data),
            data
        ]), emptyData)
    }

    /**
     * Post method
     * @param url
     * @param data
     * @param emptyData response中的空数据，在系统错误时保证前端功能
     * @returns {Promise<any>}
     */
    post(url: string, data?: any, emptyData?: any): Promise<any> {
        return this.processResponse(request.post, this.buildData(arguments), emptyData);
    }

    /**
     * Put method
     * @param url
     * @param data
     * @param emptyData response中的空数据，在系统错误时保证前端功能
     * @returns {Promise<any>}
     */
    put(url: string, data?: any, emptyData?: any): Promise<any> {
        return this.processResponse(request.put, this.buildData(arguments), emptyData);
    }

    /**
     * Delete method
     * @param url
     * @param data
     * @param emptyData response中的空数据，在系统错误时保证前端功能
     * @returns {Promise<any>}
     */
    del(url: string, data?: any, emptyData?: any): Promise<any> {
        return this.processResponse(request.delete, this.buildData([
            this.buildQueryParam(url, data),
            data
        ]), emptyData);
    }
}
