import { } from './ActionTypes';
import { createAction } from "redux-actions";
import {
    REQUEST_START_CHAT,
    REQUEST_CHAT_MESSAGE,
    CHECK_FORM_CACHE,
    REQUEST_TASKCOUNT,
    UPDATE_APPCENTER_LIST,
    SELECT_APPCENTER,
    UPDATA_SELECT_APPCENTER,
    REQUEST_APPCENTER_LIST,
    UPDATE_PER,
    FILE_LIBRARY_REQUEST,
    FILE_LIBRARY_LOADED,
    COMMENT_ADD,
    COMMENT_DELETE,
    COMMENT_MORE,
    COMMENT_LOADED,
    UPDATE_SYSTEM_APP_LIST,
    UPDATE_PORTAL_LIST,
    MASTERPAGE_SHOW_SIDER_ICON,
    MASTERPAGE_SHOW_SIDER,
    COMMON_USERBASEINFO,
    COMMON_NOTIFICATION,
    UPDATE_NAVIGATION_LIST,
    REQUEST_NAVIGATION_LIST,
    MASTERPAGEHEADER_TRIGGER,
    MASTERPAGEFOOTER_TRIGGER,
    MASTERPAGECONTENT_MIN_HEIGHT,
    REQUEST_USERS,
    REQUEST_USERS_BEGIN,
    REQUEST_ORGANIZATIONS,
    REQUEST_LOCATIONS,
    REQUEST_POSITIONS,
    REQUEST_GROUPS,
    IDENTITY_USER_LOADED,
    IDENTITY_ORGANIZATION_LOADED,
    IDENTITY_ORGANIZATION_SHOULD_LOAD,
    IDENTITY_LOCATION_LOADED,
    IDENTITY_LOCATION_SHOULD_LOAD,
    IDENTITY_GROUP_LOADED,
    IDENTITY_GROUP_SHOULD_LOAD,
    IDENTITY_POSITION_LOADED,
    IDENTITY_POSITION_SHOULD_LOAD,
    METADATA_REQUEST,
    METADATA_LOADED,
    METADATA_REQUEST_BEGIN,
    METADATA_REQUEST_BYID,
    METADATA_CATEGORY_REQUEST,
    METADATA_CATEGORY_LOADED,
    CONTENT_LIST_REQUEST,
    CONTENT_LIST_LOADED,
    CONTENT_FIELD_REQUEST,
    CONTENT_FIELD_LOADED,
    REQUEST_APPINFO,
    APPINFO_LOADED,
    REQUEST_HISTORYLOG,
    HISTORYLOG_LOADED,
    REQUEST_CHAT_PULL,
    RECEIVE_UPDATE_MESSAGE,
    SAVE_CHAT_ITEM,
    REQUEST_CHAT_MESSAGE_LIST,
    SHOW_CHAT_ITEM,
    CREATE_CHAT,
    REQUEST_CHAT_LIST,
    RECEIVE_CHAT_LIST,
    SHOW_CHAT_LIST,
    REQUEST_CHAT_DETAIL,
    SHOW_CHAT_INFO,
    RECEIVE_CHAT_DETAIL,
    SHOW_CHAT_DETAIL,
    REQUEST_QUIT_GROUP,
    REQUEST_CHANGE_IS_TOP,
    REQUEST_GROUP_NAME,
} from "./ActionTypes";
import { AkOrganization, AkLocation, AkGroup, AkPosition, AkUser, AkIdentity } from "../api/identity/identity";
import { MetadataModel, MetadataCategoryInfo } from "../api/flowcraft/metadata";
import { ChatPullModel, ChatMessagePullModel } from "../api/masterpage/socketmodel";
import { FORM_DELAY_UPDATE, REQUEST_ADD_GROUP_PERSON, REQUEST_DELETE_GROUP_PERSON, RECEIVE_UPDATE_GROUP_INFO, REQUEST_HISTORY_MESSAGE, RECEIVE_REMOVE_GROUP_PERSON, RECEIVE_ADD_GROUP_PERSON, RECEIVE_IS_TOP, SHOW_CREATE_WORKFLOW_MODAL } from './ActionTypes';
import { AppCenterListResponse } from '../api/appcenter/appcentermodal';
import { AppCenterPermissionEnum, NavigationModel, AkDictionaryString } from '..';
import { ContentListType, CustomListMgrAddRequest, ListSetSortModal } from '../api/content-list/content-list-model';
import { CommentDeleteRequest, CommentAddRequest, CommentMoreRequest } from '../api/comment/commentmodel';
import { CommentHolder } from '../reducers/commentReducer';
import { PortalViewModel } from '../components/masterpage/navigator';
import { AkArgsProps } from '../components/controls/ak-notification';
import { UPDATE_LISTSET_DEF, REQUEST_LISTSET_LISTS, UPDATE_LISTSET_LISTS, UPDATE_LISTSET_LOADING, UPDATE_LISTSET_PAGE, REQUEST_LISTSET_PAGE, REQUEST_LISTSET_DEF, REQUEST_ALL_DEF_OF_LISTSET, SAVE_LIST_SET_SORT_DEF } from './listSetActionTypes';

export class CommonAction {
    static NOTIFY_TYPE_ERROR = "error";
    static NOTIFY_TYPE_INFO = "info";
    static NOTIFY_TYPE_WARN = "warn";
    static NOTIFY_TYPE_WARNING = "warning";
    static error = createAction(COMMON_NOTIFICATION, (args: AkArgsProps) => { return { type: CommonAction.NOTIFY_TYPE_ERROR, args } });
    static info = createAction(COMMON_NOTIFICATION, (args: AkArgsProps) => { return { type: CommonAction.NOTIFY_TYPE_INFO, args } });
    static warn = createAction(COMMON_NOTIFICATION, (args: AkArgsProps) => { return { type: CommonAction.NOTIFY_TYPE_WARN, args } });
    static warning = createAction(COMMON_NOTIFICATION, (args: AkArgsProps) => { return { type: CommonAction.NOTIFY_TYPE_WARNING, args } });
}

export class MasterPageAction {
    /**
     * 显示头部
     */
    static triggerMasterpageHeader = createAction(MASTERPAGEHEADER_TRIGGER, (showHeader: boolean) => {
        return showHeader;
    });
    /**
     * 显示底部
     */
    static triggerMasterpageFooter = createAction(MASTERPAGEFOOTER_TRIGGER, (showFooter: boolean) => {
        return showFooter;
    });
    /**
     * 显示头部
     */
    static minHeightMasterpageContent = createAction(MASTERPAGECONTENT_MIN_HEIGHT, (minHeight: number) => {
        return minHeight;
    });

    /**显示菜单图标 */
    static showSiderIcon = createAction(MASTERPAGE_SHOW_SIDER_ICON, (show: boolean) => show);

    /**显示侧拉菜单 */
    static showSider = createAction(MASTERPAGE_SHOW_SIDER, (show: boolean) => show);

    /**显示创建流程模态框（首次注册用户） */
    static showCreateWorkFlowModal = createAction(SHOW_CREATE_WORKFLOW_MODAL, (showCreateWorkFlowModal: boolean) => {
        return showCreateWorkFlowModal;
    })
}

export class IdentityAction {
    static requestUsers = createAction(REQUEST_USERS, (arg: AkIdentity | AkIdentity[] | string | string[]) => arg);
    static requestUsersBegin = createAction(REQUEST_USERS_BEGIN, (arg: AkIdentity[]) => arg);
    static requestOrganizations = createAction(REQUEST_ORGANIZATIONS);
    static requestLocations = createAction(REQUEST_LOCATIONS);
    static requestPositions = createAction(REQUEST_POSITIONS);
    static requestGroups = createAction(REQUEST_GROUPS);
    static identityUserLoaded = createAction(IDENTITY_USER_LOADED, (org: AkUser[]) => org);
    static identityOrganizationLoaded = createAction(IDENTITY_ORGANIZATION_LOADED, (org: AkOrganization[]) => org);
    static identityOrganizationShouldLoad = createAction(IDENTITY_ORGANIZATION_SHOULD_LOAD);
    static identityLocationLoaded = createAction(IDENTITY_LOCATION_LOADED, (loc: AkLocation[]) => loc);
    static identityLocationShouldLoad = createAction(IDENTITY_LOCATION_SHOULD_LOAD);
    static identityGroupLoaded = createAction(IDENTITY_GROUP_LOADED, (loc: AkGroup[]) => loc);
    static identityGroupShouldLoad = createAction(IDENTITY_GROUP_SHOULD_LOAD);
    static identityPositionLoaded = createAction(IDENTITY_POSITION_LOADED, (loc: AkPosition[]) => loc);
    static identityPositionShouldLoad = createAction(IDENTITY_POSITION_SHOULD_LOAD);
}

export class MetadataAction {
    static requestByID = createAction(METADATA_REQUEST_BYID, (categoryID: string, parentID: string) => {
        return { categoryID: categoryID, parentID: parentID }
    });

    static request = createAction(METADATA_REQUEST, (categoryCode: string, parentCode: string) => {
        return { categoryCode: categoryCode, parentCode: parentCode }
    });

    static requestBegin = createAction(METADATA_REQUEST_BEGIN, (categoryCode: string, parentCode: string) => {
        return { categoryCode: categoryCode, parentCode: parentCode }
    });

    static loaded = createAction(METADATA_LOADED, (items: MetadataModel[], categoryCode: string, parentCode: string) => {
        return { categoryCode: categoryCode, parentCode: parentCode, items: items }
    });

    static categoryRequest = createAction(METADATA_CATEGORY_REQUEST);
    static categoryLoaded = createAction(METADATA_CATEGORY_LOADED, (items: MetadataCategoryInfo[]) => items);
}

export class ContentListAction {
    static requestList = createAction(CONTENT_LIST_REQUEST, (appID: number, title: string, type: ContentListType) => { return { appID, title, type } });
    static requestField = createAction(CONTENT_FIELD_REQUEST, (appID: number, listID: string) => { return { appID, listID } });
    static listLoaded = createAction(CONTENT_LIST_LOADED, (lists: any[]) => lists);
    static fieldLoaded = createAction(CONTENT_FIELD_LOADED, (fields, listID) => {
        return { fields, listID };
    });
}

/** 判断是否为管理员*/
export class AppAdmin {
    static requestIsAppAdmin = createAction(REQUEST_APPINFO, (appid: number) => appid);
    static isAppAdminLoad = createAction(APPINFO_LOADED, (isAppAdmin: boolean) => isAppAdmin);
}

export class NotificationAction {
    /**是否显示聊天列表 */;
    static showChatList = createAction(SHOW_CHAT_LIST, (showChatList) => showChatList);
    /** 建立连接后拉取会话 */
    static requestChatPullAction = createAction(REQUEST_CHAT_PULL);

    /** 处理拉取的会话 */
    static requestChatList = createAction(REQUEST_CHAT_LIST, (chatList: ChatPullModel[]) => chatList);
    static receiveChatList = createAction(RECEIVE_CHAT_LIST, (chatList: ChatPullModel[]) => chatList);

    /** 消息拉取 */
    static requestChatMessageList = createAction(REQUEST_CHAT_MESSAGE_LIST, (message: ChatMessagePullModel) => message);
    /** 收到发送的消息 */
    static requestChatMessage = createAction(REQUEST_CHAT_MESSAGE, (receiveMessage) => receiveMessage);

    /**更新消息已读 */
    static updataMessage = createAction(RECEIVE_UPDATE_MESSAGE, (readMessage) => readMessage);
    /**存单个会话*/
    static saveChatItem = createAction(SAVE_CHAT_ITEM, (chatItem) => chatItem);
    /**是否显示会话 */
    static showChatItem = createAction(SHOW_CHAT_ITEM, (showChatItem) => showChatItem);
    /**建立会话 */
    static createChat = createAction(CREATE_CHAT, (ids) => ids);
    /**收到创建会话 */
    static requestChat = createAction(REQUEST_START_CHAT, (chatItem) => chatItem);
    /**是否显示聊天人员信息 */
    static showChatInfo = createAction(SHOW_CHAT_INFO, (showChatInfo) => showChatInfo);
    /**拉取聊天人员名单 */
    static requestChatPerson = createAction(REQUEST_CHAT_DETAIL);
    /**接收聊天人员名单 */
    static receiveChatPerson = createAction(RECEIVE_CHAT_DETAIL, (item) => item);
    /** 展示聊天详情 */
    static showChatPerson = createAction(SHOW_CHAT_DETAIL, (chatPerson) => chatPerson);
    /**saga退出群组 */
    static requestQuitChat = createAction(REQUEST_QUIT_GROUP);
    /**saga聊天置顶 */
    static requestChangeIsTop = createAction(REQUEST_CHANGE_IS_TOP, (isTop) => isTop);
    /**修改群聊名称 */
    static requestGroupName = createAction(REQUEST_GROUP_NAME, (groupName) => groupName);
    /**添加群成员 */
    static requestAddGroupPerson = createAction(REQUEST_ADD_GROUP_PERSON, (addPersons) => addPersons);
    /**删除群成员 */
    static requestDeleteGroupPerson = createAction(REQUEST_DELETE_GROUP_PERSON, (deletePersons) => deletePersons);
    /**更新群组信息 */
    static requestUpdateGroupInfo = createAction(RECEIVE_UPDATE_GROUP_INFO, (chat) => chat);
    /**移除群成员 */
    static receiveRemoveGroupPerson = createAction(RECEIVE_REMOVE_GROUP_PERSON, (chat) => chat);
    /**添加群成员 */
    static receiveAddGroupPerson = createAction(RECEIVE_ADD_GROUP_PERSON, (chat) => chat);
    /**拉取历史消息 */
    static requestHistoryMessage = createAction(REQUEST_HISTORY_MESSAGE);
    /**更新置顶 */
    static receiveIsTop = createAction(RECEIVE_IS_TOP, (chatid) => chatid);
}
/** 判断是否加签*/
export class AddAssisignAction {
    static requestAddAssisign = createAction(REQUEST_HISTORYLOG);
    static historyLogLoad = createAction(HISTORYLOG_LOADED);
}

export class FormAction {
    static formDelayUpdate = createAction(FORM_DELAY_UPDATE, (form, values) => { return { form: form, values: values } });
    static checkFormCache = createAction(CHECK_FORM_CACHE, (handler: () => void) => { return { handler: handler } });
}

export class TaskAction {
    static requestTaskCount = createAction(REQUEST_TASKCOUNT);
}

export class AppCenterAction {
    /**系统AppList */
    static updateSystemAppList = createAction(UPDATE_SYSTEM_APP_LIST, (systemAppList: NavigationModel[]) => systemAppList);
    /**portalList */
    static updatePortalTreeData = createAction(UPDATE_PORTAL_LIST, (portalTreeData: PortalViewModel) => portalTreeData);
    /**更新AppCenter列表数据 */
    static requestAppCenterList = createAction(REQUEST_APPCENTER_LIST);
    /**更新AppCenter列表数据 */
    static updateAppCenterList = createAction(UPDATE_APPCENTER_LIST, (appcenterlist: AppCenterListResponse[]) => appcenterlist);
    /**当前选中的app */
    static selectAppCenter = createAction(SELECT_APPCENTER, (url: string, appCenterID: number) => { return { url, appCenterID } });
    /**更新当前选中的app */
    static updateSelectApp = createAction(UPDATA_SELECT_APPCENTER, (selectAppCenter: AppCenterListResponse) => selectAppCenter);
    /**App无权限 */
    static updatePer = createAction(UPDATE_PER, (per: AppCenterPermissionEnum) => per);

    /**更新导航数据 */
    static requestNavgationList = createAction(REQUEST_NAVIGATION_LIST);
    static updateNavigationList = createAction(UPDATE_NAVIGATION_LIST, (navigationData: any) => navigationData);
}
export class FileAction {
    static requestLibrary = createAction(FILE_LIBRARY_REQUEST, (code) => code);
    static libraryLoaded = createAction(FILE_LIBRARY_LOADED, (code, data) => { return { code, data } });
}

export class CommentAction {
    // static request = createAction(COMMENT_REQUEST, (request: GetCommentRequest) => request);
    static more = createAction(COMMENT_MORE, (request: CommentMoreRequest) => request);
    static add = createAction(COMMENT_ADD, (add: CommentAddRequest) => add);
    static delete = createAction(COMMENT_DELETE, (req: CommentDeleteRequest) => req);
    static loaded = createAction(COMMENT_LOADED, (req: CommentHolder) => req);
}

export class CommonYeeflowBaseInfoAction {
    static userBaseInfo = createAction(COMMON_USERBASEINFO, (info: CommonUserBaseInfo) => info);
}

export class ListSetAction {
    //请求ListSet的所有定义
    static requestAllDefOfListSet = createAction(REQUEST_ALL_DEF_OF_LISTSET, (AppID, customType, isUpdate: boolean) => ({ AppID, customType, isUpdate }));

    //请求ListSet定义
    static requestListSetDef = createAction(REQUEST_LISTSET_DEF, (AppID, customType, isUpdate: boolean) => ({ AppID, customType, isUpdate }));

    //更新ListSet定义
    static updateListSetDef = createAction(UPDATE_LISTSET_DEF, (customType, listDef: CustomListMgrAddRequest) => ({ customType, listDef }));

    //请求对应ListSet的list
    static requestListsInListSet = createAction(REQUEST_LISTSET_LISTS, (AppID, customType, isUpdate: boolean) => ({ AppID, customType, isUpdate }));

    //更新对应ListSet的list
    static updateListsInListSet = createAction(UPDATE_LISTSET_LISTS, (customType, lists) => ({ customType, lists }));

    //请求对应listset的page
    static requestListSetPage = createAction(REQUEST_LISTSET_PAGE, (AppID, customType, isUpdate: boolean) => ({ AppID, customType, isUpdate }));

    //更新对应listset的page
    static updateListSetPage = createAction(UPDATE_LISTSET_PAGE, (customType, pages) => ({ customType, pages }));

    //更新是否加载完成
    static updateListSetLoading = createAction(UPDATE_LISTSET_LOADING, (customType, loading) => ({ customType, loading }));

    //保存listset的Sort的定义
    static saveListSetDef = createAction(SAVE_LIST_SET_SORT_DEF, (AppID, customType, LayoutView, callbackDic: AkDictionaryString<() => void>) => ({ AppID, customType, LayoutView, callbackDic }))
}
