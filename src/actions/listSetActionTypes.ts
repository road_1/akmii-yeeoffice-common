//请求Listset定义
export const REQUEST_LISTSET_DEF = "REQUEST_LISTSET_DEF";
//更新ListSet定义
export const UPDATE_LISTSET_DEF = "UPDATE_LISTSET_DEF";
//请求属于对应ListSet的list
export const REQUEST_LISTSET_LISTS = "REQUEST_LISTSET_LISTS";
//更新对应ListSet的list
export const UPDATE_LISTSET_LISTS = "UPDATE_LISTSET_LISTS";
//更新是否加载完成
export const UPDATE_LISTSET_LOADING = "UPDATE_LISTSET_LOADING";
//更新对应listset的page
export const UPDATE_LISTSET_PAGE = "UPDATE_LISTSET_PAGE";
//请求对应listset的page
export const REQUEST_LISTSET_PAGE = "REQUEST_LISTSET_PAGE";
//请求ListSet的所有定义
export const REQUEST_ALL_DEF_OF_LISTSET = "REQUEST_ALL_DEF_OF_LISTSET";
//保存listset的Sort的定义
export const SAVE_LIST_SET_SORT_DEF = "SAVE_LIST_SET_SORT_DEF";

