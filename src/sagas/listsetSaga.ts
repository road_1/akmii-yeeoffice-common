import { fork, take, call, put, select } from "redux-saga/effects";
import { ListSetAction } from "../actions";
import { REQUEST_LISTSET_LISTS, REQUEST_LISTSET_PAGE, REQUEST_LISTSET_DEF, REQUEST_ALL_DEF_OF_LISTSET, SAVE_LIST_SET_SORT_DEF } from "../actions/listSetActionTypes";
import { ContentListApi } from "../api/content-list/content-list-api";
import { ContentListLayoutType, LIST_SET_PREFIX } from "..";
import { AkMessage, AkNotification } from "../components";
import { AkGlobal } from "../util";
import { CommonLocale } from "../locales";

//listset def cache
let listSetRequestWatcher = {};
//listset pages cache
let listSetPagesWatcher = {};
//listset lists cache
let listSetListsWatcher = {};

function* requestListSet() {
    while (true) {
        const { payload: { AppID, customType, isUpdate } } = yield take(REQUEST_LISTSET_DEF);
        yield put(ListSetAction.updateListSetLoading(customType, true));
        if (isUpdate) {
            delete listSetRequestWatcher[customType]
        }
        if (!(customType in listSetRequestWatcher)) {
            listSetRequestWatcher[customType] = true;
            yield fork(listsetRes, AppID, customType);
        }
        yield put(ListSetAction.updateListSetLoading(customType, false));
    }
}

function* listsetRes(AppID, customType, cb?: () => void) {
    const ListID = customType.split("_")[1];
    const listSet = yield call(ContentListApi.GetList, { AppID, ListID })
    if (listSet.Status === 0) {
        yield put(ListSetAction.updateListSetDef(customType, listSet.Data));
        cb && cb();
    } else {
        delete listSetRequestWatcher[customType];
        console.warn("server error: request listdef in  ListSet ");
    }
}

function* requestPagesInListSet() {
    while (true) {
        const { payload: { AppID, customType, isUpdate } } = yield take(REQUEST_LISTSET_PAGE);
        yield put(ListSetAction.updateListSetLoading(customType, true));
        if (isUpdate) {
            delete listSetPagesWatcher[customType]
        }
        if (!(customType in listSetPagesWatcher)) {
            listSetPagesWatcher[customType] = true;
            yield fork(pagesInListSetRes, AppID, customType);
        }
        yield put(ListSetAction.updateListSetLoading(customType, false));
    }
}

function* pagesInListSetRes(AppID, customType) {
    const ListID = customType.split("_")[1];
    const pages = yield call(ContentListApi.GetLayoutByListID, { AppID, ListID });
    if (pages.Status === 0) {
        yield put(ListSetAction.updateListSetPage(customType, pages.Data.filter(i => i.Type === ContentListLayoutType.CustomPage)))
    } else {
        delete listSetPagesWatcher[customType];
        console.warn("server error: request pages in  ListSet ");
    }
}

function* requestListsInListSet() {
    while (true) {
        const { payload: { AppID, customType, isUpdate } } = yield take(REQUEST_LISTSET_LISTS);
        yield put(ListSetAction.updateListSetLoading(customType, true));
        if (isUpdate) {
            delete listSetListsWatcher[customType];
        }
        if (!(customType in listSetListsWatcher)) {
            listSetListsWatcher[customType] = true;
            yield fork(listsInListSetRes, AppID, customType);
        }
        yield put(ListSetAction.updateListSetLoading(customType, false));
    }
}

function* listsInListSetRes(AppID, CustomType) {
    const lists = yield call(ContentListApi.GetLists, { AppID, CustomType });
    if (lists.Status === 0) {
        yield put(ListSetAction.updateListsInListSet(CustomType, lists.Data));
    } else {
        delete listSetListsWatcher[CustomType];
        console.warn("server error: request lists in  ListSet ");
    }
}


function* requestAllDefOfListSet() {
    while (true) {
        const { payload: { AppID, customType, isUpdate } } = yield take(REQUEST_ALL_DEF_OF_LISTSET);
        yield put(ListSetAction.updateListSetLoading(customType, true));
        if (isUpdate) {
            delete listSetRequestWatcher[customType];
            delete listSetPagesWatcher[customType];
            delete listSetListsWatcher[customType];
        }
        if (!(customType in listSetRequestWatcher)) {
            listSetRequestWatcher[customType] = true;
            yield fork(listsetRes, AppID, customType);
        }
        if (!(customType in listSetPagesWatcher)) {
            yield fork(pagesInListSetRes, AppID, customType);
            listSetPagesWatcher[customType] = true;
        }
        if (!(customType in listSetListsWatcher)) {
            listSetListsWatcher[customType] = true;
            yield fork(listsInListSetRes, AppID, customType);
        }
        yield put(ListSetAction.updateListSetLoading(customType, false));
    }
}

function* saveListSetLayoutView() {
    while (true) {
        const { payload: { AppID, customType, LayoutView, callbackDic } } = yield take(SAVE_LIST_SET_SORT_DEF);
        const { formatMessage } = AkGlobal.intl;
        const ListID = customType.split(LIST_SET_PREFIX)[1];
        const saveRes = yield call(ContentListApi.SaveLayoutView, { AppID, ListID, LayoutView })
        if (saveRes.Status === 0) {
            if (callbackDic["update"]) {
                callbackDic["update"]();
            } else {
                yield fork(listsetRes, AppID, customType, callbackDic["end"]);
            }
        } else {
            AkNotification.error({
                message: formatMessage({ id: CommonLocale.Tip }),
                description: formatMessage({ id: CommonLocale.TipSaveFail })
            })
        }
    }
}


export function* listsetSaga(): any {
    yield [
        fork(requestListSet),
        fork(requestPagesInListSet),
        fork(requestListsInListSet),
        fork(requestAllDefOfListSet),
        fork(saveListSetLayoutView)
    ]
}