import { fork, take, call, put, select, all, cancel } from "redux-saga/effects";
import { SELECT_APPCENTER, REQUEST_APPCENTER_LIST, REQUEST_NAVIGATION_LIST } from "../actions/ActionTypes";
import { AppCenterApi } from "../api/appcenter/appcenterapi";
import { AppCenterAction } from "../actions";
import { AppCenterListResponse, AppCenterPermissionEnum, AkContext, NavigationHelper, AkUtil, AppKeys } from "..";
import { MerchatAPI } from "../api/masterpage/masterpage";
import { PortalViewModel } from "../components/masterpage/navigator";



export function* watchRequestAppCenter(): any {
    yield ([
        fork(initAppCenterList),
        fork(requestAppCenterList),
        fork(selectAppCenter),
        fork(requestNamgationList),
    ]);
}

function createPortalTree(navigatorData, portalData) {
    let navigator = navigatorData;
    let portal;
    if (portalData && portalData.length > 0) {
        try {
            const portalApp = AkContext.getAppinfoByAppKey(AppKeys.YeeOfficePortal);
            const portalNav = navigator.find(i => i.AppID === portalApp.AppInfoID);
            portalData = portalData.map(item => {
                item.appID = portalNav.AppID;
                item.portalbaseUrl = NavigationHelper.getNavigationUrl(portalApp.AppKey, portalNav.YunGalaxyNavigationUrl);
            })
            portal = PortalViewModel.processSimpleTreeData(portalData);
            AkUtil.remove(navigator, i => i === portalNav);
        } catch (ex) {
            console.log("no portal navigation data.")
        }
    }

    return [navigator, portal];
}


function* initAppCenterList() {
    try {
        const ResArrary = yield [
            call(MerchatAPI.getMechatNavigation, {}),
            call(MerchatAPI.getMechatPortal),
            call(AppCenterApi.getAppCenterList),
            call(AppCenterApi.getNavigationListSort)
        ];
        //portal接口可能没有，不阻塞其他的put
        if (ResArrary[0].Status === 0 && ResArrary[2].Status === 0) {
            const [navigator, portal] = createPortalTree(ResArrary[0].Data, ResArrary[1].Data || []);
            yield put(AppCenterAction.updateSystemAppList(navigator));
            yield put(AppCenterAction.updatePortalTreeData(portal));
            yield put(AppCenterAction.updateAppCenterList(ResArrary[2].Data));
            yield put(AppCenterAction.updateNavigationList(ResArrary[3].Data));
        }
    } catch (err) {
        console.log("appcenter list request error", err);
    }
}

function* requestAppCenterList() {
    while (true) {
        yield take(REQUEST_APPCENTER_LIST);
        try {
            const rs = yield call(AppCenterApi.getAppCenterList);
            if (rs.Status === 0) {
                yield put(AppCenterAction.updateAppCenterList(rs.Data));
            }
        } catch (err) {
            console.log("appcenter list request error", err);
        }
    }
}

function* selectAppCenter() {
    while (true) {
        const { payload: { url, appCenterID } } = yield take(SELECT_APPCENTER);
        const { appCenter: { appCenterList, selectApp } } = yield select();
        let selected: AppCenterListResponse;
        if (appCenterID) {
            selected = (appCenterList as AppCenterListResponse[]).find(i => i.AppCenterID === appCenterID);
        } else {
            selected = (appCenterList as AppCenterListResponse[]).find(i => i.RelativeUrl === url);
        }

        //selectapp不存或者selectapp的appcenterid不同时更新selectapp
        // if (selectApp) {
        //     if ((selectApp as AppCenterListResponse).AppCenterID !== selected.AppCenterID) {
        //         yield put(AppCenterAction.updateSelectApp(selected))
        //     }
        // } else {
        if (selected) {
            yield put(AppCenterAction.updateSelectApp(selected))
            yield put(AppCenterAction.updatePer(selected.Perm))
        } else {
            window.location.hash = "#/nopermission";
            // yield put(AppCenterAction.updatePer(AppCenterPermissionEnum.NotPermission))
        }
        // }
    }
}

/**更新导航数据 */
function* requestNamgationList() {
    while (true) {
        yield take(REQUEST_NAVIGATION_LIST);
        try {
            const res = yield call(AppCenterApi.getNavigationListSort);
            if (res.Status === 0) {
                yield put(AppCenterAction.updateNavigationList(res.Data));
            }
        } catch (err) {
            console.log("Navigation list request error", err);
        }
    }
}

