import { take, fork, actionChannel, put, select } from 'redux-saga/effects';
import { REQUEST_CHAT_PULL, REQUEST_SEND_FILE, REQUEST_CHAT_MESSAGE, REQUEST_CHAT_DETAIL, REQUEST_QUIT_GROUP, REQUEST_ADD_GROUP_USER, REQUEST_REMOVE_GROUP_USER, REQUEST_START_CHAT, REQUEST_SEND_MESSAGE, REQUEST_CHAT_MESSAGE_LIST, CREATE_CHAT, REQUEST_CHAT_LIST, RECEIVE_CHAT_DETAIL, REQUEST_CHANGE_IS_TOP, REQUEST_GROUP_NAME, REQUEST_ADD_GROUP_PERSON, REQUEST_DELETE_GROUP_PERSON, RECEIVE_UPDATE_GROUP_INFO, REQUEST_HISTORY_MESSAGE, RECEIVE_REMOVE_GROUP_PERSON, RECEIVE_ADD_GROUP_PERSON, RECEIVE_IS_TOP, RECEIVE_UPDATE_MESSAGE } from '../actions/ActionTypes';
import { SocketClient } from "../api/masterpage/socketclient";
import { ChatMessagePullRequest, ChatMessagePullModel, ChatTypeEnum, ChatPullModel, ChatMessageModel, MessagePullDirectionEnum } from "../api/masterpage/socketmodel";
import { AkUtil } from '../util/util';
import { buffers } from 'redux-saga';
import { NotificationAction } from '../actions/index';
import { AkGlobal, AkContext } from '../util/common';
import * as moment from 'moment';
import { Request } from '../util/request';
import { AkNotification } from '../components/controls/ak-notification';
import { CommonLocale } from '../locales/localeid';

export function* watchRequestNotification(): any {
    yield ([
        fork(requestChatPull),
        fork(receiveChatPull),
        fork(receiveChatMessageList),
        fork(startChatWithUser),
        fork(receiveChatMessage),
        fork(receiveChat),
        fork(sendMessage),
        fork(sendFile),
        fork(receiveChatDetail),
        fork(requestQuitGroup),
        fork(requestAddGroupUser),
        fork(receiveAddGroupUser),
        fork(requestRemoveGroupUser),
        fork(receiveRemoveGroupUser),
        fork(requestChatDetail),
        fork(requestChangeIsTop),
        fork(requestGroupName),
        fork(receiveUpdataGroupInfo),
        fork(requestHistoryMessage),
        fork(receiveIsTop),
        fork(receiveReadMessage),
    ]);
}
/** 成功连接，拉取会话列表 */
function* requestChatPull() {
    while (true) {
        yield take(REQUEST_CHAT_PULL);
        //请求拉取
        try {
            SocketClient.getInstance().pullChatList({});
        } catch (err) {
            console.log("请求拉取", err);
        }
    }
}
/** 拉取到会话列表，遍历拉取单个会话的消息 */
function* receiveChatPull() {
    while (true) {
        let response = yield take(REQUEST_CHAT_LIST);
        try {
            let data = response.payload as ChatPullModel[];
            let chatList = AkGlobal.store.getState().notification.accountChatList as ChatPullModel[];
            // let count = 0;
            data.map(chat => {
                // if (count < 20) {
                const params: ChatMessagePullRequest = {
                    LastMsgID: chat.AccountChat.LastReadID,
                    ChatID: chat.AccountChat.ChatID,
                    Direction: MessagePullDirectionEnum.Down
                }
                SocketClient.getInstance().pullMessage(params, AkUtil.guid());
                // count++;
                // }
                chat.LastMessageTime = chat.AccountChat.Modified;
                chat.ChatID = chat.AccountChat.ChatID;
            });

            if (data.length === 1 && !chatList.find(i => i.AccountChat.ChatID === data[0].ChatID)) {
                yield put(NotificationAction.receiveChatList([...new Set([...chatList, ...data])]));
            } else {
                yield put(NotificationAction.receiveChatList(data));
            }
        } catch (err) {
            console.log("拉取到会话列表，遍历拉取单个会话的消息", err);
        }
    }
}

/**拉取历史消息 */
function* requestHistoryMessage() {
    while (true) {
        yield take(REQUEST_HISTORY_MESSAGE);
        try {
            let chatItem = AkGlobal.store.getState().notification.chatItem as ChatPullModel;
            // let chatList = AkGlobal.store.getState().notification.accountChatList as ChatPullModel[];
            let message = chatItem.MessagesPull.Messages;
            const params: ChatMessagePullRequest = {
                LastMsgID: (message.length > 0 && message[0].MessageID) || "0",
                ChatID: chatItem.AccountChat.ChatID,
                Direction: MessagePullDirectionEnum.Up,
                Self: !(message.length > 0 && message[0].MessageID)
            }

            SocketClient.getInstance().pullMessage(params, AkUtil.guid());
        } catch (err) {
            console.log("拉取历史消息", err);
        }
        // let changeItem = chatList.find(i => i.AccountChat.ChatID === chatItem.AccountChat.ChatID);
        // changeItem.MessagesPull.Direction = MessagePullDirectionEnum.Up;
        // chatItem.MessagesPull.Direction = MessagePullDirectionEnum.Up;
        // yield put(NotificationAction.saveChatItem({ ...chatItem }));
        // yield put(NotificationAction.receiveChatList([...chatList]));
    }
}

/**获取多个会话消息 */
function* receiveChatMessageList() {
    let channelRequest = yield actionChannel(REQUEST_CHAT_MESSAGE_LIST, buffers.expanding(1000));
    while (true) {
        let response = yield take(channelRequest);
        try {
            let data: ChatMessagePullModel = response.payload;

            const chatList = AkGlobal.store.getState().notification.accountChatList as ChatPullModel[];
            const templateChat = AkGlobal.store.getState().notification.chatItem as ChatPullModel;
            const chatItem = chatList.find(i => i.AccountChat.ChatID === data.ChatID)

            chatItem.MessagesPull = data;

            if (data.Direction === MessagePullDirectionEnum.Down) {
                chatItem.UnReadCount = (chatItem.UnReadCount || 0) + data.Messages.length;
            }
            if (templateChat && templateChat.ChatID === data.ChatID) {
                if (data.Direction === MessagePullDirectionEnum.Up) {
                    data.Messages = [...new Set([...data.Messages.reverse(), ...templateChat.MessagesPull.Messages])];
                }
                templateChat.MessagesPull = data;
                yield put(NotificationAction.saveChatItem({ ...templateChat }));
            }
            yield put(NotificationAction.receiveChatList([...chatList]));
        } catch (err) {
            console.log("获取多个会话消息", err);
        }
    }
}

/** 接收到消息,包括发送消息成功后的回执 */
function* receiveChatMessage() {
    while (true) {
        let messageresponse = yield take(REQUEST_CHAT_MESSAGE);
        try {
            const messagedata = messageresponse.payload as ChatMessageModel;
            const chatList = AkGlobal.store.getState().notification.accountChatList;
            if (!messagedata) {
                continue;
            }

            const chatItem = chatList.find(i => i.ChatID === messagedata.ChatID) as ChatPullModel;

            if (chatItem && !chatItem.MessagesPull) {
                chatItem.MessagesPull = {
                    Messages: []
                };
            }

            //更新消息已读
            let showChatItem = AkGlobal.store.getState().notification.showChatItem;
            if (showChatItem) {
                SocketClient.getInstance().readMessage({
                    ChatID: chatItem.AccountChat.ChatID,
                    LastMessageID: messagedata.MessageID
                });
            }

            chatItem.MessagesPull.Messages.push(messagedata);
            chatItem.LastMessageTime = moment().format("YYYY-MM-DD HH:mm:ss");
            //服务通知以及聊天未读数
            if (messagedata.Author !== AkContext.getUser().AccountID || messagedata.Type === 10000) {
                chatItem.UnReadCount = (chatItem.UnReadCount || 0) + 1;
            }
            const tempItem = AkGlobal.store.getState().notification.chatItem as ChatPullModel;
            if (tempItem && tempItem.ChatID === chatItem.ChatID) {
                yield put(NotificationAction.saveChatItem({ ...chatItem }));
            }
            yield put(NotificationAction.receiveChatList([...chatList]));
        } catch (err) {
            console.log("接收到消息,包括发送消息成功后的回执", err);
        }

    }
}
/** 选定用户开始聊天（单人/多人） */
function* startChatWithUser() {
    while (true) {
        let requestAction = yield take(CREATE_CHAT);
        try {
            let ids = requestAction.payload;
            if (ids.length === 0) {
                continue;
            }
            let userList = AkGlobal.store.getState().identity.identityDict;
            let ext1 = '';
            if (ids.length >= 2) {
                let temp = [];
                ids.map(item => {
                    if (userList[item]) {
                        temp.push(userList[item].data.Name);
                    } else {
                        temp.push(AkContext.getUser().Name_CN)
                    }
                })
                ext1 = [...new Set([...temp])].join('、');
            }
            const createChat = {
                ChatType: ids.length < 3 ? ChatTypeEnum.OneToOneChat : ChatTypeEnum.GroupChat,
                MemberIDs: ids,
                Ext1: ext1
            }
            SocketClient.getInstance().createChat(createChat);
            //显示聊天详细界面
            let chatItemAction = yield take(REQUEST_START_CHAT);
            let chatItem = chatItemAction.payload as ChatPullModel;
            let accountChatList = AkGlobal.store.getState().notification.accountChatList as ChatPullModel[];
            chatItem.ChatID = chatItem.AccountChat.ChatID;
            const addChat = accountChatList.find(i => i.AccountChat.ChatID === chatItem.AccountChat.ChatID)
            if (!chatItem.MessagesPull) {
                const params: ChatMessagePullRequest = {
                    LastMsgID: chatItem.AccountChat.LastReadID,
                    ChatID: chatItem.AccountChat.ChatID,
                    Direction: MessagePullDirectionEnum.Down
                }
                SocketClient.getInstance().pullMessage(params, AkUtil.guid());
                chatItem.LastMessageTime = chatItem.AccountChat.Modified;
                chatItem.ChatID = chatItem.AccountChat.ChatID;
            }
            if (!addChat) {
                accountChatList.push(chatItem);
            }
            yield put(NotificationAction.receiveChatList([...accountChatList]));
            yield put(NotificationAction.saveChatItem(chatItem));
            yield put(NotificationAction.showChatItem(true));
        } catch (err) {
            console.log("选定用户开始聊天（单人/多人）", err);
        }

        //加载首页聊天历史消息
    }
}

/**接收发起的聊天会话 */
function* receiveChat() {
    while (true) {
        let request = yield take(REQUEST_START_CHAT);
        try {
            let chatItem = request.payload as ChatPullModel;
            let accountChatList = AkGlobal.store.getState().notification.accountChatList as ChatPullModel[];
            const chat = accountChatList.find(i => i.ChatID === chatItem.AccountChat.ChatID)
            if (chat) {

            } else {
                chatItem.ChatID = chatItem.AccountChat.ChatID;
                chatItem.LastMessageTime = chatItem.AccountChat.Modified;
                chatItem.MessagesPull = {
                    Messages: []
                };
                accountChatList.push(chatItem);
            }
            yield put(NotificationAction.receiveChatList([...accountChatList]));
        } catch (err) {
            console.log("接收发起的聊天会话", err);
        }

    }
}


/** 发送消息 */
function* sendMessage() {
    while (true) {
        let requestAction = yield take(REQUEST_SEND_MESSAGE);
        try {
            let request = requestAction.payload;
            SocketClient.getInstance().sendMessage({
                ChatID: request.ChatID,
                Type: request.Type,
                Body: request.Body
            });
        } catch (err) {
            console.log("发送消息", err);
        }

    }
}
/** 发送文件 */
function* sendFile() {
    while (true) {
        yield take(REQUEST_SEND_FILE)
    }
}

/** 请求到聊天详情 */
function* requestChatDetail() {
    while (true) {
        yield take(REQUEST_CHAT_DETAIL)
        try {
            let chatItem = AkGlobal.store.getState().notification.chatItem as ChatPullModel;
            if (chatItem.Chat.Type === ChatTypeEnum.GroupChat) {
                SocketClient.getInstance().getMember({ ChatID: chatItem.Chat.ChatID })
            } else {
                const targetID = chatItem.AccountChat.ChatID.replace(AkContext.getUser().AccountID, '').replace('|', '');
                yield put(NotificationAction.showChatPerson([targetID]));
            }
        } catch (err) {
            console.log("请求到聊天详情", err);
        }

        //1.显示聊天详情栏
        //2.发送聊天详情请求
    }
}
/** 接收到聊天详情 */
function* receiveChatDetail() {
    while (true) {
        let response = yield take(RECEIVE_CHAT_DETAIL);
        try {
            let chatPerson = response.payload;
            if (chatPerson.length <= 0) {
                let chatItem = AkGlobal.store.getState().notification.chatItem;
                const targetID = chatItem.AccountChat.ChatID.replace(AkContext.getUser().AccountID, '').replace('|', '');
                yield put(NotificationAction.showChatPerson([targetID]));
            } else {
                let targetIDs = chatPerson.map(i => i = i.AccountID)
                yield put(NotificationAction.showChatPerson(targetIDs));
            }
        } catch (err) {
            console.log("接收到聊天详情", err);
        }


        //展示聊天详情
    }
}

/** 退出群组聊天 */
function* requestQuitGroup() {
    while (true) {
        yield take(REQUEST_QUIT_GROUP)
        try {
            let chatItem = AkGlobal.store.getState().notification.chatItem as ChatPullModel;
            //1.发送退出群组聊天消息
            SocketClient.getInstance().quitGroup({
                ChatID: chatItem.ChatID
            });
            //2.移除ChatList里的ChatItem
            let chatList = AkGlobal.store.getState().notification.accountChatList as ChatPullModel[];
            // console.log(chatList.length);
            AkUtil.remove(chatList, i => i.ChatID === chatItem.ChatID);
            // console.log(chatList.length);
            //3.返回到聊天会话列表
            yield put(NotificationAction.showChatInfo(false));
            yield put(NotificationAction.showChatItem(false));
        } catch (err) {
            console.log("退出群聊", err);
        }
    }
}
/** 群组添加用户 */
function* requestAddGroupUser() {
    while (true) {
        let request = yield take(REQUEST_ADD_GROUP_PERSON);
        try {
            let addids = request.payload;
            let chatItem = AkGlobal.store.getState().notification.chatItem as ChatPullModel;
            if (chatItem.Chat.Type === ChatTypeEnum.OneToOneChat) {
                let userList = AkGlobal.store.getState().identity.identityDict;
                let menberids = [...new Set([...chatItem.AccountChat.ChatID.split("|"), ...addids.map(i => i = i.ID)])];
                const createChat = {
                    ChatType: ChatTypeEnum.GroupChat,
                    MemberIDs: menberids,
                    Ext1: menberids.map(i => i = userList[i].data.Name).join("、")
                }
                SocketClient.getInstance().createChat(createChat);
                let chatItemAction = yield take(REQUEST_START_CHAT);
                chatItem = chatItemAction.payload as ChatPullModel;
                let accountChatList = AkGlobal.store.getState().notification.accountChatList;
                chatItem.ChatID = chatItem.AccountChat.ChatID;

                accountChatList.push(chatItem);
                yield put(NotificationAction.receiveChatList([...accountChatList]));
                yield put(NotificationAction.saveChatItem(chatItem));
                yield put(NotificationAction.showChatPerson(menberids));
            } else {
                let name = [...new Set([...chatItem.Chat.Ext1.split("、"), ...addids.map(i => i = i.Name)])];
                SocketClient.getInstance().addMember({
                    ChatID: chatItem.ChatID,
                    MemberIDs: addids.map(i => i = i.ID),
                    Ext1: name.join("、")
                })
                let chatPerson = AkGlobal.store.getState().notification.chatPerson as string[];
                yield put(NotificationAction.showChatPerson([...new Set([...chatPerson, ...addids.map(i => i = i.ID)])]));
            }
        } catch (err) {
            console.log("群组添加用户操作", err);
        }
    }
}
/** 群组移除用户 */
function* requestRemoveGroupUser() {
    while (true) {
        let request = yield take(REQUEST_DELETE_GROUP_PERSON);
        try {
            let deleteids = request.payload;
            let chatItem = AkGlobal.store.getState().notification.chatItem as ChatPullModel;
            let userList = AkGlobal.store.getState().identity.identityDict;
            let name = chatItem.Chat.Ext1.split("、");
            SocketClient.getInstance().removeMember({
                ChatID: chatItem.ChatID,
                MemberIDs: deleteids,
                Ext1: name.filter(i => i !== userList[deleteids[0]].data.Name).join("、")
            });
            let chatPerson = AkGlobal.store.getState().notification.chatPerson as string[];
            yield put(NotificationAction.showChatPerson(chatPerson.filter(i => i !== deleteids[0])));
        } catch (err) {
            console.log("移除用户操作", err);
        }
    }
}

/**接收移除用户的消息 */
function* receiveRemoveGroupUser() {
    while (true) {
        let request = yield take(RECEIVE_REMOVE_GROUP_PERSON);
        try {
            let remove = request.payload;
            let chatList = AkGlobal.store.getState().notification.accountChatList as ChatPullModel[];
            let showChatItem = AkGlobal.store.getState().notification.showChatItem;
            let showChatInfo = AkGlobal.store.getState().notification.showChatInfo;
            let chatItem = AkGlobal.store.getState().notification.chatItem as ChatPullModel;
            let chatPerson = AkGlobal.store.getState().notification.chatPerson as string[];
            if (remove.DelMemberIDs[0] === AkContext.getUser().AccountID) {
                chatList = chatList.filter(i => i.AccountChat.ChatID !== remove.ChatID);
                if ((showChatItem || showChatInfo) && chatItem.AccountChat.ChatID === remove.ChatID) {
                    yield put(NotificationAction.showChatItem(false));
                    yield put(NotificationAction.showChatInfo(false));

                }
            }
            yield put(NotificationAction.showChatPerson(chatPerson.filter(i => i !== remove.DelMemberIDs[0])));
            yield put(NotificationAction.receiveChatList([...chatList]));
        } catch (err) {
            console.log("接收移除用户的消息", err);
        }

    }
}
/**接收添加人员的消息 */
function* receiveAddGroupUser() {
    while (true) {
        let request = yield take(RECEIVE_ADD_GROUP_PERSON);
        try {
            let add = request.payload;
            let chatList = AkGlobal.store.getState().notification.accountChatList as ChatPullModel[];
            let isMe = add.DelMemberIDs.find(i => i === AkContext.getUser().AccountID);
            let isAdd = chatList.find(i => i.AccountChat.ChatID === add.ChatID);
            let chatPerson = AkGlobal.store.getState().notification.chatPerson as string[];
            if (!isAdd && isMe) {
                SocketClient.getInstance().pullChatList({
                    ChatID: add.ChatID
                });
            }
            yield put(NotificationAction.showChatPerson([...new Set([...chatPerson, ...add.DelMemberIDs])]))
        } catch (err) {
            console.log("接收添加人员的消息", err);
        }

    }
}

/**置顶聊天 */
function* requestChangeIsTop() {
    while (true) {
        let request = yield take(REQUEST_CHANGE_IS_TOP);
        try {
            let istop = request.payload;
            let chatItem = AkGlobal.store.getState().notification.chatItem as ChatPullModel;
            SocketClient.getInstance().setChatFavorite({
                ChatID: chatItem.ChatID,
                Favorite: istop
            })
            chatItem.AccountChat.IsFavorite = istop;
            let chatList = AkGlobal.store.getState().notification.accountChatList as ChatPullModel[];
            let changeItem = chatList.find(i => i.ChatID === chatItem.AccountChat.ChatID);
            changeItem.AccountChat.IsFavorite = istop;
            yield put(NotificationAction.saveChatItem({ ...chatItem }));
            yield put(NotificationAction.receiveChatList([...chatList]));
        } catch (err) {
            console.log("置顶操作", err);
        }
    }
}

/**修改群聊名称 */
function* requestGroupName() {
    while (true) {
        let request = yield take(REQUEST_GROUP_NAME);
        try {
            let groupname = request.payload;
            let chatItem = AkGlobal.store.getState().notification.chatItem as ChatPullModel;
            SocketClient.getInstance().editGroupName({
                ChatID: chatItem.ChatID,
                Name: groupname
            })
            chatItem.Chat.Name = groupname;
            yield put(NotificationAction.saveChatItem({ ...chatItem }));
        } catch (err) {
            console.log("修改群名称", err);
        }
    }
}

/**更新群聊信息 */
function* receiveUpdataGroupInfo() {
    while (true) {
        let request = yield take(RECEIVE_UPDATE_GROUP_INFO);
        try {
            let chat = request.payload;
            let chatList = AkGlobal.store.getState().notification.accountChatList as ChatPullModel[];
            let changeItem = chatList.find(i => i.ChatID === chat.ChatID);
            let chatItem = AkGlobal.store.getState().notification.chatItem as ChatPullModel;
            if (chatItem && chatItem.AccountChat.ChatID === chat.ChatID) {
                chatItem.Chat = chat;
                yield put(NotificationAction.saveChatItem({ ...chatItem }))
            }
            if (changeItem) {
                changeItem.Chat = chat;
                yield put(NotificationAction.receiveChatList([...chatList]));
            }
        } catch (err) {
            console.log("更新群信息", err);
        }
    }
}

/**更新置顶 */
function* receiveIsTop() {
    while (true) {
        let request = yield take(RECEIVE_IS_TOP);
        try {
            let istop = request.payload;
            let chatList = AkGlobal.store.getState().notification.accountChatList as ChatPullModel[];
            let changeItem = chatList.find(i => i.AccountChat.ChatID === istop.ChatID);
            changeItem.AccountChat.IsFavorite = istop.Favorite;
            yield put(NotificationAction.receiveChatList([...chatList]));
        } catch (err) {
            console.log("更新置顶", err);
        }
    }
}

/**更新消息已读 */
function* receiveReadMessage() {
    while (true) {
        let request = yield take(RECEIVE_UPDATE_MESSAGE);
        try {
            let readMessage = request.payload;
            let chatList = AkGlobal.store.getState().notification.accountChatList as ChatPullModel[];
            let templateItem: ChatPullModel = yield select((state: any) => state.notification.chatItem);
            let chatItem = chatList.find(i => i.AccountChat.ChatID === readMessage.ChatID);
            if (chatItem.ChatID === templateItem.ChatID) {
                chatItem.UnReadCount = 0;
            }
            chatItem.AccountChat.LastReadID = readMessage.LastMessageID;
            yield put(NotificationAction.receiveChatList([...chatList]))
        } catch (err) {
            console.log("该消息已读", err);
        }
    }
}


