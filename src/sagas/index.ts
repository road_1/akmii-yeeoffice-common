import { fork } from "redux-saga/effects";
import { mainSaga } from './mainSaga';
import { watchRequestNotification } from './notification';
import { watchRequestAppCenter } from "./appcenter";
import { commentSaga } from "./commentSaga";
import { listsetSaga } from "./listsetSaga";

export default function* rootSaga(): any {
    yield [
        fork(mainSaga),
        fork(watchRequestNotification),
        fork(watchRequestAppCenter),
        fork(commentSaga),
        fork(listsetSaga)
    ]
}
