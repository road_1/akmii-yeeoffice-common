import { call, fork, put, select, take } from "redux-saga/effects";
import { AkMessage, CommentAPI } from "..";
import { COMMENT_ADD, COMMENT_DELETE, COMMENT_MORE } from '../actions/ActionTypes';
import { CommentAction } from '../actions/index';
import { CommentAddRequest, CommentDeleteRequest, CommentMoreRequest, GetCommentRequest } from '../api/comment/commentmodel';
import { AkCommentsLocale } from "../locales/localeid";
import { CommentHolder, CommentReducerUtil } from '../reducers/commentReducer';
import { AkGlobal } from '../util/common';

const DEFAULT_PAGE_SIZE = 10;

function* watchMore() {
    while (true) {
        const { payload } = yield take(COMMENT_MORE);

        const { commentReducer: { commentDict } } = yield select();
        let rs = CommentReducerUtil.getFromCache(payload, commentDict);
        let request = { ...payload, pageSize: payload.pageSize || DEFAULT_PAGE_SIZE, lastID: "0" }
        if (rs) {
            if (rs.status === "loading") {
                //正在加载
                continue;
            }

            if (rs.data.length > 0) {
                const last = rs.data[rs.data.length - 1];
                request.lastID = last.ID;
            }
        } else {
            rs = getDefaultHolder(payload);
        }
        yield put(CommentAction.loaded({ ...rs, status: "loading" }));
        yield fork(requestComment, request);
    }
}

// function* watchRequest() {
//     while (true) {
//         const { payload }: { payload: GetCommentRequest } = yield take(COMMENT_REQUEST);

//         const { commentReducer: { commentDict } } = yield select();
//         if (payload.appKey && payload.dataType && payload.dataID) {
//             let rs = CommentReducerUtil.getFromCache(payload, commentDict);
//             if (rs && rs.status !== "error") {
//                 //如果存在并且状态正常，不加载
//                 continue;
//             }

//             if (!rs) {
//                 rs = getDefaultHolder(payload);
//             }

//             yield put(CommentAction.loaded({ ...rs, status: "loading" }));
//             yield fork(requestComment, payload);
//         }
//     }
// }

function* watchDelete() {
    while (true) {
        const { payload }: { payload: CommentDeleteRequest } = yield take(COMMENT_DELETE);

        const { commentReducer: { commentDict } } = yield select();
        let key = CommentReducerUtil.getKey(payload.appKey, payload.DataType, payload.DataID);
        let existing = CommentReducerUtil.getFromCacheByKey(key, commentDict);
        if (existing) {
            try {
                let request = {
                    ID: payload.ID,
                    DataType: payload.DataType,
                    Status: false,
                    appKey: payload.appKey
                }

                const rs = yield call(CommentAPI.putCommentByIDState, request);
                if (rs.Status === 0) {
                    let holder = { ...existing, data: existing.data.filter(v => v.ID !== payload.ID) };
                    yield put(CommentAction.loaded(holder));
                    AkMessage.success(AkGlobal.intl.formatMessage({ id: AkCommentsLocale.DeleteSuccess }));
                }
            } catch (ex) {
                console.log("Delete comment error", ex);
            }
        }
    }
}

function* watchAdd() {
    while (true) {
        const { payload } = yield take(COMMENT_ADD);
        const { onComplete, ...request }: CommentAddRequest = payload;
        let success = false;
        try {
            const rs = yield call(CommentAPI.postComment, request);
            if (rs.Status === 0) {
                success = true;
            }
        } catch (ex) {
            console.log("Add comment error", ex);
        }
        onComplete && onComplete(success);
    }
}

function getDefaultHolder(payload): CommentHolder {
    return {
        status: "loading",
        errorCount: 0,
        appKey: payload.appKey,
        dataID: payload.dataID,
        dataType: payload.dataType,
        pageSize: payload.pageSize || DEFAULT_PAGE_SIZE,
        data: []
    }
}

function* processError(request: GetCommentRequest, existing: CommentHolder) {
    existing.errorCount++;
    if (existing.errorCount <= 3) {
        yield put(CommentAction.loaded(existing));
        yield fork(requestComment, request);
    } else {
        yield put(CommentAction.loaded({ ...existing, status: "error" }));
    }
}

function* requestComment(request: CommentMoreRequest) {
    const { onComplete, ...model } = request;
    try {
        let rs = yield call(CommentAPI.getComment, model);
        const { commentReducer: { commentDict } } = yield select();
        let existing = CommentReducerUtil.getFromCache(request, commentDict);
        if (rs.Status === 0) {
            // let holder: CommentHolder = {
            //     status: "done",
            //     errorCount: 0,
            //     appKey: model.appKey,
            //     dataID: model.dataID,
            //     dataType: model.dataType,
            //     pageSize: model.pageSize,
            //     data: rs.Data
            // }
            yield put(CommentAction.loaded({ ...existing, status: "done", errorCount: 0, data: [...existing.data, ...rs.Data] }));
            onComplete && onComplete(true);
        } else {
            processError(request, existing);
            onComplete && onComplete(false);
        }
    } catch (ex) {
        const { commentReducer: { commentDict } } = yield select();
        let existing = CommentReducerUtil.getFromCache(request, commentDict);
        processError(request, existing);
        onComplete && onComplete(false);
        console.log("get comment error", ex, existing);
    }
}

export function* commentSaga(): any {
    yield [
        // fork(watchRequest),
        fork(watchMore),
        fork(watchDelete),
        fork(watchAdd)
    ]
}