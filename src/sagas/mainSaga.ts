import { buffers, delay } from "redux-saga";
import { actionChannel, call, fork, put, select, take } from "redux-saga/effects";
import * as request from "superagent";
import { AppAdmin, CommonYeeflowBaseInfoAction, ContentListAction, IdentityAction, MetadataAction } from "../actions";
import { CHECK_FORM_CACHE, COMMON_NOTIFICATION, CONTENT_FIELD_REQUEST, CONTENT_LIST_REQUEST, FILE_LIBRARY_REQUEST, FORM_DELAY_UPDATE, METADATA_CATEGORY_REQUEST, METADATA_REQUEST, METADATA_REQUEST_BYID, REQUEST_APPINFO, REQUEST_GROUPS, REQUEST_LOCATIONS, REQUEST_ORGANIZATIONS, REQUEST_POSITIONS, REQUEST_USERS } from "../actions/ActionTypes";
import { CommonAction, FileAction } from '../actions/index';
import { CommonBaseInfoApi } from "../api/common/commonuserinfo";
import { GetMetadataModelRequest, GetMetadataRequest, MetadataAPI } from "../api/flowcraft/metadata";
import { AkGroup, AkIdentity, AkLocation, AkOrganization, AkPosition, AkUser, IdentityAPI } from '../api/identity/identity';
import { IdentityUtil } from "../reducers/Identity";
import { MetadataDictUtil } from "../reducers/Metadata";
import { AkDatetimeUtil } from "../util";
import { AkGlobal } from "../util/common";
import { ContentListApi } from "./../api/content-list/content-list-api";
import moment = require("moment");
import { FileUpLoadAPI } from "../api/common/fileupload";
import { notification } from "antd";

// function* syncTaskCount() {
//     while (true) {
//         const response = yield call(TaskAPI.getTaskCount, {});
//         if (response.Status === 0) {
//             yield put(TaskAction.taskCountLoaded(response.Data.PendingCount,
//                 response.Data.CandidateCount))
//         }
//         yield call(delay, 1000 * 60);
//     }
// }

// /**
//  * 当接收到syncTaskCount的请求，中止当前的sync任务并重启
//  */
// function* watchRequestTaskCount() {
//     while (true) {
//         const syncTask = yield fork(syncTaskCount); //执行异步方法，不block当前代码
//         yield call(delay, 1000); //1s之内不重复刷新
//         yield take(REQUEST_TASKCOUNT); //接受request更新请求
//         yield cancel(syncTask); //取消当前的sync job
//     }
// }

let notificationCache = {};

function* watchNotification() {
    const requestChan = yield actionChannel(COMMON_NOTIFICATION, buffers.expanding());
    while (true) {
        const { payload } = yield take(requestChan);
        let payloadStr = JSON.stringify(payload);
        let cache = notificationCache[payloadStr];
        let timetick = Date.now();
        //console.log(payloadStr, timetick);
        if (cache && (cache + 2000) >= timetick) {
            //cache 存在，并且在3s之内不重新提醒
            continue;
        } else {
            notificationCache[payloadStr] = timetick;
            switch (payload.type) {
                case CommonAction.NOTIFY_TYPE_ERROR:
                    notification.error(payload.args);
                    break;
                case CommonAction.NOTIFY_TYPE_INFO:
                    notification.info(payload.args);
                    break;
                case CommonAction.NOTIFY_TYPE_WARN:
                    notification.warn(payload.args);
                    break;
                case CommonAction.NOTIFY_TYPE_WARNING:
                    notification.warning(payload.args);
                    break;
            }
        }
    }
}


function* watchRequestUsers() {
    const requestChan = yield actionChannel(REQUEST_USERS, buffers.expanding());
    while (true) {
        const { payload } = yield take(requestChan);
        const { identity: { identityDict } } = yield select();

        const reqIDs: AkIdentity[] = IdentityUtil.diffForRequest(identityDict, payload);
        if (reqIDs && reqIDs.length) {
            yield put(IdentityAction.requestUsersBegin(reqIDs));
            yield fork(fetchIdentities, { identities: reqIDs });
        }
    }
}

function* fetchIdentities(request) {
    const rs = yield call(IdentityAPI.resolveIdentities, request);
    if (rs.Status === 0) {
        yield put(IdentityAction.identityUserLoaded(rs.Data.map(u => new AkUser(u))));
    }
}
function* watchRequestGroups() {
    while (true) {
        yield take(REQUEST_GROUPS);
        const { identity: { groupLoaded } } = yield select();
        if (!groupLoaded) {
            const rs = yield call(IdentityAPI.getGroups);
            if (rs.Status === 0) {
                yield put(IdentityAction.identityGroupLoaded(rs.Data.map(d => new AkGroup(d))));
            }
        }
        //     yield call(delay, 1000 * 600); //10分钟内不刷新
    }
}
function* watchRequestOrganizations() {
    const requestChan = yield actionChannel(REQUEST_ORGANIZATIONS, buffers.expanding());
    while (true) {
        yield take(requestChan);
        const { identity: { organizationLoaded } } = yield select();
        if (!organizationLoaded) {
            //yield put(IdentityAction.identityOrganizationLoaded([])); //会导致loaded状态变成true
            const rs = yield call(IdentityAPI.getOrganizations);
            if (rs.Status === 0) {
                yield put(IdentityAction.identityOrganizationLoaded(rs.Data.map(d => new AkOrganization(d))));
            }
        }
        //yield call(delay, 1000 * 600); //10分钟内不刷新
    }
}

function* watchRequestLocations() {
    const requestChan = yield actionChannel(REQUEST_LOCATIONS, buffers.expanding());
    while (true) {
        yield take(requestChan);
        const { identity: { locationLoaded } } = yield select();
        if (!locationLoaded) {
            const rs = yield call(IdentityAPI.getLocations);
            if (rs.Status === 0) {
                yield put(IdentityAction.identityLocationLoaded(rs.Data.map(d => new AkLocation(d))));
            }
        }
        //     yield call(delay, 1000 * 600); //10分钟内不刷新
    }
}

function* watchRequestPositions() {
    while (true) {
        yield take(REQUEST_POSITIONS);
        const { identity: { positionLoaded } } = yield select();
        if (!positionLoaded) {
            const rs = yield call(IdentityAPI.getJobPositions);
            if (rs.Status === 0) {
                yield put(IdentityAction.identityPositionLoaded(rs.Data.map(d => new AkPosition(d))));
            }
        }
    }
    //     yield call(delay, 1000 * 600); //10分钟内不刷新
}

function* watchRequestMetadataCategory() {
    yield take(METADATA_CATEGORY_REQUEST);
    const rs = yield call(MetadataAPI.getCategorys);
    if (rs.Status === 0) {
        yield put(MetadataAction.categoryLoaded(rs.Data));
    }
}

function* fetchMetadata(request: GetMetadataModelRequest) {
    const rs = yield call(MetadataAPI.getByCode, request);
    if (rs.Status === 0) {
        yield put(MetadataAction.loaded(rs.Data, request.categoryCode, request.parentCode));
    }
}

function* watchRequestMetadata() {
    const requestChan = yield actionChannel(METADATA_REQUEST, buffers.expanding());
    while (true) {
        const { payload } = yield take(requestChan);
        const cate = payload.categoryCode;
        const parent = payload.parentCode;

        const { metadata } = AkGlobal.store.getState();
        let metadataDict = metadata.metadataDict;
        if (MetadataDictUtil.requireFetch(metadataDict, cate, parent)) {
            yield put(MetadataAction.requestBegin(cate, parent));
            yield fork(fetchMetadata, { categoryCode: cate, parentCode: parent });
        }
    }
}

function* fetchMetadataByID(request: GetMetadataRequest) {
    const rs = yield call(MetadataAPI.get, request);
    if (rs.Status === 0) {
        yield put(MetadataAction.loaded(rs.Data, request.categoryID, request.parentID));
    }
}

function* watchRequestMetadataByID() {
    const requestChan = yield actionChannel(METADATA_REQUEST_BYID, buffers.expanding());
    while (true) {
        const { payload } = yield take(requestChan);
        const cate = payload.categoryID;
        const parent = payload.parentID;
        if (cate) {
            const { metadata } = AkGlobal.store.getState();
            let metadataDict = metadata.metadataDict;
            if (MetadataDictUtil.requireFetch(metadataDict, cate, parent)) {
                yield put(MetadataAction.requestBegin(cate, parent));
                yield fork(fetchMetadataByID, { categoryID: cate, parentID: parent, isChild: true, status: -1 });
            }
        }
    }
}


// function* watchRequestContentLists() {
//     yield take(CONTENT_LIST_REQUEST);
//     const rs = yield call(ContentListApi.GetLists);
//     if (rs.Status === 0) {
//         yield put(ContentListAction.listLoaded(rs.Data));
//     }
// }
function* watchRequestContentLists() {
    while (true) {
        const { payload } = yield take(CONTENT_LIST_REQUEST);
        if (payload) {
            let params: any = { AppID: payload.appID, title: payload.title || "" }
            if (payload.type) {
                params = { ...params, type: payload.type };
            }
            const rs = yield call(ContentListApi.GetLists, params);
            if (rs.Status === 0) {
                yield put(ContentListAction.listLoaded(rs.Data));
            }
        }
    }
}

let contentListFieldRequestWatcher = {};

function* watchRequestContentList() {
    while (true) {
        const { payload: { appID, listID } } = yield take(CONTENT_FIELD_REQUEST);

        if (listID && !(listID in contentListFieldRequestWatcher)) {
            contentListFieldRequestWatcher[listID] = true;
            yield fork(requestContentListFields, appID, listID);
        }
    }
}

/**
 * 实际执行contentlistfields请求
 * @param appid
 * @param listid 
 */
function* requestContentListFields(appid, listid) {
    const rs = yield call(ContentListApi.GetFields, { AppID: appid, ListID: listid });
    if (rs.Status === 0) {
        yield put(ContentListAction.fieldLoaded(rs.Data, listid));
    } else {
        delete contentListFieldRequestWatcher[listid]; //请求失败，移除cache
    }
}


/**  判断是否为AppAdmin */
// function* watchRequestAppInfo() {
//     while (true) {
//         yield take(REQUEST_APPINFO);
//         const rs = yield call(IdentityAPI.getAppAdmin);
//         if (rs.Status === 0) {
//             yield put(AppAdmin.isAppAdminLoad(rs.Data));
//             break; //请求成功后终止循环，只请求一次
//         }
//     }
// }
function* watchRequestAppInfo() {
    while (true) {
        const { payload } = yield take(REQUEST_APPINFO);
        const rs = yield call(IdentityAPI.getAppAdmin, payload);
        if (rs.Status === 0) {
            yield put(AppAdmin.isAppAdminLoad(rs.Data));
            break; //请求成功后终止循环，只请求一次
        }
    }
}

let formCache;
let formUpdateCache = {};

function* watchFormDelayUpdate() {
    while (true) {
        const { payload: { form, values } } = yield take(FORM_DELAY_UPDATE);
        // console.log("request delay update", values);
        formCache = form;
        formUpdateCache = { ...formUpdateCache, ...values };
    }
}


function* processDelayUpdate() {
    while (true) {
        try {
            if (formUpdateCache && Object.keys(formUpdateCache).length > 0) {
                const temp = formUpdateCache;
                formUpdateCache = {};
                formCache.setFieldsValue(temp);
            }
        } catch (err) {
            console.log("execute form delay update error", err, formUpdateCache);
        }
        yield delay(400);

    }
}

function* checkFormCache() {
    while (true) {
        let { payload: { handler } } = yield take(CHECK_FORM_CACHE);
        try {
            // console.log("submit wait");
            yield delay(1000)
            // console.log("submit complete", formUpdateCache);
            while (formUpdateCache && Object.keys(formUpdateCache).length > 0) {
                yield delay(450)
            }
            handler();
        } catch (err) {
            console.log("execute form delay update error", err, formUpdateCache);
        }
    }
}

let libraryRequestCache = {};
const LibraryCacheStatusProcessing = 1;
const LibraryCacheStatusDone = 2;

/**
 * file library request
 */
function* fileLibraryRequest() {
    while (true) {
        const { payload } = yield take(FILE_LIBRARY_REQUEST);
        if (!libraryRequestCache[payload]) {
            //缓存中不存在
            libraryRequestCache[payload] = LibraryCacheStatusProcessing;
            yield fork(processLibraryRequest, payload);
        }
    }
}

/**
 * 执行library request请求
 * @param code 
 */
function* processLibraryRequest(code) {
    const rs = yield FileUpLoadAPI.getLibraryByCode({ Code: code });
    if (rs.Status === 0) {
        libraryRequestCache[code] = LibraryCacheStatusDone;
        yield put(FileAction.libraryLoaded(code, rs.Data));
    } else {
        delete libraryRequestCache[code]; //请求失败，移除cache
    }
}



function* watchRequestUserBaseInfo() {
    const userInfoResponse = yield call(CommonBaseInfoApi.GetPeifileInfo);
    const dataSourceResponse = yield request.get(`https://cdn.yungalaxy.com/yeeflow/pub/ver3.0/timezone.json`);
    if (userInfoResponse.Status === 0 && dataSourceResponse) {
        const dataSource = JSON.parse(dataSourceResponse.text).Data;
        const userInfo: UserProfileInfo = userInfoResponse.Data;
        const ServerTimeZoneItem = dataSource.find(item => item.be === userInfo.ServerTimeZone);
        const timeZone = userInfo.TimeZone;
        const timeZoneFe = timeZone ? JSON.parse(timeZone).fe : moment.tz.guess();
        const baseInfo: CommonUserBaseInfo = {
            UserID: userInfo.UserID,
            TenantID: userInfo.TenantID,
            TimeZone: userInfo.TimeZone,
            TimeZoneFe: timeZoneFe,
            DateFormat: userInfo.DateFormat || "YYYY-MM-DD",
            LanguageCode: userInfo.LanguageCode,
            ServerTimeZone: (ServerTimeZoneItem && ServerTimeZoneItem.fe) || AkDatetimeUtil.defaultTimeZone,
            isSuccess: true
        }
        // if (AkContext.isYeeFlow()) {
        moment.tz.setDefault(timeZoneFe);
        AkGlobal.reloadMasterpage(userInfo.LanguageCode);
        // }
        yield put(CommonYeeflowBaseInfoAction.userBaseInfo(baseInfo));
    } else {
        const errorbaseInfo: CommonUserBaseInfo = {
            UserID: 0,
            TenantID: 0,
            TimeZone: null,
            TimeZoneFe: moment.tz.guess(),
            ServerTimeZone: moment.tz.guess(),
            DateFormat: "YYYY-MM-DD",
            LanguageCode: "",
            isSuccess: false
        }
        yield put(CommonYeeflowBaseInfoAction.userBaseInfo(errorbaseInfo));
        AkGlobal.reloadMasterpage(errorbaseInfo.LanguageCode);
    }
}

export function* mainSaga(): any {
    yield [
        fork(watchNotification),
        // fork(watchRequestTaskCount),
        fork(watchRequestUsers),
        fork(watchRequestOrganizations),
        fork(watchRequestLocations),
        fork(watchRequestPositions),
        fork(watchRequestGroups),
        fork(watchRequestMetadata),
        fork(watchRequestMetadataCategory),
        fork(watchRequestMetadataByID),
        fork(watchRequestContentLists),
        fork(watchRequestContentList),
        fork(watchRequestAppInfo),
        fork(watchFormDelayUpdate),
        fork(processDelayUpdate),
        fork(checkFormCache),
        fork(watchRequestUserBaseInfo),
        fork(fileLibraryRequest)
    ]
}