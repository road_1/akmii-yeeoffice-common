import * as React from "react";
import { Component } from "react";
import { AkGlobal } from "../../util/common";
import { FlowGuideModalTips } from "../../locales/localeid";
import { AkModal } from "../controls/ak-modal";

export interface FlowGuideModalProps {
    addModalClick?: () => void;
    skipClick?: () => void;
    showModal?: boolean;
};
export interface FlowGuideModalStates {
    showAddModal?: boolean;
    tabContent?: string;
};

export class FlowGuideModal extends React.Component<FlowGuideModalProps, FlowGuideModalStates>{
    guideSetTimeOut = null;
    guideListAndTime = [];
    guideList = [];
    constructor(props, context) {
        super(props, context);
        this.state = {
            showAddModal: false,
            tabContent: AkGlobal.intl.formatMessage({ id: FlowGuideModalTips.GuideCreateWorkflow }),
        };

        this.guideList = [
            AkGlobal.intl.formatMessage({ id: FlowGuideModalTips.GuideCreateWorkflow }),
            AkGlobal.intl.formatMessage({ id: FlowGuideModalTips.GuideDesignSteps }),
            AkGlobal.intl.formatMessage({ id: FlowGuideModalTips.GuideDesignForms }),
            AkGlobal.intl.formatMessage({ id: FlowGuideModalTips.GuidePublishWorkflow })
        ];

        this.guideListAndTime = [
            {
                guide:AkGlobal.intl.formatMessage({ id: FlowGuideModalTips.GuideCreateWorkflow }),
                times:7000
            },
            {
                guide:AkGlobal.intl.formatMessage({ id: FlowGuideModalTips.GuideDesignSteps }),
                times:14000
            },
            {
                guide:AkGlobal.intl.formatMessage({ id: FlowGuideModalTips.GuideDesignForms }),
                times:17000
            },
            {
                guide:AkGlobal.intl.formatMessage({ id: FlowGuideModalTips.GuidePublishWorkflow }),
                times:11000
            }
        ];
    }

    componentDidMount() {
        this.setTimeOutCarousel(0,7000);
    }

    setTimeOutCarousel(key,value) {
        let topThis = this;
        this.setState({
            tabContent:this.guideList[key]
        });
        this.guideSetTimeOut = setTimeout(()=>{
            clearTimeout(this.guideSetTimeOut);
            if (key === 3) {
                topThis.setTimeOutCarousel(0,7000);
            } else {
                topThis.setTimeOutCarousel(key+1,topThis.guideListAndTime[key+1].times);
            }
        },value);
    }

    TabLiClickHandle(da) {
        clearTimeout(this.guideSetTimeOut);
        
        for (let i = 0;i<this.guideListAndTime.length;i++) {
            if (da === this.guideListAndTime[i].guide) {
                this.setTimeOutCarousel(i,this.guideListAndTime[i].times);
            }
        }
    }

    renderTabLi() {
        const { formatMessage } = AkGlobal.intl;
        let topThis = this;
        let dataLi = [formatMessage({ id: FlowGuideModalTips.GuideCreateWorkflow }), formatMessage({ id: FlowGuideModalTips.GuideDesignSteps }),
        formatMessage({ id: FlowGuideModalTips.GuideDesignForms }), formatMessage({ id: FlowGuideModalTips.GuidePublishWorkflow })];

        return dataLi.map((da, index) => {

            return <li className={this.state.tabContent === da ? "active" : ""} key={index} onClick={() => { topThis.TabLiClickHandle(da) }}>
                <a href="javascript:void(0)">{da}</a>
            </li>

        });
    }

    renderTabContent() {
        const { formatMessage } = AkGlobal.intl;
        const { tabContent } = this.state;
        let dataLi = [formatMessage({ id: FlowGuideModalTips.GuideCreateWorkflow }), formatMessage({ id: FlowGuideModalTips.GuideDesignSteps }),
        formatMessage({ id: FlowGuideModalTips.GuideDesignForms }), formatMessage({ id: FlowGuideModalTips.GuidePublishWorkflow })];
        let dataImg = [window["CDN"] + "images/guide01.gif", window["CDN"] + "images/guide02.gif", window["CDN"] + "images/guide03.gif", window["CDN"] + "images/guide04.gif"];
        return <div>
            <div className="picbox">
                <img
                    src={
                        tabContent === "" || tabContent === dataLi[0] ? dataImg[0] :
                            tabContent === dataLi[1] ? dataImg[1] :
                                tabContent === dataLi[2] ? dataImg[2] :
                                    dataImg[3]
                    }
                    className="img-guide-content" />
            </div>
            <p className="guidedesc">
                {
                    tabContent === "" || tabContent === dataLi[0] ? formatMessage({ id: FlowGuideModalTips.GuideCreateWorkflowContent }) :
                        tabContent === dataLi[1] ? formatMessage({ id: FlowGuideModalTips.GuideDesignStepsContent }) :
                            tabContent === dataLi[2] ? formatMessage({ id: FlowGuideModalTips.GuideDesignFormsContent }) :
                                formatMessage({ id: FlowGuideModalTips.GuidePublishWorkflowContent })
                }
            </p>
        </div>
    }

    createWorkflow() {
        const { addModalClick } = this.props;
        if (addModalClick) {
            addModalClick();
        } else {
            return;
        }
    }

    skipClick() {
        const { skipClick } = this.props;
        if (skipClick) {
            skipClick();
        } else {
            return;
        }
    }

    render() {
        const { formatMessage } = AkGlobal.intl;
        let topThis = this;
        let guideTitle = <div className="guidePage-title">
            <a className="skipbtn" data-dismiss="modal" onClick={() => { topThis.skipClick() }}>{formatMessage({ id: FlowGuideModalTips.GuideSkipText })} <i className="caretright"></i></a>
            <h3 className="title-h3">{formatMessage({ id: FlowGuideModalTips.GuideTitle })}</h3>
            <p className="title-content">{formatMessage({ id: FlowGuideModalTips.GuideContent })}</p>
        </div>

        return (
            <AkModal
                visible={this.props.showModal}
                footer={null}
                title={guideTitle}
                dragable={false}
                width={"60%"}
                closable={false}
                className="ak-flow-guidePage-modal"
            >
                <div className="guidemaincon clearfix">
                    <div className="lefttab">
                        <ul className="guidetab">
                            {topThis.renderTabLi()}
                        </ul>

                        <button className="guide-startbtn" onClick={() => { topThis.createWorkflow() }}>{formatMessage({ id: FlowGuideModalTips.GuideCreateWorkflowBtn })}</button>
                    </div>
                    <div className="righttabcon tab-content">
                        {topThis.renderTabContent()}
                    </div>

                </div>
            </AkModal>
        )
    }
}