import * as React from 'react';
import {Component} from 'react';
import { AkModal } from '../controls';
import { AkContext, AkGlobal, MerchatLoginModel, EnterpriseFlag } from '../..';
import { MasterPageLocale } from '../../locales/localeid';
import { LocalizationAPI } from '../../api/common/localization';

export interface YeeflowRemindProps {
    user?: any 
    onSetRenderStatus?: () =>void
};
export interface YeeflowRemindStates {
    showRemindModal?: boolean,
    merchat?: MerchatLoginModel;
};

export class YeeFlowRemind extends Component<YeeflowRemindProps, YeeflowRemindStates>{
    constructor(props, context) {
        super(props, context)
        this.state = {
            showRemindModal: false
        }
    }
    
    componentDidMount(){
        const {props: {onSetRenderStatus}} = this;
        let loginData = AkContext.getMerchantData();
        const {EnterpriseInfo} = loginData;
        if(EnterpriseInfo.Flag === EnterpriseFlag.FreeComplete || EnterpriseInfo.Flag === EnterpriseFlag.SubscriptionExpired) {
            loginData = AkContext.RefreshAKTokenData();
        }
        const hashName = location.hash;
        if (hashName.indexOf("payments")!==2 && (EnterpriseInfo.Flag === EnterpriseFlag.FreeComplete || EnterpriseInfo.Flag === EnterpriseFlag.SubscriptionExpired)) {
            this.setState({showRemindModal: true});
            onSetRenderStatus()
        } else {
            AkGlobal.reloadMasterpage();
        }
    }
    render() {
        const { formatMessage } = AkGlobal.intl;
        const {state: { showRemindModal }, props: {user}} = this
        return showRemindModal ? <AkModal 
            visible={true}
            closable={false}
            title={null}
            footer={null}
            className="ak-yeeflow-remind"
        >
            <h3 className="ak-trial-name">{formatMessage({ id: MasterPageLocale.SuSubscriptionRemindTitle })}</h3>
            <p className="ak-trialdesc">{formatMessage({ id: MasterPageLocale.SuSubscriptionRemindDescription })}</p>
            <div className="ak-wrfooter">
                {
                    user && user["IsAdmin"]?<button className={AkContext.isYeeFlow()?"ak-wrfbtn ak-submitbtn":"ak-wrfbtn ak-submitbtn-yeeoffice"} onClick={() => {
                        window.location.href = "/Home/settings#/payments/order/upgrade/YeeFlow";
                    }}>
                        {formatMessage({ id: MasterPageLocale.SuSubscriptionRemindBtn })}
                    </button>: null
                }
                
            </div>
        </AkModal> : null
    }
}