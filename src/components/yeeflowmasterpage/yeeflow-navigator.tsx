import * as React from "react";
import { AkPopover, AkIcon, AkRow, AkCol } from "..";
import { connect } from "react-redux";
import { NavigationHelper, AkUtil, AppCenterListResponse, URLHelper, NavigationModel, NavigationResponse } from "../..";
import { AkGlobal, AkContext, AppKeys } from '../../util/common';
import { appCenter } from '../../reducers/AppCenter';
import akGroupSelect from "../identity/ak-group-select";
import { MASTERMENU_PREFIX, PortalViewModel } from "../masterpage/navigator";
import { CommonLocale } from '../../locales/localeid';

export interface YeeFlowNavigatorProps {
    appCenterList?: AppCenterListResponse[];
    systemAppList?: NavigationModel[];
    portalTreeData?: PortalViewModel;
    navigationList?: NavigationResponse[];
}
export interface YeeFlowNavigatorStates {
    visible: boolean;
    showFooter: boolean;
}

const mapStateToProps = (state) => {

    let navigationList = state.appCenter.navigationList ? AkUtil.deepClone(state.appCenter.navigationList).filter(item => {
        const HideEntrance = item.AppCenterID < 10000 ? false : item.Settings && JSON.parse(item.Settings).HideEntrance;
        if (HideEntrance) {
            return false;
        } else {
            return true;
        }
    }) : [];
    //sp登陆.net环境地址转换
    let appCenterList = state.appCenter.appCenterList ? AkUtil.deepClone(state.appCenter.appCenterList).filter(item => {
        const HideEntrance = item.Settings && JSON.parse(item.Settings).HideEntrance;
        if (HideEntrance) {
            return false;
        } else {
            return true;
        }
    }) : [];
    appCenterList = appCenterList.map(i => {
        i.WelcomePage = NavigationHelper.parseURL(i.WelcomePage);
        return i;
    });
    return {
        portalTreeData: state.appCenter.portalTreeData,
        systemAppList: state.appCenter.systemAppList,
        appCenterList: appCenterList,
        navigationList: navigationList
    }
}

const DEFAULT_SHOW_COUNT = 12;

@connect(mapStateToProps)
export class YeeFlowNavigator extends React.Component<YeeFlowNavigatorProps, YeeFlowNavigatorStates>{
    constructor(props, context) {
        super(props, context)
        this.state = {
            visible: false,
            showFooter: true,
        }
    }

    renderPortal(item?: NavigationResponse) {
        const { props: { portalTreeData } } = this;
        if (!item) return;
        var portalApp = AkContext.getAppinfoByAppKey(AppKeys.YeeOfficePortal);
        const href = URLHelper.GetDomainUrl(item.AppCenterID, "#/sites" + "/");
        return <AkCol key="YeeOfficePortal" span={24} lg={8} md={8} sm={8} xs={8}>
            <div className="ak-yeeflow-appstore-appbox"
                onClick={() => {
                    this.setState({ visible: false });
                    window.location.href = AkContext.getBranch() === AkContext.YeeOffice ? NavigationHelper.getNavigationUrl(portalApp.AppKey, item.WelcomePage) + href : href;
                }}>
                <img className="bgcolor-YeeOfficePortal" src={window["CDN"] + item.IconUrl} />
                <p>{AkGlobal.intl.formatMessage({ id: MASTERMENU_PREFIX + "YeeOfficePortal" })}</p>
            </div>
        </AkCol>
    }

    renderApps() {
        const { props: { systemAppList, portalTreeData, navigationList } } = this;
        const systemCount = (systemAppList ? systemAppList.length - 1 : 0) + (portalTreeData ? 1 : 0);
        return <AkRow className="ak-yeeflow-appstore-content">
            {/* {this.renderPortal()} */}
            {/* {this.renderSystemApp()} */}
            {this.renderUserDefinedApp(DEFAULT_SHOW_COUNT - systemCount)}
        </AkRow>
    }

    /**渲染系统自带App */
    renderSystemApp(item?: NavigationResponse) {
        const { props: { systemAppList } } = this;
        if(item && item.AppCenterID !== 2 && item.AppCenterID !== 30) {
            let app = AkContext.getAppinfobyLocalStorage().find(a => {
                return a.AppInfoID === item.AppCenterID;
            });
            //系统设置不显示
            if (app && app.AppKey === "YeeOfficeSettings") {
                // let user = AkContext.getUser();
                // if (user && !user["IsAdmin"]) {
                return null;
                // }
            }
            //修改.net版本地址（默认存储sp的地址）
            if (app && AkContext.getBranch() === AkContext.YeeOffice) {
                item.WelcomePage = NavigationHelper.getNavigationUrl(app.AppKey, item.WelcomePage)
            }

            return <AkCol key={item.AppCenterID} span={24} lg={8} md={8} sm={8} xs={8}>
                <div className="ak-yeeflow-appstore-appbox"
                    onClick={() => {
                        this.setState({ visible: false });
                        window.location.href = URLHelper.GetDomainUrl(item.AppCenterID, item.WelcomePage);
                    }}>
                    <img className={app ? "bgcolor-" + app.AppKey : ""} src={window["CDN"] + item.IconUrl} />
                    <p>
                        {app ?
                            AkGlobal.intl.formatMessage({ id: MASTERMENU_PREFIX + app.AppKey }) :
                            (item && item.Title)
                        }
                    </p>
                </div>
            </AkCol>
        } else if(item && item.AppCenterID === 2) {
            return this.renderPortal(item)
        } else {
            return null;
        }
    }

    /**渲染用户自定义App */
    renderUserDefinedApp(showcount?: number) {
        const { props: { appCenterList, navigationList }, state: { showFooter } } = this;
        return navigationList && navigationList.map((item, index) => {
            if (showFooter && (index > DEFAULT_SHOW_COUNT - 1)) return null;
            if(item.AppCenterID < 10000) {
                return this.renderSystemApp(item)
            } else {
                return <AkCol key={index} span={24} lg={8} md={8} sm={8} xs={8}>
                    <div
                        className="ak-yeeflow-appstore-appbox"
                        onClick={() => {
                            this.setState({ visible: false });
                            window.location.href = URLHelper.getNewAppJumpUrl(item.WelcomePage, item.RelativeUrl);
                        }}>
                        <img src={item.IconUrl || process.env.CDN + "images/placeholder.png"} />
                        <p>{item.Title}</p>
                    </div>
                </AkCol>
            }
            
        })
    }

    renderFooter() {
        return <div onClick={() => this.setState({ showFooter: false })}>
            {AkGlobal.intl.formatMessage({ id: CommonLocale.more })}
        </div>
    }

    render() {
        const { state: { showFooter } } = this;
        const { props: { systemAppList, portalTreeData, appCenterList } } = this;
        const systemCount = (systemAppList ? systemAppList.length - 1 : 0) + (portalTreeData ? 1 : 0);
        const customappCount = appCenterList.length;
        return <div
            className="ak-master-navigator"
            style={{ float: "right" }}
        >
            <AkPopover
                onVisibleChange={visible => this.setState({ visible })}
                visible={this.state.visible}
                overlayClassName={
                    showFooter && systemCount + customappCount > DEFAULT_SHOW_COUNT ? "ak-yeeflow-appstore" : "ak-yeeflow-appstore ak-yeeflow-appstore-footer-disable"
                }
                trigger="click"
                getPopupContainer={() => document.getElementsByClassName("ak-master-navigator")[0] as HTMLElement}
                placement="bottomRight"
                title={this.renderApps()}
                content={this.renderFooter()}>
                <AkRow className="ak-master-navigator-detail">
                    <AkIcon type="app-store" />
                </AkRow>
            </AkPopover>
        </div >;
    }
}