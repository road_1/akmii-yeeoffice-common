import * as React from "react";
import { AkButton } from '../controls/ak-button';
import { AkGlobal, AkContext } from '../../util/common';
import { CommonLocale, MasterPageLocale } from '../../locales/localeid';
import { AkSelect } from '../controls/ak-select';
import { UserModel } from "../..";

export interface YeeFlowLanguageProps {
    userInfo?: UserModel;
    languages?: string[]; //系统支持的语言，默认['zh', 'en']
    onCancel?: () => void;
    onOk?: (value) => void;
}
export interface YeeFlowLanguageStates { }

const LANGUAGE_PREFIX = "masterpage.language.";

export class YeeFlowLanguage extends React.Component<YeeFlowLanguageProps, YeeFlowLanguageStates>{
    language?: string;
    constructor(props, context) {
        super(props, context)
        this.language = AkContext.getLanguage() || AkContext.getMerchantData().CompanyInfo.LanguageCode;
    }
    render() {
        const { props: { languages, onCancel, onOk } } = this;
        return <div className="ak-yeeflow-language">
            <div className="ak-yeeflow-language-content-item">
                <span className="lable">{AkGlobal.intl.formatMessage({ id:MasterPageLocale.Language})}</span>
                <div className="control">
                    <AkSelect
                        defaultValue={this.language}
                        onChange={(value: string) => {
                            this.language = value
                        }}>
                        {languages.map(i =>
                            <AkSelect.Option key={i} value={i}>{AkGlobal.intl.formatMessage({ id: LANGUAGE_PREFIX + i })}</AkSelect.Option>
                        )}
                    </AkSelect>
                </div>
            </div>
            <div></div>
            <div></div>
            <div></div>
            <div className="ak-yeeflow-language-footer">
                <AkButton
                    size="large"
                    className="ak-yeeflow-language-footer-button1"
                    onClick={() => {
                        onOk && onOk(this.language)
                    }}
                >
                    {AkGlobal.intl.formatMessage({ id: CommonLocale.Save })}
                </AkButton>
                <AkButton
                    size="large"
                    className="ak-yeeflow-language-footer-button2"
                    onClick={() => {
                        onCancel && onCancel()
                    }}
                >
                    {AkGlobal.intl.formatMessage({ id: CommonLocale.Cancel })}
                </AkButton>
            </div>
        </div>
    }
}