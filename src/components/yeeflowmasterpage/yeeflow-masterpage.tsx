import * as React from "react";
import { URLHelper, AkLayout, AkRow, AkCol, AkUtil, AkContext, MerchatLoginModel, AkGlobal, MasterPageAction, AkIcon } from "../..";
import { connect } from "react-redux";
import { MasterPageLocale } from "../../locales/localeid";
import { MasterNotification } from "../masterpage/notify/masternotification";
import { YeeFlowMasterPageUserInfo, YeeFlowTitle, YeeFlowSettings, YeeFlowMenu, YeeFlowSettingsBack } from ".";
import { YeeFlowRemind } from "./yeeflow-remind";
import { YeeflowSubscriptionModal } from "./yeeflow-subscription-modal";
import { MerchatAPI } from "../../api/masterpage/masterpage";
import { FlowGuideModal } from "./flow-guide-modal";

export interface YeeFlowMasterPageProps {
    showHeader?: boolean;
    baseInfo?: CommonUserBaseInfo;
    showMasterHeader?: boolean;
    languages?: string[]; //系统支持的语言，默认['zh', 'en']
    onChangeLanguage?: (language: string) => void;
    onLoaded?: (merchat?: MerchatLoginModel) => void;
    renderActions?: React.ReactNode | React.ReactNode[];
}

export interface YeeFlowMasterPageStates {
    isRemind?: boolean;
    /**展示引导页 */
    showGuidePageModal?: boolean;
    renderStatus?: boolean;
}

const mapStateToProps = (state) => {
    return {
        showHeader: state.masterpage.showHeader,
        baseInfo: state.commonBaseInfo.baseInfo
    };
}

@connect(mapStateToProps)
export class YeeFlowMasterPage extends React.Component<YeeFlowMasterPageProps, YeeFlowMasterPageStates>{
    static defaultProps: YeeFlowMasterPageProps = {
        showMasterHeader: true,
        languages: ['zh', 'en']
    };
    showHeader: boolean = true;
    constructor(props, context) {
        super(props, context)
        URLHelper.init();
        require("../../themes/masterpage.less");
        this.showHeader = this.props.showMasterHeader;
        if (AkContext.isMobileLoginIn()) {
            this.showHeader = false
        }
        this.state = {
            isRemind: false,
            showGuidePageModal: false,
            renderStatus: false
        };
    }

    componentWillMount() {
        const { props: { onLoaded } } = this;
        const loginData = AkContext.getMerchantData();
        AkGlobal.store.dispatch(MasterPageAction.triggerMasterpageHeader(this.showHeader));
        onLoaded && onLoaded(loginData);
        this.setCompanyName(loginData);
    }

    componentDidMount() {
        //若为Yeeflow则调用获取新用户接口
        if (AkContext.isYeeFlow()) {
            this.getGuideViewData();
        }
    }

    //引导页modal点击事件
    addGuideModalClick = () => {
        //发送dispatch，让其显示创建流程modal
        AkGlobal.store.dispatch(MasterPageAction.showCreateWorkFlowModal(true));

        this.setState({ showGuidePageModal: false });
        this.putGuideViewData();
    }

    //引导页跳过点击事件
    guideSkipClick = () => {
        this.setState({ showGuidePageModal: false });
        this.putGuideViewData();
    }

    //获取该用户是否为新用户
    getGuideViewData() {
        let isYeeflow = AkContext.isYeeFlow();
        MerchatAPI.getNewUserForGuideModal().then((res) => {
            if (!res.Data && res.Status === 0) {
                if (isYeeflow) {
                    this.setState({ showGuidePageModal: true });
                }
            }
        });
    }

    //写入已阅读引导页记录
    putGuideViewData() {
        MerchatAPI.putUserGuideViewData().then((res) => {
            if (res.Status === 0) {
                this.setState({ showGuidePageModal: false });
            }
        });
    }

    setCompanyName(loginData) {
        /**统一用公司名称*/
        if (AkContext.isNeteaseVersion()) {
            document.title = AkGlobal.intl.formatMessage({ id: MasterPageLocale.MasterTitleNetease });
        } else {
            document.title = loginData ? loginData.CompanyInfo.CompanyName : AkGlobal.intl.formatMessage({ id: MasterPageLocale.PageTitle });
        }
    }

    getLogo(logo: string) {
        if (logo) {
            return logo;
        } else {
            if (AkContext.isNeteaseVersion()) {
                return "images/logo-psm-w.png";
            } else {
                return "images/logo-psm.png";
            }
        }
    }

    render() {
        const { props: { languages, showHeader, baseInfo }, state: {renderStatus} } = this;
        let show = true;
        if (!this.showHeader) {
            show = false;
        }
        if (!showHeader) {
            show = false;
        }
        const company = AkContext.getCompanyInfo();
        const loginData = AkContext.getMerchantData();
        let user = AkContext.getUser();
        if( renderStatus ) {
            return <YeeFlowRemind user={user}  onSetRenderStatus={() => {
                this.setState({renderStatus: true})
            }} />
        }
        return loginData && baseInfo.TenantID ? <AkLayout className="ak-react-masterpage">
            {AkContext.isYeeFlow() && this.state.showGuidePageModal ? <FlowGuideModal showModal={this.state.showGuidePageModal} addModalClick={() => { this.addGuideModalClick() }} skipClick={() => { this.guideSkipClick() }} /> : null}
            <AkLayout.Header style={show ? null : YeeFlowMasterPageStyle.disableHeaderStyle} className="ak-react-masterpage-header" >
                <AkRow type="flex" justify="space-between" align="middle" style={company ? {
                    background: company.NavBarColour, color: company.NavFontColour,
                    boxShadow: "0 0 5px 0 rgba(0,0,0,.3)"
                } : { boxShadow: "0 0 5px 0 rgba(0,0,0,.3)" }}>
                    <AkCol xs={11} sm={15} md={8} lg={8} >
                        <YeeFlowSettingsBack />
                        <YeeFlowMenu />
                        <YeeFlowTitle loginData={loginData} />
                    </AkCol>
                    <AkCol xs={0} sm={0} md={8} lg={8} style={{ textAlign: "center" }}>
                        <img className="ak-master-logo" src={loginData && URLHelper.GetFileUrl(this.getLogo(loginData.CompanyInfo.Logo))} />
                    </AkCol>
                    <AkCol xs={13} sm={9} md={8} lg={8}>
                        <AkRow type="flex" justify="end">
                            <AkCol>
                                <YeeFlowRemind user={user} onSetRenderStatus={() => {
                                    this.setState({renderStatus: true})
                                }} />
                            </AkCol>
                            {AkUtil.isArray(this.props.renderActions) ?
                                (this.props.renderActions as React.ReactNode[]).map(item => {
                                    if ((this.props.renderActions as React.ReactNode[]).length > 0) {
                                        return <AkCol>
                                            {item}
                                        </AkCol>
                                    }
                                })
                                : (this.props.renderActions &&
                                    <AkCol>
                                        {this.props.renderActions}
                                    </AkCol>
                                )
                            }
                            {/* <AkCol>
                                <YeeFlowNavigator />
                            </AkCol> */}
                            <AkCol>
                                <MasterNotification></MasterNotification>
                            </AkCol>
                            <AkCol>
                                {
                                    user && !user["IsAdmin"] ? null :
                                        <YeeFlowSettings />
                                }
                            </AkCol>
                            <AkCol>
                                <YeeFlowMasterPageUserInfo
                                    languages={languages}
                                    enterpriseInfo={loginData && loginData.EnterpriseInfo}
                                    userInfo={loginData && loginData.UserModel}
                                    onChangeLanguage={(language) => {
                                        this.props.onChangeLanguage(language);
                                    }} />
                            </AkCol>
                        </AkRow>
                    </AkCol>
                </AkRow>
            </AkLayout.Header>
            <AkLayout className="ak-react-masterpage-content">
                <AkLayout.Content className="ak-master-content" style={show ? YeeFlowMasterPageStyle.contentStyle : null}>
                    <div style={YeeFlowMasterPageStyle.contentHeight(show)}>
                        {this.props.children}
                    </div>
                </AkLayout.Content>
            </AkLayout>
        </AkLayout > : null;
    }
}

class YeeFlowMasterPageStyle {
    static disableHeaderStyle: React.CSSProperties = {
        display: "none",
    }
    static contentStyle: React.CSSProperties = {
        marginTop: 56,
    }
    static contentHeight = (showHeader: boolean) => {
        return {
            minHeight: showHeader ? "calc(100vh - 56px)" : "calc(100vh - 0px)",
            backgroundColor: "inherit"
        }
    };
}