import * as React from "react";
import { Component } from "react";
import { AkPopover, AkRow, AkCol, AkButton, AkLocationSelectLabel, AkTag, AkImg } from "../index";
import { MasterPageLocale } from "../../locales/localeid";
import { UserModel, LoginType, MerchantLevelEnum, EnterpriseFlag, EnterpriseInfo } from '../../api/masterpage/masterpagemodel';
import { URLHelper } from "../../util/URLHelper";
import { AkIcon } from '../controls/ak-icon';
import { AkContext } from "../../index";
import { AkGlobal } from '../../util/common';
import UpdatePassword from "../masterpage/updatepassword";
import { YeeFlowLanguage, UserInfoHeader } from ".";

import { YeeflowSubscriptionModal } from "./yeeflow-subscription-modal";
export interface YeeFlowMasterPageUserInfoProps {
    enterpriseInfo?: EnterpriseInfo;
    userInfo?: UserModel;
    languages?: string[]; //系统支持的语言，默认['zh', 'en']
    onChangeLanguage?: (language: string) => void;
    getPopupContainer?: () => HTMLElement;
}
export interface YeeFlowMasterPageUserInfoStates {
    language?: string;
    showUpdataPassword?: boolean;
    showUserInfoCard?: boolean;
    showChangeLanguageCard?: boolean;
    showSubscriptionModal?: boolean;
    isRemind?: boolean;
    showRemindModal?: boolean;
}
export class YeeFlowMasterPageUserInfo extends Component<YeeFlowMasterPageUserInfoProps, YeeFlowMasterPageUserInfoStates> {

    constructor(props, context) {
        super(props, context);
        this.state = {
            language: AkContext.getLanguage(),
            showUpdataPassword: false,
            showUserInfoCard: false,
            showChangeLanguageCard: false,
            showSubscriptionModal: false,
            isRemind: false,
            showRemindModal: true
        };
    }

    renderContent() {
        return <div>
            <UserInfoHeader />
            {this.renderSettingsInfo()}
        </div>
    }

    showSubscriptionModalClick = () => {
        // this.setState({
        //     showSubscriptionModal: true
        // })

        //跳转支付链接
        if (AkContext.isYeeFlow()) {
            window.location.href = "/Home/settings#/payments/order/upgrade/YeeFlow";
        }
    }

    renderSettingsInfo() {
        const { props: { userInfo, enterpriseInfo }, state: { showChangeLanguageCard, showSubscriptionModal } } = this;
        const { formatMessage } = AkGlobal.intl;
        const subscribeLevel = this.renderSubscribeLevel();
        return <div className="ak-yeeflow-usersettings-box">
            {/* {!AkContext.isYeeFlow() && < div className="ak-yeeflow-usersettings-item" onClick={() => {
                this.setState({ showChangeLanguageCard: !showChangeLanguageCard })
            }}>
                <div className="ak-yeeflow-usersettings-title">
                    <span>{formatMessage({ id: MasterPageLocale.LanguageSettings })}</span>
                </div>
                <div className="ak-yeeflow-usersettings-describe">
                    <span>
                        {formatMessage({ id: MasterPageLocale.YeeFlowLanguageTrigger })} | <AkLocationSelectLabel value={userInfo.LocationID} />
                        <AkIcon type="right" />
                    </span>
                </div>
            </div>
            } */}
            <div className="ak-yeeflow-usersettings-item default-cursor" >
                <div className="ak-yeeflow-usersettings-title">
                    <span>{formatMessage({ id: MasterPageLocale.Subscription })}</span>
                    <AkTag closable={false}>{enterpriseInfo.Flag === EnterpriseFlag.Free ?
                        formatMessage({ id: MasterPageLocale.SubscribeLevelNotBuy }) :
                        subscribeLevel
                    }</AkTag>
                </div>
                <div className="ak-yeeflow-usersettings-describe">
                    {enterpriseInfo.Flag === EnterpriseFlag.Free &&
                        <span>
                            {subscribeLevel + " " + formatMessage({ id: MasterPageLocale.Trial }) + " "}
                            <span className="gray">{formatMessage({ id: MasterPageLocale.ExpiresDays }, { number: AkContext.diffDays(null, enterpriseInfo.ExpireDate) })}</span>
                        </span>
                    }
                </div>
                {AkContext.getUser().IsAdmin && AkContext.isYeeFlow() ? <div className="ak-yeeflow-usersettings-subscription" onClick={this.showSubscriptionModalClick}><span>{formatMessage({ id: MasterPageLocale.SubscriptionModal })}</span></div> : null}
            </div>
            {AkContext.isYeeFlow() && <div className="ak-yeeflow-usersettings-item" onClick={() => this.clickHandler("help")}>
                <div className="ak-yeeflow-usersettings-title">
                    <span>
                        {formatMessage({ id: MasterPageLocale.NeedHelp })}
                    </span>
                </div>
                <div className="ak-yeeflow-usersettings-describe">
                    <span>
                        {formatMessage({ id: MasterPageLocale.Resources })}
                        <AkIcon type="right" />
                    </span>
                </div>
            </div>}
            {/* <div className="ak-yeeflow-usersettings-item" onClick={() => this.clickHandler("news")}>
                <div className="ak-yeeflow-usersettings-title">
                    <span>
                        {formatMessage({ id: MasterPageLocale.NewsRoom })}
                    </span>
                </div>
                <div className="ak-yeeflow-usersettings-describe">
                    <span className="purple">
                        {formatMessage({ id: MasterPageLocale.WhatNew })}
                    </span>
                </div>
            </div> */}
        </div >
    }



    renderSubscribeLevel() {
        const { enterpriseInfo } = this.props;
        switch (enterpriseInfo.Level) {
            case MerchantLevelEnum.Free:
                return AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeLiteTitle });
            case MerchantLevelEnum.Enterprise:
                return AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeEnterpriseTitle });
            case MerchantLevelEnum.Standard:
                return AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeStandardTitle });
            case MerchantLevelEnum.Ultimate:
                return AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeEnterpriseTitle });
            default:
                return null;
        }
        // if (enterpriseInfo.Flag === EnterpriseFlag.Free) {
        //     text += " " + AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeLevelNotBuy });
        // } else {
        //     text += " " + AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeLevelBuy });
        // }

    }



    clickHandler(type: "help" | "news" | "changepwd") {
        let url;
        let languagPath = "";
        if (AkContext.getLanguage().toLowerCase() === "zh") {
            languagPath = "zh-cn/"
        }

        switch (type) {
            case "help":
                url = process.env.EntranceSite + languagPath + "index/resources.html";
                break;
            case "news":
                url = process.env.EntranceSite + languagPath + "index/releasenotes.html";
                break;
            case "changepwd":
                url = window.location.origin + "/Account/changepassword";
                break;
            default:
                url = "";
        }
        window.open(url);
    }

    renderFooter() {
        const { props: { enterpriseInfo } } = this
        return <div className="footer-box">
            {AkContext.getAccountType() === LoginType.YeeOffice ? (
                AkContext.isYeeFlow() ?
                    <span className="left" onClick={() => this.clickHandler("changepwd")}>
                        {AkGlobal.intl.formatMessage({ id: MasterPageLocale.ChangePassword })}
                    </span> :
                    <span
                        className="left"
                        onClick={() => {
                            if (AkContext.getAccountType() === LoginType.YeeOffice) {
                                this.setState({ showUpdataPassword: true })
                            } else {
                                window.location.href = AkContext.getCloseConnectionURI();
                            }
                        }}>
                        {
                            AkGlobal.intl.formatMessage({
                                id: AkContext.getAccountType() === LoginType.YeeOffice ?
                                    MasterPageLocale.ChangePassword :
                                    MasterPageLocale.ChangeUser
                            })
                        }
                    </span>)
                : null
            }
            {AkContext.getAccountType() === LoginType.YeeOffice ? <span className="line">|</span> : null}
            <span
                className={AkContext.getAccountType() === LoginType.YeeOffice ? "right" : "right center"}
                onClick={() => {
                    window.location.href = AkContext.getSignOutURI();
                }}>
                {AkGlobal.intl.formatMessage({ id: MasterPageLocale.SignOut })}
            </span>
        </div >
    }

    renderLanguageCard() {
        const { state: { showChangeLanguageCard }, props: { languages, userInfo, onChangeLanguage } } = this;
        return showChangeLanguageCard &&
            <YeeFlowLanguage
                userInfo={userInfo}
                languages={languages}
                onOk={(language) => {
                    onChangeLanguage && onChangeLanguage(language);
                    this.setState({ showChangeLanguageCard: false });
                }}
                onCancel={() => this.setState({ showChangeLanguageCard: false })}
            />
    }

    renderChangePwdCard() {
        return this.state.showUpdataPassword &&
            <UpdatePassword
                onOk={() => this.setState({ showUpdataPassword: false })}
                onCancel={() => this.setState({ showUpdataPassword: false })}
            />
    }

    render() {
        const { state: { showUserInfoCard, showSubscriptionModal, isRemind, showRemindModal }, props: { userInfo, enterpriseInfo, getPopupContainer } } = this;
        const style = AkContext.getBranch() === AkContext.YeeOffice ? { height: "56px" } : null;
        return <div className="ak-master-user"
            style={{ float: "right" }}
        >
            <AkPopover
                // visible={showUserInfoCard}
                overlayClassName="ak-yeeflow-userinfo-box" trigger="click"
                getPopupContainer={() =>
                    getPopupContainer ? getPopupContainer() : document.getElementsByClassName("ak-master-user")[0] as HTMLElement
                }
                placement="bottomRight"
                title={this.renderContent()}
                content={this.renderFooter()}>
                <AkRow
                    className="ak-master-user-detail"
                    style={style}
                // onClick={() => {
                //     if (showUserInfoCard) {
                //         this.setState({ showUserInfoCard: false, showChangeLanguageCard: false });
                //     } else {
                //         this.setState({ showUserInfoCard: true });
                //     }
                // }}
                >
                    {/* <AkIcon type="user-line" /> */}
                    <AkImg className="ak-master-user-img" magnifier={false}
                        src={userInfo && URLHelper.GetFileUrl(userInfo.Photo) || AkContext.getUserDefaultPhoto()} />
                </AkRow>
                {this.renderChangePwdCard()}
                {this.renderLanguageCard()}
                {/* {AkContext.getUser().IsAdmin && AkContext.isYeeFlow() ? <YeeflowSubscriptionModal
                    onCancel={() => { this.setState({ showSubscriptionModal: false }) }}
                    showRemindModal={() => { this.setState({ showRemindModal: true }) }}
                    isRemind={isRemind}
                    showModal={showSubscriptionModal} /> : null} */}
            </AkPopover>
        </div >;
    }
}