import * as React from "react";
import { AkLayout, AkRow, AkCol, UserInfoHeader } from "..";
import { MenuData } from "../..";
import { AkIcon, AkBadge } from "../controls";
import { AkGlobal, RouterProps, AkLink, AkContext } from "../../util";
import { withRouter } from "react-router";

export interface YeeFlowMobileFrameProps extends RouterProps {
    // Navigator?: React.ReactNode;//直接传入sider内容
    NavigatorConfig?: MenuData[];//根据传入数组渲染sider菜单
}
export interface YeeFlowMobileFrameStates {
    showChildMenu?: boolean;
    ClickMenuItem?: MenuData;
    selectKeys?: string[];
}

@withRouter
export class YeeFlowMobileFrame extends React.Component<YeeFlowMobileFrameProps, YeeFlowMobileFrameStates>{
    constructor(props, context) {
        super(props, context)
        this.state = {
            showChildMenu: false,
            ClickMenuItem: null,
            selectKeys: []
        }
    }

    /** 寻找当前选中节点 */
    findSelected(path: string, menuData: MenuData[], filterData: MenuData[] = []): MenuData[] {
        for (let i = 0; i < menuData.length; i++) {
            if (menuData[i].Path === path) {
                filterData.push(menuData[i]);
                if (!menuData[i].Children) return filterData;
            }

            if (menuData[i].Children) {
                let menu = this.findSelected(path, menuData[i].Children, filterData);
                if (menu && menu.length > 0) {
                    filterData.push(menuData[i]);
                }
            }
        }
        return filterData
    }

    renderMenu() {
        const { props: { NavigatorConfig }, state: { showChildMenu } } = this;
        let selectedKeys = this.findSelected(this.props.location.pathname, NavigatorConfig).map(i => i.Key);
        return NavigatorConfig.map(item => {
            const isSelected = selectedKeys.findIndex(i => i === item.Key) > -1;
            let badgeCount = item.Badge;
            if (item.Children) {
                badgeCount = item.Children.map(i => i.Badge || 0).reduce((a, b) => a + b);
            }
            return <div
                key={item.Key}
                onClick={() => {
                    if (item.Path) {
                        this.props.router.push({ pathname: item.Path });
                        this.setState({ showChildMenu: false });
                    }
                    if (item.Children) {
                        this.setState({ selectKeys: selectedKeys, ClickMenuItem: item, showChildMenu: !showChildMenu });
                    }
                }}
                className={isSelected ? "mobile-menu-item mobile-menu-item-selected" : "mobile-menu-item"} >
                <div className="mobile-menu-item-content">
                    <AkBadge count={badgeCount} />
                    <AkIcon type={item.Icon} />
                </div>
                <div>{AkGlobal.intl.formatMessage({ id: item.Title })}</div>
            </div >
        })
    }

    renderChildMenu() {
        const { state: { showChildMenu, ClickMenuItem, selectKeys } } = this;
        return showChildMenu && <div className="mobile-child-menu">
            <UserInfoHeader />
            <div className="mobile-child-menu-box">
                {ClickMenuItem.Children.map(item => {
                    const isSelected = selectKeys.findIndex(i => i === item.Key) > -1;
                    if (item.IsDivider) {
                        return <div className="mobile-child-menu-line" key={item.Key}></div>;
                    }
                    if (item.IsTitle) {
                        return <div
                            key={item.Key}
                            className="mobile-child-menu-title">
                            <span className="mobile-child-menu-title-text"> {AkGlobal.intl.formatMessage({ id: item.Title })}</span>
                        </div>
                    }
                    return <div
                        key={item.Key}
                        onClick={(e) => {
                            e.stopPropagation();
                            if (item.Path) {
                                this.setState({ showChildMenu: false });
                                this.props.router.push({ pathname: item.Path });
                            }
                            if (item.OnClick) {
                                item.OnClick();
                            }
                        }}
                        className={isSelected ? "mobile-child-menu-item mobile-child-menu-item-selected" : "mobile-child-menu-item"}>
                        <AkIcon type={item.Icon} />
                        <span className="mobile-child-menu-item-text"> {AkGlobal.intl.formatMessage({ id: item.Title })}</span>
                        <AkBadge count={item.Badge} />
                    </div>
                })}
            </div>
        </div>
    }

    render() {
        const { state: { showChildMenu } } = this;
        return <AkLayout className="ak-mobile-layout-frame" >
            {this.props.children}
            <div className="mobile-menu">
                {this.renderMenu()}
            </div>
            {this.renderChildMenu()}
            {showChildMenu && <div className="menu-mask" onClick={() => this.setState({ showChildMenu: false })}></div>}
        </AkLayout>
    }
}