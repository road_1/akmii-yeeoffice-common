import * as React from "react";
import { AkIcon, AkRow } from "..";
import { AkContext, URLHelper, NavigationModel } from "../..";
import { connect } from "react-redux";
import { AppKeys, NavigationHelper } from '../../util/common';

export interface YeeFlowSettingsProps {
    settingsInfo?: NavigationModel;
}
export interface YeeFlowSettingsStates { }

const mapStateToProps = (state) => {
    const systemAppList = state.appCenter.systemAppList;
    const settingsAppID = AkContext.getAppInfoID(AppKeys.YeeOfficeSettings);
    const settingsInfo = systemAppList && systemAppList.find(i => i.AppID === settingsAppID);

    return { settingsInfo }
}

@connect(mapStateToProps)
export class YeeFlowSettings extends React.Component<YeeFlowSettingsProps, YeeFlowSettingsStates>{
    constructor(props, context) {
        super(props, context)
    }
    render() {
        const { props: { settingsInfo } } = this;
        return <AkRow
            className="ak-master-settings"
            onClick={() => {
                if (AkContext.isYeeFlow()) {
                    window.location.href = "/home/Settings";
                } else {
                    window.location.href = NavigationHelper.getNavigationUrl(AppKeys.YeeOfficeSettings, settingsInfo.YunGalaxyNavigationUrl, "");
                    //window.location.href = URLHelper.GetDomainUrl(settingsInfo.AppID, settingsInfo.YunGalaxyNavigationUrl);
                }
            }}
        >
            <AkIcon type="banshou-line" />
        </AkRow>
    }
}