import * as React from "react";
import { Component } from "react";
import { AkModal, AkGlobal, ContentListApi } from "../..";
import { MasterPageLocale, CommonLocale } from "../../locales/localeid";
import { AkForm, AkFormComponentProps, AkInput, AkInputNumber, AkButton, AkNotification } from "../controls";
import { AkContext, AppKeys } from "../../util";
import superagent = require("superagent");

export interface YeeflowSubscriptionModalStates{
    formName?:string,
    formWorkEmail?:string,
    formJobTitle?:string,
    formPhone?:number,
    formMany?:number,

    showCloseModal?:boolean
}

export interface YeeflowSubscriptionModalProps extends AkFormComponentProps{
    showModal?:boolean;
    onCancel?:()=>void;
    showRemindModal?: ()=>void;
    isRemind?: boolean;

}

@AkForm.create()
export class YeeflowSubscriptionModal extends Component<YeeflowSubscriptionModalProps,YeeflowSubscriptionModalStates>{
    salesEmail?:string;
    saleasAppID?:number;
    constructor(props, context){
        super(props,context);

        this.state = {
            formName:"",
            formWorkEmail:"",
            formJobTitle:"",
            formPhone:null,
            formMany:null,

            showCloseModal:false
        }

        this.salesEmail = "sales@yeeflow.com";

        this.saleasAppID = 41;
    }

    //关闭或提交信息后重置输入框
    resetAddMessages(){
        this.props.form.resetFields();
        this.setState({
            formName:"",
            formWorkEmail:"",
            formJobTitle:"",
            formPhone:null,
            formMany:null,
        });
    }

    //提交表单关闭事件
    addClose = () =>{
        const {props: {onCancel}} = this;
        this.resetAddMessages();
        if (onCancel) {
            onCancel();
        }
    }

    modalCancel = () => {
        const {props: {showRemindModal, onCancel}, state: {showCloseModal}} = this;
        this.resetAddMessages();
        if (onCancel) {
            onCancel();
        }
        if (!showCloseModal && showRemindModal) {
            showRemindModal()
        }
    }

    //发送联系销售人员请求
    async sendAddRequest(dic){
        return new Promise((resolve, reject)=>{
           
            const requestFiles = superagent.post(AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeSettings) + "/api/crafts/datas/title/add");

            requestFiles.set("AkmiiSecret",dic.Token);
            requestFiles.send(dic);
            requestFiles.end((err, res)=>{
                if (res.ok && res.body.Status === 0) {
                    resolve(res.body);
                } else {
                    AkNotification.error({
                        message: AkGlobal.intl.formatMessage({ id: CommonLocale.Tip }),
                        description: AkGlobal.intl.formatMessage({ id: CommonLocale.ResponseError })
                    });
                    reject(Response);
                }
            });
        });
    }

    submitForm = () =>{
        const { state: {formName,formPhone,formJobTitle,formMany,formWorkEmail} } = this;
        let companyN = AkContext.getCompanyInfo().CompanyName;
        let subscriptionID = AkContext.getCompanyInfo().TenantID;
        const topThis = this;
        let dic = {
            Title:"Contact Sales",
            AppID:this.saleasAppID,
            Token:process.env.ContactSalesToken,
            Dic:{
                Name: formName,
                WorkEmail: formWorkEmail,
                JobTitle: formJobTitle,
                PhoneNumber: formPhone,
                CompanyName: companyN,
                UserNumber: formMany,
                SubscriptionID:subscriptionID
            }
        };
        this.props.form.validateFields(
            (err) => {
              if (!err) {
                topThis.sendAddRequest(dic).then((res:any)=>{
                    if (res.Status === 0) {
                        if(topThis.props.onCancel){
                            topThis.addClose();
                            topThis.setState({showCloseModal:true});
                        }
                    }else{
                        AkNotification.error({
                            message: AkGlobal.intl.formatMessage({ id: CommonLocale.Tip }),
                            description: AkGlobal.intl.formatMessage({ id: CommonLocale.ResponseError })
                        });
                    };
                });
              }
            },
          );
    }

    //关闭模态框的关闭事件
    closeModalBtn = () =>{
        const {props: {isRemind, showRemindModal}} = this;
        if(isRemind) {
            showRemindModal && showRemindModal();
            this.setState({showCloseModal:false});
        } else {
            this.setState({showCloseModal:false});
        }
    }

    renderCloseModal(){
        const { formatMessage } = AkGlobal.intl;
        return(
            <AkModal 
            className="yeeflow-subscription-close-modal"
            footer={null}
            visible={this.state.showCloseModal}
            onCancel={this.closeModalBtn}>
            <div className="close-modal">
                <div className="rocketicon">
                   <i className="anticon anticon-success-green big-icon"></i>
                </div>
                <h3 className="h3-text">{formatMessage({id:MasterPageLocale.SuSubscriptionCloseModalH3})}</h3>
                <p className="formtext">{formatMessage({id:MasterPageLocale.SuSubscriptionCloseModalThanksTip})}</p>
                <button onClick={this.closeModalBtn} className="close-modal-btn">{formatMessage({id:MasterPageLocale.SuSubscriptionCloseModalBtn})}</button>
                <p className="greytext">
                {formatMessage({id:MasterPageLocale.SuSubscriptionCloseModalSalesTip})}
                    <a href={"mailto:"+this.salesEmail+""}> {this.salesEmail}</a>
                </p>
            </div>
            </AkModal>
        );
    }

    render(){
        const { formatMessage } = AkGlobal.intl;
        const { getFieldDecorator } = this.props.form;
        const topThis = this;
        return(
            <div>
            <AkModal
            className="yeeflow-subscription-modal-green"
            footer={null} 
            title={formatMessage({ id: MasterPageLocale.SubscriptionModalTitle })}
            onCancel={this.modalCancel} 
            visible={this.props.showModal}>
            <AkForm>
                <AkForm.Item key="Name" label={formatMessage({id:MasterPageLocale.SubscriptionModalName})}>
                   {getFieldDecorator("Name",{
                       rules:[{required:true,message:formatMessage({id:MasterPageLocale.SubscriptionModalRequireTip})}]
                   })(
                       <AkInput value={this.state.formName} onChange={(value)=>{topThis.setState({formName:value})}}/>
                   )}
                </AkForm.Item>

                <AkForm.Item key="workEmail" label={formatMessage({id:MasterPageLocale.SubscriptionModalWorkEmail})}>
                {getFieldDecorator("workEmail",{
                       rules:[{required:true,message:formatMessage({id:MasterPageLocale.SubscriptionModalRequireTip})},
                              {type:"email",message:formatMessage({id:MasterPageLocale.SubscriptionModalEmailTip})}]
                   })(
                       <AkInput value={this.state.formWorkEmail} onChange={(value)=>{topThis.setState({formWorkEmail:value})}}/>
                   )}
                </AkForm.Item>

                <AkForm.Item key="jobTitle" label={formatMessage({id:MasterPageLocale.SuSubscriptionModalJobTitle})}>
                {getFieldDecorator("jobTitle",{
                       rules:[{required:true,message:formatMessage({id:MasterPageLocale.SubscriptionModalRequireTip})}]
                   })(
                       <AkInput value={this.state.formJobTitle} onChange={(value)=>{topThis.setState({formJobTitle:value})}}/>
                   )}
                </AkForm.Item>

                <AkForm.Item key="phone" label={formatMessage({id:MasterPageLocale.SuSubscriptionModalPhoneNumber})}>
                {getFieldDecorator("phone",{
                       rules:[{required:true,message:formatMessage({id:MasterPageLocale.SubscriptionModalRequireTip})}]
                   })(
                       <AkInputNumber value={this.state.formPhone} onChange={(value:number)=>{topThis.setState({formPhone:value})}}/>
                   )}
                </AkForm.Item>

                <AkForm.Item key="many" label={formatMessage({id:MasterPageLocale.SuSubscriptionModalHowManyUsers})}>
                {getFieldDecorator("many",{
                       rules:[{required:true,message:formatMessage({id:MasterPageLocale.SubscriptionModalRequireTip})}]
                   })(
                       <AkInputNumber value={this.state.formMany} onChange={(value:number)=>{topThis.setState({formMany:value})}}/>
                   )}
                </AkForm.Item>

                <AkForm.Item>
                    <div className="terms-service-and-privacy-policy">
                    <span>{formatMessage({id:MasterPageLocale.SuSubscriptionModalTermsService1})}</span>
                    <span onClick={()=>{window.open(process.env.EntranceSite+"agreement/termsService.html")}}><a className="terms-service">{formatMessage({id:MasterPageLocale.SuSubscriptionModalTermsService2})}</a></span>
                    <span>{formatMessage({id:MasterPageLocale.SuSubscriptionModalTermsService3})}</span>
                    <span onClick={()=>{window.open(process.env.EntranceSite+"agreement/privacyPolicy.html")}}><a className="terms-service">{formatMessage({id:MasterPageLocale.SuSubscriptionModalTermsService4})}</a></span>
                    </div>
                </AkForm.Item>

                <AkForm.Item>
                    <div style={{"textAlign":"center"}}>
                    <button onClick={this.submitForm} className="submit-btn">{formatMessage({id:MasterPageLocale.SuSubscriptionModalSubmit})}</button>
                    </div>
                </AkForm.Item>

                <AkForm.Item>
                    <div className="company-message">
                    <p>
                        <span>{formatMessage({id:MasterPageLocale.SuSubscriptionModalCompanyName})}</span>
                        <span>{AkContext.getCompanyInfo().CompanyName}</span>
                    </p>
                    <p>
                        <span>{formatMessage({id:MasterPageLocale.SuSubscriptionModalSubscriptionID})}</span>
                        <span>{AkContext.getCompanyInfo().TenantID}</span>
                    </p>
                    </div>
                </AkForm.Item>
            </AkForm>
            </AkModal>
            {this.renderCloseModal()}
            </div>
        )
    }
}