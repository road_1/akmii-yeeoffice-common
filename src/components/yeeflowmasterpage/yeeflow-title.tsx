import * as React from "react";
import { connect } from "react-redux";
import { NavigationHelper, AkUtil, URLHelper, AkContext } from "../../util";
import { AppCenterListResponse, AkPermissionControl, MerchantLevelEnum, MerchatLoginModel } from "../..";

export interface YeeFlowTitleProps {
    loginData?: MerchatLoginModel;
    selectApp?: AppCenterListResponse;
    isHomePage?: boolean;
}
export interface YeeFlowTitleStates { }

const mapStateToProps = (state) => {
    //sp登陆.net环境地址转换
    let selectApp = AkUtil.deepClone(state.appCenter.selectApp);
    if (selectApp) {
        selectApp.WelcomePage = NavigationHelper.parseURL(selectApp.WelcomePage);
    }
    return {
        selectApp: selectApp
    }
}

@connect(mapStateToProps)
export class YeeFlowTitle extends React.Component<YeeFlowTitleProps, YeeFlowTitleStates>{
    constructor(props, context) {
        super(props, context)
    }

        
    getLogo(logo: string) {
        if (logo) {
            return logo;
        } else {
            if (AkContext.isNeteaseVersion()) {
                return "images/logo-psm-w.png"
            } else {
                return "images/logo-psm.png"
            }
        }
    }

    render() {
        const companyInfoAttr = AkContext.getCompanyInfo().Attr;
        const { props: { selectApp, loginData, isHomePage } } = this;
        let isMobile = window.innerWidth < 768;
        let netHomePage = "";
        //判断是否有homepage
        if (companyInfoAttr && JSON.parse(companyInfoAttr).HomePage) {
            netHomePage = JSON.parse(companyInfoAttr).HomePage;
        }
        return selectApp ? <div className={"ak-master-merchat-title"}
            style={{ color: loginData && loginData.CompanyInfo.NavFontColour }}
            onClick={() => {
                window.location.href = URLHelper.getNewAppJumpUrl(selectApp.WelcomePage, selectApp.RelativeUrl)
            }}>
            {isMobile?null:<img className="ak-master-logo" style={{paddingRight:'0px'}} src={URLHelper.GetFileUrl(this.getLogo(loginData.CompanyInfo.Logo))} />}
            {selectApp.Title}
            {isMobile?null:<img className="ak-master-logo" style={{paddingLeft:'10px',maxWidth:'99px'}} src={selectApp?selectApp.IconUrl:loginData && URLHelper.GetFileUrl(this.getLogo(loginData.CompanyInfo.Logo))} />}
        </div>
            : <div className={"ak-master-merchat-title" + (isHomePage ? " ak-master-home-title" : "")}
                style={{ color: isHomePage ? "#000" : loginData && loginData.CompanyInfo.NavFontColour }}
                onClick={() => {
                    if (isHomePage) return;
                    if (AkContext.getBranch() !== AkContext.YeeOffice) {
                        window.location.href = AkContext.getRootWebUrl() +
                            (AkPermissionControl.hasPermission(MerchantLevelEnum.Standard) ? "" : "/Portal/SitePages/Pages/Index.aspx#/")
                    } else {
                        window.location.href = netHomePage ? netHomePage : AkPermissionControl.hasPermission(MerchantLevelEnum.Standard) ? "/" : "/Home/portal"
                    }
                }}>
                    {isMobile?null:<img className="ak-master-logo" src={`${window["CDN"]}Images/logo-welcomeapps.png`} />}
                    {loginData && loginData.CompanyInfo.CompanyName}
            </div>
    }
}