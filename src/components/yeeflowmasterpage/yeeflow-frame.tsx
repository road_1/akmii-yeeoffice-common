import * as React from "react";
import { AkLayout, YeeFlowSider, AkIcon, } from "..";
import { AkLink, AkGlobal, AkContext } from "../../util";
import { connect } from "react-redux";
import { MenuData, MasterPageAction } from "../..";
import { YeeFlowMobileFrame } from "./yeeflow-mobile-frame";

export interface YeeFlowFrameProps {
    showSider?: boolean; //redux控制显示sider
    title?: string;//标题
    Navigator?: React.ReactNode;//直接传入sider内容
    NavigatorConfig?: MenuData[];//根据传入数组渲染sider菜单
    withBack?: boolean;//显示返回
    backUrl?: string;//返回的地址
    className?: string;//最外层layout的className
    siderClassName?: string;
}
export interface YeeFlowFrameStates { }

const mapStateToProps = (state) => {
    return {
        showSider: state.masterpage.showSider
    };
}

let isMobile = window.innerWidth < 768;
@connect(mapStateToProps)
export class YeeFlowFrame extends React.Component<YeeFlowFrameProps, YeeFlowFrameStates>{
    constructor(props, context) {
        super(props, context)
        if (isMobile) {
            AkGlobal.store.dispatch(MasterPageAction.showSider(false))
        }
    }

    renderSiderSwitch() {
        return <div className="yeeflow-switch" onClick={() => {
            AkGlobal.store.dispatch(MasterPageAction.showSider(!this.props.showSider))
        }}>
            <AkIcon type={this.props.showSider ? "minus" : "plus"} />
        </div>
    }

    render() {
        const { props: { showSider, Navigator, NavigatorConfig, title, withBack, backUrl, className, siderClassName } } = this;
        const isMobileLogin = AkContext.isMobileLoginIn();
        let minheight = isMobileLogin ? "calc(100vh - 0px)" : `calc(100vh - 56px)`;
        if (isMobileLogin && NavigatorConfig) {
            minheight = "calc(100vh - 51px)";
        }
        return isMobileLogin && NavigatorConfig ?
            <YeeFlowMobileFrame
                NavigatorConfig={NavigatorConfig}
            >
                {this.props.children}
            </YeeFlowMobileFrame>
            : <AkLayout className={className ? "ak-layout-frame " + className : "ak-layout-frame"} style={{ minHeight: minheight }}>
                {(Navigator || NavigatorConfig) ?
                    <AkLayout.Sider
                        style={{ height: minheight }}
                        width={220}
                        collapsed={!showSider}
                        collapsedWidth={0}
                        className={siderClassName}>
                        <AkLayout className="ak-transparent-header">
                            {(title || withBack) &&
                                <AkLayout.Header className="ak-header-title ak-transparent-background">
                                    {withBack && <AkLink to={backUrl} style={{ color: "#666" }}>
                                        <AkIcon type="leftarrow" className="ak-header-back"></AkIcon>
                                    </AkLink>}
                                    {title}
                                </AkLayout.Header>
                            }
                        </AkLayout>
                        <AkLayout.Content style={(title || withBack) ? FrameContentStyle.isTitle : FrameContentStyle.noTitle}>
                            {Navigator || <YeeFlowSider NavigatorConfig={NavigatorConfig} />}
                            {!AkContext.isNewVersion() && !AkContext.isYeeFlow() && this.renderSiderSwitch()}
                        </AkLayout.Content>
                    </AkLayout.Sider> :
                    null
                }
                {this.props.children}
            </AkLayout>;
    }
}

export const FrameContentStyle = {
    isTitle: { paddingTop: "0px" },
    noTitle: { paddingTop: "0px" }
}

