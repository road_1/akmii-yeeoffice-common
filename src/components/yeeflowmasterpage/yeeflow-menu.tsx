import * as React from "react";
import { AkCol, AkIcon } from "..";
import { connect } from "react-redux";
import { MasterPageAction } from "../../actions";
import { AkGlobal } from "../../util";

export interface YeeFlowMenuProps {
    showSiderIcon?: boolean;
    showSider?: boolean;
}
export interface YeeFlowMenuStates { }

const mapStateToProps = (state) => {
    return {
        showSiderIcon: state.masterpage.showSiderIcon,
        showSider: state.masterpage.showSider
    };
}

@connect(mapStateToProps)
export class YeeFlowMenu extends React.Component<YeeFlowMenuProps, YeeFlowMenuStates>{
    constructor(props, context) {
        super(props, context)
        this.state = {}
    }

    onIconClick() {
        AkGlobal.store.dispatch(MasterPageAction.showSider(!this.props.showSider))
    }

    render() {
        const { props: { showSiderIcon } } = this;
        return showSiderIcon &&
            <div className="yeeflow-menu"
                onClick={() => this.onIconClick()}
            >
                <AkIcon type="caidan-line" />
            </div>
    }
}