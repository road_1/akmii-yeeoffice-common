import * as React from "react";
import { AkRow, AkIcon } from "..";
import { AkContext, AppKeys } from "../../util/common";
interface YeeFlowSettingsBackProps {}

interface YeeFlowSettingsBackStates {}
export class YeeFlowSettingsBack extends React.Component<YeeFlowSettingsBackProps, YeeFlowSettingsBackStates>{
    constructor(props, context) {
        super(props, context)
    }
    render() {
        const isSettings = AkContext.getAppKey() === AppKeys.YeeOfficeSettings;
        const isYeeFlow = AkContext.isYeeFlow();
        return isSettings && isYeeFlow && <AkRow
            className="yeeflow-settings-back"
            onClick={() => {
                window.location.href = "/home/Index";
            }}
        >
            <AkIcon type="arrow-left" />
        </AkRow>
    }
}