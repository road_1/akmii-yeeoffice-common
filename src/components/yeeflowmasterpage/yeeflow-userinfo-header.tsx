import * as React from "react";
import { AkContext, AkImg, URLHelper, NavigationHelper, AppKeys, NavigationModel } from "../..";
import { connect } from "react-redux";

export interface UserInfoHeaderProps {
    settingsInfo?: NavigationModel;
}
export interface UserInfoHeaderStates { }

const mapStateToProps = (state) => {
    const systemAppList = state.appCenter.systemAppList;
    const settingsAppID = AkContext.getAppInfoID(AppKeys.YeeOfficeSettings);
    const settingsInfo = systemAppList && systemAppList.find(i => i.AppID === settingsAppID);

    return { settingsInfo }
}

@connect(mapStateToProps)
export class UserInfoHeader extends React.Component<UserInfoHeaderProps, UserInfoHeaderStates> {
    constructor(props, context) {
        super(props, context)
    }

    render() {
        const loginData = AkContext.getMerchantData();
        const userInfo = loginData.UserModel;
        if (!userInfo) return;
        return <div className="ak-yeeflow-userinfo-top-box" style={{ cursor: "pointer" }} onClick={() => {
            if (AkContext.isYeeFlow()) {
                window.location.href = "/Home/Settings#/userinfo";
            } else {
                window.location.href = NavigationHelper.getNavigationUrl(AppKeys.YeeOfficeSettings, this.props.settingsInfo.YunGalaxyNavigationUrl, "") + "#/userinfo";
            }

        }}>
            <span className="ak-yeeflow-userinfo-img-box">
                <AkImg width={50} height={50} className="ak-yeeflow-userinfo-img" magnifier={false}
                    src={userInfo && URLHelper.GetFileUrl(userInfo.Photo) || AkContext.getUserDefaultPhoto()} />
            </span>
            <h4 className="ak-yeeflow-userinfo-name" title={userInfo && userInfo.Name_CN || userInfo.Name_EN} >{userInfo && userInfo.Name_CN || userInfo.Name_EN}</h4>
            <p className="ak-yeeflow-userinfo-email" title={userInfo && userInfo.Email}>  {userInfo && userInfo.Email}</p>
        </div>
    }
}