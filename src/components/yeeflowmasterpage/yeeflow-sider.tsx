import * as React from "react";
import { AkMenu, AkBadge, AkRow, AkCol, AkIcon } from "..";
import { MenuData, AkGlobal, AkLink, RouterProps, MasterPageAction } from "../..";
import { withRouter } from "react-router";

export interface YeeFlowSiderProps extends RouterProps {
    NavigatorConfig?: MenuData[];
}

export interface YeeFlowSiderStates { }

let isMobile = window.innerWidth < 768;

@withRouter
export class YeeFlowSider extends React.Component<YeeFlowSiderProps, YeeFlowSiderStates>{
    openKeys: string[];
    constructor(props, context) {
        super(props, context)
        this.openKeys = [];
    }

    renderSubMenu(menu: MenuData) {
        // const { task } = this.props;
        if (menu.NotShow) {
            return;
        }
        if (menu.IsDivider) {
            return <AkMenu.Divider key={menu.Key}></AkMenu.Divider>;
        }
        if (menu.Children) {
            return <AkMenu.SubMenu
                key={menu.Key}
                title={AkGlobal.intl.formatMessage({ id: menu.Title })}>
                {menu.Children.map((item) => {
                    return this.renderSubMenu(item);
                })}
            </AkMenu.SubMenu>;
        }
        if (menu.IsTitle) {
            return <AkMenu.Item key={menu.Key} disabled={true}>
                <AkRow justify="space-between" type="flex" align="middle">
                    <AkCol>
                        <span className="menu_main_title">
                            {AkGlobal.intl.formatMessage({ id: menu.Title })}
                        </span>
                    </AkCol>
                </AkRow>
            </AkMenu.Item>
        }

        let badgeCount = menu.Badge;
        // switch (menu.Key) {
        //     case NavigatorKey.WaittingTask:
        //         badgeCount = task.todoCount;
        //         break;
        //     case NavigatorKey.ReceiveTask:
        //         badgeCount = task.claimCount;
        //         break;
        // }

        let badge = badgeCount > 0 && <AkBadge style={{ background: "#63A8EB" }} count={badgeCount} />;

        return <AkMenu.Item key={menu.Key} className="menu_li" >
            <AkLink to={menu.Path}
                onClick={() => {
                    if (isMobile) {
                        AkGlobal.store.dispatch(MasterPageAction.showSider(false))
                    }
                }}>
                <AkRow justify="space-between" type="flex" align="middle">
                    <AkCol>
                        <AkIcon type={menu.Icon} className="menu_icon"></AkIcon>
                        <span className="menu_title">
                            {AkGlobal.intl.formatMessage({ id: menu.Title })}
                        </span>
                    </AkCol>
                    <AkCol>
                        {badge}
                    </AkCol>
                </AkRow>
            </AkLink>
        </AkMenu.Item>;
    }
    /** 寻找当前选中节点 */
    findSelected(path: string, menuData: MenuData[]): MenuData {
        for (let i = 0; i < menuData.length; i++) {
            if (menuData[i].Path === path) {
                return menuData[i];
            }
            if (menuData[i].Children) {
                let menu = this.findSelected(path, menuData[i].Children);
                if (menu) {
                    this
                        .openKeys
                        .push(menuData[i].Key);
                }
                return menu;
            }
        }
        return {};
    }
    render() {
        let menuConfig = this.props.NavigatorConfig || [];
        let selectMenu = this.findSelected(this.props.location.pathname, menuConfig);
        return <AkMenu
            mode="inline"
            className="ak-yeeflow-sider"
            defaultOpenKeys={this.openKeys}
            onClick={() => {
                return true;
            }}
            selectedKeys={[selectMenu.Key]}
            defaultSelectedKeys={[selectMenu.Key]}>
            {menuConfig.map((item) => {
                return this.renderSubMenu(item);
            })}
        </AkMenu>;
    }
}