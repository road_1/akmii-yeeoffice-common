import * as React from "react";
import { AkConstants, AkGlobal } from "../../util/common";
import { CommonLocale } from "../../locales";

export interface AkNoSiteAppsProps { }

export interface AkNoSiteAppsStates { }

export class AkNoSiteApps extends React.Component<AkNoSiteAppsProps, AkNoSiteAppsStates>{
    constructor(props, context) {
        super(props, context)
    }

    render() {
        return <div className="ak-nosite-apps">
            <div className="empty-img">
                <img src={AkConstants.DefualtHttps + "/common/apps-empty1.png"} />
            </div>
            <p>{AkGlobal.intl.formatMessage({ id: CommonLocale.NoSiteApps })}</p>
        </div>
    }
}
