import * as React from "react";
import { Component, CSSProperties } from "react";
import { FormattedMessage } from "react-intl";
import { AkRow, AkCol } from "../index";
import { AkUtil, CommonLocale } from "../../index";


export interface AkNoDataProps {
    loading?: boolean;
    noDataTip?:string;//没有内容底部提示的文字
    noDataImgUrl?:string;//没有内容显示的图片
    titleStyle?:CSSProperties;//提示的文字样式
}
export interface AkNoDataStates {
}
export class AkNoData extends Component<AkNoDataProps, AkNoDataStates> {
    render() {
        const{noDataTip,noDataImgUrl,titleStyle}=this.props;
        let tip=noDataTip?noDataTip:<FormattedMessage id={CommonLocale.NoContent}></FormattedMessage>;
        return this.props.loading ? <div style={{ height: 320 }}></div> : <AkRow className="ak-nodata">
            {
                <AkRow className="content">
                        <div><img src={noDataImgUrl ? noDataImgUrl : AkUtil.noDataIcon} /></div>
                        <div style={titleStyle ? titleStyle : null}>{tip}</div>
                    </AkRow>
            }
        </AkRow>
    }
}