import * as React from "react";
import { CommonLocale } from "../../locales";
import { AkIcon } from "..";
import { AkGlobal } from "../..";

export interface NoPermissionProps { }
export interface NoPermissionStates { }

export class NoPermission extends React.Component<NoPermissionProps, NoPermissionStates>{
    constructor(props, context) {
        super(props, context)
    }

    render() {
        return <div className="nopermission-page">
            <div className="page-middlebox">
                <h1><AkIcon type="quanxian" /></h1>
                <h3>{AkGlobal.intl.formatMessage({ id: CommonLocale.NoPermission })}</h3>
                <p>{AkGlobal.intl.formatMessage({ id: CommonLocale.NoPermissionDes })}</p>
            </div>
        </div>
    }

}