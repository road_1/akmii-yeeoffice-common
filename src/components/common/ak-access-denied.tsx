import * as React from "react";
import { AkIcon } from "..";
import { AkGlobal, CommonLocale } from "../..";

export interface AccessDeniedProps { }
export interface AccessDeniedStates { }

export class AccessDenied extends React.Component<AccessDeniedProps, AccessDeniedStates>{
    constructor(props, context) {
        super(props, context)
    }

    render() {
        return <div className="nopermission-page">
        <div className="page-middlebox">
            <h1><AkIcon type="quanxian" /></h1>
            <h3>{AkGlobal.intl.formatMessage({ id: CommonLocale.AccessDenied })}</h3>
            <p>{AkGlobal.intl.formatMessage({ id: CommonLocale.AccessDeniedDes })}</p>
        </div>
    </div>
    }
}