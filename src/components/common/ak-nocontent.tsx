import * as React from "react";
import { Component } from "react";
import { FormattedMessage } from "react-intl";
import { AkRow, AkCol } from "../index";
import { AkUtil, CommonLocale } from "../../index";


export interface AkNoContentProps {
    loading?: boolean;
}
export interface AkNoContentStates {
}
export class AkNoContent extends Component<AkNoContentProps, AkNoContentStates> {
    render() {
        return this.props.loading ? <div style={{ height: 320 }}></div> : <AkRow type="flex" justify="center">
            {
                this.props.children ?
                    <AkCol>
                        {this.props.children}
                    </AkCol> :
                    <AkCol>
                        <div><img src={AkUtil.noContentIcon} /></div>
                        <div><FormattedMessage id={CommonLocale.NoContent}></FormattedMessage></div>
                    </AkCol>
            }
        </AkRow>
    }
}