import {MerchantLevelEnum} from '../../api/masterpage/masterpagemodel';
import * as React from 'react';
import {AkContext, AppKeys} from '../../util/common';
import {ReactNode} from 'react';
export interface PermissionControlProps {
    merchantLevel : MerchantLevelEnum;
    appKey?: string;
}

export interface PermissionControlStatus {}
export class AkPermissionControl extends React.Component < PermissionControlProps,
PermissionControlStatus > {
    level : MerchantLevelEnum = AkContext.getAppLevel(this.props.appKey);
    //兼容AkMenu.Item 方法。控制AkMenu.Item调用这个方法
    static hasPermission = (merchantLevel : MerchantLevelEnum, appKey?: string) => {
        let level = AkContext.getAppLevel(appKey);
        return level >= merchantLevel;
    }
    constructor(props, context) {
        super(props, context);
    }

    //非AkMenu.Item直接render
    render() {
        let node = this.props.children as JSX.Element;
        if (this.level >= this.props.merchantLevel) {
            return node;
        }
        return null;
    }
}