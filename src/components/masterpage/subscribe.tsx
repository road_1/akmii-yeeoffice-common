import * as React from "react";
import { Component } from "react";
import { FormattedMessage } from "react-intl";
import { AkPopover, AkRow, AkCol, AkButton, AkModal, AkMessage } from "../index";
import { MasterPageLocale } from "../../locales/localeid";
import { UserModel, EnterpriseInfo } from '../../api/masterpage/masterpagemodel';
import { URLHelper } from "../../util/URLHelper";
import { AkMenu } from '../controls/ak-menu';
import { AkSwitch } from '../controls/ak-switch';
import { AkIcon } from '../controls/ak-icon';
import { AkRadio } from '../controls/ak-radio';
import { Cookies } from '../../util/cookie';
import { AkContext, EnterpriseFlag, MerchantLevelEnum, SupportModel, CommentAPI } from "../../index";
import { AkBadge } from '../controls/ak-badge';
import { AkGlobal } from '../../util/common';
import { AkAlert } from "../controls/ak-alert";
import { CacheHelper } from "../../util/cache";



export interface FreeTrialProps {
    enterpriseInfo?: EnterpriseInfo;
    onChangeLanguage?: (language: string) => void;
    tenantID?: string;
    color?: string;
}
export interface FreeTrialStates {
    language?: string;
    supportInfo: SupportModel;
}
export class Subscribe extends Component<FreeTrialProps, FreeTrialStates> {

    constructor(props, context) {
        super(props, context);
        this.state = {
            language: AkContext.getLanguage(),
            supportInfo: { Mail: "business@akmii.com", Telephone: "400-1680-365", WorkHours: "9:00-18:00", InviteCode: "" }
        };
    }
    componentDidMount() {
        const { InviteCode } = this.props.enterpriseInfo;
        if (InviteCode) {
            var data = CacheHelper.getLocalStorage(AkContext.Keys.SupportInfo)
            if (data && data.InviteCode == InviteCode) {
                this.setState({ supportInfo: data })
            } else {
                CommentAPI.getSupportInfobyCode(InviteCode).then((data) => {
                    if (data.Status == 0 && data.Data) {
                        var supportData = JSON.parse(data.Data);
                        supportData["InviteCode"] = InviteCode;
                        this.setState({ supportInfo: supportData });
                        CacheHelper.setLocalStorage(AkContext.Keys.SupportInfo, supportData)
                    }
                });
            }
        }
    }
    renderFooter() {
        return <AkRow style={{ maxWidth: "300px" }} type="flex" justify="space-between">
            <AkCol style={{width:"100%"}}>
                {this.renderEnterpriseInfoFooter()}
            </AkCol>
        </AkRow>;
    }

    renderEnterpriseInfoFooter() {
        let desc = AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeUpgrade },
            {phone:this.state.supportInfo.Telephone,
            email:this.state.supportInfo.Mail,
            worktime:this.state.supportInfo.WorkHours});
        // desc = desc.replace("{0}", this.state.supportInfo.Telephone);
        // desc = desc.replace("{1}", this.state.supportInfo.Mail);
        // desc = desc.replace("{2}", this.state.supportInfo.WorkHours);
        return <AkAlert
            message={AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeBuy })}
            description={desc}
            type="info"
            showIcon
        ></AkAlert>
    }

    renderEnterpriseInfo() {
        const { enterpriseInfo, tenantID } = this.props;
        let text = "";

        if (enterpriseInfo.Flag == EnterpriseFlag.Free) {
            text = AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeLevelNotBuy })
        } else {
            text = AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeLevelBuy })
        }
        switch (enterpriseInfo.Level) {
            case MerchantLevelEnum.Free:
                text += " : " + AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeLiteTitle });
                break;
            case MerchantLevelEnum.Enterprise:
                text += " : " + AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeEnterpriseTitle });
                break;
            case MerchantLevelEnum.Standard:
                text += " : " + AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeStandardTitle });
                break;
            case MerchantLevelEnum.Ultimate:
                text += " : " + AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeEnterpriseTitle });
                break;
            default:
                return null;

        }
        return <AkRow type="flex" justify="start" align="middle">
            <AkCol span={24}>
                <AkCol>
                    <AkBadge count={0} status="error" text={text} />
                </AkCol>
                <AkCol>
                    <AkBadge count={0} status="error" text={AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeCreated }) + " : " + enterpriseInfo.Created.substring(0, 10)} />
                </AkCol >
                <AkCol>
                    <AkBadge count={0} status="error" text={AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeExpireDate }) + " : " + enterpriseInfo.ExpireDate.substring(0, 10)} />
                </AkCol>
                <AkCol>
                    <AkBadge count={0} status="error" text={AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeUserMaxCount }) + " : " + enterpriseInfo.UserMaxCount} />
                </AkCol>
                <AkCol>
                    <AkBadge count={0} status="error" text={AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeSubscribeID }) + " : " + tenantID} />
                </AkCol>
            </AkCol>
        </AkRow>
    }

    /**
     * 根据订阅级别显示过期信息
     * @param level 
     */
    renderExpiredInfo() {
        return (<AkModal
            title={AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeEnds })}
            visible={true}
            maskClosable={false}
            footer={
                [
                    <AkButton type="primary" onClick={() => {
                        window.location.href = AkContext.getSignOutURI();
                    }}>{AkGlobal.intl.formatMessage({ id: MasterPageLocale.Exit })}</AkButton>,
                    // <AkButton type="primary" onClick={() => {
                    //     AkMessage.info(AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeUpgrade }));
                    // }}>{AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeBuy })}</AkButton>
                ]
            }
            closable={false}
        >
            {this.renderEnterpriseInfo()}
            {this.renderEnterpriseInfoFooter()
            }</AkModal>)
    }
    renderMerchantLevel() {
        const isMobile=window.innerWidth < 786;
        const { enterpriseInfo, tenantID } = this.props;
        const isAdmin =  AkContext.getUser().IsAdmin;
        if (enterpriseInfo.Flag == EnterpriseFlag.FreeComplete || enterpriseInfo.Flag == EnterpriseFlag.SubscriptionExpired) {
            return this.renderExpiredInfo();
        } else {  
            /**
             * 只有管理员 可以看到订阅信息 或者试用期所有用户可见
             */
            if((isAdmin||enterpriseInfo.Flag== EnterpriseFlag.Free)&&!isMobile)
            {
                switch (enterpriseInfo.Level) {
                    case MerchantLevelEnum.Free:
                        return this.renderHtml("Subscribe-L", enterpriseInfo.Flag);
                    case MerchantLevelEnum.Enterprise:
                        return this.renderHtml("Subscribe-E", enterpriseInfo.Flag);
                    case MerchantLevelEnum.Standard:
                        return this.renderHtml("Subscribe-S", enterpriseInfo.Flag);
                    case MerchantLevelEnum.Ultimate:
                        return this.renderHtml("Subscribe-E", enterpriseInfo.Flag);
                    default:
                        return null;
    
                }
            }else{
                return null;
            } 
        }



    }
    renderHtml(icon: string, flag: number) {
        const { enterpriseInfo, color } = this.props;
     
        return <div className="ak-master-freetrial">
            <AkPopover overlayClassName="ak-react-masterpage" trigger="click"
                getPopupContainer={() => document.getElementsByClassName("ak-master-freetrial")[0] as HTMLElement}
                placement="bottomRight" content={this.renderFooter()} title={this.renderEnterpriseInfo()}
            >
                {flag == EnterpriseFlag.Subscribe ? <AkRow className="ak-master-freetrial-detailSubscribe" style={{ height: "56px", background: "none !important" }}>
                    <AkIcon type={icon} ></AkIcon>
                </AkRow> : <AkRow className="ak-master-freetrial-detailFree" style={{ height: "56px" }}>
                        {AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeLevelNotBuy })}
                    </AkRow>} 
            </AkPopover>
        </div>;
    }

    render() {

        return this.renderMerchantLevel();

    }
} class FreeTrialStyle { }