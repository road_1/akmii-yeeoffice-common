import * as React from "react";
import { MasterPageLocale } from "../../locales/localeid";
import { AkContext, AkGlobal, AkUtil } from "../../util";

export interface MasterPageHomeManageProps {
    hideSyStemActions?: boolean;//隐藏masterpage的聊天，系统设置，用户信息按钮
}

export interface MasterPageHomeManageState {

}
export class MasterPageHomeManage extends React.Component<MasterPageHomeManageProps, MasterPageHomeManageState>{
    constructor(props, context) {
        super(props, context);

    }

    getAppManagerUrl() {
        let url = AkContext.getRootWebUrl();
        let manageurl = "#/app-management";
        if (this.props.hideSyStemActions) {
            manageurl = "?hidesystemactions=true#/app-management";
        }
        if (AkContext.getBranch() === AkContext.YeeOffice) {
            url = AkContext.getRootWebUrl() + "/Home/settings" + manageurl;
        } else {
            url = AkContext.getRootWebUrl() + "/Settings/SitePages/Pages/index.aspx" + manageurl;
        }

        return url;
    }

    render() {
        return AkUtil.isHomePage() ? <a key="app-manage" href={this.getAppManagerUrl()}
            className={
                this.props.hideSyStemActions ?
                    "ak-masterpage-header-button ak-masterpage-appmanager ak-masterpage-appmanager-show" :
                    "ak-masterpage-header-button ak-masterpage-appmanager"
            } >
            {AkGlobal.intl.formatMessage({ id: MasterPageLocale.AppManage })}
        </a > : null
    }

}