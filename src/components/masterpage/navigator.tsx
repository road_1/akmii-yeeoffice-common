import * as classNames from "classnames";
import * as React from "react";
import { Component } from "react";
import { FormattedMessage } from "react-intl";
import { AkIdentity } from "../../api/identity/identity";
import { MerchatAPI } from "../../api/masterpage/masterpage";
import { MerchantLevelEnum, MerchatLoginModel, NavigationModel, PortalModel } from '../../api/masterpage/masterpagemodel';
import { AppKeys } from "../../index";
import { AkContext, NavigationHelper, RouterProps, AkGlobal } from '../../util/common';
import { URLHelper } from "../../util/URLHelper";
import { AkUtil } from "../../util/util";
import { AkPermissionControl } from '../common/ak-permission';
import { AkButton } from "../controls/ak-button";
import { AkCol, AkIcon, AkPopover, AkRow, AkTooltip, AkImg } from "../index";
import { connect } from "react-redux";
import { AppCenterListResponse, NavigationResponse } from "../../api/appcenter/appcentermodal";

const DEFAULT_PORTAL_KEY = 37;
const LANGUAGE_PREFIX = "masterpage.language.";
export const MASTERMENU_PREFIX = "masterpage.menu.";
export class PortalViewModel implements PortalModel {
    Admins: AkIdentity[];
    PortalID?: string;
    ApplicationID?: string;
    Title?: string;
    Description?: string;
    WebRelativeUrl?: string;
    ParentID?: string;
    SupperManagerID?: string;
    Configs?: string;
    DraftConfigs?: string;
    Status?: string;
    Ext1?: string;
    Ext2?: string;
    Ext3?: string;
    Image?: string;
    Children?: PortalViewModel[] = [];
    Collapse?: boolean = false;
    portalbaseUrl?: string;
    appID?: number;

    static processSimpleTreeData(treeData) {
        function unflatten2(array: PortalModel[], p?: PortalModel) {
            let parent: PortalViewModel = p;
            if (p === undefined) {
                parent = array.find(i => i.ParentID === "0");
                parent.Image = "images/menhu01.png"; // Hard code Image Url
            } else {
                parent.Image = "images/menhu02.png"; // Hard code Image Url
            }

            let children = array.filter(i => i.ParentID === parent.PortalID);

            if (children.length) {
                parent.Children = children;
                children.forEach(child => unflatten2(array, child));
            }

            return parent;
        }

        return unflatten2(treeData);
    }
}


export interface MasterPageNavigatorProps extends RouterProps {
    appCenterList?: AppCenterListResponse[];
    selectApp?: AppCenterListResponse;
    merchatInfo?: MerchatLoginModel;
    title?: string; //页面的title，默认是YeeOffice
    languages?: string[]; //系统支持的语言，默认['zh', 'en']
    navigationList?: NavigationResponse[];
}
export interface MasterPageNavigatorStates {
    navigator?: NavigationModel[];
    portal?: PortalViewModel;
    thirdLevel?: PortalViewModel[];
    minHeight?: number;
    visible?: boolean;
    selectApp?: AppCenterListResponse;
}

const mapStateToProps = (state) => {
    //sp登陆.net环境地址转换
    let navigationList = state.appCenter.navigationList ? AkUtil.deepClone(state.appCenter.navigationList).filter(item => {
        const HideEntrance = item.AppCenterID < 10000 ? false : item.Settings && JSON.parse(item.Settings).HideEntrance;
        if (HideEntrance) {
            return false;
        } else {
            return true;
        }
    }) : [];
    let appCenterList = state.appCenter.appCenterList ? AkUtil.deepClone(state.appCenter.appCenterList).filter(item => {
        const HideEntrance = item.Settings && JSON.parse(item.Settings).HideEntrance;
        if (HideEntrance) {
            return false;
        } else {
            return true;
        }
    }) : [];
    appCenterList = appCenterList.map(i => {
        i.WelcomePage = NavigationHelper.parseURL(i.WelcomePage);
        return i;
    });
    let selectApp = AkUtil.deepClone(state.appCenter.selectApp);
    if (selectApp) {
        selectApp.WelcomePage = NavigationHelper.parseURL(selectApp.WelcomePage);
    }
    return {
        appCenterList: appCenterList,
        selectApp: selectApp,
        navigationList: navigationList
    }
}

@connect(mapStateToProps)
export class MasterPageNavigator extends Component<MasterPageNavigatorProps, MasterPageNavigatorStates> {
    appID?: number;
    portalbaseUrl?: string;

    constructor(props, context) {
        super(props, context);
        var height = window.innerHeight || document.body.clientHeight || document.documentElement.clientHeight;
        this.state = {
            minHeight: height,
            visible: false,
        };
        window.onresize = () => {
            var height = window.innerHeight || document.body.clientHeight || document.documentElement.clientHeight;
            this.setState({ minHeight: height });
        };
    }

    componentDidMount() {
        Promise.all([
            MerchatAPI.getMechatNavigation({}),
            MerchatAPI.getMechatPortal()
        ]).then(dataArray => {
            const navigator = dataArray[0].Data;
            try {
                if (dataArray[1].Status === 0 && dataArray[1].Data && dataArray[1].Data.length > 0) {
                    var portalApp = AkContext.getAppinfoByAppKey(AppKeys.YeeOfficePortal);
                    const portal = PortalViewModel.processSimpleTreeData(dataArray[1].Data);
                    const portalNav = navigator.find(i => i.AppID === portalApp.AppInfoID);
                    this.appID = portalNav.AppID;
                    this.portalbaseUrl = NavigationHelper.getNavigationUrl(portalApp.AppKey, portalNav.YunGalaxyNavigationUrl);
                    AkUtil.remove(navigator, i => i === portalNav);
                    this.setState({ portal });
                }
            } catch (ex) {
                console.log("no portal navigation data.")
            }
            this.setState({ navigator })
        });
    }
    renderAppCenterMenu() {
        const { props: { navigationList } } = this;
        if (navigationList && navigationList.length > 0) {
            return navigationList.map((entry, index) => {
                let app = AkContext.getAppinfobyLocalStorage().find(a => {
                    return a.AppInfoID === entry.AppCenterID;
                });
                if (app && app.AppKey === "YeeOfficeSettings") {
                    let user = AkContext.getUser();;
                    if (user && !user["IsAdmin"]) {
                        return;
                    }
                }
                if (app && AkContext.getBranch() === AkContext.YeeOffice) {
                    if(entry.AppCenterID<10000) {
                        entry.WelcomePage = NavigationHelper.getNavigationUrl(app.AppKey, entry.WelcomePage)
                    }
                }
                if(entry.AppCenterID === 2) {
                    return this.renderPortalMenu(this.state.portal)
                } else {
                    return < AkCol key={index} className="ak-master-appstore-appcentermenuitem ak-master-appstore-menuitem " >
                    <div onClick={() => {
                        this.setState({ visible: false });
                        window.location.href = entry.AppCenterID<10000
                                                ?URLHelper.GetDomainUrl(entry.AppCenterID, entry.WelcomePage)
                                                :URLHelper.getNewAppJumpUrl(entry.WelcomePage, entry.RelativeUrl)
                    }}>
                        <img src={entry.AppCenterID < 10000 ? window["CDN"] + entry.IconUrl :entry.IconUrl || process.env.CDN + "images/placeholder.png"} />
                        <span>{app ?
                            AkGlobal.intl.formatMessage({ id: MASTERMENU_PREFIX + app.AppKey }) :
                            (entry && entry.Title)
                        }</span>
                    </div>
                </AkCol >
                }
            })
        } else {
            return null
        }
    }

    /**
     * 加载子菜单
     */
    renderSubMenu() {
        const { navigator } = this.state;
        return navigator ? navigator.map((entry, index) => {
            let app = AkContext.getAppinfobyLocalStorage().find(a => {
                return a.AppInfoID === entry.AppID;
            });
            if (app && app.AppKey === "YeeOfficeSettings") {
                let user = AkContext.getUser();;
                if (user && !user["IsAdmin"]) {
                    return;
                }
            }
            if (app && AkContext.getBranch() === AkContext.YeeOffice) {
                entry.YunGalaxyNavigationUrl = NavigationHelper.getNavigationUrl(app.AppKey, entry.YunGalaxyNavigationUrl)
            }

            return app ? <AkCol key={index} className="ak-master-appstore-menuitem">
                <div onClick={() => {
                    this.setState({ visible: false });
                    window.location.href = URLHelper.GetDomainUrl(entry.AppID, entry.YunGalaxyNavigationUrl);
                }}>
                    <img className={"bgcolor-" + app.AppKey} src={window["CDN"] + entry.YunGalaxyNavigationImage} />
                    <FormattedMessage id={MASTERMENU_PREFIX + app.AppKey}></FormattedMessage>
                </div>
            </AkCol > : <AkCol key={index} className="ak-master-appstore-menuitem">
                    <div onClick={() => {
                        this.setState({ visible: false });
                        window.location.href = URLHelper.GetDomainUrl(entry.AppID, entry.YunGalaxyNavigationUrl);
                    }}>
                        <img src={window["CDN"] + entry.YunGalaxyNavigationImage} />
                        {entry && entry.YunGalaxyNavigationName_CN || entry.YunGalaxyNavigationName_EN}
                    </div>
                </AkCol >;
        }) : null;
    }

    renderPortalMenu(portal: PortalViewModel) {
        if (!portal) return;
        const href = URLHelper.GetDomainUrl(this.appID, "#/sites" + portal.WebRelativeUrl);
        return <AkCol className="ak-master-appstore-menuitem" key={portal.PortalID}>
            <div className="text-overflow" style={{ position: "relative", height: 48, paddingRight: 26 }} onClick={() => {
                const mainPortal = this.state.portal;
                mainPortal.Collapse = false;
                if (mainPortal.Children) mainPortal.Children.forEach(i => i.Collapse = false);
                this.setState({ visible: false, portal: mainPortal });
                window.location.href = AkContext.getBranch() === AkContext.YeeOffice ? this.portalbaseUrl + href : href;
            }}>
                <img className="bgcolor-YeeOfficePortal" src={window["CDN"] + portal.Image} />
                {portal.ParentID === "0" ? <FormattedMessage id={MASTERMENU_PREFIX + "YeeOfficePortal"} /> :
                    <AkTooltip placement="topLeft" title={portal.Title}>
                        {portal.Title}
                    </AkTooltip>}
                {portal.Children && portal.Children.length > 0 ? <span className={classNames("subwebbtn", { "active": portal.Collapse })} onClick={e => {
                    e.stopPropagation();
                    if (portal === this.state.portal) {
                        portal.Collapse = !portal.Collapse;
                        this.setState({ portal: this.state.portal });
                    } else {
                        const collapse = this.state.portal.Children.find(i => i.Collapse === true);
                        if (collapse && !portal.Collapse) {
                            collapse.Collapse = false;
                            this.setState({ portal: this.state.portal });
                            setTimeout(() => {
                                portal.Collapse = !portal.Collapse;
                                this.setState({ portal: this.state.portal });
                            }, 500);
                        } else {
                            portal.Collapse = !portal.Collapse;
                            this.setState({ portal: this.state.portal });
                        }
                    }
                }}><AkIcon type="right" /></span> : null}
            </div>
        </AkCol >
    }

    renderSubPortals() {
        const { portal } = this.state;
        if (!portal || !portal.Children) return;
        const secondPortal = portal.Children.find(i => i.Collapse);

        const secondCls = classNames("ak-master-appstore-content", "second", { "active": portal.Collapse });
        const thirdCls = classNames("ak-master-appstore-content", "third", { "active": portal.Collapse && secondPortal });

        return <div>
            <AkRow className={secondCls}>
                <AkButton className="ak-master-sub-nav-back" icon="arrow-left" onClick={() => { portal.Collapse = false; this.setState({ portal: portal }) }} />
                {portal.Children.map(p => this.renderPortalMenu(p))}
            </AkRow>
            <AkRow className={thirdCls}>
                <AkButton className="ak-master-sub-nav-back" icon="arrow-left" onClick={() => { secondPortal.Collapse = false; this.setState({ portal: portal }) }} />
                {secondPortal && secondPortal.Children.map(p => this.renderPortalMenu(p))}
            </AkRow>
        </div>
    }

    renderLeft() {
        const { merchatInfo, languages } = this.props;

        return <div>
            <AkRow className="ak-master-appstore-content" style={{ minHeight: this.state.minHeight-56 }}>
                <AkRow className="ak-master-appstore-menu" style={{ height: this.state.minHeight-56 }} >
                    {/* {this.renderPortalMenu(this.state.portal)} */}
                    {/* {this.renderSubMenu()} */}
                    {this.renderAppCenterMenu()}
                </AkRow>
            </AkRow>
            {this.renderSubPortals()}
        </div>
    }

    render() {
        const { merchatInfo, selectApp } = this.props;
        const companyInfoAttr = AkContext.getCompanyInfo().Attr;
        let homePage = "";
        //判断是否有homepage
        if (companyInfoAttr && JSON.parse(companyInfoAttr).HomePage) {
            homePage = JSON.parse(companyInfoAttr).HomePage;
        }
        return <AkRow justify="space-around" style={{ width: "100%" }} >
            <AkCol className="ak-master-app" style={{ float: "left" }} >
                <AkPopover
                    onVisibleChange={visible => this.setState({ visible })}
                    visible={this.state.visible}
                    getPopupContainer={() => document.getElementsByClassName("ak-master-app")[0] as HTMLElement}
                    trigger={"click"}
                    overlayClassName="ak-master-app-content"
                    placement="topLeft" content={this.renderLeft()} >
                    <div>
                        <AkIcon style={{ color: merchatInfo && merchatInfo.CompanyInfo.NavFontColour }}
                            className="ak-master-appstore" type="app-store"></AkIcon>
                    </div>
                </AkPopover>
            </AkCol>
            <AkCol   >
                {selectApp ? <div className="ak-master-merchat-title"
                    style={{ color: merchatInfo && merchatInfo.CompanyInfo.NavFontColour }}
                    onClick={() => {
                        window.location.href = URLHelper.getNewAppJumpUrl(selectApp.WelcomePage, selectApp.RelativeUrl)
                    }}>
                    {selectApp.Title}
                </div>
                    : <div className="ak-master-merchat-title"
                        style={{ color: merchatInfo && merchatInfo.CompanyInfo.NavFontColour }}
                        onClick={() => {
                            AkContext.getBranch() !== AkContext.YeeOffice
                                ?
                                (
                                    AkPermissionControl.hasPermission(MerchantLevelEnum.Standard)
                                        ?
                                        window.location.href = AkContext.getRootWebUrl()
                                        :
                                        window.location.href = AkContext.getRootWebUrl() + "/Portal/SitePages/Pages/Index.aspx#/"
                                )
                                :
                                (
                                        homePage 
                                        ? 
                                        window.location.href = homePage 
                                        : 
                                        AkPermissionControl.hasPermission(MerchantLevelEnum.Standard)
                                        ?
                                        window.location.href = "/"
                                        :
                                        window.location.href = "/Home/portal"
                                )
                        }}>{
                            merchatInfo && merchatInfo.CompanyInfo.CompanyName
                        }</div>}
            </AkCol>
        </AkRow>
        // }
    }


}
class MasterPageNavigatorStyle { }