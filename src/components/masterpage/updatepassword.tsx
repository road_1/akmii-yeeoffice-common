import * as React from "react";
import { AkGlobal } from '../../util/common';
import { MasterPageLocale, CommonLocale } from '../../locales/localeid';
import { AkCol } from '../controls/ak-col';
import { AkRow } from '../controls/ak-row';
import { AkButton } from '../controls/ak-button';
import { AkInput, AkFormComponentProps, AkForm } from "../index";
import { UpdatePasswordRequest } from '../../api/masterpage/masterpagemodel';
import { FormHelper } from "../../index";
import { MerchatAPI } from "../../api/masterpage/masterpage";
import { AkMessage } from '../controls/ak-message';
import { AkNotification } from '../controls/ak-notification';

export interface UpdatePasswordProps extends AkFormComponentProps {
    onOk?: () => void;
    onCancel?: () => void;
}

export interface UpdatePasswordStates {
    confirmStatus?: boolean,
}

@AkForm.create()
export default class UpdatePassword extends React.Component<UpdatePasswordProps, UpdatePasswordStates>{
    loading?: boolean;
    constructor(props, context) {
        super(props, context)
        this.state = { confirmStatus: false }
    }

    checkPassword(props, name) {
        return {
            validator: (rule, value, callback) => {
                const form = this.props.form;
                if (value && value !== form.getFieldValue('New')) {
                    callback(AkGlobal.intl.formatMessage({ id: name }));
                } else {
                    callback();
                }
            }
        }
    }

    checkConfirm(props, name) {
        return {
            validator: (rule, value, callback) => {
                const form = this.props.form;
                if (value && this.state.confirmStatus) {
                    form.validateFields(['Confirm'], { force: true }, null);
                    callback();
                } else {
                    callback();
                }
            }
        }
    }


    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({ confirmStatus: this.state.confirmStatus || !!value });
    }

    changePasswordHandler() {
        if (this.loading) return;
        const { formatMessage } = AkGlobal.intl;
        this.props.form.validateFieldsAndScroll(null, {}, (err, values) => {
            if (!err) {
                this.loading = true;
                const params: UpdatePasswordRequest = {
                    OldPwd: values.Current,
                    NewPwd: values.New
                }
                MerchatAPI.UpdatePassword(params).then(data => {
                    if (data.Status === 0) {
                        AkMessage.success(formatMessage({ id: MasterPageLocale.ChangePasswordSuccess }));
                        this.props.onOk && this.props.onOk();
                    } else {
                        AkNotification.error({
                            message: AkGlobal.intl.formatMessage({ id: CommonLocale.Tip }),
                            description: AkGlobal.intl.formatMessage({ id: MasterPageLocale.ChangePasswordFail })
                        });
                    }
                    this.loading = false;
                })
            }
        })
    }

    render() {
        const { formatMessage } = AkGlobal.intl;
        const { props } = this;
        const lableLayout = { span: 24, lg: 6, md: 6, sm: 6, xs: 6 };
        const controlLayout = { span: 24, lg: 18, md: 18, sm: 18, xs: 18 };
        return <div className="ak-update-password-modal">
            <p className="ak-update-password-modal-title">
                {formatMessage({ id: MasterPageLocale.ChangePassword })}
            </p>
            <AkRow className="ak-update-password-modal-item">
                {FormHelper.renderFormItem(props, MasterPageLocale.Current, "Current",
                    <AkInput
                        className="ak-update-password-modal-item-input"
                        placeholder={formatMessage({ id: MasterPageLocale.CurrentTip })}
                        type="password"
                    />, true, [FormHelper.ruleForRequire(props, MasterPageLocale.Current)], lableLayout, controlLayout)
                }
            </AkRow>
            <AkRow className="ak-update-password-modal-item">
                {FormHelper.renderFormItem(props, MasterPageLocale.New, "New",
                    <AkInput
                        className="ak-update-password-modal-item-input"
                        placeholder={formatMessage({ id: MasterPageLocale.NewTip })}
                        type="password"
                    />,
                    true, [FormHelper.ruleForRequire(props, MasterPageLocale.New),
                    FormHelper.ruleForRegExp(props, MasterPageLocale.New, MasterPageLocale.PasswordRuleStrength, /^(?=.*[0-9])(?=.*[a-zA-Z])(.{6,18})$/),
                    this.checkConfirm(props, MasterPageLocale.PasswordRuleEqual)
                    ], lableLayout, controlLayout)
                }
            </AkRow>
            <AkRow className="ak-update-password-modal-item">
                {FormHelper.renderFormItem(props, MasterPageLocale.Confirm, "Confirm",
                    <AkInput
                        className="ak-update-password-modal-item-input"
                        placeholder={formatMessage({ id: MasterPageLocale.ConfirmTip })}
                        type="password"
                        onBlur={this.handleConfirmBlur} />,
                    true, [FormHelper.ruleForRequire(props, MasterPageLocale.Confirm),
                    this.checkPassword(props, MasterPageLocale.PasswordRuleEqual)
                    ], lableLayout, controlLayout)
                }
            </AkRow>
            <div className="ak-update-password-modal-button-group">
                <AkButton
                    onClick={() => this.changePasswordHandler()}
                    className="ak-update-password-modal-button-group-item1" type="primary">
                    {formatMessage({ id: CommonLocale.OK })}
                </AkButton>
                <AkButton
                    onClick={() => this.props.onCancel && this.props.onCancel()}
                    className="ak-update-password-modal-button-group-item2" type="ghost">
                    {formatMessage({ id: CommonLocale.Cancel })}
                </AkButton>
            </div>
        </div>
    }
}