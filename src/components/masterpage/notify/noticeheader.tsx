import * as React from 'react';
import { AkRow, AkCol, AkIcon, AkModal, AkIdentityPickerAdv } from "../../index";
import { AkIdentityType, AkUser, AkContext, NotificationAction, CommonLocale } from "../../../index";
import { AkGlobal } from '../../../util/common';
import { AkNotification } from '../../controls/ak-notification';
import { NotifyLocal } from '../../../locales/localeid';
import { notification } from '../../../reducers/notification';
import { ChatTypeEnum } from '../../../api/masterpage/socketmodel';
import { connect } from 'react-redux';
import { IdentityAPI } from '../../../api/identity/identity';

export interface NoticeHeaderProps {
    title?: string | React.ReactNode;
    /**添加 */
    add?: boolean;
    /**返回 */
    back?: boolean;
    /** 聊天信息 */
    chat?: boolean;

    closeble?: boolean;
    onBack?: () => void;
    onClose?: () => void;
    onChatInfo?: () => void;
    identityDict?: AkUser[];
}
export interface NoticeHeaderStates {
    loading?: boolean;
    advanceVisible?: boolean;
    users?: AkUser[];
    chat?: boolean;
}

@connect((state) => {
    return {
        identityDict: state.identity.identityDict
    }
})
export class NoticeHeader extends React.Component<NoticeHeaderProps, NoticeHeaderStates> {
    selectedUser?: AkUser;
    constructor(props, context) {
        super(props, context);
        this.state = {
            loading: true,
            chat: props.chat,
        };
        this.selectedUser = null;
    }


    async componentDidMount() {
        let requestMe = await IdentityAPI.resolveIdentities({ identities: [{ ID: AkContext.getUser().AccountID, Type: 1 }] });
        this.selectedUser = requestMe.Data[0] as AkUser;
    }


    componentWillReceiveProps(nextProps) {
        if ("chat" in nextProps && nextProps.chat !== this.props.chat) {
            this.setState({ chat: nextProps.chat });
        }
    }

    /** 选择用户，进行聊天 */
    renderChooseUser() {
        const { advanceVisible, users } = this.state;
        return advanceVisible && <AkModal style={{ top: 30 }}
            visible={true}
            onOk={() => {
                var memberIDs = users ? users.map(item => item.ID) : [];
                memberIDs.push(AkContext.getUser().AccountID);
                AkGlobal.store.dispatch(NotificationAction.createChat([...new Set(memberIDs)]));
                this.setState({ users: null, advanceVisible: false })
            }}
            onCancel={() => {
                this.setState({ advanceVisible: false, users: null })
            }}>
            <AkIdentityPickerAdv
                multiple
                value={users ? [this.selectedUser, ...users] : [this.selectedUser]}
                disableValues={[AkContext.getUser().AccountID]}
                identityTypes={[AkIdentityType.User]}
                onChange={(value) => {
                    this.setState({ users: Object.keys(value).map(k => value[k]) })
                }}></AkIdentityPickerAdv>
        </AkModal >
    }

    render() {
        const { title, onClose, closeble, add, back, onBack, onChatInfo } = this.props;
        const { chat } = this.state;
        const { loading } = this.state;
        const chatItem = AkGlobal.store.getState().notification.chatItem
        const type = chatItem && chatItem.Chat.Type === ChatTypeEnum.OneToOneChat;
        return <AkRow type="flex" align="middle" justify="space-between" className="ak-notice-header">
            <AkCol>
                {back && <AkIcon type="leftarrow" onClick={onBack} className="ak-notice-header-btn">
                </AkIcon>}
                {chat && !type ?
                    <div style={{ display: "inline-block" }}>
                        <div className="ak-notice-header-title">
                            {String(title).split("|")[0]}
                        </div>
                        <span>
                            {String(title).split("|")[1]}
                        </span>
                    </div> :
                    <div className="ak-notice-header-title">{title}</div>}
            </AkCol>
            <AkCol>
                {chat && !type && <AkIcon type="multi-task" onClick={onChatInfo} className="ak-notice-header-btn"></AkIcon>}
                {chat && type && < AkIcon type="user" onClick={onChatInfo} className="ak-notice-header-btn"></AkIcon>}
                {add && <AkIcon type="plus" onClick={() => {
                    this.setState({ advanceVisible: true })
                }} className="ak-notice-header-btn">
                </AkIcon>}
                {this.renderChooseUser()}
                {closeble && <AkIcon type="close" onClick={onClose} className="ak-notice-header-btn">
                </AkIcon>}
            </AkCol>

        </AkRow >;
    }
}