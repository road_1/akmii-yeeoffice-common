import * as React from 'react';
import { AkRow } from '../../controls/ak-row';
import { connect } from 'react-redux';
import { AkAvatar } from '../../controls/ak-avatar';
import { AkCol, AkButton, AkSwitch, AkModal } from '../../index';
import { ChatPullModel, ChatTypeEnum } from '../../../api/masterpage/socketmodel';
import { AkGlobal, NotificationAction, AkIdentityPickerAdv, AkIdentityType, AkContext, IdentityAPI } from '../../../index';
import { AkIcon } from '../../controls/ak-icon';
import { AkInput } from '../../controls/ak-input';
import { NoticeLocale, NotifyLocal } from '../../../locales/localeid';
import { AkUser } from '../../../api/identity/identity';
import { AkPopover } from '../../controls/ak-popover';

export interface ChatInfoProps {
    chatPerson?: string[];
    chatItem?: ChatPullModel;
}
export interface ChatInfoStates {
    loading?: boolean;
    isEdit?: boolean;
    advanceVisible?: boolean;
    users?: AkUser[];
    isDelete?: boolean;
    isOpen?: boolean;
}
@connect((state) => {
    return {
        chatItem: state.notification.chatItem,
        chatPerson: state.notification.chatPerson
    }
})
export class ChatInfo extends React.Component<ChatInfoProps, ChatInfoStates> {
    groupname?: string;
    selectedMe?: AkUser;
    constructor(props, context) {
        super(props, context);
        this.state = {
            loading: true,
            isEdit: false,
            isOpen: false
        };
        this.groupname = props.chatItem.Chat.Name;
    }

    async componentDidMount() {
        let requestMe = await IdentityAPI.resolveIdentities({ identities: [{ ID: AkContext.getUser().AccountID, Type: 1 }] });
        this.selectedMe = requestMe.Data[0] as AkUser;
    }

    onBtnClick = (e) => {
        e.stopPropagation();
        e.preventDefault();
        this.setState({ isEdit: false });
        AkGlobal.store.dispatch(NotificationAction.requestGroupName(this.groupname))
    }

    onDeleteCancel = (e) => {
        if (e.target.className) {
            this.setState({ isDelete: false });
        }
    }

    /** 选择用户，进行聊天 */
    renderChooseUser() {
        const { advanceVisible, users } = this.state;
        const { chatPerson } = this.props;
        const selected = AkGlobal.store.getState().identity.identityDict;
        const selectedUser = [...chatPerson.map(i => selected[i] && selected[i].data), this.selectedMe];
        return advanceVisible && <AkModal style={{ top: 30 }}
            visible={true}
            onOk={() => {
                AkGlobal.store.dispatch(NotificationAction.requestAddGroupPerson(users));
                this.setState({ users: null, advanceVisible: false });
            }}
            onCancel={() => {
                this.setState({ advanceVisible: false });
            }}>
            <AkIdentityPickerAdv
                multiple
                value={users ? [...selectedUser, ...users] : selectedUser}
                disableValues={[...chatPerson, AkContext.getUser().AccountID]}
                identityTypes={[AkIdentityType.User]}
                onChange={(value) => {
                    this.setState({ users: Object.keys(value).map(k => value[k]) })
                }}></AkIdentityPickerAdv>
        </AkModal>
    }

    render() {
        const { isEdit, isDelete, isOpen } = this.state;
        const { chatPerson, chatItem } = this.props;
        return <div className="ak-notice-detail" onClick={this.onDeleteCancel.bind(this)}>
            <div className="ak-notice-detail-person">
                <AkRow type="flex" justify="start">
                    {chatPerson && chatPerson.map((item, index) => {
                        if (!isOpen && index > 17) return;
                        return <div key={item} onClick={(e) => e.stopPropagation()}>
                            <AkCol className="ak-notice-detail-person-item">
                                {isDelete &&
                                    item !== AkContext.getUser().AccountID &&
                                    <div className="ak-notice-detail-person-item-delete" onClick={(e) => { AkGlobal.store.dispatch(NotificationAction.requestDeleteGroupPerson([item])) }}>×</div>}
                                <AkAvatar textClassName="ak-notice-detail-person-text"
                                    style={{ width: 50 }}
                                    withChat={false} type="combination" layout="center" shape="square" value={item}></AkAvatar>
                            </AkCol>
                        </div>
                    })}
                    {chatPerson && !(chatPerson.length === 1 && chatPerson[0] === AkContext.getUser().AccountID && chatItem.Chat.Type === ChatTypeEnum.OneToOneChat) &&
                        <AkCol key={"add"} className="ak-notice-detail-person-item">
                            <div
                                className="ak-notice-detail-person-add"
                                onClick={() => { this.setState({ advanceVisible: true }) }}
                            >
                                <AkIcon type="plus" />
                            </div>
                        </AkCol>
                    }
                    {chatItem.Chat.Type === ChatTypeEnum.GroupChat &&
                        chatItem.Chat.Owner === AkContext.getUser().AccountID &&
                        chatPerson.length > 1 &&
                        <AkCol key={"delete"} className="ak-notice-detail-person-item">
                            <div
                                className="ak-notice-detail-person-delete"
                                onClick={(e) => { e.stopPropagation(); this.setState({ isDelete: true }) }}
                            >
                                <AkIcon type="minus" />
                            </div>
                        </AkCol>
                    }
                </AkRow>
                {chatPerson.length > 18 && <AkRow type="flex" justify="space-around" align="middle" className="ak-notice-detail-person-more">
                    <a href="javascript:void(0)" onClick={() => this.setState({ isOpen: !isOpen })}>
                        {AkGlobal.intl.formatMessage({ id: isOpen ? NotifyLocal.HiddenGroupUser : NotifyLocal.ShowAllGroupUser })}
                        <AkIcon type={isOpen ? "up" : "down"} />
                    </a>
                </AkRow>}
            </div>
            <div className="ak-notice-detail-top">
                <AkRow >
                    <AkCol span={4}>{AkGlobal.intl.formatMessage({ id: NotifyLocal.Stick })}</AkCol>
                    <AkCol span={4} offset={16}>
                        <AkSwitch value={chatItem.AccountChat.IsFavorite} onChange={(value) => {
                            AkGlobal.store.dispatch(NotificationAction.requestChangeIsTop(value))
                        }} />
                    </AkCol>
                </AkRow>
            </div>
            {
                chatItem.Chat.Type === ChatTypeEnum.GroupChat &&
                chatItem.Chat.Owner === AkContext.getUser().AccountID &&
                <div className="ak-notice-detail-groupname" onClick={() => {
                    this.setState({ isEdit: true })
                }}>
                    <AkRow>
                        <AkCol span={5}>{AkGlobal.intl.formatMessage({ id: NotifyLocal.GroupNames })}</AkCol>
                        <AkCol span={15} offset={4}>
                            {isEdit ?
                                <div>
                                    <AkInput className="ak-notice-detail-groupname-edit" defaultValue={this.groupname} onChange={(value) => { this.groupname = value }} />
                                    <AkButton className="ak-notice-detail-groupname-save" icon="save" onClick={this.onBtnClick.bind(this)} />
                                </div>
                                :
                                <div>
                                    <AkIcon className="ak-notice-detail-groupname-right" type="right" />
                                    <div className="ak-notice-detail-groupname-name">{chatItem.Chat.Name}</div>
                                </div>
                            }
                        </AkCol>
                    </AkRow>
                </div>
            }
            {
                chatItem.Chat.Type === ChatTypeEnum.GroupChat &&
                <AkRow type="flex" justify="center">
                    <AkCol style={{ width: "90%" }}>
                        <AkButton type="primary" className="ak-notice-detail-quit" onClick={() => {
                            AkModal.confirm({
                                title: AkGlobal.intl.formatMessage({ id: NoticeLocale.DeleteTitle }),
                                content: AkGlobal.intl.formatMessage({ id: NoticeLocale.DeleteContent }),
                                okType: 'danger',
                                onOk: () => {
                                    AkGlobal.store.dispatch(NotificationAction.requestQuitChat());
                                }
                            })

                        }}>
                            {AkGlobal.intl.formatMessage({ id: NoticeLocale.QuitGroup })}
                        </AkButton>
                    </AkCol>
                </AkRow>
            }
            {this.renderChooseUser()}
        </div >;
    }
}