import * as React from 'react';
import { ChatPullModel, ChatTypeEnum, TemplateMsgModel } from "../../../api/masterpage/socketmodel";
import { AkRow, AkCol, AkBadge, AkAvatar, AkNoContent, AkImg, AkIcon } from "../../index";
import { AkGlobal, AkContext, AkUtil } from "../../../index";
import { connect } from "react-redux";
import * as moment from 'moment'
import { ChatItem } from './chatitem';
import { NotificationAction } from '../../../actions/index';
import { NotifyLocal } from '../../../locales/localeid';

export interface ChatListProps {
    accountChatList?: ChatPullModel[];
    showChatItem?: boolean;
}
export interface ChatListStates {
    loading?: boolean;
}
const mapStateToProps = (state) => {
    return {
        accountChatList: state.notification.accountChatList,
        showChatItem: state.notification.showChatItem,
    };
};
// @observer
@connect(mapStateToProps)
export class ChatList extends React.Component<ChatListProps, ChatListStates> {
    constructor(props, context) {
        super(props, context);
        this.state = {
            loading: true,
        };
    }

    renderItem(chatItem: ChatPullModel) {
        let tempChatMessage = chatItem.MessagesPull;
        let tempMessage = tempChatMessage ? tempChatMessage.Messages.sort((a, b) => {
            return moment(a.Created).unix() - moment(b.Created).unix();
        }) : [];

        let lastMessage = tempMessage[tempMessage.length - 1];
        let templateMessage = chatItem.Chat.Type === ChatTypeEnum.NoticeChat
            && (lastMessage && JSON.parse(lastMessage.Body) as TemplateMsgModel);
        let body = (templateMessage && templateMessage.Title) || (lastMessage && lastMessage.Body) || '';

        var targetID = chatItem.AccountChat.ChatID.replace(AkContext.getUser().AccountID, '').replace('|', '');
        return <div key={chatItem.AccountChat.ChatID}
            className="ak-notice-list-item"
            style={{ backgroundColor: chatItem.AccountChat.IsFavorite ? "#fafafa" : "#fff" }}
            onClick={() => {
                AkGlobal.store.dispatch(NotificationAction.saveChatItem(chatItem));
                AkGlobal.store.dispatch(NotificationAction.showChatItem(true));
            }}><AkRow type="flex" gutter={40} >
                <AkCol span={4}>
                    <AkBadge count={chatItem.UnReadCount} style={{ right: "-20px" }} >
                        {
                            chatItem.Chat.Type === ChatTypeEnum.NoticeChat
                                ? <AkImg
                                    magnifier={false}
                                    className="ak-notice-list-item-icon" src={AkContext.getSystemNoticeIcon()}
                                    width={50} height={50}></AkImg>
                                : (
                                    chatItem.Chat.Photo ?
                                        <AkImg src={chatItem.Chat.Photo}
                                            magnifier={false}
                                            lazyLoad={false}
                                            width={50} height={50}
                                        /> :
                                        <AkAvatar layout="inline" shape="square" value={targetID}></AkAvatar>
                                )
                        }
                    </AkBadge>
                </AkCol>
                <AkCol span={20}>
                    <AkRow type="flex" justify="space-between" className="ak-notice-list-item-content">
                        <AkCol className="ak-notice-list-item-title">
                            {chatItem.Chat.Type === ChatTypeEnum.OneToOneChat
                                ? <AkAvatar withChat={false} type="text" value={targetID}></AkAvatar>
                                : (chatItem.Chat.Type === ChatTypeEnum.NoticeChat
                                    ? AkGlobal.intl.formatMessage({ id: NotifyLocal.Notification })
                                    : (chatItem.Chat.Name || chatItem.Chat.Ext1))}
                        </AkCol>
                        <AkCol className="ak-notice-list-item-time">
                            {AkUtil.getChatTime(moment(chatItem.LastMessageTime))}
                        </AkCol>
                    </AkRow>
                    <AkRow>
                        <AkCol className="ak-notice-list-item-body">
                            {body}
                        </AkCol>
                    </AkRow>
                </AkCol>
            </AkRow>
        </div>
    }

    render() {
        const { accountChatList, showChatItem } = this.props;
        const { loading } = this.state;
        let topChatList = accountChatList.filter(i => i.AccountChat.IsFavorite && !i.AccountChat.Status).sort((a, b) => {
            return moment(b.LastMessageTime).unix() - moment(a.LastMessageTime).unix();
        });

        let bottomChatList = accountChatList.filter(i => !i.AccountChat.IsFavorite && !i.AccountChat.Status).sort((a, b) => {
            return moment(b.LastMessageTime).unix() - moment(a.LastMessageTime).unix();
        });

        return showChatItem ? <ChatItem /> : (
            topChatList.length + bottomChatList.length > 0 ? <div className="ak-notice-list">
                {
                    topChatList && topChatList.map(item => {
                        return item.AccountChat.Status === 0 && this.renderItem(item);
                    })
                }
                {
                    bottomChatList && bottomChatList.map(item => {
                        return item.AccountChat.Status === 0 && this.renderItem(item);
                    })
                }
            </div> : <div className="ak-notice-nomessage">
                    <AkIcon type="wuxiaoxi-line" />
                    <p>{AkGlobal.intl.formatMessage({ id: NotifyLocal.NoMessage })}</p>
                </div>
        )
    }
}