import * as React from 'react';
import { ChatPullModel, ChatMessagePullModel, MessagePullDirectionEnum, ChatTypeEnum, TemplateMsgModel, IBtn } from "../../../api/masterpage/socketmodel";
import { AkRow, AkCol, AkAvatar, AkCard, AkDateLabel } from "../../index";
import { connect } from "react-redux";
import { ChatFooter } from "./chatfooter";
import { AkContext, AkUtil, NotificationAction } from "../../../index";
import * as classNames from "classnames";
import * as moment from 'moment'
import { SocketClient } from "../../../api/masterpage/socketclient";
import * as ReactDOM from "react-dom";
import { AkGlobal } from '../../../util/common';
import { NotifyLocal } from '../../../locales/localeid';
import AkImg from '../../controls/ak-img';

export interface ChatItemProps {
    chatItem?: ChatPullModel;
}
export interface ChatItemStates {
    loading?: boolean;
    message?: string;
    scrollTop?: number;
}
const mapStateToProps = (state) => {
    return {
        chatItem: state.notification.chatItem,
    };
};
@connect(mapStateToProps)
export class ChatItem extends React.Component<ChatItemProps, ChatItemStates> {
    constructor(props, context) {
        super(props, context);
        this.state = {
            loading: true,
        };
    }
    firstMessageID?: string = '0';
    lastMessageID?: string = '0';
    notScroll: boolean = false;

    componentDidMount() {
        this.scrollToBottom();
        this.readMessage();
    }

    componentDidUpdate() {
        if (!this.notScroll) {
            this.scrollToBottom();
            this.readMessage();
            this.notScroll = false;
        }
    }
    readMessage() {
        const { chatItem } = this.props;
        if (chatItem.AccountChat.LastReadID !== this.lastMessageID && this.lastMessageID) {
            SocketClient.getInstance().readMessage({
                ChatID: chatItem.AccountChat.ChatID,
                LastMessageID: this.lastMessageID
            });
        }
    }
    scrollToBottom() {
        var element = (ReactDOM.findDOMNode(this.refs["ak-message-box"]) as HTMLInputElement);
        element.scrollTop = element.scrollHeight;
    }
    renderMessage(tempChatMessage: ChatMessagePullModel) {

        let preTime: moment.Moment;
        let tempMessage = tempChatMessage.Messages;
        this.firstMessageID = '0';
        this.lastMessageID = tempMessage.length > 0 && tempMessage[tempMessage.length - 1].MessageID;
        return tempMessage.map(item => {
            let myMessage = item.Author === AkContext.getUser().AccountID;
            let className = classNames('ak-notice-chat-list-message', { 'me': myMessage })
            let messageDate = moment(item.Created);
            let timeShow: React.ReactNode;
            //拉取历史消息需要的ID
            if (this.firstMessageID === '0') {
                this.firstMessageID = item.MessageID;
            }
            //时间
            if (preTime === undefined || messageDate.diff(preTime, 'm') > 5) {
                preTime = messageDate;
                timeShow = <AkRow type="flex" justify="center" style={{ margin: "7px 0px" }}>
                    <AkCol className="ak-notice-chat-system">
                        {AkUtil.getChatTime(messageDate)}
                    </AkCol>
                </AkRow>
            }
            return <div key={item.MessageID} >
                {
                    timeShow
                }
                <AkRow
                    className="ak-notice-chat-list-item" type="flex"
                    justify={myMessage ? "end" : "start"} gutter={20}>
                    <AkCol order={myMessage ? 2 : 1}>
                        <AkAvatar size={30} shape="square" value={item.Author} layout="inline"></AkAvatar>
                    </AkCol>
                    <AkCol order={myMessage ? 1 : 2} className={className}>
                        <span className="arrow"></span>
                        <pre>
                            {item.Body}
                        </pre>
                    </AkCol>
                </AkRow>
            </div>
        });
    }
    renderCardFooter(btns: IBtn[]) {
        return <div className="ak-card-footer">
            {
                btns.map(item => {
                    return <AkRow key={item.Code} type='flex' justify='center' >
                        <AkCol>
                            <a href={item.Url}>
                                {this.renderFooterButton(item)}
                            </a>
                        </AkCol>
                    </AkRow>
                })
            }
        </div>
    }

    renderFooterButton(item: IBtn) {
        switch (item.Code) {
            case "detail":
                return AkGlobal.intl.formatMessage({ id: NotifyLocal.ViewDetail });
            default:
                return item.Name;
        }
    }

    renderTemplate(tempChatMessage: ChatMessagePullModel) {
        let preTime: moment.Moment;
        let tempMessage = tempChatMessage.Messages;
        this.firstMessageID = '0';
        this.lastMessageID = tempMessage.length > 0 && tempMessage[tempMessage.length - 1].MessageID;
        return tempMessage.map(item => {
            let myMessage = item.Author === AkContext.getUser().AccountID;
            let className = classNames('ak-notice-chat-list-message', { 'me': myMessage })
            let messageDate = moment(item.Created);
            let timeShow: React.ReactNode;
            //拉取历史消息需要的ID
            if (this.firstMessageID === '0') {
                this.firstMessageID = item.MessageID;
            }
            //时间
            if (preTime === undefined || messageDate.diff(preTime, 'm') > 5) {
                preTime = messageDate;
                timeShow = <AkRow type="flex" justify="center" style={{ margin: "7px 0px" }}>
                    <AkCol className="ak-notice-chat-system">
                        {AkUtil.getChatTime(messageDate)}
                    </AkCol>
                </AkRow>
            }
            try {
                let notice: TemplateMsgModel = JSON.parse(item.Body);
                const title = <AkRow type='flex'>
                    <AkCol className="ak-notice-system-card-img">
                        <AkImg width={40} height={40} src={notice.IconUrl} lazyLoad={false}></AkImg>
                    </AkCol>
                    <AkCol className="ak-notice-system-card-content">
                        <div>{notice.Title}</div>
                        <div><AkDateLabel value={notice.Created} /></div>
                    </AkCol>
                </AkRow>
                const Name1 = notice.Data[0]['Key'] === "AppUser" ? AkGlobal.intl.formatMessage({ id: NotifyLocal.AppUser }) : notice.Data[0]['Name'];
                const Name2 = notice.Data[1]['Key'] === "AppCreated" ? AkGlobal.intl.formatMessage({ id: NotifyLocal.AppCreated }) : notice.Data[1]['Name'];
                return <div className="ak-notice-system-card" key={item.MessageID}>
                    {
                        timeShow
                    }
                    <AkCard noHovering key={item.MessageID} title={title}>
                        {
                            (notice.TemplateCode === 'html' || notice.TemplateCode === 'text') && <div dangerouslySetInnerHTML={{ __html: notice.Data }}></div>
                        }
                        {
                            notice.TemplateCode === 'params' &&
                            <div>
                                <AkRow type='flex' >
                                    <AkCol>{Name1}:</AkCol>
                                    <AkCol>{notice.Data[0]['Value']}</AkCol>
                                </AkRow>
                                <AkRow type='flex' >
                                    <AkCol>{Name2}:</AkCol>
                                    <AkCol>{notice.Data[1]['Key'] === "AppCreated" ? <AkDateLabel value={notice.Data[1]['Value']} /> : notice.Data[1]['Value']}</AkCol>
                                </AkRow>
                            </div>
                        }
                        {
                            this.renderCardFooter(notice.Btns)
                        }
                    </AkCard>
                </div>
            } catch{
                return <div className="ak-notice-system-card" key={item.MessageID}>
                    {
                        timeShow
                    }
                    <AkCard noHovering key={item.MessageID}>

                        <div dangerouslySetInnerHTML={{ __html: item.Body }}></div>
                    </AkCard>
                </div>
            }
        });
    }
    renderGetMoreMessage(tempChatMessage) {
        return <AkRow type="flex" justify="center" style={{ margin: "7px 0px" }}>
            {
                (tempChatMessage === undefined ||
                    (tempChatMessage.Direction === MessagePullDirectionEnum.Up && tempChatMessage.More) ||
                    (tempChatMessage.Direction === MessagePullDirectionEnum.Down)
                ) &&
                <AkCol className="ak-notice-chat-system"><a onClick={() => {
                    AkGlobal.store.dispatch(NotificationAction.requestHistoryMessage())
                    // SocketClient.getInstance().pullMessage({
                    //     ChatID: chatItem.Chat.ChatID,
                    //     Direction: MessagePullDirectionEnum.Up,
                    //     LastMsgID: this.firstMessageID
                    // })
                    this.notScroll = true;
                }}>{AkGlobal.intl.formatMessage({ id: NotifyLocal.GetHistoricalMessage })}</a>
                </AkCol>
            }
        </AkRow>;
    }
    render() {
        const { props: { chatItem } } = this;
        let tempChatMessage = chatItem.MessagesPull;
        let className = classNames('ak-notice-chat-list', { 'ak-notice-chat-nofooter': chatItem.Chat.Type === ChatTypeEnum.NoticeChat })
        return <div className="ak-notice-chat">
            <div className={className}
                ref="ak-message-box"
                onScroll={(e) => {
                    if ((e.currentTarget.clientHeight + e.currentTarget.scrollTop) === e.currentTarget.scrollHeight) {
                        this.notScroll = false;
                    }
                }}>
                {
                    chatItem.Chat.Type === ChatTypeEnum.NoticeChat ?
                        <div>
                            {this.renderGetMoreMessage(tempChatMessage)}
                            {
                                this.renderTemplate(tempChatMessage)
                            }
                        </div>
                        : <div>
                            {this.renderGetMoreMessage(tempChatMessage)}
                            {tempChatMessage && this.renderMessage(tempChatMessage)}
                            <ChatFooter chatItem={chatItem}></ChatFooter>
                        </div>
                }
            </div>
        </div>;
    }
}