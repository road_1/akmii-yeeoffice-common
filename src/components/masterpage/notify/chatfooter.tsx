import * as React from 'react';
import { AkButton, AkCol, AkRow, AkInput, AkNotification } from "../../index";
import { SocketClient } from "../../../api/masterpage/socketclient";
import { ChatPullModel, MessageTypeEnum } from "../../../api/masterpage/socketmodel";
import { AkGlobal, CommonLocale } from '../../..';
import { NotifyLocal } from '../../../locales/localeid';

export interface ChatFooterProps {
    chatItem?: ChatPullModel
    onMessageSend?: () => void;
}
export interface ChatFooterStates {
    loading?: boolean;
    message?: string;
}
export class ChatFooter extends React.Component<ChatFooterProps, ChatFooterStates> {
    shiftAndeENter?: boolean;
    constructor(props, context) {
        super(props, context);
        this.state = { loading: true, message: '' };
        this.shiftAndeENter = true;
    }

    sendMessage(e) {
        if(e.shiftKey) {
            this.shiftAndeENter = false;
            return;
        }
        if(e.hasOwnProperty("keyCode") && e.keyCode !== 13) {
            return;
        }
        const { chatItem, onMessageSend } = this.props;
        const { message } = this.state;
        if (message.trim() !== "") {
            SocketClient.getInstance().sendMessage({
                ChatID: chatItem.Chat.ChatID,
                Type: MessageTypeEnum.Text,
                Body: message
            });
            this.setState({ message: '' });
            onMessageSend && onMessageSend();
        } else {
            AkNotification.warning({
                message: AkGlobal.intl.formatMessage({ id: CommonLocale.Tip }),
                description: AkGlobal.intl.formatMessage({ id: NotifyLocal.NoSendNullMessage })
            });
        }
    }

    render() {
        const { loading, message } = this.state;
        return <div className="ak-notice-chat-footer">
            <AkInput.TextArea value={message} className="ak-notice-chat-footer-messagebox"
                onKeyDown={this.sendMessage.bind(this)}
                onKeyUp={(e)=> {
                    this.shiftAndeENter = true;
                }}
                onChange={(v) => {
                    let value = v.currentTarget.value
                    if(this.shiftAndeENter) {
                        value = value.replace(/[\r\n]/g, "");
                    }
                    this.setState({ message: value })
                }}></AkInput.TextArea>
            <AkRow type="flex" justify="end" className="ak-notice-chat-footer-sendbtnbox">
                {/* <AkCol>
                    <AkIcon type="folder"></AkIcon>
                    <AkIcon type="picture"></AkIcon>
                </AkCol> */}
                <AkCol>
                    <AkButton disabled={message === ''} onClick={this.sendMessage.bind(this)} type="primary" icon="fly">{AkGlobal.intl.formatMessage({id: NotifyLocal.Send})}</AkButton>
                </AkCol>
            </AkRow>
        </div>
    }
}