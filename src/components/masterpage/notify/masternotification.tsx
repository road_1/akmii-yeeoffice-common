import * as React from 'react';
import { AkIcon, AkBadge, AkSidebar, AkAvatar } from "../../index";
import * as classNames from "classnames";
import { AkContext, NotificationAction, AkUtil } from "../../../index";
import { ChatList } from "./chatlist";
import { MerchatAPI } from "../../../api/masterpage/masterpage";
import { connect } from "react-redux";
import { ChatItem } from "./chatitem";
import { SocketClient } from "../../../api/masterpage/socketclient";
import { ChatPullModel, ChatTypeEnum, AccountChatModel } from "../../../api/masterpage/socketmodel";
import { NoticeHeader } from "./noticeheader";
import { AkGlobal } from '../../../util/common';
import { ChatInfo } from './chatinfo';
import { NoticeLocale, NotifyLocal } from '../../../locales/localeid';

export interface MasterNotificationProps {
    showChatItem?: boolean;
    chatItem?: ChatPullModel;
    accountChatList?: ChatPullModel[];
    showChatInfo?: boolean;
}
export interface MasterNotificationStates {
    showChatList?: boolean;
    // chatItem?: ChatPullModel;
    // accountChatList?: ChatPullModel[];
    // showChatItem?: boolean;
}
const mapStateToProps = (state) => {
    return {
        showChatItem: state.notification.showChatItem,
        chatItem: state.notification.chatItem,
        accountChatList: state.notification.accountChatList,
        showChatInfo: state.notification.showChatInfo,
    };
};
// @observer
@connect(mapStateToProps)
export class MasterNotification extends React.Component<MasterNotificationProps, MasterNotificationStates> {
    onMouseUp;
    constructor(props: MasterNotificationProps, context) {
        super(props, context);
        this.state = {
        };
    }

    componentDidMount() {
        MerchatAPI.getSocketToken().then((data) => {
            if (data.Status === 0) {
                SocketClient.getInstance().init(data.Data);
            }
        })
    }

    onBack() {
        if (this.props.showChatInfo) {
            AkGlobal.store.dispatch(NotificationAction.showChatInfo(false));
            AkGlobal.store.dispatch(NotificationAction.showChatItem(true));
        } else {
            AkGlobal.store.dispatch(NotificationAction.showChatItem(false));
        }
        this.setState({ showChatList: true }, () => {

        })
    }

    onChatInfo() {
        AkGlobal.store.dispatch(NotificationAction.showChatInfo(true));
        AkGlobal.store.dispatch(NotificationAction.requestChatPerson());
        AkGlobal.store.dispatch(NotificationAction.showChatItem(false));
        // this.setState({ showChatList: true }, () => {

        // })
    }

    onClose() {
        AkGlobal.store.dispatch(NotificationAction.showChatInfo(false));
        AkGlobal.store.dispatch(NotificationAction.showChatItem(false));
        this.setState({ showChatList: false }, () => {

        })
    }

    renderContent() {
        const { showChatItem, chatItem, showChatInfo } = this.props;
        const { showChatList } = this.state;
        let targetID = (showChatItem || showChatInfo) && chatItem.AccountChat.ChatID.replace(AkContext.getUser().AccountID, '').replace('|', '');
        let title = showChatItem ?
            (chatItem.Chat.Type === ChatTypeEnum.OneToOneChat
                ? <AkAvatar withChat={false} type="text" value={targetID}></AkAvatar>
                : (chatItem.Chat.Type === ChatTypeEnum.NoticeChat
                    ? AkGlobal.intl.formatMessage({ id: NotifyLocal.Notification })
                    : (chatItem.Chat.Name || chatItem.Chat.Ext1) + (chatItem.Chat.Type === ChatTypeEnum.GroupChat ? "|（" + chatItem.Chat.MemberCount + "）" : "")))
            : (showChatInfo ? `${AkGlobal.intl.formatMessage({ id: NoticeLocale.ChatDetail })}` : AkGlobal.intl.formatMessage({ id: NoticeLocale.MessageCenter }))
        return <AkSidebar ref="sider"
            closable={false}
            wrapperClassName="ak-master-notification-sidebar"
            visible={showChatItem || showChatList || showChatInfo}
            width={380}>
            <NoticeHeader
                title={title}
                add={(showChatList && !showChatItem && !showChatInfo)}
                closeble={(showChatList && !showChatItem)}
                back={showChatItem || showChatInfo}
                chat={showChatItem && chatItem && chatItem.Chat.Type !== ChatTypeEnum.NoticeChat}
                onBack={this.onBack.bind(this)}
                onChatInfo={this.onChatInfo.bind(this)}
                onClose={this.onClose.bind(this)}
            ></NoticeHeader>
            {
                (showChatList && !showChatItem && !showChatInfo) && <ChatList ></ChatList>
            }
            {
                showChatItem && !showChatInfo && <ChatItem></ChatItem>
            }
            {
                showChatInfo && !showChatItem && <ChatInfo></ChatInfo>
            }
        </AkSidebar >;
    }
    render() {
        const { showChatItem, accountChatList } = this.props;
        const { showChatList } = this.state;
        var className = classNames("ak-master-notification",
            { "activity": showChatItem || showChatList });
        const unreadcount = AkUtil.sumBy(accountChatList, 'UnReadCount');
        return <div className={className} ref="wrapper">
            {this.renderContent()}
            <div className="ak-master-notification-detail" onClick={() => {
                if (showChatList) {
                    this.onClose();
                } else {
                    this.setState({ showChatList: true });
                }
            }}>
                {unreadcount > 0 ? <AkBadge className="ak-master-notification-count" count={unreadcount} overflowCount={99} /> : <AkIcon type="xiaoxi-line"></AkIcon>}
            </div>
        </div >;
    }
}