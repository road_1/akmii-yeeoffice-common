import * as React from "react";
import { Component, ReactNode } from 'react';
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { MasterPageAction, AppCenterAction } from "../../actions/index";
import { MerchatLoginModel } from "../../api/masterpage/masterpagemodel";
import { AkUtil, AppCenterListResponse } from "../../index";
import { MasterPageLocale } from "../../locales/localeid";
import { AkContext, AkGlobal } from '../../util/common';
import { URLHelper } from "../../util/URLHelper";
import { AkCol, AkLayout, AkRow, YeeFlowMasterPageUserInfo, AccessDenied } from "../index";
import { MasterPageNavigator } from "./navigator";
import { MasterNotification } from "./notify/masternotification";
import { Subscribe } from './subscribe';
import { MasterPageUserInfo } from "./userinfo";
import { MerchatAPI } from "../../api/masterpage/masterpage";

export interface MasterPageProps {
    onChangeLanguage?: (language: string) => void;
    showMasterHeader?: boolean;
    onLoaded?: (merchat?: MerchatLoginModel) => void;
    showHeader?: boolean;
    showFooter?: boolean;
    minHeight?: number;
    title?: string; //页面的title，默认是YeeOffice
    languages?: string[]; //系统支持的语言，默认['zh', 'en']
    renderActions?: React.ReactNode | React.ReactNode[];
}
export interface MasterPageStates {
    merchat?: MerchatLoginModel;
    loginFail?: boolean;
}
const mapStateToProps = (state) => {
    return {
        showHeader: state.masterpage.showHeader,
        showFooter: state.masterpage.showFooter,
        minHeight: state.masterpage.minHeight,
    };
}

@connect(mapStateToProps)
export class MasterPage extends Component<MasterPageProps, MasterPageStates> {
    static defaultProps: MasterPageProps = {
        showMasterHeader: true,
        languages: ['zh', 'en']
    };

    showHeader: boolean = true;
    constructor(props, context) {
        super(props, context);
        URLHelper.init();
        // require.ensure([], () => {
        require("../../themes/masterpage.less");
        // }, "default.css");
        let isDlg = AkUtil.getQueryString("isdlg");

        let temp = true;
        this.showHeader = this.props.showMasterHeader && temp;
        if (isDlg === "1" || isDlg === "true") {
            this.showHeader = false;
        }
        if (AkContext.isMobileLoginIn()) {
            this.showHeader = false;
        }
        this.state = {};
    }
    componentWillMount() {
        const { onLoaded, title } = this.props;
        const { merchat } = this.state;
        AkGlobal.store.dispatch(MasterPageAction.triggerMasterpageHeader(this.showHeader));
        //运行失败，调试完成再提交
        let logainfail = false;
        var loginData = MerchatAPI.merchatLogin((logainfail: boolean) => {
            logainfail = logainfail;
            this.setState({ loginFail: true });
        });

        if (logainfail) return;
        MerchatAPI.getMerchatInfo()
        onLoaded && onLoaded(loginData);
        this.setState({ merchat: loginData });
        if (AkContext.isNeteaseVersion()) {
            document.title = AkGlobal.intl.formatMessage({ id: MasterPageLocale.MasterTitleNetease });
        } else {
            document.title = loginData ? loginData.CompanyInfo.CompanyName : AkGlobal.intl.formatMessage({ id: MasterPageLocale.PageTitle });
        }


        //var loginData = AkContext.getMerchantData();
        // onLoaded && onLoaded(loginData);
        // this.setState({ merchat: loginData });

        // /**统一用公司名称*/
        // if (AkContext.isNeteaseVersion()) {
        //     document.title = AkGlobal.intl.formatMessage({ id: MasterPageLocale.MasterTitleNetease });
        // } else {
        //     document.title = loginData ? loginData.CompanyInfo.CompanyName : AkGlobal.intl.formatMessage({ id: MasterPageLocale.PageTitle });
        // }
        // if (title) {
        //     // document.title = title;
        // } else if (AkContext.isNeteaseVersion()) {
        //     document.title = AkGlobal.intl.formatMessage({ id: MasterPageLocale.MasterTitleNetease });
        // } else {
        //     // document.title = AkGlobal.intl.formatMessage({ id: MasterPageLocale.PageTitle });;
        //     document.title = loginData ? loginData.CompanyInfo.CompanyName : AkGlobal.intl.formatMessage({ id: MasterPageLocale.PageTitle });
        // }
    }

    getLogo(logo: string) {
        if (logo) {
            return logo;
        } else {
            if (AkContext.isNeteaseVersion()) {
                return "images/logo-psm-w.png"
            } else {
                return "images/logo-psm.png"
            }
        }
    }
    render() {
        const { merchat, loginFail } = this.state;
        const { showHeader, showFooter, minHeight, languages } = this.props;

        let show = true;
        if (!this.showHeader) {
            show = false;
        }
        if (!showHeader) {
            show = false;
        }
        if (AkContext.isMobileLoginIn()) {
            show = false;
        }
        let headerStyle: React.CSSProperties = show ? null : {
            display: "none"
        };
        let contentStyle: React.CSSProperties = show ? {
            marginTop: 56
        } : null;

        let footerStyle: React.CSSProperties = showFooter ? null : {
            display: "none",
        };
        const minheight = AkContext.isMobileLoginIn() ? "calc(100vh - 0px)" : `calc(100vh - 56px)`
        let contentHeight: React.CSSProperties = {
            minHeight: minheight,
            backgroundColor: "inherit"
        };

        let company = AkContext.getCompanyInfo();
        const isSPOnline = AkContext.getBranch() === AkContext.SPOnline;
        if (loginFail) {
            return <AccessDenied />
        } else {
            return merchat ? <AkLayout className="ak-react-masterpage">
                <AkLayout.Header style={headerStyle} className="ak-react-masterpage-header noprint" >
                    <AkRow type="flex" justify="space-between" align="middle" style={company ? {
                        background: company.NavBarColour, color: company.NavFontColour,
                        borderBottom: "solid 1px #DDD"
                    } : { borderBottom: "solid 1px #DDD" }}>
                        <AkCol xs={11} sm={15} md={8} lg={8}  >
                            {merchat ? <MasterPageNavigator languages={languages} merchatInfo={merchat}></MasterPageNavigator> : null}
                        </AkCol>
                        <AkCol xs={0} sm={0} md={8} lg={8} style={{ textAlign: "center" }}>
                            <img className="ak-master-logo" src={merchat && URLHelper.GetFileUrl(this.getLogo(merchat.CompanyInfo.Logo))} />
                        </AkCol>
                        <AkCol xs={13} sm={9} md={8} lg={8}>
                            <AkRow type="flex" justify="end">
                                <AkCol>
                                    <Subscribe color={merchat.CompanyInfo.NavFontColour} enterpriseInfo={merchat.EnterpriseInfo} tenantID={merchat.CompanyInfo.TenantID} onChangeLanguage={this.props.onChangeLanguage} ></Subscribe>
                                </AkCol>
                                {AkUtil.isArray(this.props.renderActions) ?
                                    (this.props.renderActions as React.ReactNode[]).map(item => {
                                        if ((this.props.renderActions as React.ReactNode[]).length > 0) {
                                            return <AkCol>
                                                {item}
                                            </AkCol>
                                        }
                                    })
                                    : (this.props.renderActions &&
                                        <AkCol>
                                            {this.props.renderActions}
                                        </AkCol>
                                    )
                                }
                                <AkCol>
                                    <MasterNotification></MasterNotification>
                                </AkCol>
                                <AkCol>
                                    {/* <MasterPageUserInfo userInfo={merchat && merchat.UserModel} onChangeLanguage={this.props.onChangeLanguage} ></MasterPageUserInfo> */}
                                    <YeeFlowMasterPageUserInfo
                                        languages={languages}
                                        enterpriseInfo={merchat && merchat.EnterpriseInfo}
                                        userInfo={merchat && merchat.UserModel}
                                        onChangeLanguage={(language) => {
                                            this.props.onChangeLanguage(language);
                                        }} />
                                </AkCol>
                            </AkRow>
                        </AkCol>
                    </AkRow>
                </AkLayout.Header>
                <AkLayout className="ak-react-masterpage-content">
                    <AkLayout.Content className="ak-master-content" style={contentStyle}>
                        <div style={contentHeight}>
                            {this.props.children}
                        </div>
                    </AkLayout.Content>
                </AkLayout>
                {/* <AkLayout.Footer style={footerStyle}>{AkContext.isNeteaseVersion() ?
                <FormattedMessage
                    id={MasterPageLocale.MasterFooterNetease}></FormattedMessage> : "YeeOffice provided by Akmii Information Limited"}</AkLayout.Footer> */}
            </AkLayout> : null;
        }
    }


}
class MasterPageStyle {
    static contentHeight: React.CSSProperties = {
        minHeight: "calc(100vh - 50px)"
    };
}

