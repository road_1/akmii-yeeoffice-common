import * as React from "react";
import { URLHelper, AkLayout, AkRow, AkCol, AkUtil, AkContext, MerchatLoginModel, AkGlobal, MasterPageAction, AccessDenied, AkModal, AkButton, AkBadge, MerchantLevelEnum, EnterpriseFlag } from "../..";
import { connect } from "react-redux";
import { MasterPageLocale } from "../../locales/localeid";
import { MasterNotification } from "../masterpage/notify/masternotification";
import { YeeFlowMasterPageUserInfo, YeeFlowNavigator, YeeFlowTitle, YeeFlowSettings, YeeFlowMenu } from "../yeeflowmasterpage";
import { MasterPageHomeManage } from "./materpage-home-manage";
import { MasterPageUserInfo } from "./userinfo";
import { MerchatAPI } from "../../api/masterpage/masterpage";
import { YeeofficeRemind } from "./yeeoffice-remind";

export interface YeeOfficeMasterPageProps {
    showHeader?: boolean;
    showMasterHeader?: boolean;
    languages?: string[]; //系统支持的语言，默认['zh', 'en']
    onChangeLanguage?: (language: string) => void;
    onLoaded?: (merchat?: MerchatLoginModel) => void;
    renderActions?: React.ReactNode | React.ReactNode[];
    isHomePage?: boolean;
    hideSyStemActions?: boolean;//隐藏masterpage的聊天，系统设置，用户信息按钮
}

export interface YeeOfficeMasterPageStates {
    loginFail?: boolean;
    isExpired?: boolean;
}

const mapStateToProps = (state) => {
    return {
        showHeader: state.masterpage.showHeader
    };
}

@connect(mapStateToProps)
export default class YeeOfficeMasterPage extends React.Component<YeeOfficeMasterPageProps, YeeOfficeMasterPageStates>{
    static defaultProps: YeeOfficeMasterPageProps = {
        showMasterHeader: true,
        languages: ['zh', 'en']
    };
    showHeader: boolean = true;
    constructor(props, context) {
        super(props, context)
        URLHelper.init();
        require("../../themes/masterpage.less");
        this.showHeader = this.props.showMasterHeader;
        if (AkContext.isMobileLoginIn()) {
            this.showHeader = false
        }
        this.state = {

        };
    }

    componentWillMount() {
        const { props: { onLoaded } } = this;
        let logainfail = false;
        AkGlobal.store.dispatch(MasterPageAction.triggerMasterpageHeader(this.showHeader));
        var loginData = MerchatAPI.merchatLogin((logainfail: boolean) => {
            logainfail = logainfail;
            this.setState({ loginFail: true });
        });
        if(logainfail) return;
        let enterpriseInfo = loginData.EnterpriseInfo
        if((enterpriseInfo && enterpriseInfo.Flag) == EnterpriseFlag.FreeComplete || (enterpriseInfo && enterpriseInfo.Flag) == EnterpriseFlag.SubscriptionExpired) {
            this.setState({isExpired: true});
        }
        MerchatAPI.getMerchatInfo()
        onLoaded && onLoaded(loginData);
        this.setCompanyName(loginData);
    }

    setCompanyName(loginData) {
        /**统一用公司名称*/
        if (AkContext.isNeteaseVersion()) {
            document.title = AkGlobal.intl.formatMessage({ id: MasterPageLocale.MasterTitleNetease });
        } else {
            document.title = loginData ? loginData.CompanyInfo.CompanyName : AkGlobal.intl.formatMessage({ id: MasterPageLocale.PageTitle });
        }
    }

    getLogo(logo: string) {
        if (logo) {
            return logo;
        } else {
            if (AkContext.isNeteaseVersion()) {
                return "images/logo-psm-w.png";
            } else if (!AkContext.isNewVersion() || AkContext.isYeeFlow()) {
                return "images/logo-psm.png";
            } else {
                return "images/yeeoffice-newlogo.png";
            }
            // if (!AkContext.isNewVersion()) {
            //     return "images/logo-psm.png";
            // } 
            // else {
            //     return "images/yeeoffice-newlogo.png";
            // }
        }
    }

    renderHeaderStyle() {
        const { isHomePage, hideSyStemActions } = this.props;
        const company = AkContext.getCompanyInfo();
        if (isHomePage || hideSyStemActions) {
            return {
                background: "rgb(255, 255, 255)",
                color: "#000",
                height: "56px",
                lineHeight: "56px"
            }
        } else {
            if (company) {
                return {
                    background: company.NavBarColour,
                    color: company.NavFontColour,
                    boxShadow: "0 0 5px 0 rgba(0,0,0,.3)"
                }
            } else {
                return {
                    boxShadow: "0 0 5px 0 rgba(0,0,0,.3)"
                }
            }
        }
    }

    render() {
        const { props: { languages, showHeader, isHomePage, hideSyStemActions } } = this;
        const { loginFail, isExpired } = this.state;
        let show = true;
        if (!this.showHeader) {
            show = false;
        }
        if (!showHeader) {
            show = false;
        }
        const company = AkContext.getCompanyInfo();
        const loginData = AkContext.getMerchantData();
        let user = AkContext.getUser();
        if(loginFail) {
            return <AccessDenied />
        } else if(isExpired) {
            return loginData?<YeeofficeRemind enterpriseInfo={loginData.EnterpriseInfo} tenantID={loginData.CompanyInfo.TenantID}/>:null
        } else {
            return loginData ? <AkLayout className="ak-react-masterpage">
            <AkLayout.Header style={show ? null : YeeFlowMasterPageStyle.disableHeaderStyle} className={(isHomePage ? "masterpage-home-header " : "") + "ak-react-masterpage-header noprint"} >
                <AkRow type="flex" justify="space-between" align="middle" style={this.renderHeaderStyle()}>
                    <AkCol xs={isHomePage ? 15 : 11} sm={13} md={16} lg={16} >
                        <YeeFlowMenu />
                        <YeeFlowTitle isHomePage={isHomePage} loginData={loginData} />
                    </AkCol>
                    {/* {isHomePage ? null : <AkCol xs={0} sm={0} md={8} lg={8} style={{ textAlign: "center" }}>
                        <img className="ak-master-logo" src={loginData && URLHelper.GetFileUrl(this.getLogo(loginData.CompanyInfo.Logo))} />
                    </AkCol>} */}

                    <AkCol xs={isHomePage ? 9 : 13} sm={11} md={8} lg={8}>
                        <AkRow type="flex" justify="end">
                            {(isHomePage || hideSyStemActions) && user && user["IsAdmin"] ? <AkCol>
                                <MasterPageHomeManage hideSyStemActions={hideSyStemActions} />
                            </AkCol> : null}
                            {AkUtil.isArray(this.props.renderActions) ?
                                (this.props.renderActions as React.ReactNode[]).map(item => {
                                    if ((this.props.renderActions as React.ReactNode[]).length > 0) {
                                        return <AkCol>
                                            {item}
                                        </AkCol>
                                    }
                                })
                                : (this.props.renderActions &&
                                    <AkCol>
                                        {this.props.renderActions}
                                    </AkCol>
                                )
                            }

                            {isHomePage || hideSyStemActions ? null : <AkCol>
                                <YeeFlowNavigator />
                            </AkCol>}
                            {!hideSyStemActions && <AkCol>
                                <MasterNotification></MasterNotification>
                            </AkCol>}
                            {!hideSyStemActions && <AkCol>
                                {
                                    user && !user["IsAdmin"] ? null :
                                        <YeeFlowSettings />
                                }
                            </AkCol>}
                            {
                                !hideSyStemActions && <AkCol>
                                    {/* {!AkContext.isNewVersion() && isHomePage ?
                                        <MasterPageUserInfo
                                            userInfo={loginData && loginData.UserModel}
                                            onChangeLanguage={this.props.onChangeLanguage} ></MasterPageUserInfo> : */}
                                    <YeeFlowMasterPageUserInfo
                                        languages={languages}
                                        enterpriseInfo={loginData && loginData.EnterpriseInfo}
                                        userInfo={loginData && loginData.UserModel}
                                        onChangeLanguage={(language) => {
                                            this.props.onChangeLanguage(language);
                                        }} />
                                    {/* } */}
                                </AkCol>
                            }
                        </AkRow>
                    </AkCol>
                </AkRow>
            </AkLayout.Header >
            <AkLayout className="ak-react-masterpage-content">
                <AkLayout.Content className="ak-master-content" style={show ? YeeFlowMasterPageStyle.contentStyle : null}>
                    <div style={YeeFlowMasterPageStyle.contentHeight(show)}>
                        {this.props.children}
                    </div>
                </AkLayout.Content>
            </AkLayout>
        </AkLayout > : null;
        }
        
    }
}

class YeeFlowMasterPageStyle {
    static disableHeaderStyle: React.CSSProperties = {
        display: "none",
    }
    static contentStyle: React.CSSProperties = {
        marginTop: 56,
    }
    static contentHeight = (showHeader: boolean) => {
        return {
            minHeight: showHeader ? "calc(100vh - 56px)" : "calc(100vh - 0px)",
            backgroundColor: "inherit"
        }
    };
}