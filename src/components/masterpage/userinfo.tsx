import * as React from "react";
import { Component, CSSProperties } from 'react';
import { FormattedMessage } from "react-intl";
import { AkPopover, AkRow, AkCol, AkButton, AkImg } from "../index";
import { MasterPageLocale } from "../../locales/localeid";
import { UserModel, LoginType } from '../../api/masterpage/masterpagemodel';
import { URLHelper } from "../../util/URLHelper";
import { AkMenu } from '../controls/ak-menu';
import { AkSwitch } from '../controls/ak-switch';
import { AkIcon } from '../controls/ak-icon';
import { AkRadio } from '../controls/ak-radio';
import { Cookies } from '../../util/cookie';
import { AkContext } from "../../index";
import UpdatePassword from './updatepassword';
import { AkGlobal } from '../../util/common';


export interface MasterPageUserInfoProps {
    userInfo?: UserModel;
    onChangeLanguage?: (language: string) => void;
}
export interface MasterPageUserInfoStates {
    language?: string;
    showUpdataPassword?: boolean;
}
export class MasterPageUserInfo extends Component<MasterPageUserInfoProps, MasterPageUserInfoStates> {

    constructor(props, context) {
        super(props, context);
        this.state = {
            language: AkContext.getLanguage(),
            showUpdataPassword: false,
        };
    }
    renderUserInfo() {
        const { userInfo } = this.props;
        const style: CSSProperties = {
            overflow: "hidden",
            textOverflow: "ellipsis",
            whiteSpace: "nowrap",
            maxWidth: "180px"
        }

        return <AkRow type="flex" justify="start" align="middle">
            <AkCol span={7}>
                <AkImg className="ak-master-user-imgbox" magnifier={false} lazyLoad={false}
                    src={userInfo && URLHelper.GetFileUrl(userInfo.Photo) || AkContext.getUserDefaultPhoto()} />
            </AkCol>
            <AkCol span={17}>
                <AkRow>
                    <AkCol >
                        <div style={style} title={userInfo && userInfo.Name_CN || userInfo.Name_EN} >{userInfo && userInfo.Name_CN || userInfo.Name_EN}</div>
                    </AkCol>
                </AkRow>
                <AkRow>
                    <AkCol>
                        <div style={style} title={userInfo && userInfo.Email}>  {userInfo && userInfo.Email}</div>
                    </AkCol>
                </AkRow>
                <AkRow>
                    <AkCol>
                        {

                            <AkButton className="ak-view-user-info" type="primary" onClick={() => {
                                AkContext.getBranch() !== AkContext.YeeOffice ? window.location.href = AkContext.getRootWebUrl() + "/sitePages/Pages/Apps.aspx#/userinfo" :
                                    window.location.href = "/Home/Index#/userinfo";
                            }}>
                                <FormattedMessage id={MasterPageLocale.ViewDetail}></FormattedMessage>
                            </AkButton>
                        }
                        {
                            AkContext.getAccountType() === LoginType.YeeOffice &&
                            <a className="ak-update-password" href="javascript:void(0)" onClick={() => { this.setState({ showUpdataPassword: true }) }}>
                                <div className="ak-update-password-word">
                                    {AkGlobal.intl.formatMessage({ id: MasterPageLocale.ChangePwd })}
                                </div>
                            </a>
                        }
                    </AkCol>
                </AkRow>
            </AkCol>
        </AkRow>;
    }
    handleSizeChange = (e) => {
        this.setState({ language: e.target.value });
        if ("onChangeLanguage" in this.props) {
            this.props.onChangeLanguage(e.target.value);
        }
    }
    renderFooter() {
        return <AkRow type="flex" justify="space-between">
            <AkCol>
                <AkButton onClick={() => {
                    window.location.href = AkContext.getCloseConnectionURI();
                }}>
                    <FormattedMessage id={MasterPageLocale.ChangeUser}></FormattedMessage>
                </AkButton>
            </AkCol>
            <AkCol>
                <AkButton onClick={() => {
                    window.location.href = AkContext.getSignOutURI();
                }}>
                    <FormattedMessage id={MasterPageLocale.Exit}></FormattedMessage></AkButton>
            </AkCol>
        </AkRow>;
    }
    render() {
        const style = AkContext.getBranch() === AkContext.YeeOffice ? { height: "56px" } : null;
        const { userInfo } = this.props;
        return <div className="ak-master-user"
            style={{ float: "right" }}
        // onClick={() => { this.setState({ showUserInfo: true }); }}
        >
            <AkPopover

                overlayClassName="ak-react-masterpage" trigger="click"
                getPopupContainer={() => document.getElementsByClassName("ak-master-user")[0] as HTMLElement}
                placement="bottomRight" title={this.renderUserInfo()}
                content={this.renderFooter()}>
                <AkRow className="ak-master-user-detail" style={style}>
                    <AkImg className="ak-master-user-img" magnifier={false} lazyLoad={false}
                        src={userInfo && URLHelper.GetFileUrl(userInfo.Photo) || AkContext.getUserDefaultPhoto()} />
                </AkRow>
            </AkPopover>
            {this.state.showUpdataPassword &&
                <UpdatePassword
                    onOk={() => this.setState({ showUpdataPassword: false })}
                    onCancel={() => this.setState({ showUpdataPassword: false })}
                />
            }
        </div>;
    }
} class MasterPageUserInfoStyle { }