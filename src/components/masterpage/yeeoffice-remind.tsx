import * as React from 'react';
import { AkModal, AkButton, AkRow, AkCol, AkBadge } from '..';
import { AkContext, AkGlobal, MerchantLevelEnum, EnterpriseFlag, SupportModel, EnterpriseInfo, CacheHelper, CommentAPI } from '../..';
import { MasterPageLocale } from '../../locales/localeid';
import { AkAlert } from '../controls/ak-alert';

export interface YeeofficeRemindProps {
    enterpriseInfo?: EnterpriseInfo;
    tenantID?: string;
}
 
export interface YeeofficeRemindState {
    supportInfo?: SupportModel;
}
 
export class YeeofficeRemind extends React.Component<YeeofficeRemindProps, YeeofficeRemindState> {
    constructor(context, props) {
        super(context, props);
        this.state = {
            supportInfo: { Mail: "business@akmii.com", Telephone: "400-1680-365", WorkHours: "9:00-18:00", InviteCode: "" }
        }
    }

    componentDidMount() {
        const { InviteCode } = this.props.enterpriseInfo;
        if (InviteCode) {
            var data = CacheHelper.getLocalStorage(AkContext.Keys.SupportInfo)
            if (data && data.InviteCode == InviteCode) {
                this.setState({ supportInfo: data })
            } else {
                CommentAPI.getSupportInfobyCode(InviteCode).then((data) => {
                    if (data.Status == 0 && data.Data) {
                        var supportData = JSON.parse(data.Data);
                        supportData["InviteCode"] = InviteCode;
                        this.setState({ supportInfo: supportData });
                        CacheHelper.setLocalStorage(AkContext.Keys.SupportInfo, supportData)
                    }
                });
            }
        }
    }
    renderEnterpriseInfo() {
        const { enterpriseInfo, tenantID } = this.props;
        let text = "";

        if (enterpriseInfo.Flag == EnterpriseFlag.Free) {
            text = AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeLevelNotBuy })
        } else {
            text = AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeLevelBuy })
        }
        switch (enterpriseInfo.Level) {
            case MerchantLevelEnum.Free:
                text += " : " + AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeLiteTitle });
                break;
            case MerchantLevelEnum.Enterprise:
                text += " : " + AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeEnterpriseTitle });
                break;
            case MerchantLevelEnum.Standard:
                text += " : " + AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeStandardTitle });
                break;
            case MerchantLevelEnum.Ultimate:
                text += " : " + AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeEnterpriseTitle });
                break;
            default:
                return null;

        }
        return <AkRow type="flex" justify="start" align="middle">
            <AkCol span={24}>
                <AkCol>
                    <AkBadge count={0} status="error" text={text} />
                </AkCol>
                <AkCol>
                    <AkBadge count={0} status="error" text={AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeCreated }) + " : " + enterpriseInfo.Created.substring(0, 10)} />
                </AkCol >
                <AkCol>
                    <AkBadge count={0} status="error" text={AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeExpireDate }) + " : " + enterpriseInfo.ExpireDate.substring(0, 10)} />
                </AkCol>
                <AkCol>
                    <AkBadge count={0} status="error" text={AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeUserMaxCount }) + " : " + enterpriseInfo.UserMaxCount} />
                </AkCol>
                <AkCol>
                    <AkBadge count={0} status="error" text={AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeSubscribeID }) + " : " + tenantID} />
                </AkCol>
            </AkCol>
        </AkRow>
    }
    renderEnterpriseInfoFooter() {
        let desc = AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeUpgrade },
            {phone:this.state.supportInfo.Telephone,
            email:this.state.supportInfo.Mail,
            worktime:this.state.supportInfo.WorkHours});
        // desc = desc.replace("{0}", this.state.supportInfo.Telephone);
        // desc = desc.replace("{1}", this.state.supportInfo.Mail);
        // desc = desc.replace("{2}", this.state.supportInfo.WorkHours);
        return <AkAlert
            message={AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeBuy })}
            description={desc}
            type="info"
            showIcon
        ></AkAlert>
    }
    
    render() { 
        return <AkModal
        title={AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeEnds })}
        visible={true}
        maskClosable={false}
        footer={
            [
                <AkButton type="primary" onClick={() => {
                    window.location.href = AkContext.getSignOutURI();
                }}>{AkGlobal.intl.formatMessage({ id: MasterPageLocale.Exit })}</AkButton>,
                // <AkButton type="primary" onClick={() => {
                //     AkMessage.info(AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeUpgrade }));
                // }}>{AkGlobal.intl.formatMessage({ id: MasterPageLocale.SubscribeBuy })}</AkButton>
            ]
        }
        closable={false}
    >
        {this.renderEnterpriseInfo()}
        {this.renderEnterpriseInfoFooter()
        }</AkModal>
    }
}
 

