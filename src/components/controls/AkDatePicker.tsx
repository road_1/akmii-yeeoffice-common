import * as React from "react";
import * as moment from "moment";
import { connect } from "react-redux";
import { AkDatetimeUtil, AkUtil, AkContext } from "../../util";
import { DatePicker } from "antd";
import { DatePickerProps } from "antd/lib/date-picker";

export interface AkPickerProps {
    prefixCls?: string;
    inputPrefixCls?: string;
    format?: string;
    disabled?: boolean;
    allowClear?: boolean;
    className?: string;
    style?: React.CSSProperties;
    popupStyle?: React.CSSProperties;
    locale?: any;
    size?: 'large' | 'small' | 'default';
    getCalendarContainer?: (triggerNode: Element) => HTMLElement;
    open?: boolean;
    onOpenChange?: (status: boolean) => void;
    disabledDate?: (current: moment.Moment) => boolean;
    renderExtraFooter?: () => React.ReactNode;
}
export interface AkSinglePickerProps {
    value?: string | moment.Moment;
    defaultValue?: string | moment.Moment;
    defaultPickerValue?: moment.Moment;
    onChange?: (dateString: string, date: moment.Moment) => void;
}

export interface TimePickerProps {
    className?: string;
    size?: 'large' | 'default' | 'small';
    value?: moment.Moment;
    defaultValue?: moment.Moment;
    open?: boolean;
    format?: string;
    onChange?: (time: moment.Moment, timeString: string) => void;
    onOpenChange?: (open: boolean) => void;
    disabled?: boolean;
    placeholder?: string;
    prefixCls?: string;
    hideDisabledOptions?: boolean;
    disabledHours?: () => number[];
    disabledMinutes?: (selectedHour: number) => number[];
    disabledSeconds?: (selectedHour: number, selectedMinute: number) => number[];
    style?: React.CSSProperties;
    getPopupContainer?: (triggerNode: Element) => HTMLElement;
    addon?: Function;
    use12Hours?: boolean;
    allowEmpty?: boolean;
    clearText?: string;
    defaultOpenValue?: moment.Moment;
    popupClassName?: string;
}
export interface AkDatePickerProps extends AkPickerProps, AkSinglePickerProps {
    className?: string;
    showTime?: TimePickerProps | boolean;
    showToday?: boolean;
    open?: boolean;
    toggleOpen?: (e: {
        open: boolean;
    }) => void;
    disabledTime?: (current: moment.Moment) => {
        disabledHours?: () => number[];
        disabledMinutes?: () => number[];
        disabledSeconds?: () => number[];
    };
    onOpenChange?: (status: boolean) => void;
    onOk?: (selectedTime: moment.Moment) => void;
    placeholder?: string;
    baseInfo?: CommonUserBaseInfo;
}
export interface AkMonthPickerProps extends AkPickerProps, AkSinglePickerProps {
    className?: string;
    placeholder?: string;
}

export declare type AkRangePickerValue = undefined[] | [moment.Moment] | [undefined, moment.Moment] | [moment.Moment, moment.Moment];
export declare type AkRangePickerRange = AkRangePickerValue | (() => AkRangePickerValue);
export interface AkRangePickerProps extends AkPickerProps {
    className?: string;
    value?: AkRangePickerValue;
    defaultValue?: AkRangePickerValue;
    defaultPickerValue?: AkRangePickerValue;
    onChange?: (dates: AkRangePickerValue, dateStrings: [string, string]) => void;
    onCalendarChange?: (dates: AkRangePickerValue, dateStrings: [string, string]) => void;
    onOk?: (selectedTime: moment.Moment) => void;
    showTime?: TimePickerProps | boolean;
    ranges?: {
        [range: string]: AkRangePickerRange;
    };
    placeholder?: [string, string];
    mode?: string | string[];
    disabledTime?: (current: moment.Moment, type: string) => {
        disabledHours?: () => number[];
        disabledMinutes?: () => number[];
        disabledSeconds?: () => number[];
    };
    onPanelChange?: (value?: AkRangePickerValue, mode?: string | string[]) => void;
}

export interface AkDatePickerState {
    value?: moment.Moment;
}

@connect((state) => ({ baseInfo: state.commonBaseInfo.baseInfo }))
export class AkDatePicker extends React.Component<AkDatePickerProps, AkDatePickerState> {
    static defaultProps: AkDatePickerProps = {
        format: "YYYY-MM-DD",
        getCalendarContainer: AkUtil.getPopupContainer,
    };
    static RangePicker?: any = DatePicker.RangePicker;
    static MonthPicker?: any = DatePicker.MonthPicker;
    // static WeekPicker:any=DatePicker.WeekPicker;

    private hasTime: boolean;

    constructor(props: AkDatePickerProps, context) {
        super(props, context);

        const value = ("value" in props) ? props.value : props.defaultValue;
        this.hasTime = AkDatetimeUtil.hasTime(props.format || props.baseInfo.DateFormat + " HH:mm" || AkDatetimeUtil.defaultFormatShort);

        this.state = {
            value: this.processValue(value)
        };
    }

    componentWillReceiveProps(nextProps: AkDatePickerProps) {
        let shouldProcess: boolean = false;

        if ("format" in nextProps && nextProps.format !== this.props.format) {
            const hasTime = AkDatetimeUtil.hasTime(nextProps.format);
            shouldProcess = hasTime === this.hasTime;
            if (!shouldProcess) this.hasTime = hasTime;
        }

        if (shouldProcess || ("value" in nextProps && nextProps.value !== this.props.value)) {
            this.setState({ value: this.processValue(nextProps.value) });
        }
    }

    processValue(value): moment.Moment {
        if (!value) return undefined;
        return this.hasTime ?
            AkDatetimeUtil.toUserTime(value) :
            AkDatetimeUtil.toMoment(value);
    }

    onChange = (date: moment.Moment, dateString: string) => {

        // let timeZone = this.props.baseInfo.ServerTimeZone;
        // if (AkContext.isYeeFlow()) {
        let timeZone = this.props.baseInfo.TimeZoneFe || this.props.baseInfo.ServerTimeZone;
        // }

        if (this.hasTime && date && timeZone) {
            //转换成服务器时区
            const response = AkDatetimeUtil.toServerTime(date, this.props.format)
            date = response.date;
            dateString = response.dateString;
        } else {
            dateString = date ? date.format(AkDatetimeUtil.defaultFormatShort) : "";
        }

        if (!("value" in this.props)) {
            this.setState({ value: date });
        }

        this.props.onChange && this.props.onChange(dateString, date);
    }

    render() {
        // tslint:disable-next-line:no-unused-variable
        const { defaultValue, value, onChange, ...others } = this.props;

        return <DatePickers value={this.state.value} onChange={this.onChange} {...others} />;
    }
}

export const DatePickers: React.Component<DatePickerProps, any> | any = DatePicker;
export class AkDatePickerStyle { }
