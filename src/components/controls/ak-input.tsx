import * as React from "react";
import { Component } from "react";
import { Input } from "antd";
import { AkIcon } from "./ak-icon";
import { AkConstants } from "../../util/common";
import { isNullOrUndefined } from "util";
import { AkTextArea } from "./AkTextArea";

export interface AkAutoSizeType {
    minRows?: number;
    maxRows?: number;
}

export interface AkInputFilterRule {
    type?: 'key' | 'excludeReg';
    filter?: (value) => string; //根据输入值，返回新值
    regexp?: RegExp;
}

export interface AkAbstractInputProps {
    prefixCls?: string;
    className?: string;
    defaultValue?: any;
    value?: any;
    // tabIndex?: number;
    style?: React.CSSProperties;
}
export interface AkInputProp extends AkAbstractInputProps {
    autosize?: boolean | AkAutoSizeType;
    style?: React.CSSProperties;
    allowClear?: boolean;
    defaultValue?: string;
    autoTrim?: boolean; //trim value
    rules?: (string | AkInputFilterRule)[];
    triggerChangeOnBlur?: boolean; //当blur的时候才触发change通知



    placeholder?: string;
    type?: string;
    id?: number | string;
    name?: string;
    size?: 'large' | 'default' | 'small';
    maxLength?: number | string;
    disabled?: boolean;
    readOnly?: boolean;
    addonBefore?: React.ReactNode;
    addonAfter?: React.ReactNode;
    // onPressEnter?: React.FormEventHandler<HTMLInputElement>;
    // onKeyDown?: React.FormEventHandler<HTMLInputElement>;
    onPressEnter?: React.FormEventHandler<any>;
    onKeyDown?: React.FormEventHandler<any>;
    // onKeyUp?: React.FormEventHandler<HTMLInputElement>;
    onChange?: (value) => void;
    // onClick?: React.FormEventHandler<HTMLInputElement>;
    // onFocus?: React.FormEventHandler<HTMLInputElement>;
    // onBlur?: React.FormEventHandler<HTMLInputElement>;
    onClick?: React.FormEventHandler<any>;
    onFocus?: React.FormEventHandler<any>;
    onBlur?: React.FormEventHandler<any>;
    autoComplete?: string;
    prefix?: React.ReactNode;
    suffix?: React.ReactNode;
    spellCheck?: boolean;
    autoFocus?: boolean;
}

export interface AkInputState {
    value?: string;
    isFocus?: boolean;
}

export class AkInput extends Component<AkInputProp,
    AkInputState> {
    static Group?: any = Input.Group;
    static Search?: any = Input.Search;
    static TextArea?: any = AkTextArea;

    input;

    static defaultProps: AkInputProp = {
        allowClear: true,
        autoTrim: true
    }

    getMaxLength() {
        const { type, maxLength } = this.props;
        if (maxLength !== undefined) {
            return maxLength;
        } else {
            return type === 'textarea' ? 2000 : 200;
        }
    }

    constructor(props, context) {
        super(props, context);

        this.state = {
            isFocus: false,
            value: ("value" in this.props)
                ? this.props.value
                : this.props.defaultValue

        };
    }


    timeoutHandle;

    componentWillUnmount() {
        if (this.timeoutHandle) {
            window.clearTimeout(this.timeoutHandle);
        }
    }

    componentWillReceiveProps(nextProps: AkInputProp) {
        if ("value" in nextProps && nextProps.value !== this.props.value && nextProps.value !== this.state.value) {
            this.setState({ value: nextProps.value })
        }
    }

    renderSuffix() {
        const { allowClear, suffix, disabled, readOnly } = this.props;
        const { value, isFocus } = this.state;

        if (suffix) {
            return suffix;
        } else {
            return allowClear && !readOnly && !disabled && isFocus
                ? <AkIcon
                    type="close-circle"
                    onClick={(e) => {
                        this.triggerChange(null, true);
                        // this.onBlur(null);
                    }}></AkIcon>
                : null;
        }
    }

    onChange(e) {
        const maxLength = this.getMaxLength();

        let toValue = e.currentTarget.value;

        const { value } = this.state;
        if (maxLength && maxLength > 0) {
            if (toValue && toValue.length > maxLength) {
                toValue = toValue.substr(0, maxLength);
                if (toValue === value) {
                    return;
                }
            }
        }

        this.triggerChange(toValue);
    }

    triggerChange(value, force?) {
        const { triggerChangeOnBlur, onChange } = this.props;
        //如果是blur后通知，change不触发
        if (force || triggerChangeOnBlur || !("value" in this.props)) {
            this.setState({ value: value });
        }
        if (onChange && (force || !triggerChangeOnBlur)) {
            if (!("value" in this.props) || this.props.value !== value) {
                //当外部值和内部值一致时，不触发onchange
                //Bug 49516: 流程中心-清除流程名称的输入内容，点击搜索，数据不显示
                value = isNullOrUndefined(value) ? "" : value;
                onChange(value);
            }
        }
    }

    onFocus(e) {
        this.setState({ isFocus: true }, () => {
            this.props.onFocus && this.props.onFocus(e)
        });
    }

    onBlur(e) {
        const { autoTrim, rules, onBlur, triggerChangeOnBlur } = this.props;
        const { value } = this.state;
        let toValue = value;
        if (value && (typeof value === 'string') && autoTrim) {
            toValue = value.trim();
        }
        if (toValue && rules) {
            rules.forEach(r => {
                let reg;
                if (typeof r === 'string') {
                    reg = this.getRegExpByType(r);
                } else {
                    //filter方法优先级高于表达式
                    if (r.filter) {
                        toValue = r.filter(toValue);
                    } else {
                        reg = r.regexp;
                        if (r.type !== 'excludeReg') {
                            reg = this.getRegExpByType(r.type);
                        }
                    }
                }

                if (reg) {
                    toValue = toValue.replace(reg, "");
                }
            })
        }

        if (toValue !== value || triggerChangeOnBlur) {
            this.triggerChange(toValue, triggerChangeOnBlur);
        }

        onBlur && onBlur(e);

        this.timeoutHandle = setTimeout(() => this.setState({ isFocus: false }), 300);
    }

    onKeyDown(e) {
        const { onKeyDown, type } = this.props;
        if (type !== "textarea") {
            if (e.keyCode === 13) {
                e.stopPropagation();
                e.preventDefault();
            }
        }
        onKeyDown && onKeyDown(e);
    }

    getRegExpByType(type) {
        let exp;
        if (type === 'key') {
            exp = AkConstants.RegExpFieldKey;
        }
        return exp;
    }

    focus() {
        this.input.focus();
    }

    render() {
        const { allowClear, onKeyDown, onChange, value, maxLength, autoTrim, rules, onBlur, onFocus, suffix, triggerChangeOnBlur, ...others } = this.props;
        return <Input ref={v => this.input = v}
            onFocus={this.onFocus.bind(this)}
            onBlur={this.onBlur.bind(this)}
            onChange={(e) => this.onChange(e)}
            onKeyDown={this.onKeyDown.bind(this)}
            suffix={suffix
                ? suffix
                : this.renderSuffix()}
            value={this.state.value}
            {...others} />
    }
}
