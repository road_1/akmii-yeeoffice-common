import * as React from "react";
import * as moment from "moment";
import { TimePicker } from "antd";
import { AkUtil } from "../../util";
import { TimePickerProps } from "antd/lib/time-picker";

export interface AkTimePickerProps {
    className?: string;
    size?: 'large' | 'default' | 'small';
    value?: moment.Moment;
    defaultValue?: moment.Moment | moment.Moment[];
    open?: boolean;
    format?: string;
    onChange?: (timeString: string, time: moment.Moment) => void;
    onOpenChange?: (open: boolean) => void;
    disabled?: boolean;
    placeholder?: string;
    prefixCls?: string;
    hideDisabledOptions?: boolean;
    disabledHours?: () => number[];
    disabledMinutes?: (selectedHour: number) => number[];
    disabledSeconds?: (selectedHour: number, selectedMinute: number) => number[];
    style?: React.CSSProperties;
    getPopupContainer?: (triggerNode: Element) => HTMLElement;
    addon?: Function;
    use12Hours?: boolean;
    focusOnOpen?: boolean;
    hourStep?: number;
    minuteStep?: number;
    secondStep?: number;
    allowEmpty?: boolean;
    clearText?: string;
    defaultOpenValue?: moment.Moment;
    popupClassName?: string;
}

export interface AkTimePickerStates {
    value?: moment.Moment;
}

export class AkTimePicker extends React.Component<AkTimePickerProps, AkTimePickerStates> {
    static defaultProps: AkTimePickerProps = {
        getPopupContainer: AkUtil.getPopupContainer,
        defaultOpenValue: moment().minutes(0).second(0),
    };

    constructor(props, context) {
        super(props, context);
        let value = ("value" in props) ? props.value : props.defaultValue;
        this.state = { value: this.processValue(value) };
    }

    componentWillReceiveProps(nextProps) {
        if ("value" in nextProps && nextProps.value !== this.props.value) {
            this.setState({ value: this.processValue(nextProps.value) });
        }
    }

    processValue(value): moment.Moment {
        let v;
        if (value && typeof value === "string") {
            if (value.length > 8) {
                //包含年1970-01-01 05:06:07这种场景
                v = moment(value, `YYYY-MM-DD $
                {this.props.format || "HH:mm:ss"}`);
            }
            else {
                v = moment(value, this.props.format || "HH:mm:ss");
            }
            if (!v.isValid()) {
                v = undefined;
            }
        } else if (typeof value === "string") {
            v = null;
        } else {
            v = value;
        }
        return v;
    }

    onChange(date: moment.Moment, dateString: string) {

        if (!("value" in this.props)) {
            this.setState({ value: date });
        }
        this.props.onChange && this.props.onChange(dateString, date);
    }

    render() {
        const { defaultValue, value, onChange, ...others } = this.props;

        return <TimePickers value={this.state.value} onChange={this.onChange.bind(this)} {...others}></TimePickers>
    }
}
export const TimePickers: React.Component<TimePickerProps, any> | any = TimePicker;
class AkTimePickerStyle { }
