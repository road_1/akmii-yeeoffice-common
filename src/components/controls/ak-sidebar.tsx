import * as React from "react";
import * as classNames from "classnames";
import { AkIcon } from "./ak-icon";

export interface AkSidebarProps {
    /**是否可见*/
    visible?: boolean;
    /** 标题*/
    title?: React.ReactNode | string;
    /** 是否显示右上角的关闭按钮*/
    closable?: boolean;
    /** 宽度*/
    width?: number;
    className?: string;
    wrapperClassName?: string;
    layout?: "left" | "right";
    onClose?: () => void;
}

export interface AkSidebarState {
    transition?: boolean;
    visible?: boolean;
}

export class AkSidebar extends React.Component<AkSidebarProps, AkSidebarState>{
    constructor(props, context) {
        super(props, context);
        this.state = {
            transition: props.visible,
            visible: props.visible
        };
    }
    static defaultProps = {
        layout: "right",
        closable: true,
        className: ""
    }

    componentWillReceiveProps(nextProps: AkSidebarProps) {
        if ("visible" in nextProps && nextProps.visible !== this.props.visible) {

            this.setState({ transition: false, visible: true });
            setTimeout(() => this.setState({ transition: true, visible: nextProps.visible }), nextProps.visible ? 0 : 300);
        }
    }

    render() {
        const { width, title, closable, layout, className, wrapperClassName } = this.props;
        const contentClass = classNames("ak-sidebar-content",
            { 'ak-sidebar-hasclose': closable },
            { "ak-sidebar-hastitle": title || closable }, className);
        const { transition, visible } = this.state;
        const boxClass = classNames("ak-sidebar", `ak-sidebar-${layout}`, { "ak-sidebar-transition": transition }, wrapperClassName)
        return visible ?
            <div className={boxClass} ref="sider " style={{ width: width }}>
                <div className={contentClass}>
                    {
                        (title || closable) && <div className="ak-sidebar-content-title">
                            {closable ? <div className="ak-sidebar-content-title-close-btn" onClick={() => {
                                if (this.props.onClose) this.props.onClose();
                            }}><AkIcon type="close" />
                            </div> : null}
                            <div className="ak-sidebar-content-title-text"

                            >{title}</div>
                        </div>
                    }
                    <div className="ak-sidebar-body">
                        {this.props.children}
                    </div>
                </div>
            </div> : null
    }
}
