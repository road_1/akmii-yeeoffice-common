import * as React from "react";
import * as PropTypes from "prop-types";
import * as classNames from "classnames";
import {Checkbox} from "antd";

export declare type AkCheckboxValueType = string | number;
export interface AkCheckboxOptionType {
    label: string;
    value: AkCheckboxValueType;
    disabled?: boolean;
}

export interface AkAbstractCheckboxGroupProps {
    prefixCls?: string;
    className?: string;
    options?: Array<AkCheckboxOptionType | string>;
    disabled?: boolean;
    style?: React.CSSProperties;
}

export interface AkCheckboxGroupProps extends AkAbstractCheckboxGroupProps {
    defaultValue?: Array<AkCheckboxValueType>;
    value?: Array<AkCheckboxValueType>;
    onChange?: (checkedValue: Array<AkCheckboxValueType>) => void;
}

export interface AkCheckboxGroupState {
    value: any;
}

export class AkCheckboxGroup extends React.Component<AkCheckboxGroupProps, AkCheckboxGroupState> {
    static defaultProps = {
        options: [],
        prefixCls: 'ant-checkbox-group',
    };

    constructor(props) {
        super(props);
        this.state = {
            value: props.value || props.defaultValue || [],
        };
    }

    static propTypes = {
        defaultValue: PropTypes.array,
        value: PropTypes.array,
        options: PropTypes.array.isRequired,
        onChange: PropTypes.func,
    };

    static childContextTypes = {
        checkboxGroup: PropTypes.any,
    };

    getChildContext() {
        return {
            checkboxGroup: {
                toggleOption: this.toggleOption,
                value: this.state.value,
                disabled: this.props.disabled,
            },
        };
    }

    componentWillReceiveProps(nextProps) {
        if ('value' in nextProps) {
            this.setState({
                value: nextProps.value || [],
            });
        }
    }

    toggleOption = (option) => {
        const optionIndex = this.state.value.indexOf(option.value);
        const value = [...this.state.value];
        if (optionIndex === -1) {
            value.push(option.value);
        } else {
            value.splice(optionIndex, 1);
        }
        if (!('value' in this.props)) {
            this.setState({value});
        }
        const onChange = this.props.onChange;
        if (onChange) {
            onChange(value);
        }
    }

    render() {
        const {props, state} = this;
        const {prefixCls, className, options} = props;
        let children = props.children;
        if (options && options.length > 0) {
            children = (options as Array<AkCheckboxOptionType>).map(option => {
                if (typeof option === 'string') {
                    return <Checkbox key={option}
                                     disabled={props.disabled}
                                     value={option}
                                     checked={state.value.indexOf(option) !== -1}
                                     onChange={() => this.toggleOption(option)}
                                     className={`${prefixCls}-item`}>{option}</Checkbox>
                } else {
                    return <Checkbox
                        key={option.value}
                        disabled={'disabled' in option ? option.disabled : props.disabled}
                        value={option.value}
                        checked={state.value.indexOf(option.value) !== -1}
                        onChange={() => this.toggleOption(option)}
                        className={`${prefixCls}-item`}
                    >
                        {option.label}
                    </Checkbox>
                }
            });
        }

        const classString = classNames(prefixCls, className);
        return (
            <div className={classString}>
                {children}
            </div>
        );
    }
}