import * as React from 'react'
import { Component } from 'react'
import * as QRCode from 'qrcode.react';
export interface AkQRCodeProps{
    value:string;
    renderAs?:'canvas'|'svg';//default canvas
    size?:number;//default 128
    bgColor?:string;//(CSS color) #FFFFFF
    fgColor?:string;//(CSS color) #000000
    level?:'L'|'M'|'Q'|'H';//default 'L'
}
export interface AkQRCodeStates{}
export class AkQRCode extends Component<AkQRCodeProps, AkQRCodeStates> {
    render() {
        return <QRCode {...this.props} > </QRCode>
    }
}
