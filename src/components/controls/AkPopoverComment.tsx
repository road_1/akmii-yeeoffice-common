import * as moment from "moment";
import * as React from "react";
import { connect } from 'react-redux';
import { AkInfiniteScroll } from './AkInfiniteScroll';
import { CommentAction } from '../../actions/index';
import { CommentAddRequest, CommentDeleteRequest, GetCommentRequest } from "../../api/comment/commentmodel";
import { AkContext, AkPopover, AkPopoverProp } from "../../index";
import { AkCommentsLocale, CommonLocale } from '../../locales/localeid';
import { CommentHolder, CommentReducerUtil } from '../../reducers/commentReducer';
import { AkGlobal } from '../../util/common';
import { AkButton } from "./ak-button";
import { AkInput } from "./ak-input";
import { AkMessage } from './ak-message';
import { AkNotification } from "./ak-notification";
import { AkPopconfirm } from './ak-popconfirm';
import { AkRow } from "./ak-row";
import { AkSpin } from './ak-spin';

export interface AkPopoverCommentProps extends AkPopoverProp {
    categoryId: string,//类别ID
    articleId: string,//主题ID
    pageSize?: number,//分页条数
    appKeys: string,
    disabled?: boolean,//是否禁用评论
    isDesign?: boolean;//设计布局时
    commentHolder?: CommentHolder; //redux 中的缓存对象
    onCommentLoaded?: (count: number) => void; //comments加载后回调，当前回调holder中的comments数量，不代表所有数据
}
export interface AkPopoverCommentState {
    content?: string,
    popoverVisible?: boolean;
}

const mapStateToProps = (state, props: AkPopoverCommentProps) => {
    let rs: CommentHolder;
    if (props.appKeys && props.articleId) {
        let key = CommentReducerUtil.getKey(props.appKeys, props.categoryId, props.articleId);
        rs = state.commentReducer.commentDict[key];
    }
    return {
        commentHolder: rs
    }
}

@connect(mapStateToProps)
export class AkPopoverComment extends React.Component<AkPopoverCommentProps, AkPopoverCommentState> {

    submitting = false; //管理提交状态
    fm = AkGlobal.intl.formatMessage;

    constructor(props, context) {
        super(props, context)

        this.state = { popoverVisible: true }
    }

    //删除评论
    onDelComment(item) {
        let request: CommentDeleteRequest = {
            ID: item.ID,
            DataID: this.props.articleId,
            DataType: this.props.categoryId,
            appKey: this.props.appKeys
        }
        AkGlobal.store.dispatch(CommentAction.delete(request));
        this.handleVisibleChange(true);
    }

    //点击更多事件
    onHandleMore(props) {

        if (props.isDesign === true) {
            return;//设计的时候不需要请求
        }
        let request: GetCommentRequest = {
            dataType: props.categoryId,
            dataID: props.articleId,
            pageSize: props.pageSize,
            parentID: 0,
            appKey: props.appKeys
        }
        AkGlobal.store.dispatch(CommentAction.more(request));
        // this.onLoadList(false);
    }

    //富文本框onchange事件
    onChangeContent(content) {
        this.setState({ content: content });
    }

    //提交评论
    onSaveComment() {
        let content = this.state.content;
        if (content) {
            content = content.trim();
        }
        if (content === null || content === undefined || content === "") {
            AkNotification.warning({
                message: this.fm({ id: CommonLocale.Tip }),
                description: this.fm({ id: AkCommentsLocale.RegContent })
            });
            return;
        }

        if (this.submitting) {
            return;
        }
        this.submitting = true;

        const onComplete = (success) => {
            if (success) {
                this.setState({ content: "" })
                AkMessage.success(this.fm({ id: AkCommentsLocale.MsgCommentSuccess }));
                this.onHandleMore(this.props);
            } else {
                AkNotification.warning({
                    message: this.fm({ id: CommonLocale.Tip }),
                    description: this.fm({ id: AkCommentsLocale.RegContent })
                });
            }
            this.submitting = false;
        }

        let request: CommentAddRequest = {
            DataType: this.props.categoryId,
            DataID: this.props.articleId,
            ContentText: content,
            ParentID: 0,
            TargetID: 0,
            appKey: this.props.appKeys,
            onComplete: onComplete
        }

        AkGlobal.store.dispatch(CommentAction.add(request));
    }

    confirmOpen: boolean;

    onConfirmVisibleChange(visible) {
        this.confirmOpen = visible;
    }

    //渲染一条评论
    renderComment(item, key) {
        const userid = item.User.UserID;
        let isAdmin = false;
        let user = AkContext.getUser();
        if (user.AccountID == userid) {
            isAdmin = true;
        }
        return <AkRow key={key} className="comment-main">
            <div className="portrait">
                <img src={item.User.Photo || AkContext.getUserDefaultPhoto()} />
            </div>
            <div className="content">
                <div>
                    <span className="name">{item.User.Name}</span>
                    <span className="time">{moment(item.CreatedStr).fromNow()}</span>
                    {isAdmin
                        ?
                        <AkPopconfirm onVisibleChange={this.onConfirmVisibleChange.bind(this)} title={this.fm({ id: CommonLocale.DeleteConfirm })} onConfirm={this.onDelComment.bind(this, item)}>
                            <span className="delete">
                                {this.fm({ id: CommonLocale.Delete })}
                            </span>
                        </AkPopconfirm>
                        :
                        null
                    }
                </div>
                <div>
                    <pre>
                        {item.ContentText}
                    </pre>
                </div>
            </div>
        </AkRow>
    }

    renderTitle() {
        return <div className="comment-title">
            {this.fm({ id: AkCommentsLocale.Comments })}
            <AkButton className="close" size="small" onClick={() => { this.handleVisibleChange(false) }}>{this.fm({ id: CommonLocale.Close })}</AkButton>
        </div>
    }

    //渲染所有评论
    renderCommentList() {
        const ch = this.props.commentHolder;
        let list = [];
        let loading = true;
        if (ch) {
            list = ch.data;
            loading = ch.status === "loading";
        }

        return <div className="comment-list">
            <AkInfiniteScroll
                initialLoad={false}
                pageStart={0}
                loadMore={this.onHandleMore.bind(this, this.props)}
                hasMore={true}
                useWindow={false}
                loader={<div key="more" className="infinite-loader"><AkSpin spinning={loading} /></div>}
            >
                {
                    list.map((item, index) => {
                        return this.renderComment(item, index)
                    })
                }
            </AkInfiniteScroll>
        </div>
    }

    textInput;

    renderEditor() {
        let { isDesign } = this.props;
        return <div className="comment-input">
            <AkInput ref={r => this.textInput = r} autoFocus={true} onPressEnter={this.onSaveComment.bind(this)} value={this.state.content} onChange={this.onChangeContent.bind(this)} addonAfter={<AkButton disabled={isDesign}
                onClick={this.onSaveComment.bind(this)}>{this.fm({ id: AkCommentsLocale.Send })}</AkButton>} />
        </div>
    }

    renderContent() {
        return <div>
            {this.renderCommentList()}
            {this.renderEditor()}
        </div>
    }

    //渲染更多按钮
    renderMore() {
        const ch = this.props.commentHolder;
        let loading = true;
        if (ch) {
            loading = ch.status === "loading";
        }

        return <div className="comment-more">
            <AkButton loading={loading}
                onClick={this.onHandleMore.bind(this)}>{this.fm({ id: AkCommentsLocale.ClickMore })}</AkButton>
        </div>;
    }

    componentDidMount() {
        this.onHandleMore(this.props);
        this.setState({ popoverVisible: false });
        // this.requestComments(this.props);
    }

    // requestComments(props: AkPopoverCommentProps) {
    //     if (props.isDesign === true) {
    //         return;//设计的时候不需要请求
    //     }

    //     let request: GetCommentRequest = {
    //         dataType: props.categoryId,
    //         dataID: props.articleId,
    //         lastID: "0",
    //         pageSize: props.pageSize,
    //         parentID: 0,
    //         appKey: props.appKeys
    //     }
    //     AkGlobal.store.dispatch(CommentAction.more(request));
    // }

    componentWillReceiveProps(nextProps: AkPopoverCommentProps) {
        if (("articleId" in nextProps && this.props.articleId !== nextProps.articleId) || ("appKeys" in nextProps && this.props.appKeys !== nextProps.appKeys)) {
            this.onHandleMore(nextProps);
            // this.requestComments(nextProps);
        }

        if ("commentHolder" in nextProps && nextProps.commentHolder !== this.props.commentHolder && "onCommentLoaded" in nextProps) {
            nextProps.onCommentLoaded(nextProps.commentHolder.data.length);
        }
    }

    handleVisibleChange(popoverVisible) {
        if (!popoverVisible) {
            if (this.confirmOpen) {
                //删除confirm弹出时，不隐藏comments
                return;
            }
        }
        this.popupVisible(popoverVisible);
        // setTimeout(() => {
        //     this.popupVisible(popoverVisible);
        // }, 100);
    }

    popupVisible(popoverVisible) {
        this.setState({ popoverVisible }, () => {
            if (popoverVisible) {
                this.textInput && this.textInput.focus();
            }
        });
    }

    render() {
        const { children, categoryId, articleId, pageSize, appKeys, disabled, isDesign, commentHolder, onCommentLoaded, ...others } = this.props;


        let ch = children;
        // if (commentHolder && commentHolder.data.length > 0 && hasDataClassName) {
        //     ch = <span className={hasDataClassName}>{children}</span>
        // } else {
        //     ch = <span>{children}</span>;
        // }

        return <AkPopover {...others} overlayClassName="ak-popover-comment" visible={this.state.popoverVisible} onVisibleChange={this.handleVisibleChange.bind(this)} title={this.renderTitle()} content={this.renderContent()}>
            {ch}
        </AkPopover>
    }
}

