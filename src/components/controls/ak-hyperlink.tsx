import * as React from "react";
import { AkSelect } from "./ak-select";
import { AkInput, AkInputProp } from "./ak-input";
import { isNullOrUndefined } from "util";

export interface AkHyperLinkProps extends AkInputProp {
    value?: string;
    defaultValue?: string;
    onChange?: (value) => void;
    addonBefore?: string[];
    addonAfter?: string[];
}

export interface AkHyperLinkStates {
    value?: string;
    beforeValue?: string;
    afterValue?: string;
}

export default class AkHyperLink extends React.Component<AkHyperLinkProps, AkHyperLinkStates>{
    static defaultProps: AkHyperLinkProps = {
        addonBefore: ["https://", "http://"],
        style: { width: "100%" }
    };

    constructor(props, context) {
        super(props, context);
        this.state = {
            value: null,
            beforeValue: props.addonBefore && props.addonBefore.length > 0 ? props.addonBefore[0] : null,
            afterValue: props.addonAfter && props.addonAfter.length > 0 ? props.addonAfter[0] : null,
        };
    }

    componentDidMount() {
        this.valueChangeHandler("value" in this.props ? this.props.value : this.props.defaultValue);
    }

    componentWillReceiveProps(nextProps: AkHyperLinkProps) {
        if ("value" in nextProps && nextProps.value !== this.props.value) {
            this.valueChangeHandler(nextProps.value);
        }
    }

    onChange(before, input, after) {
        if (this.props.onChange) {
            let value = (before || "") + (input || "") + (after || "");
            if (value === "https://" || value === "http://") {
                 // this.props.onChange(null); //打开申请页就提示验证，(暂无有效处理方法)
                if(this.props.value){
                    this.props.onChange(null); 
                }
            } else {
                this.props.onChange(value);
            }
        }
    }

    valueChangeHandler(value) {
        const { addonBefore, addonAfter } = this.props;
        let inputValue = value;
        let before = this.state.beforeValue;
        let after = this.state.afterValue;
        if (!value) {
            this.setState({ value: null });
        } else {
            if (addonBefore && addonBefore.length > 0) {
                addonBefore.forEach(item => {
                    if (value.startsWith(item)) {
                        before = item;
                        inputValue = inputValue.substr(item.length, inputValue.length - item.length);
                    }
                });
            }

            if (addonAfter && addonAfter.length > 0) {
                addonAfter.forEach(item => {
                    if (value.endsWith(item)) {
                        after = item;
                        inputValue = inputValue.substr(0, inputValue.length - item.length);
                    }
                });
            }

            this.setState({
                value: inputValue,
                beforeValue: before,
                afterValue: after
            });
        }

        this.onChange(before, inputValue, after);
    }

    renderBefore() {
        const { state: { beforeValue, value, afterValue }, props: { addonBefore, readOnly, disabled } } = this;
        if (!addonBefore || addonBefore.length === 0) return;

        return <AkSelect className="ak-hyperlink-addonselect" disabled={readOnly || disabled} value={beforeValue}
            onChange={v => {
                this.setState({ beforeValue: v as string });
                this.onChange(v, value, afterValue);
            }}>
            {addonBefore.map(item =>
                <AkSelect.Option value={item} key={item}>{item}</AkSelect.Option>)}
        </AkSelect>;
    }

    renderAfter() {
        const { state: { afterValue, value, beforeValue }, props: { addonAfter, readOnly, disabled } } = this;
        if (!addonAfter || addonAfter.length === 0) return;

        return <AkSelect className="ak-hyperlink-addonselect" disabled={readOnly || disabled} value={afterValue}
            onChange={v => {
                this.setState({ afterValue: v as string });
                this.onChange(beforeValue, value, v);
            }}>
            {addonAfter.map(item =>
                <AkSelect.Option value={item} key={item}>{item}</AkSelect.Option>)}
        </AkSelect>;
    }

    render() {
        const { state: { value } } = this;
        const { props: { readOnly, disabled, placeholder, style, className, maxLength } } = this;
        let tempMaxLength=isNullOrUndefined(maxLength)?1000:maxLength; 
        return <AkInput
            onChange={v => this.valueChangeHandler(v)}
            addonBefore={this.renderBefore()}
            addonAfter={this.renderAfter()}
            value={value}
            readOnly={readOnly}
            disabled={disabled}
            placeholder={placeholder}
            style={style}
            className={className}
            maxLength={tempMaxLength}
        />;
    }
}