import * as React from "react";
import { Select } from "antd";
import { AkUtil } from "../../util";

export interface AkAbstractSelectProps {
    prefixCls?: string;
    className?: string;
    size?: "default" | "large" | "small";
    notFoundContent?: React.ReactNode | null;
    transitionName?: string;
    choiceTransitionName?: string;
    showSearch?: boolean;
    allowClear?: boolean;
    // autoFocus?:boolean;
    disabled?: boolean;
    style?: React.CSSProperties;
    placeholder?: string;
    defaultActiveFirstOption?: boolean;
    dropdownClassName?: string;
    dropdownStyle?: React.CSSProperties;
    dropdownMenuStyle?: React.CSSProperties;
    onSearch?: (value: string) => any;
    filterOption?: boolean | ((inputValue: string, option: React.ReactElement<AkOptionsProps>) => any);
    optionsIncludeDestroyed?: boolean;
}

export interface AkLabeledValue {
    key: string;
    label: React.ReactNode;
}

export type AkSelectValue = string | any[] | AkLabeledValue | AkLabeledValue[];

export interface AkSelectProps extends AkAbstractSelectProps {
    value?: AkSelectValue;
    defaultValue?: AkSelectValue;
    mode?: "default" | "multiple" | "tags" | "combobox";
    multiple?: boolean;
    tags?: boolean;
    combobox?: boolean;
    optionLabelProp?: string;
    onChange?: (value: AkSelectValue) => void;
    onSelect?: (value: AkSelectValue, option: Object) => any;
    onDeselect?: (value: AkSelectValue) => any;
    onBlur?: () => any;
    onFocus?: () => any;
    // onInputKeyDown?: (e: React.KeyboardEvent<HTMLInputElement>) => void;
    // maxTagCount?: number;
    // maxTagPlaceholder?: React.ReactNode | ((omittedValues: AkSelectValue[]) => React.ReactNode);
    dropdownMatchSelectWidth?: boolean;
    optionFilterProp?: string;
    defaultActiveFirstOption?: boolean;
    labelInValue?: boolean;
    getPopupContainer?: (triggerNode: Element) => HTMLElement;
    tokenSeparators?: string[];
    getInputElement?: () => React.ReactElement<any>;
    children?: React.ReactNode;
}

export interface AkOptionsProps {
    disabled?: boolean;
    value?: any;
    /**选中该 Option 后，Select 的 title */
    title?: string;
    children?: React.ReactNode;
}

export interface AkOptGroupProps {
    label?: string | React.ReactElement<any>;
}
export interface AkSelectStates {
    value?: AkSelectValue;
}

export function getValuePropValue(child) {
    const props = child.props;
    if ("value" in props) {
        return props.value;
    } 
    if ("data" in props) {
        return props.data;
    } 
    if (child.key) {
        return child.key;
    } 
    if (child.type && child.type.isSelectOptGroup && props.label) {
        return props.label;
    }
    throw new Error(`Need at least a key or a value or a label (only for OptGroup) for ${child}`);
}

export function getOptionValue(item: string | AkLabeledValue) {
    if (typeof item === "string") return item;
    return (item as AkLabeledValue).key;
}

export class AkSelect extends React.Component<AkSelectProps, AkSelectStates> {
    static defaultProps: AkSelectProps = {
        defaultActiveFirstOption: false,
        getPopupContainer: trigger => (trigger.closest(".ant-modal-content") || document.body) as HTMLElement,
    };
    static Option: any = Select.Option;
    static OptGroup: any = Select.OptGroup;

    shouldCache: boolean;
    multiple: boolean;
    options: AkSelectValue[]; // Cache all options
    removedValue: AkSelectValue; // Cache removed value

    constructor(props: AkSelectProps, context) {
        super(props, context);
        const { labelInValue, children } = props;
        const value = ("value" in props) ? props.value : props.defaultValue;

        this.shouldCache = (!props.mode || props.mode === "default") && props.optionsIncludeDestroyed;
        this.multiple = (props.mode && props.mode === 'multiple') || props.multiple;

        if (this.shouldCache) {
            this.options = [];
            this.cacheOptions(props.children);
            this.removeNotInclude(value, true);
        }

        this.state = { value };
    }

    componentWillReceiveProps(nextProps: AkSelectProps) {
        if ("children" in nextProps && nextProps.children !== this.props.children) {
            if (this.shouldCache) {
                this.options = [];
                this.cacheOptions(nextProps.children);
                this.removeNotInclude(nextProps.value || this.state.value, true);
            }
        }

        if ("value" in nextProps && nextProps.value !== this.props.value) {
            if (this.shouldCache) this.removeNotInclude(nextProps.value, true);
            this.setState({ value: nextProps.value });
        }
    }

    cacheOptions(children) {
        React.Children.forEach(children, (child: any) => {
            if (child.type.isSelectOptGroup) {
                this.cacheOptions(child.props.children);
            } else {
                this.options.push(getValuePropValue(child));
            }
        });
    }

    removeNotInclude(value: AkSelectValue, delay: boolean = false) {
        if (!this.shouldCache) return;

        let needUpdate = false;
        const arrValue = [];

        if (this.removedValue) {
            AkUtil.each(AkUtil.toArray(this.removedValue), (item) => {
                const optionValue = getOptionValue(item);
                if (this.options && this.options.indexOf(optionValue) !== -1) {
                    needUpdate = true;
                    arrValue.push(item);
                }
            });
        } else {
            AkUtil.each(AkUtil.toArray(value), (item) => {
                const optionValue = getOptionValue(item);
                if (this.options && this.options.indexOf(optionValue) === -1) {
                    needUpdate = true;
                } else {
                    arrValue.push(item);
                }
            });
        }

        if (needUpdate) {
            this.removedValue = this.removedValue ? undefined : value;
            const newValue = this.multiple ? arrValue : (arrValue.length > 0 ? arrValue[0] : undefined);
            const func = () => this.onChange(newValue);
            delay ? setTimeout(func) : func();
        }
    }

    onChange(value: AkSelectValue, reset?: boolean) {
        if (!('value' in this.props)) this.setState({ value });
        if (reset) this.removedValue = undefined;
        this.props.onChange && this.props.onChange(value);
    }

    render() {
        // tslint:disable-next-line:no-unused-variable
        const { defaultValue, value, onChange, ...others } = this.props;
        let v = this.state.value === null ? undefined : this.state.value;
        return <Select value={v} onChange={value => this.onChange(value, true)} {...others} />;
    }
}
