import * as React from "react";
import { Component } from "react";
import { Popover } from "antd";
import { AkButton } from '../controls/ak-button';
import { AkAbstractTooltipProps } from "./ak-popconfirm";

export interface AkPopoverProp extends AkAbstractTooltipProps {
    title?: React.ReactNode;
    content?: React.ReactNode;
    // overlayClassName?: string;
}

export interface AkPopoverState { }

export class AkPopover extends Component<AkPopoverProp,
    AkPopoverState> {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return <Popover {...this.props}></Popover>;
    }
}
