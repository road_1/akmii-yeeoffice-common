import * as React from "react";
import { AkPopover, AkPopoverProp } from "../../index";
import { AkGlobal } from '../../util/common';
import { AkRow } from "./ak-row";
import { AkIcon } from './ak-icon';
import { SortableHandle, SortableContainer, SortableElement } from 'react-sortable-hoc';
import { AkUtil } from "../../util";

type TitleFunc = (item) => React.ReactNode;
type IDFunc = (item) => string;

export interface AkPopoverMenuProps extends AkPopoverProp {
    className?: string;
    source?: any[];
    allowSort?: boolean;
    onSortEnd?: (source) => void;
    onItemClick?: (item) => void;
    itemTitle?: string | TitleFunc; //property of title or method to render title
    itemID?: string | IDFunc; //id property or method to render title
    header?: React.ReactNode;
    footer?: React.ReactNode;
    ignoreItemClick?: boolean;
    getPopupContainer?: (triggerNode: Element) => HTMLElement;
}
export interface AkPopoverMenuState {
    popoverVisible?: boolean;
}

const DragHandle = SortableHandle(() => <AkIcon type="move1" className="icon-drag" />);
const SortContainer = SortableContainer(({ children }) => {
    return <div>{children}</div>;
});

export class AkPopoverMenu extends React.Component<AkPopoverMenuProps, AkPopoverMenuState> {

    fm = AkGlobal.intl.formatMessage;

    constructor(props, context) {
        super(props, context)
        this.state = { popoverVisible: false }
    }

    renderTitle(item) {
        const { itemTitle } = this.props;
        var content;
        if (itemTitle) {
            if (typeof itemTitle === "string") {
                content = item[itemTitle];
            } else {
                content = itemTitle(item);
            }
        } else {
            content = item + "";
        }
        return content;
    }

    getItemID(item) {
        const { itemID } = this.props;
        var id;
        if (itemID) {
            if (typeof itemID === "string") {
                id = item[itemID];
            } else {
                id = itemID(item);
            }
        } else {
            id = `item-${item}`;
        }
        return id;
    }

    onItemClick(item) {
        const { ignoreItemClick } = this.props;
        if (!ignoreItemClick) {
            this.handleVisibleChange(false);
            this.props.onItemClick && this.props.onItemClick(item);
        }
    }

    renderItem(item, i) {
        return <AkRow className="ak-popover-menu-item" key={i} onClick={() => this.onItemClick(item)}>{this.renderTitle(item)}</AkRow>;
    }

    onSortEnd({ oldIndex, newIndex }) {
        let { source, onSortEnd } = this.props;
        if (oldIndex !== newIndex && onSortEnd) {
            let dataSource = [...source];
            var removed = AkUtil.remove(dataSource, (item, i) => i === oldIndex);
            dataSource = AkUtil.insert(dataSource, removed[0], newIndex);
            onSortEnd(dataSource);
        }
    };

    renderContent() {
        const { source, allowSort, footer, header } = this.props;
        let content = null;

        if (source) {
            if (allowSort) {
                const SortItem = SortableElement(({ value }) => (
                    <div className="ak-popover-menu-item" onClick={() => this.onItemClick(value)}>
                        <div className="title-drag">{this.renderTitle(value)}</div>
                        <DragHandle />
                    </div>
                ));

                content = <SortContainer helperClass="draggable-helper" onSortEnd={this.onSortEnd.bind(this)} useDragHandle>
                    {source.map((value, index) => (
                        <SortItem key={this.getItemID(value)} index={index} value={value} />
                    ))}
                </SortContainer>
            } else {
                content = source.map((v, i) => {
                    return this.renderItem(v, i);
                });
            }
        }

        let foot = null, head = null;
        if (header) {
            head = <div key="header" className="header">{header}</div>
        }
        if (footer) {
            foot = <div key="footer" className="footer">{footer}</div>
        }
        return <div>{head}{content}{foot}</div>;
    }

    componentWillReceiveProps(nextProps: AkPopoverMenuProps) {
        if ("visible" in nextProps && nextProps.visible !== this.props.visible) {
            this.setState({ popoverVisible: nextProps.visible });
        }
    }

    handleVisibleChange(popoverVisible) {
        if ("visible" in this.props) {

        } else {
            this.popupVisible(popoverVisible);
        }

        this.props.onVisibleChange && this.props.onVisibleChange(popoverVisible);
    }

    popupVisible(popoverVisible) {
        this.setState({ popoverVisible });
    }

    render() {
        const { children, className, ...others } = this.props;
        let ch = children;
        let cl = "ak-popover-menu";
        if (className) {
            cl += " " + className
        }
        return <AkPopover {...others} overlayClassName={cl} visible={this.state.popoverVisible} onVisibleChange={this.handleVisibleChange.bind(this)} title={null} content={this.renderContent()}>
            {ch}
        </AkPopover>
    }
}

