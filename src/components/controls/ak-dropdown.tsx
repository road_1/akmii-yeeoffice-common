import * as React from 'react'
import { Component } from 'react'
import { Dropdown } from 'antd'

export interface AkDropDownProps {
    // trigger?: ('click' | 'hover' | 'contextMenu')[];
    trigger?: ('click' | 'hover')[];
    overlay: React.ReactNode;
    style?: React.CSSProperties;
    onVisibleChange?: (visible?: boolean) => void;
    visible?: boolean;
    disabled?: boolean;
    align?: Object;
    getPopupContainer?: (triggerNode?: Element) => HTMLElement;
    prefixCls?: string;
    className?: string;
    // transitionName?: string;
    placement?: 'topLeft' | 'topCenter' | 'topRight' | 'bottomLeft' | 'bottomCenter' | 'bottomRight';
    // forceRender?: boolean;
    
}
export interface AkDropDownStates { }
export class AkDropDown extends Component<AkDropDownProps,
    AkDropDownStates> {
    static Button?: any = Dropdown.Button;
    render() {
        return <Dropdown {...this.props}></Dropdown>
    }
}

class AkDropDownStyle { }
