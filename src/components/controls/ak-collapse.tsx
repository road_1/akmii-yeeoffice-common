import * as React from 'react'
import { Component } from 'react'
import { Collapse } from "antd";


export interface AkCollapseProps {
    activeKey?: Array<string> | string;
    defaultActiveKey?: Array<string>;
    /** 手风琴效果 */
    accordion?: boolean;
    onChange?: (key: string | string[]) => void;
    style?: React.CSSProperties;
    className?: string;
    bordered?: boolean;
    prefixCls?: string;
}
export interface AkCollapseStates { }
export class AkCollapse extends Component<AkCollapseProps, AkCollapseStates> {
    constructor(props, context) {
        super(props, context);
    }

    static Panel: any = Collapse.Panel;
    render() {
        return <Collapse {...this.props}>
            {this.props.children}
        </Collapse>
    }
}