import * as React from "react";
import { Component } from "react";
import { AkIcon } from ".";

export enum ParagraphAlign {
    Left = 0,
    Center = 1,
    Right = 2
}

export interface AkParagraphAlignProps {
    className?: string;
    onChange?: (value?: number) => void;
    value?: ParagraphAlign;
    defaultValue?: ParagraphAlign;
}

export interface AkParagraphAlignStates {
    value?: ParagraphAlign
}

export class AkParagraphAlign extends Component<AkParagraphAlignProps, AkParagraphAlignStates> {
    constructor(props, context) {
        super(props, context);
        this.state = {
            value: props.defaultValue || props.value || ParagraphAlign.Left
        };
    }

    componentWillReceiveProps(nextProps: AkParagraphAlignProps) {
        if ("value" in nextProps && nextProps.value !== this.props.value) {
            this.setState({ value: nextProps.value });
        }
    }

    onClick(v) {
        if (!("value" in this.props)) {
            this.setState({ value: v });
        }
        this.props.onChange && this.props.onChange(v);
    }

    render() {
        const { className } = this.props;
        const { value } = this.state;
        let cn = "ak-paragraph-align";
        if (className) {
            cn += " " + className
        }
        return <div className={cn}>
            <AkIcon onClick={() => this.onClick(ParagraphAlign.Left)} type="paragraphleft" className={value === ParagraphAlign.Left ? "selected" : ""} />
            <AkIcon onClick={() => this.onClick(ParagraphAlign.Center)} type="paragraphcenter" className={value === ParagraphAlign.Center ? "selected" : ""} />
            <AkIcon onClick={() => this.onClick(ParagraphAlign.Right)} type="paragraphright" className={value === ParagraphAlign.Right ? "selected" : ""} />
        </div>
    }
}