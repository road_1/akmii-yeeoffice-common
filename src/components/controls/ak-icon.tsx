import * as React from "react";
import { Icon } from "antd";

export interface AkIconProps {
    ref?: any;
    type: string;
    className?: string;
    title?: string;
    onClick?: React.MouseEventHandler<any>;
    spin?: boolean;
    style?: React.CSSProperties;
}


export const AkIcon = (props: AkIconProps) => {
    return <Icon {...props} />
};

