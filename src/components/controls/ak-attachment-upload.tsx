import * as React from 'react';
import { AkUpload } from './ak-upload';
import { AkButton } from './ak-button';
import { AkIcon } from './ak-icon';
import { UploadLocale, CommonLocale } from '../../locales/localeid';
import { AkGlobal, AkContext, AppKeys } from '../../util/common';
import { AkUploadProps } from './ak-upload';
import { FileUploadMethod } from '../../util/FileUploadMethod';
import {Request} from '../../util/request';
import {  LibraryModel } from "../../index";
import { FileUpLoadAPI } from '../../api/common/fileupload';
import { AkFileUploadItem } from '../../api/common/fileuploadmodel';
import { AkFile } from "./index";
import * as classNames from 'classnames';
import { AkUtil } from '../../util/util';
import { AkNotification } from './ak-notification';
import { FileUpLoadCommon } from '../../util/FileUpLoadCommon';

export interface AkAttachmentUploadProps{
    multiple?:boolean;
    disabled?:boolean;
    maxSize?: number;
    maxCount?: number;
    value?: AkFileUploadItem|AkFileUploadItem[];
    acceptExtensions?:string|string[];
    defaultValue?: AkFileUploadItem | AkFileUploadItem[];
    className?:string;
    listType?: "text" | "picture";
    onChange?:(value: AkFileUploadItem | AkFileUploadItem[])=>void;
}
export interface AkAttachmentUploadStatus{
    fileList?:AkFile[];
    value?:AkFileUploadItem | AkFileUploadItem[];
}
export class AkAttachmentUpload extends React.Component<AkAttachmentUploadProps,AkAttachmentUploadStatus>{
    static defaultProps: AkAttachmentUploadProps = {
        listType: "picture"
    };
    formatMessage = AkGlobal.intl.formatMessage;
    library:LibraryModel;
    constructor(props,context){
        super(props,context);
        let value = ("value" in props) ? props.value : props.defaultValue;
        this.state={
            value: value,
            fileList:this.parseAkFiles(value),
        }
    }
    componentDidMount(){
        this.getLibrary().then(d=>{
            this.library=d;
        });
    }
    componentWillReceiveProps(nextProps: AkAttachmentUploadProps) {
        if ("value" in nextProps && nextProps.value !== this.props.value) {
            this.setState({
                value: nextProps.value,
                fileList: this.parseAkFiles(nextProps.value)
            });
        }
    }
    parseAkFiles(value){
        if(value){
            if(Array.isArray(value)){
                return value.map((item,index)=>{
                    return this.parseAkFile(item,index);
                })
            }else{
                let arr=[];
                arr.push(this.parseAkFile(value,0))
                return arr;
            }
        }
        return [];
        
    }
    parseAkFile(item,index){
        let obj:AkFile={
            uid:index,
            name:item.fileName,
            status: 'done',
            type:AkUtil.getFileExt(item.fileName),
            size:item.fileSize,
            url:item.fileInfo.Url,
            fileKey:item.fileKey,
            thumbUrl :AkUtil.getThumbUrl(item.fileName)
        }  
        return obj;
    }
    getLibrary(){
        return new Promise((resolve,reject)=>{
            let code="";
            if(AkContext.getAppKey() === AppKeys.Flowcraft){
                code="flowcraft-file";
            }else {
                code = AkContext.getAppKey().toLowerCase() + "-file";
            }
            FileUpLoadAPI.getLibraryByCode({ Code: code }).then(d => {
                if(d.Status===0){
                    resolve(d.Data);
                 }else{                 
                    reject(d.Message)
                 }
            });
        });
    }
    customRequestHandler(options){
        const { file } = options;
        const {multiple} =this.props;
        const {fileList} = this.state;
        FileUploadMethod.beginUpload(options, this.library).then((d) => {
            if(d.Status===0){
                let downLoadUrl = d.Data;
                let obj:AkFile={
                    uid:fileList.length,
                    name:file.name,
                    status: 'done',
                    type:AkUtil.getFileExt(file.name),
                    size:file.size,
                    url:downLoadUrl,
                    fileKey:d.Message,
                    thumbUrl:AkUtil.getThumbUrl(file.name),
                }
                if(multiple){
                    fileList.push(obj);
                }else{
                    fileList[0]=obj;
                }
                this.onChange(fileList);
            }
        });
    }
    onChange(fileList){
        const { multiple } =this.props;
        if(multiple){
            const value:AkFileUploadItem[]=fileList.map(i=>{
                let obj:AkFileUploadItem={
                    fileKey: i.fileKey,
                    fileSize: i.size,
                    fileName: i.name,
                    fileInfo: {
                        fileKey:i.fileKey,
                        Url:i.url,
                    },
                }
                return obj;
            });
            this.props.onChange && this.props.onChange(value);
        }else{
            const i = fileList[0]
            let obj:AkFileUploadItem;
            if(i){
                obj = {
                    fileKey: i.fileKey,
                    fileSize: i.size,
                    fileName: i.name,
                    fileInfo: {
                        fileKey:i.fileKey,
                        Url:i.url,
                    },
                }
            }
            
            this.props.onChange && this.props.onChange(obj);
        }
        
        
    }
    onRemoveHandler(file){
        const {fileList} =this.state;
        if(this.props.multiple){
            AkUtil.remove(fileList,i=>i.uid === file.uid);
            this.onChange(fileList.map((item,index)=>{
                item.uid=index
                return item;
            }));
        }else{
            this.onChange([]);
        }
        
    }
    beforeUploadHandler(file:AkFile,fileLists?:AkFile[]) {
        const {maxSize,maxCount,acceptExtensions } =this.props;
        const {fileList} =this.state;
        let msg="";

        if (file.size === 0) {
            msg=this.formatMessage({ id: UploadLocale.MsgNoContentError }, { file: file.name });
            AkNotification.warning({ message: this.formatMessage({ id: CommonLocale.Tip }),description:msg});
            return false;
        }

        const defaultMaxSize: number = 1024 * 1024 * 10;
        const fileSize = maxSize === undefined || maxSize === null || maxSize >= defaultMaxSize ? defaultMaxSize : maxSize;
        if (file.size > fileSize) {
            msg=this.formatMessage({ id: UploadLocale.MsgFileSizeError },
                { value: AkUtil.bytesToSize(fileSize), file: file.name });
            AkNotification.warning({ message: this.formatMessage({ id: CommonLocale.Tip }),description:msg});
            return false;
        }
        if (FileUpLoadCommon.validateFileNameSize(file.name) === false) {
            msg= this.formatMessage({id:UploadLocale.FileNameMoreLarge},{name:file.name});
            AkNotification.warning({ message: this.formatMessage({ id: CommonLocale.Tip }),description:msg});
            return false;
        }
        if (FileUpLoadCommon.validateFileName(file.name) === false) {
            msg =this.formatMessage({id:UploadLocale.FileNameInvalidate},{name:file.name});
            AkNotification.warning({ message: this.formatMessage({ id: CommonLocale.Tip }),description:msg});
            return false;
        }

        if(acceptExtensions){
            if(Array.isArray(acceptExtensions)){
                if (acceptExtensions.indexOf(AkUtil.getFileExt(file.name)) === -1) {
                    msg = this.formatMessage( { id: UploadLocale.MsgFileTypeError }, { exts: acceptExtensions.join(', ') });
                    AkNotification.warning({ message: this.formatMessage({ id: CommonLocale.Tip }),description:msg});
                    return false;
                }
            }else if(AkUtil.getFileExt(file.name)!== acceptExtensions){
                msg = this.formatMessage( { id: UploadLocale.MsgFileTypeError }, { exts: acceptExtensions });
                AkNotification.warning({ message: this.formatMessage({ id: CommonLocale.Tip }),description:msg});
                return false;
            }
        }

        if (maxCount !== undefined && maxCount !== null) {
            if (Array.isArray(fileList) && (fileList.length + fileLists.length) > maxCount) {
                msg =this.formatMessage({ id: UploadLocale.MsgFileCountError },{ value: maxCount });
                AkNotification.warning({ message: this.formatMessage({ id: CommonLocale.Tip }),description:msg});
                return false;
            }
        }

        if (Array.isArray(fileList)) {
            if(fileList.filter(i=>i.name === file.name).length>0){
                msg =this.formatMessage( { id: UploadLocale.MsgFileExists }, { value: file.name });
                AkNotification.warning({ message: this.formatMessage({ id: CommonLocale.Tip }),description:msg});
                return false;
            }
        }

        return true;
    }
    onPreviewHandler(file:AkFile){
        if (AkContext.getBranch() !== AkContext.YeeOffice) {
            let token = AkContext.getToken();
            let url = file.url;
            token = encodeURIComponent(token);
            if (token && token !== undefined && token !== '' && token !== "undefined") {
                
                let akmiisecretIndex = url.lastIndexOf("&akmiisecret=") + 13;
                url = url.substring(0, akmiisecretIndex) + token;

            }
            url+=("&random="+AkUtil.guid()+"&IsImage=true");
            window.location.href=url;
        }else{
            window.location.href=file.url +"&IsImage=true";
        }
    }
    render(){
        const {disabled,className,listType,multiple} =this.props;
        const {fileList} =this.state;
        const akUploadProps: AkUploadProps={
            className: classNames(className, { "ant-upload-disabled": disabled }),
            onRemove:file => this.onRemoveHandler(file),
            beforeUpload:(file,fileLists)=>this.beforeUploadHandler(file,fileLists),
            customRequest:(options)=>this.customRequestHandler(options),
            onPreview:file=>this.onPreviewHandler(file),
            action:'',
            fileList,
            listType,
            multiple,
            disabled,
        }
        return <AkUpload {...akUploadProps}>
                {
                    disabled
                    ?
                    null
                    :
                    <AkButton icon="attachment">
                        {this.formatMessage({ id: UploadLocale.UploadAttachment })}
                    </AkButton>
                }
        </AkUpload>;
    }
}
