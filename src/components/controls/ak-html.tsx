import * as React from "react";
import {Component} from "react";

export interface AkHtmlProps {
}
export interface AkHtmlStates {}
export class AkHtml extends Component < AkHtmlProps,
AkHtmlStates > {
    constructor(props, context) {
        super(props, context);
    }
    replaceStr(str:string):string{
        return str?str.replace(/<\/script/g, '<\\/script').replace(/<!--/g, '<\\!--'):str;
    }
    render() {
        let temp=this.props.children as string;
        temp=this.replaceStr(temp);
        return <div dangerouslySetInnerHTML={{__html: temp}}></div >
    }
}
class AkHtmlStyle {}
