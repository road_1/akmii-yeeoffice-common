import * as React from 'react';
import { render } from "react-dom";
import { CommonLocale, EditorLocale } from '../../locales/localeid';
import { AkNotification } from "./ak-notification";
import { AkGlobal, AkContext, AppKeys } from '../../util/common';
import * as SparkMD5 from "spark-md5";
import * as superagent from "superagent";
// import * as $ from "jquery";
import * as wangeditor from 'wangeditor';
import { AkUtil } from '../../util/util';
import { Request } from '../../util/request';
import { FileUploadMethod } from '../../util/FileUploadMethod';
import { AkResponse, UploadLocale, LibraryModel, AkUploadFile, FileUploadError, AkIdentityPickerAdv } from "../../index";
import { AkModal } from '.';

export interface AkKindEditorProps {
    value?: string;
    type?: string;
    height?: number;
    width?: number;
    menus?: string[];
    onChange?: (value) => void;
    onUploadImgLoad?: (bool) => void;
    onchangeTimeout?: number;
    allowAT?: boolean;//允许@功能
    // blurTriggerChange?: boolean //onblur事件不触发
}

export interface AkKindEditorStates {
    visible?: boolean;
}

export default class AkKindEditor extends React.Component<AkKindEditorProps, any> {
    type?: string;
    menus?: string[]
    editor?: any;
    value?: string;
    formatMessage = AkGlobal.intl.formatMessage;

    // static defaultProps: AkKindEditorProps = {
    //     onchangeTimeout: 300
    // };

    constructor(props, context) {
        super(props, context);
        this.state = {
            visible: false,
        }
        this.value = this.RichTextRequired(this.props.value);
    }

    getMenu() {
        let menu = [];
        switch (this.type) {
            case "nobar":
                menu = [];
                break;
            case "simple":
                menu = ["head", 'bold', 'underline', 'italic', 'strikethrough', 'foreColor', 'backColor'];
                break;
            case "custom":
                menu = this.menus || [];
                break;
            case "":
            default:
                menu = [
                    'head',  // 标题
                    'bold',  // 粗体
                    'italic',  // 斜体
                    'underline',  // 下划线
                    'strikeThrough',  // 删除线
                    'foreColor',  // 文字颜色
                    'backColor',  // 背景颜色
                    'link',  // 插入链接
                    'list',  // 列表
                    'justify',  // 对齐方式
                    'quote',  // 引用
                    // 'emoticon',  // 表情
                    'image',  // 插入图片
                    'table',  // 表格
                    'video',  // 插入视频
                    'code',  // 插入代码
                    'undo',  // 撤销
                    'redo'  // 重复
                ]
                break;
        }
        return menu;
    }
    getLibrary() {
        return new Promise((resolve, reject) => {
            let getUrl = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net) + "/api/Library/code";
            let getRequest = {
                AppID: AkContext.getAppInfoID(),
                code: "ak-kindeditor",
            }
            new Request<any, any>().get(getUrl, getRequest).then(d => {
                if (d.Status === 0) {
                    // resolve(d.Data["LibraryID"]);
                    resolve(d.Data);
                } else {
                    reject(d.Message);
                }
            });
        });
    }
    beginUpload(file) {
        return new Promise((resolve, reject) => {
            let name = AkUtil.guid(); //文件名
            let size = file.size; //总大小

            const blobSlice = File.prototype.slice;
            const fileReader = new FileReader();
            fileReader.onload = (e) => {
                const spark = new SparkMD5();
                spark.append(fileReader.result);
                let md5 = spark.end();
                let postUrl = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net) + "/api/file/upload/begin";
                let postRequest = {
                    AppID: AkContext.getAppInfoID(),
                    MD5: md5,
                    FileName: name,
                    FileExtension: AkUtil.getFileExt(name),
                    Length: size,
                    ChunkSize: 1//写死只有一个块需要上传
                }
                new Request<any, any>().post(postUrl, postRequest).then(d => {
                    if (d.Status === 0) {
                        let obj = d.Data;
                        obj.MD5 = md5;
                        obj.Name = name;
                        resolve(obj);
                    } else {
                        reject(d.Message);
                    }
                })


            };
            fileReader.readAsDataURL(blobSlice.call(file, 0, file.size));
        });
    }
    chunkUpload(file, FileID) {
        return new Promise((resolve, reject) => {
            const blobSlice = File.prototype.slice;
            var fileForm = new FormData();
            fileForm.append("file", blobSlice.call(file, 0, file.size));
            const requestFiles = superagent.post(AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net) + "/api/file/upload/chunk?fileID=" + FileID);
            requestFiles.send(fileForm);
            requestFiles.end((error, response) => {
                if (response.ok) {
                    if (response.body.Status === 0) {
                        resolve(true);
                    } else {
                        reject();
                    }
                } else {
                    reject();
                }
            });
        });
    }
    endUpload(FileID) {
        let puUrl = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net) + "/api/file/upload/end";
        let putRequest = {
            fileID: FileID,
        }
        new Request<any, any>().put(puUrl, putRequest).then(d => {
            if (d.Status !== 0) {
                AkNotification.error({
                    message: this.formatMessage({ id: CommonLocale.Tip }),
                    description: this.formatMessage({ id: EditorLocale.UploadImageFail })
                });
            }
        })
    }
    getImageUrl(file, LibraryID, request) {
        return new Promise((resolve, reject) => {
            const postUrl = AkContext.getAppInfoAPI_URL(AppKeys.YeeOfficeDocument_Net) + "/api/document";
            const postRequest = {
                AppID: AkContext.getAppInfoID(),
                LibraryID: LibraryID,
                ParentID: "0",
                ContentLength: file.size,
                Name: request.Name,
                Extension: AkUtil.getFileExt(file.name),
                MD5: request.MD5,
                Type: 1,
                OverWrite: false,
            }
            new Request<any, any>().post(postUrl, postRequest).then(d => {
                if (d.Status === 0) {
                    resolve(d.Data);
                } else {
                    reject(d.Message);
                }
            })
        })
    }
    async uploadImage(files, insert) {
        const { formatMessage } = AkGlobal.intl;
        this.props.onUploadImgLoad && this.props.onUploadImgLoad(true);
        let library: LibraryModel = await this.getLibrary();
        files.forEach(async (file) => {
            let option: AkUploadFile = {
                file: file,
                folderPath: "",
                overwrite: true,
                status: 1,
                error: FileUploadError.Unknown,
                progress: 30,
                id: "",
                canceledUpload: false
            };

            FileUploadMethod.beginUpload(option, library).then((onfulfilled: AkResponse) => {
                let Url = onfulfilled.Data + "";
                Url += ("&IsImage=true&random=" + AkUtil.guid());
                insert(Url);
            }, (onreject: AkResponse) => {
                let msg = onreject.Message ? onreject.Message : formatMessage({ id: UploadLocale.MsgFileReadFail }, { file: file.name });
                AkNotification.error({
                    message: formatMessage({ id: CommonLocale.Tip }),
                    description: msg
                });
            });
        });
        this.props.onUploadImgLoad && this.props.onUploadImgLoad(false);
    }
    getContent(html) {
        let v = this.RichTextRequired(html);
        this.value = v;
        if (typeof this.props.onChange === "function") {
            this.props.onChange(v);
        }
    }

    /**富文本未填写，但里面有内容。用于richtext的必填验证 */
    RichTextRequired(value: string) {
        if (value === '<p class="MsoTocHeading"><br></p>') {
            return null;
        }
        return value
    }

    loadEditor() {
        const { allowAT } = this.props;
        const bars = this.refs.editorBars;
        const elem = this.refs.editorElem;
        const editor = new wangeditor(bars, elem);
        this.editor = editor;
        if ((bars["clientWidth"] || elem["clientWidth"]) < 550) {
            bars["style"]["width"] = "100%";
            elem["style"]["width"] = "100%";
        }
        //onblur 不触发
        // if (this.props.blurTriggerChange) {
        //     editor.customConfig.onblur = (html) => { this.getContent(html) };
        // } else {
        //     editor.customConfig.onchange = (html) => { this.getContent(html) };
        // }
        editor.customConfig.onchange = (html) => {
            this.getContent(html)
        };
        // editor.customConfig.onblur = (html) => { this.getContent(html) };

        if (this.props.onchangeTimeout) {
            editor.customConfig.onchangeTimeout = this.props.onchangeTimeout;
        }
        editor.customConfig.zIndex = 1;
        editor.customConfig.height = 200;
        //多语言
        editor.customConfig.lang = {
            '设置标题': this.formatMessage({ id: EditorLocale.MenuTitle }),
            '正文': this.formatMessage({ id: EditorLocale.EditorBody }),
            '链接文字': this.formatMessage({ id: EditorLocale.MenuLinkText }),
            '链接': this.formatMessage({ id: EditorLocale.MenuLink }),
            '上传图片': this.formatMessage({ id: EditorLocale.MenuUploadImage }),
            '创建': this.formatMessage({ id: EditorLocale.MenuCreate }),
            '行': this.formatMessage({ id: EditorLocale.MenuRow }),
            '的表格': this.formatMessage({ id: EditorLocale.MenuTable }),
            '文字颜色': this.formatMessage({ id: EditorLocale.MenuWordColor }),
            '背景色': this.formatMessage({ id: EditorLocale.MenuBgColor }),
            '设置列表': this.formatMessage({ id: EditorLocale.MenuList }),
            '有序列表': this.formatMessage({ id: EditorLocale.MenuOlList }),
            '无序列表': this.formatMessage({ id: EditorLocale.MenuUlList }),
            '对齐方式': this.formatMessage({ id: EditorLocale.MenuAlign }),
            '靠左': this.formatMessage({ id: EditorLocale.Menuleft }),
            '靠右': this.formatMessage({ id: EditorLocale.MenuRight }),
            '居中': this.formatMessage({ id: EditorLocale.MenuCenter }),
            '表情': this.formatMessage({ id: EditorLocale.MenuEmoji }),
            '手势': this.formatMessage({ id: EditorLocale.MenuGesture }),
            '网络图片': this.formatMessage({ id: EditorLocale.MenuOnlinePicture }),
            '插入表格': this.formatMessage({ id: EditorLocale.MenuInsertTable }),
            '插入视频': this.formatMessage({ id: EditorLocale.MenuVideo }),
            '插入代码': this.formatMessage({ id: EditorLocale.MenuCode }),
            '格式如': this.formatMessage({ id: EditorLocale.MenuFormat }),
            '列': this.formatMessage({ id: EditorLocale.MenuColumn }),
            '插入': this.formatMessage({ id: EditorLocale.MenuInsert }),
        }
        editor.customConfig.menus = this.getMenu();
        // 将图片大小限制为 4M
        editor.customConfig.uploadImgMaxSize = 4 * 1024 * 1024;
        // 限制一次最多上传 5 张图片
        editor.customConfig.uploadImgMaxLength = 5
        editor.customConfig.customUploadImg = (files, insert) => {
            this.uploadImage(files, insert);
        }
        //消息提示
        editor.customConfig.customAlert = info => {
            let pattern = new RegExp("\\【(.| )+?\\】", "igm");
            let name = info.match(pattern);
            name = name ? name : "";
            if (info.indexOf("不是图片") > 1) {
                AkNotification.error({
                    message: this.formatMessage({ id: CommonLocale.Tip }),
                    description: name + this.formatMessage({ id: EditorLocale.NotImage })
                });
            } else if (info.indexOf("大于") > 1) {
                AkNotification.error({
                    message: this.formatMessage({ id: CommonLocale.Tip }),
                    description: name + this.formatMessage({ id: EditorLocale.ExceedSize })
                });
            } else if (info.indexOf("最多上传") > 1) {
                AkNotification.error({
                    message: this.formatMessage({ id: CommonLocale.Tip }),
                    description: name + this.formatMessage({ id: EditorLocale.MaxCountImages })
                });
            }
        }
        editor.create();
        if (allowAT) {
            const toolbar = editor.$toolbarElem[0];
            const menu_AT = document.createElement("div");
            menu_AT.className = "w-e-menu";
            menu_AT.innerHTML = "<i>@</i>";
            menu_AT.onclick = () => {
                this.atUser = null;
                this.setState({ visible: true });
            }
            toolbar.appendChild(menu_AT);
        }
        editor.txt.html(this.value);
    }

    atUser;

    pushATUserIntoContext() {
        const { editor } = this;
        if (this.atUser) {
            var keys = Object.keys(this.atUser);
            if (keys.length > 0) {
                var content = "";
                for (var i = 0; i < keys.length; i++) {
                    const User = this.atUser[keys[i]];
                    content += '&nbsp;<em class="ak-comment-at" value="' + User.ID + '">@' + User.Name + '</em>&nbsp;';
                }
                editor.cmd.do('insertHTML', content);
            }
        }
    }

    renderSelectUserModal() {
        const { visible } = this.state;
        return visible && <AkModal
            visible
            onOk={() => {
                this.pushATUserIntoContext();
                this.setState({ visible: false });
            }}
            onCancel={() => this.setState({ visible: false })}

        >
            <AkIdentityPickerAdv multiple={true} onChange={(value) => { this.atUser = value }} />
        </AkModal>
    }

    componentWillReceiveProps(nextProps) {
        if ("value" in nextProps && this.value !== nextProps.value) {
            this.value = nextProps.value;
            this.editor.txt.html(this.value);
        }
        if ("type" in nextProps && this.props.type !== nextProps.type) {
            this.type = nextProps.type;
            this.loadEditor();
        }
        if ("menus" in nextProps && !AkUtil.isEqual(this.props.menus, nextProps.menus)) {
            this.menus = nextProps.menus;
            this.loadEditor();
        }
    }
    componentDidMount() {
        this.loadEditor();
    }
    render() {
        let { type, menus } = this.props;
        this.type = type;
        this.menus = menus;
        return <div className="ak-editor" style={{ overflowX: "auto" }}>
            {this.renderSelectUserModal()}
            <div ref="editorBars"></div>
            <div ref="editorElem" style={{ height: this.props.height || 300 }}></div>
        </div>;
    }
}
