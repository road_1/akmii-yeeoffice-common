import * as React from 'react';
import { Cascader } from "antd";
export interface AkCascaderOptionType {
    value: string;
    label: string;
    disabled?: boolean;
    children?: Array<AkCascaderOptionType>;
    // __IS_FILTERED_OPTION?: boolean;
}
export declare type AkCascaderExpandTrigger = 'click' | 'hover';
export interface AkShowSearchType {
    filter?: (inputValue: string, path: AkCascaderOptionType[]) => boolean;
    render?: (inputValue: string, path: AkCascaderOptionType[], prefixCls: string) => React.ReactNode;
    sort?: (a: AkCascaderOptionType[], b: AkCascaderOptionType[], inputValue: string) => number;
    matchInputWidth?: boolean;
}
export interface AkCascaderProps {
    /** 可选项数据源 */
    // options: any[];
    options: AkCascaderOptionType[];
    // /** 默认的选中项 */
    // defaultValue?: string[]|AkCascaderOptionType[]|any[];
    // /** 指定选中项 */
    // value?: AkCascaderOptionType[]|any[];
    /** 默认的选中项 */
    // defaultValue?: string[];
    defaultValue?: AkCascaderOptionType[];
    /** 指定选中项 */
    // value?: string[];
    value?: AkCascaderOptionType[];
    /** 选择完成后的回调 */
    onChange?: (value: string[], selectedOptions?: AkCascaderOptionType[]) => void;
    /** 选择后展示的渲染函数 */
    displayRender?: (label: string[], selectedOptions?: AkCascaderOptionType[]) => React.ReactNode;
    /** 自定义样式 */
    style?: React.CSSProperties;
    /** 自定义类名 */
    className?: string;
    /** 自定义浮层类名 */
    popupClassName?: string;
    /** 浮层预设位置:`bottomLeft` `bottomRight` `topLeft` `topRight` */
    popupPlacement?: string;
    /** 输入框占位文本*/
    placeholder?: string;
    /** 输入框大小，可选 `large` `default` `small` */
    size?: string;
    /** 禁用*/
    disabled?: boolean;
    /** 是否支持清除*/
    allowClear?: boolean;
    showSearch?: boolean | AkShowSearchType;
    notFoundContent?: React.ReactNode;
    loadData?: (selectedOptions?: AkCascaderOptionType[]) => void;
    /** 次级菜单的展开方式，可选 'click' 和 'hover' */
    expandTrigger?: AkCascaderExpandTrigger;
    /** 当此项为 true 时，点选每级菜单选项值都会发生变化 */
    changeOnSelect?: boolean;
    /** 浮层可见变化时回调 */
    onPopupVisibleChange?: (popupVisible: boolean) => void;
    prefixCls?: string;
    inputPrefixCls?: string;
    getPopupContainer?: (triggerNode?: HTMLElement) => HTMLElement;

    labelField?: string;
    valueField?: string;
    childrenField?: string;
    popupVisible?: boolean;
}
export interface AkCascaderStates {
    loading?: boolean;
}
export class AkCascader extends React.Component<AkCascaderProps, AkCascaderStates> {

    lookupOptions(item) {
        const { labelField, childrenField, valueField } = this.props;
        let children = null;
        if (item[childrenField || 'children'] && item[childrenField || 'children'].length > 0) {
            children = item[childrenField || 'children'].map((data) => {
                return this.lookupOptions(data)
            })
        }
        return {
            value: item[valueField || 'value'],
            children: children,
            label: item[labelField || 'label'],
            disabled: item['disabled']
        }
    }
    render() {
        const { options, labelField, childrenField, valueField, ...others } = this.props;
        let temp = options.map((data) => {
            return this.lookupOptions(data)
        })
        return <Cascader {...others} options={temp} />;
    }
}