import * as React from 'react'
import {Component} from 'react'
import {Badge} from 'antd';

export interface AkBadgeProps {
    /** Number to show in badge */
    count : number | string;
    showZero?: boolean;//当数值为 0 时，是否展示 Badge
    /** Max count to show */
    overflowCount?: number;
    /** whether to show red dot without number */
    dot?: boolean;//不展示数字，只有一个小红点
    style?: React.CSSProperties;
    prefixCls?: string;
    scrollNumberPrefixCls?: string;
    className?: string;
    status?: 'success' | 'processing' | 'default' | 'error' | 'warning';
    text?: string;
    // offset?: [number | string, number | string];//设置状态点的位置偏移，格式为 x, y
}
export interface AkBadgeStates {}
export class AkBadge extends Component < AkBadgeProps,
AkBadgeStates > {
    render() {
        return <Badge {...this.props}></Badge>
    }
}
class AkBadgeStyle {}
