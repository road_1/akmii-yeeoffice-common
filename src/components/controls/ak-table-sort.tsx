import * as React from "react";
import {Component} from "react";
import {AkPaginationProps} from "./ak-pagination";
import {AkColumnProps, AkTableProps, AkTable} from "./ak-table";
import {AkIcon} from "./ak-icon";


export interface AkTableSortProps<T> extends AkTableProps<T> {
}
export interface AkTableSortStates<T> {
    orderColumns?: AkColumnProps<T>[]
}

export interface AkTableSorter {
    column?: any;
    columnKey?: string;
    field?: string;
    order: 'ascend' | 'descend';
}

export class AkTableSort<T> extends Component<AkTableSortProps<T>,
    AkTableSortStates<T>> {

    constructor(props, context) {
        super(props, context);
        this.pagination = {
            current: (props.pagination && props.pagination.current) || 1,
            pageSize: (props.pagination && props.pagination.pageSize) || 10,
            total: (props.pagination && props.pagination.total) || 0
        };
        this.filters = {};
        this.state = {orderColumns: this.processColumns(props.columns)}
    }

    sorter: AkTableSorter;
    pagination: AkPaginationProps | boolean;
    filters;

    componentWillReceiveProps(nextProps) {
        if ("columns" in nextProps && nextProps.columns !== this.props.columns) {
            this.setState({orderColumns: this.processColumns(nextProps.columns)});
        }
    }

    onSortChange(column) {
        const {onChange, columns} = this.props;
        let sorter = this.sorter;
        if (sorter && sorter.column === column) {
            sorter.order = sorter.order === "ascend" ? "descend" : "ascend";
        } else {
            sorter = this.sorter = {
                column: column,
                columnKey: column.dataIndex,
                field: column.dataIndex,
                order: 'ascend'
            };
        }
        this.setState({orderColumns: this.processColumns(columns)});
        onChange && onChange(this.pagination, this.filters, sorter);
    }

    getSortTitle(column: AkColumnProps<T>) {
        const sorter = this.sorter;
        return <span className="ak-table-sorter" onClick={()=>this.onSortChange(column)}>
            {sorter && sorter.column === column ? (sorter.order === "ascend" ?<AkIcon type="up-arrow-s"/>:
                    <AkIcon type="down-arrow-s"/>) : null}
            <span className="title">{column.title}</span>
            <AkIcon type="down-s"/>
        </span>;
    }

    /**
     * 处理columns，将sorter属性移除，添加自己的控制
     * @param columns
     * @returns {U[]}
     */
    processColumns(columns: AkColumnProps<T>[]): AkColumnProps<T>[] {
        if (columns) {
            let hasSorter = false;
            let newColumns = columns.map(c => {
                if (c.sorter) {
                    hasSorter = true;
                    let {sorter, title, ...others} = c;
                    others["title"] = this.getSortTitle(c);
                    return others;
                } else {
                    return c;
                }
            });
            return newColumns;
        } else {
            return null;
        }
    }

    onChange(pagination: AkPaginationProps | boolean, filters: string[]) {
        const {onChange} = this.props;
        this.pagination = pagination;
        this.filters = filters;
        onChange && onChange(pagination, filters, this.sorter);
    }

    render() {
        const {columns, onChange, ...others} = this.props;
        const {orderColumns} = this.state;
        return React.createElement(AkTable, {
            ...others, ...{
                columns: orderColumns,
                onChange: (pagination, filters) => this.onChange(pagination, filters)
            }
        });
    }

}