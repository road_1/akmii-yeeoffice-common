import * as React from "react";
import { AkTreeSelect } from "../ak-treeselect";
import { AkGlobal } from "../../../util/common";
import { MetadataAction } from "../../../actions/index";
import { connect } from "react-redux";
import { MetadataDictUtil } from "../../../reducers/Metadata";
import { MetadataInfo } from "../../../api/flowcraft/metadata";
import { AkTreeData } from "../index";
import { AkAbstractSelectProps } from "../ak-select";
import { CommonLocale } from "../../../locales/localeid";
import { AkMetadataOption, AkMetadataAbstractSelectProps } from "./AkMetadataSelect";

export interface AkMetadataTreeSelectProps extends AkMetadataAbstractSelectProps, AkAbstractSelectProps {
    value?: string | string[];
    defaultValue?: string | Array<any>;
    multiple?: boolean;
    onSelect?: (value: any) => void;
    onSearch?: (value: any) => void;
    searchPlaceholder?: string;
    dropdownStyle?: React.CSSProperties;
    dropdownMatchSelectWidth?: boolean;
    treeDefaultExpandAll?: boolean;
    treeCheckable?: boolean | React.ReactNode;
    treeDefaultExpandedKeys?: Array<string>;
    filterTreeNode?: (inputValue: string, treeNode: any) => boolean | boolean;
    treeNodeFilterProp?: string;
    treeNodeLabelProp?: string;
    treeData?: Array<AkTreeData>;
    treeDataSimpleMode?: boolean | Object;
    loadData?: (node: any) => void;
    showCheckedStrategy?: "SHOW_ALL" | "SHOW_PARENT" | "SHOW_CHILD";
    labelInValue?: boolean;
    treeCheckStrictly?: boolean;
    getPopupContainer?: (triggerNode: Element) => HTMLElement;
    maxSelection?: number; //最大选择项
}

export interface AkMetadataTreeSelectState {
    AkTreeSelectOptionList?: AkMetadataOption[];
}

@connect((state) => ({ metadataDict: state.metadata.metadataDict }))
export class AkMetadataTreeSelect extends React.Component<AkMetadataTreeSelectProps, AkMetadataTreeSelectState> {
    static defaultProps: AkMetadataTreeSelectProps = {
        maxSelection: 200,
        dropdownStyle: {
            maxHeight: 400
        }
    }
    constructor(props, context) {
        super(props, context);
        let options: AkMetadataOption[] = [];
        this.state = {
            AkTreeSelectOptionList: options,
        };
    }
    componentDidMount() {
        const { dispatch } = AkGlobal.store;
        const { categoryCode, parentCode, categoryID, parentID } = this.props;
        if (categoryCode) {
            dispatch(MetadataAction.request(categoryCode, parentCode));
        } else if (categoryID) {
            dispatch(MetadataAction.requestByID(categoryID, parentID));
        }
    }

    componentWillReceiveProps(nextProps) {
        const { dispatch } = AkGlobal.store;
        if ("categoryID" in nextProps && nextProps.categoryID !== this.props.categoryID) {
            dispatch(MetadataAction.requestByID(nextProps.categoryID, nextProps.parentID));
        }
        if ("parentID" in nextProps && nextProps.parentID !== this.props.parentID) {
            dispatch(MetadataAction.requestByID(nextProps.categoryID, nextProps.parentID));
        }
        if ("categoryCode" in nextProps && nextProps.categoryCode !== this.props.categoryCode) {
            dispatch(MetadataAction.request(nextProps.categoryCode, nextProps.parentCode));
        }
    }

    handleOnChange = (value: any) => {
        const { onChange } = this.props;
        if (onChange) {
            const { metadataDict, categoryCode, parentCode, categoryID, parentID, multiple } = this.props;
            let code = null,
                name = null,
                meta = null;
            if (!multiple) {
                if (categoryCode) {
                    meta = MetadataDictUtil.getMetadataByID(metadataDict, value, categoryCode, parentCode);
                } else {
                    meta = MetadataDictUtil.getMetadataByID(metadataDict, value, categoryID, parentID);
                }
                if (meta && meta.length > 0) {
                    code = meta[0].Code;
                    name = meta[0].Name;
                }
                onChange(value || "", code, name, meta);
            } else {
                onChange(value || [], null, null, meta);
            }

        }
    }

    render() {
        //load data from redux
        const { metadataDict, categoryCode, parentCode, categoryID, parentID, multiple, onChange, disabled, maxSelection, value, ...others } = this.props;
        let holder = null;
        if (categoryCode) {
            holder = MetadataDictUtil.get(metadataDict, categoryCode, parentCode);
        } else if (categoryID) {
            holder = MetadataDictUtil.get(metadataDict, categoryID, parentID);
        }
        let treeData = [];
        if (holder && holder.data && parentID !== "") {
            (holder.data as MetadataInfo[]).forEach(item => {
                if (item.Status !== 1) {
                    //被禁用项目不显示
                    return;
                }
                let pid = item.ParentID === parentID ? "0" : item.ParentID;
                const max = multiple ? maxSelection : 1;
                let values: any = [];
                if (value && typeof value === "string") {
                    values.push(value)
                } else {
                    values = value
                }
                let disabled = values ? multiple && max > 0 && values.length >= max && values.indexOf(item.ID) === -1 : false;
                treeData.push({ key: item.ID, value: item.ID, pId: pid, label: item.Name, disabled: disabled });
            });
        }
        const treeDataSimpleMode = {
            id: "key",
            rootPId: "0"
        };

        const v = value === null || value === "" ? undefined : value;
        return <AkTreeSelect
            className={this.props.className}
            notFoundContent={AkGlobal.intl.formatMessage({ id: CommonLocale.AutoCompleteNoResult })}
            dropdownMatchSelectWidth={false}
            disabled={disabled}
            multiple={multiple}
            treeCheckable={false}
            treeNodeFilterProp="title"
            showSearch={true}
            allowClear={true}
            treeData={treeData}
            value={v}
            treeDataSimpleMode={treeDataSimpleMode}
            onChange={v => this.handleOnChange(v)}
            treeDefaultExpandAll={true}
            optionsIncludeDestroyed {...others} />
    }
}