import * as React from "react";
import { connect } from "react-redux";
import { AkSelect } from "../ak-select";
import { AkGlobal } from "../../../util/common";
import { MetadataAction } from "../../../actions";
import { MetadataCategoryInfo } from "../../../api/flowcraft/metadata";
import { MetadataLocale } from "../../../locales/localeid";

export interface AkMetadataCategorySelectProps {
    onChange?: (value: string) => void;
    disabled?: boolean;
    value?: string;
    defaultValue?: string;
    placeholder?: string;
    metadataCategories?: MetadataCategoryInfo[];
    allowClear?: boolean;
}

const mapStateToProps = (state) => {
    return {
        metadataCategories: state.metadata.metadataCategories
    };
};

@connect(mapStateToProps)
export class AkMetadataCategorySelect extends React.Component<AkMetadataCategorySelectProps, undefined> {
    constructor(props: AkMetadataCategorySelectProps, context) {
        super(props, context);
    }

    componentDidMount() {
        AkGlobal.store.dispatch(MetadataAction.categoryRequest());
    }

    render() {
        const { formatMessage } = AkGlobal.intl;
        const { metadataCategories, placeholder, ...others } = this.props;
        if (!metadataCategories) return null;

        const options = (metadataCategories || []).map(c =>
            <AkSelect.Option key={c.CategoryID} value={c.CategoryID}>{c.Name}</AkSelect.Option>
        );

        return <AkSelect {...others} optionsIncludeDestroyed
            placeholder={placeholder || formatMessage({ id: MetadataLocale.MetadataCategoryPlaceholder })}
            notFoundContent={formatMessage({ id: MetadataLocale.MetadataCategoryEmpty })}>{options}</AkSelect>
    }
}