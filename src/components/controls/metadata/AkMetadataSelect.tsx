import * as React from "react";
import { AkSelect, AkAbstractSelectProps, AkSelectValue } from "../ak-select";
import { AkGlobal } from "../../../util/common";
import { MetadataAction } from "../../../actions/index";
import { connect } from "react-redux";
import { MetadataDictUtil } from "../../../reducers/Metadata";
import { MetadataInfo } from "../../../api/flowcraft/metadata";
import { AkSpin } from "../ak-spin";

export class AkMetadataOption {
    ID: string;
    Code: string;
    Name: string;
}

export interface AkMetadataAbstractSelectProps {
    categoryCode?: string;
    parentCode?: string;
    categoryID?: string;
    parentID?: string;
    onChange?: (id: string, code: string, name: string, metadata: MetadataInfo | MetadataInfo[]) => void;
    metadataDict?: any;
    notInTheseData?: Array<string>;
}

export interface AkMetadataSelectProps extends AkMetadataAbstractSelectProps, AkAbstractSelectProps {
    value?: string | string[];
    defaultValue?: AkSelectValue;
    mode?: "default" | "multiple" | "tags" | "combobox";
    multiple?: boolean;
    tags?: boolean;
    combobox?: boolean;
    optionLabelProp?: string;
    filterOption?: boolean | ((inputValue: string, option: Object) => any);
    onSelect?: (value: AkSelectValue, option: Object) => any;
    onDeselect?: (value: AkSelectValue) => any;
    onSearch?: (value: string) => any;
    dropdownMatchSelectWidth?: boolean;
    optionFilterProp?: string;
    defaultActiveFirstOption?: boolean;
    labelInValue?: boolean;
    getPopupContainer?: (triggerNode: Element) => HTMLElement;
    dropdownStyle?: React.CSSProperties;
    dropdownMenuStyle?: React.CSSProperties;
    tokenSeparators?: string[];
    getInputElement?: () => React.ReactElement<any>;
    maxSelection?: number; //最大选择项
}

export interface AkMetadataSelectState {

}

@connect((state) => ({ metadataDict: state.metadata.metadataDict }))
export class AkMetadataSelect extends React.Component<AkMetadataSelectProps, AkMetadataSelectState> {
    static defaultProps: AkMetadataSelectProps = {
        maxSelection: 200
    }

    constructor(props, context) {
        super(props, context);
        let options: AkMetadataOption[] = [];
        this.state = {
            // AkSelectOptionList: options,

        };
    }

    componentDidMount() {
        const { dispatch } = AkGlobal.store;
        const { categoryCode, parentCode, categoryID, parentID } = this.props;
        if (categoryCode) {
            dispatch(MetadataAction.request(categoryCode, parentCode));
        } else if (categoryID) {
            dispatch(MetadataAction.requestByID(categoryID, parentID));
        }
    }

    componentWillReceiveProps(nextProps) {
        const { dispatch } = AkGlobal.store;

        if ("categoryID" in nextProps && nextProps.categoryID !== this.props.categoryID) {
            dispatch(MetadataAction.requestByID(nextProps.categoryID, nextProps.parentID));
        }

        if ("parentID" in nextProps && nextProps.parentID !== this.props.parentID) {
            dispatch(MetadataAction.requestByID(nextProps.categoryID, nextProps.parentID));
        }

        if ("categoryCode" in nextProps && nextProps.categoryCode !== this.props.categoryCode) {
            dispatch(MetadataAction.request(nextProps.categoryCode, nextProps.parentCode));
        }
    }


    handleOnChange = (value: string) => {
        const { onChange } = this.props;
        if (onChange) {
            const { metadataDict, categoryCode, parentCode, categoryID, parentID } = this.props;
            let code = null,
                name = null,
                meta = null;
            if (categoryCode) {
                meta = MetadataDictUtil.getMetadataByID(metadataDict, value, categoryCode, parentCode);
            } else {
                meta = MetadataDictUtil.getMetadataByID(metadataDict, value, categoryID, parentID);
            }
            if (meta) {
                code = meta.Code;
                name = meta.Name;
            }

            onChange(value || "", code, name, meta);
        }
    }

    render() {
        //load data from redux
        const { metadataDict, categoryCode, parentCode, categoryID, parentID, onChange, multiple, value, maxSelection, ...others } = this.props;
        let holder = null;
        if (categoryCode) {
            holder = MetadataDictUtil.get(metadataDict, categoryCode, parentCode);
        } else if (categoryID) {
            holder = MetadataDictUtil.get(metadataDict, categoryID, parentID);
        }
        let loading = true;
        let children = [];
        if (holder && holder.data) {
            loading = false;
            let holders = [];
            if (this.props.notInTheseData && this.props.notInTheseData.length > 0) {
                (holder.data as MetadataInfo[]).forEach(item => {
                    if (this.props.notInTheseData.findIndex(x => x === item.ID) === -1) {
                        holders.push(item);
                    }
                })
            } else
                holders = holder.data;
            (holders as MetadataInfo[]).forEach(item => {
                if (item.Status !== 1) {
                    //被禁用项目不显示
                    return;
                }

                let values: any = [];
                if (value && typeof value === "string") {
                    values.push(value)
                } else {
                    values = value
                }

                const max = multiple ? maxSelection : 1;
                let disabled = values ? multiple && max > 0 && values.length >= max && values.indexOf(item.ID) === -1 : false;
                if (parentID) {
                    if (item.ParentID === parentID) {
                        children.push(<AkSelect.Option disabled={disabled} key={item.ID}>{item.Name}</AkSelect.Option>);
                    }
                } else {
                    children.push(<AkSelect.Option disabled={disabled} key={item.ID}>{item.Name}</AkSelect.Option>);
                }
            })
        }

        let v = value === null || value === "" ? undefined : value;
        return <AkSpin spinning={loading}>
            {!loading && <AkSelect {...others} multiple={multiple} value={v} onChange={this.handleOnChange} optionsIncludeDestroyed>{children}</AkSelect>}
        </AkSpin>
    }
}
