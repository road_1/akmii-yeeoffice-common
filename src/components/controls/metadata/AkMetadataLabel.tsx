import * as React from "react";
import { connect } from "react-redux";
import { AkGlobal } from "../../../util/common";
import { MetadataAction } from "../../../actions";
import { MetadataDictUtil } from "../../../reducers/Metadata";
import { CommonLocale } from "../../../locales/localeid";

export interface AkMetadataLabelStates {
    displayName: string;
}

export interface AkMetadataLabelProp {
    className?: string;
    categoryCode?: string;
    parentCode?: string;
    categoryID?: string;
    parentID?: string;
    optionID: string | string[];
    metadataDict?: any;
}

@connect((state) => ({ metadataDict: state.metadata.metadataDict }))
export class AkMetadataLabel extends React.Component<AkMetadataLabelProp, AkMetadataLabelStates> {
    constructor(props, context) {
        super(props, context);
        this.state = {
            displayName: ""
        };
    }
    componentDidMount() {
        const { dispatch } = AkGlobal.store;
        const { categoryCode, parentCode, categoryID, parentID } = this.props;
        if (categoryCode) {
            dispatch(MetadataAction.request(categoryCode, parentCode));
        } else {
            dispatch(MetadataAction.requestByID(categoryID, parentID));
        }
    }

    componentWillReceiveProps(nextProps) {
        const { dispatch } = AkGlobal.store;
        if ("categoryID" in nextProps && nextProps.categoryID !== this.props.categoryID) {
            dispatch(MetadataAction.requestByID(nextProps.categoryID, nextProps.parentID));
        }

        if ("categoryCode" in nextProps && nextProps.categoryCode !== this.props.categoryCode) {
            dispatch(MetadataAction.request(nextProps.categoryCode, nextProps.parentCode));
        }
    }

    render() {
        //load data from redux
        const { metadataDict, categoryCode, parentCode, categoryID, parentID, optionID } = this.props;
        let name: any = AkGlobal.intl.formatMessage({ id: CommonLocale.InlineLoading });
        let meta;
        if (categoryCode) {
            meta = MetadataDictUtil.getMetadataByID(metadataDict, optionID, categoryCode, parentCode);
        } else {
            meta = MetadataDictUtil.getMetadataByID(metadataDict, optionID, categoryID, parentID);
        }

        if (meta) {
            const Names: string[] = meta.map(item => item.Name);
            name = Names.join(",");
        } else if (meta === undefined) { // To support old data
            name = optionID;
        }

        return <span className={this.props.className}>{name}</span>;
    }
}