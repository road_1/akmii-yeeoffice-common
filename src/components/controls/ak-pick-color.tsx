import * as React from "react";
import { Component } from "react";
import { SketchPicker, TwitterPicker } from "react-color";
import { AkPopover, AkPopoverProp } from "./ak-popover";

export interface AkColorModel {
    hex: string,
    rgb: {
        r: number,
        g: number,
        b: number,
        a: number,
    },
    hsl: {
        h: number,
        s: number,
        l: number,
        a: number,
    },
}

export interface AkPickColorProp {
    onChange?: (value: string, rgba?: string) => void;
    value?: string;
    disabled?: boolean;
    style?: React.CSSProperties;
    overlayClassName?: string;
    hideLabel?: boolean;
    simple?: boolean;
}

export interface AkPickColorState {
    color?: string | AkColorModel;
}

export class AkPickColor extends Component<AkPickColorProp, AkPickColorState> {
    constructor(props, context) {
        super(props, context);
        const { value } = this.props;
        this.state = {
            color: value ? value : "#ffffff"
        };
    }

    componentWillReceiveProps(nextProps: AkPickColorProp) {
        if ("value" in nextProps && nextProps.value !== this.props.value) {
            this.setState({ color: nextProps.value ? nextProps.value : '#ffff' });
        }
    }

    handleChangeComplete(color) {
        const { onChange } = this.props;
        if (onChange) onChange(color.hex, this.getRgba(color));
        this.setState({ color: color })
    }

    renderDisplay() {
        if (this.props.children) {
            return this.props.children;
        }
        return <div className="ak-pick-color-display">
            <div style={{ backgroundColor: this.getRgba(this.state.color) }} />
        </div>
    }

    render() {
        const akPopoverProp: AkPopoverProp = {
            placement: "topLeft",
            trigger: "click",
            overlayClassName: "ak-pick-color-popover ak-popover-no-padding " + this.props.overlayClassName,
            content: this.props.simple === true
                ? <TwitterPicker
                    colors={["#ecaed7", "#d2d2d2", "#ed9eac", "#d1b49b", "#f4c392", "#a0eaa8", "#f4e884", "#fb6469", "#d67236", "#c6cdf7"]}
                    triangle={'hide'}
                    hex={this.getRgba(this.state.color)}
                    onChangeComplete={color => this.handleChangeComplete(color)}/>
                : <SketchPicker color={this.state.color} onChangeComplete={color => this.handleChangeComplete(color)}/>
        }

        return <div style={this.props.style} className={"ak-pick-color" + (this.props.disabled ? " ak-pick-color-disabled" : "")}>
            {this.props.disabled ? this.renderDisplay() : <AkPopover {...akPopoverProp}>
                {this.renderDisplay()}
            </AkPopover>}
            {
                this.props.hideLabel
                ?
                null
                :
                <span className="ak-pick-color-text">{this.getRgba(this.state.color)}</span>
            }

        </div >
    }

    getRgba(color: string | AkColorModel) {
        if (typeof color === "string") {
            return color;
        }
        let value = color as AkColorModel;
        if (value && value.rgb) {
            if (value.rgb.a === 1) {
                return value.hex;
            }
            return `rgba(${value.rgb.r}, ${value.rgb.g}, ${value.rgb.b}, ${value.rgb.a})`;
        }
        return value.hex;
    }
}