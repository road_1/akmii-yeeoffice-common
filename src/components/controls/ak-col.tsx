import * as React from "react";
import {Component} from "react";
import {Col} from "antd";
import * as classNames from "classnames";

export interface AkColSize {
    span?: number;
    order?: number;
    offset?: number;
    push?: number;
    pull?: number;
}
export interface AkColProps {
    className?: string;
    span?: number;
    order?: number;
    offset?: number;
    push?: number;
    pull?: number;
    xs?: number | AkColSize;
    sm?: number | AkColSize;
    md?: number | AkColSize;
    lg?: number | AkColSize;
    // xl?: number | AkColSize;
    // xxl?: number | AkColSize;
    prefixCls?: string;
    style?: React.CSSProperties;
}
export interface AkColStates {}
export class AkCol extends Component < AkColProps,
AkColStates > {
    render() {
        const {className, ...others} = this.props;
        const cls = classNames("ak-col", className);
        return <Col className={cls} {...others}></Col>
    }
}
class AkColStyle {}
