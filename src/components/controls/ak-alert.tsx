import * as React from 'react'
import { Component } from 'react'
import { Alert } from 'antd'

export interface AkAlertProps {
    /**
     * Type of Alert styles, options:`success`, `info`, `warning`, `error`
     */
    type?: 'success' | 'info' | 'warning' | 'error';
    /** Whether Alert can be closed */
    closable?: boolean;
    /** Close text to show */
    closeText?: React.ReactNode;
    /** Content of Alert */
    message: React.ReactNode;
    /** Additional content of Alert */
    description?: React.ReactNode;
    /** Callback when close Alert */
    onClose?: React.MouseEventHandler<HTMLAnchorElement>;
    /** Whether to show icon */
    showIcon?: boolean;
    iconType?: string;
    style?: React.CSSProperties;
    prefixCls?: string;
    className?: string;
    /**是否用作顶部公告 */
    banner?: boolean;
}
export interface AkAlertStates { }
export class AkAlert extends Component<AkAlertProps, AkAlertStates> {
    render() {
        const { } = this.props;
        const { } = this.state;
        return <Alert {...this.props} > </Alert>
    }
}