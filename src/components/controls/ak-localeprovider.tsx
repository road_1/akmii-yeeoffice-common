import * as React from 'react'
import { Component } from 'react'
import { LocaleProvider } from 'antd';

export interface ModalLocale {
    okText: string;
    cancelText: string;
    justOkText: string;
}
export interface AkLocale {
    // locale: string;
    Pagination?: Object;
    DatePicker?: Object;
    TimePicker?: Object;
    Calendar?: Object;
    Table?: Object;
    Modal?: ModalLocale;
    Popconfirm?: Object;
    Transfer?: Object;
    Select?: Object;
    Upload?: Object;
}
export interface AkLocalProviderProps {
    locale: AkLocale;
    children?: React.ReactElement<any>;
}
export interface AkLocalProviderStates { }
export class AkLocalProvider extends Component<AkLocalProviderProps,
    AkLocalProviderStates> {
    render() {
        return <LocaleProvider {...this.props}></LocaleProvider>
    }
}
class AkLocalProviderStyle { }
