import { notification } from 'antd';
import { AkGlobal } from '../../util/common';
import { CommonAction } from '../../actions/index';
export declare type notificationPlacement = 'topLeft' | 'topRight' | 'bottomLeft' | 'bottomRight';
export declare type AkIconType = 'success' | 'info' | 'error' | 'warning';
export interface AkConfigProps {
    top?: number;
    bottom?: number;
    duration?: number;
    placement?: notificationPlacement;
    getContainer?: () => HTMLElement;
}
export interface AkArgsProps {
    message: React.ReactNode;
    description: React.ReactNode;
    btn?: React.ReactNode;
    key?: string;
    onClose?: () => void;
    duration?: number | null;
    icon?: React.ReactNode;
    placement?: notificationPlacement;
    style?: React.CSSProperties;
    prefixCls?: string;
    className?: string;
    readonly type?: AkIconType;
}
export class AkNotification {
    static info(args: AkArgsProps): void {
        if (AkGlobal.store) {
            AkGlobal.store.dispatch(CommonAction.info(args));
        } else {
            return notification.info(args);
        }
    };

    static success(args: AkArgsProps): void {
        return notification.success(args);
    };

    static error(args: AkArgsProps): void {
        if (AkGlobal.store) {
            AkGlobal.store.dispatch(CommonAction.error(args));
        } else {
            return notification.error(args);
        }
    };

    static warn(args: AkArgsProps): void {
        if (AkGlobal.store) {
            AkGlobal.store.dispatch(CommonAction.warn(args));
        } else {
            return notification.warn(args);
        }
    };

    static warning(args: AkArgsProps): void {
        if (AkGlobal.store) {
            AkGlobal.store.dispatch(CommonAction.warning(args));
        } else {
            return notification.warning(args);
        }
    };

    static open(args: AkArgsProps): void {
        return notification.open(args)
    };
    static close(key: string): void {
        return notification.close(key);
    };
    static config(options: AkConfigProps): void {
        return notification.config(options)
    };
    static destroy(): void {
        notification.destroy();
    };
}
