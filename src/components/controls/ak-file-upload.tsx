import * as React from "react";
import * as SparkMD5 from "spark-md5";
import * as superagent from "superagent";
import * as classNames from "classnames";
import { AkFile, AkUpload, AkUploadProps, AkHttpRequestHeader, AkShowUploadListInterface } from "./ak-upload";
import { AkGlobal } from '../../util/common';
import { AkUtil } from '../../util/util';
import { CommonLocale, UploadLocale } from '../../locales/localeid';
import { AkNotification } from "./ak-notification";
import { AttachmentAPI, AttachmentStatus } from '../../api/flowcraft/attachment';
import { AkModal } from "./ak-modal";
import { AkButton } from "./ak-button";
import { DeleteDocumentRequest, FileUpLoadCommon, FileUploadMethod, AkContext, AppKeys, FileUpLoadAPI, LibraryResponse, LibraryModel, AkFileUploadItem } from "../../index";
import { AkMessage } from './ak-message';
import { connect } from 'react-redux';
import { FileAction } from '../../actions/index';
import { AkSpin } from './ak-spin';


export interface AkFileUploadProps {
    locale?: UploadLocale;
    type?: 'drag' | 'select';
    name?: string;
    defaultFileList?: Array<AkFile>;
    fileList?: Array<AkFile>;
    action?: string;
    data?: Object | ((file: AkFile) => any);
    headers?: AkHttpRequestHeader;
    showUploadList?: boolean | AkShowUploadListInterface;
    accept?: string;
    listType?: 'text' | 'picture' | 'picture-card';
    className?: string;
    onPreview?: (file: AkFile) => void;
    onRemove?: (file: AkFile) => void | boolean;
    supportServerRender?: boolean;
    style?: React.CSSProperties;
    disabled?: boolean;
    prefixCls?: string;
    customRequest?: (option: any) => void;
    withCredentials?: boolean;
    multiple?: boolean;
    onChange?: (value: AkFileUploadItem | AkFileUploadItem[]) => void;
    value?: AkFileUploadItem | AkFileUploadItem[];
    defaultValue?: AkFileUploadItem | AkFileUploadItem[];
    maxSize?: number;
    maxCount?: number;
    acceptExtensions?: string[];
    beforeUpload?: (file: AkFile, FileList?: AkFile[]) => boolean | PromiseLike<any>;
    beforeRemove?: (file: AkFileUploadItem) => boolean;
    afterUpload?: (file: AkFileUploadItem) => void;
    afterRemove?: (file: AkFileUploadItem) => void;
    status?: (fileList?: AkFile[]) => void;
    library?: LibraryModel
}

export interface AkFileUploadState {
    fileList?: AkFile[];
    showDeleteModal?: boolean;
    value?: AkFileUploadItem | AkFileUploadItem[];
}

function getCode() {
    let code = "flowcraft-file";
    if (AkContext.getAppKey() !== AppKeys.Flowcraft) {
        code = AkContext.getAppKey().toLowerCase() + "-file";
    }
    return code;
}

const mapStateToProps = (state) => {
    return {
        library: state.file.libraryDict[getCode()]
    };
};

@connect(mapStateToProps)
export class AkFileUpload extends React.Component<AkFileUploadProps, AkFileUploadState> {
    static defaultProps: AkFileUploadProps = {
        listType: "picture"
    };
    uploadingCount: number = 0;
    deleteFiles?: DeleteDocumentRequest;
    listDataIDs?: string[];
    // library: LibraryModel;

    constructor(props, context) {
        super(props, context);

        let value = ("value" in props) ? props.value : props.defaultValue;
        this.state = {
            showDeleteModal: false,
            value: value,
            fileList: this.parseAkFiles(value)
        };
        this.deleteFiles = null;
        this.listDataIDs = [];
        // this.library = null;
    }


    componentWillMount() {
        AkGlobal.store.dispatch(FileAction.requestLibrary(getCode()));
    }

    // componentDidMount() {
    //     this.getLibrary().then(onfulfilled => {
    //         this.library = onfulfilled;
    //     });
    // }
    componentWillReceiveProps(nextProps: AkFileUploadProps) {
        if ("value" in nextProps && nextProps.value !== this.props.value) {
            this.setState({
                value: nextProps.value,
                fileList: this.parseAkFiles(nextProps.value)
            });
        }
    }
    private parseAkFiles(files: AkFileUploadItem | AkFileUploadItem[]) {
        let akFileList: AkFile[] = [];
        if (files) {
            if (Array.isArray(files)) {
                files.forEach((file, index) => akFileList.push(this.parseAkFile(-index, file)));
            } else {
                akFileList.push(this.parseAkFile(0, files))
            }
        }

        return akFileList;
    }

    private parseAkFile(uid: number, file: AkFileUploadItem): AkFile {
        const { fileName: name, fileSize: size, fileKey } = file;
        const type = "", status = "done", thumbUrl = AkUtil.getThumbUrl(name);
        return { uid, type, name, thumbUrl, size, fileKey, status };
    }

    onChangeHandler(info) {
        let temp = info.fileList.map(file => {
            file.thumbUrl = AkUtil.getThumbUrl(file.name);
            if (file.originFileObj) {
                file.fileKey = file.originFileObj.fileKey;
            }
            return file;
        });
        if (this.props.status) {
            this.props.status(temp);
        }
        this.setState({ fileList: temp });
    }

    beforeUploadHandler(file: AkFile) {
        const { props: { maxSize, maxCount, acceptExtensions, beforeUpload }, state: { fileList } } = this;
        const { formatMessage } = AkGlobal.intl;
        let msg = "";

        if (file.size === 0) {
            msg = formatMessage({ id: UploadLocale.MsgNoContentError }, { file: file.name });
            AkNotification.warning({ message: formatMessage({ id: CommonLocale.Tip }), description: msg });
            return false;
        }

        const defaultMaxSize: number = 1024 * 1024 * 50;
        const fileSize = maxSize === undefined || maxSize === null || maxSize >= defaultMaxSize ? defaultMaxSize : maxSize;

        // check file size
        if (file.size > fileSize) {
            msg = formatMessage({ id: UploadLocale.MsgFileSizeError }, { value: AkUtil.bytesToSize(fileSize), file: file.name });
            AkNotification.warning({ message: formatMessage({ id: CommonLocale.Tip }), description: msg });
            return false;
        }
        if (FileUpLoadCommon.validateFileNameSize(file.name) === false) {
            msg = formatMessage({ id: UploadLocale.FileNameMoreLarge }, { name: file.name });
            AkNotification.warning({ message: formatMessage({ id: CommonLocale.Tip }), description: msg });
            return false;
        }
        if (FileUpLoadCommon.validateFileName(file.name) === false) {
            msg = formatMessage({ id: UploadLocale.FileNameInvalidate }, { name: file.name });
            AkNotification.warning({ message: formatMessage({ id: CommonLocale.Tip }), description: msg });
            return false;
        }
        // check file count
        if (maxCount !== undefined && maxCount !== null) {
            if (Array.isArray(fileList) && (fileList.length + this.uploadingCount) >= maxCount) {
                msg = formatMessage({ id: UploadLocale.MsgFileCountError }, { value: maxCount });
                AkNotification.warning({ message: formatMessage({ id: CommonLocale.Tip }), description: msg });
                return false;
            }
        }

        // check file ext
        if (Array.isArray(acceptExtensions)) {
            if (acceptExtensions.indexOf(AkUtil.getFileExt(file.name)) === -1) {
                msg = formatMessage({ id: UploadLocale.MsgFileTypeError }, { exts: acceptExtensions.join(', ') });
                AkNotification.warning({ message: formatMessage({ id: CommonLocale.Tip }), description: msg });
                return false;
            }
        }

        // check file name exist
        if (Array.isArray(fileList)) {
            if (fileList.map(i => i.name).indexOf(file.name) !== -1) {
                msg = formatMessage({ id: UploadLocale.MsgFileExists }, { value: file.name });
                AkNotification.warning({ message: formatMessage({ id: CommonLocale.Tip }), description: msg });
                return false;
            }
        }

        // custom check
        if (beforeUpload) {
            if (!beforeUpload(file)) return false;
        }

        this.uploadingCount++;

        return true;
    }

    // getLibrary() {
    //     const { formatMessage } = AkGlobal.intl;
    //     return new Promise((resolve, reject) => {
    //         let code = "";
    //         if (AkContext.getAppKey() === AppKeys.Flowcraft) {
    //             code = "flowcraft-file";
    //         } else {
    //             code = AkContext.getAppKey().toLowerCase() + "-file";
    //         }
    //         if (libraryCache[code]) {

    //         }

    //         FileUpLoadAPI.getLibraryByCode({ Code: code }).then(d => {
    //             if (d.Status === 0) {
    //                 resolve(d.Data);
    //             } else {
    //                 console.log(d);
    //                 reject(d.Message)
    //             }
    //         });
    //     });
    // }

    customRequestHandler(options) {
        const { formatMessage } = AkGlobal.intl;
        const { file } = options;
        FileUploadMethod.beginUpload(options, this.props.library).then((onfulfilled) => {
            this.uploadingCount--;
            let { state: { value }, props: { multiple } } = this;
            let downLoadUrl = onfulfilled.Data;
            let newName = onfulfilled.Message;
            const newFile: AkFileUploadItem = {
                fileKey: newName,
                fileSize: options.file.size,
                fileName: options.file.name,
                fileInfo: { FileKey: newName, Url: downLoadUrl }
            };
            if (multiple) {
                if (!value) value = [];
                (value as AkFileUploadItem[]).push(newFile);
            } else {
                value = newFile;
            }
            this.setState({ value, fileList: this.parseAkFiles(value) });
            if (this.props.onChange) this.props.onChange(value);
            if (this.props.afterUpload) this.props.afterUpload(newFile);
            // options.onSuccess("上传成功");
            this.forceUpdate();
        }, (onrejected) => {
            let msg = onrejected.Message ? onrejected.Message : formatMessage({ id: UploadLocale.MsgFileReadFail }, { file: file.name });
            AkNotification.error({
                message: formatMessage({ id: CommonLocale.Tip }),
                description: msg
            });
            this.uploadingCount--;
            // options.onError({ event: error, body: "error" });
            this.forceUpdate();
        });
        // const { file } = options;
        // const spark = new SparkMD5.ArrayBuffer();
        // const blobSlice = File.prototype.slice;
        // const fileReader = new FileReader();

        // fileReader.onload = e => {
        //     spark.append(fileReader.result);
        //     const md5 = spark.end();

        //     AttachmentAPI.GetAttachmentUrl({
        //         fileName: file.name,
        //         fileLength: file.size + "",
        //         fileExt: AkUtil.getFileExt(file.name),
        //         md5: md5
        //     }).then(data => {
        //         if (data.Data) {
        //             file.fileKey = data.Data.FileKey;
        //             const request = superagent.put(data.Data.Url);
        //             request.set("content-type", "");
        //             request.set("Content-Disposition", "attachment;filename=" + encodeURI(file.name));
        //             request.send(fileReader.result).on("progress", function (e) {
        //                 options.onProgress({ percent: e.percent })
        //             });
        //             request.end((error, response) => {
        //                 this.uploadingCount--;
        //                 if (response.ok) {
        //                     AttachmentAPI.PutAttachementStatus({ FileKey: file.fileKey, Status: AttachmentStatus.Success }).then(() => {
        //                         let { state: { value }, props: { multiple } } = this;

        //                         const newFile: AkFileUploadItem = {
        //                             fileKey: file.fileKey,
        //                             fileSize: file.size,
        //                             fileName: file.name,
        //                             fileInfo: data.Data
        //                         };

        //                         if (multiple) {
        //                             if (!value) value = [];
        //                             (value as AkFileUploadItem[]).push(newFile);
        //                         } else {
        //                             value = newFile;
        //                         }

        //                         this.setState({ value, fileList: this.parseAkFiles(value) });
        //                         if (this.props.onChange) this.props.onChange(value);
        //                         if (this.props.afterUpload) this.props.afterUpload(newFile);
        //                         options.onSuccess("上传成功");
        //                     });
        //                 } else {
        //                     options.onError({ event: error, body: "error" });
        //                 }
        //             });
        //         }
        //     })
        // }

        // fileReader.onerror = () => {
        //     AkNotification.error({
        //         message: formatMessage({ id: CommonLocale.Tip }),
        //         description: formatMessage({ id: UploadLocale.MsgFileReadFail }, { file: file.name })
        //     });
        // }
        // /*使用FileReader读取文件*/
        // fileReader.readAsArrayBuffer(blobSlice.call(file, 0, file.size));
    }

    onRemoveHandler(file: AkFile) {
        const { props: { disabled, beforeRemove, multiple }, state: { value } } = this;
        if (disabled) return false;

        if (beforeRemove) {
            const item: AkFileUploadItem = multiple ?
                (value as AkFileUploadItem[]).find(i => i.fileKey === file.fileKey) :
                value as AkFileUploadItem;

            if (!beforeRemove(item)) return false;
        }

        AkModal.confirm({
            title: AkGlobal.intl.formatMessage({ id: CommonLocale.Tip }),
            content: AkGlobal.intl.formatMessage({ id: UploadLocale.MsgDeleteConfirm }, { file: file.name }),
            onOk: () => this.execRemoveFile(file),
            onCancel: () => { }
        });

        return false;
    }

    execRemoveFile(file: AkFile) {
        let { state: { value }, props: { multiple } } = this;
        let removedFile;
        let temp;

        if (multiple && (value as AkFileUploadItem[]).length > 0) {
            removedFile = (value as AkFileUploadItem[]).find(i => i.fileKey === file.fileKey);
            AkUtil.remove(value as AkFileUploadItem[], i => i.fileKey === file.fileKey);
            temp = value;
        } else {
            removedFile = value;
            temp = null;
        }
        value["file_delete"] = this.getDeleteFiles(removedFile);
        this.setState({ value: temp, fileList: this.parseAkFiles(temp) });
        if (this.props.onChange) this.props.onChange(value);
        if (this.props.afterRemove) this.props.afterRemove(removedFile);
    }
    getDeleteFiles(file) {
        if (!file.fileInfo) {
            return null;
        }
        let itemInfo = file.fileInfo.Url;
        let tempStartIndex = (itemInfo + "").indexOf('appID');
        if (tempStartIndex === -1) {
            AkMessage.success(AkGlobal.intl.formatMessage({ id: CommonLocale.DeleteSuccess }));
            return;
        }
        let tempEndIndex = (itemInfo + "").indexOf('&md5');
        let tempStr = (itemInfo + "").substring(tempStartIndex, tempEndIndex);
        let arry = tempStr.split('&');
        let appID = Number((arry[0] + "").split('=')[1]);
        let libraryID = (arry[1] + "").split('=')[1] + "";
        let listDataID = (arry[2] + "").split('=')[1] + "";
        this.listDataIDs.push(listDataID);
        return this.deleteFiles = {
            AppID: appID,
            LibraryID: libraryID,
            ListDataIDs: this.listDataIDs
        }
    }
    onPreviewHandler(file: AkFile) {
        let files = this.props.value;
        let url: string;
        if (AkUtil.isArray(files)) {
            let info = (files as AkFileUploadItem[]).find(f => f.fileKey === file.fileKey);
            url = info.fileInfo["Url"];
        } else {
            url = (files as AkFileUploadItem).fileInfo["Url"]
        }

        if ((url + "").toLocaleLowerCase().includes("&libraryid")) {
            if (AkContext.getBranch() !== AkContext.YeeOffice) {
                let token = AkContext.getToken();
                token = encodeURIComponent(token);
                if (token && token !== undefined && token !== '' && token !== "undefined") {
                    let akmiisecretIndex = url.lastIndexOf("&akmiisecret=") + 13;
                    url = url.substring(0, akmiisecretIndex) + token;
                }
            }
            url += ("&random=" + AkUtil.guid());
            this.downloadFile(file.name, url);
        } else {
            AttachmentAPI.DownAttachment({ FileKey: file.fileKey }).then(data => {
                if (!file.url) {
                    this.downloadFile(file.name, data.Data);
                } else {
                    file.url = data.Data
                }
            });
        }
    }

    downloadFile(fileName, url) {
        var iframe = document.createElement('iframe');
        iframe.src = url;
        iframe.setAttribute("style", "display:none;")
        document.body.appendChild(iframe);
    }

    render() {
        const { formatMessage } = AkGlobal.intl;
        const { props: { disabled, multiple, children, listType, className }, state: { fileList } } = this;
        const akUploadProps: AkUploadProps = {
            className: classNames(className, { "ant-upload-disabled": disabled }),
            onChange: info => this.onChangeHandler(info),
            beforeUpload: file => this.beforeUploadHandler(file),
            customRequest: options => this.customRequestHandler(options),
            onRemove: file => this.onRemoveHandler(file),
            onPreview: file => this.onPreviewHandler(file),
            action: "", listType, disabled, fileList, multiple
        };

        let loading = !this.props.library;

        return <AkSpin spinning={loading}>
            {!loading && <AkUpload {...akUploadProps}>
                {disabled ? null : (children ? children : <AkButton icon="attachment">
                    {formatMessage({ id: UploadLocale.UploadAttachment })}</AkButton>)}
            </AkUpload>}
        </AkSpin>
    }
}