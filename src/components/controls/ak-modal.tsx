import * as React from "react";
import { Component } from "react";
import { Modal } from "antd";
import * as classNames from "classnames";
import AkModalConfirm from './ak-modal-confirm';
import { findDOMNode } from "react-dom";
import { ButtonType } from "./ak-button";
import { CommonLocale } from "../../locales";
import { AkGlobal } from "../../util/common";
export interface AkModalProps {
    /** 对话框是否可见*/
    visible?: boolean;
    /** 确定按钮 loading*/
    confirmLoading?: boolean;
    /** 标题*/
    title?: React.ReactNode | string;
    /** 是否显示右上角的关闭按钮*/
    closable?: boolean;
    /** 点击确定回调*/
    onOk?: (e?: React.MouseEvent<any>) => void;
    /** 点击模态框右上角叉、取消按钮、Props.maskClosable 值为 true 时的遮罩层或键盘按下 Esc 时的回调*/
    onCancel?: (e: React.MouseEvent<any>) => void;
    afterClose?: () => void;
    /** 宽度*/
    width?: string | number;
    /** 底部内容*/
    footer?: React.ReactNode;
    /** 确认按钮文字*/
    okText?: string;
    /** 确认按钮类型*/
    okType?: ButtonType;
    /** 取消按钮文字*/
    cancelText?: string;
    /** 点击蒙层是否允许关闭*/
    maskClosable?: boolean;
    // destroyOnClose?: boolean;
    style?: React.CSSProperties;
    wrapClassName?: string;
    maskTransitionName?: string;
    transitionName?: string;
    className?: string;
    getContainer?: (instance: React.ReactInstance) => HTMLElement;
    zIndex?: number;
    bodyStyle?: React.CSSProperties;
    maskStyle?: React.CSSProperties;
    mask?: boolean;
    fullscreen?: boolean;
    layout?: "normal" | "left" | "right";
    dragable?: boolean;
}

export interface AkModalFuncProps {
    visible?: boolean;
    title?: React.ReactNode | string;
    content?: React.ReactNode | string;
    onOk?: (func: Function) => any;
    onCancel?: (func: Function) => any;
    width?: string | number;
    iconClassName?: string;
    okText?: string;
    cancelText?: string;
    iconType?: string;
}

export interface AkModalStates {
    dragStyle?: any;
}
export class AkModal extends Component<AkModalProps,
    AkModalStates> {
    top?: number;
    left?: number;
    canMove?: boolean;
    maxTop?: number;
    maxleft?: number;
    constructor(props, context) {
        super(props, context);
        this.state = {
            dragStyle: {},
        };
        this.canMove = false;
    }
    static defaultProps = {
        maskClosable: false,
        layout: "normal",
        dragable: true,
    }

    static info: any = Modal.info;
    static success: any = Modal.success;
    static error: any = Modal.error;
    static warn: any = Modal.warn;
    static warning: any = Modal.warning;
    static confirm: any = Modal.confirm;
    /*
    支持redux的confirm，暂时通过用户在content中添加provider来实现
        static info: any = function (props: AkModalFuncProps) {
            const config = {
                type: 'info',
                iconType: 'info-circle',
                okCancel: false,
                ...props,
            };
            return AkModalConfirm(config);
        };
        static success: any = function (props: AkModalFuncProps) {
            const config = {
                type: 'success',
                iconType: 'check-circle',
                okCancel: false,
                ...props,
            };
            return AkModalConfirm(config);
        };
        static error: any = function (props: AkModalFuncProps) {
            const config = {
                type: 'error',
                iconType: 'cross-circle',
                okCancel: false,
                ...props,
            };
            return AkModalConfirm(config);
        };
        static warning: any = function (props: AkModalFuncProps) {
            const config = {
                type: 'warning',
                iconType: 'exclamation-circle',
                okCancel: false,
                ...props,
            };
            return AkModalConfirm(config);
        };
        static confirm: any = function (props: AkModalFuncProps) {
            const config = {
                type: 'confirm',
                okCancel: true,
                ...props,
            };
            return AkModalConfirm(config);
        };
        */
    componentDidMount() {
        let { style, dragable } = this.props;
        let { dragStyle } = this.state;

        if (dragable) {
            if (style) {
                dragStyle.top = ("top" in style) ? style.top : 100;
                dragStyle.left = ("left" in style) ? style.left : 0;
            } else {
                dragStyle.top = 100;
                dragStyle.left = 0;
            }
            this.setState({ dragStyle })
            window.addEventListener("mouseup", this.onMouseUp.bind(this));
            window.addEventListener("mousemove", this.onMouseMove.bind(this))
        }
    }

    onMouseDown(e) {
        e.stopPropagation();
        e.preventDefault();
        let { dragStyle } = this.state;
        this.top = e.pageY - dragStyle.top;
        this.left = e.pageX - dragStyle.left;
        this.canMove = true;
    }
    onMouseMove(e) {
        let { dragStyle } = this.state;
        if (this.canMove && !this.props.fullscreen) {
            dragStyle.top = e.pageY - this.top;
            dragStyle.left = e.pageX - this.left;
            this.setState({ dragStyle });
        }
    }
    onMouseUp(e) {
        let { dragStyle } = this.state,
            maxTop = window.innerHeight - 48,
            maxLeft = window.innerWidth / 2;
        const startTop = dragStyle.top,
            startLeft = dragStyle.left;
        dragStyle.top = dragStyle.top > 0 ? dragStyle.top > maxTop ? maxTop : dragStyle.top : 0;
        dragStyle.left = dragStyle.left > -maxLeft ? dragStyle.left > maxLeft ? maxLeft : dragStyle.left : -maxLeft;
        if (startTop !== dragStyle.top || startLeft !== dragStyle.left) {
            this.setState({ dragStyle });
        }
        this.canMove = false;
    }

    render() {
        const { fullscreen, layout, style, title, dragable, ...others } = this.props;
        let { dragStyle } = this.state;
        let allStyle = Object.assign({}, style, dragStyle);
        let isMobile = window.innerWidth < 768;
        if (fullscreen || isMobile) {
            allStyle.top = 0;
            allStyle.left = 0;
        }

        let modalTitle = dragable ? (title ? <div className="ak-modal-dragtitle" onMouseDown={this.onMouseDown.bind(this)} >{title}</div> : null) : title;
        const classname = classNames('ak-modal', 'ak-container', 'ak-modal-' + layout, { 'ak-modal-full-screen': fullscreen },
            { 'with-footer': (fullscreen || layout !== "normal") && this.props.footer !== null });
        return <Modal ref="innerModal" title={modalTitle} className={classname} style={allStyle} {...others}></Modal>
    }
}
class AkModalStyle { }
