import * as React from "react";
var classNames = require('classnames');
import {AkResponsiveTable,  ListView} from "./ak-responsive-table";
import {AkTableSort} from "./ak-table-sort";

export class AkResponsiveTableSort<T> extends AkResponsiveTable<T> {
    renderTableView(props) {
        let newProps = {
            ...props,
            className: classNames(props.className, "ak-responsive-table-width-" + (props.columns ? props.columns.length : 0)),
        };
        let style = {display: this.state.view === ListView.table ? "block" : "none"};
        return React.createElement(AkTableSort as any, {...newProps, style: style});
    }
}