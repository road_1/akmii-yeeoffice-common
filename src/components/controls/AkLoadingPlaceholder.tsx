import * as React from 'react';
import { AkSpin } from '.';

export interface AkLoadingPlaceholderProps {
    content?: React.ReactNode;
}
export const AkLoadingPlaceholder = (props: AkLoadingPlaceholderProps) => {
    let content = props.content ? props.content : <div style={{ width: '100%', height: '100vh' }}>&nbsp;</div>
    return <AkSpin>{content}</AkSpin>
}
