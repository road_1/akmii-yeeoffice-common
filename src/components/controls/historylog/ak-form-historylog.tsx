import * as React from 'react'
import { Component } from 'react'
import { FormattedMessage } from "react-intl";
import { AkTable, AkColumnProps, AkIcon, AkRow, AkCol, AkModal, AkColumnLayout, AkPopover, AkAvatar, AkHtml, AkTooltip } from "../index";
import { AkGlobal, FlowcraftCommon } from '../../../util/common';
import { AkUtil } from '../../../util/util';
import { CommonLocale, ApplyContentLocale, HistoryLogLocale } from '../../../locales/localeid';
import { TaskAPI, TaskInfo, ProcessLog } from "../../../api/flowcraft/task";
import { ProcInstAPI, ApplicationStatusLocale } from "../../../api/flowcraft/application";
import { colorDict, outcomeDict, orange } from "./ak-historylog";
import { AkFlowChart } from "./ak-flowchart";
import { AkDateLabel } from '../AkDateLabel';
import { AkContext, AkLink, AppCenterListResponse } from "../../../index";
import { connect } from "react-redux";

/** 自定义表单 流程日志 */
export class TaskTable extends AkTable<TaskInfo> { }
export class ChildrenTable extends AkTable<TaskInfo> { }
export interface TaskAkColumn extends AkColumnProps<TaskInfo> { }

export interface AkFormHistoryLogProps {
    taskID?: string;
    appID?: string;
    defID?: string;
    resID?: string;
    isPreview?: boolean;
    type?: "common" | "flowcraft";
    hideFlowChart?: boolean;
    addassisign?: number;
    selectApp?: AppCenterListResponse;
}
export interface AkFormHistoryLogStates {
    showFlowChart?: boolean;
    processLogList?: ProcessLog[];
    renderRowKey: any;
    fullscreen: boolean;
    isApplication: boolean;
}
@connect((state) => ({ selectApp: state.appCenter.selectApp, addassisign: state.addAssisign.addCount }))
export default class AkFormHistoryLog extends Component<AkFormHistoryLogProps, AkFormHistoryLogStates> {
    static loadflowchart?: (instID, defResID) => any;
    instID?: string;
    defResID?: string;
    container?: any;
    constructor(props, context) {
        super(props, context);
        this.state = {
            renderRowKey: [],
            fullscreen: true,
            isApplication: false
        };
    }

    componentDidMount() {
        const { taskID, appID, isPreview, defID } = this.props;
        if (isPreview) {
            this.setState({ processLogList: TempFlowHistory.ProcessLogList });
            return;
        }
        if (taskID) {
            TaskAPI.getProcessLog({ taskID: taskID }).then(data => {
                this.setState({ processLogList: data.Data });
            });
        } else if (appID) {
            ProcInstAPI.getProcessLog({ applicationID: appID }).then(data => {
                this.setState({ processLogList: data.Data });
            });
        } else if (defID) {
            this.defResID = this.props.resID;
            this.instID = "";
            this.setState({ isApplication: true });
            // TaskAPI.getNewProcessLog({ defID: defID }).then(data => {
            //     if (data.Status === 0) {
            //         this.instID = data.Data.TenantID;
            //         this.defResID = data.Data.ResourceID;
            //         this.setState({ isApplication: true });
            //     }
            // });
        }
    }
    componentWillReceiveProps(nextProps) {
        if ("addassisign" in nextProps && nextProps.addassisign != this.props.addassisign) {
            this.componentDidMount();
        }
    }

    static getFlowChart(isApp, instID, defResID, onCancel) {
        let beforLink = window.location.protocol + "//" + window.location.hostname
        let beforUrl = AkContext.getBranch() == AkContext.YeeOffice ? (isApp ? "/Home/workflow" : window.location.pathname) : (isApp ? `${beforLink}/Workflow/SitePages/Pages/index.aspx` : "./index.aspx");
        let title = AkGlobal.intl.formatMessage({ id: ApplyContentLocale.LogContentFlowchart });
        let url = beforUrl + `#/flow/display?instid=${instID}&defresid=${defResID}&isdlg=1`;
        let flowchart = AkFormHistoryLog.loadflowchart ? AkFormHistoryLog.loadflowchart(instID, defResID) : null;
        return <AkFlowChart
            url={url}
            title={title}
            isCamzoom
            flowchart={flowchart}
            onCancel={onCancel}
        ></AkFlowChart>
    }

    renderFlowcraft() {
        let { state: { showFlowChart }, props: { selectApp } } = this;
        return showFlowChart ? AkFormHistoryLog.getFlowChart(selectApp && true, this.instID, this.defResID, () => { this.setState({ showFlowChart: false }) }) : null;
        // let beforLink = window.location.protocol + "//" + window.location.hostname
        // let beforUrl = AkContext.getBranch() == AkContext.YeeOffice ? (selectApp ? "/Home/workflow" : window.location.pathname) : (selectApp ? `${beforLink}/Workflow/SitePages/Pages/index.aspx` : "./index.aspx");
        // let title = AkGlobal.intl.formatMessage({ id: ApplyContentLocale.LogContentFlowchart });
        // let url = beforUrl + `#/flow/display?instid=${this.instID}&defresid=${this.defResID}&isdlg=1`;
        // let flowchart = AkFormHistoryLog.loadflowchart ? AkFormHistoryLog.loadflowchart(this.instID, this.defResID) : null;
        // return showFlowChart ?
        //     <AkFlowChart
        //         url={url}
        //         title={title}
        //         isCamzoom
        //         flowchart={flowchart}
        //         onCancel={() => { this.setState({ showFlowChart: false }) }}
        //     ></AkFlowChart>
        //     : null;
    }

    render() {
        const { processLogList } = this.state;
        return <div>
            {this.renderFlowcraft()}
            {this.state.isApplication ? this.renderTaskTitle() : null}
            {
                processLogList && processLogList.map((entry) => {
                    return entry && entry.ProcInstModel && this.renderTaskTable(entry);
                })
            }</div>;
    }
    /**申请页面流程日志只显示title */
    renderTaskTitle() {
        const { hideFlowChart } = this.props;
        return <AkRow type="flex" justify="space-between" align="middle" style={{ fontSize: 14, backgroundColor: "#edecec", padding: "7px 8px" }}>
            <AkCol>
                <AkRow type="flex" gutter={10} justify="start" align="middle"></AkRow>
            </AkCol>
            {!hideFlowChart && <AkCol>
                <a onClick={() => {
                    this.setState({ showFlowChart: true });
                }}>
                    <FormattedMessage id={ApplyContentLocale.LogContentFlowchart}></FormattedMessage>
                </a>
            </AkCol>}
        </AkRow>;
    }

    /**
     * 是否是任务审批人或代理人
     * @param record 
     */
    isTaskOwner(record) {
        let uid = AkContext.getUser().AccountID;
        if (uid === record["AssigneeID"] || uid === record["DelegateID"]) {
            return true;
        } else {
            return false;
        }
    }

    getTaskLinkObj(record) {
        const { selectApp } = this.props;
        return {
            pathname: selectApp && selectApp.RelativeUrl ? `app/${selectApp.RelativeUrl}/form` : "form",
            query: {
                "pageid": record.TaskURL,
                "taskid": record.TaskID,
                "instid": record.ProcInstID
            }
        }
    }

    columnTaskName(name, record) {
        let reChild = record["Child"];
        if (reChild && reChild.length > 0) {
            return null;
        } else {
            let taskName = <div>{name}</div>;
            if (this.isTaskOwner(record)) {
                return <AkLink to={this.getTaskLinkObj(record)}>{taskName}</AkLink>
            } else {
                return taskName;
            }
        }
    }

    //人员信息列
    columnUser(url, record) {
        const { formatMessage } = AkGlobal.intl;
        let oldUser = null;//原处理人
        if (record["AssigneeID"] !== record["DelegateID"] && record["DelegateID"] !== "0") {
            let user = <AkAvatar value={record["DelegateID"]} type="text" />;
            oldUser = <span>{"("}{formatMessage({ id: HistoryLogLocale.HistoryLogOriginal })}{" : "}{user}{")"}</span>;
        }
        let reChild = record["Child"],
            manyTask = <span>{formatMessage({ id: HistoryLogLocale.HistoryLogMultiTask })}:{record.Name}{oldUser}</span>,
            urlData = url && url.startsWith("upload") ? (window["FilecenterUrl"] + url) : url,
            urlStr = AkContext.getUserDefaultPhoto(),// window["CDN"] + "images/user.jpg",
            img = !FlowcraftCommon.minSM ? <img style={{ verticalAlign: "middle" }} src={urlData ? urlData : urlStr} className="img-responsive" /> : null;
        let name;

        if (record.AssigneeID === "0")//领用任务
        {
            img = <img style={{ verticalAlign: "middle" }} src={urlStr} className="img-responsive" />;
            name = formatMessage({ id: HistoryLogLocale.HistoryLogWaitReceive });
            return reChild && reChild.length > 0 ?
                <div>{manyTask}</div> :
                <div>
                    {img}
                    <span style={{ height: "100%", display: "inline-block", verticalAlign: "middle", marginLeft: (!FlowcraftCommon.minSM ? 10 : 0) }}>{name}{oldUser}</span>
                </div>;
        } else {
            return reChild && reChild.length > 0 ? <div>{manyTask}</div> : <span><AkAvatar value={record.AssigneeID} type="combination" size={40} addonTextAfter={oldUser} /></span>;
        }
    }

    /**
     * 子任务入口或者在手机模式下的comments显示
     * @param text 
     * @param record 
     */
    renderIcon(text, record) {
        let { renderRowKey } = this.state;
        let rowKeyData = renderRowKey.find(l => l === record.TaskID);
        let reChild = record["Child"];
        let comment = record["Comment"];

        if (reChild && reChild.length > 0) {
            if (rowKeyData && rowKeyData.length > 0) {
                return <AkIcon onClick={() => {
                    AkUtil.remove(renderRowKey, v => v === record.TaskID);
                    this.setState({ renderRowKey });
                }} type="up"></AkIcon>;
            } else {
                return <AkIcon onClick={() => {
                    renderRowKey.push(record.TaskID);
                    this.setState({ renderRowKey });
                }} type="down"></AkIcon>;
            }
        } else if (FlowcraftCommon.minSM && comment && comment.length > 0) {
            let usr = this.columnUser(record["AssigneePhoto"], record);
            return <AkPopover trigger="click" title={usr} content={<pre className="workflow-comment">{comment}</pre>}><AkIcon style={{ color: 'blue' }} type="comments" /></AkPopover>
        }
        return null;
    }

    renderTaskTable(item: ProcessLog) {
        const { isPreview } = this.props;
        const { formatMessage } = AkGlobal.intl;
        const { hideFlowChart } = this.props;
        let columns: TaskAkColumn[] = [
            {
                title: formatMessage({ id: HistoryLogLocale.HistoryLogActorUser }),
                dataIndex: "AssigneePhoto",
                layout: AkColumnLayout.LeftTop,
                render: (url, record) => this.columnUser(url, record)
            }, {
                title: formatMessage({ id: HistoryLogLocale.HistoryLogSubtask }),
                key: "Name",
                hidden: FlowcraftCommon.minSM,
                dataIndex: "Name",
                render: (name, record) => {
                    return this.columnTaskName(name, record);
                    // let reChild = record["Child"];
                    // let temp = reChild && reChild.length > 0 ? null : name;
                    // return <AkHtml>{temp}</AkHtml>;
                }
            }, {
                title: formatMessage({ id: CommonLocale.Status }),
                key: "Outcome",
                layout: AkColumnLayout.RightTop,
                dataIndex: "Outcome",
                render: (outcome, record) => {
                    let recall = <span>{"( "}{formatMessage({ id: ApplyContentLocale.LabelRecall })}{" )"}</span>;
                    if (record.RecalledID === "0") {
                        recall = null;
                    }
                    const outcomeItem = outcomeDict[outcome] || [orange, "minus-circle"];
                    return <AkRow type="flex" justify="start" gutter={5} align="middle">
                        <AkCol><AkIcon className={`${outcomeItem[0]}`} type={outcomeItem[1]} ></AkIcon></AkCol>
                        <AkCol><FormattedMessage id={`task.outcome.${outcome || "Processing"}`} ></FormattedMessage>{recall}</AkCol>
                    </AkRow>;
                }
            }, {
                title: formatMessage({ id: HistoryLogLocale.HistoryLogStartTime }),
                key: "StartTimeStr",
                layout: AkColumnLayout.LeftBottom,
                dataIndex: "StartTimeStr",
                render: (txt, record) => {
                    return <AkDateLabel value={txt} />
                }
            }, {
                title: formatMessage({ id: HistoryLogLocale.HistoryLogEndTime }),
                key: "EndTimeStr",
                hidden: FlowcraftCommon.minSM,
                dataIndex: "EndTimeStr",
                render: (txt, record) => {
                    return <AkDateLabel value={txt} />
                }
            }, {
                title: formatMessage({ id: HistoryLogLocale.HistoryLogRemark }),
                key: "Comment",
                dataIndex: "Comment",
                hidden: FlowcraftCommon.minSM,
                render: (txt, record) => {
                    return <pre className="workflow-comment">{txt}</pre>
                }
            }, {
                key: "Icon",
                layout: AkColumnLayout.RightBottom,
                dataIndex: "Icon",
                render: (text, record) => {
                    return this.renderIcon(text, record);
                }
            }
        ];

        let isHidden = FlowcraftCommon.minSM ? "none" : "";
        let title = <AkRow key="historylog" type="flex" justify="space-between" align="middle" >
            <AkCol>
                <AkRow type="flex" gutter={10} justify="start" align="middle">
                    <AkCol style={{ display: isHidden }}>
                        <FormattedMessage id={ApplyContentLocale.LabelSubmitDate}></FormattedMessage>
                    </AkCol>
                    <AkCol>
                        <AkDateLabel value={item.ProcInstModel.StartTimeStr} />
                    </AkCol>
                    <AkCol style={{ display: isHidden }}>
                        <FormattedMessage id={ApplyContentLocale.LogContentStatus}></FormattedMessage>
                    </AkCol>
                    <AkCol className={`${colorDict[item.ProcInstModel.Status]}`}>
                        <FormattedMessage id={ApplicationStatusLocale + item.ProcInstModel.Status}></FormattedMessage>
                    </AkCol>
                </AkRow>
            </AkCol>
            {!hideFlowChart && <AkCol>
                <a onClick={() => {
                    if (isPreview) {
                        return;
                    }
                    this.defResID = item.DefResourceID;
                    this.instID = item.ProcInstModel.ProcInstID;
                    this.setState({ showFlowChart: true });
                }}>
                    <FormattedMessage id={ApplyContentLocale.LogContentFlowchart}></FormattedMessage>
                </a>
            </AkCol>}
        </AkRow>;
        return <TaskTable pagination={false} showHeader={false} className="ak-form-workflow-history" rowKey="TaskID"
            key={item.ProcInstModel.ProcInstID}
            dataSource={item.TaskList}
            expandedRowKeys={this.state.renderRowKey}
            expandIconAsCell={false}
            rowClassName={record => { return "hide parentstyle"; }}
            expandedRowRender={record => this.renderChildrenTable(record.Child, columns, this.state.renderRowKey)}
            columns={columns} title={() => title}></TaskTable>;
    }

    renderChildrenTable(item, columns, renderRowKey) {
        return <AkRow style={{ position: "relative" }}>
            <AkCol className="ak-form-workflow-spaceBorder"><div className="leftBorder"></div></AkCol>
            <AkCol>
                <ChildrenTable pagination={false}
                    className="ak-form-workflow-history"
                    rowKey="TaskID"
                    dataSource={item}
                    columns={columns}
                    expandIconAsCell={false}
                    expandedRowKeys={renderRowKey}
                    rowClassName={record => { return "hide childrenstyle"; }}
                    expandedRowRender={record => this.renderChildrenTable(record.Child, columns, renderRowKey)}
                ></ChildrenTable>
            </AkCol>
        </AkRow>;
    }
}
export class TempFlowHistory {
    static ProcessLogList: ProcessLog[] = [
        {
            "ProcInstModel": {
                "ProcInstID": "container1",
                "StartTimeStr": "2017-01-01 01:01",
                "Status": "1"
            },
            "TaskList": [
                {
                    "TaskID": "container2",
                    "Name": "name",
                    "AssigneeName": "administrator",
                    "DelegateName": "administrator",
                    "Status": 1
                }]
        }
    ];
}
