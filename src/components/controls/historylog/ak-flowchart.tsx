import * as React from 'react'
import { Component } from 'react'
import { AkTable, AkColumnProps, AkIcon, AkModal } from "../index";
import { TaskInfo } from "../../../api/flowcraft/task";
import { Icon } from 'antd';

/** 自定义表单 流程日志 */
export class TaskTable extends AkTable<TaskInfo> { }
export class ChildrenTable extends AkTable<TaskInfo> { }
export interface TaskAkColumn extends AkColumnProps<TaskInfo> { }

export interface AkFlowChartProps {
    title?: string | React.ReactNode;
    isCamzoom?: boolean;
    flowchart?: any;
    url?: any;
    onCancel?:()=>void;
}
export interface AkFlowChartStates {
    showFlowChart?: boolean;
    fullscreen: boolean;
}

export class AkFlowChart extends Component<AkFlowChartProps, AkFlowChartStates> {
    container?:any;
    constructor(props, context) {
        super(props, context);
        this.state = {
            fullscreen: true
        };
    }
    //修复IE下流程图，无法加载bug
    componentDidMount(){
        const { flowchart, url } =  this.props;
        if(!flowchart&&document["frames"]){
            setTimeout(()=>{
                document["frames"]["iframeID"].location.href=url
            },0);
        }
    }

    renderFlowcraft() {
        const { fullscreen } = this.state;
        const { flowchart, url, isCamzoom } =  this.props;
        const notMobal = window.innerWidth > 768;
        const titleFsIcon = isCamzoom ? notMobal ? fullscreen ? <AkIcon  type="shrink" /> : <AkIcon type="arrows-alt" /> : null : null;
        const title = this.props.isCamzoom ? <div style={{ paddingRight: 48 }}>
            <span style={{ float: "left" }}>{this.props.title}</span>
            <span className="ak-modal-operation" onClick={() => this.setState({ fullscreen: !fullscreen })}>{titleFsIcon}</span>
        </div> : this.props.title;
        const className = !notMobal ? {className:"ak-flowcraft-display"} : {};
        return <AkModal
                width={800}
                title={title}
                {...className}
                visible
                footer={null}
                fullscreen={notMobal && fullscreen}
                onCancel={() => {this.props.onCancel()}}>
                {flowchart ?
                    <div style={{
                        border: "1px solid #DDD",
                        overflow: "hidden",
                        height: notMobal ? fullscreen ? "100%" : 617 : "100%",
                        width: notMobal ? fullscreen ? "100%" : 760 : "100%"
                    }}>
                     {flowchart}
                    </div> : <iframe
                        id="iframeID" 
                        style={{
                            border: 0
                        }}
                        width="100%"
                        height={fullscreen ? "100%" : "617px" }
                        scrolling="no"
                        src={url}></iframe>}
            </AkModal>
    }
    render() {
        return <div>
                {this.renderFlowcraft()}
            </div>;
    }
}