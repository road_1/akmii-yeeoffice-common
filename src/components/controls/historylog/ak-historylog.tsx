import * as React from 'react'
import { Component, ReactNode } from 'react';
import { AkTable, AkColumnProps, AkIcon, AkRow, AkCol, AkModal } from "../index";
import { AkGlobal, FlowcraftCommon } from '../../../util/common';
import { AkUtil } from '../../../util/util';
import { CommonLocale, ApplyContentLocale } from '../../../locales/localeid';
import { AkFlowChart } from "./ak-flowchart";
import { AkContext } from '../../../index';
import { AkDateLabel } from '../AkDateLabel';

export const [red, orange, green, black] = ["color-red", "color-orange", "color-green", "color-black"];
export const colorDict = [
    null, // 开始
    orange, // 运行中
    green, // 已结束
    red, // 拒绝
    red, // 出错
    black, // 撤回流程
    black // 取消流程
];
export const outcomeDict = {
    "Completed": [green, "check-circle"],
    "Approved": [green, "check-circle"],
    "Rejected": [red, "close-circle"],
    "Revoked": [red, "close-circle"],
    "Cancelled": [black, "minus-circle"],
    "Terminated": [black, "minus-circle"]
};

/** 流程日志Table */
class HistoryTable extends AkTable<TaskModel> { }

export interface AkHistoryLogProps {
    columns?: any[];
    data?: ProcessLog[];
    flowChart?: (instID: string, defResID: string) => React.ReactNode;
}
export interface AkHistoryLogStates {
    value?: ProcessLog[];
    columns?: any[];
    /**显示流程图 */
    showFlowChart?: boolean;
    /**折叠图标 */
    expendIcon?: "up" | "down";
    /**显示行 */
    expendKeys?: string[];
}

export class AkHistoryLog extends Component<AkHistoryLogProps,
    AkHistoryLogStates> {
    instID?: string;
    defResID?: string;

    constructor(props, context) {
        super(props, context);
        const { data, columns } = this.props;
        if (columns) {
            columns.push({
                key: "Icon",
                dataIndex: "Icon",
                rowClassName: (record, index) => { record.Child.length > 0 ? 'hide' : '' },
                render: (text, record) => {
                    let icon: "up" | "down" = "down";
                    const { expendKeys } = this.state;
                    if (expendKeys.findIndex(e => e === record.TaskID) > -1) {
                        icon = "up";
                    }
                    return record.Child && record.Child.length > 0 ? <AkIcon type={icon} onClick={() => this.expendChange(record)}></AkIcon> : null
                }
            });
        }

        this.state = {
            value: data || null,
            columns: columns || [],
            showFlowChart: false,
            expendIcon: "down",
            expendKeys: []
        }
    }

    componentWillReceiveProps(nextProps) {
        if ("data" in nextProps && nextProps.data !== this.props.data) {
            this.setState({ value: nextProps.data });
        }
        if ("columns" in nextProps && nextProps.columns !== this.props.columns) {
            this.setState({ columns: nextProps.columns });
        }
    }
    /**折叠行事件*/
    expendChange(data) {
        const { expendKeys, expendIcon } = this.state;
        let icon: "up" | "down" = expendIcon === "up"
            ? "down"
            : "up";
        let index = expendKeys.findIndex(e => e === data.TaskID);
        if (index > -1) {
            AkUtil.remove(expendKeys, e => e === data.TaskID);
        } else {
            expendKeys.push(data.TaskID);
        }
        this.setState({ expendKeys: expendKeys, expendIcon: icon });
    }
    /**折叠行内容*/
    expandedRowRender(data: TaskModel[]) {
        const { expendKeys, columns } = this.state;
        return data && data.length > 0
            ? <AkRow style={{ position: "relative" }}>
                <AkCol className="ak-form-workflow-spaceBorder"><div className="leftBorder"></div></AkCol>
                <AkCol>
                    <HistoryTable
                        rowKey="TaskID"
                        className="ak-form-workflow-history"
                        pagination={false}
                        expandIconAsCell={false}
                        columns={columns}
                        dataSource={data}
                        rowClassName={record => 'hide'}
                        defaultExpandedRowKeys={expendKeys}
                        expandedRowRender={(record: TaskModel) => this.expandedRowRender(record.Child)}>
                    </HistoryTable>
                </AkCol>
            </AkRow>
            : null;
    }
    /**流程图 Modal */
    renderFlowChartModal() {
        const { formatMessage } = AkGlobal.intl
        let flowchart = this.props.flowChart && this.props.flowChart(this.instID, this.defResID);
        // let beforUrl= AkContext.getBranch() == AkContext.YeeOffice? window.location.pathname:"./index.aspx";

        return this.state.showFlowChart
            ? <AkFlowChart
                url={`#/flow/display?instid=${this.instID}&defresid=${this.defResID}&isdlg=1`}
                title={formatMessage({ id: ApplyContentLocale.LogContentFlowchart })}
                isCamzoom
                flowchart={flowchart}
                onCancel={() => { this.setState({ showFlowChart: false }) }}
            ></AkFlowChart>
            : null;
    }

    render() {
        let topThis = this;
        let isHidden = FlowcraftCommon.minSM ? "none" : "";
        const { expendKeys, value, columns } = this.state;
        const { formatMessage } = AkGlobal.intl;

        if (!value) {
            return null;
        }
        if (value) {
            return <div>
                {value.map((entry, index) => {
                    let title = <AkRow className="ak-form-history-title" type="flex" justify="space-between" align="middle" gutter={10}>
                        <AkCol>
                            <AkRow type="flex" gutter={10} justify="start" align="middle">
                                <AkCol style={{ display: isHidden }}>
                                    {formatMessage({ id: ApplyContentLocale.LabelSubmitDate })}
                                </AkCol>
                                <AkCol>
                                    <AkDateLabel value={entry.ProcInstModel.StartTimeStr} />
                                </AkCol>
                                <AkCol style={{ display: isHidden }}>
                                    {formatMessage({ id: ApplyContentLocale.LogContentStatus })}
                                </AkCol>
                                <AkCol className={`${colorDict[entry.ProcInstModel.Status]}`}>
                                    {formatMessage({ id: "model.application.status." + entry.ProcInstModel.Status })}
                                </AkCol>
                            </AkRow>
                        </AkCol>
                        <AkCol style={{ textAlign: "right" }}>
                            <a onClick={() => {
                                {
                                    this.instID = entry.ProcInstModel.ProcInstID;
                                    this.defResID = entry.DefResourceID;
                                    this.setState({ showFlowChart: true });
                                }
                            }}>
                                {formatMessage({ id: ApplyContentLocale.LogContentFlowchart })}
                            </a>
                        </AkCol>
                    </AkRow>;
                    return <HistoryTable
                        rowKey="TaskID"
                        key={index}
                        pagination={false}
                        expandIconAsCell={false}
                        columns={columns}
                        dataSource={entry.TaskList}
                        rowClassName={record => 'hide'}
                        defaultExpandedRowKeys={expendKeys}
                        expandedRowRender={(record: TaskModel) => topThis.expandedRowRender(record.Child)}
                        title={() => title}></HistoryTable>
                })}
                {topThis.renderFlowChartModal()}
            </div>
        } else {
            return null;
        }
    }
}

export interface BaseModel {
    /** 租户ID Index */
    TenantID?: string;
    /** 创建时间 */
    Created?: Date;
    /** 创建时间对应的字符串形式 */
    CreatedStr?: string;
    /** 创建人名称 */
    CreatedByName?: string;
    /** 创建人 */
    CreatedBy?: string;
    /** 修改时间 */
    Modified?: Date;
    /** 修改时间对应的字符串形式 */
    ModifiedStr?: string;
    /** 修改人名称 */
    ModifiedByName?: string;
    /** 修改人 */
    ModifiedBy?: string;
}
export interface ProcessLog extends BaseModel {
    /** 定义资源ID */
    DefResourceID?: string;
    /** 图片资源ID */
    ImgResourceID?: string;
    /** 实例 */
    ProcInstModel?: ProcInstModal;
    /** 任务列表 */
    TaskList: TaskModel[];
}
export interface ProcInstModal extends BaseModel {
    /** ProcInstID */
    ProcInstID?: string;
    /** Name */
    Name?: string;
    /** 父级流程实例ID */
    ParentInstID?: string;
    /** ProcDefID */
    ProcDefID?: string;
    /** 流程定义版本信息 */
    Version?: number;
    /** 流程定义类别 */
    CategoryID?: string;
    /** 流程定义类别名称 */
    CategoryName?: string;
    /** StartTime */
    StartTime?: Date;
    /** StartTime */
    StartTimeStr?: string;
    /** EndTime */
    EndTime?: Date;
    /** EndTime */
    EndTimeStr?: string;
    /** 流程运行毫秒数 */
    DURATION?: string;
    /** StartUserID */
    StartUserID?: string;
    /** StartActID */
    StartActID?: string;
    /** EndActID */
    EndActID?: string;
    /** 0:未启动，1:运行中，2:结束，3:已删除 */
    Status?: number;
    /** Comment */
    Comment?: string;
}
export interface TaskModel extends BaseModel {
    /** TaskID */
    TaskID: string;
    /** 流程实例ID */
    ProcInstID: string;
    /** 流程定义ID */
    ProcDefID: string;
    /** 流程名称 */
    ProcDefName: string;
    /** 流程编号 */
    FlowNo: string;
    /** 类别ID */
    CategoryID: string;
    /** 类别名称 */
    CategoryName: string;
    /** 执行ID */
    ExecutionID: string;
    /** 任务对应的ActivityID */
    ActivityID: string;
    /** Name */
    Name: string;
    /** ParentTaskID */
    ParentTaskID: string;
    /** Description */
    Description: string;
    /** OwnerID */
    OwnerID: string;
    /** 任务的分配对象 */
    AssigneeID: string;
    /** 分配人名字 */
    AssigneeName: string;
    /** 分配人头像 */
    AssigneePhoto: string;
    /** 任务的代理人 */
    DelegateID: string;
    /** 代理人名字 */
    DelegateName: string;
    /** 代理人头像 */
    DelegatePhoto: string;
    /** 优先级 */
    Priority: number;
    /** StartTime */
    StartTime: Date;
    /** on update CURRENT_TIMESTAMP(3) */
    StartTimeStr: string;
    /** ClaimTime */
    ClaimTime: Date;
    /** ClaimTime */
    ClaimTimeStr: string;
    /** EndTime */
    EndTime: Date;
    /** EndTime */
    EndTimeStr: string;
    /** Duration */
    Duration: string;
    /** DueDate */
    DueDate: Date;
    /** DueDate */
    DueDateStr: string;
    /** DeleteReason */
    DeleteReason: string;
    /** Outcome */
    Outcome: string;
    /** Comment */
    Comment: string;
    /** TaskURL */
    TaskURL: string;
    /** 是否存储表单数据 */
    IsSaveFormData: boolean;
    /** FormDataID */
    FormDataID: string;
    /** Ext */
    Ext: string;
    /** 状态 */
    Status: any;

    Child: any[];
}

class ProcInstItemStyle {
    static contentTableStyle: React.CSSProperties = {
        marginTop: '20px'
    };
    static aboutTableStyle: React.CSSProperties = {
        border: '1px solid #FFFFF',
        padding: '10px'
    };
}