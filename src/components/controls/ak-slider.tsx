import * as React from "react";
import {Component} from "react";
import {Slider} from "antd";

export interface AkSliderMarks {
    [key: number]: React.ReactNode | {
        style: React.CSSProperties;
        label: React.ReactNode;
    };
}
export declare type AkSliderValue = number | [number, number];
export interface AkSliderProps {
    prefixCls?: string;
    tooltipPrefixCls?: string;
    range?: boolean;
    min?: number;
    max?: number;
    step?: number | null;
    marks?: AkSliderMarks;
    dots?: boolean;
    value?: AkSliderValue;
    defaultValue?: AkSliderValue;
    included?: boolean;
    disabled?: boolean;
    vertical?: boolean;
    onChange?: (value: AkSliderValue) => void;
    onAfterChange?: (value: AkSliderValue) => void;
    tipFormatter?: null | ((value: number) => React.ReactNode);
    className?: string;
    id?: string;
}
export interface AkSliderStates {
}
export class AkSlider extends Component < AkSliderProps,
    AkSliderStates > {
    constructor(props) {
        super(props);
    }

    render() {
        const props = this.props;
        return <Slider {...props}/>
    }
}
