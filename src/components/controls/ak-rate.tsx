import * as React from "react";
import {Component} from "react";
import {Rate} from "antd";

export interface AkRateProps {
    prefixCls?: string;
    count?: number;
    value?: number;
    defaultValue?: number;
    /**是否允许半选 */
    allowHalf?: boolean;
    allowClear?: boolean;
    disabled?: boolean;
    /**选择时的回调 */
    onChange?: (value: number) => any;
    /**鼠标经过时数值变化的回调 */
    onHoverChange?: (value: number) => any;
    /**自定义字符 */
    character?: React.ReactNode;
    className?: string;
    style?: React.CSSProperties;
}
    
export interface AkRateStates {}
export class AkRate extends Component < AkRateProps,
AkRateStates > {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return <Rate  {...this.props}></Rate>
    }
}
class AkRateStyle {}