/**
 * 表达式编辑器，用于条件和邮件模板
 * 邮件模板的formatValue需要额外加<var></var>
 */
import * as React from "react";
import {Component} from "react";
import {AkGlobal} from "../../util/common";
import {CommonLocale} from "../../locales/localeid";
import {AkList} from "./ak-list";
import {AkButton} from "./ak-button";
import * as classNames from "classnames";

export interface AkHierarchyListItem {
    id?: string;
    title?: string; //当前级别的显示名称
    locale?: string;
    fullname?: string; //包含层级的完整名称
    items?: AkHierarchyListItem[];
    value?: any; //value为空的项不能添加，作为二级分类
    valueType?: "number" | "string" | "datetime" | "taskoutcome" | "metadata" | "user" | "variables" | "tasks" | "tasks" | "dict"| "list" | string;
    attr?: Object; //额外附加属性
    filter?: string; //用来匹配过滤项，父级需要包含下级所有的filter类型 |any|str|func|usr|mail|date|org|loc|url|
}


export interface AkHierarchyListProp {
    dataSource?: AkHierarchyListItem[];
    className?: string;
    onInsert?: (value: AkHierarchyListItem) => void;
    filters?: string[];
    retrieveItems?: (parent: AkHierarchyListItem) => Promise<AkHierarchyListItem[]>; //子item的异步加载方法
    insertButtonLabel?: string;
    dynamicWidth?: boolean; //动态宽度（根据横屏list）
    maxList?: number; //横向一屏显示多少个list
}

export interface AkHierarchyListState {
    checkedItems?: AkHierarchyListItem[];
    exprList?: any[];
}

export class AkHierarchyList extends Component < AkHierarchyListProp,
    AkHierarchyListState > {

    static defaultProps = {
        dynamicWidth: true,
        maxList: 4
    }

    static filter(item: AkHierarchyListItem, filters) {
        if (item.filter === '|any|') {
            return true;
        }
        for (var i = 0; i < filters.length; i++) {
            if (item.filter.indexOf('|' + filters[i] + '|') >= 0) {
                return true;
            }
        }
        return false;
    }

    static filterItems(items, filters) {
        let rs = items;
        if (filters && filters.length > 0 && items && items.length > 0) {
            if (filters.length === 1 && filters[0] === 'any') {
                //any 不做filter
            } else {
                rs = items.filter(i => {
                    return this.filter(i, filters);
                });
            }
        }
        return rs;
    }

    constructor(props, context) {
        super(props, context);
        this.state = {checkedItems: [], exprList: []};
    }

    exprListContainer: HTMLDivElement; //list的容器

    /**
     * list选中状态发生变化
     * @param keys
     * @param level
     */
    listCheckChanged(keys, level) {
        let {checkedItems} = this.state;

        let items = this.props.dataSource;
        if (level > 0) {
            items = checkedItems[level - 1].items;
        }

        checkedItems.splice(level, checkedItems.length);
        if (keys.length > 0) {

            const key = keys[0];
            let selected = items.find(i => i.id === key);
            if (selected) {
                checkedItems.push(selected);
            }
        }

        this.setState({checkedItems: checkedItems}, () => {
            this.getExprLists().then((d: any[]) => {
                this.setState({exprList: d}, () => {
                    if (d.length > 0) {
                        //自动滚动到最后一个list
                        let nodes = this.exprListContainer.childNodes;
                        nodes.length > 0 && (nodes[nodes.length - 1] as HTMLDivElement).scrollIntoView();
                    }
                });
            });
        });
    }

    /**
     * 根据对象类型，构造子对象数组
     * @param item
     * @returns {any}
     */
    async parseExprItemsByValueType(item) {
        const {retrieveItems} = this.props;
        return retrieveItems ? retrieveItems(item) : item.items;
    }


    /**
     * 渲染单个列表
     * @param exprItems
     * @param level
     * @returns {any}
     */
    getExprlist(exprItems: AkHierarchyListItem[], level) {

        const {formatMessage} = AkGlobal.intl;
        const {checkedItems} = this.state;

        let k = [];
        let childChecked = checkedItems[level];
        if (childChecked) {
            k.push(childChecked.id);
        }

        let items = AkHierarchyList.filterItems(exprItems, this.props.filters).map(d => {
            return {key: d.id, title: d.locale ? formatMessage({id: d.locale}) : d.title}
        });

        if (items.length > 0) {
            return <AkList key={level} maxSelection={1}
                           dataSource={items}
                           checkedKeys={k} footer={()=>this.renderListFooter(exprItems, k)}
                           onChange={(keys)=>this.listCheckChanged(keys, level)}></AkList>

        } else {
            return undefined;
        }
    }

    renderListFooter(exprItems: AkHierarchyListItem[], checkedKeys: string[]) {
        const {insertButtonLabel} = this.props;
        let item: AkHierarchyListItem;
        if (checkedKeys.length > 0) {
            let key = checkedKeys[0];
            item = exprItems.find(e => e.id === key);
        }

        let disabled = !(item && item.value);

        return disabled?null:<AkButton onClick={()=>this.insertListItem(item)} type="ghost"
                         size="small" disabled={disabled}
                         className="ak-hierarchy-list-insert">{insertButtonLabel ? insertButtonLabel : AkGlobal.intl.formatMessage({id: CommonLocale.Insert})}</AkButton>;
    }

    insertListItem(item: AkHierarchyListItem) {
        this.props.onInsert && this.props.onInsert(item);
    }

    async getExprLists(): Promise<any[]> {
        const {checkedItems} = this.state;

        let lastSelected;
        let result = [];
        for (let i = 0; i < checkedItems.length; i++) {
            let item = checkedItems[i];
            lastSelected = item;
            item.items = await this.parseExprItemsByValueType(item);
            //GO TO 变量类型过滤失败
            
            let k = [];
            let childChecked = checkedItems[i + 1];
            if (childChecked) {
                k.push(childChecked.title);
            }

            if (item.items && item.items.length > 0) {
                result.push(this.getExprlist(item.items, i + 1));
            }
        }
        return result;
    }


    render() {
        const {exprList} = this.state;
        const {className, maxList, dynamicWidth} = this.props;
        const cls = classNames("ak-hierarchy-list", className);
        const listCount = (1 + exprList.length);
        const containerWidth = (dynamicWidth ? (listCount > maxList ? maxList : listCount) : maxList) * 190;
        const width = listCount * 190;
        return <div className={cls} style={{width:containerWidth}}>
            <div style={{width:width}} ref={ref=>this.exprListContainer = ref}>
                {this.getExprlist(this.props.dataSource, 0)}
                {exprList}
            </div>
        </div>

    }
}
