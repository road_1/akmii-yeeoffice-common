import * as React from 'react'
import { Tag, Input, Tooltip, Icon } from 'antd';
interface EditableTagGroupProps {
    value?: string[];
    inputSiza?: "large" | "default" | "small",
    inputClassName?: string;
    inputplaceholder?: string;
    onBlurConfirm?: boolean;/**输入框失去光标-添加标签 */
    tagItemClassName?: string;
    onChange?: (value: string[]) => void;
}
interface EditableTagGroupStates {
    tags: string[];
    inputValue: string;
}

export class AkEditableTagGroup extends React.Component<EditableTagGroupProps, EditableTagGroupStates> {
    input: any;
    constructor(props, context) {
        super(props, context);
        this.state = {
            tags: [],
            inputValue: '',
        };
    }
    componentWillReceiveProps(nextProps: EditableTagGroupProps) {
        if (nextProps.value !== this.state.tags) {
            if (nextProps.value) {
                this.setState({ tags: nextProps.value, inputValue: '' });
            }
        }
    }
    componentWillMount() {
        if (this.props.value) {
            this.setState({ tags: this.props.value });
        }
    }


    handleClose = (removedTag) => {
        const tags = this.state.tags.filter(tag => tag !== removedTag);
        this.setState({ tags });
    }


    handleInputChange = (e) => {
        this.setState({ inputValue: e.target.value });
    }

    handleInputConfirm = () => {
        const state = this.state;
        const inputValue = state.inputValue;
        let tags = state.tags;
        if (inputValue && tags.indexOf(inputValue) === -1) {
            tags.push(inputValue);
        }
        this.setState({
            tags: tags,
            inputValue: '',
        });
        if (this.props.onChange) {
            this.props.onChange(tags);
        }
    }
    onBlurhandleInputConfirm = () => {
        if (this.props.onBlurConfirm === true) {
            this.handleInputConfirm();
        }
    }


    saveInputRef = input => this.input = input;

    render() {
        const { tags, inputValue } = this.state;
        const { inputClassName, inputSiza, tagItemClassName, inputplaceholder } = this.props;
        return (
            <div>
                {tags.map((tag, index) => {
                    const isLongTag = tag.length > 20;
                    const tagElem = (
                        <Tag className={tagItemClassName ? tagItemClassName : ""} style={{ marginRight: '5px', marginBottom: '10px' }} key={tag} closable={true} afterClose={() => this.handleClose(tag)}>
                            {isLongTag ? `${tag.slice(0, 20)}...` : tag}
                        </Tag>
                    );
                    return isLongTag ? <Tooltip title={tag} key={tag}>{tagElem}</Tooltip> : tagElem;
                })}
                <Input maxLength={"100"} onKeyDown={e => {
                    if (e["keyCode"] === 13) {
                        e.stopPropagation();
                        e.preventDefault();
                    }
                }}
                    className={inputClassName ? inputClassName : ""}
                    ref={this.saveInputRef}
                    placeholder={inputplaceholder ? inputplaceholder : ""}
                    type="text"
                    size={inputSiza ? inputSiza : "small"}
                    style={{ width: 200 }}
                    value={inputValue}
                    onChange={this.handleInputChange}
                    onBlur={this.onBlurhandleInputConfirm}
                    onPressEnter={this.handleInputConfirm}
                />
            </div>
        );
    }
}