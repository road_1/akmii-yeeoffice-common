import * as React from "react";
import { Component } from "react";
import { AkConstants } from "../../util/common";
import { InputNumber } from "antd";
import { AkUtil } from "../../util/util";

export interface AkInputNumberProps {
    prefixCls?: string;
    min?: number;
    max?: number;
    value?: number;
    step?: number | string;
    defaultValue?: number;
    onKeyDown?: React.FormEventHandler<any>;
    onChange?: (value: number | string | undefined) => void;
    disabled?: boolean;
    size?: 'large' | 'small' | 'default';
    formatter?: (value: number | string | undefined) => string;
    parser?: (displayValue: string | undefined) => string;
    placeholder?: string;
    style?: React.CSSProperties;
    className?: string;
    onBlur?: React.FormEventHandler<any>;
    onFocus?: React.FormEventHandler<any>;
    readOnly?: boolean;
    digit?: number;
    // tabIndex?: number;
    name?: string;
    id?: string;
    precision?: number;
    triggerChangeOnBlur?: boolean; //当blur的时候才触发change通知
    autoFocus?: boolean;
    onClick?: React.FormEventHandler<any>;
}

export interface AkInputNumberStates {
    value?: any;
    digit?: number;
}

export class AkInputNumber extends Component<AkInputNumberProps, AkInputNumberStates> {
    static defaultProps = {
        prefixCls: 'ant-input-number',
        step: 1,
        digit: undefined
    };
    constructor(props, context) {
        super(props, context);
        let value = ("value" in props) ? props.value : props.defaultValue;
        let digit = this.getDigit(props.digit);
        this.focus = props.autoFocus;
        this.state = { value: this.parseValue(value, digit), digit: digit };
    }
    parseValue(value, digit?) {
        const { max, min } = this.props;
        if (value != null) {
            let d;
            if (digit === undefined || digit == null) {
                d = this.state.digit;
            } else {
                d = digit;
            }
            if (isNaN(value)) {
                value = parseFloat(value);
                if (d > -1) {
                    value = AkUtil.round(value, d);
                }
            }

            if (isNaN(value)) {
                value = "";
            } else {
                const myMax = max === undefined ? AkConstants.NUMBER_MAX_SAFE : max;
                const myMin = min === undefined ? AkConstants.NUMBER_MIN_SAFE : min;
                if (value > myMax) {
                    value = myMax;
                }
                if (value < myMin) {
                    value = myMin;
                }
            }
        }
        return value;
    }

    componentWillReceiveProps(nextProps) {
        if ("value" in nextProps && nextProps.value !== this.props.value) {
            const newV = this.parseValue(nextProps.value);
            if (newV !== this.state.value) {
                this.setState({ value: newV });
            }
        }

        if ("digit" in nextProps && nextProps.digit !== this.props.digit) {
            this.setState({ digit: this.getDigit(nextProps.digit) });
        }
    }

    getDigit(digit) {
        let tempDigit;
        if (digit === undefined || digit === null) {
            tempDigit = -1;
        } else {
            if (AkUtil.isNumber(digit)) {
                tempDigit = digit;
            } else {
                tempDigit = parseInt(digit + "");
            }
            tempDigit = tempDigit > 15 ? 15 : tempDigit;
        }
        return tempDigit;
    }

    onChange(v) {
        // const { max, min } = this.props;
        const { digit, value } = this.state;

        // const myMax = max === undefined ? AkConstants.NUMBER_MAX_SAFE : max;
        // const myMin = min === undefined ? AkConstants.NUMBER_MIN_SAFE : min;
        // if (v && v > myMax) {
        //     AkMessage.warning(AkGlobal.intl.formatMessage({id: CommonLocale.TipExceedMax}, {max: max}));
        //     return;
        // } else if (v < myMin) {
        //      AkMessage.warning(AkGlobal.intl.formatMessage({id: CommonLocale.TipLessMin}, {min: min}));
        //     return;
        // }
        if (value === "-" && v === 0) {
            v = "-0";
        }
        if (v === "" || v === "-" || v == "-0") {
            this.setState({ value: v });
            return;
        }

        let arry = (v + "").split('.');
        if (arry.length > 1) {
            if (arry[0] === "") {
                //值以.开头，自动增加0
                arry[0] = "0";
            }

            if (arry[1] === "") {
                //输入了小数点，还没有其它数值直接更新
                this.setState({ value: arry[0] + "." });
                return;
            }

            // if (arry[1].length > 15) {
            //     arry[1] = arry[1].substr(0, 15);
            // }            
        }

        const newV = this.parseValue(v);
        this.triggerChange(newV);

        // if (triggerChangeOnBlur || !("value" in this.props)) {
        //     this.setState({ value: newV });
        // }

        // if (onChange && !triggerChangeOnBlur) {
        //     onChange(newV);
        // }
    }

    triggerChange(value, force?) {
        const { triggerChangeOnBlur, onChange } = this.props;
        //如果是blur后通知，change不触发
        if (force || triggerChangeOnBlur || !("value" in this.props)) {
            this.setState({ value: value });
        }
        if (onChange && (force || !triggerChangeOnBlur)) {
            if (!("value" in this.props) || this.props.value !== value) {
                //当外部值和内部值一致时，不触发onchange
                onChange(value);
            }
        }
    }

    onKeyDown(e) {
        const { onKeyDown } = this.props;
        if (e.keyCode === 13) {
            e.stopPropagation();
            e.preventDefault();
        }
        this.recordCursorPosition(e);
        onKeyDown && onKeyDown(e);
    }

    onKeyUp(e) {
        this.recordCursorPosition(e);
    }

    onBlur(e) {
        const { triggerChangeOnBlur, onBlur } = this.props;
        this.triggerChange(this.state.value, triggerChangeOnBlur);
        this.focus = false;
        onBlur && onBlur(e);
    }

    onFocus(e) {
        const { onFocus } = this.props;
        this.focus = true;
        this.recordCursorPosition(e);
        onFocus && onFocus(e);
    }

    componentDidUpdate(prevProps, prevState) {
        try {
            // Firefox set the input cursor after it get focused.
            // This caused that if an input didn't init with the selection,
            // set will cause cursor not correct when first focus.
            // Safari will focus input if set selection. We need skip this.
            if (this.cursorStart !== undefined && this.focus) {
                // In most cases, the string after cursor is stable.
                // We can move the cursor before it

                if (
                    // If not match full str, try to match part of str
                    !this.partRestoreByAfter(this.cursorAfter) && this.state.value !== this.props.value
                ) {
                    // If not match any of then, let's just keep the position
                    // TODO: Logic should not reach here, need check if happens
                    let pos = this.cursorStart + 1;

                    // If not have last string, just position to the end
                    if (!this.cursorAfter) {
                        pos = this.input.value.length;
                    } else if (this.lastKeyCode === 8) {
                        pos = this.cursorStart - 1;
                    } else if (this.lastKeyCode === 46) {
                        pos = this.cursorStart;
                    }
                    this.fixCaret(pos, pos);
                } else if (this.currentValue === this.input.value) {
                    // Handle some special key code
                    switch (this.lastKeyCode) {
                        case 8:
                            this.fixCaret(this.cursorStart - 1, this.cursorStart - 1);
                            break;
                        case 46:
                            this.fixCaret(this.cursorStart + 1, this.cursorStart + 1);
                            break;
                        default:
                        // Do nothing
                    }
                }
            }
        } catch (e) {
            // Do nothing
        }

        // Reset last key
        this.lastKeyCode = null;
    }


    //记录选中位置
    focus;
    input;
    cursorStart;
    cursorEnd;
    currentValue;
    cursorBefore;
    cursorAfter;
    lastKeyCode;

    restoreByAfter = (str) => {
        if (str === undefined) return false;

        const fullStr = this.input.value;
        const index = fullStr.lastIndexOf(str);

        if (index === -1) return false;

        if (index + str.length === fullStr.length) {
            this.fixCaret(index, index);

            return true;
        }
        return false;
    };

    partRestoreByAfter = (str) => {
        if (str === undefined) return false;

        // For loop from full str to the str with last char to map. e.g. 123
        // -> 123
        // -> 23
        // -> 3
        return Array.prototype.some.call(str, (_, start) => {
            const partStr = str.substring(start);

            return this.restoreByAfter(partStr);
        });
    };

    recordCursorPosition(e?) {
        // Record position
        try {
            if (e) {
                this.input = e.target;
                this.lastKeyCode = e.keyCode;
            }
            if (this.input) {
                this.cursorStart = this.input.selectionStart;
                this.cursorEnd = this.input.selectionEnd;
                this.currentValue = this.input.value;
                this.cursorBefore = this.input.value.substring(0, this.cursorStart);
                this.cursorAfter = this.input.value.substring(this.cursorEnd);
            }
        } catch (e) {
            // Fix error in Chrome:
            // Failed to read the 'selectionStart' property from 'HTMLInputElement'
            // http://stackoverflow.com/q/21177489/3040605
        }
    };

    fixCaret(start, end) {
        if (start === undefined || end === undefined || !this.input || !this.input.value) {
            return;
        }

        try {
            const currentStart = this.input.selectionStart;
            const currentEnd = this.input.selectionEnd;

            if (start !== currentStart || end !== currentEnd) {
                this.input.setSelectionRange(start, end);
            }
        } catch (e) {
            // Fix error in Chrome:
            // Failed to read the 'selectionStart' property from 'HTMLInputElement'
            // http://stackoverflow.com/q/21177489/3040605
        }
    }

    render() {
        const { max, min, onBlur, readOnly, onKeyDown, defaultValue, onChange, digit, parser, triggerChangeOnBlur, value, ...others } = this.props;

        const myMax = max === undefined ? AkConstants.NUMBER_MAX_SAFE : max;
        const myMin = min === undefined ? AkConstants.NUMBER_MIN_SAFE : min;

        let obj = { ...others };

        obj["onBlur"] = this.onBlur.bind(this);

        if (readOnly) {
            obj["readOnly"] = readOnly;
        }

        obj["onKeyDown"] = this.onKeyDown.bind(this);
        obj["onKeyUp"] = this.onKeyUp.bind(this);
        obj["onFocus"] = this.onFocus.bind(this);

        return this.state.digit > -1 ? <InputNumber precision={this.state.digit} max={myMax} min={myMin} value={this.state.value} onChange={this.onChange.bind(this)} {...obj} /> : <InputNumber max={myMax} min={myMin} value={this.state.value} onChange={this.onChange.bind(this)} {...obj} />;
    }
}