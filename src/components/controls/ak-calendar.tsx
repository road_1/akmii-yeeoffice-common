import * as React from "react";
import {Component} from "react";
import {Calendar} from "antd";
import {CalendarProps} from "antd/lib/calendar";

export interface AkCalendarProp extends CalendarProps {
}

export interface AkCalendarState {
}

export class AkCalendar extends Component <AkCalendarProp, AkCalendarState> {
    render() {
        return <Calendar {...this.props}/>;
    }
}
