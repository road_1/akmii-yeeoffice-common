import * as React from "react";
import { Component } from "react";
import { AkRow } from "./ak-row";
import { AkCol } from "./ak-col";
import { AkIcon } from "./ak-icon";
import { Table, Pagination } from 'antd';
import { AkUtil } from "../../util/util";
import { AkConstants, AkGlobal } from "../../util/common";
import { ListLocale } from "../../locales/localeid";
import { AkRadio, AkRadioGroup } from "./ak-radio";
import { AkTableProps } from './ak-table';
import { AkPagination, Paginations } from './ak-pagination';
import { AkButton } from ".";

var classNames = require('classnames');

export interface IAkTableSummary {
    id: string;
    field: string;
    type: string;
    value: any;
}

export enum ListView {
    table = 0,
    form = 1,
}

export interface AkResponsiveTableProps<T> extends AkTableProps<T> {
    summarise?: Array<IAkTableSummary>;
    haveBar?: boolean;
    defaultView?: ListView; //如果设置，则不会根据是否手机访问自动默认form视图
    hideViewSwitch?: boolean; //是否隐藏view选择
    outerClassName?: string; //最外层class
    allowReload?: boolean; //是否支持reload
    reload?: () => void; //reload方法
}

export interface AkResponsiveTableState {
    view: ListView
}

export class AkResponsiveTable<T> extends Component<AkResponsiveTableProps<T>, AkResponsiveTableState> {
    responsiveTableColumnClassName = "ak-responsive-table-column";

    constructor(props: any, context: any) {
        super(props, context);
        this.resetColumns(this.props);
        this.state = {
            view: props.defaultView === undefined ? (AkConstants.minSM ? ListView.form : ListView.table) : props.defaultView
        };
    }

    componentWillReceiveProps(nextProps) {
        this.resetColumns(nextProps);
        if ("defaultView" in nextProps && this.props.defaultView !== nextProps.defaultView) {
            this.setState({
                view: nextProps.defaultView === undefined ? (AkConstants.minSM ? ListView.form : ListView.table) : nextProps.defaultView
            });
        }
    }

    render() {
        const { className, hideViewSwitch, outerClassName } = this.props;
        let dataSource = this.props.dataSource ? [...this.props.dataSource] : [];
        this.addSummaryRow(dataSource);
        let props = {
            className: classNames(className, "ak-responsive-table"),
            dataSource: dataSource
        };
        let newProps = { ...this.props, ...props };
        return <div className={"ak-responsive-table-container " + outerClassName} >
            {!hideViewSwitch && this.renderSwitchView()
            }
            {this.renderContent(newProps)}
        </div >;
    }

    renderSwitchView() {
        const { allowReload, reload } = this.props;
        const { formatMessage } = AkGlobal.intl;
        return <AkRow key="view-switcher" type="flex" justify="end"
            className={classNames("ak-responsive-table-view-switcher", { "have-bar": this.props.haveBar })}>
            {allowReload && reload && <AkButton className="refresh" onClick={reload} icon="reload" />}
            <AkRadioGroup value={this.state.view} onChange={this.changeSelectedView.bind(this)}>
                <AkRadio.Button key="default"
                    value={ListView.table}><AkIcon title={formatMessage({ id: ListLocale.ViewTable })} type="danxuan" /></AkRadio.Button>
                <AkRadio.Button key="list"
                    value={ListView.form}><AkIcon title={formatMessage({ id: ListLocale.ViewForm })} type="category" /></AkRadio.Button>
            </AkRadioGroup>
        </AkRow>;
    }

    renderContent(newProps) {
        let controls = new Array();
        controls.push(this.renderFormView({ ...newProps, key: "__table_view_form" }));
        controls.push(this.renderTableView({ ...newProps, key: "__table_view_table" }));
        return controls;
    }

    resetColumns(props: AkResponsiveTableProps<T>) {
        if (props.columns && Array.isArray(props.columns)) {
            this.resetColumnsStyle(props);
            this.resetColumnsRender(props);
        }
    }

    resetColumnsStyle(props: AkResponsiveTableProps<T>) {
        for (let column of props.columns) {
            if (column.dataIndex) {
                if (!column.className || column.className.indexOf(this.responsiveTableColumnClassName) < 0)
                    column.className = classNames(column.className, this.responsiveTableColumnClassName);
            }
        }
    }

    changeSelectedView(event) {
        this.setState({
            view: event.target.value
        });
    }

    /*---------------- summary start -------------------*/
    summaryRowFlag = "_summary";

    hasSummaries(props: AkResponsiveTableProps<T>) {
        return props.summarise && Array.isArray(props.summarise) && props.summarise.length > 0 ? true : false;
    }

    addSummaryRow(rows: Array<T>) {
        if (this.hasSummaries(this.props) && rows.length > 0) {
            let summaryRow: any = {};
            summaryRow[this.props.rowKey as string] = AkUtil.guid();
            summaryRow[this.summaryRowFlag] = true;
            rows.push(summaryRow);
        }
    }

    resetColumnsRender(props: AkResponsiveTableProps<T>) {
        if (!this.hasSummaries(props)) return;

        const { formatMessage } = AkGlobal.intl;
        for (let column of props.columns) {
            let renderFunc = column.render;
            column.render = (text, record, index) => {
                if (record[this.summaryRowFlag] === true) {
                    for (let summary of props.summarise) {
                        if (summary.field === column.dataIndex) {
                            var type = formatMessage({ id: ListLocale.SummaryTypePre + summary.type }) + ": ";
                            return <div key={summary.id} className="summary">
                                <span className="title">{type}</span>
                                <span className="value">{summary.value}</span>
                            </div>;
                        }
                    }
                    return null;
                }
                return renderFunc(text, record, index);
            };
        }
    }

    /*---------------- summary end -------------------*/

    renderTableView(props) {
        const newColumns = this.props.columns && this.props.columns.filter(i => !i.hidden);
        if (props.pagination && props.pagination.total <= props.pagination.pageSize) {
            props.pagination = false
        }
        let newProps = {
            ...props,
            columns: newColumns,
            className: classNames(props.className, "ak-responsive-table-width-" + (props.columns ? props.columns.length : 0)),
        };
        let style = { display: this.state.view === ListView.table ? "block" : "none" };
        return React.createElement(Table as any, { ...newProps, style: style });
    }

    /*---------------- form view end -------------------*/
    renderFormView(props) {
        let style = { display: this.state.view === ListView.form ? "block" : "none", marginBottom: 48 };
        let listIsNotEmpty = props.dataSource && props.dataSource.length > 0;
        const { formatMessage } = AkGlobal.intl;

        return <div key={props.key} style={style} className="ak-responsive-table-mobile">
            {listIsNotEmpty ? null : <div key="form_empty" className="ant-table-placeholder" style={{ border: "1px solid #e9e9e9" }}>
                <span>
                    <AkIcon type="frown-o" />
                    {formatMessage({ id: ListLocale.ListEmpty })}
                </span>
            </div>}
            {(this.props.dataSource || []).map((recode, index) => {
                return this.renderFormViewRow(props, index, recode);
            })}
            {this.renderFormViewSummary(props)}
            {typeof props.pagination !== "boolean" && "onChange" in (props.pagination as any) && props.pagination.total > 0 ?
                <AkPagination {...props.pagination as any} style={{ float: "right" }}></AkPagination> : null}
        </div>;
    }

    renderFormViewRow(props, index: number, recode) {
        let cells = new Array();
        const labelSize = { xs: 24, sm: 4, md: 4, lg: 3 };
        const controlSize = { xs: 24, sm: 8, md: 8, lg: 9 };
        const rowSize = { xs: 24, sm: 12, md: 12, lg: 12 };
        AkUtil.each(props.columns, (column, columnIndex) => {
            let hiddenCurrentFiled = false;
            let columnControl: { props: { style: React.CSSProperties } } = null;
            if (column.render) {
                columnControl = column.render(recode[column.dataIndex], recode, index) as { props: { style: React.CSSProperties } };
                if (columnControl && columnControl.props && columnControl.props.style && ("display" in columnControl.props.style)) {
                    hiddenCurrentFiled = columnControl.props.style.display === "none";
                }
            }
            if (column.title) {
                let size = column.render ? labelSize : rowSize;
                let cls = classNames("ant-form-item-label cell", column.className, { "hidden": hiddenCurrentFiled }).replace(this.responsiveTableColumnClassName, "");
                cells.push(<AkCol key={columnIndex + 'label'}
                    className={cls}
                    xs={size.xs} sm={size.sm} md={size.md} lg={size.lg}>
                    {column.title}
                </AkCol>);
            }
            if (column.render) {
                let size = column.title ? controlSize : rowSize;
                let cls = classNames("ant-form-item-control-wrapper cell", column.className, { "hidden": hiddenCurrentFiled }).replace(this.responsiveTableColumnClassName, "");
                cells.push(<AkCol key={columnIndex + 'control'}

                    className={cls}
                    xs={size.xs} sm={size.sm} md={size.md} lg={size.lg}>
                    {columnControl}
                </AkCol>);
            }
        });
        return <AkRow
            key={"form-view" + recode[this.props.rowKey as string]}
            type="flex"
            justify="start"
            align="top"
            className="row"
            onClick={(e) => { this.props.onRowClick && this.props.onRowClick(recode, index, e); }}
        >
            {cells}
        </AkRow>;
    }

    renderFormViewSummary(props) {
        if (this.hasSummaries(props) && props.dataSource.length > 0) {
            const { formatMessage } = AkGlobal.intl;

            let cells = new Array();
            AkUtil.each(props.summarise, (summary, columnIndex) => {
                let summaryColumn = props.columns.find(item => item.dataIndex === summary.field);
                let value = `${formatMessage({ id: ListLocale.SummaryTypePre + summary.type })}:${summary.value}`;
                cells.push(<AkCol key={columnIndex + 'label'}
                    className="ant-form-item-label cell summary-cell"
                    xs={6} sm={4} md={4} lg={3}>{summaryColumn && summaryColumn.title}</AkCol>);
                cells.push(<AkCol key={columnIndex + 'control'}
                    className="ant-form-item-label cell summary-cell"
                    xs={18} sm={8} md={8} lg={9}>{value}</AkCol>);
            });

            return <div key="form_summary">
                <AkRow key="summary-title" className="summary-title">
                    <AkCol xs={24}>{formatMessage({ id: ListLocale.Summary })}</AkCol>
                </AkRow>
                <AkRow key="summary-details" type="flex" justify="start" align="top" className="row">
                    {cells}
                </AkRow>
            </div>;
        }

        return null;
    }

    /*---------------- form view end -------------------*/
}