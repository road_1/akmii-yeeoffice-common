import * as React from "react";
import { Component } from "react";
import { Switch } from "antd";
import { AkUtil } from "../../util/util";

export interface AkSwitchProps {
    prefixCls?: string;
    size?: "small" | "default";
    className?: string;
    checked?: boolean;
    value?: boolean | "True" | "False" | "1" | "0" | "true" | "false";
    defaultValue?: boolean | "True" | "False" | "1" | "0" | "true" | "false";
    defaultChecked?: boolean;
    onChange?: (checked: boolean) => any;
    checkedChildren?: React.ReactNode;
    unCheckedChildren?: React.ReactNode;
    disabled?: boolean;
    // loading?: boolean;
}
export interface AkSwitchStates { }
export class AkSwitch extends Component<AkSwitchProps, AkSwitchStates> {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        let { value, defaultValue, checked, defaultChecked, ...others } = this.props;
        value = AkUtil.toBoolean(value);
        defaultValue = AkUtil.toBoolean(defaultValue);

        let props = {}
        if (defaultValue !== undefined || defaultChecked !== undefined) {
            Object.assign(props, { defaultChecked: defaultChecked === undefined ? defaultValue : defaultChecked });
        }

        if (value !== undefined || checked !== undefined) {
            Object.assign(props, { checked: checked === undefined ? value : checked });
        }

        return <Switch {...props} {...others}></Switch >
    }
}
class AkSwitchStyle { }
