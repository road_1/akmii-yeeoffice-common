import * as React from "react";
import { Component } from "react";
import { Table } from "antd";
import { AkPaginationProps } from "./ak-pagination";
import { AkRow } from "./ak-row";
import { AkCol } from "./ak-col";
import { AkUtil, AkConstants } from "../../index";
import { Store } from "antd/lib/table/createStore";
import { AkGlobal } from "../../util";
import { ListLocale } from "../../locales/localeid";

export declare type AkCompareFn<T> = ((a: T, b: T) => number);
export declare type AkColumnFilterItem = {
    text: string;
    value: string;
    children?: AkColumnFilterItem[];
};
export interface AkColumnProps<T> {
    title?: React.ReactNode;
    key?: string;
    dataIndex?: string;
    render?: (text: any, record: T, index: number) => React.ReactNode;
    // filters?: AkColumnFilterItem[];
    filters?: {
        text: string;
        value: string;
        children?: any[];
    }[];
    onFilter?: (value: any, record: T) => boolean;
    filterMultiple?: boolean;
    filterDropdown?: React.ReactNode;
    filterDropdownVisible?: boolean;
    onFilterDropdownVisibleChange?: (visible: boolean) => void;
    sorter?: boolean | AkCompareFn<T>;
    // defaultSortOrder?: 'ascend' | 'descend';
    colSpan?: number;
    width?: string | number;
    className?: string;
    fixed?: boolean | ('left' | 'right');
    filterIcon?: React.ReactNode;
    filteredValue?: any[];
    sortOrder?: boolean | ('ascend' | 'descend');
    children?: AkColumnProps<T>[];
    onCellClick?: (record: T, event: any) => void;
    // onCell?: (record: T) => any;
    // onHeaderCell?: (props: AkColumnProps<T>) => any;
    /**
     * 小屏换行显示，默认从20开始
     * 小于20，将在其它列之前
     * 大于20+N列，在其它列之后
     */
    layout?: number | AkColumnLayout;
    hidden?: boolean;
}
export interface AkTableComponents {
    table?: any;
    header?: {
        wrapper?: any;
        row?: any;
        cell?: any;
    };
    body?: {
        wrapper?: any;
        row?: any;
        cell?: any;
    };
}
export interface AkTableLocale {
    filterTitle?: string;
    filterConfirm?: React.ReactNode;
    filterReset?: React.ReactNode;
    emptyText?: React.ReactNode | (() => React.ReactNode);
    selectAll?: React.ReactNode;
    selectInvert?: React.ReactNode;
}
export declare type AkRowSelectionType = 'checkbox' | 'radio';
export declare type AkSelectionSelectFn<T> = (record: T, selected: boolean, selectedRows: Object[]) => any;
export interface AkTableRowSelection<T> {
    type?: AkRowSelectionType;
    selectedRowKeys?: string[] | number[];
    onChange?: (selectedRowKeys: string[] | number[], selectedRows: Object[]) => any;
    getCheckboxProps?: (record: T) => Object;
    onSelect?: AkSelectionSelectFn<T>;
    onSelectAll?: (selected: boolean, selectedRows: Object[], changeRows: Object[]) => any;
    onSelectInvert?: (selectedRows: Object[]) => any;
    selections?: AkSelectionDecorator[] | boolean;
    hideDefaultSelections?: boolean;
    // fixed?: boolean;
}
export interface AkTableProps<T> {
    prefixCls?: string;
    dropdownPrefixCls?: string;
    rowSelection?: AkTableRowSelection<T>;
    pagination?: AkPaginationProps | boolean;
    size?: 'default' | 'middle' | 'small';
    dataSource?: T[];
    // components?: AkTableComponents;
    columns?: AkColumnProps<T>[];
    rowKey?: string | ((record: T, index: number) => string);
    rowClassName?: (record: T, index: number) => string;
    expandedRowRender?: any;
    // defaultExpandAllRows?: boolean;
    defaultExpandedRowKeys?: string[] | number[];
    expandedRowKeys?: string[] | number[];
    expandIconAsCell?: boolean;
    expandIconColumnIndex?: number;
    expandRowByClick?: boolean;
    onExpandedRowsChange?: (expandedRowKeys: string[] | number[]) => void;
    onExpand?: (expanded: boolean, record: T) => void;
    onChange?: (pagination: AkPaginationProps | boolean, filters: string[], sorter: Object) => any;
    loading?: boolean | AkSpinProps;
    locale?: AkTableLocale;
    indentSize?: number;
    onRowClick?: (record: T, index: number, event: Event) => any;
    // onRow?: (record: T, index: number) => any;
    useFixedHeader?: boolean;
    bordered?: boolean;
    showHeader?: boolean;
    footer?: (currentPageData: Object[]) => React.ReactNode;
    title?: (currentPageData: Object[]) => React.ReactNode;
    scroll?: {
        x?: boolean | number | string;
        y?: boolean | number | string;
    };
    childrenColumnName?: string;
    bodyStyle?: React.CSSProperties;
    className?: string;
    style?: React.CSSProperties;
    autoWrapColumn?: boolean; //是否在手机模式下自动隐藏表头和合并列显示
    isNoData?:boolean;
    // children?: React.ReactNode;
    
}
export interface AkTableStateFilters {
    [key: string]: string[];
}
export interface AkTableState<T> {
    filters: AkTableStateFilters;
    sortColumn: AkColumnProps<T> | null;
    sortOrder: string;
}

export interface AkSelectionDecorator {
    key: string;
    text: React.ReactNode;
    onSelect: (changeableRowKeys: string[]) => void;
}
export interface AkSelectionCheckboxAllProps<T> {
    store: Store;
    locale: any;
    disabled: boolean;
    getCheckboxPropsByItem: (item: any, index: number) => any;
    getRecordKey: (record: any, index?: number) => string;
    data: T[];
    prefixCls: string | undefined;
    onSelect: (key: string, index: number, selectFunc: any) => void;
    hideDefaultSelections?: boolean;
    selections?: AkSelectionDecorator[] | boolean;
    getPopupContainer: (triggerNode?: Element) => HTMLElement;
}
export interface AkSelectionCheckboxAllState {
    checked: boolean;
    indeterminate: boolean;
}
export interface AkSelectionBoxProps {
    store: Store;
    type?: AkRowSelectionType;
    defaultSelection: string[];
    rowIndex: string;
    name?: string;
    disabled?: boolean;
    onChange: React.ChangeEventHandler<HTMLInputElement>;
}
export interface AkSelectionBoxState {
    checked?: boolean;
}
export interface AkFilterMenuProps<T> {
    locale: AkTableLocale;
    selectedKeys: string[];
    column: AkColumnProps<T>;
    confirmFilter: (column: AkColumnProps<T>, selectedKeys: string[]) => any;
    prefixCls: string;
    dropdownPrefixCls: string;
    getPopupContainer: (triggerNode?: Element) => HTMLElement;
}
export interface FilterMenuState {
    selectedKeys: string[];
    keyPathOfSelectedItem: {
        [key: string]: string;
    };
    visible?: boolean;
}

export enum AkColumnLayout {
    /**
     * 左侧图片
     */
    Img = 1,
    /**
     * 左顶部
     */
    LeftTop = 2,
    /**
     * 右顶部
     */
    RightTop = 3,
    /**
     * 左底部
     */
    LeftBottom = 4,
    /**
     * 右底部
     */
    RightBottom = 5,
    /**
     * 操作列
     */
    Option = 6,
    /**
     * 底部
     */
    AllBottom = 7
}
export interface AkSpinProps {
    prefixCls?: string;
    className?: string;
    spinning?: boolean;
    style?: React.CSSProperties;
    size?: 'small' | 'default' | 'large';
    tip?: string;
    delay?: number;
    wrapperClassName?: string;
    indicator?: React.ReactNode;
}
export interface AkTableStates { }

export class AkTable<T> extends Component<AkTableProps<T>,
    AkTableStates> {
    static defaultProps = {
        autoWrapColumn: true
    }
    constructor(props, context) {
        super(props, context);
    }

    render() {
        let { showHeader, columns, ...otherProps } = this.props;
        let NoData = otherProps.isNoData ? AkGlobal.intl.formatMessage({ id: ListLocale.TableEmpty }): "";
        let TableLocaleProps = {
            filterTitle: '筛选',
            filterConfirm: '确定',
            filterReset: '重置',
            emptyText: NoData,
            selectAll: '全选当页',
            selectInvert: '反选当页',
        }
        columns = columns.filter(t => !t.hidden);
        //手机模式下执行特殊渲染
        if (AkConstants.minSM && this.props.autoWrapColumn) {
            showHeader = false;
            if (columns.length > 2) {
                // columns = this.renderColumns(columns);
                columns = this.renderColumnByMobile(columns);
            }
        }
        const pagination: any = this.props.pagination;
        if (pagination && pagination.total <= pagination.pageSize) {
            otherProps.pagination = false;
            return React.createElement(Table as any, Object.assign({}, otherProps, { columns: columns, showHeader: showHeader,locale: TableLocaleProps}));
        } else {
            return React.createElement(Table as any, Object.assign({}, otherProps, { columns: columns, showHeader: showHeader,locale:TableLocaleProps }));
        }
    }
    renderColumns(columns: AkColumnProps<T>[]) {
        let normalCount = 0; //普通字段

        //根据字段的layout属性分组；没有dataindex一组；没有layout的数据两两一组（+20用来和之前的分组分开）
        let dic = AkUtil.groupBy(columns, (a, index) => {
            if (a.layout || a.dataIndex == null) {
                return a.layout || a.dataIndex == null;
            } else {
                return (Math.floor((normalCount++) / 2)) + 20;
            }
        });
        let keyArray = Object.keys(dic);
        keyArray.sort();
        let maxWidth = window.innerWidth * 0.7;
        let tempColumns: AkColumnProps<T>[] = [];

        AkUtil.each(keyArray, (key, index) => {
            let style: React.CSSProperties = {};
            let n = dic[key] as any;
            if (index === keyArray.length - 1) {
                style = { textAlign: "right" }
            } else {
                style = { maxWidth: maxWidth }
            }
            tempColumns.push({
                dataIndex: n[0].dataIndex,
                className: n[0].className,
                render: (text, record, index) => {
                    return <AkRow key={AkUtil.guid()} style={style}>
                        {n.map(d => {
                            return <AkCol key={AkUtil.guid()}>
                                {d.render ? d.render(record[d.dataIndex], record, index) : record[d.dataIndex]}
                            </AkCol>;
                        })}
                    </AkRow>;
                }
            });
        });

        return tempColumns;
    }

    renderColumnByMobile(columns: AkColumnProps<T>[]) {
        let tempColumns: AkColumnProps<T>[] = [];

        //超过6个column采用默认方式render
        if (columns.length > 6) {
            return this.renderColumns(columns);
        }

        let leftTopColumn, imgColumn, optionColumn, leftBottomColumn, rightTopColumn, rightBottomColumn, allBottomColumn;

        AkUtil.each(columns, a => {
            switch (a.layout) {
                case AkColumnLayout.LeftTop:
                    leftTopColumn = a;
                    break;
                case AkColumnLayout.Img:
                    imgColumn = a;
                    break;
                case AkColumnLayout.Option:
                    optionColumn = a;
                    break;
                case AkColumnLayout.LeftBottom:
                    leftBottomColumn = a;
                    break;
                case AkColumnLayout.RightTop:
                    rightTopColumn = a;
                    break;
                case AkColumnLayout.RightBottom:
                    rightBottomColumn = a;
                    break;
                case AkColumnLayout.AllBottom:
                    allBottomColumn = a;
                    break;
            }

            if (!a.dataIndex) {
                optionColumn = a;
            }
        });

        //如果没有设置定制的layout，则直接走默认的column渲染
        if (!leftTopColumn) {
            return this.renderColumns(columns);
        }

        if (imgColumn) {
            tempColumns.push({ ...imgColumn, width: 45 });
        }
        tempColumns.push({
            dataIndex: this.props.rowKey as string,
            render: (text, record, index) => {

                let bottom = allBottomColumn ? (allBottomColumn.render ? allBottomColumn.render(record[allBottomColumn.dataIndex], record, index) : <AkRow>{record[allBottomColumn.dataIndex]}</AkRow>) : "";
                let maxWidth = imgColumn ? window.innerWidth * 0.5 : window.innerWidth * 0.63;

                return <AkRow>
                    <AkRow type="flex" justify="space-between" align="middle">
                        <AkCol className="ak-table-title" style={{ maxWidth: maxWidth }}>
                            {leftTopColumn ? (leftTopColumn.render ? leftTopColumn.render(record[leftTopColumn.dataIndex], record, index) : record[leftTopColumn.dataIndex]) : ""}
                        </AkCol>
                        <AkCol className="ak-table-title2 ak-table-right" style={{ maxWidth: maxWidth }}>
                            {rightTopColumn ? (rightTopColumn.render ? rightTopColumn.render(record[rightTopColumn.dataIndex], record, index) : record[rightTopColumn.dataIndex]) : ""}
                        </AkCol>
                    </AkRow>
                    <AkRow type="flex" justify="space-between" align="middle">
                        <AkCol className="ak-table-title2" style={{ maxWidth: maxWidth }}>
                            {leftBottomColumn ? (leftBottomColumn.render ? leftBottomColumn.render(record[leftBottomColumn.dataIndex], record, index) : record[leftBottomColumn.dataIndex]) : ""}
                        </AkCol>
                        <AkCol className="ak-table-title2 ak-table-right" style={{ maxWidth: maxWidth }}>
                            {rightBottomColumn ? (rightBottomColumn.render ? rightBottomColumn.render(record[rightBottomColumn.dataIndex], record, index) : record[rightBottomColumn.dataIndex]) : ""}
                        </AkCol>
                    </AkRow>
                    {bottom}
                </AkRow>
            }
        });
        if (optionColumn) {
            tempColumns.push({ ...optionColumn, className: "ak-table-col-option", width: 30 })
        }
        return tempColumns;
    }
}
class AkTableStyle { }
