import * as React from "react";
import { Component } from "react";
import { Pagination } from "antd";
import { PaginationProps } from "antd/lib/pagination";

export interface AkPaginationProps {
    /** 数据总数*/
    total: number;
    /** 默认的当前页数*/
    defaultCurrent?: number;
    /** 当前页数*/
    current?: number;
    /** 初始的每页条数*/
    defaultPageSize?: number;
    /** 每页条数*/
    pageSize?: number;
    /** 页码改变的回调，参数是改变后的页码*/
    onChange?: (page: number, pageSize?: number) => void;
    // hideOnSinglePage?: boolean;
    /** 是否可以改变 pageSize */
    showSizeChanger?: boolean;
    /** 指定每页可以显示多少条*/
    pageSizeOptions?: string[];
    /** pageSize 变化的回调  */
    onShowSizeChange?: (current: number, size: number) => void;
    /** 是否可以快速跳转至某页*/
    showQuickJumper?: boolean;
    /** 用于显示总共有多少条数据*/
    showTotal?: (total: number, range?: [number, number]) => React.ReactNode;
    /** 当为「small」时，是小尺寸分页 */
    size?: string;
    /** 当添加该属性时，显示为简单分页*/
    simple?: boolean;
    style?: React.CSSProperties;
    className?: string;
    locale?: Object;
    prefixCls?: string;
    selectPrefixCls?: string;
    itemRender?: (page: number, type: 'page' | 'prev' | 'next' | 'jump-prev' | 'jump-next') => React.ReactNode;
}
export interface AkPaginationStates { }
export class AkPagination extends Component<AkPaginationProps,
    AkPaginationStates> {
    render() {
        const { total, pageSize } = this.props;
        if (total <= pageSize) {
            return null
        }
        return <Paginations {...this.props}></Paginations>
    }
}

export const Paginations: React.Component<PaginationProps, any> | any = Pagination;
class AkPaginationStyle { }
