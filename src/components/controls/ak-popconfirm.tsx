import * as React from 'react';
import { Popconfirm } from 'antd';
import { ButtonType } from './ak-button';
import { PopconfirmProps } from 'antd/lib/popconfirm';
export interface AkAdjustOverflow {
    adjustX?: 0 | 1;
    adjustY?: 0 | 1;
}
export declare type AkTooltipPlacement = 'top' | 'left' | 'right' | 'bottom' | 'topLeft' | 'topRight' | 'bottomLeft' | 'bottomRight' | 'leftTop' | 'leftBottom' | 'rightTop' | 'rightBottom';

export interface AkAbstractTooltipProps {
    prefixCls?: string;
    overlayClassName?: string;
    style?: React.CSSProperties;
    overlayStyle?: React.CSSProperties;
    placement?: AkTooltipPlacement;
    builtinPlacements?: Object;
    defaultVisible?: boolean;
    visible?: boolean;
    onVisibleChange?: (visible: boolean) => void;
    mouseEnterDelay?: number;
    mouseLeaveDelay?: number;
    transitionName?: string;
    trigger?: 'hover' | 'focus' | 'click';
    openClassName?: string;
    arrowPointAtCenter?: boolean;
    autoAdjustOverflow?: boolean | AkAdjustOverflow;
    getTooltipContainer?: (triggerNode: Element) => HTMLElement;
    getPopupContainer?: (triggerNode: Element) => HTMLElement;
    children?: React.ReactNode;
}
export interface AkPopconfirmProps extends AkAbstractTooltipProps {
    title: React.ReactNode;
    onConfirm?: (e?: React.MouseEvent<any>) => void;
    onCancel?: (e?: React.MouseEvent<any>) => void;
    okText?: React.ReactNode;
    okType?: ButtonType;
    cancelText?: React.ReactNode;
}
export interface AkPopconfirmContext {
    antLocale?: {
        Popconfirm?: any;
    };
}
export class AkPopconfirm extends React.Component<AkPopconfirmProps,
    any> {
    render() {
        return <Popconfirms {...this.props}></Popconfirms>
    }
}

export const Popconfirms: React.Component<PopconfirmProps, any> | any = Popconfirm;