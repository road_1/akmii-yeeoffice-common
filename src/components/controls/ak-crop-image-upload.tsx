import * as React from "react"
import { AkFileOptions, AkUpload, AkFile, AkUploadProps } from "./ak-upload";
import { AkNotification } from "./ak-notification";
import { UploadLocale, CommonLocale, ProcModelPageLocale } from "../../locales/localeid";
import { AkUtil, AkGlobal, FileUpLoadCommon } from "../../util";
import { AppKeys, FileUploadMethod, AkUploadFile, AkContext, AkModal, AkButton, AkMessage } from "../../index";
import Cropper from "react-cropper";
import'cropperjs/dist/cropper.css';
import { isNullOrUndefined } from "util";

export interface AkImageCropperProps extends AkUploadProps{
    fixedOutputWidth?:number;//剪切过后的图片宽
    fixedOutputHeight?:number;//剪切过后的图片高
    isSquare?:boolean;//剪切框是否为正方形
    onUploaded:(options?:AkUploadFile) => Promise<void>;//上传处理
    imageQuality?:"low" | "high";//图像高清度(默认low)
    imageEnabled?:boolean;//图片是否平滑(与像素有关，默认false)
}

export interface AkImageCropperStates{
    showCropModal?:boolean;
    cropImageSrc?:string;
}

export interface defaultImageAttr{
    width?:number;
    height?:number
}

export class AkImageCropper extends React.Component<AkImageCropperProps,AkImageCropperStates>{
    cropper?:any;//剪裁图片组件对象
    imageOptions?:AkUploadFile;//上传后的图片对象
    rotate?:number;//旋转角度
    defaultImageAttr?:defaultImageAttr;

    constructor(props,context){
        super(props,context);
        this.state = {
            showCropModal:false,
            cropImageSrc:""
        };

        this.rotate = 0;

       this.defaultImageAttr = {
           width:150,
           height:150
       }
    }

    /*剪裁框title显示*/
    renderCropTitle(){
        const {formatMessage}=AkGlobal.intl;
        return (
            <div className="crop-image-modal-title">
                <i className="anticon anticon-close modal-close" onClick={this.cropperModalClose}></i>
                <div className="right">
                <i className="anticon anticon-rotate-left crop-rotate" onClick={this.retateClickLeft}></i>
                <i className="anticon anticon-rotate-right crop-rotate" onClick={this.retateClickRight}></i>
                <button className="crop-btn" onClick={this.cropImage}> {formatMessage({ id: CommonLocale.ImageCropSubMit })}</button>
                </div>
            </div>
        );
    }

    dataURLtoBlob(dataurl) {
        var arr = dataurl.split(','),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]),
            n = bstr.length,
            u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new Blob([u8arr], { type: mime });
    }

    blobToFile(theBlob?:Blob,file?:AkFile){
        var b:any = theBlob;
        b.uid = file.uid;
        b.lastModifiedDate = new Date();
        b.name = file.name;
        b.filename = file.filename;
        b.lastModified = file.lastModified;
        b.url = file.url;
        b.status = file.status;
        b.percent = file.percent;
        b.thumbUrl = file.thumbUrl;
        b.originFileObj = file.originFileObj;
        b.fileKey = file.fileKey;
        b.response = file.response;
        b.error = file.error;
        b.linkProps = file.linkProps;

        return b as AkFile;
    }

    messageFrom(type: 'warning' | 'success', messageID: string, description: string, nameID: string, isText?: boolean) {
        const { formatMessage } = AkGlobal.intl;
        const a = isNullOrUndefined(nameID) ? formatMessage({ id: description }) : formatMessage({ id: description }, { name: formatMessage({ id: nameID }) });
        const parameter = {
            message: formatMessage({ id: messageID }),
            description: isText ? description : a
        };
        const { warning, success } = AkNotification;
        switch (type) {
            case "warning":
                warning(parameter);
                break;
            case "success":
                AkMessage.success(isText ? description : a);
                // success(parameter);
                break;
            default:
                warning(parameter);
                break;
        }
    }

    /**关闭剪裁框 */
    cropperModalClose = () =>{
        this.setState({
            showCropModal:false
        });
    }

    //左旋转
    retateClickLeft = () =>{
        if (Math.abs(this.rotate) === 360) {
            this.rotate = 0;
        }
        this.rotate-=90;
        this.cropper.rotateTo(this.rotate);
    }

    //右旋转
    retateClickRight = () =>{
        if (Math.abs(this.rotate) === 360) {
            this.rotate = 0;
        }
        this.rotate+=90;
        this.cropper.rotateTo(this.rotate);
    }

    /**初始化剪裁框对象 */
    initCropperRef = (cropper:any) =>{
        let topThis = this;
        if (cropper) {
            this.cropper = cropper;
            //初始化剪裁框大小
            cropper.setCropBoxData({
                width:topThis.props.fixedOutputWidth,
                height:topThis.props.fixedOutputHeight
            });
        }
    }

    /**自定义上传 */
    customRequest = (options?:AkUploadFile) =>{
        const topThis = this;
        this.imageOptions = options;
        //若满足条件则直接上传不剪切
        if (! this.state.showCropModal&&this.props.onUploaded) {
                this.props.onUploaded(options).then((res) =>{
                    topThis.setState({
                        showCropModal:false
                    });
                });
        }
    }

    /**剪裁图片 */
    cropImage = () =>{
        const topThis = this;
        if (this.cropper.getCroppedCanvas() === "null") {
            return false;
        }

        //剪裁的图片大小比例
        const canvasData = {
            width:topThis.props.fixedOutputWidth,
            height:topThis.props.fixedOutputHeight,
            imageSmoothingEnabled:topThis.props.imageEnabled ? topThis.props.imageEnabled : false,
            imageSmoothingQuality:topThis.props.imageQuality ? topThis.props.imageQuality : "low",
        };

        let srcCroppered = this.cropper.getCroppedCanvas(canvasData).toDataURL(this.imageOptions.file.type);

        let fileBlob = this.dataURLtoBlob(srcCroppered);

        let cropOptions = {...this.imageOptions};
        let cropFile = this.blobToFile(fileBlob,cropOptions.file);
        cropOptions.file = cropFile;
        
        this.props.onUploaded(cropOptions).then((res) =>{
            this.setState({
                showCropModal:false
            });
        });
    }

    /**上传前操作 */
    beforeUpload = (file) =>{
        const topThis = this;
        const { Tip } = CommonLocale;
        const { PropsPlaceholderImgDescribe } = ProcModelPageLocale;

        //判断类型
        if (file && file.type) {
            const type = file.type + "";
            if (!(type.toLocaleLowerCase() === "image/jpeg" || type.toLocaleLowerCase() === "image/jpg" || type.toLocaleLowerCase() === "image/png")) {
                topThis.messageFrom("warning", Tip, PropsPlaceholderImgDescribe, null);
                return false;
            }
        }

        // 因为读取文件需要时间,所以要在回调函数中使用读取的结果
        return new Promise((resolve)=>{
            var reader = new FileReader();
            if (file) {
                reader.readAsDataURL(file); //开始读取文件
            }
            reader.onload = (e) => {
                var image = new Image();
                image.src = e.target["result"];
                this.setState({
                    cropImageSrc: e.target["result"],
                });

                //判断图片是否小于150x150，小于则直接上传
                image.onload = (e) =>{
                    if (image.width > topThis.defaultImageAttr.width  || image.height > topThis.defaultImageAttr.height) {
                        topThis.setState({
                            showCropModal:true,
                        });
                        resolve(file);
                    } else {
                        topThis.setState({
                            showCropModal:false,
                        });
                        resolve(file);
                    }
                }
            }
        });
    }

    render(){
        const { isSquare,action,listType,className,style,showUploadList,data,headers,multiple,onChange,onPreview,onRemove,supportServerRender,disabled,prefixCls,withCredentials,locale } = this.props;
        return (
            <AkUpload 
            action={action}
            accept="image/png,image/jpeg,image/jpg"
            className={className}
            style={style}
            listType={listType}
            showUploadList={showUploadList}
            customRequest={this.customRequest}
            data={data}
            headers={headers}
            multiple={multiple}
            beforeUpload={file => this.beforeUpload(file)}
            onChange={onChange}
            onPreview={onPreview}
            onRemove={onRemove}
            supportServerRender={supportServerRender}
            disabled={disabled}
            prefixCls={prefixCls}
            withCredentials={withCredentials}
            locale={locale}
            >
             {this.props.children}
            <AkModal 
            className="crop-image-modal"
            closable={false}
            footer={null}
            visible={this.state.showCropModal}
            title={this.renderCropTitle()}
            dragable={false}
            >
            <div className="crop-image-modal-content">
            <Cropper
                className="crop-image-modal-crop"
                ref={Cropper => this.initCropperRef(Cropper)}
                src={this.state.cropImageSrc}
                rotatable={true}
                viewModal={1}
                aspectRatio={isSquare ? 1 :null}//剪裁框拖动时的宽高比
                autoCrop={true}//剪裁框是否可拖动
                movable={false}//图片是否可拖动
                toggleDragModeOnDblclick={false}//双击是否可以切换
                zoomOnWheel={false}//缩小或放大图片
                background={false}//网格背景
                highlight={false}
                        />
                        </div>
            </AkModal>
            </AkUpload>
        );
    }
}