import * as React from "react";
import { AkRow } from './ak-row';
import { ContentListApi, GetTagsByItemIdRequest, AkTag, AkTooltip, AkSelect } from "../..";
import { Component } from "react";
import { Input, Select } from "antd";

interface TagPickermodal {
    TagID?: string;
    TagName?: string;
    Status?: number;
}
interface TagSearchRelateModel {
    Tags?: TagPickermodal[],
    TargetTags?: TagPickermodal[],
}
interface AkTagSelectPickerProps {
    ItemId?: string;
    onChange: (value: any) => void;
}
interface AkTagSelectPickerStates {
    defaultValue?: string[];
    selectValue?: TagPickermodal[];
    searchOptions?: TagSearchRelateModel;
}

export class AkTagSelectPicker extends Component<AkTagSelectPickerProps, AkTagSelectPickerStates>{
    inputValue: string;
    inputTimeout = null;
    constructor(props, context) {
        super(props, context);
        this.state = {
            defaultValue: [],
            selectValue: [],
            searchOptions: {
                Tags: [],
                TargetTags: []
            }
        }
        this.inputValue = "";
    }
    componentDidMount() {
        if (this.props.ItemId) {
            let req: GetTagsByItemIdRequest = {
                ItemID: this.props.ItemId,
                IsGetTagName: true
            };
            ContentListApi.GetTagsByItemID(req).then((data) => {
                if (data.Data) {
                    let dft: TagPickermodal[] = [];
                    let dftIds: string[] = [];
                    data.Data.map((i) => {
                        dft.push({
                            TagID: i.TagID,
                            TagName: i.TagName
                        });
                        dftIds.push(i.TagID);
                    });
                    this.setState({
                        defaultValue: dftIds,
                        selectValue: dft
                    });
                }
            });
        }
    }

    afterClose = (tagID?: string) => {
        let dt = this.state.selectValue;
        let idx = dt.findIndex(o => o.TagID === tagID);
        dt.splice(idx, 1);
        this.setState({ selectValue: dt });
    }
    renderSelectTags = () => {
        const { selectValue } = this.state;
        return selectValue.map((tag, index) => {
            const isLongTag = tag.TagName.length > 20;
            const tagElem = (
                <div style={{ marginRight: '5px', marginBottom: '10px', float: 'left' }}>
                    <AkTag key={tag.TagID} onClose={(e) => { e.stopPropagation() }} closable={true} afterClose={() => {
                        this.afterClose(tag.TagID);
                    }}>
                        {isLongTag ? `${tag.TagName.slice(0, 20)}...` : tag.TagName}
                    </AkTag>
                </div>
            );
            return isLongTag ? <AkTooltip title={tag} key={tag.TagID}>{tagElem}</AkTooltip> : tagElem;
        });
    }
    onInputChange = (value) => {
        this.inputValue = value;
        this.loadTagsBySearchKey(value);
    }
    loadTagsBySearchKey(value) {
        ContentListApi.SearchTags({ Limit: 10, TagName: value }).then((data) => {
            if (data.Data) {
                this.inputValue = value;
                this.setState({ searchOptions: data.Data });
            }
        });
    }
    onSelect = (value, option) => {
        this.inputValue = "";
        this.state.searchOptions.Tags.map((i) => {
            if (i.TagID === value) {
                const { selectValue } = this.state;
                let idx = selectValue.findIndex(o => o.TagID === i.TagID);
                if (idx >= 0) { return; }
                let m: TagPickermodal = {
                    TagID: i.TagID,
                    TagName: i.TagName
                };
                selectValue.push(m);
                this.setState({
                    selectValue: selectValue
                });
                if (this.props.onChange) {
                    this.props.onChange(selectValue);
                }
            }
        });
    }

    renderTagOptions = () => {

        let tags = <AkSelect.OptGroup>
            {
                this.state.searchOptions.Tags.map((i) => {
                    // return (<AkSelect.Option key={i.TagID}>{i.TagName}</AkSelect.Option>);
                    return <AkSelect.Option value={i.TagID}>{i.TagName}</AkSelect.Option>;
                })
            }
        </AkSelect.OptGroup>;
        return tags;
    }
    rendertargetTagsOptions = () => {
        let targetTags = <AkSelect.OptGroup key={3} label="关联标签">
            {
                this.state.searchOptions.TargetTags.map((i) => {
                    return (<AkSelect.Option key={i.TagID}>{i.TagName}</AkSelect.Option>)
                })
            }</AkSelect.OptGroup>;
        return targetTags
    }

    getInputElement() {
        return <Input onKeyDown={e => {
            if (e["keyCode"] === 13) {
                e.stopPropagation();
                e.preventDefault();
            }
        }} />
    }


    onChange(value) {
        this.loadTagsBySearchKey(value);
    }
    render() {
        return <AkRow>
            {this.renderSelectTags()}
            <Select className="ak-autocomplete" style={{ width: '200px', float: 'left' }}
                getInputElement={() => this.getInputElement()}
                onChange={this.onChange.bind(this)}
                onSelect={this.onSelect.bind(this)}>
                {this.renderTagOptions()}
                {this.rendertargetTagsOptions()}
            </Select>
        </AkRow>
    }
}