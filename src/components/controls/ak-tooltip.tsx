import * as React from "react";
import {Component} from "react";
import {Tooltip} from "antd";
import {AkTooltipPlacement,AkAbstractTooltipProps} from './ak-popconfirm'

export declare type AkRenderFunction = () => React.ReactNode;
export interface AKTooltipProps extends AkAbstractTooltipProps {
    title?: React.ReactNode | AkRenderFunction;
    overlay?: React.ReactNode | AkRenderFunction;
}
export interface AKTooltipStates { }
export class AkTooltip extends Component<AKTooltipProps,
    AKTooltipStates> {
    render() {
        return <Tooltip {...this.props}></Tooltip>
    }
}
class AKTooltipStyle { }
export default AkTooltip;