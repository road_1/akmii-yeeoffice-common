import * as React from 'react';
import { Component } from 'react';
import { Card } from 'antd';

export interface AkCardProps {
    prefixCls?: string;
    title?: React.ReactNode;
    extra?: React.ReactNode;
    bordered?: boolean;
    bodyStyle?: React.CSSProperties;
    style?: React.CSSProperties;
    loading?: boolean;
    noHovering?: boolean;
    // hoverable?: boolean;
    children?: React.ReactNode;
    id?: string;
    className?: string;
    // type?: CardType;
    // cover?: React.ReactNode;
    // actions?: Array<React.ReactNode>;
    // tabList?: CardTabListType[];
    // onTabChange?: (key: string) => void;
}

export interface AkCardStates { }

export class AkCard extends Component<AkCardProps, AkCardStates> {
    render() {
        return <Card {...this.props}></Card>
    }
}

class AkCardStyle { }