import { Icon, Input, AutoComplete } from 'antd';
import * as React from 'react';
import { Component } from 'react';
import { AkSelect } from './ak-select';
import { ContentListApi, GetTagsByItemIdRequest, AkTag, AkTooltip, GetTagsByIdsRequest, AkUtil, TagAddRequest } from '../..';
import { AkRow } from '.';
import { AkGlobal } from '../../util/common';
import { CommonLocale } from '../../locales';
import { CommonControlsLocale } from '../../locales/localeid';
import * as classNames from 'classnames';

interface TagModel {
    TagID?: string;
    TagName?: string;
    Status?: number;
}
interface TagDataModel {
    Title?: string;
    Children?: TagModel[];
}

interface AkTagPickerProps {
    className?: string;
    itemId?: string;
    onChange?: (value: any) => void;
    readonly?: boolean;//只读
    value?: string[] | TagModel[];//标签IDs
    maxcount?: number;//最大标签数
    disable?: boolean;//禁用
    disableEnterAdd?: boolean;//禁用回车实现新增-用于搜索时不需要回车新增
}
interface AkTagPickerStates {
    searchData: TagModel[];//搜索标签
    data?: TagDataModel[];//分组搜索结果集
    selectValue?: TagModel[];//当前选中的标签
    defaultValue?: TagModel[];//Item 已存在的IDs
}

export class AkTagPicker extends Component<AkTagPickerProps, AkTagPickerStates>{
    onFocus?: boolean;
    formatMessage = AkGlobal.intl.formatMessage;
    inputTimeout = null;
    selectValueText = "";
    doOnSelect = false;
    doOnEnter = false;
    firstonSelect = false;
    hasInput = false;
    constructor(props, context) {
        super(props, context);
        this.state = {
            data: [],
            searchData: [],
            defaultValue: [],
            selectValue: []
        }
        this.onFocus = false;
    }
    componentWillReceiveProps(nextProps: AkTagPickerProps) {
        if ("value" in nextProps && nextProps.value !== this.props.value) {
            if (!nextProps.value) { this.setState({ selectValue: [] }) } else {
                this.changeValue(nextProps.value);
            }
        }
    }
    componentDidMount() {
        const { itemId, value } = this.props;
        if (itemId) {
            let req: GetTagsByItemIdRequest = {
                ItemID: itemId,
                IsGetTagName: true
            };
            ContentListApi.GetTagsByItemID(req).then((data) => {
                if (data.Data) {
                    let dft: TagModel[] = [];
                    data.Data.map((i) => {
                        if (i.TagName) {//2018年12月29日13:58:07 标签合并后name为null，合并后不显示
                            dft.push({
                                TagID: i.TagID,
                                TagName: i.TagName
                            });
                        }
                    });
                    let selt: TagModel[] = [].concat(dft);
                    this.setState({
                        defaultValue: dft,
                        selectValue: selt
                    });
                }
            });
        }
        this.changeValue(value);
    }
    changeValue(value) {
        if (typeof value === "string") {
            if (value.trim() === "") {
                //2019年3月7日15:53:24 防空啊
                value = [];
            } else {
                try {
                    value = JSON.parse(value);
                } catch{
                    value = [];
                }
            }
        }
        if (value && value.length > 0) {
            if (typeof value[0] === "object") {
                //流程表单-控件
                let _value = [];
                let pvalue: any[] = value;
                pvalue.map((i) => {
                    if (i.Status !== -1) {
                        _value.push(i);
                    }
                });
                this.setState({ selectValue: _value });
                return;
            }
            if (typeof value[0] === "string") {
                let req: GetTagsByIdsRequest = {
                    TagIDs: value
                };
                ContentListApi.GetTagsByIDs(req).then((data) => {
                    if (data.Data) {
                        const { selectValue } = this.state;
                        let dft: TagModel[] = [];
                        data.Data.map((i) => {
                            if (i.TagName) {
                                dft.push({
                                    TagID: i.TagID,
                                    TagName: i.TagName
                                });
                            }
                        });
                        this.setState({
                            selectValue: dft
                        });
                    }
                });
            }
        }
    }
    InputonFocus() {
        const { readonly, disable } = this.props;
        if (readonly === true || disable === true) { return; }
        if (this.onFocus === false || this.state.data.length === 0) {
            this.onFocus = true;
            this.loadTagsBySearchKey("");
        }
    }
    renderTitle(title) {
        return (
            <span>
                {title}
                {/* <a
                    style={{ float: 'right' }}
                    href="https://www.google.com/search?q=antd"
                    target="_blank"
                    rel="noopener noreferrer"
                >更多
          </a> */}
            </span>
        );
    };

    loadTagsBySearchKey(value) {
        let _th = this;
        ContentListApi.SearchTags({ Limit: 10, TagName: value }).then((data) => {
            if (data.Data) {
                let tagdata: TagDataModel[] = [];
                let Tags = data.Data.Tags;
                let TargetTags = data.Data.TargetTags;
                let searchDt: TagModel[] = [];
                if (Tags) {
                    let tagmodel: TagDataModel = { Title: "", Children: [] };
                    Tags.map((i) => {
                        let md = {
                            TagID: i.TagID,
                            TagName: i.TagName,
                            Status: i.Status
                        };
                        tagmodel.Children.push(md);
                        searchDt.push(md);
                    });
                    if (tagmodel.Children.length > 0) {
                        tagdata.push(tagmodel);
                    }
                }
                /***关联标签 */
                if (TargetTags) {
                    let targetaggmodel: TagDataModel = { Title: this.formatMessage({ id: CommonControlsLocale.Tag_Relation }), Children: [] };
                    TargetTags.map((i) => {
                        let md = {
                            TagID: i.TagID,
                            TagName: i.TagName,
                            Status: i.Status
                        };
                        targetaggmodel.Children.push(md);
                        searchDt.push(md);
                    });
                    if (targetaggmodel.Children.length > 0) {
                        tagdata.push(targetaggmodel);
                    }
                }
                _th.setState({ data: tagdata, searchData: searchDt });
            }
        });
    }

    afterClose = (TagName?: string) => {
        let dt = this.state.selectValue;
        let idx = dt.findIndex(o => o.TagName.toLocaleLowerCase() === TagName.toLocaleLowerCase());
        dt.splice(idx, 1);
        this.setState({ selectValue: dt });
        this.returnOnchange(dt);
    }
    inputonPressEnter(e) {
        if (this.props.disableEnterAdd === true) { return; }
        let tagName = e.target.value + "";
        if (tagName !== "") {
            this.doOnEnter = true;
            const { selectValue, defaultValue, searchData } = this.state;
            let idx = selectValue.findIndex(o => o.TagName.toLocaleLowerCase() === tagName.toLocaleLowerCase());
            if (searchData.length === 0) {
                this.doOnEnter = false;
                this.hasInput = false;
            }
            if (idx < 0) {
                if (this.props.itemId) {
                    //为内容列表-itemid
                    selectValue.push({
                        TagName: tagName,
                        TagID: "0"
                    });
                    this.selectValueText = "";
                    this.setState({ selectValue: selectValue });
                    this.loadTagsBySearchKey("");
                    this.returnOnchange(selectValue);
                } else {
                    //流程中心-直接新增
                    let req: TagAddRequest = {
                        TagName: tagName
                    };
                    ContentListApi.AddTag(req).then((data) => {
                        if (data.Status === 0) {
                            selectValue.push({
                                TagName: tagName,
                                TagID: data.Data
                            });
                            this.selectValueText = "";
                            this.setState({ selectValue: selectValue });
                            this.loadTagsBySearchKey("");
                            this.returnOnchange(selectValue);
                        }
                    });

                }
            } else {
                this.selectValueText = "";
            }
        } else {
            this.doOnEnter = false;
        }
    }
    onSelect = (value, option) => {
        const { defaultValue, searchData } = this.state;
        this.doOnSelect = true;
        if (this.hasInput) {
            this.selectValueText = "";
            this.doOnSelect = !this.doOnEnter;
            this.hasInput = false;
        }
        let selectValue = this.state.selectValue;
        let _tagID = searchData.find(o => o.TagName.toLocaleLowerCase() === value.toLocaleLowerCase()).TagID;
        if (this.doOnEnter === true) {
            // if (this.props.disableEnterAdd === true) {
            //     this.doOnEnter = false;
            // }
            this.doOnEnter = false;
            this.doOnSelect = false;
            return;
        }
        let idx = selectValue.findIndex(o => o.TagName.toLocaleLowerCase() === value.toLocaleLowerCase());
        if (idx >= 0) {
            selectValue.map((i) => {
                if (i.TagName.toLocaleLowerCase() === value.toLocaleLowerCase()) {
                    i.TagID = _tagID;
                }
            });
            this.setState({ selectValue: selectValue });
            return;
        }
        let m: TagModel = {
            TagID: _tagID,
            TagName: value
        };
        selectValue.push(m);
        this.setState({
            selectValue: selectValue
        });
        this.loadTagsBySearchKey("");
        this.returnOnchange(selectValue);
    }
    onChange(value) {
        let _search = "";
        if (this.doOnSelect || this.doOnEnter) {
            //通过选择---清空文本框
            this.selectValueText = "";
        } else {
            this.selectValueText = value;
            this.hasInput = true;
            _search = value;
        }
        this.doOnSelect = false;
        this.doOnEnter = false;
        if (this.inputTimeout) {
            window.clearTimeout(this.inputTimeout);
        }
        this.inputTimeout = setTimeout(() => {
            this.loadTagsBySearchKey(_search);
        }, 300);
    }

    returnOnchange(data: TagModel[]) {
        const { defaultValue } = this.state;
        if (this.props.onChange) {
            //计算新增的
            let dt: TagModel[] = [];
            data.map((i) => {
                let idx = defaultValue.findIndex(o => o.TagName.toLocaleLowerCase() === i.TagName.toLocaleLowerCase());
                if (idx < 0) {
                    i.Status = 1;
                    dt.push(i);
                } else {
                    //原有的
                    i.Status = 0;
                    dt.push(i);
                }
            });
            //删除的
            defaultValue.map((i) => {
                let idx = data.findIndex(o => o.TagName.toLocaleLowerCase() === i.TagName.toLocaleLowerCase());
                if (idx < 0) {
                    i.Status = -1;
                    dt.push(i);
                }
            });
            this.props.onChange(JSON.stringify(dt));
        }
    }

    renderSelectTags = () => {
        const { selectValue } = this.state;
        let tags = selectValue.map((tag, index) => {
            const isLongTag = tag.TagName.length > 20;
            let tagElem = <div className="tag">
                <AkTag onClose={(e) => { e.stopPropagation() }} closable={this.props.readonly !== true} afterClose={() => {
                    this.afterClose(tag.TagName);
                }}>
                    {isLongTag ? `${tag.TagName.slice(0, 20)}...` : tag.TagName}
                </AkTag>
            </div>;
            return <AkTooltip title={tag.TagName} key={tag.TagName + tag.TagID}>{tagElem}</AkTooltip>;
        });
        return tags;
    }

    render() {
        const { readonly, disable, maxcount, className } = this.props;
        const { selectValue } = this.state;
        let max_count = maxcount ? maxcount : 10;
        let tagoptions: any = this.state.data.map(group => (
            <AkSelect.OptGroup
                key={group.Title}
                label={this.renderTitle(group.Title)}
            >
                {group.Children.map(opt => (
                    <AkSelect.Option key={opt.TagID} value={opt.TagName}>
                        {opt.TagName}
                        {/* <span className="certain-search-item-count">{opt.count} 人 关注</span> */}
                    </AkSelect.Option>
                ))}
            </AkSelect.OptGroup>
        ));
        // .concat([
        //     <AkSelect.Option disabled key="all" className="show-all">
        //         <a
        //             href=""
        //             target="_blank"
        //             rel="noopener noreferrer"
        //         >
        //             拓展更多
        //     </a>
        //     </AkSelect.Option>,
        // ]);
        let cl = classNames("ak-tagpicker", className)
        return (
            <AkRow className={cl}>
                {this.renderSelectTags()}
                {
                    (readonly || max_count <= selectValue.length) ? null : <div className="autocomplete">
                        <AutoComplete
                            allowClear={true}
                            dropdownStyle={{ width: 300 }}
                            dropdownClassName="tags-auto-dropdown"
                            value={this.selectValueText}
                            size="large"
                            style={{ width: '100%' }}
                            dataSource={tagoptions}
                            placeholder={this.formatMessage({ id: CommonLocale.TagName })}
                            optionLabelProp="value"
                            onChange={this.onChange.bind(this)}
                            onSelect={this.onSelect.bind(this)}
                        >
                            <Input onFocus={this.InputonFocus.bind(this)} readOnly={disable ? disable : false}
                                onPressEnter={this.inputonPressEnter.bind(this)} />
                        </AutoComplete>
                    </div>
                }
            </AkRow>
        );
    }

}

