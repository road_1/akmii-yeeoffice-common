import * as React from "react"
import { findDOMNode } from "react-dom";
import * as SparkMD5 from "spark-md5";
import * as superagent from "superagent";
import { AkFileOptions, AkUpload, AkFile } from "./ak-upload";
import { IconsAPI } from "../../api/common/icon";
import { AkIcon } from "./ak-icon";
import { AkNotification } from "./ak-notification";
import { UploadLocale, CommonLocale } from "../../locales/localeid";
import { AkUtil, AkGlobal, FileUpLoadCommon } from "../../util";
import { AppKeys, FileUploadMethod, AkUploadFile, AkContext } from "../../index";
import AkImg from "./ak-img";
import { AkSpin } from "./ak-spin";

export interface AkIconUploadProps {
    disabled?: boolean;
    onChange?: (value: string) => void;
    value?: string;
    defaultValue?: string;
    maxSize?: number;
    maxWidth?: number;
    maxHeight?: number;
    minWidth?: number;
    minHeight?: number;
    acceptExtensions?: string[];
    imgStyle?: React.CSSProperties;
    imgWidth?: number;
    imgHeight?: number;
    defaultLable?: string;
    defaultIcon?: string;
    className?: string;
    isAbsoluteUrl?: boolean;
    formatImageUrl?: (url: string) => string;
    description?: string | React.ReactNode;
    switchStatus?: boolean;
    maxSelection?: number;
}

export interface AkIconUploadStates {
    iconUrl?: string;
    loading?: boolean;
}

export class AkIconUpload extends React.Component<AkIconUploadProps, AkIconUploadStates> {
    constructor(props, context) {
        super(props, context);
        this.state = {
            iconUrl: ("value" in props) ? props.value : props.defaultValue,
            loading: false
        };
    }

    static defaultProps: AkIconUploadProps = {
        maxSize: 1024 * 1024 * 10,
        maxHeight: 150,
        maxWidth: 150,
        minHeight: 0,
        minWidth: 0,
        imgHeight: 96,
        imgWidth: 96,
        defaultIcon: "plus",
        acceptExtensions: ["gif", "jpg", "jpeg", "png"]
    }

    componentDidMount() {
        const $this = findDOMNode(this);
        const item = $this.childNodes[0].childNodes[0] as HTMLDivElement;
        item.style.width = `${this.props.imgWidth}px`;
        item.style.height = `${this.props.imgHeight}px`;

        this.setUploadPlaceholderStyle(this.props.imgWidth, this.props.imgHeight);
    }

    componentWillReceiveProps(nextProps: AkIconUploadProps) {
        if ("value" in nextProps && nextProps.value !== this.props.value) {
            this.setState({ iconUrl: nextProps.value });
        }

        if ("imgWidth" in nextProps && nextProps.imgHeight !== this.props.imgHeight) {
            this.setUploadPlaceholderStyle(nextProps.imgWidth, nextProps.imgHeight);
        }

        if ("imgHeight" in nextProps && nextProps.imgHeight !== this.props.imgWidth) {
            this.setUploadPlaceholderStyle(nextProps.imgWidth, nextProps.imgHeight);
        }
    }

    setUploadPlaceholderStyle(imgWidth: number, imgHeight: number) {
        const $this = findDOMNode(this);
        const item = $this.childNodes[0].childNodes[0] as HTMLDivElement;
        item.style.width = `${imgWidth}px`;
        item.style.height = `${imgHeight}px`;
    }

    /**自定义上传*/
    customRequest(options?: AkUploadFile) {
        const {formatMessage}=AkGlobal.intl;
        const { file } = options;
        this.setState({loading:true});
        let code = "";
        if (AkContext.getAppKey() === AppKeys.Flowcraft) {
            code = "flowcraft-img";
        } else {
            code = AkContext.getAppKey().toLowerCase() + "-img";
        }
        FileUploadMethod.UpLoadFile(code,options).then((onfulfilled)=>{
            let Url=onfulfilled.Data+"";
            // if (AkContext.getBranch() !== AkContext.YeeOffice) {
            //     let token = AkContext.getToken();
            //     token = encodeURIComponent(token);
            //     let index=Url.lastIndexOf("&akmiisecret")+13;
            //     Url=Url.substring(0,index)+token;
            // }
            Url+=("&IsImage=true&random="+AkUtil.guid());
            this.props.onChange?this.props.onChange(Url):null;
            this.setState({iconUrl:Url,loading:false});
            this.forceUpdate();
        },(onrejected)=>{
            let msg=onrejected.Message?onrejected.Message:formatMessage({ id: UploadLocale.MsgFileReadFail }, { file: file.name });
            AkNotification.error({
                message: formatMessage({ id: CommonLocale.Tip }),
                description: msg
            });
            this.forceUpdate();
        });
        
        // const { onChange, isAbsoluteUrl } = this.props;
        // const blobSlice = File.prototype.slice;
        // const fileReader = new FileReader();
        // fileReader.onload = () => {
        //     const spark = new SparkMD5();
        //     spark.append(fileReader.result);
        //     let md5 = spark.end();
        //     this.setState({ loading: true });
        //     IconsAPI.GetIcons({ md5: md5, appKey: window[AppKeys.CurrentAppKey], isAbsoluteUrl: isAbsoluteUrl }).then(data => {
        //         if (data.Data) {
        //             var request = superagent.put(data.Data.Url);
        //             request.set("content-type", "")
        //             request.set("Content-Disposition", "attachment;filename=" + encodeURI(file.name));
        //             request.send(this.dataURLtoBlob(fileReader.result)).on("progress", e => options.onProgress({ percent: e.percent }))
        //             request.end((error, response) => {
        //                 if (response.ok) {
        //                     this.setState({ iconUrl: data.Data.DownloadUrl, loading: false });
        //                     if (onChange) onChange(data.Data.DownloadUrl);
        //                 }
        //             });
        //         }
        //     });
        // };
        // /*使用FileReader读取文件*/
        // fileReader.readAsDataURL(blobSlice.call(file, 0, file.size));
    }

    beforeUpload(file: AkFile) {
        const { maxSize, maxWidth, maxHeight, minHeight, minWidth, acceptExtensions } = this.props;
        const {formatMessage}=AkGlobal.intl;
        let msg="";
        if (file.size === 0) {
            msg= formatMessage({id:UploadLocale.MsgNoContentError},{name:file.name});
            AkNotification.warning({message: formatMessage({ id: CommonLocale.Tip }),description: msg});
            return false;
        }
        if (file.size > maxSize) {
            msg=formatMessage( { id: UploadLocale.MsgIconSizeError }, { value: AkUtil.bytesToSize(maxSize) });
            AkNotification.warning({ message:formatMessage({ id: CommonLocale.Tip }),description: msg});
            return false;
        }
        if (acceptExtensions.indexOf(AkUtil.getFileExt(file.name)) === -1) {
            msg=formatMessage({ id: UploadLocale.MsgFileTypeError }, { exts: acceptExtensions.join(", ") });
            AkNotification.warning({ message:formatMessage({ id: CommonLocale.Tip }),description: msg});
            return false;
        }
        if (FileUpLoadCommon.validateFileNameSize(file.name) === false) {
            msg= formatMessage({id:UploadLocale.FileNameMoreLarge},{name:file.name});
            AkNotification.warning({message: formatMessage({ id: CommonLocale.Tip }),description: msg});
            return false;
        }
        if (FileUpLoadCommon.validateFileName(file.name) === false) {
            msg =formatMessage({id:UploadLocale.FileNameInvalidate},{name:file.name});
            AkNotification.warning({message: formatMessage({ id: CommonLocale.Tip }),description: msg});
            return false;
        }
        return new Promise((resolve) => {
            const fileReader = new FileReader();
            const blobSlice = File.prototype.slice;
            fileReader.onload = () => {
                var image = new Image();
                let des = "";
                let value = null;
                image.onload = () => {
                    if (image.width > maxWidth || image.height > maxHeight) {
                        if (maxWidth && maxHeight) {
                            des = UploadLocale.MsgIconMaxWidthAndHeight;
                            value = maxWidth + "x" + maxHeight;
                        } else if (maxWidth) {
                            des = UploadLocale.MsgIconMaxWidth;
                            value = maxWidth
                        } else if (maxHeight) {
                            des = UploadLocale.MsgIconMaxHeight;
                            value = maxHeight;
                        }
                        AkNotification.error({
                            message: formatMessage({ id: CommonLocale.Tip }),
                            description: formatMessage(
                                { id: des },
                                { value: value }
                            )
                        });
                    } else if (image.width < minWidth || image.height < minHeight) {
                        if (minWidth > 0 && minHeight > 0) {
                            des = UploadLocale.MsgIconMinWidthAndHeight
                            value = minWidth + "x" + minHeight;
                        } else if (minWidth > 0 && minHeight === 0) {
                            des = UploadLocale.MsgIconMinWidth;
                            value = minWidth
                        } else if (minHeight > 0 && minWidth === 0) {
                            des = UploadLocale.MsgIconMinHeight;
                            value = minHeight;
                        }
                        AkNotification.error({
                            message: formatMessage({ id: CommonLocale.Tip }),
                            description: formatMessage(
                                { id: des },
                                { value: value }
                            )
                        });
                    } else {
                        resolve(file);
                    }
                }
                image.src = fileReader.result;
            }
            fileReader.readAsDataURL(blobSlice.call(file, 0, file.size));
        });
    }

    // dataURLtoBlob(dataurl) {
    //     var arr = dataurl.split(","),
    //         mime = arr[0].match(/:(.*?);/)[1],
    //         bstr = atob(arr[1]),
    //         n = bstr.length,
    //         u8arr = new Uint8Array(n);
    //     while (n--) {
    //         u8arr[n] = bstr.charCodeAt(n);
    //     }
    //     return new Blob([u8arr], { type: mime });
    // }

    render() {
        const { defaultLable, defaultIcon, className, disabled, imgStyle, formatImageUrl, description, imgWidth, imgHeight,onChange } = this.props;
        const { iconUrl, loading } = this.state;
        let defaultImg

        if (iconUrl&&iconUrl!=="uploading") {
            const img = <AkImg src={formatImageUrl ? formatImageUrl(iconUrl) : iconUrl}
                style={imgStyle} width={imgWidth - 2} height={imgHeight - 2} magnifier={false} />
            if (loading) {
                defaultImg = <div onClick={e => { e.preventDefault(); e.stopPropagation(); }}>
                    <AkSpin spinning>{img}</AkSpin>
                </div>
            } else {
                defaultImg = img;
            }
        } else {
            defaultImg = <div className="ak-icon-upload">
                <AkIcon type={defaultIcon} style={{ lineHeight: `${imgHeight}px` }} />
                <div>{defaultLable}</div>
            </div>;
        }

        return <div className="ak-icon-upload-wrapper">
            <AkUpload className={className}
                action=""
                accept="image/gif, image/jpg, image/jpeg, image/png"
                listType="picture-card"
                showUploadList={false}
                onChange={(value)=>{
                    onChange(value.file.status);
                }}
                beforeUpload={file => this.beforeUpload(file)}
                customRequest={options => this.customRequest(options)}
                disabled={disabled}>
                {defaultImg}
            </AkUpload>
            {description}
        </div>;
    }
}