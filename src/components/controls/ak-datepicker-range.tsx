import * as React from "react";
import {Component} from "react";
import * as moment from "moment";
import {AkTimePickerProps} from "./ak-timepicker";
import {AkPickerProps, AkDatePicker} from "./AkDatePicker";
export declare type AkRangePickerValue = undefined[] | [moment.Moment] | [undefined, moment.Moment] | [moment.Moment, moment.Moment];
export declare type AkRangePickerPresetRange = AkRangePickerValue | (() => AkRangePickerValue);
export interface AkRangePickerProps extends AkPickerProps {
    className?: string;
    value?: AkRangePickerValue;
    defaultValue?: AkRangePickerValue;
    defaultPickerValue?: AkRangePickerValue;
    onCalendarChange?: (dates: AkRangePickerValue, dateStrings: [string, string]) => void;
    onOk?: (selectedTime: moment.Moment) => void;
    ranges?: {
        [range: string]: AkRangePickerPresetRange;
    };
    placeholder?: [string, string];
    mode?: string | string[];
    disabledTime?: (current: moment.Moment, type: string) => {
        disabledHours?: () => number[];
        disabledMinutes?: () => number[];
        disabledSeconds?: () => number[];
    };
    onPanelChange?: (value?: AkRangePickerValue, mode?: string | string[]) => void;
    onChange?: (dateStrings: [string, string], dates: AkRangePickerValue) => void;
    showTime?: AkTimePickerProps | boolean;
}
export interface AkRangePickerStates {
    value?: [moment.Moment, moment.Moment];
}

export class AkDatePickerRange extends Component < AkRangePickerProps,
    AkRangePickerStates > {

    constructor(props, context) {
        super(props, context);

        let value = ("value" in props) ? props.value : props.defaultValue;
        this.state = {value: this.processValue(value)};
    }

    componentWillReceiveProps(nextProps) {
        if ("value" in nextProps && nextProps.value !== this.props.value) {
            this.setState({value: this.processValue(nextProps.value)});
        }
    }

    processSingleValue(value): moment.Moment {
        let v;
        if (value && typeof value === "string") {
            v = moment(value);
            if (!v.isValid()) {
                v = undefined;
            }
        } else if (typeof value === "string") {
            v = undefined;
        } else {
            v = value;
        }
        return v;
    }

    processValue(value): [moment.Moment, moment.Moment] {
        let rs: [moment.Moment, moment.Moment] = [undefined, undefined];
        if (value && value.length > 0) {
            rs[0] = this.processSingleValue(value[0]);
            rs[1] = value.length > 1 ? this.processSingleValue(value[1]) : undefined;
        }
        return rs;
    }

    onChange(dates: [moment.Moment, moment.Moment], dateStrings: [string, string]) {

        if (!("value" in this.props)) {
            this.setState({value: dates});
        }

        this.props.onChange && this.props.onChange(dateStrings, dates);
    }

    render() {
        const {defaultValue, value, onChange, ...others} = this.props;

        return <AkDatePicker.RangePicker value={this.state.value}
                                         onChange={this.onChange.bind(this)} {...others}></AkDatePicker.RangePicker>
    }
}
