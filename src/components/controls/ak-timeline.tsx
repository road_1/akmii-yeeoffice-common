import * as React from 'react'
import { Component } from 'react'
import { Timeline } from 'antd'

export interface AkTimelineProps {
    prefixCls?: string;
    className?: string;
    /** 指定最后一个幽灵节点是否存在或内容 */
    pending?: React.ReactNode;
    style?: React.CSSProperties;
}
export interface AkTimelineStates { }

export class AkTimeline extends Component<AkTimelineProps, AkTimelineStates> {
    static Item?: React.ReactNode|any = Timeline.Item;
    render() {

        return <Timeline {...this.props} ></Timeline>
    }
}