import * as React from "react";
import LazyLoad from "react-lazyload";
import * as classNames from "classnames";
import { AkOneUp } from "./ak-oneup";
import { AkUtil } from "../..";

/*
oss resize
https://help.aliyun.com/document_detail/44688.html?spm=5176.doc44693.6.939.tQQDrt
*/

export interface AkImgProps {
    src?: string;
    width?: number | string;
    height?: number | string;
    placeholder?: string;
    shape?: "circle" | "square";
    magnifier?: boolean;
    style?: React.CSSProperties;
    mode?: "lfit" | "mfit" | "fill" | "pad" | "fixed";
    limit?: "0" | "1";
    className?: string;
    lazyLoad?: boolean;
    isImsStyle?: boolean;
    contentListType?: number;//内容列表类型
}

export interface AkImgState {
    visible?: boolean;
}

export default class AkImg extends React.Component<AkImgProps, AkImgState> {
    contentListTypeDefaultImg = {
        dataImg: process.env.CDN + "images/dataListDefaultImg.png",
        calendarImg: process.env.CDN + "images/calendarDefaultImg.png",
        documentImg: process.env.CDN + "images/documentDefaultImg.png",
        listSetImg: process.env.CDN + "images/clapp.png"
    };
    static defaultProps: AkImgProps = {
        shape: "square",
        width: 150,
        height: 150,
        placeholder: process.env.CDN + "images/placeholder.png",
        magnifier: true,
        mode: "fill",
        limit: "0"
    };

    constructor(props, context) {
        super(props, context);

        this.state = {
            visible: false
        };
    }

    onImgClick = (e) => {
        if (this.props.magnifier) {
            e.stopPropagation();
            e.preventDefault();
            this.setState({ visible: true });
        }
    }

    render() {
        const { contentListTypeDefaultImg } = this;
        const { src, width, height, limit, mode, placeholder, magnifier, style, className, lazyLoad, isImsStyle, contentListType } = this.props;
        const resize = typeof width === "number" && typeof height === "number";
        const query = resize ? `?x-oss-process=image/resize,m_${mode},limit_${limit},w_${width},h_${height}` : "";
        let ph = "";

        //判断是否传入内容列表并判断传入的类型
        if (contentListType) {
            switch (contentListType) {
                case 1://当列表类型为1表示为数据列表
                    ph = contentListTypeDefaultImg.dataImg + query;
                    break;

                case 8://日历列表
                    ph = contentListTypeDefaultImg.calendarImg + query;
                    break;

                case 16://文档列表;
                    ph = contentListTypeDefaultImg.documentImg + query;
                    break;
                case 1024://列表集;
                    ph = contentListTypeDefaultImg.listSetImg + query;
                    break;
            }
        } else {
            ph = placeholder && placeholder.startsWith("data:") ? placeholder : placeholder + query;
        }

        const imgStyle: React.CSSProperties = { width, height, ...style };
        let parsed = AkUtil.JSONParse(src);
        let imgSrc = Array.isArray(parsed) ? parsed : (src ? [src] : []);
        // let imgSrc = Array.isArray(AkUtil.JSONParse(src)) ? AkUtil.JSONParse(src) : (src ? [src] : []);
        let url = imgSrc.length > 0 ? imgSrc[imgSrc.length - 1] : ''

        if (this.props.shape === "circle") { imgStyle["borderRadius"] = "50%" };
        let img = <img className={classNames({ "cursor": magnifier })}
            src={url ? (url.startsWith("data:") ? url : (url + query)) : ph} style={!isImsStyle ? imgStyle : null}
            onClick={this.onImgClick}
            onError={e => e.target["src"] = ph} />;
        return <div style={{ width, height, lineHeight: 0 }} className={classNames("ak-img", className)}>
            {lazyLoad !== false ? <LazyLoad placeholder={<img src={ph} style={imgStyle} />}>
                {img}
            </LazyLoad> : img}
            <AkOneUp visible={this.state.visible} src={url} onClose={() => this.setState({ visible: false })} />
        </div>
    }
}