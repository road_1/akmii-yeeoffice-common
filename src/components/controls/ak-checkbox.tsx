import * as React from "react";
import {Checkbox} from "antd";
import Component = React.Component;
import { AkUtil } from "../..";

export interface AkAbstractCheckboxProps {
    prefixCls?: string;
    className?: string;
    defaultChecked?: boolean;
    checked?: boolean;
    style?: React.CSSProperties;
    disabled?: boolean;
    defaultValue?: boolean;
    onChange?: React.ChangeEventHandler<HTMLInputElement>;
    onMouseEnter?: React.MouseEventHandler<any>;
    onMouseLeave?: React.MouseEventHandler<any>;
    // onKeyPress?: React.KeyboardEventHandler<any>;
    // onKeyDown?: React.KeyboardEventHandler<any>;
    value?: any;
    // tabIndex?: number;
    name?: string;
    children?: React.ReactNode;
}
export interface AkCheckboxProps extends AkAbstractCheckboxProps {
    indeterminate?: boolean;
}

export class AkCheckbox extends Component<AkCheckboxProps, any> {
    static Group?: any = Checkbox.Group;

    render() {
        let {defaultValue, defaultChecked, value, checked, ...other} = this.props;

        value = AkUtil.toBoolean(value);
        defaultValue = AkUtil.toBoolean(defaultValue);

        let props = {}
        if (defaultValue !== undefined || defaultChecked !== undefined) {
            Object.assign(props, {defaultChecked: defaultChecked === undefined ? defaultValue : defaultChecked});
        }

        if (value !== undefined || checked !== undefined) {
            Object.assign(props, {checked: checked === undefined ? value : checked});
        }

        return <Checkbox {...props} {...other}></Checkbox>
    }
}
