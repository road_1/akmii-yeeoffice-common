import * as React from "react";
import * as classNames from "classnames";
import { Component } from "react";
import { Layout } from "antd";
import { BasicProps } from "antd/lib/layout/layout";

export declare type CollapseType = 'clickTrigger' | 'responsive';
export interface AkSiderProps {
    // 是否导航和内容分开滚动
    isdefaultFixed?: boolean;

    style?: React.CSSProperties;
    prefixCls?: string;
    className?: string;
    collapsible?: boolean;
    collapsed?: boolean;
    defaultCollapsed?: boolean;
    reverseArrow?: boolean;
    onCollapse?: (collapsed: boolean, type: 'clickTrigger' | 'responsive') => void;
    trigger?: React.ReactNode;
    width?: number | string;
    collapsedWidth?: number | string;
    // breakpoint?: 'xs' | 'sm' | 'md' | 'lg' | 'xl' | 'xxl';
    breakpoint?: 'xs' | 'sm' | 'md' | 'lg' | 'xl';
}
export default class AkSider extends React.Component<AkSiderProps, any> {
    static __ANT_LAYOUT_SIDER: any = true;
    static defaultProps: {
        prefixCls: string;
        collapsible: boolean;
        defaultCollapsed: boolean;
        reverseArrow: boolean;
        width: number;
        collapsedWidth: number;
        style: {};
    };
    render() {
        let { isdefaultFixed, className, ...others } = this.props;
        className = classNames(className, { "ak-sider-fixed": isdefaultFixed })
        return <Layouts.Sider className={className} {...others}></Layouts.Sider>
    }
}

export interface AkLayoutProps {
    style?: React.CSSProperties;
    prefixCls?: string;
    className?: string;
    // hasSider?: boolean;
}

export interface AkLayoutStates { }
export class AkLayout extends Component<AkLayoutProps,
    AkLayoutStates> {
    constructor(props, context) {
        super(props, context);
    }

    static Header: any = Layout.Header;
    static Footer: any = Layout.Footer;
    static Content: any = Layout.Content;
    static Sider: any = AkSider;
    render() {
        return <Layouts {...this.props}></Layouts>
    }
}
export const Layouts: React.Component<BasicProps, any> | any = Layout;
class AkLayoutStyle { }
