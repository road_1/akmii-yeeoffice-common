import * as React from "react"
import { findDOMNode } from "react-dom";
import * as SparkMD5 from "spark-md5";
import * as superagent from "superagent";
import { AkFileOptions, AkUpload, AkFile } from "./ak-upload";
import { IconsAPI } from "../../api/common/icon";
import { AkIcon } from "./ak-icon";
import { AkNotification } from "./ak-notification";
import { UploadLocale, CommonLocale } from "../../locales/localeid";
import { AkUtil, AkGlobal, FileUpLoadCommon } from "../../util";
import { AppKeys, FileUploadMethod, AkUploadFile, AkContext, AkModal, AkButton } from "../../index";
import AkImg from "./ak-img";
import { AkSpin } from "./ak-spin";
import Cropper from "react-cropper";
import'cropperjs/dist/cropper.css';

export interface AkCropIconUploadProps {
    disabled?: boolean;
    onChange?: (value: string) => void;
    value?: string;
    defaultValue?: string;
    maxSize?: number;
    maxWidth?: number;
    maxHeight?: number;
    minWidth?: number;
    minHeight?: number;
    acceptExtensions?: string[];
    imgStyle?: React.CSSProperties;
    imgWidth?: number;
    imgHeight?: number;
    defaultLable?: string;
    defaultIcon?: string;
    className?: string;
    isAbsoluteUrl?: boolean;
    formatImageUrl?: (url: string) => string;
    description?: string | React.ReactNode;
    switchStatus?: boolean;
    maxSelection?: number;

    fixedOutputWidth?:number;//剪切过后的图片宽
    fixedOutputHeight?:number;//剪切过后的图片高
    isSquare?:boolean;//剪切框是否为正方形
    imageQuality?:"low" | "high";//图像高清度(默认low)
    imageEnabled?:boolean;//图片是否平滑(与像素有关，默认false)
}

export interface AkCropIconUploadStates {
    iconUrl?: string;
    loading?: boolean;

    showCropModal?:boolean;
    imageSrc?:string;
}

export class AkCropIconUpload extends React.Component<AkCropIconUploadProps, AkCropIconUploadStates> {
    cropper?:any;
    imgCode?:string;
    imgOption?:AkUploadFile;
    rotate?:number;
    constructor(props, context) {
        super(props, context);
        this.rotate = 0;
        this.state = {
            iconUrl: ("value" in props) ? props.value : props.defaultValue,
            loading: false,
            showCropModal:false,
            imageSrc:""
        };

        this.cropper = null;
    }

    static defaultProps: AkCropIconUploadProps = {
        maxSize: 1024 * 1024 * 10,
        maxHeight: 150,
        maxWidth: 150,
        minHeight: 0,
        minWidth: 0,
        imgHeight: 96,
        imgWidth: 96,
        defaultIcon: "plus",
        acceptExtensions: ["gif", "jpg", "jpeg", "png"],

        fixedOutputWidth:150,
        fixedOutputHeight:150,
        isSquare:false,
        imageQuality:"low"
    }

    componentDidMount() {
        const $this = findDOMNode(this);
        const item = $this.childNodes[0].childNodes[0] as HTMLDivElement;
        item.style.width = `${this.props.imgWidth}px`;
        item.style.height = `${this.props.imgHeight}px`;

        this.setUploadPlaceholderStyle(this.props.imgWidth, this.props.imgHeight);
    }

    componentWillReceiveProps(nextProps: AkCropIconUploadProps) {
        if ("value" in nextProps && nextProps.value !== this.props.value) {
            this.setState({ iconUrl: nextProps.value });
        }

        if ("imgWidth" in nextProps && nextProps.imgHeight !== this.props.imgHeight) {
            this.setUploadPlaceholderStyle(nextProps.imgWidth, nextProps.imgHeight);
        }

        if ("imgHeight" in nextProps && nextProps.imgHeight !== this.props.imgWidth) {
            this.setUploadPlaceholderStyle(nextProps.imgWidth, nextProps.imgHeight);
        }
    }

    setUploadPlaceholderStyle(imgWidth: number, imgHeight: number) {
        const $this = findDOMNode(this);
        const item = $this.childNodes[0].childNodes[0] as HTMLDivElement;
        item.style.width = `${imgWidth}px`;
        item.style.height = `${imgHeight}px`;
    }

    /**自定义上传*/
    customRequest(options?: AkUploadFile) {
        this.setState({
            loading:true,
        });
        const {formatMessage}=AkGlobal.intl;
        const { file } = options;
        let code = "";
        if (AkContext.getAppKey() === AppKeys.Flowcraft) {
            code = "flowcraft-img";
        } else {
            code = AkContext.getAppKey().toLowerCase() + "-img";
        }

        this.imgOption = options;
        this.imgCode = code;

        if (!this.state.showCropModal) {
            FileUploadMethod.UpLoadFile(code,options).then((onfulfilled)=>{
                let Url=onfulfilled.Data+"";
                // if (AkContext.getBranch() !== AkContext.YeeOffice) {
                //     let token = AkContext.getToken();
                //     token = encodeURIComponent(token);
                //     let index=Url.lastIndexOf("&akmiisecret")+13;
                //     Url=Url.substring(0,index)+token;
                // }
                Url+=("&IsImage=true&random="+AkUtil.guid());
                this.props.onChange ? this.props.onChange(Url) : null;
                this.setState({iconUrl:Url,loading:false});
                this.forceUpdate();
            },(onrejected)=>{
                let msg=onrejected.Message?onrejected.Message:formatMessage({ id: UploadLocale.MsgFileReadFail }, { file: file.name });
                AkNotification.error({
                    message: formatMessage({ id: CommonLocale.Tip }),
                    description: msg
                });
                this.forceUpdate();
            });
        }
        
        // const { onChange, isAbsoluteUrl } = this.props;
        // const blobSlice = File.prototype.slice;
        // const fileReader = new FileReader();
        // fileReader.onload = () => {
        //     const spark = new SparkMD5();
        //     spark.append(fileReader.result);
        //     let md5 = spark.end();
        //     this.setState({ loading: true });
        //     IconsAPI.GetIcons({ md5: md5, appKey: window[AppKeys.CurrentAppKey], isAbsoluteUrl: isAbsoluteUrl }).then(data => {
        //         if (data.Data) {
        //             var request = superagent.put(data.Data.Url);
        //             request.set("content-type", "")
        //             request.set("Content-Disposition", "attachment;filename=" + encodeURI(file.name));
        //             request.send(this.dataURLtoBlob(fileReader.result)).on("progress", e => options.onProgress({ percent: e.percent }))
        //             request.end((error, response) => {
        //                 if (response.ok) {
        //                     this.setState({ iconUrl: data.Data.DownloadUrl, loading: false });
        //                     if (onChange) onChange(data.Data.DownloadUrl);
        //                 }
        //             });
        //         }
        //     });
        // };
        // /*使用FileReader读取文件*/
        // fileReader.readAsDataURL(blobSlice.call(file, 0, file.size));
    }

    beforeUpload(file: AkFile) {
        const { maxSize, maxWidth, maxHeight, minHeight, minWidth, acceptExtensions } = this.props;
        const {formatMessage}=AkGlobal.intl;
        const topThis = this;
        let msg="";
        if (file.size === 0) {
            msg= formatMessage({id:UploadLocale.MsgNoContentError},{name:file.name});
            AkNotification.warning({message: formatMessage({ id: CommonLocale.Tip }),description: msg});
            return false;
        }
        // if (file.size > maxSize) {
        //     msg=formatMessage( { id: UploadLocale.MsgIconSizeError }, { value: AkUtil.bytesToSize(maxSize) });
        //     AkNotification.warning({ message:formatMessage({ id: CommonLocale.Tip }),description: msg});
        //     return false;
        // }
        if (acceptExtensions.indexOf(AkUtil.getFileExt(file.name)) === -1) {
            msg=formatMessage({ id: UploadLocale.MsgFileTypeError }, { exts: acceptExtensions.join(", ") });
            AkNotification.warning({ message:formatMessage({ id: CommonLocale.Tip }),description: msg});
            return false;
        }
        if (FileUpLoadCommon.validateFileNameSize(file.name) === false) {
            msg= formatMessage({id:UploadLocale.FileNameMoreLarge},{name:file.name});
            AkNotification.warning({message: formatMessage({ id: CommonLocale.Tip }),description: msg});
            return false;
        }
        if (FileUpLoadCommon.validateFileName(file.name) === false) {
            msg =formatMessage({id:UploadLocale.FileNameInvalidate},{name:file.name});
            AkNotification.warning({message: formatMessage({ id: CommonLocale.Tip }),description: msg});
            return false;
        }
        return new Promise((resolve) => {
            const fileReader = new FileReader();
            const blobSlice = File.prototype.slice;
            fileReader.onload = () => {
                var image = new Image();
                let des = "";
                let value = null;
                image.onload = () => {
                    this.setState({
                        imageSrc:image.src,
                    });
                    if (image.width > AkCropIconUpload.defaultProps.fixedOutputWidth || image.height > AkCropIconUpload.defaultProps.fixedOutputHeight) {
                        topThis.setState({
                            showCropModal:true,
                        });
                        resolve(file);
                        // resolve(file);
                        // if (maxWidth && maxHeight) {
                        //     des = UploadLocale.MsgIconMaxWidthAndHeight;
                        //     value = maxWidth + "x" + maxHeight;
                        // } else if (maxWidth) {
                        //     des = UploadLocale.MsgIconMaxWidth;
                        //     value = maxWidth
                        // } else if (maxHeight) {
                        //     des = UploadLocale.MsgIconMaxHeight;
                        //     value = maxHeight;
                        // }
                        // AkNotification.error({
                        //     message: formatMessage({ id: CommonLocale.Tip }),
                        //     description: formatMessage(
                        //         { id: des },
                        //         { value: value }
                        //     )
                        // });
                    } else {
                        topThis.setState({
                            showCropModal:false,
                        });
                        resolve(file);
                    }
                }
                image["src"] = fileReader.result.toString();
            }
            fileReader.readAsDataURL(blobSlice.call(file, 0, file.size));
        });
    }

    cropperModalClose = () =>{
        this.setState({
            showCropModal:false,
            loading:false
        });
    }

    dataURLtoBlob(dataurl) {
        var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new Blob([u8arr], { type: mime });
    }

    blobToFile(theBlob?:Blob,file?:AkFile){
        var b:any = theBlob;
        b.uid = file.uid;
        b.lastModifiedDate = new Date();
        b.name = file.name;
        b.filename = file.filename;
        b.lastModified = file.lastModified;
        b.url = file.url;
        b.status = file.status;
        b.percent = file.percent;
        b.thumbUrl = file.thumbUrl;
        b.originFileObj = file.originFileObj;
        b.fileKey = file.fileKey;
        b.response = file.response;
        b.error = file.error;
        b.linkProps = file.linkProps;

        return b as AkFile;
    }

    cropImage = () =>{
        const topThis = this;
        const {formatMessage}=AkGlobal.intl;
        if (this.cropper.getCroppedCanvas() === "null") {
            return false;
        }

        const canvasData = {
            width: topThis.props.fixedOutputWidth,
            height: topThis.props.fixedOutputHeight,
            imageSmoothingEnabled:topThis.props.imageEnabled ? topThis.props.imageEnabled : false,
            imageSmoothingQuality:topThis.props.imageQuality ? topThis.props.imageQuality : "low",
        };

        let srcCroppered = this.cropper.getCroppedCanvas(canvasData).toDataURL(this.imgOption.file.type);

        let fileBlob = this.dataURLtoBlob(srcCroppered);

        let cropOptions = {...this.imgOption};
        let cropFile = this.blobToFile(fileBlob,cropOptions.file);
        cropOptions.file = cropFile;


        FileUploadMethod.UpLoadFile(this.imgCode,cropOptions).then((onfulfilled)=>{
            let Url=onfulfilled.Data+"";
            // if (AkContext.getBranch() !== AkContext.YeeOffice) {
            //     let token = AkContext.getToken();
            //     token = encodeURIComponent(token);
            //     let index=Url.lastIndexOf("&akmiisecret")+13;
            //     Url=Url.substring(0,index)+token;
            // }
            Url+=("&IsImage=true&random="+AkUtil.guid());
            this.props.onChange?this.props.onChange(Url):null;
            this.setState({iconUrl:Url,loading:false,showCropModal:false});
            this.forceUpdate();
        },(onrejected)=>{
            let msg=onrejected.Message?onrejected.Message:formatMessage({ id: UploadLocale.MsgFileReadFail }, { file: cropOptions.file.name });
            AkNotification.error({
                message: formatMessage({ id: CommonLocale.Tip }),
                description: msg
            });
            this.forceUpdate();
        });

        // lrz(srcCroppered, {
        //     width: 150,
        //     height: 150,
        //     quality: 1
        // }).then((rest:any) => {
        //     let up = {...this.imgOption};
            
        //     FileUploadMethod.UpLoadFile(this.imgCode,up).then((onfulfilled)=>{
        //     let Url=onfulfilled.Data+"";
        //     // if (AkContext.getBranch() !== AkContext.YeeOffice) {
        //     //     let token = AkContext.getToken();
        //     //     token = encodeURIComponent(token);
        //     //     let index=Url.lastIndexOf("&akmiisecret")+13;
        //     //     Url=Url.substring(0,index)+token;
        //     // }
        //     Url+=("&IsImage=true&random="+AkUtil.guid());
        //     this.props.onChange?this.props.onChange(Url):null;
        //     this.setState({iconUrl:Url,loading:false,showCropModal:false});
        //     this.forceUpdate();
        // },(onrejected)=>{
        //     let msg=onrejected.Message?onrejected.Message:formatMessage({ id: UploadLocale.MsgFileReadFail }, { file: this.imgOption.file.name });
        //     AkNotification.error({
        //         message: formatMessage({ id: CommonLocale.Tip }),
        //         description: msg
        //     });
        //     this.forceUpdate();
        // });
        // });
    }

    //旋转
    retateClickLeft = () =>{
        if (Math.abs(this.rotate) === 360) {
            this.rotate = 0;
        }
        this.rotate-=90;
        this.cropper.rotateTo(this.rotate);
    }

    retateClickRight = () =>{
        if (Math.abs(this.rotate) === 360) {
            this.rotate = 0;
        }
        this.rotate+=90;
        this.cropper.rotateTo(this.rotate);
    }

    renderCropTitle(){
        const {formatMessage}=AkGlobal.intl;
        return (
            <div className="crop-image-modal-title">
                <i className="anticon anticon-close modal-close" onClick={this.cropperModalClose}></i>
                <div className="right">
                <i className="anticon anticon-rotate-left crop-rotate" onClick={this.retateClickLeft}></i>
                <i className="anticon anticon-rotate-right crop-rotate" onClick={this.retateClickRight}></i>
                <button className="crop-btn" onClick={this.cropImage}> {formatMessage({ id: CommonLocale.ImageCropSubMit })}</button>
                </div>
            </div>
        );
    }

    // dataURLtoBlob(dataurl) {
    //     var arr = dataurl.split(","),
    //         mime = arr[0].match(/:(.*?);/)[1],
    //         bstr = atob(arr[1]),
    //         n = bstr.length,
    //         u8arr = new Uint8Array(n);
    //     while (n--) {
    //         u8arr[n] = bstr.charCodeAt(n);
    //     }
    //     return new Blob([u8arr], { type: mime });
    // }

    render() {
        const { defaultLable,isSquare, defaultIcon, className, disabled, imgStyle, formatImageUrl, description, imgWidth, imgHeight,onChange } = this.props;
        const { iconUrl, loading } = this.state;
        let defaultImg

        if (iconUrl&&iconUrl!=="uploading") {
            const img = <AkImg className="ak-showImgcenter" isImsStyle={true} src={formatImageUrl ? formatImageUrl(iconUrl) : iconUrl}
                style={imgStyle} width={imgWidth - 2} height={imgHeight - 2} magnifier={false} />
            if (loading) {
                defaultImg = <div onClick={e => { e.preventDefault(); e.stopPropagation(); }}>
                    <AkSpin spinning>{img}</AkSpin>
                </div>
            } else {
                defaultImg = img;
            }
        } else {
            defaultImg = <div className="ak-icon-upload">
                <AkIcon type={defaultIcon} style={{ lineHeight: `${imgHeight}px` }} />
                <div>{defaultLable}</div>
            </div>;
        }

        return <div className="ak-icon-upload-wrapper">
            <AkUpload className={className}
                action=""
                accept="image/gif, image/jpg, image/jpeg, image/png"
                listType="picture-card"
                showUploadList={false}
                onChange={(value)=>{
                    onChange(value.file.status);
                }}
                beforeUpload={file => this.beforeUpload(file)}
                customRequest={options => this.customRequest(options)}
                disabled={disabled}>
                {defaultImg}
            </AkUpload>
            <AkModal 
            className="crop-image-modal"
            closable={false}
            footer={null}
            visible={this.state.showCropModal}
            onCancel={this.cropperModalClose}
            title={this.renderCropTitle()}
            dragable={false}
            >
            <div className="crop-image-modal-content">
            <Cropper
                className="crop-image-modal-crop"
                ref={Cropper => this.cropper = Cropper}
                src={this.state.imageSrc}
                
                viewModal={1}
                aspectRatio={isSquare ? 1 : null}//剪裁框拖动时的宽高比
                autoCrop={true}//剪裁框是否可拖动
                movable={false}//图片是否可拖动
                toggleDragModeOnDblclick={false}//双击是否可以切换
                zoomOnWheel={false}//缩小或放大图片
                background={false}//网格背景
                highlight={false}
                        />
                        </div>
            </AkModal>
            {description}
        </div>;
    }
}