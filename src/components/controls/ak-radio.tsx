import * as React from "react";
import {Component} from "react";
import {Radio} from "antd";
import * as classNames from "classnames";
import { AkAbstractCheckboxProps } from './ak-checkbox';

function getCheckedValue(children) {
    let value = null;
    let matched = false;
    React.Children.forEach(children, (radio: any) => {
        if (radio && radio.props && radio.props.checked) {
            value = radio.props.value;
            matched = true;
        }
    });
    return matched ? {value} : undefined;
}

export interface RadioGroupProps {
    prefixCls?: string;
    className?: string;
    /** 选项变化时的回调函数*/
    onChange?: React.ChangeEventHandler<HTMLInputElement>;
    /** 用于设置当前选中的值*/
    value?: any;
    /** 默认选中的值*/
    defaultValue?: any;
    /**  大小，只对按钮样式生效*/
    size?: 'large' | 'default' | 'small';
    style?: React.CSSProperties;
    disabled?: boolean;
    onMouseEnter?: React.MouseEventHandler<HTMLDivElement>;
    onMouseLeave?: React.MouseEventHandler<HTMLDivElement>;
    options?:Array<{ label: string ,value: string | number,disabled?: boolean }>;
    name?: string;
    children?: React.ReactNode;
    id?: string;
}

export class AkRadioGroup extends React.Component<RadioGroupProps, any> {
    static defaultProps = {
        disabled: false,
    };

    constructor(props) {
        super(props);
        let value;
        if ('value' in props) {
            value = props.value;
        } else if ('defaultValue' in props) {
            value = props.defaultValue;
        } else {
            const checkedValue = getCheckedValue(props.children);
            value = checkedValue && checkedValue.value;
        }
        this.state = {
            value,
        };
    }

    componentWillReceiveProps(nextProps) {
        if ('value' in nextProps) {
            this.setState({
                value: nextProps.value,
            });
        } else {
            const checkedValue = getCheckedValue(nextProps.children);
            if (checkedValue) {
                this.setState({
                    value: checkedValue.value,
                });
            }
        }
    }

    onRadioChange = (ev) => {
        const lastValue = this.state.value;
        const {value} = ev.target;
        if (!('value' in this.props)) {
            this.setState({
                value,
            });
        }

        const onChange = this.props.onChange;
        if (onChange && value !== lastValue) {
            onChange(ev);
        }
    }

    render() {
        const props = this.props;
        const children = !props.children ? [] : React.Children.map(props.children, (radio: any) => {
                if (radio && (radio.type === AkRadio || radio.type === Radio || radio.type === AkRadio.Button) && radio.props) {
                    return React.cloneElement(radio, Object.assign({}, radio.props, {
                        onChange: this.onRadioChange,
                        checked: this.state.value === radio.props.value,
                        disabled: radio.props.disabled || this.props.disabled,
                    }));
                }
                return radio;
            });

        const {prefixCls = 'ant-radio-group', className = ''} = props;
        const classString = classNames(prefixCls, {
            [`${prefixCls}-${props.size}`]: props.size,
        }, className);
        return (
            <div
                className={classString}
                style={props.style}
                onMouseEnter={props.onMouseEnter}
                onMouseLeave={props.onMouseLeave}
            >
                {children}
            </div>
        );
    }
}

export interface AkRadioProps extends AkAbstractCheckboxProps {
}
export interface AkRadioStates {
}
export class AkRadio extends Component < AkRadioProps,
    AkRadioStates > {

    static Group = AkRadioGroup;
    static Button: any = Radio.Button;

    constructor(props, context) {
        super(props, context);
    }

    render() {
        return <Radio {...this.props}></Radio>
    }
}
