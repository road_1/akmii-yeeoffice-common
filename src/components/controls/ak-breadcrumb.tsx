import * as React from 'react'
import { Component } from 'react'
import { Breadcrumb } from 'antd';

export interface AkBreadcrumbProps {
    prefixCls?: string;
    routes?: Array<any>;
    params?: Object;
    separator?: React.ReactNode;
    itemRender?: (route: any, params: any, routes: Array<any>, paths: Array<string>) => React.ReactNode;
    style?: React.CSSProperties;
    className?: string;
}
export interface AkBreadcrumbStates { }
export class AkBreadcrumb extends Component<AkBreadcrumbProps, AkBreadcrumbStates> {
    static Item = Breadcrumb.Item;
    render() {
        return <Breadcrumb {...this.props}></Breadcrumb>;
    }
}
class AkBreadcrumbStyle { }