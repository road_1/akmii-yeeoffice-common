import * as React from 'react'
import { Component } from 'react'
import { Carousel } from "antd";


export declare type AkCarouselEffect = 'scrollx' | 'fade'
export interface AkCarouselProps {
    effect?: AkCarouselEffect;
    dots?: boolean;
    vertical?: boolean;
    autoplay?: boolean;
    easing?: string;
    beforeChange?: (from: number, to: number) => void;
    afterChange?: (current: number) => void;
    style?: React.CSSProperties;
    prefixCls?: string;
    accessibility?: boolean;
    nextArrow?: HTMLElement | any;
    prevArrow?: HTMLElement | any;
    pauseOnHover?: boolean;
    className?: string;
    adaptiveHeight?: boolean;
    arrows?: boolean;
    autoplaySpeed?: number;
    centerMode?: boolean;
    centerPadding?: string | any;
    cssEase?: string | any;
    dotsClass?: string;
    draggable?: boolean;
    fade?: boolean;
    focusOnSelect?: boolean;
    infinite?: boolean;
    initialSlide?: number;
    lazyLoad?: boolean;
    rtl?: boolean;
    slide?: string;
    slidesToShow?: number;
    slidesToScroll?: number;
    speed?: number;
    swipe?: boolean;
    swipeToSlide?: boolean;
    touchMove?: boolean;
    touchThreshold?: number;
    variableWidth?: boolean;
    useCSS?: boolean;
    slickGoTo?: number;
}
export interface AkCarouselStates { }
export class AkCarousel extends Component<AkCarouselProps, AkCarouselStates> {
    render() {
        return <Carousel {...this.props}></Carousel>
    }
} 