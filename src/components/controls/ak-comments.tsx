import * as classNames from "classnames";
import * as moment from "moment";
import * as React from "react";
import { connect } from 'react-redux';
import { CommentAction } from '../../actions/index';
import { CommentAddRequest, CommentDeleteRequest, GetCommentRequest, CommentMoreRequest } from "../../api/comment/commentmodel";
import { AkContext } from "../../index";
import { AkCommentsLocale, CommonLocale } from "../../locales/localeid";
import { CommentHolder, CommentReducerUtil } from '../../reducers/commentReducer';
import { AkGlobal } from '../../util/common';
import { AkButton } from "./ak-button";
import { AkCol } from "./ak-col";
import { AkInput } from "./ak-input";
import AkKindEditor from "./ak-kindeditor";
import { AkMessage } from './ak-message';
import { AkModal } from "./ak-modal";
import { AkNotification } from "./ak-notification";
import { AkRow } from "./ak-row";
import { AkUtil } from '../../util/util';

export interface AkCommentsProps {
    EditorMode?: string,//nobar:没有工具栏(textarea) simple:简易模式。不填写:提供全部功能
    CategoryId: string,//类别ID
    ArticleId: string,//主题ID
    PageSize?: number,//分页条数
    EditorStyle?: React.CSSProperties,//富文本样式
    ClassName?: string,//评论类样式
    AppKeys: string,
    IsShowAvatar?: boolean,//是否显示头像
    IsFromNow?: boolean,//是否格式化时间
    IsDisable?: boolean,//是否禁用评论
    BeforeFn?: () => void,//提交之前
    AfterFn?: () => void,//提交之后
    IsKindEditor?: boolean,//否使用富文本评论控件
    IsDesign?: boolean;//设计布局时
    commentHolder?: CommentHolder; //redux 中的缓存对象
}
export interface AkCommentsState {
    EditCotent?: string;
    // Spinning: boolean,
    isShowEditor: boolean;
    // isPaging: Boolean
    Loading: boolean;
}

const mapStateToProps = (state, props) => {
    let rs: CommentHolder;
    if (props.AppKeys && props.ArticleId) {
        let key = CommentReducerUtil.getKey(props.AppKeys, props.CategoryId, props.ArticleId);
        rs = state.commentReducer.commentDict[key];
    }
    return {
        commentHolder: rs
    }
}

@connect(mapStateToProps)
export default class AkComments extends React.Component<AkCommentsProps, AkCommentsState> {
    fm = AkGlobal.intl.formatMessage;
    getCommentList: GetCommentRequest = {
        dataType: this.props.CategoryId,
        dataID: this.props.ArticleId,
        lastID: "0",
        pageSize: this.props.PageSize || 10,
        parentID: 0,
        appKey: this.props.AppKeys
    }

    constructor(props, context) {
        super(props, context)
        this.state = {
            Loading: false,
            isShowEditor: false,
        }
    }

    submitting = false;

    //删除评论
    onDelComment(item) {
        AkModal.confirm({
            title: this.fm({ id: CommonLocale.Delete }),
            content: this.fm({ id: CommonLocale.DeleteConfirm }),
            onOk: () => {
                let request: CommentDeleteRequest = {
                    ID: item.ID,
                    DataID: this.props.ArticleId,
                    DataType: this.props.CategoryId,
                    appKey: this.props.AppKeys
                }
                AkGlobal.store.dispatch(CommentAction.delete(request));
            }
        })
    }

    //点击更多事件
    onHandleMore() {
        if (this.props.IsDesign === true) {
            return;//设计的时候不需要请求
        }
        let request: GetCommentRequest = {
            dataType: this.props.CategoryId,
            dataID: this.props.ArticleId,
            pageSize: this.props.PageSize,
            parentID: 0,
            appKey: this.props.AppKeys
        }
        AkGlobal.store.dispatch(CommentAction.more(request));
        // this.onLoadList(false);
    }

    onUploadImgLoad(isloading) {
        this.setState({
            Loading: isloading
        })
    }

    //富文本框onchange事件
    onChangeContent(content) {
        this.setState({ EditCotent: content });
    }

    //提交评论
    onSaveComment() {
        let content = this.state.EditCotent;
        if (content) {
            content = content.trim();
        }
        if (content === null || content === undefined || content === "" || !AkUtil.htmlToText(content)) {
            AkNotification.warning({
                message: this.fm({ id: CommonLocale.Tip }),
                description: this.fm({ id: AkCommentsLocale.RegContent })
            });
            return;
        }


        if (this.submitting) {
            return;
        }
        this.submitting = true;
        this.props.BeforeFn && this.props.BeforeFn();

        const onComplete = (success) => {
            if (success) {
                this.setState({ EditCotent: "", isShowEditor: false })
                AkMessage.success(this.fm({ id: AkCommentsLocale.MsgCommentSuccess }));
                this.props.AfterFn && this.props.AfterFn();
                this.onHandleMore();
            } else {
                AkNotification.warning({
                    message: this.fm({ id: CommonLocale.Tip }),
                    description: this.fm({ id: AkCommentsLocale.RegContent })
                });
            }
            this.submitting = false;
        }

        let request: CommentAddRequest = {
            DataType: this.props.CategoryId,
            DataID: this.props.ArticleId,
            ContentText: content,
            ParentID: 0,
            TargetID: 0,
            appKey: this.props.AppKeys,
            onComplete: onComplete
        }

        AkGlobal.store.dispatch(CommentAction.add(request));

        // if (this.state.EditCotent && AkUtil.htmlToText(this.state.EditCotent)) {
        //     let PostComment: CommentModelRequest = {
        //         DataType: this.props.CategoryId,
        //         DataID: this.props.ArticleId,
        //         ContentText: this.state.EditCotent,
        //         ParentID: 0,
        //         TargetID: 0,
        //         appKey: this.props.AppKeys
        //     }
        //     CommentAPI
        //         .postComment(PostComment)
        //         .then(data => {
        //             if (data.Status == 0) {
        //                 this.setState({ EditCotent: "", isShowEditor: false })
        //                 AkMessage.success(this.formatMessage({ id: AkCommentsLocale.MsgCommentSuccess }));
        //                 this.props.AfterFn && this.props.AfterFn();
        //                 this.onHandleMore();
        //             }
        //         })
        // } else {

        // }
    }

    // //加载评论数据
    // onLoadList(isloadOne?: boolean) {
    //     let CommentList = this.state.CommentList || [];
    //     if (isloadOne) {
    //         this.getCommentList.lastID = "0";
    //         this.setState({ Spinning: true, CommentList: [] });
    //     } else {
    //         this.setState({ Spinning: true });
    //     }
    //     CommentAPI.getComment(this.getCommentList).then(data => {
    //         this.setState({ Spinning: false })
    //         if (data.Status == 0 && data.Data.length > 0) {
    //             const lastId = data.Data[data.Data.length - 1].ID;
    //             this.getCommentList.lastID = lastId;
    //             const NewCommentList = this.state.CommentList.concat(data.Data);

    //             if (data.Data.length == this.getCommentList.pageSize) {
    //                 this.setState({ isPaging: true });
    //             } else {
    //                 this.setState({ isPaging: false });
    //             }
    //             this.setState({ CommentList: NewCommentList });
    //         } else {
    //             this.setState({ isPaging: false })
    //         }
    //     })
    // }

    //渲染一条评论
    renderComment(item) {
        const userid = item.User.UserID;
        let isAdmin = false;
        let user = AkContext.getUser();
        if (user.AccountID == userid) {
            isAdmin = true;
        }
        let isShowAvatar = this.props.IsShowAvatar === undefined ? true : this.props.IsShowAvatar;
        return <div className="ak-comment-list">

            {
                isShowAvatar ? <AkRow>
                    <AkCol span={24}>
                        <span className="ak-comment-pic">
                            <img src={item.User.Photo || AkContext.getUserDefaultPhoto()} />
                        </span>
                    </AkCol>
                </AkRow> : null
            }
            <AkRow>
                <AkCol span={24} className="ak-comment-info" style={isShowAvatar ? {} : { paddingLeft: 0 }}>
                    <p className="ak-comment-info-p">
                        <span className="name">{item.User.Name}</span>
                        <span
                            className="time">{this.props.IsFromNow ? moment(item.CreatedStr).fromNow() : item.CreatedStr}</span>
                        {
                            isAdmin
                                ?
                                <span className="delete" onClick={this.onDelComment.bind(this, item)}>
                                    {this.fm({ id: CommonLocale.Delete })}
                                </span>
                                :
                                null
                        }
                    </p>
                    <p className="ak-comment-content" dangerouslySetInnerHTML={{ __html: item.ContentText }}>

                    </p>
                </AkCol>
            </AkRow>
        </div>
    }

    //渲染所有评论
    renderCommentList() {
        const ch = this.props.commentHolder;
        let list = [];
        if (ch) {
            list = ch.data;
        }

        return <div className="ak-comment-listbox">
            <AkRow>
                <AkCol span={24} className="ak-comment-title">
                    <span className="line"></span>
                    {this.fm({ id: AkCommentsLocale.Reply })}
                </AkCol>
            </AkRow>
            {
                list.map((item, index) => {
                    return <div key={index}>{this.renderComment(item)}</div>
                })
            }
        </div>
    }

    //渲染富文本框
    renderEditor() {
        let { EditorStyle, IsDesign } = this.props;
        if (!EditorStyle) {
            EditorStyle = {
                width: "100%",
                height: "220px"
            }
        }

        const isMobile = window.innerWidth < 786;
        return <AkRow className="ak-comment-editor">
            <AkCol span={24} className="ak-comment-editorbox">
                {
                    this.state.isShowEditor
                        ?
                        <div>
                            {this.props.IsKindEditor
                                ? <AkKindEditor
                                    allowAT
                                    onUploadImgLoad={this.onUploadImgLoad.bind(this)}
                                    onChange={this.onChangeContent.bind(this)}
                                    value={this.state.EditCotent}
                                    type={isMobile ? "nobar" : this.props.EditorMode ? "nobar" : "simple"}
                                    height={200} />
                                : <AkInput
                                    onChange={this.onChangeContent.bind(this)}
                                    value={this.state.EditCotent}
                                    type={isMobile ? "nobar" : this.props.EditorMode ? "nobar" : "simple"} />
                            }
                            <div className="ak-comment-btnwrap">
                                <AkButton disabled={IsDesign} type="primary" loading={this.state.Loading}
                                    onClick={this.onSaveComment.bind(this)}>
                                    {this.fm({ id: CommonLocale.Submit })}
                                </AkButton>
                                <AkButton disabled={IsDesign} onClick={() => { this.setState({ isShowEditor: false, EditCotent: "" }) }}>
                                    {this.fm({ id: CommonLocale.Cancel })}
                                </AkButton>
                            </div>
                        </div>
                        :
                        <AkInput
                            placeholder={this.fm({ id: AkCommentsLocale.Add })}
                            size="large"
                            onClick={(e) => { this.setState({ isShowEditor: true }) }} />
                }
            </AkCol>
        </AkRow>
    }

    //渲染更多按钮
    renderMore() {
        const ch = this.props.commentHolder;
        let loading = true;
        if (ch) {
            loading = ch.status === "loading";
        }

        return <div className="ak-comment-more">
            <AkButton loading={loading}
                onClick={this.onHandleMore.bind(this)}>{this.fm({ id: AkCommentsLocale.ClickMore })}</AkButton>
        </div>;
    }

    componentDidMount() {
        this.requestComments(this.props);
        // this.onLoadList(true);
    }

    requestComments(props) {
        if (this.props.IsDesign === true) {
            return;//设计的时候不需要请求
        }

        let request: CommentMoreRequest = {
            dataType: props.CategoryId,
            dataID: props.ArticleId,
            lastID: "0",
            pageSize: props.PageSize,
            parentID: 0,
            appKey: props.AppKeys
        }
        AkGlobal.store.dispatch(CommentAction.more(request));
    }

    componentWillReceiveProps(nextProps: AkCommentsProps) {
        if (("ArticleId" in nextProps && this.props.ArticleId !== nextProps.ArticleId) || ("AppKeys" in nextProps && this.props.AppKeys !== nextProps.AppKeys)) {
            this.requestComments(nextProps);
            // this.getCommentList = {
            //     dataType: nextProps.CategoryId,
            //     dataID: nextProps.ArticleId,
            //     lastID: "0",
            //     pageSize: nextProps.PageSize || 10,
            //     parentID: 0,
            //     appKey: nextProps.AppKeys
            // }
            // this.onLoadList(true);
        }
    }

    render() {
        const cls = classNames("ak-comment", this.props.ClassName);

        return <div className={cls}>
            {this.props.IsDisable ? null : this.renderEditor()}
            {this.renderCommentList()}
            {this.renderMore()}
        </div>;
    }
}
