import * as React from "react";
import { isNullOrUndefined } from "util";
import { ProcDefBrief } from "../../../api";
import { ContentListAdditionModel } from "../../../api/content-list/content-list-model";
import { AkGlobal, AkTagInput } from "../../../index";
import { CommonLocale, LookUpLocale } from "../../../locales/localeid";
import { AkCol } from "../ak-col";
import { AkResponsiveTable, ListView } from '../ak-responsive-table';
import { AkRow } from "../ak-row";
import { AkSearch } from "../ak-search";
import { AkTag } from "../ak-tag";
import AkTooltip from "../ak-tooltip";

export interface AkLookupFlowNameAdvProps {
    searchRowCount?: number;
    exclusions?: string[];//排除项
    multiple?: boolean; //is allow multiple selection
    maxSelection?: number; //how many identity selection allowed
    nameDisplayLength?: number; //超出长度的名称会被截取
    style?: React.CSSProperties; //additional style apply to control
    value?: Object; //和onChange一起结合使用，如果value赋值则defaultValue无效
    allowRowClickSelection?: boolean; //允许通过点击行选中
    open?: boolean;
    onChange?: (value: Object) => void;
    data?: ProcDefBrief[];
    listID?: string; // Linked list ID
    listAddition?: ContentListAdditionModel[]; // display options
    listFieldName?: string; // Default display on tags field
}

export interface AkLookupFlowNameAdvState {
    loading?: boolean; //是否加载中状态
    index?: number; //成本中心页码
    total?: number; //总用户数
    searchResult?: ProcDefBrief[]; //用户搜索结果
    selectedRowKeys?: string[]; // 用户选择结果key列表
    selectionDisabled?: boolean; //用户选择的checkbox是否禁用
    value?: Object; //选中的user identity;
    lablename?: any[];
}

export default class AkLookupFlowNameAdv extends React.Component<AkLookupFlowNameAdvProps, AkLookupFlowNameAdvState>{
    static defaultProps: AkLookupFlowNameAdvProps = {
        searchRowCount: 10,
        multiple: true,
        maxSelection: 99999999999,
        allowRowClickSelection: true,
    };

    searchListRequest: any;
    loaded: boolean;
    selectValue: string;//流程定义名查询
    tableColumns?: any[];
    DefKey: string;
    DefName: string;

    constructor(props: AkLookupFlowNameAdvProps, context) {
        super(props, context);
        let self = this;
        self.state = {
            total: props.data.length,
            index: 1,
            loading: false,
            searchResult: props.data,
            value: { ...props.value },
            selectedRowKeys: Object.keys(props.value),
            selectionDisabled: false,
            lablename: []
        };

        self.searchListRequest = {
            categoryID: "",
            flowName: ""
        };
        self.selectValue = "";
        self.loaded = false;
        self.DefKey = "DefKey";
        self.DefName = "DefName";
        self.tableColumns = [
            {
                title: AkGlobal.intl.formatMessage({ id: LookUpLocale.FlowName }),
                key: self.DefKey,
                dataIndex: self.DefKey,
                className: "name",
                render(text, record) {
                    return record[self.DefName];
                }
            }
        ]
    }

    // componentDidMount() {
    //     // this.onLoad(this.props);
    // }

    componentWillReceiveProps(nextProps: AkLookupFlowNameAdvProps) {
        if ("value" in nextProps && !this.ifValueAreEqual(nextProps.value, this.props.value)) {
            this.setState({
                value: { ...nextProps.value },
                selectedRowKeys: Object.keys(nextProps.value)
            });
            // setTimeout(() => this.changeDefaultValue());
        }

        // if ("open" in nextProps && this.props.open !== nextProps.open && nextProps.open) {
        //     if (nextProps.open) {
        //         if (this.loaded) {
        //             this.setState({ index: 1 });
        //             this.searchList(true);
        //         } else {
        //             this.onLoad(nextProps);
        //         }
        //     }
        // }
    }

    // onLoad(props: AkLookupFlowNameAdvProps) {
    //     this.loaded = true;
    //     this.searchList();
    // }

    ifValueAreEqual(v1, v2) {
        return v1 === v2 || JSON.stringify(v1) === JSON.stringify(v2);
    }

    // async searchList(reset?: boolean) {
    //     const { data } = this.state;
    //     const { exclusions } = this.props;
    //     let result = data.filter(f => exclusions.findIndex(e => e === f.DefKey) === -1);
    //     this.setState({
    //         loading: false,
    //         searchResult: result,
    //         total: result.length || 0
    //     });

    //     this.setState({ loading: true });
    //     const { index, searchResult, total } = this.state;
    //     if (searchResult.length > 1 && this.selectValue.trim().length > 0) {
    //         this.setState({
    //             loading: false,
    //             searchResult: searchResult,
    //             total: total
    //         });
    //         return false;
    //     }

    //     ProcDefsAPI.getFlowList({}).then(data => {
    //         const { exclusions } = this.props;
    //         let result = data.Data.filter(f => exclusions.findIndex(e => e === f.DefKey) === -1);
    //         this.setState({
    //             loading: false,
    //             searchResult: result,
    //             total: result.length || 0
    //         });
    //     });
    // }

    onSearch(value) {
        let self = this;
        const { data } = this.props;

        let len = data.length;
        if (!isNullOrUndefined(value) && (value + "").trim().length > 0 && len > 0) {
            //正则表达式 模糊查询
            var arr = [];
            var reg = new RegExp(value);
            for (var i = 0; i < len; i++) {
                //如果字符串中不包含目标字符会返回-1
                if (data[i][self.DefName].match(reg)) {
                    arr.push(data[i]);
                }
            }
            self.setState({ loading: false, index: 1, searchResult: arr, total: arr.length });
        } else {
            self.setState({ loading: false, index: 1, searchResult: data, total: data.length });
        }

        // self.selectValue = value;

        // if (!isNullOrUndefined(value) && (value + "").trim().length > 0 && len > 0) {
        //     //正则表达式 模糊查询
        //     var arr = [];
        //     var reg = new RegExp(value);
        //     for (var i = 0; i < len; i++) {
        //         //如果字符串中不包含目标字符会返回-1
        //         if (searchResult[i][self.DefName].match(reg)) {
        //             arr.push(searchResult[i]);
        //         }
        //     }
        //     self.setState({ loading: false, index: 1, searchResult: arr, total: arr.length });
        // } else {
        //     self.setState({ loading: true, index: 1 });
        //     self.searchList(true);
        // }
    }

    changeValue(value: Object, selectedRowKeys: string[], selectionDisabled: boolean) {
        const { props: { onChange } } = this;;
        this.setState({ selectionDisabled });

        if (!selectedRowKeys) selectedRowKeys = Object.keys(value);

        if (!("value" in this.props)) this.setState({ selectedRowKeys, value });

        if (onChange) onChange({ ...value });
    }

    tableRowOnClick(item) {
        const { multiple, maxSelection } = this.props;
        let { value, selectedRowKeys } = this.state;
        const listDataID = item[this.DefKey];
        let selectionDisabled = false;

        if (value.hasOwnProperty(listDataID)) {
            delete value[listDataID];
            selectedRowKeys = Object.keys(value);
            selectionDisabled = false;
        } else {
            if (multiple) {
                let count = Object.keys(value).length;
                if (maxSelection === 0 || count < maxSelection) {
                    value[listDataID] = item;
                    selectedRowKeys.push(listDataID);
                    count++;
                }
                selectionDisabled = maxSelection > 0 && count === maxSelection;

            } else {
                value = {};
                value[listDataID] = item;
                selectedRowKeys = Object.keys(value);
                selectionDisabled = true;
            }
        }

        this.changeValue(value, selectedRowKeys, selectionDisabled);
    }

    tableSelectChange(selectedRowKeys: string[]) {
        const { state: { value, searchResult }, props: { maxSelection } } = this;

        Object.keys(value).forEach(key => {
            if (selectedRowKeys.indexOf(key) === -1) delete value[key];
        });

        let selectionCount = Object.keys(value).length;
        const rowKeys = selectedRowKeys.slice(0);

        searchResult.forEach(item => {
            const listDataID = item[this.DefKey];
            const index = rowKeys.indexOf(listDataID);
            if (index !== -1) {
                if (!value.hasOwnProperty(listDataID)) {
                    if (maxSelection === 0 || selectionCount < maxSelection) {
                        value[listDataID] = item;
                        selectionCount++;
                    } else {
                        rowKeys.splice(index, 1);
                    }
                }
            }
        });

        const selectionDisabled = maxSelection > 0 && selectionCount === maxSelection;
        this.changeValue(value, rowKeys, selectionDisabled);
    }

    tagClosed(item) {
        const { value, selectedRowKeys } = this.state;
        const listDataID = item[this.DefKey];

        delete value[listDataID];
        const index = selectedRowKeys.indexOf(listDataID);
        if (index > -1) {
            selectedRowKeys.splice(index, 1);
        }
        this.changeValue(value, selectedRowKeys, false);
    }

    renderTable() {
        const { multiple, allowRowClickSelection, searchRowCount } = this.props;
        const { searchResult, loading, total, selectedRowKeys, selectionDisabled, index } = this.state;

        return <AkResponsiveTable
            rowSelection={{
                type: multiple ? "checkbox" : "radio",
                selectedRowKeys: selectedRowKeys,
                onChange: (selectedRowKeys) => this.tableSelectChange((selectedRowKeys as string[])),
                getCheckboxProps: record => ({
                    disabled: multiple ? selectedRowKeys.indexOf(record[this.DefKey]) === -1 && selectionDisabled : false
                })
            }}
            onRowClick={allowRowClickSelection ? this.tableRowOnClick.bind(this) : undefined}
            size="small"
            rowKey={this.DefKey}
            columns={this.tableColumns}
            loading={loading}
            pagination={{
                total: total,
                current: index,
                pageSize: searchRowCount,
                onChange: (page: number) => {
                    this.setState({ index: page });
                    // this.searchList();
                }
            }}

            dataSource={searchResult} hideViewSwitch defaultView={ListView.table} />;
    }

    renderTagDisplay() {
        let self = this;
        const { state: { value }, props: { listFieldName, listID } } = self;
        const keys = Object.keys(value);
        let form;
        if (keys.length > 0) {
            const listID = value[keys[0]].ListID;
            const fields = null;
            if (fields) {
                form = AkTagInput.AssignDefToForm(fields.find(field => field.FieldName === listFieldName));
            }
        }

        return Object.keys(value)
            .map(v => {
                let obj = value[v];
                if (form) form.value = obj[listFieldName];
                return <AkTooltip title={obj[listFieldName]} key={obj[self.DefKey]} >
                    <AkTag className="ak-tag-input-close" closable afterClose={() => self.tagClosed(obj)}>
                        <span>{obj[self.DefName]}</span>
                    </AkTag>
                </AkTooltip>;
            })
    }

    render() {
        return <div className="ak-identity-picker-wrapper ak-lookup-picker-warpper">
            <AkRow gutter={16}>
                <AkCol span={24}>
                    <AkSearch placeholder={AkGlobal.intl.formatMessage({ id: CommonLocale.KeywordPlaceHolder })} onSearch={v => this.onSearch(v)} />
                </AkCol>
            </AkRow>
            <AkRow style={{ marginTop: 5 }}>
                {this.renderTable()}
            </AkRow>
            <div className="tag-container">
                {this.renderTagDisplay()}
            </div>
        </div>;
    }
}