import * as React from "react";
import { connect } from "react-redux";
import { AkGlobal, AkDictionaryString, IdentityAPI, AkIdentityType, MetadataAPI, AkImg, AkTagInput } from "../../../index";
import { AkTable } from "../ak-table";
import { AkTag } from "../ak-tag";
import { AkRow } from "../ak-row";
import { AkCol } from "../ak-col";
import { AkSearch } from "../ak-search";
import AkTooltip from "../ak-tooltip";
import { AkLookupRenderControl } from "./AkLookupRenderControl";
import { ContentListAction } from "../../../actions";
import { CommonLocale } from "../../../locales/localeid";
import { ContentListApi } from "../../../api/content-list/content-list-api";
import { AkResponsiveTable, ListView } from '../ak-responsive-table';
import {
    ContentListDatasByQueryRequest,
    ContentListField,
    ContentListWhereModel,
    ContentListAdditionModel,
    ContentListWhereType
} from "../../../api/content-list/content-list-model";
import { AkMetadataSelect } from "../metadata";
import { CONTROL_TEXTAREA, PROP_TAG } from "./AkLookupUtil";

export interface AkLookupAdvProps {
    searchRowCount?: number;
    multiple?: boolean; //is allow multiple selection
    maxSelection?: number; //how many identity selection allowed
    nameDisplayLength?: number; //超出长度的名称会被截取
    style?: React.CSSProperties; //additional style apply to control
    value?: Object; //和onChange一起结合使用，如果value赋值则defaultValue无效
    allowRowClickSelection?: boolean; //允许通过点击行选中
    open?: boolean;
    onChange?: (value: Object) => void;
    appID?: number;
    listID?: string; // Linked list ID
    listAddition?: ContentListAdditionModel[]; // display options
    listFieldName?: string; // Default display on tags field
    filters?: () => Promise<ContentListWhereModel[]>; // Custom where functions
    listFieldDict?: AkDictionaryString<ContentListField[]>;
}

export interface AkLookupAdvState {
    loading?: boolean; //是否加载中状态
    index?: number; //成本中心页码
    total?: number; //总用户数
    searchResult?: any[]; //用户搜索结果
    selectedRowKeys?: string[]; // 用户选择结果key列表
    selectionDisabled?: boolean; //用户选择的checkbox是否禁用
    value?: Object; //选中的user identity;
    tableColumns?: any[];
    lablename?: any[];
    metadataValue?: string;
    onSearchValue?: string;
}

const mapStateToProps = (state) => {
    return {
        listFieldDict: state.contentList.listFieldDict
    };
};

@connect(mapStateToProps)
export default class AkLookupAdv extends React.Component<AkLookupAdvProps, AkLookupAdvState>{
    static defaultProps: AkLookupAdvProps = {
        searchRowCount: 10,
        multiple: false,
        maxSelection: 200,
        allowRowClickSelection: true,
    };

    searchListRequest: ContentListDatasByQueryRequest;
    loaded: boolean;
    temp: any;

    constructor(props: AkLookupAdvProps, context) {
        super(props, context);

        this.state = {
            total: 0,
            index: 1,
            loading: false,
            searchResult: [],
            value: { ...props.value },
            selectedRowKeys: Object.keys(props.value),
            selectionDisabled: false,
            tableColumns: [],
            lablename: []
        };

        this.searchListRequest = {
            FilterValue: "",
            IsExport: false,
            PageIndex: 1,
            PageSize: props.searchRowCount,
            AppID: props.appID,
            ListID: props.listID,
            Columns: []
        };

        this.loaded = false;
    }

    componentDidMount() {
        AkGlobal.store.dispatch(ContentListAction.requestField(this.props.appID, this.props.listID));
        this.onLoad(this.props);
    }

    componentWillReceiveProps(nextProps: AkLookupAdvProps) {
        if ("value" in nextProps && !this.ifValueAreEqual(nextProps.value, this.props.value)) {
            this.setState({
                value: { ...nextProps.value },
                selectedRowKeys: Object.keys(nextProps.value)
            });
            // setTimeout(() => this.changeDefaultValue());
        }

        if ("listFieldDict" in nextProps && this.props.listFieldDict !== nextProps.listFieldDict) {
            if (this.loaded) return;
            this.onLoad(nextProps);
        }

        if ("open" in nextProps && this.props.open !== nextProps.open && nextProps.open) {
            if (nextProps.open) {
                if (this.loaded) {
                    this.setState({
                        index: 1,
                    });
                    this.searchList(true);
                } else {
                    this.onLoad(nextProps);
                }
            }
        }
    }

    // changeDefaultValue() {
    //     const { state: { lablename, value }, props: { listFieldDict, listID, listFieldName, nameDisplayLength } } = this;
    //     const type = listFieldDict[listID] && listFieldDict[listID].filter(i => i.FieldName === listFieldName)[0].Type;

    //     switch (type) {
    //         case CONTROL_IDENTITY_PICKER: this.GetIdentityName(AkIdentityType.User)
    //             break;
    //         case CONTROL_ORGANIZATION_PICKER: this.GetIdentityName(AkIdentityType.Organization)
    //             break;
    //         case CONTROL_LOCATION_PICKER: this.GetIdentityName(AkIdentityType.Location)
    //             break;
    //         case CONTROL_COST_CENTER_PICKER: this.GetIdentityName(AkIdentityType.CostCenter)
    //             break;
    //         case CONTROL_METADATA: this.GetMetadataName()
    //             break;
    //         case CONTROL_ICON_UPLOAD: Object.keys(value).map(v => {
    //             let obj = value[v];
    //             if (obj[listFieldName]) {
    //                 this.state.lablename[v] = <AkImg magnifier={false} style={{ verticalAlign: "middle" }} width={20} height={20} src={obj[listFieldName]} />;
    //                 this.setState({ lablename })
    //             }
    //         })
    //             break;
    //         default: Object.keys(value).map(v => {
    //             let obj = value[v];
    //             if (obj[listFieldName]) {
    //                 const isLong = obj[listFieldName].length > nameDisplayLength;
    //                 this.state.lablename[v] = isLong ? (obj[listFieldName].slice(0, nameDisplayLength) + "...") : obj[listFieldName];
    //                 this.setState({ lablename })
    //             }
    //         })
    //     }
    // }

    // GetMetadataName() {
    //     const { state: { value, lablename }, props: { listFieldName } } = this;
    //     Object.keys(value).map(v => {
    //         let obj = value[v];
    //         const name = obj[listFieldName];
    //         if (name) {
    //             MetadataAPI.getByID({ id: obj[listFieldName] }).then(data => {
    //                 if (data.Status === 0) {
    //                     this.state.lablename[v] = data.Data.Name;
    //                     this.setState({ lablename });
    //                 }
    //             })
    //         } else {
    //             this.state.lablename[v] = "";
    //             this.setState({ lablename });
    //         }
    //     })
    // }

    // GetIdentityName(type) {
    //     const { state: { value, lablename }, props: { listFieldName } } = this;
    //     Object.keys(value)
    //         .map(v => {
    //             let obj = value[v];
    //             const name = obj[listFieldName];
    //             if (name) {
    //                 let params = [{
    //                     ID: obj[listFieldName],
    //                     Type: type
    //                 }];
    //                 if (Array.isArray(JSON.parse(name))) {
    //                     params = [];
    //                     JSON.parse(name).forEach(item => {
    //                         params.push({
    //                             ID: item,
    //                             Type: type
    //                         })
    //                     })
    //                 }
    //                 IdentityAPI.resolveIdentities({ identities: params }).then(data => {
    //                     if (data.Status === 0) {
    //                         const name = data.Data.map(item => { return item.Name })
    //                         this.state.lablename[v] = name.join(",");
    //                         this.setState({ lablename });
    //                     }
    //                 })
    //             } else {
    //                 this.state.lablename[v] = "";
    //                 this.setState({ lablename });
    //             }
    //         });

    // }

    onLoad(props: AkLookupAdvProps) {
        const { listID, listFieldDict, listAddition } = props;
        if (listFieldDict[listID]) {
            this.setColumns(listAddition, listFieldDict[listID]);
            this.loaded = true;
            this.searchList();
        }
    }

    setColumns(listAddition: ContentListAdditionModel[], listFields: ContentListField[]) {
        function addColumn(fieldName: string) {
            const field = listFields.find(i => i.FieldName === fieldName);
            if (field) {
                columns.push({
                    title: field.DisplayName,
                    key: fieldName,
                    dataIndex: fieldName,
                    className: "name",
                    render(text, record) {
                        let form: any = {
                            type: field.Type,
                            attrs: field.Rules ? JSON.parse(field.Rules) : {},
                            value: record[fieldName],
                        };
                        if (field.Type === PROP_TAG) {
                            //修复查阅项table内tag类型字段不显示
                            form = { ...form, listDataID: record["ListDataID"] };
                        }
                        return AkLookupRenderControl.renderList(form);
                    }
                });
            }
        }

        const columns = [];
        const searchColumns = [];

        if (listAddition) {
            listAddition.forEach(c => {
                if (c.IsShow) addColumn(c.FieldName);
                if (c.IsShow || c.RelationName) searchColumns.push(c.FieldName);
            });
        }

        if (columns.length === 0) addColumn(this.props.listFieldName);
        if (searchColumns.indexOf(this.props.listFieldName) === -1) searchColumns.push(this.props.listFieldName);
        this.searchListRequest.Columns = searchColumns.length > 0 ? searchColumns : [this.props.listFieldName];
        this.setState({ tableColumns: columns });
    }

    ifValueAreEqual(v1, v2) {
        return v1 === v2 || JSON.stringify(v1) === JSON.stringify(v2);
    }

    async searchList(reset?: boolean) {
        this.setState({ loading: true });
        if (reset) this.searchListRequest.PageIndex = 1;
        this.searchListRequest.Wheres = this.props.filters ? await this.props.filters() : [];
        this.searchListRequest.Verification = false;

        ContentListApi.SearchEngines(this.searchListRequest).then(data => {
            this.setState({
                loading: false,
                searchResult: data.Data,
                total: data.TotalCount
            });
        });
    }

    onSearch() {
        this.setState({ loading: true, index: 1 });
        this.searchListRequest.PageIndex = 1;
        this.searchListRequest.FilterValue = this.state.onSearchValue;
        this.searchList(true);
    }

    changeValue(value: Object, selectedRowKeys: string[], selectionDisabled: boolean) {
        const { props: { onChange } } = this;;
        this.setState({ selectionDisabled });

        if (!selectedRowKeys) selectedRowKeys = Object.keys(value);

        if (!("value" in this.props)) this.setState({ selectedRowKeys, value });

        if (onChange) onChange({ ...value });
    }

    tableRowOnClick(item) {
        const { multiple, maxSelection } = this.props;
        let { value, selectedRowKeys } = this.state;
        const listDataID = item["ListDataID"];
        let selectionDisabled = false;

        if (value.hasOwnProperty(listDataID)) {
            delete value[listDataID];
            selectedRowKeys = Object.keys(value);
            selectionDisabled = false;
        } else {
            if (multiple) {
                let count = Object.keys(value).length;
                if (maxSelection === 0 || count < maxSelection) {
                    value[listDataID] = item;
                    selectedRowKeys.push(listDataID);
                    count++;
                }
                selectionDisabled = maxSelection > 0 && count === maxSelection;

            } else {
                value = {};
                value[listDataID] = item;
                selectedRowKeys = Object.keys(value);
                selectionDisabled = true;
            }
        }

        this.changeValue(value, selectedRowKeys, selectionDisabled);
    }

    tableSelectChange(selectedRowKeys: string[]) {
        const { state: { value, searchResult }, props: { maxSelection } } = this;

        Object.keys(value).forEach(key => {
            if (selectedRowKeys.indexOf(key) === -1) delete value[key];
        });

        let selectionCount = Object.keys(value).length;
        const rowKeys = selectedRowKeys.slice(0);

        searchResult.forEach(item => {
            const listDataID = item["ListDataID"];
            const index = rowKeys.indexOf(listDataID);
            if (index !== -1) {
                if (!value.hasOwnProperty(listDataID)) {
                    if (maxSelection === 0 || selectionCount < maxSelection) {
                        value[listDataID] = item;
                        selectionCount++;
                    } else {
                        rowKeys.splice(index, 1);
                    }
                }
            }
        });

        const selectionDisabled = maxSelection > 0 && selectionCount === maxSelection;
        this.changeValue(value, rowKeys, selectionDisabled);
    }

    tagClosed(item) {
        const { value, selectedRowKeys } = this.state;
        const listDataID = item["ListDataID"];

        delete value[listDataID];
        const index = selectedRowKeys.indexOf(listDataID);
        if (index > -1) {
            selectedRowKeys.splice(index, 1);
        }
        this.changeValue(value, selectedRowKeys, false);
    }

    renderTable() {
        const { multiple, allowRowClickSelection, searchRowCount } = this.props;
        const { searchResult, loading, total, selectedRowKeys, selectionDisabled, tableColumns, index } = this.state;

        return <AkResponsiveTable
            rowSelection={{
                type: multiple ? "checkbox" : "radio",
                selectedRowKeys: selectedRowKeys,
                onChange: (selectedRowKeys) => this.tableSelectChange((selectedRowKeys as string[])),
                getCheckboxProps: record => ({
                    disabled: multiple ? selectedRowKeys.indexOf(record["ListDataID"]) === -1 && selectionDisabled : false
                })
            }}
            onRowClick={allowRowClickSelection ? this.tableRowOnClick.bind(this) : undefined}
            size="small"
            rowKey="ListDataID"
            columns={tableColumns}
            loading={loading}
            pagination={{
                total: total,
                current: index,
                pageSize: searchRowCount,
                onChange: (page: number) => {
                    this.setState({ loading: true, index: page });
                    this.searchListRequest.PageIndex = page;
                    this.searchList();
                }
            }}

            dataSource={searchResult} hideViewSwitch defaultView={ListView.table} />;
    }

    renderTagDisplay() {
        const { state: { value }, props: { listFieldName, listFieldDict, listID } } = this;
        const keys = Object.keys(value);
        let form;
        if (keys.length > 0 && listFieldDict) {
            const listID = value[keys[0]].ListID;
            const fields = listFieldDict[listID];
            if (fields) {
                form = AkTagInput.AssignDefToForm(fields.find(field => field.FieldName === listFieldName));
            }
        }

        return Object.keys(value)
            .map(v => {
                let obj = value[v];
                if (form) form.value = obj[listFieldName];
                return <AkTooltip title={obj[listFieldName]} key={obj["ListDataID"]} >
                    <AkTag className="ak-tag-input-close" closable afterClose={() => this.tagClosed(obj)}>
                        <span>{AkLookupRenderControl.renderList(form, "lookup")}</span>
                    </AkTag>
                </AkTooltip>;
            })
    }

    render() {
        return <div className="ak-identity-picker-wrapper ak-lookup-picker-warpper">
            <AkRow gutter={16}>
                <AkCol span={24}>
                    <AkSearch placeholder={AkGlobal.intl.formatMessage({ id: CommonLocale.KeywordPlaceHolder })} onSearch={v =>
                        this.setState({ onSearchValue: v }, () => {
                            this.onSearch()
                        })} />
                </AkCol>
            </AkRow>
            <AkRow style={{ marginTop: 5 }}>
                {this.renderTable()}
            </AkRow>
            <div className="tag-container">
                {this.renderTagDisplay()}
            </div>
        </div>;
    }
}