import * as React from "react";
import * as moment from "moment";
import { AkGlobal, AkDatetimeUtil } from "../../../util";
import { AkMetadataLabel } from "../metadata";
import { CommonLocale } from "../../../locales";
import { AkFormIdentityPicker, AkFormCostCenterPicker } from "../../identity";
import { AkIdentityType } from "../../../api";
import { AkFileUpload } from "../ak-file-upload";
import AkImg from "../ak-img";
import AkLookup from "./AkLookup";
import { AkUtil } from "../../../util";
import { AkDateLabel } from '../AkDateLabel';
import { AkIdentityPickerLabel } from '../../identity/AkIdentityPickerLabel';
import {
    CONTROL_INPUT,
    CONTROL_TEXTAREA,
    CONTROL_INPUT_NUMBER,
    CONTROL_NUMBER_INT,
    CONTROL_CURRENCY,
    CONTROL_SELECT,
    CONTROL_METADATA,
    CONTROL_SWITCH,
    CONTROL_RADIO,
    CONTROL_CHECKBOX,
    CONTROL_DATEPICKER,
    CONTROL_DATE_RANGE,
    CONTROL_TIME,
    CONTROL_LIST,
    CONTROL_ATTACHMENT_UPLOAD,
    CONTROL_IDENTITY_PICKER,
    CONTROL_ORGANIZATION_PICKER,
    CONTROL_LOCATION_PICKER,
    CONTROL_COST_CENTER_PICKER,
    CONTROL_FILE_UPLOAD,
    CONTROL_ICON_UPLOAD,
    CONTROL_HYPERLINK,
    CONTROL_LOOKUP,
    CONTROL_CALCULATED,
    CONTROL_TITLE,
    CONTROL_FLOWSTATUS,
    CONTROL_RICHTEXT,
    CONTROL_CALCULATEDCOLUMN,
    PROP_CALCULATED_RESULT,
    PROP_TAG,
    CONTROL_FILE_UPLOAD_MERGE,
    CONTROL_METADATA_MUTIPLE
} from "./AkLookupUtil";
import { FormattedMessage } from "react-intl";
import { ApplicationStatusLocale, AkTag, ApplicationStatusEnum } from "../../..";
import { AkTooltip } from "../ak-tooltip";
import { AkTagPicker } from "..";

export class AkLookupRenderControl extends React.Component<null, null> {
    constructor(props, context) {
        super(props, context)
    }

    static onFileClick = (e) => {
        e.stopPropagation();
        e.preventDefault();
    }

    static renderList(control, type?: string, key?: string) {
        const { formatMessage } = AkGlobal.intl;
        if (!control) return;
        let value = AkUtil.JSONParse(control.value);

        if (!value && control.type !== CONTROL_SWITCH && control.type !== CONTROL_CURRENCY) return value;

        const multiple = control.attrs.multiple;
        switch (control.type) {
            case CONTROL_TEXTAREA:
                return <div className={type === "news" ? "ak-lookup-display-ellipsis-4" : "ak-lookup-display-ellipsis-3"}>{value}</div>;
            // <AkTooltip title={value}>
            //     <div className={type === "news" ? "ak-lookup-display-ellipsis-4" : "ak-lookup-display-ellipsis"}>{value}</div>
            // </AkTooltip>
            case CONTROL_INPUT_NUMBER:
                let float = control.attrs.number_float;
                let displayThousandths = control.attrs.displayThousandths;
                if (float) {
                    return displayThousandths ? (Number(value).toFixed(float) + "").replace(/\B(?=(\d{3})+(?!\d))/g, ',') : Number(value).toFixed(float);
                }
                return displayThousandths ? (value + "").replace(/\B(?=(\d{3})+(?!\d))/g, ',') : value;
            case CONTROL_RICHTEXT:
                return <div className="ak-lookup-richtext" key={AkUtil.guid()} dangerouslySetInnerHTML={{ __html: control.value }}></div>
            case CONTROL_CURRENCY:
                const SYMBOLS = ['¥', '$', '€'];
                let symbol = control.attrs.currency_symbol;
                symbol = symbol ? SYMBOLS[symbol] : SYMBOLS[0];
                if (type === "lookup") symbol = "";
                value = value ? value : 0;
                return `${symbol} ${Number(value).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ',')}`;
            case CONTROL_METADATA:
            case CONTROL_METADATA_MUTIPLE:
                var id = control.attrs.categoryId;
                return <AkMetadataLabel categoryID={id} optionID={value} />;
            case CONTROL_SWITCH:
                return control.attrs.displayStyle === "decision" ? formatMessage({ id: AkUtil.toBoolean(value) ? CommonLocale.True : CommonLocale.False }) :
                    formatMessage({ id: AkUtil.toBoolean(value) ? CommonLocale.On : CommonLocale.Off });
            // return <AkSwitch
            //     checkedChildren={formatMessage({ id: CommonLocale.On })}
            //     unCheckedChildren={formatMessage({ id: CommonLocale.Off })}
            //     value={value}
            //     disabled={true} />
            case CONTROL_CHECKBOX:
                const options = control.attrs["choices"] || [];
                return value.filter(i => options.indexOf(i) !== -1).join(" | ");
            case CONTROL_DATEPICKER:
                const showTime = control.attrs.showtime ? control.attrs.showtime : false;
                let dateFormat = AkDatetimeUtil.getBaseInfo().DateFormat;
                let date = value;
                if (showTime) {
                    switch (control.attrs.dateformat) {
                        case "1": dateFormat += " HH:mm"; break;
                        case "2": dateFormat += " HH"; break;
                        case "0":
                        default:
                            dateFormat += " HH:mm:ss";
                            let arr = date ? date.split(":") : null;
                            if (arr && arr.length > 1 && arr[arr.length - 1].length < 2) {
                                arr[arr.length - 1] = "0" + arr[arr.length - 1];
                                date = arr.join(":");
                            }
                            break;
                    }

                }
                return <AkDateLabel value={date} format={dateFormat} />;
            case CONTROL_IDENTITY_PICKER:
                return type === "lookup" ? <AkIdentityPickerLabel key={key || control.id} value={value} identityType={AkIdentityType.User} /> :
                    <AkFormIdentityPicker multiple={multiple} value={value} readonly />;
            case CONTROL_ORGANIZATION_PICKER:
                return type === "lookup" ? <AkIdentityPickerLabel value={value} identityType={AkIdentityType.Organization} /> :
                    <AkFormIdentityPicker identityTypes={[AkIdentityType.Organization]} value={value} readonly={true} />;
            case CONTROL_LOCATION_PICKER:
                return type === "lookup" ? <AkIdentityPickerLabel value={value} identityType={AkIdentityType.Location} /> :
                    <AkFormIdentityPicker identityTypes={[AkIdentityType.Location]} value={value} readonly={true} />;
            case CONTROL_COST_CENTER_PICKER:
                return type === "lookup" ? <AkIdentityPickerLabel value={value} identityType={AkIdentityType.CostCenter} />
                    : <AkFormCostCenterPicker multiple={multiple} value={value} readonly />;
            case CONTROL_FILE_UPLOAD:
            case CONTROL_FILE_UPLOAD_MERGE:
                return <span onClick={(e) => this.onFileClick(e)}>
                    <AkFileUpload listType="text" disabled={true} value={value} />
                </span>;
            case CONTROL_ICON_UPLOAD:
                let imgSrc = Array.isArray(AkUtil.JSONParse(value)) ? AkUtil.JSONParse(value) : (value ? [value] : []);
                let url = imgSrc.length > 0 ? imgSrc[imgSrc.length - 1] : ''
                return <AkImg src={url} width={50} height={50} lazyLoad={false} magnifier={type !== "lookup"} ></AkImg>;
            case CONTROL_HYPERLINK:

                return <a onClick={(e) => e.stopPropagation()} href={value} target={control.attrs.hyperlink_open ? control.attrs.hyperlink_open.toLowerCase() : null}>
                    {control.attrs.hyperlink_buttonname || value}
                </a>;
            case CONTROL_LOOKUP:
                const appID = control.attrs.appid;
                const listID = control.attrs.listid;
                const listFieldName = control.attrs.listfield;
                const tooltipFieldName = control.attrs.list_tooltip_field;
                return <AkLookup appID={appID} tooltipFieldName={tooltipFieldName} listID={listID} listFieldName={listFieldName} multiple={multiple} value={value} readonly />
            case CONTROL_TIME:
                if (!value) return value;
                let format = "HH:mm:ss";
                switch (control.attrs.dateformat) {
                    case "1": format = "HH:mm"; break;
                    case "2": format = "HH"; break;
                    case "0":
                    default: format = "HH:mm:ss"; break;
                }
                return moment(value, value.length > 8 ? 'YYYY-MM-DD HH:mm:ss' : 'HH:mm:ss').format(format);
            case CONTROL_FLOWSTATUS:
                if (typeof value === "string") {
                    value = JSON.parse(value);
                }
                if (value instanceof Object && value.hasOwnProperty("Status")) {
                    const status = value.Status;
                    const style = status !== ApplicationStatusEnum.Rejected
                        && status !== ApplicationStatusEnum.Error
                        ? null : {
                            backgroundColor: "#FCE3D6",
                            color: "red",
                            border: "1px #FCE3D6 solid"
                        };
                    return <AkTag style={style}>
                        <FormattedMessage id={ApplicationStatusLocale + status}></FormattedMessage>
                    </AkTag>
                }
                return value;
            case CONTROL_RICHTEXT:
                return <div className="ak-lookup-richtext" key={AkUtil.guid()} dangerouslySetInnerHTML={{ __html: control.value }}></div>
            case CONTROL_CALCULATEDCOLUMN:
                let result = control.attrs[PROP_CALCULATED_RESULT];
                if (result && result.type === "boolean") {
                    return AkGlobal.intl.formatMessage({ id: AkUtil.isTrue(value) ? CommonLocale.True : CommonLocale.False });
                } else if (result && result.type === "datetime") {
                    const showTime = result.attrs ? result.attrs["showTime"] : false;
                    const format = showTime ? AkDatetimeUtil.getBaseInfo().DateFormat + " HH:mm:ss" : AkDatetimeUtil.getBaseInfo().DateFormat;
                    return <AkDateLabel value={value} format={format} />
                } else {
                    return (typeof value === "object") ? JSON.stringify(control.value) : value;
                }
            case PROP_TAG:
                return <AkTagPicker itemId={control.listDataID} disable readonly />;
            default:
                return (typeof value === "object") ? JSON.stringify(control.value) : value;
        }
    }
}