import * as React from "react";
import * as classNames from "classnames";
import { AkUtil, ContentListField, AkDictionaryString, AkGlobal, ContentListAction, AkLookupRenderControl, AkTooltip, AkTag } from "../../../index";
import { DataHolder, DataHolderDict, AkTagInput } from "../ak-tag-input";
import { ContentListApi } from "../../../api/content-list/content-list-api";
import { connect } from 'react-redux';
import { SSL_OP_NETSCAPE_CHALLENGE_BUG } from 'constants';
import {
    ContentListDatasByIdsRequest,
    ContentListWhereModel,
    ContentListAdditionModel
} from "../../../api/content-list/content-list-model";


export interface AkLookupLabelProps {
    searchRowCount?: number;
    multiple?: boolean; //is allow multiple selection
    maxSelection?: number; //how many identity selection allowed
    maxDisplay?: number; //how many identity will be displayed on control
    displayAll?: boolean; //是否显示全部选项
    nameDisplayLength?: number; //超出长度的名称会被截取
    style?: React.CSSProperties; //additional style apply to control
    className?: string;
    placeholder?: string; //输入默认的提示语
    defaultValue?: string | string[]; //default selection
    readonly?: boolean;
    autoCollapse?: boolean; //展开隐藏内容后，鼠标离开是否自动收起
    value?: string | string[]; //和onChange一起结合使用，如果value赋值则defaultValue无效
    hideInputOnMaxSelectReached?: boolean;
    onChange?: (id: string | string[], obj?: any | any[]) => any; //onchange事件，将选中dict输出
    appID?: number;
    listID?: string; // Linked list ID
    listAddition?: ContentListAdditionModel[]; // display options
    tooltipFieldName?:string;
    listFieldName?: string; // Default display on tags field
    filters?: () => Promise<ContentListWhereModel[]>; // Custom where functions
    listFieldDict?: AkDictionaryString<ContentListField[]>;
}

export interface AkLookupLabelState {
    value?: Object;
    maxSelection?: number; //最大选择项
    initialized?: boolean; //是否初始化完成
    displayAll?: boolean;
    searchResult?: any[];
    advanceVisible?: boolean; //高级选择器显示
    advanceTempValue?: Object; //advance picker的临时值
    inputValue?: string;
    hideInput?: boolean;
    width?: number;
}

@connect(state => ({ listFieldDict: state.contentList.listFieldDict }))
export default class AkLookupLabel extends React.Component<AkLookupLabelProps, AkLookupLabelState>{
    static defaultProps: AkLookupLabelProps = {
        searchRowCount: 10,
        multiple: false,
        maxSelection: 200,
        maxDisplay: 5,
        nameDisplayLength: 20,
        readonly: false,
        displayAll: false,
        autoCollapse: false,
        hideInputOnMaxSelectReached: true
    };

    constructor(props, context) {
        super(props, context);
        const columns = props.listAddition ? props.listAddition.filter(i => i.IsShow).length : 0;
        this.state = {
            maxSelection: props.multiple ? props.maxSelection : 1,
            value: {},
            initialized: false,
            searchResult: [],
            advanceVisible: false,
            advanceTempValue: {},
            width: columns >= 4 ? 150 * columns : undefined
        };
    }
    componentDidMount() {
        let value = ("value" in this.props) ? this.props.value : this.props.defaultValue;
        this.parseUnresolvedValue(value).then(d => {
            this.setState({ value: AkUtil.arrayToObject(d, "ListDataID"), initialized: true });
        });
        AkGlobal.store.dispatch(ContentListAction.requestField(this.props.appID, this.props.listID));
    }
    componentWillReceiveProps(nextProps: AkLookupLabelProps) {
        if ("value" in nextProps && nextProps.value !== this.props.value) {
            this.parseUnresolvedValue(nextProps.value).then(d => {
                this.setState({ value: AkUtil.arrayToObject(d, 'ListDataID'), initialized: true });
            });
        }
        if ("multiple" in nextProps && nextProps.multiple !== this.props.multiple) {
            this.setState({ maxSelection: nextProps.multiple ? nextProps.maxSelection || this.props.maxSelection : 1 })
        }
        if ("maxSelection" in nextProps && nextProps.maxSelection !== this.props.maxSelection) {
            this.setState({ maxSelection: nextProps.multiple ? nextProps.maxSelection : 1 })
        }
    }

    async parseUnresolvedValue(val) {
        let result = [];
        let valArr = [];
        if (val) {
            if (val instanceof Array) {
                valArr = val;
            } else {
                valArr.push(val);
            }
            //获取所有需要解析的id数组，一次获取解析
            const listDataIDs = [];
            valArr.forEach(p => {
                if (p && p instanceof Object && ("ListDataID" in p)) {
                    result.push(p);
                } else if (p) {
                    listDataIDs.push(p);
                }
            });

            if (listDataIDs.length > 0) {
                let listDatas = await this.getIdentityFromSimpleValue(listDataIDs);
                if (listDatas) listDatas.forEach(data => result.push(data));
            }
        }
        return result;
    }

    async getIdentityFromSimpleValue(vals) {
        let checkArray = [];
        const { listID, listAddition, listFieldName, appID ,tooltipFieldName} = this.props;
        if (vals instanceof String) {
            checkArray.push(vals);
        } else {
            checkArray = vals;
        }
        let list=tooltipFieldName?[listFieldName,tooltipFieldName]:[listFieldName]
        const request: ContentListDatasByIdsRequest = {
            AppID: appID,
            ListID: listID,
            ListDataIDs: vals,
            Columns: listAddition ? AkUtil.distinct(
                listAddition.filter(i => i.RelationName || i.IsShow)
                .map(i => i.FieldName).concat(list)) :list,
            Verification: false
        };
        let rs = await ContentListApi.GetDataByIDs(request);
        return rs.Data;
    }

    parseSingle(record): DataHolder {
        const { listFieldName,tooltipFieldName } = this.props;
        let id = record["ListDataID"];
        let label = record[listFieldName];
        let tooltip=record[tooltipFieldName];
        // if (id && label) {
        return { id: id, label: label, data: record,tooltip:tooltip};
        // }
        // return null;
    }
    parseValue(original): DataHolderDict {
        let rs = {};
        if (original && original !== null) {
            if (AkUtil.isArray(original)) {
                original.forEach(record => {
                    let d = this.parseSingle(record);
                    if (d) {
                        rs[d.id] = d;
                    }
                })
            } else {
                let d = this.parseSingle(original);
                if (d) {
                    rs[d.id] = d;
                }
            }
        }
        return rs;
    }

    /**
     * 显示选中的Identity名称
     * @returns {any[]}
     */
    getTagDisplay(v, displayAll, maxDisplay) {
        const { nameDisplayLength, listFieldDict, listFieldName,tooltipFieldName } = this.props;
        let value = this.parseValue(v);
        const keys = Object.keys(value);
        if (!displayAll && keys.length > 0) {
            const listId = value[keys[0]].data.ListID;
            let form;
            if (listFieldDict && listFieldDict[listId]) {
                form = AkTagInput.AssignDefToForm(listFieldDict[listId].find(i => i.FieldName === listFieldName));
            }
            let temp = Object.keys(value).slice(0, this.props.maxDisplay);
            // if (temp.length === 1) {
            //     return this.getSingleTagDisplay(value[temp[0]], form);
            // } else { 
                const compArr = [];
                for (var i = 0; i < temp.length; i++) {
                    const item = temp[i];
                    compArr.push({text:this.getSingleTagDisplay(value[item], form),tooltip:value[item].tooltip,id:value[item].id});
                    // if (i < (temp.length - 1)) {
                    //     compArr.push(';');
                    // }
                }
                // let display = temp.map((item) => {
                //     return this.getSingleTagDisplay(value[item], form);
                // }).reduce((prev, curr) => [prev, ';', curr]);
                let display=compArr.map(c=>{  
                    return<AkTooltip  key={c.id} title={c.tooltip?c.tooltip:c.text} >
                        <AkTag>{c.text}</AkTag>
                     </AkTooltip>
                    });
                return <div>{display}</div>;
            //}
        } else {
            return keys;
        }
    }

    getSingleTagDisplay(value: DataHolder, form?) {
        const { nameDisplayLength, listFieldDict, listFieldName } = this.props;
        const obj = value.data;
        if (form)
        {
            form.value = obj[listFieldName]; 
        }
        return AkLookupRenderControl.renderList(form, "lookup", value.id)
        // let islong = value.label.length > nameDisplayLength;
        // return islong ? value.label.slice(0, nameDisplayLength) : value.label;
    }

    render() {
        const { multiple, maxSelection, maxDisplay, displayAll, nameDisplayLength, placeholder, readonly, autoCollapse, hideInputOnMaxSelectReached, listFieldName, className } = this.props;
        const { initialized, value } = this.state;
        const cl = classNames("ak-identity-picker-wrapper ak-lookup-picker-warpper", { multiple: multiple }, className);

        let values = Object.keys(value).map(k => value[k]);
        let display = displayAll || this.state.displayAll;

        return <div className={cl}>
            {this.getTagDisplay(values, display, maxDisplay)}
        </div>;
    }

}