import * as React from "react";
import { connect } from "react-redux";
import { AkSelect } from "../ak-select";
import { AkDictionaryString, ContentListField, AppCenterListResponse } from "../../../index";
import { ContentListAction } from "../../../actions";
import { AkGlobal } from "../../../util/common";
import { ListLookupLocale } from "../../../locales/localeid";
import { AkUtil } from '../../../util/util';
import { ContentListFieldStatus } from '../../../api/content-list/content-list-model';

export interface AkLookupFieldSelectProps {
    AppID?: number;
    onChange?: (value: string, label: string) => void;
    selectApp?: AppCenterListResponse;
    disabled?: boolean;
    listID?: string; // if not null select field, else select list
    value?: string;
    defaultValue?: string;
    ignoreType?: string[];
    listFieldDict?: AkDictionaryString<ContentListField[]>;
    placeholder?: string;
    style?: React.CSSProperties;
}

@connect(state => ({ listFieldDict: state.contentList.listFieldDict, selectApp: state.appCenter.selectApp }))
export default class AkLookupFieldSelect extends React.Component<AkLookupFieldSelectProps, undefined>{
    static defaultProps: AkLookupFieldSelectProps = {
        ignoreType: [],
    };

    constructor(props: AkLookupFieldSelectProps, context) {
        super(props, context);
        this.removeNotInclude(props);
    }

    componentDidMount() {
        this.dispatch(this.props);
    }

    componentWillReceiveProps(nextProps: AkLookupFieldSelectProps) {
        if ('listID' in nextProps && nextProps.listID !== this.props.listID) {
            this.dispatch(nextProps);
        }
        if ("AppID" in nextProps && nextProps.AppID !== this.props.AppID) {
            this.dispatch(nextProps);
        }
        if ('listFieldDict' in nextProps && nextProps.listFieldDict !== this.props.listFieldDict) {
            setTimeout(() => this.removeNotInclude(nextProps));
        }
    }

    dispatch(props: AkLookupFieldSelectProps) {
        let AppID;
        if (props.AppID) {
            AppID = props.AppID;
        } else {
            if (props.selectApp) {
                AppID = props.selectApp.AppCenterID;
            }
        }
        AkGlobal.store.dispatch(ContentListAction.requestField(props.AppID, props.listID));
    }

    removeNotInclude(props: AkLookupFieldSelectProps) {
        if (props.value && props.listFieldDict && props.listID &&
            props.listFieldDict[props.listID] && props.listFieldDict[props.listID].length) {
            const item = props.listFieldDict[props.listID].find(i => i.FieldName === props.value && props.ignoreType.indexOf(i.Type) === -1);
            if (!item && this.props.onChange) this.props.onChange(undefined, undefined);
        }
    }

    onChange = (value: string) => {
        const { onChange, listFieldDict, listID } = this.props;

        if (onChange && listID && listFieldDict &&
            listFieldDict[listID] && listFieldDict[listID].length) {
            if (value) {
                const item = listFieldDict[listID].find(i => i.FieldName === value);
                onChange(item.FieldName, item.DisplayName);
            } else {
                onChange(undefined, undefined);
            }
        }
    }

    render() {
        // tslint:disable-next-line:no-unused-variable
        const { listID, ignoreType, listFieldDict, onChange, placeholder: defaultPlaceholder, ...others } = this.props;
        const options = [];

        if (listFieldDict[listID]) {
            AkUtil.each(listFieldDict[listID], (item: ContentListField) => {
                if (ignoreType.indexOf(item.Type) === -1 &&
                    !AkUtil.enumFlagContians(item.Status, ContentListFieldStatus.NotForLookup)) {
                    options.push(<AkSelect.Option key={listID + item.FieldName} value={item.FieldName}>
                        {item.DisplayName}
                    </AkSelect.Option>);
                }
            });
        }

        const placeholder = defaultPlaceholder ? defaultPlaceholder :
            AkGlobal.intl.formatMessage({ id: ListLookupLocale.SelectFieldPlaceholder });

        return <AkSelect {...others} onChange={this.onChange} placeholder={placeholder}
            dropdownMatchSelectWidth={false}>{options}</AkSelect>;
    }
}
