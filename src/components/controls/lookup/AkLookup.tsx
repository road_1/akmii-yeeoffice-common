import * as React from "react";
import * as classNames from "classnames";
import AkLookupAdv from "./AkLookupAdv";
import { AkUtil, ContentListAction, AkDictionaryString, ContentListField } from "../../../index";
import { AkModal } from "../ak-modal";
import { AkTagInput } from "../ak-tag-input";
import { AkGlobal } from "../../../util/common";
import { CommonLocale } from "../../../locales/localeid";
import { ContentListApi } from "../../../api/content-list/content-list-api";
import { connect } from 'react-redux';
import {
    ContentListDatasByIdsRequest,
    ContentListWhereModel,
    ContentListAdditionModel
} from "../../../api/content-list/content-list-model";
import { AkInput } from "../ak-input";

export interface AkLookupProps {
    searchRowCount?: number;
    multiple?: boolean; //is allow multiple selection
    maxSelection?: number; //how many identity selection allowed
    maxDisplay?: number; //how many identity will be displayed on control
    displayAll?: boolean; //是否显示全部选项
    nameDisplayLength?: number; //超出长度的名称会被截取
    style?: React.CSSProperties; //additional style apply to control
    className?: string;
    placeholder?: string; //输入默认的提示语
    defaultValue?: string | string[]; //default selection
    readonly?: boolean;
    tooltipFieldName?:string;
    autoCollapse?: boolean; //展开隐藏内容后，鼠标离开是否自动收起
    value?: string | string[]; //和onChange一起结合使用，如果value赋值则defaultValue无效
    hideInputOnMaxSelectReached?: boolean;
    onChange?: (id: string | string[], obj?: any | any[]) => any; //onchange事件，将选中dict输出
    appID?: number;
    listID?: string; // Linked list ID
    listAddition?: ContentListAdditionModel[]; // display options
    listFieldName?: string; // Default display on tags field
    filters?: () => Promise<ContentListWhereModel[]>; // Custom where functions
    listFieldDict?: AkDictionaryString<ContentListField[]>;
    readOnlyShowBorder?: boolean;//readonly时是否显示边框
  
}

export interface AkLookupState {
    value?: Object;
    maxSelection?: number; //最大选择项
    initialized?: boolean; //是否初始化完成
    displayAll?: boolean;
    searchResult?: any[];
    advanceVisible?: boolean; //高级选择器显示
    advanceTempValue?: Object; //advance picker的临时值
    inputValue?: string;
    hideInput?: boolean;
    width?: number;
}

@connect(state => ({ listFieldDict: state.contentList.listFieldDict }))
export default class AkLookup extends React.Component<AkLookupProps, AkLookupState>{
    static defaultProps: AkLookupProps = {
        searchRowCount: 20,
        multiple: false,
        maxSelection: 200,
        maxDisplay: 5,
        nameDisplayLength: 20,
        readonly: false,
        displayAll: false,
        autoCollapse: false,
        hideInputOnMaxSelectReached: true
    };

    constructor(props, context) {
        super(props, context);
        const columns = props.listAddition ? props.listAddition.filter(i => i.IsShow).length : 0;

        let v = ("value" in props) ? props.value : props.defaultValue;

        this.state = {
            maxSelection: props.multiple ? props.maxSelection : 1,
            value: {},
            initialized: (v && v.length > 0) ? false : true,
            searchResult: [],
            advanceVisible: false,
            advanceTempValue: {},
            width: columns >= 4 ? 150 * columns : undefined
        };
    }

    componentDidMount() {
        let value = ("value" in this.props) ? this.props.value : this.props.defaultValue;
        if (value && value.length > 0) {
            this.parseUnresolvedValue(value).then(d => {
                this.setState({ value: AkUtil.arrayToObject(d, "ListDataID"), initialized: true });
            });
        }
        AkGlobal.store.dispatch(ContentListAction.requestField(this.props.appID, this.props.listID));
    }

    ifValueAreEqual(v1, v2) {
        return v1 === v2 || JSON.stringify(v1) === JSON.stringify(v2);
    }

    componentWillReceiveProps(nextProps: AkLookupProps) {
        if ("value" in nextProps && nextProps.value !== this.props.value && !AkUtil.isEqual(this.props.value, nextProps.value)) {
            this.parseUnresolvedValue(nextProps.value).then(d => {
                this.setState({ value: AkUtil.arrayToObject(d, 'ListDataID'), initialized: true });
            });
        }

        if ("multiple" in nextProps && nextProps.multiple !== this.props.multiple) {
            this.setState({ maxSelection: nextProps.multiple ? nextProps.maxSelection || this.props.maxSelection : 1 })
        }

        if ("maxSelection" in nextProps && nextProps.maxSelection !== this.props.maxSelection) {
            this.setState({ maxSelection: nextProps.multiple ? nextProps.maxSelection : 1 })
        }
    }

    changeValue(value: Object) {
        if (!("value" in this.props)) {
            //如果外部传入value，则value由外部控制
            this.setState({ value: value });
        }

        if (this.props.onChange) {
            const objs = Object.keys(value).map(k => value[k]);
            if (this.props.multiple) {
                this.props.onChange(objs.map(i => i.ListDataID), objs);
            } else {
                if (objs.length > 0) {
                    this.props.onChange(objs[0].ListDataID, objs[0]);
                } else {
                    this.props.onChange(null, null)
                }
            }
        }
    }

    async parseUnresolvedValue(val) {
        let result = [];
        let valArr = [];

        if (val) {
            if (val instanceof Array) {
                valArr = val;
            } else {
                valArr.push(val);
            }
            //获取所有需要解析的id数组，一次获取解析
            const listDataIDs = [];
            valArr.forEach(p => {
                if (p && p instanceof Object && ("ListDataID" in p)) {
                    result.push(p);
                } else if (p) {
                    listDataIDs.push(p);
                }
            });

            if (listDataIDs.length > 0) {
                let listDatas = await this.getIdentityFromSimpleValue(listDataIDs);
                if (listDatas) listDatas.forEach(data => result.push(data));
            }
        }

        return result;
    }

    async getIdentityFromSimpleValue(vals) {
        let checkArray = [];
        const { listID, listAddition, listFieldName, tooltipFieldName, appID } = this.props;
        if (vals instanceof String) {
            checkArray.push(vals);
        } else {
            checkArray = vals;
        }
        let list=tooltipFieldName?[listFieldName,tooltipFieldName]:[listFieldName]
        const request: ContentListDatasByIdsRequest = {
            AppID: appID,
            ListID: listID,
            ListDataIDs: vals,
            Columns: listAddition ? AkUtil.distinct(
                listAddition.filter(i => i.RelationName || i.IsShow)
                    .map(i => i.FieldName).concat(list)) :list,
            Verification: false
        };
       
        let rs = await ContentListApi.GetDataByIDs(request);
        return rs.Data;
    }

    /**
     * 根据输入值的变化调用后端搜索api
     * @param value 当前输入的值
     */
    async handleInputChange(value): Promise<any[]> {
        const { filters, listAddition, listFieldName,tooltipFieldName } = this.props;

        const wheres = filters ? await filters() : null;


        let list=tooltipFieldName?[listFieldName,tooltipFieldName]:[listFieldName]
      
        if (wheres !== undefined) {
            const d = await ContentListApi.SearchEngines({
                AppID: this.props.appID,
                ListID: this.props.listID,
                Columns: listAddition ? AkUtil.distinct(
                    listAddition.filter(i => i.RelationName || i.IsShow)
                    .map(i => i.FieldName).concat(list)) :list,
                PageIndex: 1,
                PageSize: this.props.searchRowCount,
                IsExport: false,
                FilterValue: value,
                Wheres: wheres,
                Verification: false
            });
            if (d) {
                return d.Data;
            }
        }
        return undefined;
    }

    advancePickerOK() {
        this.setState({ advanceVisible: false });
        this.changeValue(Object.assign({}, this.state.advanceTempValue));
    }

    advancePickerCancel() {
        this.setState({ advanceVisible: false, advanceTempValue: this.state.value });//直接传对象，因为adv控件不直接使用
    }

    advanceValueChange(value) {
        this.setState({ advanceTempValue: value });
    }

    getAdvancePickerDisplay() {
        const { value, appID, onChange, maxDisplay, displayAll, style, placeholder, defaultValue, readonly, autoCollapse, hideInputOnMaxSelectReached, ...others } = this.props;

        return this.state.advanceVisible && <AkModal title={AkGlobal.intl.formatMessage({ id: CommonLocale.FormValidatePleaseSelect })} width={this.state.width}
            style={{ top: 30 }} visible={this.state.advanceVisible} onOk={this.advancePickerOK.bind(this)}
            onCancel={this.advancePickerCancel.bind(this)}>
            <AkLookupAdv appID={appID} value={this.state.advanceTempValue} open={this.state.advanceVisible}
                onChange={this.advanceValueChange.bind(this)} {...others} />
        </AkModal>
    }

    displayAdvancePicker() {
        const newValue = Object.assign({}, this.state.value);
        this.setState({ advanceVisible: true, advanceTempValue: newValue });
    }
    getAkTagInput() {
        const { formatMessage } = AkGlobal.intl;
        const { multiple, maxSelection, maxDisplay, displayAll, nameDisplayLength, listID, placeholder, readonly, autoCollapse, hideInputOnMaxSelectReached, listFieldName,tooltipFieldName, listFieldDict, readOnlyShowBorder } = this.props;
        const { initialized, value } = this.state;
        return <AkTagInput multiple={multiple}
            className={this.props.className}
            maxSelection={maxSelection}
            maxDisplay={maxDisplay}
            displayAll={displayAll}
            nameDisplayLength={nameDisplayLength}
            placeholder={placeholder ? placeholder : formatMessage({ id: CommonLocale.KeywordPlaceHolder })}
            onChange={(arr, v) => this.changeValue(v)}
            onInputChange={value => this.handleInputChange(value)}
            readonly={readonly}
            autoCollapse={autoCollapse}
            hideInputOnMaxSelectReached={hideInputOnMaxSelectReached}
            advanceIcon="search"
            advanceIconClick={this.displayAdvancePicker.bind(this)}
            showSpin={!initialized}
            openLushDisplay={true}
            listFieldDict={listFieldDict[listID]}
            value={Object.keys(value).map(k => value[k])}
            valueDescriber={{ id: "ListDataID", label: listFieldName,tooltip:tooltipFieldName }}
            dropdownMatchSelectWidth={!multiple} />
    }

    render() {
        const { multiple, readonly, readOnlyShowBorder } = this.props;
        const className = classNames("ak-identity-picker-wrapper ak-lookup-picker-warpper", { multiple: multiple });
        return <div className={className}>
            {
                readOnlyShowBorder ?
                    (this.props.value || !readonly ? this.getAkTagInput() : <AkInput disabled={true}></AkInput>)
                    : this.getAkTagInput()
            }
            {this.getAdvancePickerDisplay()}
        </div>;
    }

}