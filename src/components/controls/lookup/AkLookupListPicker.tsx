import * as React from "react";
import { connect } from "react-redux";
import { AkSelect } from "../ak-select";
import { ContentListItem, AppCenterListResponse, AkTagInput, IdentityLocale, IdentityAPI, AkIdentity, AkIdentityType, AkTagValueDescriber, AkLookupListPickerValue, AkModal, AkLookupListPickerAdv, ListLookupLocale, ContentListApi } from "../../../index";
import { ContentListAction, AppCenterAction } from "../../../actions";
import { AkUtil } from "../../../util/util";
import { AkGlobal, AkContext, AppKeys } from '../../../util/common';
import { ContentListType, ContentListRequest } from '../../../api/content-list/content-list-model';

export interface AkLookupListPickerProps {
    onChange?: (value?: ContentListItem) => void;
    className?: string;
    searchRowCount?: number;//搜索结果的行数---默认10行
    multiple?: boolean; //是否开启多选---默认不开启
    maxSelection?: number; //最多可选多少个标签---默认200个
    maxDisplay?: number; //最多显示多少个标签---默认5个
    displayAll?: boolean; //是否显示全部选项
    nameDisplayLength?: number; //超出长度的名称会被截取
    style?: React.CSSProperties; //additional style apply to control
    placeholder?: string; //输入默认的提示语
    defaultValue?: ContentListItem; //default selection
    readonly?: boolean;
    autoCollapse?: boolean; //展开隐藏内容后，鼠标离开是否自动收起
    value?: ContentListItem; //和onChange一起结合使用，如果value赋值则defaultValue无效
    hideInputOnMaxSelectReached?: boolean;//超过maxSelection隐藏输入框
    appCenterList?: AppCenterListResponse[];
    selectApp?: AppCenterListResponse;
    filterlistID?: string | string[];
    ListType?: ContentListType;

    isShowWriteRoleList?:boolean;
}

export interface AkLookupListPickerStates {
    AppID?: number;
    value?: ContentListItem;
    maxSelection?: number; //最大选择项
    initialized?: boolean; //是否初始化完成
    displayAll?: boolean;
    // searchResult?: AkIdentity[];
    advanceVisible?: boolean; //高级选择器显示
    advanceTempValue?: ContentListItem; //advance picker的临时值
    // inputValue?: string;
    hideInput?: boolean;
    searchResource?: ContentListItem[];
}

@connect(state => ({ appCenterList: state.appCenter.appCenterList, selectApp: state.appCenter.selectApp }))
export default class AkLookupListPicker extends React.Component<AkLookupListPickerProps, AkLookupListPickerStates>{
    static defaultProps: AkLookupListPickerProps = {
        searchRowCount: 10,
        multiple: false,
        maxSelection: 200,
        maxDisplay: 5,
        nameDisplayLength: 20,
        readonly: false,
        displayAll: false,
        autoCollapse: false,
        hideInputOnMaxSelectReached: true
    }
    constructor(props: AkLookupListPickerProps, context) {
        super(props, context);
        this.state = {
            maxSelection: props.multiple ? props.maxSelection : 1,
            AppID: this.getAppID(props),
            value: this.props.value,
            initialized: false,
            searchResource: [],
        }
    }

    componentDidMount() {
        if (this.props.appCenterList) {
            this.setState({ initialized: true });
        }
        const { value } = this.state;
        if (typeof value === "string") {
            this.getListByValue(AkContext.getAppInfoID(), value, this.props.ListType).then(d => this.setState({ value: d }));
        } else {
            if (value && value.ListID && !value.Title) {
                this.getListByValue(value.AppID, value.ListID, this.props.ListType).then(d => this.setState({ value: d }));
            }
        }
    }

    getAppID(props) {
        return (props.value && props.value.AppID) || (props.selectApp && props.selectApp.AppCenterID) || AkContext.getAppinfoByAppKey(AppKeys.Flowcraft).AppInfoID;
    }

    async getListByValue(appId, listId, type) {
        let params: any = { AppID: appId, ListID: listId }
        if (type) {
            params = { ...params, type }
        }
        const rs = await ContentListApi.GetList(params);
        return rs.Status === 0 ? rs.Data : this.state.value;
    }

    componentWillReceiveProps(nextProps: AkLookupListPickerProps) {
        if ("appCenterList" in nextProps && nextProps.appCenterList !== this.props.appCenterList) {
            this.setState({ initialized: true });
        }
        if ("selectApp" in nextProps && nextProps.selectApp !== this.props.selectApp) {
            this.setState({ AppID: this.getAppID(nextProps) });
        }
        if ("value" in nextProps && this.props.value !== nextProps.value) {
            if (typeof nextProps.value === "string") {
                this.getListByValue(AkContext.getAppInfoID(), nextProps.value, nextProps.ListType).then(d => this.setState({ value: d }));
            } else {
                if (nextProps.value && nextProps.value.ListID && !nextProps.value.Title) {
                    this.getListByValue(nextProps.value.AppID, nextProps.value.ListID, nextProps.ListType).then(d => this.setState({ value: d }));
                }
            }
            this.setState({ AppID: this.getAppID(nextProps) });
        }
    }

    getAdvancePickerDisplay() {
        const { value, onChange, maxDisplay, displayAll, placeholder, defaultValue, readonly, autoCollapse, ...others } = this.props;
        const { advanceTempValue, advanceVisible } = this.state;
        const { formatMessage } = AkGlobal.intl;
        return advanceVisible ?
            <AkModal
                title={formatMessage({ id: ListLookupLocale.SelectListTitle })}
                style={{ top: 30 }}
                visible={true}
                onOk={() => this.advancePickerOk()}
                onCancel={() => {
                    this.advancePickerCancel();
                }}>
                <AkLookupListPickerAdv ListType={this.props.ListType} AppID={this.state.AppID} value={advanceTempValue}
                    onChange={(v) => { this.setState({ advanceTempValue: v, value: v }) }} {...others} />
            </AkModal> : undefined;
    }

    advancePickerCancel() {
        this.setState({ advanceVisible: false, advanceTempValue: this.state.value as any });
    }

    advancePickerOk() {
        const { props: { onChange }, state: { advanceTempValue, value } } = this;
        this.setState({ advanceVisible: false });
        if (value.ListID) {
            onChange && onChange(value);
        }
    }

    changeValue(arr, v) {
        const { props: { onChange } } = this;
        if (arr.length > 0) {
            this.setState({ advanceTempValue: arr[0], value: arr[0] })
            onChange && onChange(arr[0]);
        } else {
            this.setState({ advanceTempValue: null, value: null })
            onChange && onChange(null);
        }
    }

    async inputChangeAsync(value) {
        const { state: { searchResource, AppID }, props: { ListType } } = this;

        if (searchResource.length > 0) {
            if (AppID !== searchResource[0].AppID) {
                const data = await this.searchRequest();
                return this.searchDataHandler(data, value);
            } else {
                return this.searchDataHandler(searchResource, value);
            }
        } else {
            const data = await this.searchRequest();
            return this.searchDataHandler(data, value);
        }
    }

    async searchRequest() {
        const { state: { AppID }, props: { ListType } } = this;
        let params: ContentListRequest = { AppID, title: "" }
        if (ListType) {
            params = { ...params, type: ListType }
        }
        let rs = await ContentListApi.GetLists(params);
        let data;
        if (typeof this.props.filterlistID === "string") {
            data = rs.Data.filter(i => i.ListID !== this.props.filterlistID);
        } else {
            data = rs.Data
        }
        this.setState({ searchResource: data });
        return data
    }

    searchDataHandler(values: ContentListItem[], value: string) {
        return values.filter(item => item.Title.indexOf(value) > -1);
    }


    displayAdvancePicker() {
        let newValue = Object.assign({}, this.state.value);
        this.setState({ advanceVisible: true, advanceTempValue: newValue });
    }

    render() {
        const { formatMessage } = AkGlobal.intl;
        const { readonly, autoCollapse, hideInputOnMaxSelectReached, style, className, multiple, maxSelection, maxDisplay, displayAll, nameDisplayLength, placeholder } = this.props;
        const { initialized, value } = this.state;
        const describer: AkTagValueDescriber = {
            id: "ListID",
            label: "Title",
            tooltip: "tooltip"
        }
        return <div className={className} style={style}>
            <AkTagInput className={className} multiple={multiple} maxSelection={maxSelection}
                maxDisplay={maxDisplay} displayAll={displayAll} nameDisplayLength={nameDisplayLength}
                placeholder={placeholder ? placeholder : formatMessage({ id: IdentityLocale.SimplePlaceholder })}
                onChange={(arr, v) => this.changeValue(arr, v)}
                onInputChange={value => this.inputChangeAsync(value)}
                readonly={readonly} autoCollapse={autoCollapse}
                hideInputOnMaxSelectReached={hideInputOnMaxSelectReached}
                advanceIcon="organization" advanceIconClick={this.displayAdvancePicker.bind(this)}
                value={value}
                showSpin={!initialized}
                valueDescriber={describer}
                dropdownMatchSelectWidth={!multiple} />
            {this.getAdvancePickerDisplay()}
        </div>

    }
}