import * as React from "react";
import { connect } from "react-redux";
import { AkSelect } from "../ak-select";
import { ContentListItem, AppCenterListResponse } from "../../../index";
import { ContentListAction } from "../../../actions";
import { AkUtil } from "../../../util/util";
import { AkGlobal, AkContext, AppKeys } from '../../../util/common';
import { ListLookupLocale } from "../../../locales/localeid";
import { ContentListType } from '../../../api/content-list/content-list-model';

export interface AkLookupListSelectProps {
    AppID?: number;
    selectApp?: AppCenterListResponse;
    onChange?: (value: string, label: string) => void;
    disabled?: boolean;
    value?: string;
    defaultValue?: string;
    lists?: ContentListItem[];
    placeholder?: string;
    style?: React.CSSProperties;
    allowClear?: boolean;
    ListType?: ContentListType;
}

@connect(state => ({ lists: state.contentList.lists, selectApp: state.appCenter.selectApp }))
export default class AkLookupListSelect extends React.Component<AkLookupListSelectProps, undefined>{
    constructor(props: AkLookupListSelectProps, context) {
        super(props, context);
        this.removeNotInclude(props);
    }

    componentDidMount() {
        const { props: { value, AppID, selectApp } } = this;
        if (!AppID && !value) {
            AkGlobal.store.dispatch(ContentListAction.requestList(selectApp && selectApp.AppCenterID, null, this.props.ListType));
        } else {
            AkGlobal.store.dispatch(ContentListAction.requestList(this.props.AppID, null, this.props.ListType));
        }
    }

    componentWillReceiveProps(nextProps: AkLookupListSelectProps) {
        if ("AppID" in nextProps && this.props.AppID !== nextProps.AppID) {
            AkGlobal.store.dispatch(ContentListAction.requestList(nextProps.AppID, null, nextProps.ListType));
            this.removeNotInclude(nextProps);
        }
        if ('lists' in nextProps && nextProps.lists !== this.props.lists) {
            this.removeNotInclude(nextProps);
        }
    }

    removeNotInclude(props: AkLookupListSelectProps) {
        if (props.value && props.lists && props.lists.length) {
            const item = props.lists.find(i => i.ListID === props.value);
            if (!item && this.props.onChange) this.props.onChange(undefined, undefined);
        }
    }

    onChange = (value: string) => {
        const { onChange, lists } = this.props;

        if (onChange && lists && lists.length) {
            if (value) {
                const item = lists.find(i => i.ListID === value);
                onChange(item.ListID, item.Title);
            } else {
                onChange(undefined, undefined);
            }
        }
    }

    render() {
        // tslint:disable-next-line:no-unused-variable
        const { lists, onChange, placeholder: defaultPlaceholder, ...others } = this.props;
        const options = [];
        AkUtil.each(lists, (item) => {
            options.push(<AkSelect.Option key={item.ListID} value={item.ListID}>
                {item.Title}
            </AkSelect.Option>);
        });

        const placeholder = defaultPlaceholder ? defaultPlaceholder :
            AkGlobal.intl.formatMessage({
                id: (AkContext.getAppKey() === AppKeys.YeeOfficeDocument_Net || AkContext.getAppKey() === AppKeys.YeeOfficeDocument) ?
                    ListLookupLocale.SelectLibraryPlaceholder : ListLookupLocale.SelectListPlaceholder
            });

        return <AkSelect {...others} onChange={this.onChange} placeholder={placeholder}
            dropdownMatchSelectWidth={false}>{options}</AkSelect>;
    }
}
