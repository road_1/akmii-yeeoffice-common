import * as classNames from "classnames";
import * as React from "react";
import { AkModal, AkTagInput } from "..";
import { AkGlobal, AkUtil, CommonLocale, ProcDefsAPI, ProcDefBrief } from "../../..";
import AkLookupFlowNameAdv from "./AkLookupFlowNameAdv";

export interface AkLookupFlowNameProp {
    className?: string;
    searchRowCount?: number;
    exclusions?: string[];//排除项
    maxSelection?: number; //how many identity selection allowed
    maxDisplay?: number; //how many identity will be displayed on control
    displayInput?: boolean; //是否显示输入框
    displayAll?: boolean; //是否显示全部选项
    nameDisplayLength?: number; //超出长度的名称会被截取
    style?: React.CSSProperties; //additional style apply to control
    placeholder?: string; //输入默认的提示语
    defaultValue?: string | string[]; //default selection
    readonly?: boolean;
    autoCollapse?: boolean; //展开隐藏内容后，鼠标离开是否自动收起
    value?: string | string[]; //和onChange一起结合使用，如果value赋值则defaultValue无效
    hideInputOnMaxSelectReached?: boolean;
    onChange?: (id: string | string[], obj?: any | any[]) => any; //onchange事件，将选中dict输出
}

export interface AkLookupFlowNameState {
    value?: Object;
    maxSelection?: number; //最大选择项
    initialized?: boolean; //是否初始化完成
    displayAll?: boolean;
    searchResult?: ProcDefBrief[];
    advanceVisible?: boolean; //高级选择器显示
    advanceTempValue?: Object; //advance picker的临时值
    inputValue?: string;
    hideInput?: boolean;
    upDisplayTag?: boolean;//更新展示范围
}

export class AkLookupFlowName extends React.Component<AkLookupFlowNameProp, AkLookupFlowNameState> {
    DefKey: string;
    DefName: string;

    static defaultProps: AkLookupFlowNameProp = {
        displayInput: true,
        searchRowCount: 10,
        maxSelection: 9999999999999,
        maxDisplay: 5,
        nameDisplayLength: 20,
        readonly: false,
        displayAll: false,
        autoCollapse: false,
        hideInputOnMaxSelectReached: true
    }

    constructor(props, context) {
        super(props, context);
        let self = this;
        self.state = {
            maxSelection: props.maxSelection,
            value: {},
            initialized: false,
            searchResult: [],
            advanceVisible: false,
            advanceTempValue: {}
        };
        self.DefKey = "DefKey";
        self.DefName = "DefName";
    }

    componentWillMount() {
        let self = this;
        const { exclusions, value, defaultValue } = this.props;
        let v = ("value" in self.props) ? value : defaultValue;

        ProcDefsAPI.getFlowList({}).then(d => {
            if (d.Status === 0) {
                if (v && v.length > 0) {
                    self.parseUnresolvedValue(v, d.Data).then(d => {
                        self.setState({ value: AkUtil.arrayToObject(d, self.DefKey), initialized: true });
                    });
                }
                let result = d.Data;
                if (exclusions && exclusions.length > 0) {
                    result = d.Data.filter(f => exclusions.findIndex(e => e === f.DefKey) === -1);
                }
                this.setState({ searchResult: result, initialized: true })
            }
        });
    }

    async parseUnresolvedValue(val, data) {
        let self = this;
        let result = [];
        let valArr = [];

        if (val) {
            if (val instanceof Array) {
                valArr = val;
            } else {
                valArr.push(val);
            }
            //获取所有需要解析的id数组，一次获取解析
            const listDataIDs = [];
            valArr.forEach(p => {
                if (p && p instanceof Object && (self.DefKey in p)) {
                    result.push(p);
                } else if (p) {
                    listDataIDs.push(p);
                }
            });

            if (listDataIDs.length > 0) {
                let listDatas = await this.getIdentityFromSimpleValue(listDataIDs, data);
                if (listDatas) listDatas.forEach(data => result.push(data));
            }
        }
        return result;
    }
    async getIdentityFromSimpleValue(vals, data) {
        let self = this;
        let checkArray = [];
        if (vals instanceof String) {
            checkArray.push(vals);
        } else {
            checkArray = vals;
        }
        return data.filter(f => checkArray.findIndex(a => a === f[self.DefKey]) > -1) as any[];
    }

    displayAdvancePicker() {
        const newValue = Object.assign({}, this.state.value);
        this.setState({ advanceVisible: true, advanceTempValue: newValue });
    }

    changeValue(value: Object) {
        if (!("value" in this.props)) {
            //如果外部传入value，则value由外部控制
            this.setState({ value: value });
        }
        if (this.props.onChange) {
            const objs = Object.keys(value).map(k => value[k]);
            this.props.onChange(objs.map(i => i[this.DefKey]), objs);
        }
    }
    advancePickerOK() {
        this.setState({ advanceVisible: false });
        this.changeValue(Object.assign({}, this.state.advanceTempValue));
    }

    advancePickerCancel() {
        this.setState({ advanceVisible: false, advanceTempValue: this.state.value });//直接传对象，因为adv控件不直接使用
    }
    /**
    * 根据输入值的变化调用后端搜索api
    * @param value 当前输入的值
    */
    async handleInputChange(value): Promise<any[]> {
        // let self = this;
        // const d = await ProcDefsAPI.getFlowList({});
        // let result = undefined;
        const { searchResult } = this.state;
        return searchResult.filter(f => f[this.DefName].indexOf(value) > -1);
    }

    getAkTagInput() {
        let self = this;
        const { formatMessage } = AkGlobal.intl;
        const { maxSelection, maxDisplay, displayAll, nameDisplayLength, placeholder, readonly, autoCollapse, hideInputOnMaxSelectReached, displayInput } = self.props;
        const { initialized, value } = self.state;

        return <AkTagInput
            readonly={readonly}
            multiple={true}
            className={self.props.className}
            maxSelection={maxSelection}
            maxDisplay={maxDisplay}
            displayInput={displayInput}
            displayAll={displayAll}
            nameDisplayLength={nameDisplayLength}
            placeholder={placeholder ? placeholder : formatMessage({ id: CommonLocale.KeywordPlaceHolder })}
            onChange={(arr, v) => self.changeValue(v)}
            onInputChange={value => self.handleInputChange(value)}
            autoCollapse={autoCollapse}
            hideInputOnMaxSelectReached={hideInputOnMaxSelectReached}
            advanceIcon="search"
            advanceIconClick={self.displayAdvancePicker.bind(self)}
            showSpin={!initialized}
            value={Object.keys(value).map(k => value[k])}
            valueDescriber={{ id: self.DefKey, label: self.DefName }}
            dropdownMatchSelectWidth={false} />
    }
    advanceValueChange(value) {
        this.setState({ advanceTempValue: value });
    }

    getAdvancePickerDisplay() {
        const { value, onChange, maxDisplay, displayAll, style, placeholder, defaultValue, readonly, autoCollapse, hideInputOnMaxSelectReached, exclusions, ...others } = this.props;

        return this.state.advanceVisible && <AkModal title={AkGlobal.intl.formatMessage({ id: CommonLocale.FormValidatePleaseSelect })}
            style={{ top: 30 }} visible={this.state.advanceVisible} onOk={this.advancePickerOK.bind(this)}
            onCancel={this.advancePickerCancel.bind(this)}>
            <AkLookupFlowNameAdv exclusions={exclusions} value={this.state.advanceTempValue} open={this.state.advanceVisible}
                onChange={this.advanceValueChange.bind(this)} data={this.state.searchResult} {...others} />
        </AkModal>
    }


    render() {
        const { readonly } = this.props;
        const className = classNames("ak-identity-picker-wrapper ak-lookup-picker-warpper", { multiple: true });
        return <div className={className}>
            {this.getAkTagInput()}
            {this.getAdvancePickerDisplay()}
        </div>;
    }
}