import * as React from "react";
import { connect } from "react-redux";
import { AkSelect } from "../ak-select";
import { AppCenterListResponse } from "../../../index";
import { AppCenterAction } from "../../../actions";
import { AkUtil } from "../../../util/util";
import { AkGlobal, AkContext, AppKeys } from '../../../util/common';
import { ListLookupLocale } from "../../../locales/localeid";
import { MASTERMENU_PREFIX } from "../../masterpage/navigator";

export interface AkLookupAppSelectProps {
    onChange?: (value: number, label: string) => void;
    disabled?: boolean;
    value?: number;
    defaultValue?: string;
    placeholder?: string;
    style?: React.CSSProperties;
    allowClear?: boolean;

    appCenterList?: AppCenterListResponse[];
}


@connect(state => ({ appCenterList: state.appCenter.appCenterList }))
export default class AkLookupAppSelect extends React.Component<AkLookupAppSelectProps, undefined>{
    constructor(props: AkLookupAppSelectProps, context) {
        super(props, context);
        this.removeNotInclude(props);
    }

    componentWillReceiveProps(nextProps: AkLookupAppSelectProps) {
        if ('appCenterList' in nextProps && nextProps.appCenterList !== this.props.appCenterList) {
            this.removeNotInclude(nextProps);
        }
        // if ('value' in nextProps && nextProps.value !== this.props.value) {
        //     const { props: { onChange, appCenterList } } = this;
        //     onChange && onChange(nextProps.value, appCenterList.find(i => i.AppCenterID === nextProps.value).Title);
        // }
    }

    removeNotInclude(props: AkLookupAppSelectProps) {
        if (props.value && props.appCenterList && props.appCenterList.length) {
            const item = props.appCenterList.find(i => i.AppCenterID === props.value);
            if (!item && this.props.onChange) this.props.onChange(undefined, undefined);
        }
    }

    onChange = (value: any) => {
        const { appCenterList, onChange } = this.props;
        let appCenterLists = appCenterList;
        const flowcraft = AkContext.getAppinfoByAppKey(AppKeys.Flowcraft);
        if (flowcraft) {
            appCenterLists = [{ AppCenterID: flowcraft.AppInfoID, Title: AkGlobal.intl.formatMessage({ id: MASTERMENU_PREFIX + AppKeys.Flowcraft }) }, ...appCenterLists]
        }
        if (onChange && appCenterLists && appCenterLists.length) {
            if (value) {
                const item = appCenterLists.find(i => i.AppCenterID + "" === value);
                onChange(item.AppCenterID, item.Title);
            } else {
                onChange(undefined, undefined);
            }
        }
    }

    render() {
        const { value, appCenterList, onChange, placeholder: defaultPlaceholder, ...others } = this.props;
        const options = [];
        const flowcraft = AkContext.getAppinfoByAppKey(AppKeys.Flowcraft);
        let appCenterLists = appCenterList
        if (flowcraft) {
            appCenterLists = [{ AppCenterID: flowcraft.AppInfoID, Title: AkGlobal.intl.formatMessage({ id: MASTERMENU_PREFIX + AppKeys.Flowcraft }) }, ...appCenterLists]
        }
        AkUtil.each(appCenterLists, (item) => {
            options.push(<AkSelect.Option key={item.AppCenterID} value={item.AppCenterID + ""}>
                {item.Title}
            </AkSelect.Option>);
        });
        const placeholder = defaultPlaceholder ? defaultPlaceholder : AkGlobal.intl.formatMessage({ id: ListLookupLocale.SelectAppPlaceholder });

        return <AkSelect {...others} value={value ? value + "" : ""} onChange={this.onChange} placeholder={placeholder}
            dropdownMatchSelectWidth={false}>{options}</AkSelect>;
    }
}
