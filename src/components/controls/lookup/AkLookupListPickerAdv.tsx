import * as React from 'react';
import { AkRow, AkCol, AkSearch, AkTable, AkLookupAppSelect, AkLookupListSelect, AkColumnProps, AkTableRowSelection, AkTag, AkPagination, AkTree, AkRadio } from "..";
import { ContentListItem, AkLookupListPickerValue, ListLookupLocale, AppCenterListResponse, ContentListAction, ContentListApi, AkUtil } from "../../..";
import { AkGlobal, AkContext, AppKeys } from '../../../util/common';
import { connect } from "react-redux";
import { ContentListType, ContentListRightTypeEnum } from '../../../api/content-list/content-list-model';
import { ListSetAction } from '../../../actions';

export interface AkLookupListPickerAdvProps {
    AppID?: number;
    value?: ContentListItem;
    onChange?: (value?: ContentListItem, customType?: string) => void;
    nameDisplayLength?: number;
    appCenterList?: AppCenterListResponse[];
    lists?: ContentListItem[];
    filterlistID?: string | string[];
    ListType?: ContentListType;

    isShowWriteRoleList?:boolean;

    listSets?: any
}
export interface AkLookupListPickerAdvStates {
    AppID?: number;
    value?: ContentListItem;
    searchValue?: string;
    lists?: ContentListItem[];
    listSiteData?: ContentListItem[];
    pageLists?: any[];

    radioCheckValue?: string;
    current?: number;
    selectedKeys?: string[];
}

export class AkAppListTable extends AkTable<ContentListItem> {
}

const CUSTOMTYPE_PREFIX = "ListSite_";

const loadConnect = (state, props) => {
    return {
        appCenterList: state.appCenter.appCenterList,
        lists: state.contentList.lists,
        listSets: state.listSets
    };
}

@connect(loadConnect)
export default class AkLookupListPickerAdv extends React.Component<AkLookupListPickerAdvProps, AkLookupListPickerAdvStates>{
    columns?: AkColumnProps<ContentListItem>[];
    searchValue?: string;
    format = AkGlobal.intl.formatMessage;
    defaultPageSize?: number;
    constructor(props, context) {
        super(props, context);
        this.defaultPageSize = 10;

        //过滤读写权限问题
        let roleList = [];
        if (props.isShowWriteRoleList) {
            roleList = this.props.lists.filter(i=>i.MaxRole >= ContentListRightTypeEnum.Write);
        } else {
            roleList = this.props.lists;
        }
        this.state = {
            AppID: this.props.AppID,
            value: this.props.value,
            lists: roleList,
            pageLists: this.loadPageData(roleList),
            listSiteData: [],
            radioCheckValue: "",
            current: 1,
            selectedKeys: []
        }
        this.searchValue = null;
        this.columns = [
            {
                title: this.props.ListType === ContentListType.LibraryList ? this.format({ id: ListLookupLocale.LibraryColumnTitle }) : this.format({ id: ListLookupLocale.TableColumnTitle }),
                key: "Title",
                dataIndex: "Title"
            }
        ]
    }

    componentDidMount() {
        AkGlobal.store.dispatch(ContentListAction.requestList(this.state.AppID, null, this.props.ListType));
    }

    componentWillReceiveProps(nextProps) {
        if ("lists" in nextProps && this.props.lists !== nextProps.lists) {
            let rolesLists = [];
            if (typeof this.props.filterlistID === "string") {
                rolesLists = nextProps.lists.filter(i => i.ListID !== this.props.filterlistID);
            } else {
                rolesLists = nextProps.lists;
            }
            if (nextProps.isShowWriteRoleList) {
                rolesLists = rolesLists.filter(i=> i.MaxRole >= ContentListRightTypeEnum.Write);
            }
            this.setState({ lists: rolesLists,pageLists: this.loadPageData(rolesLists)});
        }
    }


    listSelectChange(selectRowKey, selectedRows) {
        const { props: { onChange } } = this;
        //未做多选处理
        const listSelectRowKeys = selectRowKey.map(item => {
            return selectedRows.find(i => i.ListID === item)
        })
        this.setState({ value: listSelectRowKeys[0] });
        onChange && onChange(listSelectRowKeys[0]);
    }

    renderTable() {
        const { props: { }, state: { value, lists, pageLists, selectedKeys } } = this;
        return <div className="contentList-select-tree">
            <p className="tree-title">{this.format({ id: ListLookupLocale.TableColumnTitle })}</p>
            <AkTree
                selectedKeys={selectedKeys}
                onExpand={(expandedKeys, data) => { this.onExpand(expandedKeys, data) }}
                className="terr-content"
                autoExpandParent={false}
                onSelect={(selectedKeys, item) => { this.onSelect(selectedKeys, item) }}
            >
                {this.renderTreeNode(pageLists)}
            </AkTree>
            {lists.length > 0 ?
                <AkPagination className="pageination" onChange={(page, pageSize) => { this.pageOnchange(page, pageSize) }} size="small" defaultCurrent={1} current={this.state.current} defaultPageSize={this.defaultPageSize} total={lists.length} />
                : null}
        </div>

    }

    //点击展开树的事件
    onExpand = (expandedKeys, data) => {
        const { listSets, ListType } = this.props;
        if (listSets[CUSTOMTYPE_PREFIX + expandedKeys[expandedKeys.length - 1]]) {
            return;
        }
        //点击展开时
        if (!data.expanded) {
            return;
        }
        AkGlobal.store.dispatch(ListSetAction.requestListsInListSet(data.node.props.data.AppID, CUSTOMTYPE_PREFIX + expandedKeys[expandedKeys.length - 1], false));
    }

    //树的点击事件
    onSelect = (selectedKeys, item) => {
        let lists = item.node.props.data;
        let datas = [];
        if (lists.Type !== ContentListType.ListSet) {
            this.setState({ radioCheckValue: lists.ListID, selectedKeys: selectedKeys });
            datas.push(lists);
            this.listSelectChange(selectedKeys, datas);
        }
        if (selectedKeys.length === 0) {
            this.setState({ radioCheckValue: "" });
        }
    }

    getPageData(page) {
        const { defaultPageSize, state: { lists } } = this;
        let datas = lists.slice(((page - 1) * defaultPageSize), defaultPageSize + ((page - 1) * defaultPageSize));

        return datas;
    }

    loadPageData(list) {
        const { defaultPageSize, props: { ListType } } = this;
        let datas = list.slice(0, defaultPageSize);

        return datas;
    }

    loadRenderLists() {

    }

    //分页点击事件
    pageOnchange = (page, pageSize) => {
        this.setState({ pageLists: this.getPageData(page), current: page });
    }

    //渲染树形结构
    renderTreeNode(lists) {
        const { listSets, ListType } = this.props;
        return lists.map((item, index) => {
            if (item.Type === ContentListType.ListSet) {
                let listSetLists = [];
                if (listSets[CUSTOMTYPE_PREFIX + item.ListID]) {
                    listSetLists = listSets[CUSTOMTYPE_PREFIX + item.ListID].lists;
                }
                if (listSetLists.length > 0 && ListType && ListType - ContentListType.ListSet > 0) {
                    let datas = listSetLists.filter(i => i.Type === (ListType - ContentListType.ListSet));
                    listSetLists = datas ? datas : [];
                }

                return <AkTree.TreeNode disableCheckbox title={item.Title} key={item.ListID} data={item}>
                    {this.renderTreeNode(listSetLists)}
                </AkTree.TreeNode>
            } else {
                return <AkTree.TreeNode
                    isLeaf
                    title={
                        <AkRadio checked={this.state.radioCheckValue === item.ListID}>
                            {item.Title}
                        </AkRadio>
                    }
                    key={item.ListID}
                    data={item} />
            }
        });
    }

    tagClosed() {
        const { state: { value, radioCheckValue }, props: { onChange } } = this;
        //没做多选处理
        //value.ListID = null;
        this.setState({ value: {}, radioCheckValue: "", selectedKeys: [] });
        onChange && onChange({});
    }

    renderTagDisplay() {
        const { state: { value } } = this;
        return value && value.ListID && <AkTag
            closable={true}
            key={value.ListID}
            afterClose={() => this.tagClosed()}>
            {value.Title}
        </AkTag>
    }

    onAppSelectHandler(v) {
        const { state: { value }, props: { onChange } } = this;
        const newValues = { AppID: v || this.state.AppID };
        this.setState({ value: newValues, AppID: v || this.state.AppID, current: 1 });
        onChange && onChange(newValues);
        AkGlobal.store.dispatch(ContentListAction.requestList(v || this.state.AppID, this.searchValue, this.props.ListType));

        this.getPageData(1);
    }

    onListSearchHandler(v) {
        //const { state: { value } } = this;
        let AppId = this.state.value.AppID ? this.state.value.AppID : this.props.AppID;
        this.searchValue = v;
        this.setState({ searchValue: this.searchValue, current: 1 });
        AkGlobal.store.dispatch(ContentListAction.requestList(AppId, v, this.props.ListType));
    }

    render() {
        const { state: { value, searchValue, AppID } } = this;
        return <div className="ak-identity-picker-wrapper ak-lookup-picker-warpper">
            <AkRow gutter={16}>
                <AkCol span={12}>
                    <AkLookupAppSelect value={AppID} onChange={(v) => this.onAppSelectHandler(v)} />
                </AkCol>
                <AkCol span={12}>
                    <AkSearch
                        value={searchValue}
                        placeholder={this.props.ListType === ContentListType.LibraryList ? this.format({ id: ListLookupLocale.SearchLibraryListPlaceholder }) : this.format({ id: ListLookupLocale.SearchListPlaceholder })}
                        onChange={v => this.setState({ searchValue: v.target.value })}
                        onSearch={v => this.onListSearchHandler(v)} />
                </AkCol>
            </AkRow>
            <AkRow style={{ marginTop: 5 }}>
                {this.renderTable()}
            </AkRow>
            <div className="tag-container">
                {this.renderTagDisplay()}
            </div>
        </div>;
    }
}