import * as React from "react";
import { connect } from "react-redux";
import { AkDatetimeUtil } from "../../util/datetime";
import { AkContext } from "../../util";
import moment = require("moment");

export interface AkDateLabelProps {
    format?: string;
    value?: string;
    processTZ?: boolean;
    className?: string;
    style?: React.CSSProperties;
    baseInfo?: CommonUserBaseInfo;
    withoutTime?: boolean;
}


export interface AkDateLabelState {
    value?: string;
}
@connect((state) => ({ baseInfo: state.commonBaseInfo.baseInfo }))
export class AkDateLabel extends React.Component<AkDateLabelProps, AkDateLabelState> {
    static defaultProps: AkDateLabelProps = {
        // format: AkDatetimeUtil.defaultFormatShort,
        processTZ: false,
    };

    private hasTime: boolean;

    constructor(props: AkDateLabelProps, context) {
        super(props, context);

        this.hasTime = !props.withoutTime && AkDatetimeUtil.hasTime(props.format || props.baseInfo.DateFormat + " HH:mm" || AkDatetimeUtil.defaultFormatShort, props.value);
        this.state = {
            value: this.processValue(props.value, props.format),
        };
    }

    componentWillReceiveProps(nextProps: AkDateLabelProps) {
        let shouldProcess: boolean = false;

        if ("format" in nextProps && nextProps.format !== this.props.format) {
            const hasTime = !nextProps.withoutTime && AkDatetimeUtil.hasTime(nextProps.format);
            shouldProcess = hasTime === this.hasTime;
            this.setState({ value: this.processValue(nextProps.value, nextProps.format) });
            if (!shouldProcess) this.hasTime = hasTime;
        }

        if (shouldProcess || ("value" in nextProps && nextProps.value !== this.props.value)) {
            this.setState({ value: this.processValue(nextProps.value, nextProps.format) });
        }
    }

    processValue(value, format): string {
        format = format || (this.hasTime ? this.props.baseInfo.DateFormat + " HH:mm" : this.props.baseInfo.DateFormat);

        if (!value) return value;
        if (value === "0001/1/1 0:00:00") return "";

        let isPM = false;
        if (value.indexOf("AM") > -1) {
            value = value.replace(/AM/, "");
            //value = AkDatetimeUtil.parseDate(new Date(value));
        } else if (value.indexOf("PM") > -1) {
            value = value.replace(/PM/, "");
            isPM = true;
            //const hours = new Date(value).getHours();
            //const parIntValue = new Date(value).setHours(hours + 12);
            //value = AkDatetimeUtil.parseDate(new Date(parIntValue));
        }

        // if (!this.hasTime) {
        //     format = format.substring(0, 10);
        // }
        let momentObj: moment.Moment;
        if (this.hasTime || this.props.processTZ) {
            momentObj = AkDatetimeUtil.toUserTime(value);
            if (momentObj && isPM) {
                momentObj.add(12, 'hours');
            }
        } else {
            momentObj = AkDatetimeUtil.toMoment(value);
        }

        if (value && !momentObj) {
            //如果value有值，但moment不存在，返回value原本内容
            return value;
        }

        return AkDatetimeUtil.toValue(momentObj, format);
    }

    render() {
        const { className, style } = this.props;

        return <span className={className} style={style}>{this.state.value}</span>;
    }
}
