import * as React from "react";
import {Component} from "react";
import {Button} from "antd";

export declare type ButtonType = 'primary' | 'ghost' | 'dashed' | 'danger';
export declare type ButtonShape = 'circle' | 'circle-outline';
export declare type ButtonSize = 'small' | 'default' | 'large';

export interface AkButtonProp {
    type?: ButtonType;
    htmlType?: string;
    icon?: string;
    shape?: ButtonShape;
    size?: ButtonSize;
    onClick?: React.FormEventHandler<any>;
    onMouseUp?: React.FormEventHandler<any>;
    onMouseDown?: React.FormEventHandler<any>;
    // onKeyPress?: React.KeyboardEventHandler<any>;
    // onKeyDown?: React.KeyboardEventHandler<any>;
    // tabIndex?: number;
    loading?: boolean | {
        delay?: number;
    };
    disabled?: boolean;
    style?: React.CSSProperties;
    prefixCls?: string;
    className?: string;
    ghost?: boolean;
    // target?: string;
    // href?: string;
    // download?: string;
}

export interface AkButtonState {}

export class AkButton extends Component < AkButtonProp,
AkButtonState > {
    static Group : any = Button.Group;
    constructor(props, context) {
        super(props, context);
    }

    render() {
        const {onMouseDown, ...others} = this.props;
        let obj = {...others};
        if (onMouseDown) {
            obj["onMouseDown"] = onMouseDown;
        }

        return <Button {...obj}/>;
    }
}
