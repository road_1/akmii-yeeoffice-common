// import * as React from "react";
// import {Component, ReactNode} from "react";
// import {Editor, EditorState, RichUtils} from "draft-js";
// import {EditorDecorator, AkRtUtil, ENTITY_TYPE, AkFormatTypeConstants} from "./ak-rt-util";
// import {AkButton} from "../ak-button";
// import {AkPopover} from "../ak-popover";
// import {AkInput} from "../ak-input";

// export interface AkRichTextProp {
//     getLinkPopoverContent?: (value: string) => ReactNode
//     onLinkUrlConfirm?: () => string; //必须和getLinkPopoverContent一起提供
// }

// export interface AkRichTextState {
//     editorState?: EditorState;
//     urlPopoverVisible?: boolean;
//     urlPopoverValue?: string;
// }

// export class AkRichText extends Component < AkRichTextProp,
//     AkRichTextState > {
//     static defaultProps: AkRichTextProp = {}


//     constructor(props, context) {
//         super(props, context);

//         this.state = {editorState: EditorState.createEmpty(EditorDecorator)};
//     }

//     editor;

//     editorStateChange(v) {
//         this.setState({editorState: v});
//     }

//     onFormatClick(type) {
//         const {editorState} = this.state;
//         let newEditor;
//         switch (type) {
//             case AkFormatTypeConstants.BOLD:
//             case AkFormatTypeConstants.ITALIC:
//             case AkFormatTypeConstants.UNDERLINE:
//             case AkFormatTypeConstants.STRIKETHROUGH:
//                 newEditor = RichUtils.toggleInlineStyle(editorState, type);
//                 break;
//             case AkFormatTypeConstants.BLOCK_LIST:
//             case AkFormatTypeConstants.BLOCK_LIST_ORDERED:
//             case AkFormatTypeConstants.BLOCK_QUOTE:
//             case AkFormatTypeConstants.BLOCK_UNSTYLED:
//                 newEditor = RichUtils.toggleBlockType(editorState, type);
//                 break;
//         }
//         this.editorStateChange(newEditor);
//     }

//     onUndoClick(editorState) {
//         this.editorStateChange(EditorState.undo(editorState));
//     }

//     onRedoClick(editorState) {
//         this.editorStateChange(EditorState.redo(editorState));
//     }

//     onAddLink(editorState) {
//         const entity = AkRtUtil.getEntity(editorState);
//         let url;
//         if (entity) {
//             url = entity.getData().url;
//         }
//         this.setState({urlPopoverVisible: true, urlPopoverValue: url});
//     }

//     onSetLink(editorState) {
//         const {onLinkUrlConfirm} = this.props;
//         const extUrl = onLinkUrlConfirm ? onLinkUrlConfirm() : this.state.urlPopoverValue;

//         if (extUrl && extUrl.trim().length > 0) {
//             let url = extUrl.trim();

//             const contentState = editorState.getCurrentContent();
//             const contentStateWithEntity = contentState.createEntity(
//                 ENTITY_TYPE.LINK, 'MUTABLE', {url: url}
//             );
//             const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
//             const newEditorState = EditorState.set(editorState, {currentContent: contentStateWithEntity});
//             this.setState({urlPopoverVisible: false});
//             this.editorStateChange(RichUtils.toggleLink(
//                 newEditorState,
//                 newEditorState.getSelection(),
//                 entityKey
//             ));
//             this.focusEditor();
//         }
//     }

//     focusEditor() {
//         // Hacky: Wait to focus the editor so we don't lose selection.
//         setTimeout(() => {
//             this.editor.focus();
//         }, 50);
//     }

//     onRemoveLink(editorState) {
//         //   const entity = getEntity(editorState);

//         // const selection = editorState.getSelection();
//         // if (!selection.isCollapsed()) {
//         //     this.editorStateChange(RichUtils.toggleLink(editorState, selection, null));
//         // }
//         let entity = AkRtUtil.getEntityDescAtCursor(editorState);
//         if (entity != null) {
//             let {blockKey, startOffset, endOffset} = entity;
//             this.editorStateChange(AkRtUtil.clearEntityForRange(editorState, blockKey, startOffset, endOffset));
//         }
//     }

//     onExprAdvVisibleChange(visible) {
//         this.setState({urlPopoverVisible: !visible});
//     }

//     /**
//      * 加载默认的url输入或者传入的
//      * @returns {any}
//      */
//     renderPopoverContent() {
//         const {getLinkPopoverContent} = this.props;
//         const {urlPopoverValue} = this.state;
//         if (getLinkPopoverContent) {
//             return getLinkPopoverContent(urlPopoverValue);
//         } else {
//             return <AkInput value={urlPopoverValue} onChange={v=>{this.setState({urlPopoverValue:v})}}/>;
//         }
//     }

//     renderPopover() {
//         const {editorState} = this.state;
//         return <div>
//             {this.renderPopoverContent()}
//             <AkButton size="small" type="primary" onClick={()=>this.onSetLink(editorState)}>Confirm</AkButton>
//             <AkButton size="small" type="ghost" onClick={()=>this.onExprAdvVisibleChange(true)}>Cancel</AkButton></div>
//     }

//     renderControls(editorState) {
//         const {urlPopoverVisible} = this.state;

//         let canUndo = editorState.getUndoStack().size !== 0;
//         let canRedo = editorState.getRedoStack().size !== 0;

//         let selection = editorState.getSelection();
//         let entity = AkRtUtil.getEntity(editorState);
//         let hasSelection = !selection.isCollapsed();
//         let isCursorOnLink = (entity != null && entity.getType() === ENTITY_TYPE.LINK);
//         let shouldShowLinkButton = hasSelection || isCursorOnLink;

//         return  <div className="ak-rt-controls">
//             <AkButton.Group>
//                 <AkButton icon="bold" onClick={()=>this.onFormatClick(AkFormatTypeConstants.BOLD)}/>
//                 <AkButton icon="italic" onClick={()=>this.onFormatClick(AkFormatTypeConstants.ITALIC)}/>
//                 <AkButton icon="strikethrough" onClick={()=>this.onFormatClick(AkFormatTypeConstants.STRIKETHROUGH)}/>
//                 <AkButton icon="underline" onClick={()=>this.onFormatClick(AkFormatTypeConstants.UNDERLINE)}/>
//             </AkButton.Group>

//             <AkButton.Group>
//                 <AkButton icon="list" onClick={()=>this.onFormatClick(AkFormatTypeConstants.BLOCK_LIST)}/>
//                 <AkButton icon="listnumbered"
//                           onClick={()=>this.onFormatClick(AkFormatTypeConstants.BLOCK_LIST_ORDERED)}/>
//                 <AkButton icon="quotesleft" onClick={()=>this.onFormatClick(AkFormatTypeConstants.BLOCK_QUOTE)}/>
//                 <AkButton icon="blocked" onClick={()=>this.onFormatClick(AkFormatTypeConstants.BLOCK_UNSTYLED)}/>
//             </AkButton.Group>

//             <AkButton.Group>
//                 <AkPopover overlayClassName="ak-rt-urlinput" content={this.renderPopover()} title="URL"
//                            visible={urlPopoverVisible}><AkButton icon="link" disabled={!shouldShowLinkButton}
//                                                                  onClick={()=>this.onAddLink(editorState)}/></AkPopover>
//                 <AkButton icon="clearlink" disabled={!isCursorOnLink} onClick={()=>this.onRemoveLink(editorState)}/>
//             </AkButton.Group>

//             <AkButton.Group>
//                 <AkButton icon="undo2" disabled={!canUndo} onClick={()=>this.onUndoClick(editorState)}/>
//                 <AkButton icon="redo2" disabled={!canRedo} onClick={()=>this.onRedoClick(editorState)}/>
//             </AkButton.Group>
//         </div>;
//     }

//     render() {
//         const {editorState} = this.state;
//         // return <div className="ak-rt">
//         //     {this.renderControls(editorState)}
//         //     <Editor ref={ref=>this.editor = ref} editorState={editorState}
//         //             onChange={this.editorStateChange.bind(this)}/></div>;
//         return null;
//     }
// }
