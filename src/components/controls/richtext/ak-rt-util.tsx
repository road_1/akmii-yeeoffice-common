// import {
//     EntityInstance,
//     EditorState,
//     Entity,
//     ContentBlock,
//     CharacterMetadata,
//     ContentState,
//     CompositeDecorator
// } from "draft-js";
// import * as React from "react";

// export const ENTITY_TYPE = {
//     LINK: 'LINK',
//     IMAGE: 'IMAGE',
// };

// export type EntityDescription = {
//     entityKey: string;
//     blockKey: string;
//     startOffset: number;
//     endOffset: number;
// };

// export class AkFormatTypeConstants {
//     static BOLD = "BOLD";
//     static ITALIC = "ITALIC";
//     static UNDERLINE = "UNDERLINE";
//     static STRIKETHROUGH = "STRIKETHROUGH";

//     static BLOCK_LIST = "unordered-list-item";
//     static BLOCK_LIST_ORDERED = "ordered-list-item";
//     static BLOCK_QUOTE = "blockquote";
//     static BLOCK_UNSTYLED = "unstyled";
// }

// export class AkRtUtil {
//     static getEntity(editorState: EditorState): EntityInstance {
//         const selection = editorState.getSelection();
//         const contentState = editorState.getCurrentContent();
//         const startKey = selection.getStartKey();
//         const startOffset = selection.getStartOffset();
//         const blockWithLinkAtBeginning = contentState.getBlockForKey(startKey);
//         const linkKey = blockWithLinkAtBeginning.getEntityAt(startOffset);
//         return linkKey ? contentState.getEntity(linkKey) : null;
//     }

//     static getEntityAtCursor(editorState: EditorState): Entity|null {
//         let entity = AkRtUtil.getEntityDescAtCursor(editorState);
//         return entity ? Entity.get(entity.entityKey) : null;
//     }

//     static getEntityDescAtCursor(editorState: EditorState): EntityDescription|null {
//         let selection = editorState.getSelection();
//         let startKey = selection.getStartKey();
//         let startBlock = editorState.getCurrentContent().getBlockForKey(startKey);
//         let startOffset = selection.getStartOffset();
//         if (selection.isCollapsed()) {
//             // Get the entity before the cursor (unless the cursor is at the start).
//             return AkRtUtil.getEntityDescAtOffset(startBlock, startOffset === 0 ? startOffset : startOffset - 1);
//         }
//         if (startKey !== selection.getEndKey()) {
//             return null;
//         }
//         let endOffset = selection.getEndOffset();
//         let startEntityKey = startBlock.getEntityAt(startOffset);
//         for (let i = startOffset; i < endOffset; i++) {
//             let entityKey = startBlock.getEntityAt(i);
//             if (entityKey == null || entityKey !== startEntityKey) {
//                 return null;
//             }
//         }
//         return {
//             entityKey: startEntityKey,
//             blockKey: startBlock.getKey(),
//             startOffset: startOffset,
//             endOffset: endOffset,
//         };
//     }

//     static getEntityDescAtOffset(block: ContentBlock, offset: number): EntityDescription|null {
//         let entityKey = block.getEntityAt(offset);
//         if (entityKey == null) {
//             return null;
//         }
//         let startOffset = offset;
//         while (startOffset > 0 && block.getEntityAt(startOffset - 1) === entityKey) {
//             startOffset -= 1;
//         }
//         let endOffset = offset;

//         let blockLength = block.getLength();
//         while (endOffset < blockLength && block.getEntityAt(endOffset + 1) === entityKey) {
//             endOffset += 1;
//         }
//         return {
//             entityKey,
//             blockKey: block.getKey(),
//             startOffset,
//             endOffset: endOffset + 1,
//         };
//     }

//     static clearEntityForRange(editorState: EditorState,
//                                blockKey: string,
//                                startOffset: number,
//                                endOffset: number,): EditorState {
//         let contentState = editorState.getCurrentContent();
//         let blockMap = contentState.getBlockMap();
//         let block = blockMap.get(blockKey);
//         let charList = block.getCharacterList();
//         let newCharList = charList.map((char: CharacterMetadata, i) => {
//             if (i >= startOffset && i < endOffset) {
//                 return CharacterMetadata.applyEntity(char, null);
//             } else {
//                 return char;
//             }
//         });
//         let newBlock = block.set('characterList', newCharList);
//         let newBlockMap = blockMap.set(blockKey, newBlock as ContentBlock);
//         let newContentState = contentState.set('blockMap', newBlockMap);
//         return EditorState.push(editorState, newContentState as ContentState, 'apply-entity');
//     }


//     static findLinkEntities(contentBlock: ContentBlock, callback: (start: number, end: number) => void, contentState) {
//         contentBlock.findEntityRanges(
//             (character) => {
//                 const entityKey = character.getEntity();
//                 return (
//                     entityKey !== null &&
//                     contentState.getEntity(entityKey).getType() === ENTITY_TYPE.LINK
//                 );
//             },
//             callback
//         );
//     }
// }

// const Link = (props) => {
//     const {url} = props.contentState.getEntity(props.entityKey).getData();
//     return (
//         <a href={url}>
//             {props.children}
//         </a>
//     );
// };

// export const EditorDecorator = new CompositeDecorator([
//     {
//         strategy: AkRtUtil.findLinkEntities,
//         component: Link,
//     },
// ]);







