import * as React from "react";
import { Component, ReactNode } from "react";
import * as classNames from "classnames";
import { AkTag, AkButton, AkSpin, AkTooltip } from "./";
import { AkUtil } from "../../util/util";
import { AkGlobal } from "../../util/common";
import { CommonLocale } from "../../locales/localeid";
import { AkAutoComplete } from "./ak-autocomplete";
import { AkDictionaryString, ContentListField } from "../../index";
import { AkLookupRenderControl } from './lookup/AkLookupRenderControl';

/**
 * Created by franklu on 12/30/16.
 */

export interface AkTagValueDescriber {
    id?: string | ((record) => string); //id field or function return id value
    label?: string | ((record) => string | ReactNode); //label field or function return id value
    tooltip?: string | ((record) => string | ReactNode)//tooltip
}

export interface DataHolder {
    id?: string,
    label?: string,
    data?: any;
    tooltip?: string;
}

const enum UserStatus {
    Enabled = 1,//启用
    Disabled = 2,//禁用
    Deleted = 4//删除
}

export interface DataHolderDict {
    [key: string]: DataHolder;
}

export interface AkTagInputProp {
    className?: string; //additional class
    multiple?: boolean; //is allow multiple selection
    maxSelection?: number; //how many identity selection allowed
    dropdownMatchSelectWidth?: boolean;
    dropdownStyle?: React.CSSProperties; //下拉菜单样式
    maxDisplay?: number; //how many identity will be displayed on control
    displayAll?: boolean; //是否显示全部选项
    displayInput?: boolean; //是否显示输入框
    nameDisplayLength?: number; //超出长度的名称会被截取
    style?: React.CSSProperties; //additional style apply to control
    placeholder?: string; //输入默认的提示语
    defaultValue?: any | any[];
    value?: any | any[];
    valueDescriber?: AkTagValueDescriber;
    readonly?: boolean;
    autoCollapse?: boolean; //展开隐藏内容后，鼠标离开是否自动收起
    onChange?: (value: any[], dict: any) => void; //onchange事件，将选中的tag输出
    onInputChange?: (keyword: string) => Promise<any[]>; //输入控件变更事件，返回搜索结果
    hideInputOnMaxSelectReached?: boolean;
    advanceIcon?: string;
    advanceIconClick?: (value: any[]) => void; //如果不传则无高级选项
    showSpin?: boolean; //初始化完成之前显示spin
    clearSearchInput?: boolean;
    delay?: number; //搜索延迟
    openLushDisplay?: boolean;//是否开启根据value的类型进行相应的显示；（需和listFieldDict属性一起使用）
    listFieldDict?: ContentListField[];
}

export interface AkTagInputState {
    displayAll?: boolean;
    value?: DataHolderDict;
    searchResult?: DataHolder[];
    inputValue?: string;
    hideInput?: boolean;
}

export class AkTagInput extends Component<AkTagInputProp,
    AkTagInputState> {
    static defaultProps: AkTagInputProp = {
        showSpin: false,
        advanceIcon: "organization",
        multiple: false,
        maxSelection: 200,
        maxDisplay: 5,
        nameDisplayLength: 20,
        readonly: false,
        displayAll: false,
        displayInput: true,
        autoCollapse: false,
        hideInputOnMaxSelectReached: true,
        valueDescriber: { id: 'id', label: 'label', tooltip: "tooltip" },
        clearSearchInput: true,
    }

    // inputTimeout = null;
    constructor(props, context) {
        super(props, context);

        const value = "value" in props ? props.value : props.defaultValue;

        this.state = {
            value: this.parseValue(value)
        };
    }

    getValueId(record) {
        const { valueDescriber } = this.props;
        return AkUtil.getPropByKeyFunc(record, valueDescriber.id);
    }

    getValueLabel(record) {
        const { valueDescriber } = this.props;
        return AkUtil.getPropByKeyFunc(record, valueDescriber.label);
    }
    getTooltipl(record) {
        const { valueDescriber } = this.props;
        if (valueDescriber.tooltip) {
            return AkUtil.getPropByKeyFunc(record, valueDescriber.tooltip);
        }
        return "";
    }

    parseSingle(record): DataHolder {
        let id = this.getValueId(record);
        let label = this.getValueLabel(record);
        let tooltip = this.getTooltipl(record);
        if (this.props.openLushDisplay) {
            return { id: id, label: label, data: record, tooltip: tooltip };
        } else {
            if (id && label) {
                return { id: id, label: label, data: record, tooltip: tooltip };
            }
            return null;
        }
    }

    parseValue(original): DataHolderDict {
        let rs = {};
        if (original && original !== null) {
            if (AkUtil.isArray(original)) {
                original.forEach(record => {
                    let d = this.parseSingle(record);
                    if (d) {
                        rs[d.id] = d;
                    }
                })
            } else {
                let d = this.parseSingle(original);
                if (d) {
                    rs[d.id] = d;
                }
            }
        }
        return rs;
    }

    componentWillReceiveProps(nextProps) {
        if ("value" in nextProps && nextProps.value !== this.props.value) {
            this.setState({ value: this.parseValue(nextProps.value) });
        }
        if ("value" in nextProps && (!nextProps.value || nextProps.value.length < 1)) {
            this.setState({ inputValue: null })
        }
    }

    /**
     * 将dataholder的dictionary转成数组和原始对象的dictionary
     * @param value
     * @param arr
     * @param flattenObj
     * @returns {any[]}
     */
    dictToArray(value: DataHolderDict, arr, flattenObj) {
        AkUtil.each(value, obj => {
            arr.push(obj.data);
            flattenObj[obj.id] = obj.data;
        });
    }

    changeValue(value) {
        const { onChange } = this.props;

        if (!("value" in this.props)) {
            //如果外部传入value，则value由外部控制
            this.setState({ value: value });
        }
        if (onChange) {
            let arr = [], dict = {};
            this.dictToArray(value, arr, dict)
            onChange(arr, dict);
        }
    }

    tagClosed(removedTag: DataHolder) {
        let value = this.state.value;
        delete value[removedTag.id];
        this.changeValue(value);
    }

    static AssignDefToForm(from: ContentListField) {
        if (!from) return
        const to: any = { id: AkUtil.guid() };
        const rules = from.Rules ? JSON.parse(from.Rules) : {
            "displayLabel": true
        };

        to.binding = from.FieldName;
        to.type = from.Type;
        to.label = from.DisplayName;
        to.displayLabel = rules["displayLabel"];
        to.readonly = rules["readonly"];
        to.attrs = from.Rules ? JSON.parse(from.Rules) : {};
        to.fieldID = from.FieldID;
        to.isFilter = from.IsFilter;
        to.isSort = from.IsSort;
        to.isUnique = from.IsUnique;
        to.isSystem = from.IsSystem;
        to.status = from.Status;

        // format value
        const value = from.DefaultValue;
        if (value && ((value.startsWith("[") && value.endsWith("]")) ||
            (value.startsWith("{") && value.endsWith("}")) ||
            (value.startsWith("\"") && value.endsWith("\"")))) {
            try {
                to.value = JSON.parse(value);
            } catch (error) {
                to.value = value;
            }
        } else {
            to.value = value;
        }

        return to;
    }

    /**
     * 显示选中的Identity名称
     * @returns {any[]}
     */
    getTagDisplay() {
        const { value, displayAll } = this.state;
        const { valueDescriber, openLushDisplay, listFieldDict } = this.props;
        let form;
        const keys = Object.keys(value);
        if (openLushDisplay) {
            if (listFieldDict && listFieldDict.length > 0) {
                form = AkTagInput.AssignDefToForm(listFieldDict.find(field => field.FieldName === valueDescriber.label));
            }
        }

        return (this.props.displayAll || displayAll
            ? keys
            : keys.slice(0, this.props.maxDisplay)).map((v) => {
                return this.getSingleTagDisplay(value[v], form);
            });
    }

    getSingleTagDisplay(value: DataHolder, form?) {
        const { props: { nameDisplayLength, valueDescriber, openLushDisplay } } = this;
        let islong = value.label.length > nameDisplayLength;
        if (form) form.value = value.label;

        const isDeleted = value && value.data && value.data.Attr && value.data.Attr.Status === UserStatus.Deleted;

        let label = openLushDisplay ? AkLookupRenderControl.renderList(form, "lookup") : value.label
        return <AkTooltip
            key={value.id}
            title={value.tooltip ? value.tooltip : label}>
            <AkTag className={openLushDisplay && "ak-tag-input-close"} closable={!this.props.readonly} onClose={() => this.tagClosed(value)}>
                {openLushDisplay ? label : (islong ? value.label.slice(0, nameDisplayLength) : value.label)}
                {isDeleted ? <span style={{ color: "#f00" }}>(deleted)</span> : ""}
            </AkTag>
        </AkTooltip >
    }

    /**
     * 显示更多按钮
     * @returns {any}
     */
    getMoreDisplay() {
        const { value } = this.state;
        let moreCount = Object.keys(value).length - this.props.maxDisplay;

        if (!this.props.displayAll && !this.state.displayAll && moreCount > 0) {
            return <AkButton
                style={{
                    marginRight: 8
                }}
                onClick={(e) => {
                    e.stopPropagation();
                    this.setState({ displayAll: true })
                }}
                size="small">{moreCount + ' ' + AkGlobal.intl.formatMessage({ id: CommonLocale.more })}</AkButton>;
        }
        return null;
    }

    /**
     * 如果设置了自动折叠，对应的处理逻辑
     */
    handleContainerMouseLeave() {
        if (this.props.autoCollapse && !this.props.displayAll) {
            this.setState({ displayAll: false });
        }
    }

    /**
     * 根据输入值的变化调用后端搜索api
     * @param value 当前输入的值
     */
    handleInputChange(value) {
        const { formatMessage } = AkGlobal.intl;
        const { onInputChange } = this.props;
        this.setState({ inputValue: value });
        if (onInputChange && value && value.length > 0) {
            onInputChange(value).then(ds => {
                let rs = [];
                if (ds) {
                    ds.forEach(d => {
                        let o = this.parseSingle(d);
                        if (o) {
                            rs.push(o);
                        }
                    });
                }

                if (rs.length === 0) {
                    rs.push({ label: formatMessage({ id: CommonLocale.AutoCompleteNoResult }) })
                }

                this.setState({ searchResult: rs });
                // if ((!searchResult || searchResult.length === 0) && rs.length === 0) {
                //     //搜索结果和现有结果都是空的时候不更新。
                // } else {
                //     this.setState({ searchResult: rs });
                // }
            });
        }
    }

    onAutocompleteSelect(value) {
        var fs = this.state.searchResult.find(sr => {
            return sr.id === value;
        });

        if (fs) {
            this.addValue(fs);
            if (this.props.clearSearchInput) {
                this.setState({ searchResult: [], inputValue: "" });
            }
        }

        // this.setState({searchResult: []});
    }

    addValue(value: DataHolder) {
        const { maxSelection, multiple } = this.props;
        let max = multiple ? maxSelection : 1;
        let v = this.state.value;
        if (!v.hasOwnProperty(value.id)) {
            if (max > 0 && Object.keys(v).length >= max) {
                if (max === 1) {
                    v = {};
                    v[value.id] = value;
                    this.changeValue(v);
                }
            } else {
                v[value.id] = value;
                this.changeValue(v);
            }
        }
    }

    getInputDisplay() {
        const { formatMessage } = AkGlobal.intl;
        const { placeholder, readonly, hideInputOnMaxSelectReached, advanceIcon, advanceIconClick, delay, maxSelection, multiple, dropdownMatchSelectWidth, dropdownStyle, displayInput } = this.props;
        const { searchResult, inputValue, hideInput, value } = this.state;
        let max = multiple ? maxSelection : 1;
        if (readonly || (hideInputOnMaxSelectReached && max > 0 && Object.keys(value).length >= max) || !displayInput) {
            //readonly or hide input on reached max selection
            return undefined;
        } else {
            return <AkAutoComplete
                dropdownMatchSelectWidth={dropdownMatchSelectWidth}
                dropdownStyle={dropdownStyle}
                value={inputValue}
                iconType={advanceIconClick ? advanceIcon : null}
                iconClick={advanceIconClick}
                showSearch={true}
                placeholder={placeholder ? placeholder : formatMessage({ id: CommonLocale.KeywordPlaceHolder })}
                dataSource={searchResult ? searchResult.map(d => { return { value: d.id, text: d.label } }) : []}
                onInputChange={value => this.handleInputChange(value)}
                onSelect={this.onAutocompleteSelect.bind(this)} delay={delay} />;
        }
    }

    render() {
        const { showSpin, className, readonly } = this.props;

        const cl = classNames('ak-tag-picker', className,
            { 'ak-tag-picker-disabled': readonly });

        return <div
            className={cl}
            style={this.props.style}
            onMouseLeave={this
                .handleContainerMouseLeave
                .bind(this)}>
            <AkSpin size="small" spinning={showSpin}>
                {this.getTagDisplay()
                }
                {this.getMoreDisplay()
                }
                {this.getInputDisplay()
                }
            </AkSpin>
        </div>;
    }
}
