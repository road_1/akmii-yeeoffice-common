import * as React from 'react'
import {Component} from 'react'
import {Spin} from 'antd';
export declare type AkSpinIndicator = React.ReactElement<any>;
export interface AkSpinProps {
    prefixCls?: string;
    className?: string;
    spinning?: boolean;
    style?: React.CSSProperties;
    size?: 'small' | 'default' | 'large';
    tip?: string;
    delay?: number;
    wrapperClassName?: string;
    // indicator?: AkSpinIndicator;
}
export interface AkSpinStates { }
export class AkSpin extends Component<AkSpinProps,
    AkSpinStates> {
    render() {
        return <Spin {...this.props}></Spin>
    }
}
class AkSpinStyle { }
