// import * as React from "react";
// import { Component } from "react";
// import { DatePicker } from "antd";
// import * as moment from "moment";
// import { AkTimePickerProps } from "./ak-timepicker";

// export interface AkPickerProps {
//     prefixCls?: string;
//     inputPrefixCls?: string;
//     format?: string;
//     disabled?: boolean;
//     allowClear?: boolean;
//     className?: string;
//     style?: React.CSSProperties;
//     popupStyle?: React.CSSProperties;
//     locale?: any;
//     size?: 'large' | 'small' | 'default';
//     getCalendarContainer?: (triggerNode: Element) => HTMLElement;
//     open?: boolean;
//     onOpenChange?: (status: boolean) => void;
//     disabledDate?: (current: moment.Moment) => boolean;
//     renderExtraFooter?: () => React.ReactNode;
// }
// export interface AkSinglePickerProps {
//     value?: string | moment.Moment;
//     defaultValue?: string | moment.Moment;
//     defaultPickerValue?: moment.Moment;
//     onChange?: (dateString: string, date: moment.Moment) => void;
// }

// export interface TimePickerProps {
//     className?: string;
//     size?: 'large' | 'default' | 'small';
//     value?: moment.Moment;
//     defaultValue?: moment.Moment;
//     open?: boolean;
//     format?: string;
//     onChange?: (time: moment.Moment, timeString: string) => void;
//     onOpenChange?: (open: boolean) => void;
//     disabled?: boolean;
//     placeholder?: string;
//     prefixCls?: string;
//     hideDisabledOptions?: boolean;
//     disabledHours?: () => number[];
//     disabledMinutes?: (selectedHour: number) => number[];
//     disabledSeconds?: (selectedHour: number, selectedMinute: number) => number[];
//     style?: React.CSSProperties;
//     getPopupContainer?: (triggerNode: Element) => HTMLElement;
//     addon?: Function;
//     use12Hours?: boolean;
//     allowEmpty?: boolean;
//     clearText?: string;
//     defaultOpenValue?: moment.Moment;
//     popupClassName?: string;
// }
// export interface AkDatePickerProps extends AkPickerProps, AkSinglePickerProps {
//     className?: string;
//     showTime?: TimePickerProps | boolean;
//     showToday?: boolean;
//     open?: boolean;
//     toggleOpen?: (e: {
//         open: boolean;
//     }) => void;
//     disabledTime?: (current: moment.Moment) => {
//         disabledHours?: () => number[];
//         disabledMinutes?: () => number[];
//         disabledSeconds?: () => number[];
//     };
//     onOpenChange?: (status: boolean) => void;
//     onOk?: (selectedTime: moment.Moment) => void;
//     placeholder?: string;
// }
// export interface AkMonthPickerProps extends AkPickerProps, AkSinglePickerProps {
//     className?: string;
//     placeholder?: string;
// }

// export declare type AkRangePickerValue = undefined[] | [moment.Moment] | [undefined, moment.Moment] | [moment.Moment, moment.Moment];
// export declare type AkRangePickerRange = AkRangePickerValue | (() => AkRangePickerValue);
// export interface AkRangePickerProps extends AkPickerProps {
//     className?: string;
//     value?: AkRangePickerValue;
//     defaultValue?: AkRangePickerValue;
//     defaultPickerValue?: AkRangePickerValue;
//     onChange?: (dates: AkRangePickerValue, dateStrings: [string, string]) => void;
//     onCalendarChange?: (dates: AkRangePickerValue, dateStrings: [string, string]) => void;
//     onOk?: (selectedTime: moment.Moment) => void;
//     showTime?: AkTimePickerProps | boolean;
//     ranges?: {
//         [range: string]: AkRangePickerRange;
//     };
//     placeholder?: [string, string];
//     mode?: string | string[];
//     disabledTime?: (current: moment.Moment, type: string) => {
//         disabledHours?: () => number[];
//         disabledMinutes?: () => number[];
//         disabledSeconds?: () => number[];
//     };
//     onPanelChange?: (value?: AkRangePickerValue, mode?: string | string[]) => void;
// }

// export interface AkDatePickerStates {
//     value?: moment.Moment;
// }
// export class AkDatePicker extends Component<AkDatePickerProps,
//     AkDatePickerStates> {
//     static RangePicker?: any = DatePicker.RangePicker;
//     static MonthPicker?: any = DatePicker.MonthPicker;

//     constructor(props, context) {
//         super(props, context);

//         let value = ("value" in props) ? props.value : props.defaultValue;
//         this.state = { value: this.processValue(value) };
//     }

//     componentWillReceiveProps(nextProps) {
//         if ("value" in nextProps && nextProps.value !== this.props.value) {
//             this.setState({ value: this.processValue(nextProps.value) });
//         }
//     }

//     processValue(value): moment.Moment {
//         let v;
//         if (value && typeof value === "string") {
//             v = moment(value);
//             if (!v.isValid()) {
//                 v = undefined;
//             }
//         } else if (typeof value === "string") {
//             v = null;
//         } else {
//             v = value;
//         }
//         return v;
//     }

//     onChange(date: moment.Moment, dateString: string) {

//         if (!("value" in this.props)) {
//             this.setState({ value: date });
//         }

//         this.props.onChange && this.props.onChange(dateString, date);
//     }

//     render() {
//         const { defaultValue, value, onChange, ...others } = this.props;
//         //others["showTime"] = showTime;
//         return <DatePicker value={this.state.value} onChange={this.onChange.bind(this)} {...others} />
//     }
// }
// class AkDatePickerStyle { }
