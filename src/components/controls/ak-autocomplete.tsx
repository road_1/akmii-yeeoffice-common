/**
 * Created by franklu on 12/30/16.
 */
import * as React from "react";
import { Component, ReactNode } from "react";
import { Select, Input } from 'antd';
import { AkButton } from "./ak-button";
import { AkIcon } from "./ak-icon";
import { AkAbstractSelectProps, AkSelect, AkSelectValue } from "./ak-select";
import { AkUtil } from "../../util/util";

export interface AkDataSourceItemValueObject {
    key: string;
    value: any;
    text: string | ReactNode;
}
export interface AkDataSourceItemObject {
    value: string;
    text: string | ReactNode;
}
export declare type AkDataSourceItemType = string | AkDataSourceItemObject | AkDataSourceItemValueObject;

export interface AkAutoCompleteProp extends AkAbstractSelectProps {
    placeholder?: string;
    value?: AkSelectValue;
    defaultValue?: AkSelectValue;
    dataSource?: AkDataSourceItemType[];
    optionLabelProp?: string;
    filterOption?: boolean | ((inputValue: string, option: Object) => any);
    onChange?: (value: AkSelectValue, option: Object) => void;
    onSelect?: (value: AkSelectValue, option: Object) => any;
    onDeselect?: (value: AkSelectValue) => any;
    onSearch?: (value: string) => any;
    dropdownMatchSelectWidth?: boolean;
    optionFilterProp?: string;
    defaultActiveFirstOption?: boolean;
    labelInValue?: boolean;
    getPopupContainer?: (triggerNode: Element) => HTMLElement;
    dropdownStyle?: React.CSSProperties;
    dropdownMenuStyle?: React.CSSProperties;
    tokenSeparators?: string[];
    getInputElement?: () => React.ReactElement<any>;
    delay?: number; //输入搜索延迟
    iconType?: string; //如果需要在右侧显示icon 对应的名称
    iconClick?: (value) => void; //icon点击事件，参数为当前的输入值
    onInputChange?: (value) => void;
    backfill?: boolean;//使用键盘选择选项的时候把选中项回填到输入框中
    children?: React.ReactElement<any> | Array<React.ReactElement<any>> | HTMLInputElement | HTMLTextAreaElement;//(自动完成的数据源)|自定义输入框;

}

export interface AkAutoCompleteState {
}

export class AkAutoComplete extends Component<AkAutoCompleteProp, AkAutoCompleteState> {
    static Option?: any = AkAutoComplete.Option;
    static OptGroup?: any = AkAutoComplete.OptGroup;

    static defaultProps: AkAutoCompleteProp = {
        delay: 300,
        placeholder: 'please input',
        getPopupContainer: AkUtil.getPopupContainer,
    }

    inputTimeout = null;
    requireSearch = true;
    value = null;
    selectedValue = null;
    selectedObjValue = null;

    constructor(props, context) {
        super(props, context);
        this.state = {};
    }

    onChange(value) {
        const { onChange, delay, onInputChange } = this.props;
        this.value = value;

        if (onChange || onInputChange) {
            // on input change
            if (onInputChange) {
                if (delay > 0) {
                    if (this.inputTimeout) {
                        window.clearTimeout(this.inputTimeout);
                    }
                    this.inputTimeout = setTimeout(() => {
                        onInputChange(value);
                    }, delay);
                } else {
                    onInputChange(value);
                }
            } else {
                this.requireSearch = true;
            }
        }
    }

    onSelect(value, option) {
        const { onSelect } = this.props;
        this.requireSearch = false;
        if (onSelect) {
            onSelect(value, option);
        }
    }

    getOptionsByDataSource(ds) {
        return ds.map((item) => {
            if (React.isValidElement(item)) {
                return item;
            }
            switch (typeof item) {
                case 'string':
                    return <AkSelect.Option key={item}>{item}</AkSelect.Option>;
                case 'object':
                    const key = "key" in item ?
                        (item as AkDataSourceItemValueObject).key : (item as AkDataSourceItemObject).value;
                    if (key === undefined) {
                        return <AkSelect.Option key={AkUtil.guid()} disabled>
                            {(item as AkDataSourceItemObject).text}
                        </AkSelect.Option>
                    } else {
                        return (
                            <AkSelect.Option key={key}>
                                {(item as AkDataSourceItemObject).text}
                            </AkSelect.Option>
                        );
                    }
                default:
                    throw new Error('AutoComplete[dataSource] only supports type `string[] | Object[]`.');
            }
        });
    }

    getInputElement() {
        // return <Input onKeyDown={e => {
        //     if (e["keyCode"] === 13) {
        //         e.stopPropagation();
        //         e.preventDefault();
        //     }
        // }} />
        return <WrappedInput anotherOnChange={v => { this.onChange(v) }} />;
    }

    render() {
        let { dataSource, filterOption, onInputChange, onChange, delay, onSelect, iconType, children, iconClick, ...others } = this.props;
        let options = dataSource ? this.getOptionsByDataSource(dataSource) : children;
        const filter = filterOption ? filterOption : false;

        return this.props.iconType ?
            <div className="ak-autocomplete-wrapper" style={this.props.style}>
                <Select mode="combobox" filterOption={filter} className="ak-autocomplete"
                    getInputElement={() => this.getInputElement()}
                    // onChange={this.onChange.bind(this)}
                    onSelect={this.onSelect.bind(this)} {...others}>
                    {options}
                </Select>
                <AkButton onClick={iconClick ? () => iconClick(this.value) : undefined}
                    className="autocomplete-button"><AkIcon type={this.props.iconType}></AkIcon></AkButton>
            </div> :
            <Select mode="combobox" filterOption={filter} className="ak-autocomplete"
                getInputElement={() => this.getInputElement()}
                // onChange={this.onChange.bind(this)}
                onSelect={this.onSelect.bind(this)} {...others}>{options}</Select>
    }
}

/**
 * 包装input，把onchange放到input这里避免select的选中重复触发onChange
 * @param props 
 */
const WrappedInput = (props) => {
    const { onChange, anotherOnChange, ...others } = props;
    return <Input onChange={e => {
        anotherOnChange && anotherOnChange(e.target.value);
        onChange && onChange(e);
    }} onKeyDown={e => {
        if (e["keyCode"] === 13) {
            e.stopPropagation();
            e.preventDefault();
        }
    }} {...others} />
}