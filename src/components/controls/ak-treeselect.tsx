import * as React from "react";
// import { TreeNode } from "rc-tree-select";
import { TreeSelect } from "antd";
import { AkAbstractSelectProps } from "./ak-select";
import { AkTreeData } from "./ak-tree";
import { AkUtil } from "../../util/util";
import { TreeSelectProps } from "antd/lib/tree-select";

export interface AkTreeSelectProps extends AkAbstractSelectProps {
    value?: string | Array<any>;
    defaultValue?: string | Array<any>;
    multiple?: boolean;
    onSelect?: (value: any) => void;
    onChange?: (value: any, label: any) => void;
    onSearch?: (value: any) => void;
    searchPlaceholder?: string;
    dropdownStyle?: React.CSSProperties;
    dropdownMatchSelectWidth?: boolean;
    treeDefaultExpandAll?: boolean;
    treeCheckable?: boolean | React.ReactNode;
    treeDefaultExpandedKeys?: Array<string>;
    filterTreeNode?: (inputValue: string, treeNode: any) => boolean | boolean;
    treeNodeFilterProp?: string;
    treeNodeLabelProp?: string;
    treeData?: Array<AkTreeData>;
    treeDataSimpleMode?: boolean | Object;
    loadData?: (node: any) => void;
    showCheckedStrategy?: "SHOW_ALL" | "SHOW_PARENT" | "SHOW_CHILD";
    labelInValue?: boolean;
    treeCheckStrictly?: boolean;
    getPopupContainer?: (triggerNode: Element) => HTMLElement;
}

export interface AkTreeSelectState {
    value?: string | Array<any>;
}

export class AkTreeSelect extends React.Component<AkTreeSelectProps, AkTreeSelectState> {
    static defaultProps: AkTreeSelectProps = {
        treeCheckable: true,
        showCheckedStrategy: "SHOW_PARENT",
        style: { width: "100%" },
        getPopupContainer: AkUtil.getPopupContainer,
    };

    // static TreeNode = TreeNode;

    shouldCache: boolean;
    multiple: boolean;
    options: string[]; // Cache all options
    removedValue: string | any[]; // Cache removed value

    constructor(props, context) {
        super(props, context);
        const value = ("value" in props) ? props.value : props.defaultValue;

        this.shouldCache = (!props.mode || props.mode === "default") && props.optionsIncludeDestroyed;
        this.multiple = props.treeCheckable || props.multiple;

        if (this.shouldCache) {
            this.options = [];
            this.cacheOptions(props);
            this.removeNotInclude(value, true);
        }

        this.state = { value };
    }

    componentWillReceiveProps(nextProps) {
        if (("children" in nextProps && nextProps.children !== this.props.children) ||
            ("treeData" in nextProps && nextProps.treeData !== this.props.treeData)) {
            if (this.shouldCache) {
                this.options = [];
                this.cacheOptions(nextProps);
                this.removeNotInclude(nextProps.value, true);
            }
        }

        if ("value" in nextProps && nextProps.value !== this.props.value) {
            if (this.shouldCache) this.removeNotInclude(nextProps.value, true);
            this.setState({ value: nextProps.value });
        }
    }

    cacheOptions(props) {
        if (props.treeData) {
            this.loopTreeData(props.treeData);
        } else {
            this.loopChildren(props.children);
        }
    }

    loopTreeData(data: AkTreeData[]) {
        if (data) {
            AkUtil.each(data, (item: AkTreeData) => {
                this.options.push(item.value);
                this.loopTreeData(item.children);
            });
        }
    }

    loopChildren(children) {
        if (children) {
            React.Children.forEach(children, (child: any) => {
                this.options.push(child.props.value);
                this.loopChildren(child.props.children);
            });
        }
    }

    removeNotInclude(value: any, delay: boolean = false) {
        if (!this.shouldCache) return;

        let needUpdate = false;
        const newValue = [];

        if (this.removedValue) {
            AkUtil.each(AkUtil.toArray(this.removedValue), (item) => {
                const optionValue = item;
                if (this.options && this.options.indexOf(optionValue) !== -1) {
                    needUpdate = true;
                    newValue.push(item);
                }
            });
        } else {
            AkUtil.each(AkUtil.toArray(value), (item) => {
                const optionValue = item;
                if (this.options && this.options.indexOf(optionValue) === -1) {
                    needUpdate = true;
                } else {
                    newValue.push(item);
                }
            });
        }

        if (needUpdate) {
            this.removedValue = this.removedValue ? undefined : value;
            const setValue = this.multiple ? newValue : (newValue.length > 0 ? newValue[0] : undefined);
            const func = () => this.onChange(setValue, []);
            delay ? setTimeout(func) : func();
        }
    }

    onChange(value: any, labels: any, reset?: boolean) {
        if (!("value" in this.props)) this.setState({ value });
        if (reset) this.removedValue = undefined;
        this.props.onChange && this.props.onChange(value, labels);
    }

    render() {
        // tslint:disable-next-line:no-unused-variable
        const { defaultValue, value, onChange, ...others } = this.props;
        return <TreeSelects value={this.state.value} onChange={(v, labels) => this.onChange(v, labels, true)} {...others} />;
    }
}
export const TreeSelects: React.Component<TreeSelectProps, any> | any = TreeSelect;
