import * as React from 'react'
import { Progress } from 'antd'


export interface AkProgressProps {
    prefixCls?: string;
    className?: string;
    type?: 'line' | 'circle' | 'dashboard';
    percent?: number;
    // successPercent?: number;
    format?: (percent: number) => string;
    status?: 'success' | 'active' | 'exception';
    showInfo?: boolean;
    strokeWidth?: number;
    trailColor?: string;
    width?: number;
    style?: React.CSSProperties;
    gapDegree?: number;
    gapPosition?: 'top' | 'bottom' | 'left' | 'right';
    // size?: 'default' | 'small';
}
export interface AkProgressStates { }

export const AkProgress = (props) => {
    return <Progress {...props} > </Progress>
}
// export class AkProgress extends Component<AkProgressProps, AkProgressStates> {
//     render() {
//         const { } = this.props;
//         const { } = this.state;
//         return <Progress {...this.props} > </Progress>
//     }
// }