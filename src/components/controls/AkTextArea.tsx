import * as React from "react";
import { Component } from "react";
import { Input } from "antd";
import { AkIcon } from "./ak-icon";
import { AkConstants } from "../../util/common";
import { isNullOrUndefined } from "util";
import { InputProps } from "antd/lib/input/Input";
import { AkAutoSizeType, AkInputProp } from ".";


export interface AkTextAreaProp extends AkInputProp {
    id?: string;
    prefix?: string;
    maxLength?: number;
    triggerChangeOnBlur?: boolean; //当blur的时候才触发change通知
    autosize?: boolean | AkAutoSizeType;
    onPressEnter?: React.FormEventHandler<any>;
}

export interface AkTextAreaState {
    value?: string;
}

export class AkTextArea extends Component<AkTextAreaProp, AkTextAreaState> {

    constructor(props, context) {
        super(props, context);

        this.state = {
            value: ("value" in this.props)
                ? this.props.value
                : this.props.defaultValue
        };
    }

    componentWillReceiveProps(nextProps: AkTextAreaProp) {
        if ("value" in nextProps && nextProps.value !== this.props.value && nextProps.value !== this.state.value) {
            this.setState({ value: nextProps.value })
        }
    }

    onChange(e) {
        this.triggerChange(e);
    }

    triggerChange(e, onBlur?) {
        const { triggerChangeOnBlur, onChange } = this.props;
        let value = e.currentTarget.value;
        //如果是blur后通知，不需要setState
        if (!onBlur) {
            this.setState({ value: value });
        }
        if (onChange && (onBlur || !triggerChangeOnBlur)) {
            if (!("value" in this.props) || this.props.value !== value) {
                //当外部值和内部值一致时，不触发onchange
                onChange(e);
            }
        }
    }


    onBlur(e) {
        const { triggerChangeOnBlur, onBlur } = this.props;

        if (triggerChangeOnBlur) {
            this.triggerChange(e, triggerChangeOnBlur);
        }

        onBlur && onBlur(e);
    }


    render() {
        const { onBlur, onChange, triggerChangeOnBlur, defaultValue, value, ...others } = this.props;
        return <Input.TextArea
            onBlur={this.onBlur.bind(this)}
            onChange={(e) => this.onChange(e)}
            value={this.state.value}
            {...others} />
    }
}
