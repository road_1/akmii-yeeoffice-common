import * as React from "react";
import { Component } from "react";
import { Input } from "antd";
const Search = Input.Search;

export interface AkSearchProps {
    inputPrefixCls?: string;
    onSearch?: (value: string) => any;
    
    prefixCls?: string;
    className?: string;
    defaultValue?: any;
    value?: any;
    style?: React.CSSProperties;

    placeholder?: string;
    type?: string;
    id?: number | string;
    name?: string;
    size?: 'large' | 'default' | 'small';
    maxLength?: string;
    disabled?: boolean;
    readOnly?: boolean;
    // enterButton?: boolean | React.ReactNode;
    addonBefore?: React.ReactNode;
    addonAfter?: React.ReactNode;
    // onPressEnter?: React.FormEventHandler<any>;
    // onKeyDown?: React.FormEventHandler<any>;
    // onChange?: React.ChangeEventHandler<HTMLInputElement>;
    // onClick?: React.FormEventHandler<any>;
    // onFocus?: React.FormEventHandler<any>;
    // onBlur?: React.FormEventHandler<any>;
    onPressEnter?: React.FormEventHandler<HTMLInputElement>;
    onKeyDown?: React.FormEventHandler<HTMLInputElement>;
    onKeyUp?: React.FormEventHandler<HTMLInputElement>;
    onChange?: React.ChangeEventHandler<HTMLInputElement>;
    onClick?: React.FormEventHandler<HTMLInputElement>;
    onFocus?: React.FormEventHandler<HTMLInputElement>;
    onBlur?: React.FormEventHandler<HTMLInputElement>;
    autoComplete?: string;
    prefix?: React.ReactNode;
    suffix?: React.ReactNode;
    spellCheck?: boolean;
    autoFocus?: boolean;
}
export interface AkSearchStates {
}
export class AkSearch extends Component<AkSearchProps,
    AkSearchStates> {
    constructor(props, context) {
        super(props, context);
    }

    onKeyDown(e) {
        const { onKeyDown } = this.props;

        if (e.keyCode === 13) {
            e.stopPropagation();
            e.preventDefault();
        }

        onKeyDown && onKeyDown(e);
    }

    render() {
        const { onKeyDown, ...others } = this.props;
        
        return <Search onKeyDown={this.onKeyDown.bind(this)} {...others}></Search>
    }
}
