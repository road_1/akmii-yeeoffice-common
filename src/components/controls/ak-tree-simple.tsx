import * as React from "react";
import { Component } from "react";
import { AkTree, AkTreeNodeEvent, AkTreeData, AkTreeNode } from "./ak-tree";
import { AkUtil } from "../../util/util";

export interface AkTreeDataDescriber {
    id?: string | ((record) => string);
    pId?: string | ((record) => string);
    title?: string | ((record) => string);
    rootPId?: any;
}

export interface AkTreeSimpleProp {
    treeData?: AkTreeData[] | any[]; //层级的数数据 或者 简单树对象，层级关系通过parent进行维护，默认参数id, label, pId, value，不然需要额外说明
    simpleTreeDescriber?: boolean | AkTreeDataDescriber; //简单数对象的说明，用于获取id, label, pId, rootPId等值， 如果treeData符合标准字段，传true
    onCheck?: (checkedKeys: string[]) => any; //oncheck回调函数，第一个参数是所有checkedkeys，第二个是根据strategy过滤后的对象
    checkedStrategy?: 'ALL' | 'PARENT' | 'CHILD'; //check parent节点时checkkeys获取逻辑，包括父子节点，仅父节点，仅子节点
    /** 是否支持多选 */
    //multiple?: boolean;
    disableCheck?: boolean;
    /** 默认展开所有树节点 */
    defaultExpandAll?: boolean;
    /** 是否支持选中 */
    checkable?: boolean;
    /** 允许的最大check数量 */
    maxChecking?: number;
    /** 默认展开指定的树节点 */
    defaultExpandedKeys?: Array<string>;
    /** （受控）展开指定的树节点 */
    expandedKeys?: Array<string>;
    /** （受控）选中复选框的树节点 */
    checkedKeys?: Array<string>;
    /** 默认选中复选框的树节点 */
    defaultCheckedKeys?: Array<string>;
    /** （受控）设置选中的树节点 */
    selectedKeys?: Array<string>;
    /** 默认选中的树节点 */
    defaultSelectedKeys?: Array<string>;
    /** 点击树节点触发 */
    onSelect?: (selectedKeys: Array<string>, e: AkTreeNodeEvent) => void;
    style?: React.CSSProperties;
    onExpand?: (expandedKeys: Array<string>, info: {
        node: AkTreeNode;
        expanded: boolean;
    }) => void | PromiseLike<any>;

    disableItem?: (item: AkTreeData) => boolean; //单条判断是否需要disable选择的函数，返回true代表需要disable
}

export interface AkTreeSimpleState {
    treeData?: AkTreeData[];
    treeDataDict?: Object;
    checkedKeys?: string[];//记录tree checked所有keys
    disableCheck?: boolean; //是否禁用checkbox
    expandedKeys?: string[];
}

/**
 * 将简单（平级）的data转换成层级的treedata
 * @param treeData
 * @param format
 * @returns {Array}
 */
function processSimpleTreeData(treeData, format) {

    function unflatten2(array, p?) {
        let parent = p;
        if (p === undefined) {
            parent = {};
            parent[format.id] = format.rootPId;
        }

        let children = [];
        for (let i = 0; i < array.length; i++) {
            array[i] = Object.assign({}, array[i]); // copy, can not corrupts original data
            if (array[i][format.pId] === parent[format.id]) {
                array[i].key = array[i][format.id];
                children.push(array[i]);
                array.splice(i--, 1);
            }
        }
        if (children.length) {
            parent.children = children;
            children.forEach(function (child) {
                return unflatten2(array, child);
            });
        }
        if (parent[format.id] === format.rootPId) {
            return children;
        }
    }

    return unflatten2(treeData);
}

export class AkTreeSimple extends Component<AkTreeSimpleProp, AkTreeSimpleState> {

    static defaultProps: AkTreeSimpleProp = {
        treeData: [],
        defaultCheckedKeys: [],
        checkedStrategy: 'ALL',
        maxChecking: 200,
    }

    static defaultTreeDataDescriber: AkTreeDataDescriber = {
        id: "key",
        pId: "pId",
        rootPId: "0",
        title: "label"
    }

    constructor(props, context) {
        super(props, context);

        this.describer = Object.assign({}, AkTreeSimple.defaultTreeDataDescriber, this.props.simpleTreeDescriber);
        let treeData = this.parseTreeData(props.treeData);
        const dict = this.parseTreeData2Value(treeData)
        let checkedKeys = this.props.checkedKeys || this.props.defaultCheckedKeys;
        this.state = {
            treeData: treeData,
            treeDataDict: dict,
            checkedKeys: checkedKeys,
            expandedKeys: props.defaultExpandAll ? Object.keys(dict) : [],
            disableCheck: ("disableCheck" in props) ? props.disableCheck : this.props.maxChecking > 0 && checkedKeys.length >= this.props.maxChecking,
        }
    }

    describer: AkTreeDataDescriber;

    componentWillReceiveProps(nextProps: AkTreeSimpleProp) {
        let state;

        if ("checkedKeys" in nextProps && nextProps.checkedKeys !== this.props.checkedKeys) {
            state = Object.assign({}, state, { checkedKeys: nextProps.checkedKeys });
        }

        if ("disableCheck" in nextProps && nextProps.disableCheck !== this.props.disableCheck) {
            state = Object.assign({}, state, { disableCheck: nextProps.disableCheck });
        }

        if ("treeData" in nextProps && nextProps.treeData !== this.props.treeData) {
            let treeData = this.parseTreeData(AkUtil.deepClone(nextProps.treeData));
            let dict = this.parseTreeData2Value(treeData);
            if (this.props.defaultExpandAll) {
                state = Object.assign({}, state, { expandedKeys: Object.keys(dict) });
            }
            state = Object.assign({}, state, { treeData: treeData, treeDataDict: dict });
        }

        if (state) {
            this.setState(state);
        }
    }

    parseTreeData(treeData) {
        let data;
        if (this.props.simpleTreeDescriber) {
            data = processSimpleTreeData(treeData, this.describer);
        } else {
            data = treeData;
        }
        return data;
    }

    getTitle(item: AkTreeData) {
        let title;
        if (this.props.simpleTreeDescriber) {
            title = typeof this.describer.title === 'function' ? this.describer.title(item) : item[this.describer.title];
        } else {
            title = item.label;
        }
        return title;
    }

    parseTreeData2Value(treeData: AkTreeData[]) {
        let value = {}

        function tree2Value(children: AkTreeData[], value) {
            children.forEach(d => {
                value[d.key] = d;
                if (d.children) {
                    tree2Value(d.children, value);
                }
            });
        }

        tree2Value(treeData, value);
        return value;
    }

    getTreeNodes(treeData) {
        let that = this;
        const { disableItem } = this.props;

        function loop(data: AkTreeData[]) {
            return data.map((item) => {

                if (item.children) {
                    return (
                        <AkTree.TreeNode key={item.key} title={that.getTitle(item)}
                            disabled={disableItem && disableItem(item)}
                            disableCheckbox={that.state.disableCheck && (that.state.checkedKeys.indexOf(item.key) === -1) && disableItem && disableItem(item)}>
                            {loop(item.children)}
                        </AkTree.TreeNode>
                    );
                }
                return <AkTree.TreeNode key={item.key} title={that.getTitle(item)}
                    disabled={disableItem && disableItem(item)}
                    disableCheckbox={that.state.disableCheck && (that.state.checkedKeys.indexOf(item.key) === -1) && disableItem && disableItem(item)} />;
            });
        }

        return loop(treeData);
    }

    changeCheckedKeys(checkedKeys: Array<string>) {

        let state = {};

        if (!("checkedKeys" in this.props)) {
            Object.assign(state, { checkedKeys: checkedKeys });
        }

        if (!("disableCheck" in this.props)) {
            Object.assign(state, { disableCheck: this.props.maxChecking > 0 && checkedKeys.length >= this.props.maxChecking });
        }

        this.setState(state);

        if (this.props.onCheck) {
            this.props.onCheck(checkedKeys);
        }
    }

    /**
     * 根据点击策略，修正checkedKeys
     * @param checkedKeys
     * @param e
     */
    onCheck(checkedKeys: Array<string>, e: AkTreeNodeEvent) {
        function removeChildren(parent: AkTreeData, dict: Object) {
            if (parent.children) {
                parent.children.forEach(k => {
                    if (dict.hasOwnProperty(k.key)) {
                        delete dict[k.key];
                    }
                    if (k.children) {
                        removeChildren(k, dict);
                    }
                });
            }
        }

        function removeParent(parent: AkTreeData, dict: Object) {
            if (parent.children) {
                let del = false;
                parent.children.forEach(c => {
                    if (dict.hasOwnProperty(c.key)) {
                        del = true;
                    }
                    if (c.children) {
                        removeParent(c, dict);
                    }
                });
                delete dict[parent.key];
            }
        }

        let checkedDict = {};
        checkedKeys.forEach(k => {
            checkedDict[k] = this.state.treeDataDict[k];
        });

        switch (this.props.checkedStrategy) {
            case "PARENT":
                //移除所有的children
                checkedKeys.forEach(k => {
                    let obj = checkedDict[k];
                    if (obj) {
                        removeChildren(obj, checkedDict);
                    }
                });
                break;
            case "CHILD":
                //移除有children被选中的parent
                checkedKeys.forEach(k => {
                    let obj = checkedDict[k];
                    if (obj) {
                        removeParent(obj, checkedDict);
                    }
                });
                break;
            default:
        }

        let keys = Object.keys(checkedDict);

        if (this.props.maxChecking > 0 && keys.length > this.props.maxChecking) {
            //超过允许的最大选择数
            keys.splice(this.props.maxChecking, keys.length);
        }

        this.changeCheckedKeys(keys);
    }

    /**
     * 当有check逻辑是，select走check的功能
     * @param selectedKeys 
     * @param e 
     */
    onSelect(selectedKeys: Array<string>, e: AkTreeNodeEvent) {
        const { maxChecking } = this.props;
        if (e.node.props.disableCheckbox || maxChecking === 0) {
            //当前控件被禁用
            return false;
        }

        const currentSelect = e.node.props["eventKey"];

        let checkedKeys = this.state.checkedKeys;
        let newCheckedKeys = [];
        let isExist = false;

        if (maxChecking === 1) {
            newCheckedKeys.push(currentSelect);
        } else {
            AkUtil.each(checkedKeys, key => {
                if (key === currentSelect) {
                    isExist = true;
                } else {
                    newCheckedKeys.push(key);
                }
            });
            if (!isExist) {
                newCheckedKeys.push(currentSelect);
            }
        }

        this.onCheck(newCheckedKeys, null);
        this.props.onSelect && this.props.onSelect(newCheckedKeys, e);
    }

    render() {
        const { treeData, simpleTreeDescriber, onCheck, checkedStrategy, checkedKeys, disableCheck, maxChecking, onSelect, ...others } = this.props;
        const myTreeData = this.state.treeData;
        return myTreeData && myTreeData.length > 0 ?
            <AkTree defaultExpandAll={this.props.defaultExpandAll} checkedKeys={this.state.checkedKeys}
                onCheck={this.onCheck.bind(this)} multiple={maxChecking > 1} onSelect={this.onSelect.bind(this)} {...others}>{this.getTreeNodes(myTreeData)}</AkTree> :
            <span></span>;
    }
}
