import * as React from "react";
import { AkSelect } from ".";
import { AkGlobal } from "../../util";
import { LocalizationAPI } from "../../api/common/localization";
import { AkSelectProps } from "./ak-select";
import { LocalizationLocale } from "../../locales/localeid";

interface CountryItem {
    Code?: string;
    Country?: string;
}

interface AkCountryProps extends AkSelectProps {
}

interface AkCountryStates {
    countryItems?: CountryItem[];
}

export class AkCountry extends React.Component<AkCountryProps, AkCountryStates>{
    constructor(props, context) {
        super(props, context);
        this.state = {
            countryItems: []
        }
    }
    componentDidMount() {
        this.loadCountryData();
    }
    loadCountryData = () => {
        LocalizationAPI.getAllCountrys().then((data) => {
            if (data.Status === 0 && data.Data) {
                this.setState({ countryItems: data.Data });
            }
        });
    }
    onChange = (e) => {
        const { onChange } = this.props;
        e && onChange&& onChange(e)
    }
    render() {
        const { onChange, placeholder, ...others } = this.props;
        const { countryItems } = this.state;
        const { formatMessage } = AkGlobal.intl;
        return <AkSelect placeholder={placeholder ? placeholder : formatMessage({ id: LocalizationLocale.CountrySelectHd })} onChange={this.onChange}  {...others}>
            {
                countryItems.map(item => {
                    return <AkSelect.Option key={item.Code} value={item.Code}>{item.Country}</AkSelect.Option>;
                })
            }
        </AkSelect>
    }
}