import * as React from "react";
import * as classNames from "classnames";
import { connect } from "react-redux";
import { AkUser } from "../../api/identity/identity";
import { AkGlobal, AkContext } from '../../util/common';
import { IdentityAction } from "../../actions";
import { IdentityUserHolder } from "../../reducers/Identity";
import AkImg from "./ak-img";
import { AkDictionaryString } from "../../index";
import { AkPopover } from "./ak-popover";
import { AkRow } from "./ak-row";
import { AkCol } from "./ak-col";
import { AkButton } from "./ak-button";
import { SocketClient } from "../../api/masterpage/socketclient";
import { ChatTypeEnum } from "../../api/masterpage/socketmodel";

export interface AkAvatarProps {
    identityDict?: AkDictionaryString<IdentityUserHolder>;
    organizations?: any[];
    organizationLoaded?: boolean;
    value?: string;
    type?: "text" | "picture" | "combination";
    layout?: "inline" | "center";
    shape?: "circle" | "square";
    size?: number;
    attr?: "Email" | "Photo" | "Name" | "Name_EN" | "SPAccount" | "EmployeeNo" | "JobTitle" | "organization";
    className?: string;
    style?: React.CSSProperties;
    withChat?: boolean;
    addonTextAfter?: string | React.ReactNode;
    textClassName?: string;
    // size?: "default" | "large" | "small";
}

export interface AkAvatarStates {
    orgName?: string;
}

@connect(state => ({
    identityDict: state.identity.identityDict,
    organizations: state.identity.organizations,
    organizationLoaded: state.identity.organizationLoaded
}))
export class AkAvatar extends React.Component<AkAvatarProps, AkAvatarStates> {
    static defaultProps: AkAvatarProps = {
        type: "picture",
        shape: "circle",
        size: 50,
        withChat: false
        // size: "default",
    };
    static placeholder: string = AkContext.getUserDefaultPhoto();// process.env.CDN + "images/user.jpg";

    constructor(props: AkAvatarProps, context) {
        super(props, context);
        this.state = {
            orgName: ""
        }
    }

    componentDidMount() {
        AkGlobal.store.dispatch(IdentityAction.requestUsers(this.props.value));
        AkGlobal.store.dispatch(IdentityAction.requestOrganizations());
    }

    componentWillReceiveProps(nextProps: AkAvatarProps) {
        if ("value" in nextProps && nextProps.value !== this.props.value) {
            AkGlobal.store.dispatch(IdentityAction.requestUsers(nextProps.value));
        }
    }

    renderText(user: AkUser) {
        const { className, textClassName, type, style, attr = "Name", addonTextAfter } = this.props;
        const isCombination = type === "combination";
        const clsName = classNames("ak-avatar-text", textClassName, { [className]: !isCombination && className });
        let textContent: string = "";
        if (attr === "organization" && user) {
            for (let item of this.props.organizations) {
                if (user['Attr'] && item['ID'] === user['Attr']['OrgID']) {
                    textContent = item['Name'];
                }
            }
        }
        else
            textContent = user ? user[attr] : "";
        return <span className={clsName}
            style={isCombination ? undefined : style}> {textContent}{addonTextAfter ? addonTextAfter : null}</span>;
    }

    renderPicture(user: AkUser) {
        const { size, shape, className, type, style } = this.props;
        const isCombination = type === "combination";
        const clsName = classNames("ak-avatar-picture", { [className]: !isCombination && className });

        return <AkImg
            lazyLoad={false}
            src={user ? user.Photo : ""}
            className={clsName}
            placeholder={AkAvatar.placeholder}
            magnifier={false}
            style={isCombination ? undefined : style}
            shape={shape}
            width={size}
            height={size} />;
    }

    render() {
        const { identityDict, withChat, value, className, style, size, layout = "inline" } = this.props;
        const current = identityDict[value];
        const data = current ? current.data : null;
        var item;
        switch (this.props.type) {
            case "combination":
                const clsName = classNames("ak-avatar", `ak-avatar-${layout}`, className);
                item = <div className={clsName} style={style}>
                    {this.renderPicture(data)}
                    {/* //283流程中心-表单中流程日志的审批人头像和姓名上下显示 */}
                    {/* <div className="ak-avatar-text-wrapper" style={{ width: size }}> */}
                    {this.renderText(data)}
                    {/* </div> */}
                </div>;
                break;
            case "text":
                item = this.renderText(data);
                break;
            case "picture":
            default:
                item = this.renderPicture(data);
                break;
        }
        return data && withChat
            ? <AkPopover title={data.Name} content={
                <AkRow type="flex" justify="space-between">
                    <AkCol>
                        <AkButton>Skpye联系</AkButton>
                    </AkCol>
                    <AkCol>
                        <AkButton icon="message" onClick={() => {
                            SocketClient.getInstance().createChat({
                                ChatType: ChatTypeEnum.OneToOneChat,
                                MemberIDs: [data.ID, AkContext.getUser().AccountID]
                            });
                        }}>发信息</AkButton>
                    </AkCol>
                </AkRow>
            }>{item}</AkPopover>
            : item;
    }
}
