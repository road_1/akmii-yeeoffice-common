import * as React from "react";
import { Component } from "react";
import { Upload } from "antd";

export declare type AkUploadFileStatus = 'error' | 'success' | 'done' | 'uploading' | 'removed';

export interface AkFileOptions {
    file: AkFile;
    fileList: AkFile[];
    headers?: any;
    action?: string;
    data?: any;
    onSuccess?: (body: Object) => void
    onError?: (event: any, body?: Object) => void
    onProgress: (event: {
        percent: number
    }) => void
}

export interface FileExt extends File {
    uid: number;
    fileKey?: string;
}

export interface AkFile {
    uid;
    size: number;
    name: string;
    filename?: string;
    lastModified?: string;
    lastModifiedDate?: Date;
    type: string;
    url?: string;
    status?: AkUploadFileStatus;
    percent?: number;
    thumbUrl?: string;
    originFileObj?: FileExt;
    fileKey?: string;
    response?: string;
    error?: any;
    linkProps?: any;
}

export interface AkHttpRequestHeader {
    [key: string]: string;
}

export interface AkUploadChangeParam {
    file: AkFile;
    fileList: Array<AkFile>;
    event?: {
        percent: number;
    };
}

export interface AkShowUploadListInterface {
    showRemoveIcon?: boolean;
    showPreviewIcon?: boolean;
}
export interface AkUploadLocale {
    uploading?: string;
    removeFile?: string;
    uploadError?: string;
    previewFile?: string;
}
export interface AkUploadProps {
    type?: 'drag' | 'select';
    name?: string;
    defaultFileList?: Array<AkFile>;
    fileList?: Array<AkFile>;
    action: string;
    data?: Object | ((file: AkFile) => any);
    headers?: AkHttpRequestHeader;
    showUploadList?: boolean | AkShowUploadListInterface;
    multiple?: boolean;
    accept?: string;
    beforeUpload?: (file: AkFile, FileList: AkFile[]) => boolean | PromiseLike<any>;
    onChange?: (info: AkUploadChangeParam) => void;
    listType?: 'text' | 'picture' | 'picture-card';
    className?: string;
    onPreview?: (file: AkFile) => void;
    onRemove?: (file: AkFile) => void | boolean;
    supportServerRender?: boolean;
    style?: React.CSSProperties;
    disabled?: boolean;
    prefixCls?: string;
    customRequest?: (option: any) => void;
    withCredentials?: boolean;
    locale?: AkUploadLocale;
}

export class AkUpload extends Component<AkUploadProps, undefined> {
    static Dragger?: any = Upload.Dragger;

    render() {
        return <Upload {...this.props}></Upload>
    }
}