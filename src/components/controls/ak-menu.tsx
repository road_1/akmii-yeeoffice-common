import * as React from 'react'
import {Component} from 'react'
import {Menu} from 'antd';

export interface AkSelectParam {
    key : string;
    keyPath : Array < string >;
    item : any;
    domEvent : any;
    selectedKeys : Array < string >;
}
export interface AkClickParam {
    key : string;
    keyPath : Array < string >;
    item : any;
    domEvent : any;
}
// export declare type AkMenuMode = 'vertical' | 'vertical-left' | 'vertical-right' | 'horizontal' | 'inline';
export declare type AkMenuMode = 'vertical' | 'horizontal' | 'inline';
export interface AkMenuProps {
    
    id?: string;
    /** `light` `dark` */
    theme?: 'light' | 'dark';
    /** enum: `vertical` `horizontal` `inline` */
    mode?: 'vertical' | 'horizontal' | 'inline';
    selectable?: boolean;
    selectedKeys?: Array<string>;
    defaultSelectedKeys?: Array<string>;
    openKeys?: Array<string>;
    defaultOpenKeys?: Array<string>;
    onOpenChange?: (openKeys: string[]) => void;
    onSelect?: (param: AkSelectParam) => void;
    onDeselect?: (param: AkSelectParam) => void;
    onClick?: (param: AkSelectParam) => void;
    style?: React.CSSProperties;
    openAnimation?: string | Object;
    openTransitionName?: string | Object;
    className?: string;
    prefixCls?: string;
    multiple?: boolean;
    inlineIndent?: number;
    inlineCollapsed?: boolean;
}
export interface AkMenuStates {}
export class AkMenu extends Component < AkMenuProps,
AkMenuStates > {
    constructor(props, context) {
        super(props, context);
    }
    static Divider = Menu.Divider;
    static Item:any = Menu.Item;
    static SubMenu:any = Menu.SubMenu;
    static ItemGroup = Menu.ItemGroup;
    render() {
        return <Menu {...this.props}></Menu>
    }
}
class AkMenuStyle {}
