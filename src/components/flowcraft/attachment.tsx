import * as React from "react";
import { Component } from "react";
import { FormattedMessage } from "react-intl";
import { TaskDetailInfo } from "../../api/";
import { FileUpLoadAPI } from '../../api/common/fileupload';
import { AkUploadFile, DeleteDocumentRequest } from '../../api/common/fileuploadmodel';
import { AttachmentAPI, LibraryModel } from "../../index";
import { ApplyContentLocale, CommonLocale, UploadLocale } from '../../locales/localeid';
import { IntlProps } from "../../util/";
import { AkFileIcon } from '../../util/ak-file-icon';
import { AkContext, AkGlobal } from '../../util/common';
import { FileUpLoadCommon } from '../../util/FileUpLoadCommon';
import { FileUploadMethod } from '../../util/FileUploadMethod';
import { AkUtil } from '../../util/util';
import { AkCol } from "../controls/ak-col";
import { AkIcon } from "../controls/ak-icon";
import { AkMessage } from '../controls/ak-message';
import { AkModal } from '../controls/ak-modal';
import { AkNotification } from '../controls/ak-notification';
import { AkRow } from "../controls/ak-row";
import { AkFile, AkUpload } from "../controls/ak-upload";

export interface AkAttachmentProps extends IntlProps {
    /**
     * 文件列表
     *
     * @type {any[]}
     * @memberOf AkAttachmentProps
     */
    taskDetail?: TaskDetailInfo;
    readonly?: boolean;
}

export interface AkAttachmentStates {
    fileList?: AkFile[];
}

export class AkAttachment extends Component<AkAttachmentProps, AkAttachmentStates> {
    attachment?: AkFile;
    formatMessage?: any;
    deleteFiles?:DeleteDocumentRequest;
    listDataIDs?:string[];
    library:LibraryModel;

    constructor(props, context) {
        super(props, context);
        this.props.taskDetail.Variables = Object.assign({}, this.props.taskDetail.Variables);
        this.props.taskDetail.Variables.__attachments = Object.assign([], this.props.taskDetail.Variables.__attachments);
        this.state={fileList:[]};
        this.formatMessage = AkGlobal.intl.formatMessage;
        this.deleteFiles=null;
        this.listDataIDs=[];
        this.library=null;
    }
    componentDidMount(){
        this.getLibrary().then(onfulfilled=>{
            this.library=onfulfilled;
        });
     }
    componentWillReceiveProps(nextProps: AkAttachmentProps) {
        if (nextProps.taskDetail.Variables && nextProps.taskDetail.Variables.__attachments) {
            this.setState({
                fileList: this.parseAkFile(nextProps.taskDetail.Variables.__attachments)
            });
        }
    }

    parseAkFile(attachments) {
        let akFileList: AkFile[] = [];
        if (attachments&&AkUtil.isArray(attachments)) {
            attachments.map((item, index) => {
                akFileList.push({
                    uid: index + 1 + "",
                    type: "",
                    name: item.fileName,
                    thumbUrl: this.getThumbUrl(item.fileName),
                    size: item.fileSize,
                    fileKey: item.fileKey,
                    status: "done"
                });
            });
        }
        return akFileList;
    }
    getFileExt(fileName) {
        return ((/[.]/.exec(fileName)) ? /[^.]+$/.exec(fileName)[0] : "").toLowerCase();
    }
    getThumbUrl(fileName: string) {
        switch (this.getFileExt(fileName)) {
            case "ppt":
            case "pptx":
                return AkFileIcon.PPT;
            case "doc":
            case "docx":
                return AkFileIcon.Word;
            case "xls":
            case "xlsx":
                return AkFileIcon.Excel;
            case "vsdx":
            case "vsd":
                return AkFileIcon.VISO;
            case "pdf":
                return AkFileIcon.PDF;
            case "txt":
                return AkFileIcon.TXT;
            case "png":
            case "jpg":
            case "gif":
            case "jpeg":
            case "icon":
                return AkFileIcon.IMG;
            default:
                return AkFileIcon.FILE;
        }
    }
    onPreviewHandler(file: AkFile) {
        let files= this.props.taskDetail.Variables.__attachments;
        let tempFile= files.find(f=>f.fileName===file.name);
        let url=tempFile.fileInfo.Url;
        if ((url+"").toLocaleLowerCase().includes("&libraryid")) {
            if (AkContext.getBranch() !== AkContext.YeeOffice) {
                let token = AkContext.getToken();
                token = encodeURIComponent(token);
                if (token && token !== undefined && token !== '' && token !== "undefined") {
                    let akmiisecretIndex = url.lastIndexOf("&akmiisecret=") + 13;
                    url = url.substring(0, akmiisecretIndex) + token;
                }
            }
            url+=("&random="+AkUtil.guid());
            this.downloadFile(url);
        } else {
            AttachmentAPI.DownAttachment({ FileKey: file.fileKey }).then(data => {
                if (!file.url) {
                    this.downloadFile(data.Data);
                } else {
                    file.url = data.Data
                }
            });
        }
    }
    downloadFile(url) {
        var iframe = document.createElement('iframe');
        iframe.src = url;
        iframe.setAttribute("style", "display:none;")
        document.body.appendChild(iframe);
    }
    beforeUpload(file?: AkFile) {
        let result = true;
        let msg="";
        const{formatMessage}=AkGlobal.intl;
        if (file.size === 0) {
            msg= formatMessage({id:UploadLocale.MsgNoContentError},{name:file.name});
            AkNotification.warning({message: formatMessage({ id: CommonLocale.Tip }),description: msg});
            return false;
        }
        /**文件最高10M */
        if (file.size > 1024 * 1024 * 10) {
            msg=file.name + formatMessage({ id: ApplyContentLocale.SizeCannot });
            AkNotification.warning({ message: formatMessage({ id: CommonLocale.Tip }),  description: msg });
            return false;
        }
        if (FileUpLoadCommon.validateFileNameSize(file.name) === false) {
            msg= formatMessage({id:UploadLocale.FileNameMoreLarge},{name:file.name});
            AkNotification.warning({message: formatMessage({ id: CommonLocale.Tip }),description: msg});
            return false;
        }
        if (FileUpLoadCommon.validateFileName(file.name) === false) {
            msg =formatMessage({id:UploadLocale.FileNameInvalidate},{name:file.name});
            AkNotification.warning({message: formatMessage({ id: CommonLocale.Tip }),description: msg});
            return false;
        }

        let fileList = this.state.fileList
        fileList.map((a) => {
            if (a.name === file.name) {
                AkNotification.error({
                    message: this.formatMessage({ id: CommonLocale.Tip }),
                    description: this.formatMessage({ id: ApplyContentLocale.UploadFail }) + file.name + this.formatMessage({ id: ApplyContentLocale.Exists })
                });
                result = false;
            }
        })
        return result;
    }
    uploadChange(fileList: AkFile[]) {
        fileList = fileList.map((file) => {
            file.thumbUrl = this.getThumbUrl(file.name);
            if (file.originFileObj) {
                file.fileKey = file.originFileObj.fileKey;
            }
            return file;
        });
        this.setState({ fileList });
    }
    getLibrary(){
        return new Promise((resolve,reject)=>{
            FileUpLoadAPI.getLibraryByCode({ Code: "flowcraft-file" }).then(d => {
                if(d.Status===0){
                    resolve(d.Data);
                 }else{
                    console.log(d);
                    reject(d.Message)
                 }
            });
        });
    }
    customRequest(options?: AkUploadFile) {
        let topThis = this;
        FileUploadMethod.beginUpload(options, this.library).then((onfulfilled) => {
            let downLoadUrl = onfulfilled.Data;
            let newName = onfulfilled.Message;
            let __attachments = topThis.props.taskDetail.Variables.__attachments;
            const currentId = AkContext.getUser().AccountID;
            if (typeof (__attachments) !== "string") {
                __attachments.push({
                    fileKey: newName,
                    fileSize: options.file.size,
                    fileName: options.file.name,
                    fileInfo: { FileKey: newName, Url: downLoadUrl, CreatedBy: currentId }
                });
            }
            (options as any).onSuccess(topThis.formatMessage({ id: ApplyContentLocale.UploadComplete }));
        }, (onrejected) => {
            let msg = onrejected.Message ? onrejected.Message : AkGlobal.intl.formatMessage({ id: ApplyContentLocale.UploadFail });
            (options as any).onError({ event: onrejected, body: "error" });
            AkNotification.error({
                message: AkGlobal.intl.formatMessage({ id: CommonLocale.Tip }),
                description: msg
            });
        });
    }
    renderDeleteModal() {
        const formatMessage = AkGlobal.intl.formatMessage;
        let attachment = this.attachment;

        return AkModal.confirm({
            title:formatMessage({ id: CommonLocale.Tip }),
            content:formatMessage({ id: ApplyContentLocale.ConfirmDelete },{name:attachment.name}),
            onOk: () => this.onRemove(attachment),
            onCancel: () => { }
        });
    }
    onRemove(file) {
        let __attachments = this.props.taskDetail.Variables.__attachments;
        let fileList = this.state.fileList;

        if (typeof (__attachments) !== "string") {
            let idx = __attachments.findIndex(a => a.fileName === file.name);
            if (idx > -1) {
                const attachment = __attachments[idx];
                const currentId = AkContext.getUser().AccountID;

                if (!currentId || !attachment.fileInfo) {
                    __attachments.splice(idx, 1);
                    __attachments["file_delete"]=this.deleteDocument(attachment);
                    AkUtil.remove(fileList, f => {
                        return f.name === attachment.fileName;
                    });
                } else {
                    if (currentId === attachment.fileInfo.CreatedBy) {
                        __attachments.splice(idx, 1);
                        __attachments["file_delete"]=this.deleteDocument(attachment);
                        AkUtil.remove(fileList, f => {
                            return f.name === attachment.fileName;
                        });
                    } else {
                        AkNotification.error({
                            message: this.formatMessage({ id: CommonLocale.Tip }),
                            description: this.formatMessage({ id: ApplyContentLocale.DeleteFileByOther })
                        });
                        return false;
                    }
                }
                this.setState({ fileList: fileList });
            }
        }
    }
    deleteDocument(file){
            if(!file.fileInfo){
                return null;
            }
            let itemInfo = file.fileInfo.Url;
            let tempStartIndex = (itemInfo + "").indexOf('appID');
            if (tempStartIndex === -1) {
                AkMessage.success(AkGlobal.intl.formatMessage({ id: CommonLocale.DeleteSuccess }));
               return ;
           }
            let tempEndIndex = (itemInfo + "").indexOf('&md5');
            let tempStr = (itemInfo + "").substring(tempStartIndex, tempEndIndex);
            let arry = tempStr.split('&');
            let appID = Number((arry[0] + "").split('=')[1]);
            let libraryID = (arry[1] + "").split('=')[1] + "";
            let listDataID = (arry[2] + "").split('=')[1] + "";
            this.listDataIDs.push(listDataID);
            return this.deleteFiles={
                AppID:appID,
                LibraryID:libraryID,
                ListDataIDs:this.listDataIDs
            }
    }
    render() {
        if (!this.props.taskDetail ||
            (this.props.readonly && (!this.state.fileList || this.state.fileList.length === 0))) return null;

        return <AkUpload
            className={this.props.readonly ? "ant-upload-disabled" : ""}
            action=""
            disabled={this.props.readonly}
            multiple
            listType="picture"
            fileList={this.state.fileList}
            onChange={(info) => {
                this.uploadChange(info.fileList);
            }}
            beforeUpload={(file) => {
                return this.beforeUpload(file);
            }}
            customRequest={(options) => {
                this.customRequest(options)
            }}
            onRemove={(file: AkFile) => {
                if (this.props.readonly) return false;

                this.attachment = file;
                this.renderDeleteModal();
                return false; 
            }}
            onPreview={(file: AkFile) => {
                this.onPreviewHandler(file);
            }}>
            {/* {this.renderDeleteModal()} */}
            <AkRow
                type="flex"
                justify="space-between"
                align="middle"
                className="ak-border-b1">
                <AkCol className="ak-form-title no-border">
                    <span className="title-bluespan"></span>
                    <FormattedMessage id={ApplyContentLocale.AttachmentFile}></FormattedMessage>
                </AkCol>
                <AkCol>
                    {!this.props.readonly
                        ? <a>
                            <AkIcon type="attachment"></AkIcon>
                            <FormattedMessage id={ApplyContentLocale.AttachmentAdd}></FormattedMessage>
                        </a>
                        : null}
                </AkCol>
            </AkRow>
        </AkUpload>
    }
}

class AkAttachmentStyle { }
