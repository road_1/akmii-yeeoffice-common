import * as React from "react";
import { Component } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import * as moment from "moment";
import { WorkflowFormHelper } from "./workflow-form-helper";
import { IntlProps, RouterProps } from "../../util/common";
import { TaskDetailInfo } from "../../api/flowcraft/task";
import { ApplyContentLocale } from "../../locales/localeid";
import { AkRow } from "../controls/ak-row";
import { AkCol } from "../controls/ak-col";
import { AkInput } from "../controls/ak-input";
import { AkIdentity } from "../../api/identity/identity";
import { AkIdentityPicker } from "../index";
import { AkIcon } from "../controls/ak-icon";
import { ApplicationStatusEnum } from "../../api/flowcraft/application";

export interface YeeUserContentProps {
    /**
     * 展示还是编辑
     *
     * @type {boolean}
     * @memberOf YeeUserContentProps
     */
    display?: boolean;
    /**
     *是否允许代申请(只在编辑时可用)
     *
     * @type {boolean}
     * @memberOf YeeUserContentProps
     */
    allowDelegate?: boolean;
    /**
     * 申请人信息变更
     *
     *
     * @memberOf YeeUserContentProps
     */
    onChangeApplyUser?: (userID: string) => void;
    /**
     * 申请人信息
     *
     * @type {ApplicationModel}
     * @memberOf YeeUserContentProps
     */
    taskDetail?: TaskDetailInfo;

    readonly?: boolean;

    title?: string;
}

export interface YeeUserContentStates {
    toggle?: boolean;
    appUser?: any;
}

export default class YeeUserContent extends Component<YeeUserContentProps, YeeUserContentStates> {
    constructor(props, context) {
        super(props, context);
        this.state = {
            toggle: true
        };
    }

    componentWillReceiveProps(nextProps: YeeUserContentProps) {
        this.setState({ appUser: nextProps.taskDetail })
    }

    toggleDetailHandler() {
        this.setState({
            toggle: !this.state.toggle
        });
    }

    renderView() {
        const { taskDetail } = this.props;
        if (!taskDetail) return null;
        let { rowLayout, colLabelLayout, colControlLayout } = WorkflowFormHelper;
        colControlLayout = { ...colControlLayout, ...{ className: 'ant-form-item-control-wrapper' } };

        return <div className="ak-form-group">
            <AkRow {...rowLayout}>
                <AkCol {...colLabelLayout}>
                    <FormattedMessage id={ApplyContentLocale.LabelSubmitUser}></FormattedMessage>
                </AkCol>
                <AkCol {...colControlLayout}>{taskDetail.CreatedByName}
                </AkCol>
                <AkCol {...colLabelLayout}>
                    <FormattedMessage id={ApplyContentLocale.LabelSubmitDate}></FormattedMessage>
                </AkCol>
                <AkCol {...colControlLayout}>
                    {taskDetail.ApplyDateStr}
                </AkCol>
                <AkCol {...colLabelLayout}>
                    <FormattedMessage id={ApplyContentLocale.LabelApplyUser}></FormattedMessage>
                </AkCol>
                <AkCol {...colControlLayout}>
                    {taskDetail.ApplicantName}
                </AkCol>
                <AkCol {...colLabelLayout}>
                    <FormattedMessage id={ApplyContentLocale.LabelEmployeeNo}></FormattedMessage>
                </AkCol>
                <AkCol {...colControlLayout}>{taskDetail.EmployeeNo}
                </AkCol>
                <AkCol {...colLabelLayout}>
                    <FormattedMessage id={ApplyContentLocale.LabelJobTitle}></FormattedMessage>
                </AkCol>
                <AkCol {...colControlLayout}>{taskDetail.JobTitle}
                </AkCol>
                <AkCol {...colLabelLayout}>
                    <FormattedMessage id={ApplyContentLocale.LabelLocation}></FormattedMessage>
                </AkCol>
                <AkCol {...colControlLayout}>{taskDetail.LocationName}
                </AkCol>
                <AkCol {...colLabelLayout}>
                    <FormattedMessage id={ApplyContentLocale.LabelLineManager}></FormattedMessage>
                </AkCol>
                <AkCol {...colControlLayout}>{taskDetail.LineManagerName}
                </AkCol>
                <AkCol {...colLabelLayout}>
                    <FormattedMessage id={ApplyContentLocale.LabelOrg}></FormattedMessage>
                </AkCol>
                <AkCol {...colControlLayout}>{taskDetail.OrgName}
                </AkCol>
            </AkRow>
        </div>
    }

    renderEdit() {
        const { taskDetail, readonly, allowDelegate } = this.props;
        if (!taskDetail) return null;
        let { rowLayout, colLabelLayout, colControlLayout } = WorkflowFormHelper;
        colControlLayout = { ...colControlLayout, ...{ className: 'ant-form-item-control-wrapper' } };

        return <div className="ak-form-group">
            <AkRow {...rowLayout}>
                <AkCol {...colLabelLayout}>
                    <FormattedMessage id={ApplyContentLocale.LabelSubmitUser}></FormattedMessage>
                </AkCol>
                <AkCol {...colControlLayout}>
                    <AkInput value={taskDetail.CreatedByName} disabled></AkInput>
                </AkCol>
                <AkCol {...colLabelLayout}>
                    <FormattedMessage id={ApplyContentLocale.LabelSubmitDate}></FormattedMessage>
                </AkCol>
                <AkCol {...colControlLayout}>
                    <AkInput value={taskDetail.ApplyDateStr ? taskDetail.ApplyDateStr : moment().format("YYYY-MM-DD HH:mm")} disabled></AkInput>
                </AkCol>
                <AkCol {...colLabelLayout}>
                    <FormattedMessage id={ApplyContentLocale.LabelApplyUser}></FormattedMessage>
                </AkCol>
                <AkCol {...colControlLayout}>
                    <AkIdentityPicker value={taskDetail.ApplicantID} readonly={
                        readonly || !allowDelegate || this.props.taskDetail.AppStatus === ApplicationStatusEnum.Rejected || this.props.taskDetail.AppStatus === ApplicationStatusEnum.Revoked}
                        onChange={user => this.props.onChangeApplyUser(user ? (user as AkIdentity).ID : null)}></AkIdentityPicker>
                </AkCol>
                <AkCol {...colLabelLayout}>
                    <FormattedMessage id={ApplyContentLocale.LabelEmployeeNo}></FormattedMessage>
                </AkCol>
                <AkCol {...colControlLayout}>
                    <AkInput value={taskDetail.EmployeeNo} disabled></AkInput>
                </AkCol>
                <AkCol {...colLabelLayout}>
                    <FormattedMessage id={ApplyContentLocale.LabelJobTitle}></FormattedMessage>
                </AkCol>
                <AkCol {...colControlLayout}>
                    <AkInput value={taskDetail.JobTitle} disabled></AkInput>
                </AkCol>
                <AkCol {...colLabelLayout}>
                    <FormattedMessage id={ApplyContentLocale.LabelLocation}></FormattedMessage>
                </AkCol>
                <AkCol {...colControlLayout}>
                    <AkInput value={taskDetail.LocationName} disabled></AkInput>
                </AkCol>
                <AkCol {...colLabelLayout}>
                    <FormattedMessage id={ApplyContentLocale.LabelLineManager}></FormattedMessage>
                </AkCol>
                <AkCol {...colControlLayout}>
                    <AkInput value={taskDetail.LineManagerName} disabled></AkInput>
                </AkCol>
                <AkCol {...colLabelLayout}>
                    <FormattedMessage id={ApplyContentLocale.LabelOrg}></FormattedMessage>
                </AkCol>
                <AkCol {...colControlLayout}>
                    <AkInput value={taskDetail.OrgName} disabled></AkInput>
                </AkCol>
            </AkRow>
        </div>
    }

    render() {
        const { title } = this.props;
        return <div>
            <div className="ak-form-title mb15 toggle-title" onClick={() => this.toggleDetailHandler()}>
                <span className="title-bluespan"></span>
                {title ? title : <FormattedMessage id={ApplyContentLocale.ApplyUserTitle} />}
                <AkIcon type={this.state.toggle ? "up" : "down"}></AkIcon>
            </div>
            {this.state.toggle ? (this.props.display ? this.renderView() : this.renderEdit()) : null}
        </div>
    }
}
class YeeUserContentStyle { }
// export default injectIntl(YeeUserContent)