import * as React from "react";
import { withRouter } from "react-router";
import { FormattedMessage, injectIntl } from "react-intl";
import YeeUserContent from "./yee-user-content";
import { AkAttachment } from "./attachment";
import { StatusBar } from "./status-bar";
import { WorkflowFormHelper } from "./workflow-form-helper";
import { IntlProps, RouterProps, AkGlobal, AkContext, FlowcraftCommon } from '../../util/common';
import { TaskDetailInfo, PutTaskHandleRequest, TaskAPI, PutTaskClaimRequest, ApplicationInfo } from '../../api/flowcraft/task';
import { AkNotification } from "../controls/ak-notification";
import { AkAutoSizeType, AkInput } from "../controls/ak-input";
import { AkButton, AkButtonProp } from "../controls/ak-button";
import { ApplyContentLocale, CommonLocale } from '../../locales/localeid';
import { AkSpin } from "../controls/ak-spin";
import { AkIdentity } from "../../api/identity/identity";
import { AkModal } from "../controls/ak-modal";
import { AkCol } from '../controls/ak-col';
import { TaskStatusEnum, TaskFlagsEnum, ApplicationStatusEnum, AkUtil } from "../../index";
import AkFormHistoryLog from '../controls/historylog/ak-form-historylog';
import { AkIcon } from '../controls/ak-icon';

/**
 * 审批模块按钮
 * 
 * @export 
 * @interface ApprovalPanelAction
 */
export interface ApprovalPanelAction {
    /**
     * 审批前操作
     * 
     * 
     * @memberOf ApprovalPanelAction
     */
    before?: (taskDetail?: TaskDetailInfo) => any;
    /**
     * 审批后操作
     * 
     * 
     * @memberOf ApprovalPanelAction
     */
    after?: (taskDetail?: TaskDetailInfo, response?: any, comment?: string) => any;
    /**
     * 是否显示此按钮
     * 
     * @type {boolean}
     * @memberOf ApprovalPanelAction
     */
    display?: boolean;
    /**
     * 按钮属性
     * 
     * @type {AkButtonProp}
     * @memberOf ApprovalPanelAction
     */
    buttonProp?: AkButtonProp;
    /**
     * 多语言Key
     * 
     * @type {string}
     * @memberOf ApprovalPanelAction
     */
    localeId?: string;
}

export interface ApprovalContentProps extends RouterProps {
    /**
     * 加载完成回调操作
     *
     *
     * @memberOf ApprovalContentProps
     */
    onLoaded: (variables: any, readonly?: boolean, taskDetail?: TaskDetailInfo) => void;
    /**
     * 审批通过前操作
     *
     *
     * @memberOf ApprovalContentProps
     */
    approval?: ApprovalPanelAction;
    /**
     * 拒绝前操作
     *
     *
     * @memberOf ApprovalContentProps
     */
    reject?: ApprovalPanelAction;
    /**
     * 转签前操作
     *
     *
     * @memberOf ApprovalContentProps
     */
    forward?: ApprovalPanelAction;
    /**
     * 关闭前
     *
     *
     * @memberOf ApprovalContentProps
     */
    beforeClose?: (taskDetail?: TaskDetailInfo) => boolean;
    /**
     * 是否允许领用
     *
     *
     * @memberOf ApprovalContentProps
     */
    beforeReceive?: (taskDetail?: TaskDetailInfo) => boolean;
    /**
     * 是否允许修改报单
     * 
     * @type {boolean}
     * @memberOf ApprovalContentProps
     */
    allowEditable?: boolean;
    disableApprovalPanel?: boolean;
    disableComment?: boolean;
    /**不显示附件上传 */
    disabledAttachment?: boolean;
    /**不显示用户信息 */
    disabledApplicationInfo?: boolean;
    /**
     * 页面Title
     *
     * @type {string}
     * @memberOf ApprovalContentProps
     */
    pageTitle?: string;

    renderCustomHeader?: () => JSX.Element | JSX.Element[];

    renderCustomFooter?: () => JSX.Element | JSX.Element[];
}

export interface ApprovalContentStates {
    /**
     * 加载中
     *
     * @type {boolean}
     * @memberOf ApprovalContentStates
     */
    loading?: boolean;
    /**
     * 任务详情
     *
     * @type {TaskDetailInfo}
     * @memberOf ApprovalContentStates
     */
    taskDetail?: TaskDetailInfo;
    /**
     * 显示转办弹窗
     * 
     * @type {boolean}
     * @memberOf ApprovalContentStates
     */
    showForwardModal?: boolean;
    showAddAssisign?: boolean;
    /**
     * 转办人
     * 
     * @type {AkIdentity}
     * @memberOf ApprovalContentStates
     */
    directValue?: AkIdentity;

    toggle?: boolean;

    /**
     * 召回
     */
    hasRecalled?: boolean;
}

/**
 * 审批流程任务信息
 *
 * @export
 * @class ApprovalContent
 * @extends {Component<ApprovalContentProps, ApprovalContentStates>}
 */
@withRouter
export default class ApprovalContent extends React.Component<ApprovalContentProps, ApprovalContentStates> {
    taskID: string;
    procInstID: string;
    loading: boolean;
    disableApprove: string;
    // allowForward?: boolean;
    comment: string;
    isclaim: string;
    endorseUser: string[];

    constructor(props, context) {
        super(props, context);

        this.taskID = this.props.location.query["taskid"] as string;
        this.procInstID = this.props.location.query["instid"] as string;
        this.disableApprove = this.props.location.query["disableapprove"] as string;
        this.isclaim = this.props.location.query["isclaim"] as string;
        this.loading = true;
        // // hard code allow forword
        // this.allowForward = true;
        this.comment = '';

        this.state = { taskDetail: {}, loading: true, showForwardModal: false, toggle: true, hasRecalled: false };
    }

    componentDidMount() {
        this.loadDetailData();
    }

    loadDetailData() {
        TaskAPI.getTaskDetail({ taskID: this.taskID, procInstID: this.procInstID }).then(data => {
            if (data.Data) {
                const variables = WorkflowFormHelper.deserializeVariables(data.Data.Variables);
                data.Data.Variables = variables;
                const taskDetail = Object.assign({}, this.state.taskDetail, data.Data);
                this.props.onLoaded(variables, AkUtil.toBoolean(this.disableApprove), taskDetail);
                this.setState({ taskDetail });
            } else {
                AkNotification.error({
                    message: AkGlobal.intl.formatMessage({ id: CommonLocale.Tip }),
                    description: AkGlobal.intl.formatMessage({ id: CommonLocale.GetInfoError })
                });
            }

            this.setLoading(false);
        });
    }

    setLoading(flag: boolean) {
        this.loading = flag;
        this.setState({ loading: flag });
    }

    // renderForward() {
    //     const { props: { forward, intl: { formatMessage } }, loading, comment, taskID } = this;
    //     if (loading || (forward && forward.before && !forward.before(this.state.taskDetail))) return;

    //     return this.state.showForwardModal ? <AkModal
    //         visible={this.state.showForwardModal}
    //         onCancel={() => this.setState({ showForwardModal: false })}
    //         onOk={() => { this.setState({ showForwardModal: false }); this.onForward(); }}
    //         title={formatMessage({ id: ApplyContentLocale.ClaimAssigneeTitle })}>
    //         <AkRow type="flex" justify="end" align="middle" className="mb20">
    //             <AkCol span={4}>
    //                 <FormattedMessage id={ApplyContentLocale.ClaimAssigneeTitle}></FormattedMessage>
    //             </AkCol>
    //             <AkCol span={20}>
    //                 <AkIdentityPicker multiple={false} defaultValue={this.state.directValue}
    //                     onChange={v => this.setState({ directValue: v as AkIdentity })} />
    //             </AkCol>
    //         </AkRow>
    //     </AkModal> : null;
    // }

    executeApprovalAction(outcome: 'Approved' | 'Rejected', action: ApprovalPanelAction) {
        if (this.loading) return;
        let variables: any = {};
        const { taskID, comment, props: { allowEditable }, state: { taskDetail } } = this;
        const COMMENT_MAX_LENGTH = 200
        if (comment && comment.length > COMMENT_MAX_LENGTH) {
            AkNotification.warning({
                message: AkGlobal.intl.formatMessage({ id: CommonLocale.Tip }),
                description: AkGlobal.intl.formatMessage({ id: ApplyContentLocale.CommentLengthCheck }, { length: COMMENT_MAX_LENGTH.toString() })
            });
            return;
        }

        const request: PutTaskHandleRequest = {
            TaskID: taskID, Outcome: outcome, Comment: comment
        };

        if (action && action.before) {
            variables = action.before(taskDetail);
            if (!variables) return;
        }

        if (allowEditable) {
            // if (!variables) variables = {};
            variables.__attachments = taskDetail.Variables.__attachments;
            request.Variables = WorkflowFormHelper.serializeVariables(variables);
        }
        WorkflowFormHelper.executeApprovalAction(this, request, action, "common")
    }

    onReject() {
        if (this.loading) return;

        // reject must has comment
        if (!this.comment || this.comment.length === 0) {
            AkNotification.warning({
                message: AkGlobal.intl.formatMessage({ id: CommonLocale.Tip }),
                description: AkGlobal.intl.formatMessage({ id: ApplyContentLocale.CommentOnReject })
            });
            return;
        }
        this.executeApprovalAction("Rejected", this.props.reject)
    }

    onApprove() {
        this.executeApprovalAction("Approved", this.props.approval)
    }

    // onForward() {
    //     if (this.loading) return;
    //     let newAssignee = this.state.directValue;
    //     if (!newAssignee) {
    //         AkNotification.warning({
    //             message: AkGlobal.intl.formatMessage({ id: CommonLocale.Tip }),
    //             description: AkGlobal.intl.formatMessage({ id: ApplyContentLocale.ClaimAssigneeTitle })
    //         });
    //         return;
    //     }
    //     const request: PutTaskClaimRequest = {
    //         TaskID: this.taskID,
    //         AssigneeID: newAssignee.ID,
    //     }
    //     WorkflowFormHelper.onForward(this, request, "common")
    // }

    renderApprovalActionButton(action: ApprovalPanelAction, defaultButtonProp: AkButtonProp, defaultLocaleId: string) {
        if (action) {
            if (action.display) {
                const _buttonProp = { ...defaultButtonProp, ...action.buttonProp };
                return <AkButton {..._buttonProp}>
                    <FormattedMessage id={action.localeId || defaultLocaleId}></FormattedMessage>
                </AkButton>
            }
        } else {
            return <AkButton {...defaultButtonProp}>
                <FormattedMessage id={defaultLocaleId}></FormattedMessage>
            </AkButton>
        }

        return null;
    }

    renderApprovePanel() {
        const autoSizeType: AkAutoSizeType = { minRows: 4 };
        const { pleaseInputMessage } = WorkflowFormHelper;
        const { state: { taskDetail }, disableApprove, props: { approval, reject, forward, disableApprovalPanel, disableComment } } = this;
        const { ButtonApprove, ButtonReject, ButtonClaim, ButtonAddAssignee } = ApplyContentLocale;

        if (!taskDetail || !taskDetail.CurrentTask || taskDetail.CurrentTask.Status !== TaskStatusEnum.Pending) return null;

        return <div className="ak-approval-box grey mb30">
            {disableComment ? null : <div className="ak-approval-content mb15">
                <AkInput.TextArea onChange={event => { this.comment = event.target.value; }}
                    autosize={autoSizeType} placeholder={pleaseInputMessage(this.props, ApplyContentLocale.ApprovalComments)}></AkInput.TextArea>
            </div>}
            <div className="text-center">
                {this.renderApprovalActionButton(approval, {
                    type: "primary", className: "mr20", onClick: () => this.onApprove()
                }, ButtonApprove)}
                {this.renderApprovalActionButton(reject, {
                    onClick: () => this.onReject()
                }, ButtonReject)}
                {/* {this.renderApprovalActionButton(forward, {
                    className: "pull-right", onClick: () => this.setState({ showForwardModal: true })
                }, ButtonClaim)} */}
                {taskDetail.CurrentTask && taskDetail.CurrentTask.IsAllowSign ? <AkButton className="pull-right" onClick={() => {
                    this.setState({ showAddAssisign: true })
                }}>
                    <FormattedMessage id={ButtonAddAssignee}></FormattedMessage>
                </AkButton> : null}
            </div>
        </div>
    }
    onClose() {
        if (this.props.beforeClose && !this.props.beforeClose(this.state.taskDetail)) return;
        WorkflowFormHelper.goBack();
    }

    onReceive() {
        const current = this;
        const { props: { beforeReceive, pageTitle }, loading, taskID } = this;
        if (loading || (beforeReceive && !beforeReceive(this.state.taskDetail))) return;
        const confirm = AkModal.confirm;
        confirm({
            title: AkGlobal.intl.formatMessage({ id: CommonLocale.Tip }),
            content: AkGlobal.intl.formatMessage({ id: ApplyContentLocale.ReceiveConfirm }, { title: pageTitle }),
            onOk() {
                const request: PutTaskClaimRequest = { TaskID: taskID };
                WorkflowFormHelper.onReceive(current, request, "common");
            },
            onCancel() { },
        });
    }
    onSaveVariable(comment) {
        //保存变量时真删除附件
        let variables = WorkflowFormHelper.deleteFiles(this.state.taskDetail.Variables);
        const params = {
            TaskID: this.taskID,
            Outcome: "SaveVariable",
            Variables: WorkflowFormHelper.serializeVariables(variables),
            Comment: comment
        }
        WorkflowFormHelper.onSaveVariable(this, params, "common")
    }
    onRecall() {
        if (!this.state.hasRecalled) {
            this.setState({
                hasRecalled: true
            })
        }

        const { taskDetail } = this.state;
        const { formatMessage } = AkGlobal.intl;
        const { RecallNotExit, RecallRuning, RecallUnSelf, RecallRefuseReceive, RecallRefuse } = ApplyContentLocale;

        let msg = "";
        if (!taskDetail.CurrentTask) {
            msg = formatMessage({ id: RecallNotExit });
        } else if (!taskDetail.CurrentTask.Outcome) {
            msg = formatMessage({ id: RecallRuning });
        } else if (taskDetail.CurrentTask.AssigneeID !== AkContext.getUser().AccountID) {
            msg = formatMessage({ id: RecallUnSelf });
        } else if (taskDetail.CurrentTask && taskDetail.CurrentTask.AssigneeID === "0" && taskDetail.CurrentTask.Flags === TaskFlagsEnum.CandidateTask) {
            msg = formatMessage({ id: RecallRefuseReceive });
        } else if (taskDetail.CurrentTask && ((taskDetail.CurrentTask.Flags & TaskFlagsEnum.RecallTask) !== TaskFlagsEnum.RecallTask)) {
            msg = formatMessage({ id: RecallRefuse });
        } else if (taskDetail.AppStatus !== ApplicationStatusEnum.Running) {//允许中任务禁止召回
            msg = formatMessage({ id: RecallRefuse });
        }
        if (msg) {
            return AkNotification.warning({
                message: formatMessage({ id: CommonLocale.Tip }),
                description: msg
            });
        }
        const params = {
            TaskID: taskDetail.CurrentTask.TaskID
        };
        WorkflowFormHelper.onRecall(this, params, "flowcraft", (recalled: boolean) => {
            // this.setState({hasRecalled:true});
            if (recalled) {
                setTimeout(() => { FlowcraftCommon.goBack(); }, 2000);
            }
        });


    }

    render() {
        const { state: { loading, taskDetail, toggle, hasRecalled }, props: { pageTitle, disabledApplicationInfo, disabledAttachment }, isclaim, disableApprove } = this;
        const { ButtonClose, ButtonReceive, ButtonSave, ButtonRecall } = ApplyContentLocale;
        let width = 81;
        if (isclaim) {
            width = width + 81;
        }
        if (taskDetail.CurrentTask && !taskDetail.CurrentTask.Outcome && taskDetail.CurrentTask.AssigneeID !== "0") {
            width = width + 81;
        }
        if (!hasRecalled) {
            width = width + 81;
        }
        let company = AkContext.getCompanyInfo();
        let canRecallOutComs = ["Completed", "Approved", "Rejected"];
        let fontColor = company ? { color: company.NavFontColour } : null
        return <div className="ak-approval-content">
            <div className="ak-zper-inbox-title" style={company ? {
                background: company.NavBarColour,
                color: company.NavFontColour
            } : null}>
                <AkCol className="ak-content-title-box">
                    <span className="inbox-text" style={fontColor}>{pageTitle}</span>
                </AkCol>
                <AkCol className="ak-content-button-box" style={{ width: width + "px" }}>
                    <AkButton className="pull-right cancel" style={fontColor}
                        icon="close" onClick={() => this.onClose()} disabled={loading}>
                        <FormattedMessage id={ButtonClose}></FormattedMessage>
                    </AkButton>
                    {/* 召回按钮 */}
                    {taskDetail.CurrentTask
                        && ((taskDetail.CurrentTask.Flags & TaskFlagsEnum.RecallTask) === TaskFlagsEnum.RecallTask)
                        && (taskDetail.CurrentTask.Outcome || canRecallOutComs.findIndex(f => f === taskDetail.CurrentTask.Outcome) > -1)
                        && (taskDetail.AppStatus === ApplicationStatusEnum.Running)//任务运行中
                        && taskDetail.CurrentTask.AssigneeID === AkContext.getUser().AccountID//如果是自己的任务并可以召回,则显示召回功能
                        && !(taskDetail.CurrentTask.RecalledID !== "0" || ((taskDetail.CurrentTask.Flags & TaskFlagsEnum.NoCanRecallTask) === TaskFlagsEnum.NoCanRecallTask))
                        && (!hasRecalled) ? <AkButton className="pull-right cancel" style={fontColor}
                            icon="recallback" onClick={() => { this.onRecall() }} disabled={loading}>
                            <FormattedMessage id={ButtonRecall}></FormattedMessage>
                        </AkButton> : null
                    }
                    {taskDetail.CurrentTask && !taskDetail.CurrentTask.Outcome && taskDetail.CurrentTask.AssigneeID !== "0" ?
                        <AkButton className="pull-right cancel" style={fontColor}
                            icon="save" onClick={() => { this.onSaveVariable("") }} disabled={loading}>
                            <FormattedMessage id={ButtonSave}></FormattedMessage>
                        </AkButton>
                        : null}
                    {isclaim ? <AkButton className="pull-right cancel" style={fontColor}
                        icon="receive" onClick={() => this.onReceive()} disabled={loading}>
                        <FormattedMessage id={ButtonReceive}></FormattedMessage>
                    </AkButton> : null}
                </AkCol>
            </div>
            <AkSpin spinning={loading}>
                <div className="ak-workflow-container">
                    {this.props.renderCustomHeader && this.props.renderCustomHeader()}
                    <StatusBar taskDetail={taskDetail} />
                    {disabledApplicationInfo ? null : <YeeUserContent display taskDetail={taskDetail} />}
                    {this.props.children}
                    {disabledAttachment ? null : <AkAttachment readonly={true} taskDetail={taskDetail}></AkAttachment>}

                    {this.renderApprovePanel()}
                    {/* {this.renderForward()}  */}
                    {WorkflowFormHelper.renderAddAssisign(this, this.taskID)}
                    <div className="ak-form-title mb15 toggle-title" onClick={() => { this.setState({ toggle: !toggle }) }}>
                        <span className="title-bluespan"></span>
                        <FormattedMessage id={ApplyContentLocale.LogTitle} />
                        <AkIcon type={this.state.toggle ? "up" : "down"}></AkIcon>
                    </div>
                    {toggle ? <AkFormHistoryLog taskID={this.taskID} appID={taskDetail.ApplicationID} isPreview={false} type="common"></AkFormHistoryLog> : null}
                    {this.props.renderCustomFooter && this.props.renderCustomFooter()}
                </div>
            </AkSpin>
        </div>
    }
}
