import * as React from "react";
import { FormattedMessage } from "react-intl";
import { FormAction, TaskAction, AddAssisignAction } from '../../actions/index';
import { AddLibraryDocsRequest, AddLibraryDocsType, AkIdentity, ContentListApi, DeleteDocumentRequest, GetApplicantDetailRequest, PostProcDraftRequest, PostStartProcInstByKeyRequest, ProcDraftsAPI, ProcInstAPI, PutTaskChangeRequest, PutTaskEndorseRequest, TaskAPI, TaskInfo, TaskStatusEnum, TaskFlagsEnum } from "../../api";
import { ApplyContentLocale, CommonLocale } from "../../locales";
import { LibraryListLocal, NoticeLocale } from '../../locales/localeid';
import { AkConstants, AkGlobal, AkUtil, FileUploadMethod, FlowcraftCommon, FormHelper, RouterProps, AkContext } from "../../util";
import { AkCol, AkFormComponentProps, AkMessage, AkModal, AkNotification, AkRow } from "../controls";
import { AkIdentityPicker } from "../identity";
import RequisitionContent, { SubmitStatus, SaveStatus } from "./requisition-content";

export interface AkFormArrayFieldDesc {
    arrField: string; //form中数组变量
    index: number; //字段所在行
    field: string; //行中field名称
}

export enum AkFormRequestTypes {
    ApplicationSubmit,
    ApplicationSave,
    TaskSave,
    TaskReject,
    TaskComplete,
    TaskOk
}

export enum ListcraftFormActions {
    Save,
    Delete
}

export enum ProcModelFlags {
    hide = 1
}

const PROP_CONTROL_VISIBILITY = "control_visibility";
const PROP_LIST_FIELD = "list_field";
const PROP_list_fields = "list-fields";
const PROP_CONTROL_DYNAMIC_STYLE = "control_dynamic_style";

export const FORM_PROP_CONTENTLIST = "____customListFields";
export const FORM_PROP_CONTENTLIST_PREFIX = FORM_PROP_CONTENTLIST + "_";
export const TEMPORARY_VARIABLES = "__temporaryvariables";

/**临时变量的form前缀 */
export const TEMP_VAR_PREFIX = "__temp_";
class FormCacheObj {
    cache = {}
    timerCache;

    getValueCache() {
        return this.cache;
    }
    setValueCache(values) {
        this.cache = { ...this.cache, ...values };
    }
    clearValueCache() {
        this.cache = undefined;
    }

    getTimerCache() {
        return this.timerCache;
    }
    setTimerCache(timer) {
        this.timerCache = timer;
    }
}

const formCacheInst = new FormCacheObj();

function executeDelayUpdate(form) {
    const values = formCacheInst.getValueCache();
    if (values) {
        form.setFieldsValue(values);
    }
    formCacheInst.clearValueCache();
    formCacheInst.setTimerCache(undefined);
}

/**
 * Workflow Form Helper functions
 *
 * @export
 * @class WorkflowFormHelper
 */
export class WorkflowFormHelper {

    /**
     * 移除内容列表前缀
     * @param name 
     */
    public static removeContentListPrefix(name) {
        let rs;
        if (name) {
            let s = name.split(FORM_PROP_CONTENTLIST_PREFIX);
            if (s.length > 1) {
                rs = s[1];
            }
        }
        return rs;
    }

    /**
     * Go back functions
     */
    public static goBack = () => {
        FlowcraftCommon.goBack();
    }

    public static setIntegerFieldValue = FormHelper.setIntegerFieldValue;
    public static setFloatFieldValue = FormHelper.setFloatFieldValue;
    public static setOneOrHalfValue = FormHelper.setOneOrHalfValue;
    public static pleaseSelectMessage = FormHelper.pleaseSelectMessage;
    public static pleaseInputMessage = FormHelper.pleaseInputMessage;
    public static ruleForRequire = FormHelper.ruleForRequire;
    public static ruleForRegExp = FormHelper.ruleForRegExp;
    public static ruleForInteger = FormHelper.ruleForInteger;
    public static ruleForNumber = FormHelper.ruleForNumber;
    public static ruleForEmail = FormHelper.ruleForEmail;
    public static ruleForBankAccount = FormHelper.ruleForBankAccount;
    public static ruleForURL = FormHelper.ruleForURL;
    public static ruleForComparisonNumber = FormHelper.ruleForComparisonNumber;
    public static ruleForComparisonLength = FormHelper.ruleForComparisonLength;
    public static ruleForRangeFrom = FormHelper.ruleForRangeFrom;
    public static ruleForRangeTo = FormHelper.ruleForRangeTo;
    public static rowLayout = FormHelper.rowLayout;
    public static colLabelLayout = FormHelper.colLabelLayout;
    public static colControlLayout = FormHelper.colControlLayout;
    public static largeControlLayout = FormHelper.largeControlLayout;
    public static renderSectionTitle = FormHelper.renderSectionTitle;
    public static renderSubSectionTitle = FormHelper.renderSubSectionTitle;
    public static renderFormItem = FormHelper.renderFormItem;
    public static renderFormItemLabel = FormHelper.renderFormItemLabel;
    public static renderFormItemControl = FormHelper.renderFormItemControl;
    public static renderFormItemInModal = FormHelper.renderFormItemInModal;
    public static renderApprovalItem = FormHelper.renderApprovalItem;

    public static deserializeVariables(value, type?: "formcraft") {

        const parse = (str) => {
            try {
                return JSON.parse(str);
            }
            catch {
                return str;
            }
        }

        // JSON parse value
        const variables = value ? (typeof value === "string" ? JSON.parse(value) : {}) : {};
        for (const key in variables) {
            if (variables.hasOwnProperty(key)) {
                const element = variables[key] + "";
                if (element === "null") {
                    variables[key] = null;
                }
                // JSON parse for string
                if (element && element.startsWith("\"") && element.endsWith("\"")) {
                    variables[key] = parse(element);
                }
                // JSON parse Array
                if (element && element.startsWith("[") && element.endsWith("]")) {
                    if (type === "formcraft") {
                        variables[key] = parse(element);
                    } else if (type === undefined) {
                        //项目表单中，增加唯一ID,tbd:remove this logic
                        var arrayObj = parse(element);
                        arrayObj.map(v => {
                            if (v !== null && typeof v === "object") {
                                v.ID = v.ID || AkUtil.guid();
                            }
                        });
                        variables[key] = arrayObj;
                    }
                }

                //JSON parse Object
                if (element && element.startsWith("{") && element.endsWith("}")) {
                    variables[key] = parse(element);
                }
            }
        }
        return variables;
    }

    public static serializeVariables(value: Object, type?: "formcraft"): string {
        let result = {};
        if (value) {
            for (var key in value) {
                if (value.hasOwnProperty(key)) {
                    var element = value[key];
                    if (element === null) {
                        //避免出现JSON.stringify后字符串null("null")的情况
                        result[key] = null;
                        continue;
                    }
                    if (element !== undefined) {
                        if (typeof element === "object") {
                            if (type === "formcraft") {
                                //自定义表单，移除删除的列
                                if (element && Array.isArray(element)) {
                                    element = element.filter(v => v && !v["_del"])
                                    if (element.length === 0) {
                                        result[key] = null;
                                    } else {
                                        result[key] = JSON.stringify(element);
                                    }
                                } else {
                                    result[key] = JSON.stringify(element);
                                }
                            } else if (type === undefined) {
                                //项目表单中，移除列表ID字段
                                if (element && Array.isArray(element)) {
                                    result[key] = JSON.stringify(element.map(v => {
                                        if (v !== null && typeof v === "object") {
                                            delete v.ID;
                                        }
                                        return v;
                                    }));
                                } else {
                                    result[key] = JSON.stringify(element);
                                }
                            }
                        } else {
                            result[key] = element;
                        }
                    }
                    else {
                        //undefined序列化后会丢失，重置为null
                        result[key] = null;
                    }
                }
            }
        }
        return JSON.stringify(result);
    }

    /**
     * 判断form中是否有该field
     * @param form
     * @param fieldId
     */
    public static containField(form, fieldId) {
        let fields = form.getFieldsValue();
        return fieldId in fields;
    }

    public static generateListUUIDs(uuid: any) {
        const uuids = [];
        for (let i = 0; i <= uuid; i++) {
            uuids.push(i);
        }
        return uuids;
    }

    /**
     * 设置form中的field值，如果不存在初始化该值
     * @param form
     * @param key
     * @param value
     */
    public static setFormField(form, field, value): any {
        let obj = {};
        if (form && field && value !== undefined) {
            let formValues = form.getFieldsValue();
            let lv;
            if (field in formValues) {
                lv = formValues[field];
            } else {
                form.getFieldDecorator(field, {});
            }

            if (lv !== value) {
                obj[field] = value;
                form.setFieldsValue(obj);
            }
        }
        return obj;
    }

    private static delayUpdate(form, fields) {
        // formCacheInst.setValueCache(fields);
        // if (formCacheInst.getTimerCache() === undefined) {
        //     let timer = setTimeout(()=>{ executeDelayUpdate(form)}, 500); 
        //     formCacheInst.setTimerCache(timer);
        // }
        AkGlobal.store.dispatch(FormAction.formDelayUpdate(form, fields));
        // WorkflowFormHelper.formUpdateCache = {...WorkflowFormHelper.formUpdateCache, ...fields};
        // if (WorkflowFormHelper.formUpdateTimer) {
        //     //已经有timer在运行，无需处理
        // } else {
        //     WorkflowFormHelper.formUpdateTimer = setTimeout(()=>{
        //         form.setFieldsValue(WorkflowFormHelper.formUpdateCache);
        //         WorkflowFormHelper.formUpdateCache = {};
        //         WorkflowFormHelper.formUpdateTimer = undefined;
        //     }, 500); //延迟1s更新数据
        // }
    }



    /**
     * 设置form中的field值，如果不存在初始化该值
     * @param form
     * @param fields {key:value,key1:value}
     * @return 返回需要更新的字段
     */
    public static setFormFields(form, fields, delay?: boolean): any {
        let needSet = false;
        let formExisting = form.getFieldsValue();
        let updatedFields = {};

        function requireInit(field, value) {
            needSet = true;
            form.getFieldDecorator(field, {});
            updatedFields[field] = value;
        }

        AkUtil.each(fields, (value, field) => {
            const desc = this.extractArrayFieldDesc(field);
            if (desc) {
                //当前是数组中对象更新
                if (!(desc.arrField in formExisting)) {
                    //数组变量不存在，初始化字段
                    requireInit(field, value);
                } else {
                    //获取数组变量的值
                    const arr = formExisting[desc.arrField];
                    if (arr.length <= desc.index) {
                        //当前数组长度小于目标索引，初始化字段
                        requireInit(field, value);
                    } else {
                        const row = arr[desc.index];
                        if (!(desc.field in row) || !form.getFieldValue(field)) {
                            //当前字段不存在于列，初始化字段
                            requireInit(field, value);
                        } else if (row[desc.field] !== value) {
                            //值不同时才更新
                            needSet = true;
                            updatedFields[field] = value;
                        }
                    }
                }
            } else {
                //普通字段更新
                if (!(field in formExisting)) {
                    requireInit(field, value);
                } else if (!AkUtil.isEqual(formExisting[field], value)) {
                    needSet = true;
                    updatedFields[field] = value;
                }
            }
        });
        if (needSet) {
            if (delay) {
                this.delayUpdate(form, updatedFields);
            } else {
                form.setFieldsValue(updatedFields);
            }
        }
        return updatedFields;
    }

    /**
     * 根据数组中的字段key获取对应的说明
     * @param fieldKey arr[0].field
     */
    public static extractArrayFieldDesc(fieldKey): AkFormArrayFieldDesc {
        let result: AkFormArrayFieldDesc;
        const arrStart = fieldKey ? fieldKey.indexOf("[") : 0;//数组标开始
        if (arrStart > 0) {
            //变量为数组对象
            const arrVar = fieldKey.substr(0, arrStart); //list 变量
            const arrEnd = fieldKey.indexOf("]");//数组标结束
            const arrIdx = fieldKey.substring(arrStart + 1, arrEnd); //数组下标
            const pointIdx = fieldKey.indexOf("."); //点的位置            
            const fieldVar = pointIdx < 1 ? null : fieldKey.substr(pointIdx + 1);
            result = { arrField: arrVar, index: arrIdx, field: fieldVar }
        }
        return result;
    }

    /**
     * 保存
     *
     * @param self 需要执行流程保存操作的组件本身（this）
     *
     * @param saveRequest 保存需要的参数
     *
     * @param type 判断是公用流程还是自定义流程
     */
    public static onSave(self, saveRequest: PostProcDraftRequest, user: "common" | "flowcraft") {
        if (self.draftID) {
            if (user === "common" && self.props.afterSave) {
                ProcDraftsAPI.putProcDrafts(saveRequest).then(data => {
                    this.handleResponse(data, self, user)
                    self.props.afterSave(self.state.taskDetail, data.Data);
                })
            }
            // else {
            //     ProcDraftsAPI.putProcDrafts(saveRequest).then(data => {
            //         this.handleResponse(data, self, user, null, null, "save")
            //         if (self.props.afterSave) {
            //             self.props.afterSave(self.state.taskDetail, data.Data);
            //         }
            //     })
            // }
        } else {
            ProcDraftsAPI.postProcDrafts(saveRequest).then(data => {
                if (user === "common") {
                    if (self.props.afterSave) {
                        self.props.afterSave(self.state.taskDetail, data.Data);
                    }
                    this.handleResponse(data, self, user, (self) => self.draftID = data.Data);
                }
                // if (user === "flowcraft") {
                //     if (data.Status === 0) {
                //         self.draftID = data.Data;
                //     }
                //     this.handleResponse(data, self, user, null, null, "save");
                // }
            });
        }
    }

    public static changeFormVariables(formVariables, variables) {
        if (variables && variables[FORM_PROP_CONTENTLIST]) {
            let newformVariables = {};
            newformVariables[FORM_PROP_CONTENTLIST] = {};
            for (var item in formVariables) {
                if (item.startsWith(FORM_PROP_CONTENTLIST_PREFIX)) {
                    if (formVariables[item] === null ||
                        formVariables[item] === undefined
                    ) {
                        if (variables[FORM_PROP_CONTENTLIST][WorkflowFormHelper.removeContentListPrefix(item)]) {
                            newformVariables[FORM_PROP_CONTENTLIST][WorkflowFormHelper.removeContentListPrefix(item)] = JSON.stringify(formVariables[item]);
                        } else {
                            continue;
                        }
                    }
                    else if (typeof formVariables[item] === "object") {
                        newformVariables[FORM_PROP_CONTENTLIST][WorkflowFormHelper.removeContentListPrefix(item)] = JSON.stringify(formVariables[item]);
                    } else {
                        newformVariables[FORM_PROP_CONTENTLIST][WorkflowFormHelper.removeContentListPrefix(item)] = formVariables[item];
                    }
                } else {
                    newformVariables[item] = formVariables[item];
                }
            }
            return newformVariables;
        } else {
            return formVariables;
        }
    }

    public static stringflyValues(values) {
        let submitValues = {};
        for (const item in values) {
            if (item.startsWith(TEMP_VAR_PREFIX)) {
                continue;
            }


            if (values[item] && values[item].file_delete) {
                // FileUploadMethod.deleteFiles(values[item].file_delete);
                if (!Array.isArray(values[item])) {
                    values[item] = null;
                } else {
                    values[item] = JSON.stringify(values[item]);
                }
            } else {
                if (typeof values[item] === "object" && values[item] !== null) {
                    values[item] = JSON.stringify(values[item]);
                }
                if (values[item] === undefined) {
                    values[item] = null;
                }
            }
            submitValues[item] = values[item];
        }
        return submitValues
    }

    //子文档库自定义地址文件上传
    public static documentCustomPathUpload(temporaryvariables: any[]) {
        const ERROR_CODE_PREFIX: string = "crafts.server.status.";
        const { formatMessage } = AkGlobal.intl;
        const res = temporaryvariables.map(i => {
            return new Promise(async (resolve, reject) => {
                const Req1 = {
                    AppID: i.AppID,
                    ListID: i.ListID,
                    path: i.path,
                    isCreate: true,
                }
                //添加指定文档库文件夹
                const Res1 = await ContentListApi.checkOrAddLibraryPath(Req1);
                if (Res1.Status === 0 && Res1.Data) {
                    const Req2: AddLibraryDocsRequest = {
                        AppID: i.AppID,
                        ListID: i.ListID,
                        Type: AddLibraryDocsType.Rename,
                        ParentID: Res1.Data,
                        DicList: i.values.map(item => {
                            return WorkflowFormHelper.stringflyValues(item)
                        })
                    }
                    //批量添加文件
                    const Res2 = await ContentListApi.AddLibraryDocs(Req2);
                    Res2.ServerMessage = Req1.path;
                    resolve(Res2)
                } else {
                    //指定文件夹添加失败
                    reject(Req1.path)
                }
            })
        });
        Promise.all(res).then(datas => {
            const errs: any = datas.filter((i: any) => i.Status !== 0);
            if (errs.length > 0) {
                //文件添加失败
                errs.forEach(err => {
                    let description = null;
                    switch (err.Status) {
                        case 540013:
                            if (err.Message === "UniqueName") {
                                description = formatMessage({ id: LibraryListLocal.FileExistedMessage });
                            } else {
                                description = formatMessage({ id: ERROR_CODE_PREFIX + err.Status }, { name: err.Message });
                            }
                            break;
                        case 540016:
                        case 401:
                            description = formatMessage({ id: ERROR_CODE_PREFIX + err.Status });
                            break;
                        default:
                            description = formatMessage({ id: CommonLocale.AddFail });
                    }
                    AkNotification.error({
                        message: formatMessage({ id: CommonLocale.Tip }),
                        description: err.ServerMessage + ":" + description
                    });
                });
            } else {
                //全部成功 
                AkMessage.success(AkGlobal.intl.formatMessage({ id: LibraryListLocal.AddFilesSuccess }));
            }
        }).catch(path => {
            //指定文件夹添加失败
            AkNotification.error({
                message: formatMessage({ id: CommonLocale.Tip }),
                description: formatMessage({ id: LibraryListLocal.AddFolderFail }, { name: path })
            });
        })
    }

    static getControlVisibility(control, binding): boolean {
        const desc = WorkflowFormHelper.extractArrayFieldDesc(binding);
        let index = desc ? desc.index : -1;
        if (control.attrs
            && control.attrs[PROP_CONTROL_VISIBILITY]
            && (control.attrs[PROP_CONTROL_VISIBILITY][index + "parent"] === -1
                || control.attrs[PROP_CONTROL_VISIBILITY][index] < 0
                || (index >= 0 && control.attrs[PROP_CONTROL_VISIBILITY][-1] && control.attrs[PROP_CONTROL_VISIBILITY][-1] < 0))) {
            return false;
        }
        return true;
    }

    public static processFormErr(context, err) {
        //处理隐藏的控件
        var errMsgs = {};

        const processErrObj = (errObj, control) => {
            //列字段
            let hasErr = false;
            if (errObj.errors && errObj.errors.length > 0) {
                errObj.errors.forEach(e => {
                    if (this.getControlVisibility(control, e.field)) {
                        //控件显示，添加错误消息
                        errMsgs[e.message] = true;
                        hasErr = true;
                    }
                })
            }
            return hasErr;
        }

        const processControl = (control) => {
            if (this.getControlVisibility(control, control.binding)) {
                if (control.binding) {
                    let errDetail = err[control.binding];
                    if (errDetail) {
                        //该控件有错误
                        if (AkUtil.isArray(errDetail)) {
                            //对象是列表控件，字段出错
                            //if (this.getControlVisibility(control, control.binding)) {
                            //该列表是显示状态才执行内部的验证
                            let hasErr = false;
                            for (var i = 0; i < errDetail.length; i++) {
                                let errObjs = errDetail[i];
                                for (var fieldName in errObjs) {
                                    let fields = control.attrs[PROP_list_fields];
                                    let field = fields.find(f => f.id === fieldName);
                                    if (field) {
                                        let errObj = errObjs[fieldName];

                                        if (processErrObj(errObj, field.control)) {
                                            hasErr = true;
                                        }
                                        // //列字段
                                        // if (errObj.errors && errObj.errors.length > 0) {
                                        //     errObj.errors.forEach(e => {
                                        //         if (this.getControlVisibility(field.control, e.field)) {
                                        //             //控件显示，添加错误消息
                                        //             errMsgs.push(e.message);
                                        //         }
                                        //     })
                                        // }
                                    }
                                }
                            }
                            if (hasErr) {
                                //如果列表控件中有控件出错，需要重新render列表控件
                                control.attrs[PROP_CONTROL_DYNAMIC_STYLE] = { ...(control.attrs[PROP_CONTROL_DYNAMIC_STYLE] || {}) }
                            }
                            //}
                        } else {
                            processErrObj(errDetail, control);
                        }
                    }
                } else if (control.children) {
                    //有binding的控件没有children
                    for (var i = 0; i < control.children.length; i++) {
                        processControl(control.children[i]);
                    }
                }
            } else {
                //如果当前控件不显示，则不处理其内部所有其他控件
            }
        }

        if (err) {
            processControl(context.formDef);
        }



        // for (var cid in controlDict) {
        //     let control = controlDict[cid];
        //     if (control && control.binding) {
        //         let errDetail = err[control.binding];
        //         if (errDetail) {
        //             //该控件有错误
        //             if (AkUtil.isArray(errDetail)) {
        //                 //对象是列表控件，字段出错
        //                 if (this.getControlVisibility(control, control.binding)) {
        //                     //该列表是显示状态才执行内部的验证
        //                     for (var i = 0; i < errDetail.length; i++) {
        //                         let errObjs = errDetail[i];
        //                         for (var fieldName in errObjs) {
        //                             let fields = control.attrs[PROP_list_fields];
        //                             let field = fields.find(f => f.id === fieldName);
        //                             if (field) {
        //                                 let errObj = errObjs[fieldName];

        //                                 processErrObj(errObj, field.control);
        //                                 // //列字段
        //                                 // if (errObj.errors && errObj.errors.length > 0) {
        //                                 //     errObj.errors.forEach(e => {
        //                                 //         if (this.getControlVisibility(field.control, e.field)) {
        //                                 //             //控件显示，添加错误消息
        //                                 //             errMsgs.push(e.message);
        //                                 //         }
        //                                 //     })
        //                                 // }
        //                             }
        //                         }
        //                     }
        //                 }
        //             } else {
        //                 processErrObj(errDetail, control);
        //             }
        //         }
        //     }
        // }

        let msgKeys = Object.keys(errMsgs);
        if (msgKeys.length > 0) {
            //存在具体错误
            let msg = <div>{msgKeys.map((m, i) => {
                return <div key={i}>{m}</div>;
            })}</div>
            AkNotification.warning({ message: AkGlobal.intl.formatMessage({ id: CommonLocale.FormValidateMessage }), description: msg });
            // AkMessage.warning(msg);
            return false;
        }
        return true;
    }

    public static async formValidate(context, type: AkFormRequestTypes, ignoreValid?: boolean) {
        const fm = AkGlobal.intl.formatMessage;
        return new Promise((resolve, reject) => {
            if (ignoreValid || type === AkFormRequestTypes.ApplicationSave || type === AkFormRequestTypes.TaskSave) {
                //不验证或者保存操作
                resolve(true)
            } else {
                if (context.taskDetail) {
                    if (!context.taskDetail.ApplicantInfo || !context.taskDetail.ApplicantInfo.ApplicantID) {
                        AkNotification.warning({ message: fm({ id: CommonLocale.FormValidateMessage }), description: fm({ id: CommonLocale.FormValidateApplicant }) });
                        resolve(false);
                        return;
                    }
                }
                context.form.validateFieldsAndScroll(null, { force: true }, (err, values) => {
                    if (err) {
                        if (!WorkflowFormHelper.processFormErr(context, err)) {
                            resolve(false);
                        } else {
                            resolve(true);
                        }
                    } else {
                        resolve(true);
                    }
                });
            }
        });
    }

    public static onRequestHandler(context, type: AkFormRequestTypes, afterRequest?: () => void, comment?: string, ignoreValid?: boolean) {

        const _this = this;
        const requesthandler = async () => {
            const { form, taskDetail, defKey, defID, appID, draftID, formDef } = context;
            const { formatMessage } = AkGlobal.intl;
            console.log("start process submit event");
            let valid = await this.formValidate(context, type, ignoreValid);
            if (!valid) {
                afterRequest && afterRequest();
                return;
            }

            let formVariables = form.getFieldsValue();
            if (type === AkFormRequestTypes.ApplicationSubmit ||
                type === AkFormRequestTypes.TaskOk ||
                type === AkFormRequestTypes.TaskComplete
            ) {
                const temporaryvariables = formVariables[TEMPORARY_VARIABLES];
                delete formVariables[TEMPORARY_VARIABLES];
                temporaryvariables && WorkflowFormHelper.documentCustomPathUpload(temporaryvariables);
            }
            formVariables = WorkflowFormHelper.deleteFiles(formVariables);
            formVariables = WorkflowFormHelper.changeFormVariables(formVariables, taskDetail.Variables);
            //merge当前表单和服务器端taskDetail.Variables中的值
            for (var key in formVariables) {
                if (formVariables[key] === undefined) {
                    //deepclone 时，undefined会被移除
                    formVariables[key] = null;
                }
            }

            let submitValues = AkUtil.deepClone({ ...taskDetail.Variables, ...formVariables });
            const ext2 = JSON.stringify(WorkflowFormHelper.buildApplicationExt2(submitValues));
            let variables = WorkflowFormHelper.serializeVariables(submitValues, "formcraft");
            let params, action, success;
            switch (type) {
                case AkFormRequestTypes.ApplicationSubmit:
                    action = ProcInstAPI.postStartProcInstByKey;
                    params = {
                        Key: defKey,
                        Variables: variables,
                        ApplicantID: taskDetail.ApplicantInfo && taskDetail.ApplicantInfo.ApplicantID,
                        ProcDraftID: draftID,
                        ApplicationID: appID,
                        applicationext2: ext2
                    }
                    success = () => {
                        setTimeout(() => {
                            AkGlobal.store.dispatch(TaskAction.requestTaskCount());
                            this.goBack()
                        }, 2000)
                    }
                    break;
                case AkFormRequestTypes.ApplicationSave:
                    params = {
                        ProcDefID: defID,
                        DefKey: defKey,
                        FormData: variables,
                        ProcDraftID: draftID,
                        ApplicantID: taskDetail.ApplicantInfo && taskDetail.ApplicantInfo.ApplicantID
                    }
                    action = draftID ? ProcDraftsAPI.putProcDrafts : ProcDraftsAPI.postProcDrafts;
                    if (!draftID) {
                        success = (data) => {
                            context.draftID = data.Data;
                        }
                    }
                    break;
                case AkFormRequestTypes.TaskComplete:
                    success = () => {
                        setTimeout(_this.goBack, 2000)
                    }
                    action = TaskAPI.putTaskHandle;
                    params = {
                        TaskID: taskDetail.TaskInfo.TaskID,
                        Outcome: "Completed",
                        Variables: variables,
                        Comment: comment,
                        applicationext2: JSON.stringify(ext2)
                    }
                    break;
                case AkFormRequestTypes.TaskReject:
                    success = () => {
                        setTimeout(_this.goBack, 2000)
                    }
                    action = TaskAPI.putTaskHandle;
                    params = {
                        TaskID: taskDetail.TaskInfo.TaskID,
                        Outcome: "Rejected",
                        Variables: variables,
                        Comment: comment
                    }
                    break;
                case AkFormRequestTypes.TaskOk:
                    success = () => {
                        setTimeout(_this.goBack, 2000)
                    }
                    action = TaskAPI.putTaskHandle;
                    params = {
                        TaskID: taskDetail.TaskInfo.TaskID,
                        Outcome: "Approved",
                        Variables: variables,
                        Comment: comment,
                        applicationext2: ext2
                    }
                    break;
                case AkFormRequestTypes.TaskSave:
                    action = TaskAPI.putTaskHandle;
                    params = {
                        TaskID: taskDetail.TaskInfo.TaskID,
                        Outcome: "SaveVariable",
                        Variables: variables,
                        Comment: comment
                    }
                    break;
            }

            action(params).then(data => {
                if (data.Status === 0) {
                    AkMessage.success(formatMessage({ id: AkConstants.CommonServerStatusLocale + data.Status }));
                    success && success(data);
                } else {
                    let msg = data.Message;
                    if (data.Status === 110000) {
                        let name = "";
                        let json = JSON.parse(data.Message);
                        for (var item in json) {//考虑多个唯一值情况
                            let obj = formDef.children.find(f => f.binding === item);
                            name += obj ? (obj.label + "、") : "";
                        }
                        name = name.substring(0, name.length - 1);
                        msg = formatMessage({ id: ApplyContentLocale.TipUniqueValue }, { name: name });
                    } else if (data.Status === 120025) {
                        msg = formatMessage({ id: ApplyContentLocale.BudgetExceed });
                    }
                    AkNotification.warning({
                        message: formatMessage({ id: CommonLocale.Tip }),
                        description: msg
                    });
                }
                afterRequest && afterRequest();
            })

            // form.validateFieldsAndScroll(ignoreValid ? [] : null, { force: true }, (err, values) => {
            //     if ((type !== AkFormRequestTypes.ApplicationSave && type !== AkFormRequestTypes.TaskReject) || !ignoreValid) {
            //         if (err) {
            //             if (!WorkflowFormHelper.processFormErr(context, err)) {
            //                 afterRequest && afterRequest();
            //                 return;
            //             }
            //         }
            //     }


            // })
        }
        AkGlobal.store.dispatch(FormAction.checkFormCache(requesthandler));
    }

    public static onSubmitHandler(self) {
        const { form, taskDetail, defKey, defID, appID, formDef } = self.props;
        const submithandler = () => {
            //force默认为false，控件的验证结果上一次是true，将会跳过验证
            //force为true，无论上次验证结果是什么，都会执行再次验证
            form.validateFieldsAndScroll({ force: true }, (err, values) => {

                // WorkflowFormHelper.onSubmit(self, startRequest, "flowcraft", self.upTaskCount);
            });
        }
    }

    public static onSaveHandler(self) {
        // const { form, taskDetail, defKey, defID, appID, formDef } = self.props;
        // const savehandler = () => {
        //     let formVariables = form.getFieldsValue();
        //     formVariables = WorkflowFormHelper.changeFormVariables(formVariables);
        //     formVariables = WorkflowFormHelper.deleteFiles(formVariables);

        //     //merge当前表单和服务器端taskDetail.Variables中的值
        //     const values = { ...taskDetail.Variables, ...formVariables };
        //     let variables = WorkflowFormHelper.serializeVariables(values, "formcraft");
        //     const saveRequest: PostProcDraftRequest = {
        //         ProcDefID: defID,
        //         DefKey: defKey,
        //         FormData: variables,
        //         ProcDraftID: self.draftID,
        //         ApplicantID: taskDetail.ApplicantInfo && taskDetail.ApplicantInfo.ApplicantID
        //     };
        //     WorkflowFormHelper.onSave(self, saveRequest, "flowcraft");
        // }
        // AkGlobal.store.dispatch(FormAction.checkFormCache(savehandler));
    }

    /**
     * 提交
     *
     * @param self 需要执行流程提交操作的组件本身（this）
     *
     * @param startRequest 提交需要的参数
     *
     * @param type 判断是公用流程还是自定义流程
     */
    public static onSubmit(self, startRequest: PostStartProcInstByKeyRequest, user: "common" | "flowcraft", callBack?: any) {
        self.setLoading(true);
        ProcInstAPI.postStartProcInstByKey(startRequest).then(data => {
            let func = null;
            if (user === "common") {
                func = (self) => {
                    if (user === "common" && self.props.afterSubmit) {
                        self.props.afterSubmit(self.state.taskDetail, data.Data);
                    }
                    setTimeout(() => this.goBack(), 2000);
                }
            }
            // if (user === "flowcraft") {
            //     if (callBack) {
            //         func = callBack;//无异常时调用回调
            //     } else {
            //         func = () => {
            //             setTimeout(() => FlowcraftCommon.goBack(), 2000);
            //         }
            //     }
            // }
            this.handleResponse(data, self, user, func, null, "submit");
        });
    }

    //当保存或提交时从文档库中删除文件
    public static deleteFiles(variables) {
        let deleteRequest: DeleteDocumentRequest = {
            AppID: 0,
            LibraryID: "",
            ListDataIDs: []
        };
        let values = variables;
        // var values = WorkflowFormHelper.deserializeVariables(variables);
        for (let item in values) {
            if (values[item] && values[item]["file_delete"]) {
                //Edit By Evan :has error so ...
                //let deleteFiles=values[item]["file_delete"] as any;
                //FileUploadMethod.deleteFiles(deleteFiles);
                let temp = values[item]["file_delete"] as DeleteDocumentRequest;
                deleteRequest.AppID = temp.AppID;
                deleteRequest.LibraryID = temp.LibraryID;
                temp.ListDataIDs.forEach((item, index) => {
                    if (!deleteRequest.ListDataIDs || deleteRequest.ListDataIDs.indexOf(item) === -1) {
                        deleteRequest.ListDataIDs.push(item);
                    }
                })
                delete values[item]["file_delete"];
                // if (!AkUtil.isArray(values[item])) {
                //     values[item] = [];
                // }
            }
        }
        if (deleteRequest.ListDataIDs && deleteRequest.ListDataIDs.length > 0) {
            FileUploadMethod.deleteFiles(deleteRequest);
        }
        //return WorkflowFormHelper.serializeVariables(values);
        return values;
    }

    /**
     * 取消/终止
     *
     * @param self 需要执行流程取消操作的组件本身（this）
     *
     * @param startRequest 取消需要的参数
     *
     * @param type 判断是公用流程还是自定义流程
     */
    public static onCancel(self, cancelRequest: GetApplicantDetailRequest, user: "common" | "flowcraft") {
        let format = AkGlobal.intl.formatMessage;
        let topThis = this;
        AkModal.confirm({
            title: format({ id: CommonLocale.Tip }),
            content: format({ id: ApplyContentLocale.ModalIsCancelApply }, { name: "" }),
            onOk() {
                self.setLoading(true);
                ProcInstAPI.putApplicationCancel(cancelRequest).then(data => {
                    topThis.handleResponse(data, self, user, (self) => {
                        setTimeout(() => topThis.goBack(), 2000);
                    });
                });
            },
            onCancel() {
                self.setLoading(false);
            }
        })
    }

    /**
     * 撤销
     *
     * @param self 需要执行流程撤销操作的组件本身（this）
     *
     * @param startRequest 撤销需要的参数
     *
     * @param type 判断是公用流程还是自定义流程
     */
    public static onRevoke(self, revokeRequest: GetApplicantDetailRequest, user: "common" | "flowcraft") {
        let format = AkGlobal.intl.formatMessage;
        let topThis = this;
        AkModal.confirm({
            title: format({ id: CommonLocale.Tip }),
            content: format({ id: ApplyContentLocale.ModalIsRecallApply }, { name: "" }),
            onOk() {
                self.setLoading(true);
                ProcInstAPI.putApplicationRevoke(revokeRequest).then(data => {
                    topThis.handleResponse(data, self, user, (self) => {
                        setTimeout(() => topThis.goBack(), 2000);
                    });
                });
            },
            onCancel() {
                self.setLoading(false);
            }
        })
    }

    /**
     * 转签
     *
     * @param self 需要执行流程转签操作的组件本身（this）
     *
     * @param request 转签需要的参数
     *
     * @param type 判断是公用流程还是自定义流程
     */
    public static onForward(self, request, user: "common" | "flowcraft") {
        self.setLoading(true);
        TaskAPI.putChangeTask(request).then(data => this.handleResponse(data, self, user, (x) => {
            if (self.props.forward && self.props.forward.after) self.props.forward.after(self.state.taskDetail, data);
            setTimeout(() => this.goBack(), 2000);
        }));
    }

    /**
     *
     * @param self 需要执行流程领用操作的组件本身（this）
     *
     * @param request 领用需要的参数
     *
     * @param type 判断是公用流程还是自定义流程
     */
    public static onReceive(self, receiveRequest, user: "common" | "flowcraft", callBack?: any) {
        self.setLoading(true);
        let error = null;
        if (user === "flowcraft") {
            error = (self) => { self.setState({ loading: false }) }
        }
        TaskAPI.putClaimTask(receiveRequest).then(data => this.handleResponse(data, self, user, (self) => {
            if (callBack) {
                callBack();
            } else {
                setTimeout(() => this.goBack(), 2000);
            }
        }, error));
    }


    public static executeApprovalAction(self, request, action, user: "common" | "flowcraft") {
        TaskAPI.putTaskHandle(request).then(data => {
            if (user === "common") {
                self.setLoading(true);
                this.handleResponse(data, self, user, (self) => {
                    if (action && action.after) action.after(self.state.taskDetail, data, self["comment"]);
                    setTimeout(() => this.goBack(), 2000);
                })
            }
            // if (user === "flowcraft") {
            //     this.handleResponse(data, self, user, () => {
            //         setTimeout(this.goBack(), 2000)
            //     });
            // }
        }
        );
    }

    /**
     * render加签Modal
     * @param self render加签Modal的组件
     *
     * @param taskID taskID
     */
    public static renderAddAssisign(self, taskID, callBack?: any) {
        let endorseUser: AkIdentity[] = [];
        let params: PutTaskEndorseRequest = {
            TaskID: taskID,
            AssigneeIDs: [],
            isCheckUser: true
        }
        const { formatMessage } = AkGlobal.intl;
        return self.state.showAddAssisign ? <AkModal title={formatMessage({ id: ApplyContentLocale.ButtonAddAssignee })}
            visible onOk={() => {
                params.AssigneeIDs = endorseUser.map((entry, index) => {
                    return entry.ID;
                });
                if (endorseUser && endorseUser.length > 0) {
                    this.onAddAssisign(self, params, endorseUser, callBack);
                } else {
                    AkNotification.warning({
                        message: formatMessage({ id: CommonLocale.Tip }),
                        description: formatMessage({ id: ApplyContentLocale.AddAssigneeTitle })
                    })
                }
            }} onCancel={() => { self.setState({ showAddAssisign: false }); }}>
            <AkRow type="flex" justify="end" align="middle" className="mb20">
                <AkCol span={6}>
                    <FormattedMessage id={ApplyContentLocale.PropsAssignee}></FormattedMessage>
                </AkCol>
                <AkCol span={18}>
                    <AkIdentityPicker multiple={true}
                        onChange={v => {
                            endorseUser = [];
                            (v as AkIdentity[]).map(item => {
                                endorseUser.push(item)
                            });
                        }} />
                </AkCol>
            </AkRow>
        </AkModal> : null;
    }

    /**
     * render转办Modal
     * @param self render转办Modal的组件
     *
     * @param taskID taskID
     */
    public static renderReassign(self, taskInfo: TaskInfo, callBack?: any) {
        let endorseUser: AkIdentity = null;
        let params: PutTaskChangeRequest = {
            TaskID: taskInfo ? taskInfo.TaskID : "",
            AssigneeID: ""
        }
        const { formatMessage } = AkGlobal.intl;
        return self.state.showAllowReassign ? <AkModal title={formatMessage({ id: ApplyContentLocale.ButtonForward })}
            visible onOk={() => {
                if (endorseUser) {
                    params.AssigneeID = endorseUser.ID;
                    this.onReassign(self, params, endorseUser, callBack);
                } else {
                    AkNotification.warning({
                        message: formatMessage({ id: CommonLocale.Tip }),
                        description: formatMessage({ id: ApplyContentLocale.ReassignTitle })
                    })
                }
            }} onCancel={() => { self.setState({ showAllowReassign: false }); }}>
            <AkRow type="flex" justify="space-around" align="middle" className="mb20">
                <AkCol span={6}>
                    <FormattedMessage id={ApplyContentLocale.PropsForwardAssignee}></FormattedMessage>
                </AkCol>
                <AkCol span={18}>
                    <AkIdentityPicker multiple={false}
                        onChange={(v: AkIdentity) => {
                            endorseUser = v;
                        }} />
                </AkCol>
            </AkRow>
        </AkModal> : null;
    }
    /**
     * 加签操作 验证该人员已存在当前任务节点中
     */
    public static onAddAssisign(self, params: PutTaskEndorseRequest, endorseUser: AkIdentity[], callBack?: any) {
        const { formatMessage } = AkGlobal.intl;
        TaskAPI.putEndorseTask(params).then(data => {
            let userNames = "";
            if (data.Status === 10025) {//加签人已存在
                let usersIDs = JSON.parse(data.Data) as string[];
                usersIDs.forEach((entry, index) => {
                    userNames += (index < (usersIDs.length - 1))
                        ? endorseUser.find(e => e.ID === entry).Name + " , "
                        : endorseUser.find(e => e.ID === entry).Name;
                });
                AkModal.confirm({
                    content: formatMessage({ id: ApplyContentLocale.TipAssigneeExist }, { name: userNames }),
                    onOk() { },
                    onCancel() { }
                })
            } else if (data.Status === 0) {
                endorseUser.forEach((entry, index) => {
                    userNames += (index < (endorseUser.length - 1)) ? entry.Name + " , " : entry.Name;
                });
                AkModal.confirm({
                    content: formatMessage({ id: ApplyContentLocale.TipAssigneeUnExist }, { name: userNames }),
                    onOk() {
                        params.isCheckUser = false;
                        TaskAPI.putEndorseTask(params).then(d => {
                            if (d.Status === 0) {
                                AkGlobal.store.dispatch(AddAssisignAction.historyLogLoad());//更新加签信息
                                AkMessage.success(formatMessage({ id: CommonLocale.TipOperationSuccess }));
                            } else {
                                AkNotification.error({
                                    message: AkGlobal.intl.formatMessage({ id: CommonLocale.Tip }),
                                    description: data.Message
                                });
                            }
                            callBack(d.Status === 0);
                        });
                    },
                    onCancel() { },
                })
            } else {
                AkNotification.warning({
                    message: formatMessage({ id: CommonLocale.Tip }),
                    description: data.Message
                })
            }
        });
        self.setState({ showAddAssisign: false });
    }

    /**
     * 转办操作
     */
    public static onReassign(self, params: PutTaskChangeRequest, endorseUser: AkIdentity, callBack?: any) {
        const { formatMessage } = AkGlobal.intl;
        if (self.props.taskInfo.AssigneeID === endorseUser.ID) {
            return AkNotification.warning({
                message: formatMessage({ id: CommonLocale.Tip }),
                description: formatMessage({ id: ApplyContentLocale.TipReassignUnAssign })
            });
        }
        AkModal.confirm({
            content: formatMessage({ id: ApplyContentLocale.TipReassignedTo }, { name: endorseUser.Name }),
            onOk() {
                TaskAPI.putChangeTask(params).then(data => {
                    callBack(data.Status === 0);
                    if (data.Status === 0) {
                        AkMessage.success(formatMessage({ id: CommonLocale.TipOperationSuccess }));
                    } else {
                        AkNotification.error({
                            message: formatMessage({ id: CommonLocale.Tip }),
                            description: data.Message
                        });
                    }
                });
            },
            onCancel() { }
        });
        self.setState({ showAllowReassign: false });
    }

    /**
     * 从变量的值解析budget对象
     * @param value
     */
    public static parseBudgetObj(value) {
        if (value && typeof value === "object") {
            const { EntityID, EntityType, SubjectID, OccDate, Amount, IsControlled, Balance } = value;
            if (EntityID && EntityType && SubjectID && OccDate && Amount && Amount > 0) {
                //合法的预算数据
                //&& Balance !== null && Balance !== undefined 没有预算的不提交到后台ext2
                if (IsControlled && Balance !== null && Balance !== undefined) {
                    //冻结预算后，清空,已避免重复提交
                    value.EntityID = undefined;
                    value.EntityType = undefined;
                    value.SubjectID = undefined;
                    value.OccDate = undefined;
                    value.Amount = undefined;
                    value.IsControlled = undefined;
                    value.Balance = undefined;
                    //受控才返回
                    return { EntityID, EntityType, SubjectID, OccDate, Amount }
                }
            }
        }
    }

    /**
     * 从变量的值解析vacation对象
     * @param value
     */
    public static parseVacationObj(value) {
        if (value && typeof value === "object") {
            const { ApplyUser, Type, Start, End, Hours, Balance, NoControl, IsControlled, AutomaticCalculationHours, HoursManual } = value;
            if (ApplyUser && Type && Start && End && Hours && Hours > 0 && (NoControl === true || (Balance !== null && Balance !== undefined))) {
                if (IsControlled) {
                    //冻结预算后，清空,已避免重复提交
                    value.IsControlled = undefined;
                    //受控才返回
                    return {
                        LeaveUserID: ApplyUser,
                        HolidayType: Type,
                        StartTime: Start,
                        EndTime: End,
                        IsCustomHours: AutomaticCalculationHours === false ? true : false,
                        Hours: AutomaticCalculationHours === false ? HoursManual : Hours,
                    };
                }
            }
        }
    }

    /**
     * 从变量的值解析vacationConfirmation对象
     * @param value
     */
    public static parseVacationConfirmationObj(value) {
        if (value && typeof value === "object") {
            const { ProcessInfo, ActualStart, ActualEnd, ActualHours, AutomaticCalculationHours, Submitted, Checked } = value;
            if (Submitted === false && ProcessInfo && ProcessInfo.ProcInstID && (Checked || (ActualStart && ActualEnd && ActualHours >= 0))) {
                value.Submitted = true;
                let isCustomHours = false;
                if (Checked) {
                    isCustomHours = false;
                } else if (AutomaticCalculationHours === false) {
                    isCustomHours = true;
                }
                return {
                    LeaveProcInstID: ProcessInfo.ProcInstID,
                    StartTime: Checked ? ProcessInfo.StartTime : ActualStart,
                    EndTime: Checked ? undefined : ActualEnd,
                    IsCustomHours: isCustomHours,
                    Hours: Checked ? 0 : ActualHours,
                    Remark: null
                };
            }
        }
    }

    /**
     * 生成流程扩展信息，在表单提交和审批时处理，包括预算提交控制、假期提交控制等
     * @param variables
     */
    public static buildApplicationExt2(variables): any[] {
        var budgetData = [];
        var vacationData = [];
        var vacationConfirmationData = [];
        var ext2 = [];

        function processVar(variable) {
            if (variable && typeof variable === "object") {
                AkUtil.each(variable, (value, field) => {
                    if (value) {
                        if (AkUtil.isArray(value) && value.length > 0 && typeof value[0] === "object") {
                            //如果是对象数组
                            AkUtil.each(value, v => {
                                //过滤已删除的行
                                if (!v || v["_del"] === true) {
                                    return;
                                }
                                //递归调用本方法
                                processVar(v);
                            });
                        } else {
                            const budget = WorkflowFormHelper.parseBudgetObj(value);
                            if (budget) {
                                budgetData.push(budget);
                            }

                            const vacation = WorkflowFormHelper.parseVacationObj(value);
                            if (vacation) {
                                if (vacation.StartTime && vacation.StartTime.length <= 10) {
                                    vacation.StartTime = vacation.StartTime + " 00:00:00";
                                }
                                if (vacation.EndTime && vacation.EndTime.length <= 10) {
                                    vacation.EndTime = vacation.EndTime + " 23:59:59";
                                }
                                vacationData.push(vacation);
                            }

                            const vacationConfirmation = WorkflowFormHelper.parseVacationConfirmationObj(value);
                            if (vacationConfirmation) {
                                if (vacationConfirmation.StartTime && vacationConfirmation.StartTime.length <= 10) {
                                    vacationConfirmation.StartTime = vacationConfirmation.StartTime + " 00:00:00";
                                }
                                if (vacationConfirmation.EndTime && vacationConfirmation.EndTime.length <= 10) {
                                    vacationConfirmation.EndTime = vacationConfirmation.EndTime + " 23:59:59";
                                }
                                vacationConfirmationData.push(vacationConfirmation);
                            }
                        }
                    }
                });
            }
        }

        if (variables) {
            processVar(variables);
            if (budgetData.length > 0) {
                ext2.push({
                    "Type": "budget",
                    "Operate": "Freeze",
                    "Data": JSON.stringify(budgetData)
                });
            };
            if (vacationData.length > 0) {
                ext2.push({
                    "Type": "leave",
                    "Operate": "Freeze",
                    "Data": JSON.stringify(vacationData && vacationData.length > 0 ? vacationData[0] : null)//一次只能申请一个休假
                });
            };
            if (vacationConfirmationData.length > 0) {
                ext2.push({
                    "Type": "leave-confirmation",
                    "Operate": "Unleave",
                    "Data": JSON.stringify(vacationConfirmationData && vacationConfirmationData.length > 0 ? vacationConfirmationData[0] : null)//一次只能申请一个销假
                });
            };
        }
        return ext2;
    }

    /**
     *
     *
     * @static
     * @param {*} data
     * @param {RequisitionContent} self
     * @param {(self?: RequisitionContent) => void} [success]
     * @param {(self?: RequisitionContent) => void} [error]
     *
     */
    public static handleResponse(data: any, self, user: "common" | "flowcraft", success?: (self?: RequisitionContent) => void, error?: (self?: RequisitionContent) => void, action?: "submit" | "save") {
        const { formatMessage } = AkGlobal.intl;
        if (data.Status === 0) {
            AkMessage.success(formatMessage({ id: NoticeLocale.SuccessTip }))
            if (success) success(self);
            action === "save" && self.setLoading ? self.setLoading(false) : null;
        } else {
            let msg = data.Message;
            if (data.Status === 110000) {
                let name = "";
                let json = JSON.parse(data.Message);
                for (var item in json) {//考虑多个唯一值情况
                    let obj = self.flowContext.formDef.children.find(f => f.binding === item);
                    name += obj ? (obj.label + "、") : "";
                }
                name = name.substring(0, name.length - 1);
                msg = formatMessage({ id: ApplyContentLocale.TipUniqueValue }, { name: name });
            } else if (data.Status === 120025) {
                msg = formatMessage({ id: ApplyContentLocale.BudgetExceed });
            } else if (data.Status === 110009) {
                msg = formatMessage({ id: ApplyContentLocale.DontRecall })
            } else if (data.Status === 110010) {
                msg = formatMessage({ id: ApplyContentLocale.DontTerminate })
            }
            AkNotification.warning({
                message: formatMessage({ id: CommonLocale.Tip }),
                description: msg
            });
            if (error) error(self);
            self.setLoading ? self.setLoading(false) : null;
        }
    }

    /**
     * 审批页保存
     */
    public static onSaveVariable(self, params, type) {
        TaskAPI.putTaskHandle(params).then(d => {
            //Bug 49597: 待办任务表单页面，点击保存页面灰掉，一直转圈
            this.handleResponse(d, self, type, (success) => {
                self.setLoading ? self.setLoading(false) : null;
            }, (error) => {
                self.setLoading ? self.setLoading(false) : null;
            });
        });
    }
    /**取消领用 */
    public static onCancelClaim(self, params, type) {
        let error = null;
        if (type === "flowcraft") {
            error = (self) => { self.setState({ loading: false }) }
        }
        TaskAPI.putCancelClaimTask(params).then(d => this.handleResponse(d, self, type, (self) => {
            setTimeout(() => this.goBack(), 2000);
        }, error));
    }
    /**任务召回 */
    public static onRecall(self, params, type, callBack?: any) {
        let error = null;
        if (type === "flowcraft") {
            error = (self) => { self.setState({ loading: false }) }
        }
        TaskAPI.putRecallTask(params).then(d => this.handleResponse(d, self, type, (self) => {
            if (callBack) {
                callBack(true);//成功
            } else {
                setTimeout(() => this.goBack(), 2000);
            }
        }, error => {
            if (callBack) {
                callBack(false);
            }
        }));
    }

    /**
     * 从formcontext获取TaskDetail对象
     * @param context 
     */
    static getTastDetail(context) {
        if (context) {
            return context.taskDetail;
        }
        return null;
    }

    /**
     * 从context获取表单的申请状态
     * @param context 
     */
    static getAppStatus(context) {
        const taskDetail = this.getTastDetail(context);
        if (taskDetail) {
            return taskDetail.ApplicationInfo && taskDetail.ApplicationInfo.Status || 0;
        } else {
            return 0;
        }
    }

    /**
     * 从context获取表单的任务对象
     * @param context 
     */
    static getTaskInfo(context) {
        const taskDetail = this.getTastDetail(context);
        if (taskDetail && taskDetail.TaskInfo) {
            return taskDetail.TaskInfo;
        } else {
            return null;
        }
    }

    /**
     * 是否是新建申请
     */
    static isNewApplication(context) {
        const taskInfo = this.getTaskInfo(context);
        const taskDetail = this.getTastDetail(context);
        return !taskInfo && !taskDetail.ApplicationInfo;
    }

    /**
     * 我提交的或我申请的
     */
    static isMyApplication(context) {
        const taskDetail = this.getTastDetail(context);
        let user = AkContext.getUser().AccountID;
        //增加new app的情况
        return (this.isNewApplication(context)) || (user === taskDetail.ApplicationInfo.CreatedBy) || (user === taskDetail.ApplicationInfo.ApplicantID);
    }

    /**
     * 当前表单是否允许提交
     * @param context 
     */
    static canFormSubmit(context) {
        let appStatus = this.getAppStatus(context);
        let taskInfo = this.getTaskInfo(context);
        return SubmitStatus.indexOf(appStatus) > -1 && !taskInfo && this.isMyApplication(context);
    }

    /**
     * 当前表单是否允许保存
     * @param context 
     */
    static canFormSave(context) {
        let appStatus = this.getAppStatus(context);
        let taskInfo = this.getTaskInfo(context);
        return SaveStatus.indexOf(appStatus) > -1 && !taskInfo && this.isMyApplication(context);
    }

    /**
     * 判断任务是否可以提交，判断经办人是否当前用户
     * @param context 
     */
    static canTaskSubmit(context) {
        let taskInfo = this.getTaskInfo(context);
        return this.canFormUpdate(context) && taskInfo && AkContext.getUser().AccountID === taskInfo.AssigneeID;
    }

    static canFormUpdate(context) {
        //表单是否允许修改（流程未结束，任务未完成）
        let taskInfo = this.getTaskInfo(context);
        return this.canFormSave(context) || this.canFormSubmit(context) || (taskInfo && taskInfo.Status === TaskStatusEnum.Pending);
    }

    /**
     * 没有任务信息和完成类型的任务都返回false
     * @param context 
     */
    static isApprovalTask(context) {
        let taskInfo = this.getTaskInfo(context);
        if (taskInfo) {
            const { Flags } = taskInfo;
            if ((Flags & TaskFlagsEnum.Complete) === TaskFlagsEnum.Complete) {
                return false;
            } else {
                return true;
            }
        }
        return null;
    }
}

export const BooleanLocalePrefix: string = "page.workflow.common.";

/**
 * 审批通用Props
 *
 * @interface WorkflowApprovalProps
 * @extends {ReactRouter.RouteComponentProps<any, any>}
 */
export interface WorkflowApprovalProps extends RouterProps {
}

/**
 * 表单通用Props
 *
 * @interface WorkflowFormProps
 * @extends {ReactRouter.RouteComponentProps<any, any>}
 * @extends {AkFormComponentProps}
 */
export interface WorkflowFormProps extends RouterProps, AkFormComponentProps {
}

/**
 * 公共状态
 *
 * @interface WorkflowCommonStates
 * @template T
 */
export interface WorkflowCommonStates<T> {
    variables?: T;
    disableProps?: boolean
}