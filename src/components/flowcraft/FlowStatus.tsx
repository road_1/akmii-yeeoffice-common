import * as React from "react";
import { Component } from "react";
import { ApplicationStatusColor, ApplicationStatusEnum, ProcInstAPI } from "../../api/flowcraft/application";
import { ApplyContentLocale } from "../../locales/localeid";
import { AkGlobal } from "../../util/common";
import { AkIcon, AkPopover, AkSpin } from "../controls";
import AkTooltip from "../controls/ak-tooltip";
import { AkIdentityPicker } from "../identity";

export interface FlowStatusProps {
    app: any
}

export interface FlowStatusStates {
    visible?: boolean;
    loading?: boolean;
    assignees?: string[];
}

export class FlowStatus extends Component<FlowStatusProps, FlowStatusStates> {
    constructor(props, context) {
        super(props, context);
        this.state = { loading: true }
    }
    loaded = false;

    loadAssignees(props: FlowStatusProps) {
        this.loaded = true;
        if (props.app.Status === ApplicationStatusEnum.Running && props.app.FlowNo) {
            ProcInstAPI.activeAssignees([props.app.FlowNo]).then(d => {
                if (d.Status === 0) {
                    if (d.Data && d.Data[props.app.FlowNo]) {
                        this.setState({ assignees: d.Data[props.app.FlowNo] });
                    }
                }
                this.setState({ loading: false });
            });
        }
    }

    visibleChange(visible) {
        if (visible && !this.loaded) {
            this.loadAssignees(this.props);
        }
    }

    renderPendingAssigness() {
        const { loading, assignees } = this.state;
        return <AkSpin spinning={loading}>
            {!loading && <AkIdentityPicker readonly value={assignees} />}
        </AkSpin>
    }

    render() {
        const { app } = this.props;
        const { visible } = this.state;
        let color, showTooltip, showInfo;
        const fm = AkGlobal.intl.formatMessage;
        let tag = fm({
            id: "model.application.status." + app.Status
        });
        switch (app.Status) {
            case ApplicationStatusEnum.Running:
                color = ApplicationStatusColor.Running
                showInfo = true;
                break;
            case ApplicationStatusEnum.Complete:
                color = ApplicationStatusColor.Complete;
                break;
            case ApplicationStatusEnum.Rejected:
                color = ApplicationStatusColor.Rejected
                showTooltip = true;
                break;
            case ApplicationStatusEnum.Revoked:
            case ApplicationStatusEnum.Revoking:
                color = ApplicationStatusColor.Revoked
                showTooltip = true;
                break;
            case ApplicationStatusEnum.Canceled:
            case ApplicationStatusEnum.Cancelling:
                color = ApplicationStatusColor.Cancelled
                break;
            case ApplicationStatusEnum.Error:
                color = ApplicationStatusColor.Error
        }

        return <span className="ak-flowstatus" style={{ color }}>
            {showTooltip ? <AkTooltip
                placement="right"
                title={fm({ id: ApplyContentLocale.StatusTip })}>
                {tag}
            </AkTooltip> : <span>{tag}</span>}
            {showInfo && <AkPopover title={fm({ id: ApplyContentLocale.PendingAssignees })} onVisibleChange={v => this.visibleChange(v)} content={this.renderPendingAssigness()}><AkIcon className="info" type="info-circle-o" /></AkPopover>}
        </span>;
    }
}