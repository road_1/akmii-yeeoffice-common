import * as React from "react";
import { Component } from "react";
import { injectIntl, FormattedMessage } from "react-intl";
import { withRouter } from "react-router";
import {
    ApplicationStatusEnum,
    ProcInstAPI,
    PostStartProcInstByKeyRequest,
    GetApplicantDetailRequest
} from "../../api/flowcraft/application";
import { WorkflowFormHelper } from "./workflow-form-helper";
import YeeUserContent from "./yee-user-content";
import { AkAttachment } from "./attachment";
import { StatusBar } from "./status-bar";
import { IntlProps, RouterProps, AkGlobal, AkContext } from '../../util/common';
import { TaskDetailInfo } from '../../api/flowcraft/task';
import { ProcDraftsAPI, PostProcDraftRequest } from "../../api/flowcraft/procdraft";
import { AkNotification } from "../controls/ak-notification";
import { AkButton } from "../controls/ak-button";
import { AkSpin } from "../controls/ak-spin";
import { ApplyContentLocale, CommonLocale } from '../../locales/localeid';
import { AkRow } from '../controls/ak-row';
import { AkCol } from '../controls/ak-col';
import { AkMessage } from '../controls/ak-message';
import AkFormHistoryLog from '../controls/historylog/ak-form-historylog';
import { AkIcon } from '../controls/ak-icon';

export const ReadonlyStatus: ApplicationStatusEnum[] = [
    ApplicationStatusEnum.Running,
    ApplicationStatusEnum.Complete,
    ApplicationStatusEnum.Error,
    ApplicationStatusEnum.Canceled
];

export const SaveStatus: ApplicationStatusEnum[] = [
    ApplicationStatusEnum.Draft,
    ApplicationStatusEnum.Start
];

export const SubmitStatus: ApplicationStatusEnum[] = [
    ApplicationStatusEnum.Draft,
    ApplicationStatusEnum.Start,
    ApplicationStatusEnum.Rejected,
    ApplicationStatusEnum.Revoked
];

export const RevokeStatus: ApplicationStatusEnum[] = [
    ApplicationStatusEnum.Running,
];

export const CancelStatus: ApplicationStatusEnum[] = [
    ApplicationStatusEnum.Running
];

export interface RequisitionContentProps extends IntlProps, RouterProps {
    /**
     * 加载完成回调操作
     *
     *
     * @memberOf RequisitionContentProps
     */
    onLoaded?: (variables: any, readonly?: boolean, taskDetail?: TaskDetailInfo) => void;
    /**
     * 保存前方法
     *
     *
     * @memberOf RequisitionContentProps
     */
    beforeSave?: (taskDetail?: TaskDetailInfo) => any;
    /**
     * 提交前
     *
     *
     * @memberOf RequisitionContentProps
     */
    beforeSubmit?: (taskDetail?: TaskDetailInfo) => any;
    /**
     * 提交后
     * 
     * 
     * @memberOf RequisitionContentProps
     */
    afterSubmit?: (taskDetail?: TaskDetailInfo, applicationId?: string) => any;
    /**
     * 保存后
     */
    afterSave?: (taskDetail?: TaskDetailInfo, draftId?: string) => any;
    /**
     * 取消前
     *
     *
     * @memberOf RequisitionContentProps
     */
    beforeCancel?: (taskDetail?: TaskDetailInfo) => any;
    /**
     * 撤回前
     * 
     * 
     * @memberOf RequisitionContentProps
     */
    beforeRevoke?: (taskDetail?: TaskDetailInfo) => any;
    /**
     * 关闭前
     * 
     * 
     * @memberOf RequisitionContentProps
     */
    beforeClose?: (taskDetail?: TaskDetailInfo) => any;
    /**
     * 页面Title
     *
     *
     * @memberOf RequisitionContentProps
     */
    pageTitle?: string;

    /**禁止带申请 */
    disabledDelegate?: boolean;
    /**不显示附件上传 */
    disabledAttachment?: boolean;
    /**不显示用户信息 */
    disabledApplicationInfo?: boolean;

    renderCustomHeader?: () => JSX.Element | JSX.Element[];

    renderCustomFooter?: () => JSX.Element | JSX.Element[];
}

export interface RequisitionContentStates {
    /**
     * loading flag
     * 
     * @type {boolean}
     * @memberOf RequisitionContentStates
     */
    loading?: boolean;
    /**
     * readonly flag
     * 
     * @type {boolean}
     * @memberOf RequisitionContentStates
     */
    readonly?: boolean;
    /**
     * Task Detail Info
     * 
     * @type {TaskDetailInfo}
     * @memberOf RequisitionContentStates
     */
    taskDetail?: TaskDetailInfo;

    toggle?: boolean;
}

/**
 * 提交任务申请
 *
 * @export
 * @class RequisitionContent
 * @extends {Component<RequisitionContentProps, RequisitionContentStates>}
 */
@withRouter
export default class RequisitionContent extends Component<RequisitionContentProps, RequisitionContentStates> {
    taskID: string;
    draftID: string;
    appID: string;
    defID: string;
    defKey: string;
    loading: boolean;
    formatMessage: any;
    constructor(props, context) {
        super(props, context);
        this.taskID = this.props.location.query["taskid"] as string;
        this.defID = this.props.location.query['defid'] as string;
        this.defKey = this.props.location.query['defkey'] as string;
        this.draftID = this.props.location.query["draftid"] as string;
        this.appID = this.props.location.query["appid"] as string;
        this.loading = true;
        this.formatMessage = AkGlobal.intl.formatMessage;
        this.state = { taskDetail: {}, loading: true, toggle: true };
    }

    setLoading(flag: boolean) {
        this.loading = flag;
        this.setState({ loading: flag });
    }

    componentDidMount() {
        if (this.draftID) {
            // 草稿信息，查询草稿内容
            ProcDraftsAPI.getProcDraftByID({ procDraftID: this.draftID }).then(data => {
                // set readonly to false for draft, JSON parse FormDATA
                const formDATA = data.Data.FormDATA ? JSON.parse(data.Data.FormDATA) : {};
                const readonly = false;
                const taskDetail = Object.assign({}, this.state.taskDetail, data.Data, { Variables: formDATA, AppStatus: ApplicationStatusEnum.Draft });
                this.props.onLoaded(formDATA, readonly, taskDetail);
                this.setState({ taskDetail, readonly });
                this.getApplicationUser(data.Data.ApplicantID);
            });
        } else if (this.appID) {
            // 我的申请点开后
            ProcInstAPI.getApplicationByID({ applicationID: this.appID }).then(data => {
                data.Data.Variables = WorkflowFormHelper.deserializeVariables(data.Data.Variables);
                const readonly = ReadonlyStatus.indexOf(data.Data.AppStatus) !== -1;
                this.props.onLoaded(data.Data.Variables, readonly, data.Data);
                this.setState({ taskDetail: data.Data, readonly: readonly });
                this.setLoading(false);
                //this.getApplicationUser(data.Data.ApplicantID);
            });
        } else {
            // 新建流程
            this.state.taskDetail.AppStatus = ApplicationStatusEnum.Start;
            this.setState({ taskDetail: this.state.taskDetail })
            this.getApplicationUser();
        }
    }

    /**
     * 获取申请人详细信息，userID为空是获取当前用的信息
     * @param userID 
     */
    getApplicationUser(userID?: string) {
        ProcInstAPI.getApplicantByUserID({ userID: userID }).then(data => {
            if (data.Data) {
                this.setState({ taskDetail: Object.assign({}, this.state.taskDetail, data.Data) });
                this.setLoading(false);
            }
        });
    }

    onChangeApplyUser(userId) {
        if (userId) {
            this.getApplicationUser(userId);
        } else {
            const { taskDetail } = this.state;
            taskDetail.ApplicantID = null;
            this.setState({ taskDetail: taskDetail });
        }
    }
    onSave() {
        if (this.loading) return;
        const { state: { taskDetail }, props: { beforeSave }, defID, defKey, draftID } = this;
        // break action when before submit return null Add by Eden
        let formVariables = beforeSave ? beforeSave(taskDetail) : null;
        if (!formVariables) return;

        //保存时真删除附件
        let variables = WorkflowFormHelper.deleteFiles(taskDetail.Variables);
        variables = Object.assign({}, variables, formVariables);

        const saveRequest: PostProcDraftRequest = {
            ProcDefID: defID,
            DefKey: defKey,
            FormData: JSON.stringify(variables),
            ProcDraftID: draftID,
            ApplicantID: taskDetail.ApplicantID
        };
        WorkflowFormHelper.onSave(this, saveRequest, "common");
    }

    onSubmit() {
        if (this.loading) return;
        const { formatMessage } = AkGlobal.intl;
        const { props: { beforeSubmit }, state: { taskDetail, taskDetail: { ApplicantID } }, defKey, draftID, appID } = this;
        let Variables = this.state.taskDetail.Variables;
        // break action when before submit return null Add by Eden
        let formVariables = beforeSubmit ? beforeSubmit(taskDetail) : null;
        if (formVariables === false) {
            return;
        }

        if (!formVariables) {
            AkNotification.warning({
                message: formatMessage({ id: CommonLocale.Tip }),
                description: formatMessage({ id: CommonLocale.FormValidaMessage }, { name: "" })
            });
            return;
        };
        //提交时真删除附件
        Variables = WorkflowFormHelper.deleteFiles(Variables);

        // //处理删除文件逻辑
        // formVariables = WorkflowFormHelper.deleteFiles(formVariables);

        const variables = Object.assign({}, Variables, formVariables);
        const startRequest: PostStartProcInstByKeyRequest = {
            Key: defKey,
            Variables: WorkflowFormHelper.serializeVariables(variables),
            ApplicantID: ApplicantID,
            ProcDraftID: draftID,
            ApplicationID: appID
        };
        if (!startRequest.ApplicantID) {
            AkNotification.error({
                message: formatMessage({ id: ApplyContentLocale.Submit }),
                description: formatMessage({ id: ApplyContentLocale.SelectApplicant })
            });
            return;
        }
        WorkflowFormHelper.onSubmit(this, startRequest, "common");
    }

    onCancel() {
        const { props: { beforeCancel }, state: { taskDetail }, loading, appID } = this;
        if (loading || (beforeCancel && !beforeCancel(taskDetail)) || CancelStatus.indexOf(taskDetail.AppStatus) === -1) return;

        const cancelRequest: GetApplicantDetailRequest = { applicationID: appID };

        WorkflowFormHelper.onCancel(this, cancelRequest, "common");
    }
    onRevoke() {
        const { props: { beforeRevoke }, state: { taskDetail }, loading, appID } = this;
        if (loading || (beforeRevoke && !beforeRevoke(taskDetail)) || RevokeStatus.indexOf(taskDetail.AppStatus) === -1) return;

        const revokeRequest: GetApplicantDetailRequest = { applicationID: appID };

        WorkflowFormHelper.onRevoke(this, revokeRequest, "common");
    }
    onClose() {
        if (this.props.beforeClose && !this.props.beforeClose(this.state.taskDetail)) return;
        WorkflowFormHelper.goBack();
    }
    render() {
        const { state: { loading, readonly, taskDetail, toggle }, props: { pageTitle, disabledDelegate, disabledAttachment, disabledApplicationInfo } } = this;
        const { ButtonClose, ButtonTerminate, ButtonSave, ButtonSubmit, ButtonRevoke } = ApplyContentLocale;
        const x = [CancelStatus, RevokeStatus, SaveStatus, SubmitStatus]
        let num = 5;
        for (var i = 0; i < x.length; i++) {
            if (x[i].indexOf(taskDetail.AppStatus) === -1) {
                num = num - 1;
            }
        }
        let company = AkContext.getCompanyInfo();
        let fontColor = company ? { color: company.NavFontColour } : null
        return <div className="ak-approval-content">
            <div className="ak-top-action-bar">
                <AkRow className="ak-zper-inbox-title" style={company ? { background: company.NavBarColour, color: company.NavFontColour } : null}>
                    <AkCol className="ak-content-title-box">
                        <span className="inbox-text" style={fontColor}>{pageTitle}</span></AkCol>
                    <AkCol className="ak-content-button-box" style={{ width: 81 * num + "px" }}>
                        {CancelStatus.indexOf(taskDetail.AppStatus) === -1 ? null
                            : <AkButton icon="minus-circle-o" style={fontColor} onClick={() => this.onCancel()}>
                                <FormattedMessage id={ButtonTerminate}></FormattedMessage>
                            </AkButton>}
                        {RevokeStatus.indexOf(taskDetail.AppStatus) === -1 ? null
                            : <AkButton icon="retweet" style={fontColor} onClick={() => this.onRevoke()}>
                                <FormattedMessage id={ButtonRevoke}></FormattedMessage>
                            </AkButton>}
                        {SaveStatus.indexOf(taskDetail.AppStatus) === -1 || readonly ? null
                            : <AkButton icon="save" style={fontColor} onClick={() => this.onSave()}>
                                <FormattedMessage id={ButtonSave}></FormattedMessage>
                            </AkButton>}
                        {SubmitStatus.indexOf(taskDetail.AppStatus) === -1 || readonly ? null
                            : <AkButton icon="fly" style={fontColor} onClick={() => this.onSubmit()}>
                                <FormattedMessage id={ButtonSubmit}></FormattedMessage>
                            </AkButton>}
                        <AkButton icon="close" style={fontColor} onClick={() => this.onClose()}>
                            <FormattedMessage id={ButtonClose}></FormattedMessage>
                        </AkButton>
                    </AkCol>
                </AkRow>
            </div>
            <AkSpin spinning={loading}>
                <div className="ak-workflow-container">
                    {this.props.renderCustomHeader && this.props.renderCustomHeader()}
                    <StatusBar taskDetail={taskDetail} />
                    {disabledApplicationInfo ? null : <YeeUserContent onChangeApplyUser={userId => this.onChangeApplyUser(userId)} readonly={readonly} taskDetail={taskDetail} allowDelegate={!disabledDelegate} />}
                    {this.props.children}
                    {disabledAttachment ? null : <AkAttachment readonly={this.state.readonly} taskDetail={this.state.taskDetail} />}
                    <div className="ak-form-title mb15 toggle-title" onClick={() => { this.setState({ toggle: !toggle }) }}>
                        <span className="title-bluespan"></span>
                        <FormattedMessage id={ApplyContentLocale.LogTitle} />
                        <AkIcon type={this.state.toggle ? "up" : "down"}></AkIcon>
                    </div>
                    {toggle ? <AkFormHistoryLog defID={this.defID} taskID={this.taskID} appID={this.appID} type="common" isPreview={false} /> : null}
                    {this.props.renderCustomFooter && this.props.renderCustomFooter()}
                </div>
            </AkSpin>
        </div>
    }
}



