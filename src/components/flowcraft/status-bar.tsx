import * as React from "react";
import { Component } from "react";
import { FormattedMessage } from "react-intl";
import { IntlProps } from "../../util/common";
import { TaskDetailInfo } from "../../api/flowcraft/task";
import { ApplicationStatusLocale, ApplicationStatusEnum } from "../../api/flowcraft/application";
import { TaskLocale } from "../../locales/localeid";

export interface StatusBarProps extends IntlProps {
    taskDetail?: TaskDetailInfo
}

export interface StatusBarStates { }

export class StatusBar extends Component<StatusBarProps, StatusBarStates> {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        if (!this.props.taskDetail) return null;
        const { FlowNo, AppStatus } = this.props.taskDetail;
        if (!AppStatus || AppStatus === ApplicationStatusEnum.Start) return null;
        const status = <FormattedMessage id={ApplicationStatusLocale + AppStatus}></FormattedMessage>;

        return <div className="ak-h2box ak-border-b2">
            <h2 className="ak-application-idstatus">
                <FormattedMessage id={TaskLocale.Code} values={{ code: FlowNo }}></FormattedMessage>
                <span className="pull-right ak-state">
                    {status}
                </span>
            </h2>
        </div>
    }
}