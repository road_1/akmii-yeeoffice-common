import * as React from "react";
import * as classNames from "classnames";
import { IdentityAPI, AkIdentity, AkIdentityType } from "../../api/";
import { AkModal, AkInput } from "../controls/";
import { AkCostCenterPickerAdv } from "./AkCostCenterPickerAdv";
import { IdentityLocale } from "../../locales";
import { AkTagInput, AkTagValueDescriber } from "../controls/ak-tag-input";
import { AkGlobal } from "../../util/common";

export interface AkCostCenterPickerAbstractProp {
    className?: string;
    identityTypes?: AkIdentityType[];
    searchRowCount?: number;
    multiple?: boolean; //is allow multiple selection
    maxSelection?: number; //how many identity selection allowed
    maxDisplay?: number; //how many identity will be displayed on control
    displayAll?: boolean; //是否显示全部选项
    nameDisplayLength?: number; //超出长度的名称会被截取
    style?: React.CSSProperties; //additional style apply to control
    placeholder?: string; //输入默认的提示语
    defaultValue?: string | string[] | AkIdentity | AkIdentity[]; //default selection
    readonly?: boolean;
    autoCollapse?: boolean; //展开隐藏内容后，鼠标离开是否自动收起
    value?: string | string[] | AkIdentity | AkIdentity[]; //和onChange一起结合使用，如果value赋值则defaultValue无效
    hideInputOnMaxSelectReached?: boolean;
    orgID?: string;
    readOnlyShowBorder?: boolean;//readonly时是否显示边框
}

export interface AkCostCenterPickerProp extends AkCostCenterPickerAbstractProp {
    onChange?: (obj: AkIdentity | AkIdentity[]) => any; //onchange事件，将选中dict输出
}

export interface AkCostCenterPickerState {
    value?: Object;
    maxSelection?: number; //最大选择项
    initialized?: boolean; //是否初始化完成
    displayAll?: boolean;
    searchResult?: AkIdentity[];
    advanceVisible?: boolean; //高级选择器显示
    advanceTempValue?: Object; //advance picker的临时值
    inputValue?: string;
    hideInput?: boolean;
}

let resolvedIdentities: {
    [key: string]: AkIdentity;
} = {};

const describer: AkTagValueDescriber = {
    id: "ID",
    label: "Name"
}

export class AkCostCenterPicker extends React.Component<AkCostCenterPickerProp, AkCostCenterPickerState> {
    static defaultProps: AkCostCenterPickerProp = {
        identityTypes: [AkIdentityType.CostCenter],
        searchRowCount: 10,
        multiple: false,
        maxSelection: 200,
        maxDisplay: 5,
        defaultValue: [],
        nameDisplayLength: 20,
        readonly: false,
        displayAll: false,
        autoCollapse: false,
        orgID: "0",
        hideInputOnMaxSelectReached: true
    }

    constructor(props, context) {
        super(props, context);

        this.state = {
            maxSelection: props.multiple ? props.maxSelection : 1,
            value: {},
            initialized: false,
            searchResult: [],
            advanceVisible: false,
            advanceTempValue: {},
        };
    }

    componentDidMount() {
        let value = ("value" in this.props) ? this.props.value : this.props.defaultValue;
        this.parseUnresolvedValue(value).then(d => {
            this.setState({ value: IdentityAPI.identityArray2Dict(d), initialized: true })
        });
    }

    ifValueAreEqual(v1, v2) {
        return v1 === v2 || JSON.stringify(v1) === JSON.stringify(v2);
    }

    componentWillReceiveProps(nextProps: AkCostCenterPickerProp) {
        if ("value" in nextProps && nextProps.value !== this.props.value) {
            this.parseUnresolvedValue(nextProps.value).then(d => {
                this.setState({ value: IdentityAPI.identityArray2Dict(d), initialized: true })
            });
        }

        if ("multiple" in nextProps && nextProps.multiple !== this.props.multiple) {
            this.setState({ maxSelection: nextProps.multiple ? nextProps.maxSelection || this.props.maxSelection : 1 })
        }

        if ("maxSelection" in nextProps && nextProps.maxSelection !== this.props.maxSelection) {
            const { value } = this.state;
            if (value && nextProps.maxSelection !== Object.getOwnPropertyNames(value).length) {
                this.setState({ value: [], maxSelection: nextProps.multiple ? nextProps.maxSelection : 1 });
            } else {
                this.setState({ maxSelection: nextProps.multiple ? nextProps.maxSelection : 1 });
            }
        }

    }

    // /**
    //  * 根据传入参数适配专成Dict
    //  * @param data
    //  */
    // getDictFromValue(data: AkIdentity[]|AkIdentity|string) {
    //     if (data) {
    //         if (data instanceof String) {
    //
    //         } else if (data instanceof Array) {
    //             return IdentityAPI.identityArray2Dict(data);
    //         } else {
    //             return data;
    //         }
    //     } else {
    //         return {};
    //     }
    // }

    changeValue(value: Object) {
        if (!("value" in this.props)) {
            //如果外部传入value，则value由外部控制
            this.setState({ value: value });
        }
        if (this.props.onChange) {
            if (this.props.multiple) {
                this.props.onChange(Object.keys(value).map(k => value[k]));
            } else {
                let values = Object.keys(value).map(k => value[k]);
                this.props.onChange(values.length > 0 ? values[0] : undefined);
            }
        }
    }

    async parseUnresolvedValue(val) {

        let result = [];
        let valArr = [];
        if (val) {
            if (val instanceof Array) {
                valArr = val;
            } else {
                valArr.push(val);
            }

            //获取所有需要解析的id数组，一次获取解析
            let identityIDs = [];
            valArr.forEach(p => {
                if (p && p instanceof Object && ("ID" in p || p instanceof AkIdentity)) {
                    result.push(p);
                } else if (p) {
                    if (p in resolvedIdentities) {
                        result.push(resolvedIdentities[p]);
                    } else {
                        identityIDs.push(p);
                    }
                }
            });

            if (identityIDs.length > 0) {
                let ids = await this.getIdentityFromSimpleValue(identityIDs);
                ids.forEach(id => {
                    resolvedIdentities[id.ID] = id;
                    result.push(id);
                });
            }
        }

        return result;
    }


    // //根据设置的defaultvalue解析出AkIdentity对象数组
    // async parseDefaultValue(val) {
    //     let result = [];
    //     if (val !== undefined) {
    //         if (val instanceof Array) {
    //             //获取所有需要解析的id数组，一次获取解析
    //             let identityIDs = [];
    //             val.forEach(p => {
    //                 if (p instanceof AkIdentity) {
    //                     result.push(p);
    //                 } else {
    //                     identityIDs.push(p);
    //                 }
    //             });
    //
    //             if (identityIDs.length > 0) {
    //                 result = result.concat(await this.getIdentityFromSimpleValue(identityIDs));
    //             }
    //         } else {
    //             await this.parseSingleValue(result, val);
    //         }
    //     }
    //     return result;
    // }
    //
    // /**
    //  * 解析单个value
    //  * @param arr
    //  * @param val
    //  * @returns {Promise<void>}
    //  */
    // async parseSingleValue(arr, val) {
    //     if (val instanceof String) {
    //         let str = val as string;
    //         if (str in resolvedIdentities) {
    //             arr.push(resolvedIdentities[str]);
    //         } else {
    //             let ids = await this.getIdentityFromSimpleValue([val]);
    //             if (ids.length>0) {
    //                 let id = ids[0];
    //                 resolvedIdentities[id.ID] = id;
    //                 arr.push(id);
    //             }
    //         }
    //     } else if (val instanceof AkIdentity) {
    //         arr.push(val);
    //     } else {
    //         //not support value type
    //     }
    // }

    async getIdentityFromSimpleValue(vals) {
        let checkArray = [];
        if (vals instanceof String) {
            checkArray.push(vals);
        } else {
            checkArray = vals;
        }
        //默认搜索使用identitytypes的第一个选项，不然值应该是AkIdentity对象
        let identities = checkArray.map(ca => {
            return { ID: ca, Type: this.props.identityTypes[0] }
        });
        let rs = await IdentityAPI.resolveIdentities({ identities: identities });
        ///todo: process error?
        return rs.Data;
    }

    // tagClosed(removedTag) {
    //     let value = this.state.value;
    //     delete value[removedTag.ID];
    //     this.changeValue(value);
    // }

    // /**
    //  * 显示选中的Identity名称
    //  * @returns {any[]}
    //  */
    // getTagDisplay() {
    //     const {value} = this.state;
    //
    //     return (this.props.displayAll || this.state.displayAll
    //         ? Object.keys(value)
    //         : Object.keys(value).slice(0, this.props.maxDisplay)).map((v) => {
    //
    //         return this.getSingleTagDisplay(value[v]);
    //     });
    // }
    //
    // getSingleTagDisplay(value: AkIdentity) {
    //     let islong = value.Name.length > this.props.nameDisplayLength;
    //     return <AkTag
    //         key={value.ID}
    //         closable={!this.props.readonly}
    //         afterClose={() => this.tagClosed(value)}>
    //         {islong
    //             ? value
    //                 .Name
    //                 .slice(0, this.props.nameDisplayLength)
    //             : value.Name}
    //     </AkTag>
    // }

    // /**
    //  * 显示更多按钮
    //  * @returns {any}
    //  */
    // getMoreDisplay() {
    //     const {value} = this.state;
    //     let moreCount = Object
    //             .keys(value)
    //             .length - this.props.maxDisplay;
    //
    //     if (!this.props.displayAll && !this.state.displayAll && moreCount > 0) {
    //         return <AkButton
    //             style={{
    //             marginRight: 8
    //         }}
    //             onClick={() => {
    //             this.setState({displayAll: true})
    //         }}
    //             size="small">{moreCount + ' more'}</AkButton>;
    //     }
    //     return null;
    // }

    // /**
    //  * 如果设置了自动折叠，对应的处理逻辑
    //  */
    // handleContainerMouseLeave() {
    //     if (this.props.autoCollapse && !this.props.displayAll) {
    //         this.setState({displayAll: false});
    //     }
    // }

    /**
     * 根据输入值的变化调用后端搜索api
     * @param value 当前输入的值
     */
    handleInputChange(value: string) {
        return new Promise<any[]>((resolve, reject) => {
            IdentityAPI
                .searchCostCenter({
                    pageIndex: 1,
                    keyword: value,
                    orgId: this.props.orgID,
                    pageSize: this.props.searchRowCount
                })
                .then(d => {
                    resolve(d.Data);
                }, (e) => {
                    reject();
                });
        });
    }

    // handleIdentitySelected(value, option) {
    //     this.setState({inputValue: ""});
    //
    //     var fs = this.state.searchResult.find(sr => {
    //         return sr.ID === value;
    //     });
    //
    //     if (fs) {
    //         this.addValue(fs);
    //     }
    // }

    // addValue(value: AkIdentity) {
    //     const {hideInputOnMaxSelectReached} = this.props;
    //     let v = this.state.value;
    //     const {maxSelection} = this.state;
    //     if (!v.hasOwnProperty(value.ID)) {
    //         if (maxSelection > 0 && Object.keys(v).length >= maxSelection) {
    //             if (maxSelection === 1) {
    //                 v = {};
    //                 v[value.ID] = value;
    //                 this.changeValue(v);
    //             }
    //         } else {
    //             v[value.ID] = value;
    //             this.changeValue(v);
    //         }
    //     }
    // }

    advancePickerOK() {
        this.setState({ advanceVisible: false });
        this.changeValue(Object.assign({}, this.state.advanceTempValue));
    }

    advancePickerCancel() {
        this.setState({ advanceVisible: false, advanceTempValue: this.state.value });//直接传对象，因为adv控件不直接使用
    }

    advanceValueChange(value) {
        this.setState({ advanceTempValue: value });
    }

    getAdvancePickerDisplay() {
        const { orgID, value, onChange, maxDisplay, displayAll, style, placeholder, defaultValue, readonly, autoCollapse, hideInputOnMaxSelectReached, readOnlyShowBorder, ...others } = this.props;

        return <AkModal style={{ top: 30 }} visible={this.state.advanceVisible} onOk={this.advancePickerOK.bind(this)}
            onCancel={this.advancePickerCancel.bind(this)}>
            {this.state.advanceVisible?<AkCostCenterPickerAdv orgID={orgID} value={this.state.advanceTempValue}
                onChange={this.advanceValueChange.bind(this)} {...others}></AkCostCenterPickerAdv>:null}
        </AkModal>
    }

    getAkTagInput() {
        const { formatMessage } = AkGlobal.intl;
        const { multiple, maxSelection, maxDisplay, displayAll, nameDisplayLength, placeholder, readonly, autoCollapse, hideInputOnMaxSelectReached } = this.props;
        const { initialized, value } = this.state;
        return <AkTagInput className={this.props.className} multiple={multiple} maxSelection={maxSelection}
            maxDisplay={maxDisplay} displayAll={displayAll} nameDisplayLength={nameDisplayLength}
            placeholder={placeholder ? placeholder : formatMessage({ id: IdentityLocale.AdvCostCenterPlaceholder })}
            onChange={(arr, v) => this.changeValue(v)}
            onInputChange={value => this.handleInputChange(value)}
            readonly={readonly} autoCollapse={autoCollapse}
            hideInputOnMaxSelectReached={hideInputOnMaxSelectReached}
            advanceIcon="organization" advanceIconClick={this.displayAdvancePicker.bind(this)}
            value={IdentityAPI.identityDict2Array(value)}
            showSpin={!initialized} valueDescriber={describer}
            dropdownMatchSelectWidth={!multiple} />
    }

    displayAdvancePicker() {
        let newValue = Object.assign({}, this.state.value);
        this.setState({ advanceVisible: true, advanceTempValue: newValue });
    }

    // getInputDisplay() {
    //     const {formatMessage} = this.props.intl;
    //     const {placeholder, readonly, hideInputOnMaxSelectReached} = this.props;
    //     const {searchResult, inputValue, hideInput, value, maxSelection} = this.state;
    //
    //     if (readonly || (hideInputOnMaxSelectReached && maxSelection > 0 && Object.keys(value).length >= maxSelection)) {
    //         //readonly or hide input on reached max selection
    //         return undefined;
    //     } else {
    //         return <AkAutoComplete
    //             value={inputValue}
    //             iconType="organization"
    //             iconClick={this.displayAdvancePicker.bind(this)}
    //             showSearch={true}
    //             placeholder={placeholder?placeholder:formatMessage({id:IdentityLocale.SimplePlaceholder})}
    //             dataSource={searchResult?searchResult.map(d=>{return {value: d.ID, text: d.Name}}):[]}
    //             onInputChange={value => this.handleInputChange(value)}
    //             onSelect={this.handleIdentitySelected.bind(this)}/>;
    //     }
    // }


    render() {
        const { multiple, readOnlyShowBorder, readonly } = this.props;

        const className = classNames("ak-identity-picker-wrapper", { multiple: multiple });
        return <div className={className}>
            {
                readOnlyShowBorder ?
                    (this.props.value || !readonly ? this.getAkTagInput() : <AkInput disabled={true}></AkInput>)
                    : this.getAkTagInput()
            }
            {this.getAdvancePickerDisplay()
            }</div>;
    }
}