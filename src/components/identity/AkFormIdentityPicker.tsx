import * as React from "react";
import { AkIdentityPicker, AkIdentityPickerAbstractProp, AkIdentityPickerState } from "./AkIdentityPicker";

export interface AkFormIdentityPickerProp extends AkIdentityPickerAbstractProp {
    parentId?: string;
    treeSelect?: boolean;
    onChange?: (obj: string | string[]) => any; //onchange事件，将选中dict输出
}

export class AkFormIdentityPicker extends React.Component<AkFormIdentityPickerProp, AkIdentityPickerState> {
   

    constructor(props, context) {
        super(props, context);
        this.state={
            upDisplayTag:false
        }
    }
    componentWillReceiveProps(nextProps) {
        if ("value" in nextProps && nextProps.value !== this.props.value) {
            this.setState({upDisplayTag:true});
        }
    }

    handleUserChanges(value) {
        if (this.props.onChange) {
            if (!value) this.props.onChange(value);
            else if (Array.isArray(value)) this.props.onChange(value.map(i => i.ID));
            else this.props.onChange(value.ID);
        }
    }

    render() {
        const { onChange, ...other } = this.props;
        return <AkIdentityPicker upDisplayTag={this.state.upDisplayTag} onChange={value => this.handleUserChanges(value)} {...other} />
    }
}