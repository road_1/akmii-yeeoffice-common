import * as React from "react";
import {Component} from "react";
import {IntlProps} from "../../util";
import {AkSpin, AkSelect} from "../controls";
import {AkLocation, AkIdentity} from "../../api/";
import {IdentityLocale} from "../../locales/localeid";
import {AkGlobal} from "../../util/common";
import {connect} from "react-redux";
import {IdentityAction} from "../../actions/index";

export interface AkLocationSelectLabelProp extends IntlProps {
    className?: string;
    value?: string|AkIdentity;
    locations?: AkLocation[];
}

export interface AkLocationSelectLabelState {
}

export class AkLocationSelectLabel extends Component < AkLocationSelectLabelProp,
    AkLocationSelectLabelState > {

    constructor(props, context) {
        super(props, context);
    }
    componentDidMount(){
        AkGlobal.store.dispatch(IdentityAction.requestLocations());
    }
    parseValue(value) {
        let val = value;

        if (value && value.ID) {
            val = value.ID;
        }
        return val;
    }

    render() {
        const {value,locations,className} = this.props;
        let id=this.parseValue(value);
        let obj=locations.find(obj=>obj.ID===id);

        return <span className={className}>
            {
                obj?obj.Name:null
            }
        </span>
    }
}

const mapStateToProps = (state) => {
    return {
        locations: state.identity.locations,
        locationLoaded: state.identity.locationLoaded,
    }
}

export default connect(mapStateToProps)(AkLocationSelectLabel);
