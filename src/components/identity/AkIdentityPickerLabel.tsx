import * as React from "react";
import { IdentityAPI, AkIdentity } from "../../api/";
import { AkIdentityType } from "../../api/identity/identity";

export interface AkIdentityPickerLabelProp {
    className?: string;
    identityType: AkIdentityType;
    value: string | string[] | AkIdentity | AkIdentity[]; //和onChange一起结合使用，如果value赋值则defaultValue无效
}

export interface AkIdentityPickerLabelState {
    value?: Object;
}

export class AkIdentityPickerLabel extends React.Component<AkIdentityPickerLabelProp, AkIdentityPickerLabelState> {
    constructor(props, context) {
        super(props, context);

        this.state = {
            value: null,
        };
    }

    componentDidMount() {
        this.parseUnresolvedValue(this.props.value).then(d => {
            this.setState({
                value: IdentityAPI.identityArray2Dict(d)
            })
        });
    }

    componentWillReceiveProps(nextProps: AkIdentityPickerLabelProp) {
        if ("value" in nextProps && nextProps.value !== this.props.value) {
            this.parseUnresolvedValue(nextProps.value).then(d => {
                this.setState({ value: IdentityAPI.identityArray2Dict(d) })
            });
        }
    }
    render() {
        const { value } = this.state;
        if (!value) return null;

        return <div className={this.props.className}>
            {IdentityAPI.identityDict2Array(value).map(item => item.Name).join(";")}
        </div>;
    }

    async parseUnresolvedValue(val) {
        let result = [];
        let valArr = [];
        if (val) {
            if (val instanceof Array) {
                valArr = val;
            } else {
                valArr.push(val);
            }

            //获取所有需要解析的id数组，一次获取解析
            let identityIDs = [];
            valArr.forEach(p => {
                if (p && p instanceof Object && ("ID" in p || p instanceof AkIdentity)) {
                    result.push(p);
                } else if (p) {
                    if (p in resolvedIdentities) {
                        result.push(resolvedIdentities[p]);
                    } else {
                        identityIDs.push(p);
                    }
                }
            });

            if (identityIDs.length > 0) {
                let ids = await this.getIdentityFromSimpleValue(identityIDs);
                ids.forEach(id => {
                    resolvedIdentities[id.ID] = id;
                    result.push(id);
                });
            }
        }

        return result;
    }

    async getIdentityFromSimpleValue(vals) {
        let checkArray = [];
        if (vals instanceof String) {
            checkArray.push(vals);
        } else {
            checkArray = vals;
        }
        //默认搜索使用identitytypes的第一个选项，不然值应该是AkIdentity对象
        let identities = checkArray.map(ca => {
            return { ID: ca, Type: this.props.identityType }
        });
        let rs = await IdentityAPI.resolveIdentities({ identities: identities });
        ///todo: process error?
        return rs.Data;
    }
}

let resolvedIdentities: {
    [key: string]: AkIdentity;
} = {};