import * as React from "react";
import { Component } from "react";
import { AkSelect, AkSpin } from "../controls";
import { AkGroup, AkIdentity } from "../../api/";
import { IdentityLocale } from "../../locales/localeid";
import { AkGlobal } from "../../util/common";
import { connect } from "react-redux";
import { IdentityAction } from "../../actions/index";

export interface AkGroupSelectProp {
    placeholder?: string;
    notFoundContent?: string;
    defaultValue?: string | AkIdentity;
    value?: string | AkIdentity;
    onChange?: (value: string, identity: AkGroup) => any; //onchange事件，将选中dict输出
    groups?: AkGroup[];
    groupLoaded?: boolean;
}

export interface AkGroupSelectState {
    value?: string;
}

export class AkGroupSelect extends Component<AkGroupSelectProp,
    AkGroupSelectState> {

    static defaultProps: AkGroupSelectProp = {}

    constructor(props, context) {
        super(props, context);
        let value = "value" in props ? props.value : props.defaultValue;
        this.state = { value: this.parseValue(value) }
    }
    componentDidMount() {
        AkGlobal.store.dispatch(IdentityAction.requestGroups());
    }
    parseValue(value) {
        if (value && value.ID) {
            return value.ID;
        } else {
            return value;
        }
    }

    componentWillReceiveProps(nextProps) {
        if ("value" in nextProps && nextProps.value !== this.props.value) {
            this.setState({ value: this.parseValue(nextProps.value) });
        }
    }

    valueChanged(v) {
        if (!("value" in this.props)) {
            this.setState({ value: v });
        }

        if (this.props.onChange) {
            const { groups } = this.props;
            let identity;
            if (v && v.length > 0) {
                identity = groups.find(r => r.ID === v);
            }
            this.props.onChange(v, identity);
        }
    }

    render() {
        const { placeholder, notFoundContent, groupLoaded, groups } = this.props;
        const intl = AkGlobal.intl;

        let pl = placeholder ? placeholder : intl.formatMessage({ id: IdentityLocale.GroupPlaceholder });
        let value = this.state.value === null || this.state.value === "" ? undefined : this.state.value;
        return <AkSpin spinning={!groupLoaded}><AkSelect placeholder={pl} notFoundContent={notFoundContent}
            value={value}
            onChange={v => this.valueChanged(v)}>
            {groups.map(r => {
                return <AkSelect.Option key={r.ID} value={r.ID}>{r.Name}</AkSelect.Option>
            })}
        </AkSelect></AkSpin>
    }
}


const mapStateToProps = (state) => {
    return {
        groupLoaded: state.identity.groupLoaded,
        groups: state.identity.groups
    }
}

export default connect(mapStateToProps)(AkGroupSelect);
