import * as React from "react";
import * as classNames from "classnames";
import { IdentityAPI, AkIdentity, AkIdentityType, SearchIdentitiesRequest } from "../../api/";
import { AkIdentityPickerAdv } from "./AkIdentityPickerAdv";
import { IdentityLocale } from "../../locales";
import { AkModal, AkTagInput, AkTagValueDescriber } from "../controls";
import { AkGlobal } from '../../util/common';
import { IdentityUtil } from '../../reducers/Identity';
import { AkUtil } from '../../util/util';
import { AkUser } from "../../index";
import { isNullOrUndefined } from "util";

export interface AkIdentityPickerAbstractProp {
    className?: string;
    identityTypes?: AkIdentityType[];
    searchRowCount?: number;
    multiple?: boolean; //is allow multiple selection
    maxSelection?: number; //how many identity selection allowed
    maxDisplay?: number; //how many identity will be displayed on control
    displayAll?: boolean; //是否显示全部选项
    nameDisplayLength?: number; //超出长度的名称会被截取
    style?: React.CSSProperties; //additional style apply to control
    placeholder?: string; //输入默认的提示语
    defaultValue?: string | string[] | AkIdentity | AkIdentity[] | (string | AkIdentity)[]; //default selection
    readonly?: boolean;
    autoCollapse?: boolean; //展开隐藏内容后，鼠标离开是否自动收起
    value?: string | string[] | AkIdentity | AkIdentity[] | (string | AkIdentity)[]; //和onChange一起结合使用，如果value赋值则defaultValue无效
    hideInputOnMaxSelectReached?: boolean;
    parentId?: string;
    treeSelect?: boolean;
    orgCheckedStrategy?: 'ALL' | 'PARENT' | 'CHILD';//选择子节点的模式
    selectionRange?: string[] | AkIdentity[] | (string | AkIdentity)[]; //可选择范围，赋值之后，用户或者组织等只可以从这个范围中选取
    upDisplayTag?: boolean;//更新展示的Tag
    fieldDataList?: any[];
}

export interface AkIdentityPickerProp extends AkIdentityPickerAbstractProp {
    onChange?: (obj: AkIdentity | AkIdentity[]) => any; //onchange事件，将选中dict输出
}

export interface AkIdentityPickerState {
    value?: Object;
    initialized?: boolean; //是否初始化完成
    displayAll?: boolean;
    searchResult?: AkIdentity[];
    advanceVisible?: boolean; //高级选择器显示
    advanceTempValue?: Object; //advance picker的临时值
    inputValue?: string;
    hideInput?: boolean;
    selectionRange?: AkIdentity[]; //可选择范围
    upDisplayTag?: boolean;//更新展示范围
}

let resolvedIdentities: {
    [key: string]: AkIdentity;
} = {};

const describer: AkTagValueDescriber = {
    id: "ID",
    label: "Name"
}

export class AkIdentityPicker extends React.Component<AkIdentityPickerProp, AkIdentityPickerState> {
    static defaultProps: AkIdentityPickerProp = {
        identityTypes: [AkIdentityType.User],
        searchRowCount: 10,
        multiple: false,
        maxSelection: 200,
        maxDisplay: 5,
        nameDisplayLength: 20,
        readonly: false,
        displayAll: false,
        autoCollapse: false,
        hideInputOnMaxSelectReached: true
    }

    constructor(props, context) {
        super(props, context);

        this.state = {
            value: {},
            initialized: false,
            searchResult: [],
            advanceVisible: false,
            advanceTempValue: {},
            selectionRange: null
        };
    }

    componentDidMount() {
        let value = ("value" in this.props) ? this.props.value : this.props.defaultValue;
        if (!value) {
            this.setState({ initialized: true })
        } else {
            this.parseUnresolvedValue(value).then(d => {
                this.setState({ value: IdentityAPI.identityArray2Dict(d), initialized: true })
            });
        }

        this.setSelectionRange(this.props.selectionRange);
    }

    setSelectionRange(range) {
        if (range) {
            if (range.length > 0) {
                this.parseUnresolvedValue(range).then(d => {
                    this.setState({ selectionRange: d });
                });
            } else {
                this.setState({ selectionRange: [] })
            }
        } else {
            this.setState({ selectionRange: null })
        }
    }

    ifValueAreEqual(v1, v2) {
        return v1 === v2 || JSON.stringify(v1) === JSON.stringify(v2);
    }

    componentWillReceiveProps(nextProps: AkIdentityPickerProp) {
        if ("value" in nextProps && nextProps.value !== this.props.value) {
            this.parseUnresolvedValue(nextProps.value).then(d => {
                this.setState({ value: IdentityAPI.identityArray2Dict(d), initialized: true })
            });
        }

        if ("multiple" in nextProps && nextProps.multiple !== this.props.multiple) {
            let tempMaxSelection = nextProps.multiple ? nextProps.maxSelection || this.props.maxSelection : 1;
            if (nextProps.multiple === false && tempMaxSelection === 1) {
                this.setState({ value: {} });
            }
        }
        if ("maxSelection" in nextProps && nextProps.maxSelection !== this.props.maxSelection) {
            const { value } = this.state;
            if (value && nextProps.maxSelection !== Object.getOwnPropertyNames(value).length) {
                if (nextProps.maxSelection < Object.getOwnPropertyNames(value).length) {
                    this.setState({ value: [] });
                }
            }
        }

        if ("selectionRange" in nextProps && nextProps.selectionRange !== this.props.selectionRange) {
            this.setSelectionRange(nextProps.selectionRange);
        }

        if ("value" in nextProps && !isNullOrUndefined(nextProps.value)
            && "upDisplayTag" in nextProps && nextProps.upDisplayTag === true
            && "selectionRange" in nextProps && !isNullOrUndefined(nextProps.selectionRange)) {
            let tempValue = nextProps.value;
            if (AkUtil.isArray(tempValue)) {
                tempValue = [];
                (nextProps.value as any[]).forEach(item => {
                    if (AkUtil.isArray(nextProps.selectionRange)) {
                        if ((nextProps.selectionRange as any[]).findIndex(f => {
                            if (f.ID && (f.ID === item)) {//如果f 为AkIdentity
                                return f;
                            } else if (f === item) {
                                return f;
                            }
                        }) > -1) {
                            (tempValue as any[]).push(item);
                        }
                    } else {
                        (tempValue as any[]).push(nextProps.selectionRange);
                    }
                });
            } else {
                let findItem = null;
                //当未开启多选时，切 选择范围改变不包含默认值元素时
                if (AkUtil.isArray(nextProps.selectionRange) && (nextProps.selectionRange as any[]).findIndex(f => {
                    if (f.ID && (f.ID === tempValue)) {//如果f 为AkIdentity
                        findItem = f;
                        return f;
                    } else if (f === tempValue) {
                        findItem = f;
                        return f;
                    }
                }) === -1) {
                    tempValue = [];
                } else {
                    if (findItem) {
                        tempValue = findItem;
                    } else {
                        //Bug 49647: 流程设计器-用户控件选择范围为变量当前用户，在表单中用户选择框中无数据，提交后也不会回显
                        tempValue = nextProps.selectionRange !== tempValue ? [] : tempValue;
                    }
                }
            }
            this.parseUnresolvedValue(tempValue).then(d => {
                this.setState({ value: IdentityAPI.identityArray2Dict(d), initialized: true })
            });
        }
    }

    changeValue(value: Object) {
        if (!("value" in this.props)) {
            //如果外部传入value，则value由外部控制
            this.setState({ value: value });
        }
        if (this.props.onChange) {
            if (this.props.multiple) {
                this.props.onChange(Object.keys(value).map(k => value[k]));
            } else {
                let values = Object.keys(value).map(k => value[k]);
                this.props.onChange(values.length > 0 ? values[0] : null);
            }
        }
    }

    async parseUnresolvedValue(val) {

        let result = [];
        let valArr = [];
        if (val) {
            if (val instanceof Array) {
                valArr = val;
            } else {
                valArr.push(val);
            }

            //获取所有需要解析的id数组，一次获取解析
            let identityIDs = [];
            valArr.forEach(p => {

                if (IdentityUtil.isValidIdentity(p)) {
                    result.push(p);
                }
                else if (p) {
                    //不是完整的identity对象，需要执行解析
                    let id;
                    if (p instanceof Object && "ID" in p) {
                        id = p.ID;
                    } else {
                        //p是id
                        id = p;
                    }

                    if (id in resolvedIdentities) {
                        result.push(resolvedIdentities[id]);
                    } else {
                        identityIDs.push(p);
                    }
                }
            });

            if (identityIDs.length > 0) {
                let ids = await this.getIdentityFromSimpleValue(identityIDs);
                if (ids && ids.length > 0) {
                    ids.forEach(id => {
                        resolvedIdentities[id.ID] = id;
                        result.push(id);
                    });
                }
            }
        }

        return result;
    }

    //#region  已注释内容

    // //根据设置的defaultvalue解析出AkIdentity对象数组
    // async parseDefaultValue(val) {
    //     let result = [];
    //     if (val !== undefined) {
    //         if (val instanceof Array) {
    //             //获取所有需要解析的id数组，一次获取解析
    //             let identityIDs = [];
    //             val.forEach(p => {
    //                 if (p instanceof AkIdentity) {
    //                     result.push(p);
    //                 } else {
    //                     identityIDs.push(p);
    //                 }
    //             });
    //
    //             if (identityIDs.length > 0) {
    //                 result = result.concat(await this.getIdentityFromSimpleValue(identityIDs));
    //             }
    //         } else {
    //             await this.parseSingleValue(result, val);
    //         }
    //     }
    //     return result;
    // }
    //
    // /**
    //  * 解析单个value
    //  * @param arr
    //  * @param val
    //  * @returns {Promise<void>}
    //  */
    // async parseSingleValue(arr, val) {
    //     if (val instanceof String) {
    //         let str = val as string;
    //         if (str in resolvedIdentities) {
    //             arr.push(resolvedIdentities[str]);
    //         } else {
    //             let ids = await this.getIdentityFromSimpleValue([val]);
    //             if (ids.length>0) {
    //                 let id = ids[0];
    //                 resolvedIdentities[id.ID] = id;
    //                 arr.push(id);
    //             }
    //         }
    //     } else if (val instanceof AkIdentity) {
    //         arr.push(val);
    //     } else {
    //         //not support value type
    //     }
    // }

    // tagClosed(removedTag) {
    //     let value = this.state.value;
    //     delete value[removedTag.ID];
    //     this.changeValue(value);
    // }

    // /**
    //  * 显示选中的Identity名称
    //  * @returns {any[]}
    //  */
    // getTagDisplay() {
    //     const {value} = this.state;
    //
    //     return (this.props.displayAll || this.state.displayAll
    //         ? Object.keys(value)
    //         : Object.keys(value).slice(0, this.props.maxDisplay)).map((v) => {
    //
    //         return this.getSingleTagDisplay(value[v]);
    //     });
    // }
    //
    // getSingleTagDisplay(value: AkIdentity) {
    //     let islong = value.Name.length > this.props.nameDisplayLength;
    //     return <AkTag
    //         key={value.ID}
    //         closable={!this.props.readonly}
    //         afterClose={() => this.tagClosed(value)}>
    //         {islong
    //             ? value
    //                 .Name
    //                 .slice(0, this.props.nameDisplayLength)
    //             : value.Name}
    //     </AkTag>
    // }

    // /**
    //  * 显示更多按钮
    //  * @returns {any}
    //  */
    // getMoreDisplay() {
    //     const {value} = this.state;
    //     let moreCount = Object
    //             .keys(value)
    //             .length - this.props.maxDisplay;
    //
    //     if (!this.props.displayAll && !this.state.displayAll && moreCount > 0) {
    //         return <AkButton
    //             style={{
    //             marginRight: 8
    //         }}
    //             onClick={() => {
    //             this.setState({displayAll: true})
    //         }}
    //             size="small">{moreCount + ' more'}</AkButton>;
    //     }
    //     return null;
    // }

    // /**
    //  * 如果设置了自动折叠，对应的处理逻辑
    //  */
    // handleContainerMouseLeave() {
    //     if (this.props.autoCollapse && !this.props.displayAll) {
    //         this.setState({displayAll: false});
    //     }
    // }

    // /**
    //  * 根据输入值的变化调用后端搜索api
    //  * @param value 当前输入的值
    //  */
    // handleInputChange(value) {
    //     return new Promise<any[]>((resolve, reject) => {
    //         let requestModel: SearchIdentitiesRequest = {
    //             types: this.props.identityTypes,
    //             keyword: value,
    //             rowCount: this.props.searchRowCount
    //         };
    //         if (this.props.treeSelect === true) {
    //             requestModel.IsChild = true;
    //         }
    //         if (this.props.parentId) {
    //             requestModel.ParentOrgID = this.props.parentId;
    //         }

    //         IdentityAPI
    //             .searchIdentities(requestModel)
    //             .then(d => {
    //                 resolve(d.Data);
    //             }, (e) => {
    //                 reject();
    //             });
    //     });
    // }

    // handleIdentitySelected(value, option) {
    //     this.setState({inputValue: ""});
    //
    //     var fs = this.state.searchResult.find(sr => {
    //         return sr.ID === value;
    //     });
    //
    //     if (fs) {
    //         this.addValue(fs);
    //     }
    // }

    // addValue(value: AkIdentity) {
    //     const {hideInputOnMaxSelectReached} = this.props;
    //     let v = this.state.value;
    //     const {maxSelection} = this.state;
    //     if (!v.hasOwnProperty(value.ID)) {
    //         if (maxSelection > 0 && Object.keys(v).length >= maxSelection) {
    //             if (maxSelection === 1) {
    //                 v = {};
    //                 v[value.ID] = value;
    //                 this.changeValue(v);
    //             }
    //         } else {
    //             v[value.ID] = value;
    //             this.changeValue(v);
    //         }
    //     }
    // }

    //#endregion

    async getIdentityFromSimpleValue(vals) {
        const defaultType = this.props.identityTypes[0];
        //默认搜索使用identitytypes的第一个选项，不然值应该是AkIdentity对象
        let identities = AkUtil.toArray(vals).map(ca => {
            if (typeof (ca) === "string") {
                //没有type类型 则赋值默认类型
                return { ID: ca, Type: defaultType }
            } else {
                //ca是对象
                if ("Type" in ca) {
                    return ca;
                } else {
                    ca["Type"] = defaultType;
                    return ca;
                }
            }
        });
        let rs = await IdentityAPI.resolveIdentities({ identities: identities });
        ///todo: process error?
        return rs.Data;
    }

    async inputChangeAsync(value) {
        let result;

        const { selectionRange } = this.state;

        if (selectionRange) {
            //有设置选择范围，不从服务搜索
            result = AkIdentityPicker.searchIdentitiesFromArray(value, selectionRange);
        } else {
            let requestModel: SearchIdentitiesRequest = {
                types: this.props.identityTypes,
                keyword: value,
                rowCount: this.props.searchRowCount
            };
            if (this.props.treeSelect === true) {
                requestModel.IsChild = true;
            }
            if (this.props.parentId) {
                requestModel.ParentOrgID = this.props.parentId;
            }
            let rs = await IdentityAPI.searchIdentities(requestModel);
            if (rs.Status === 0) {
                result = rs.Data;
            }
        }

        return result;
    }

    static searchIdentitiesFromArray(keyword: string, identities: AkIdentity[], type?, orgId?: string) {
        let rs = [];
        let key = "";
        if (keyword) {
            key = keyword.trim().toLowerCase();
        }
        // if (key.length === 0) {
        //     //如果没有输入内容，则返回全部结果
        //     return identities;
        // }

        if (identities && identities.length > 0) {
            AkUtil.each(identities, (i: AkIdentity) => {
                //Bug 49405: 表单设计器:用户控件选择范围设置为用户，申请页面选择时选人弹框选择组织搜索无效
                if (i.Name && (!type || i.Type === type) && (!orgId || i.Attr["OrgID"] === orgId)) {//Bug 49405: 表单设计器:用户控件选择范围设置为用户，申请页面选择时选人弹框选择组织搜索无效
                    if (key.length === 0) {
                        rs.push(i);
                    } else if (i.Type === AkIdentityType.User) {
                        if (i.Name.toLowerCase().indexOf(key) > -1 || i.Attr['Email'].toLowerCase().indexOf(key) > -1) {
                            rs.push(i);
                        }
                    } else if (i.Name.toLowerCase().indexOf(key) > -1) {
                        if (!type || i.Type === type) {
                            rs.push(i);
                        }
                    }
                }
            });
        }
        return rs;
    }

    advancePickerOK() {
        this.setState({ advanceVisible: false });
        this.changeValue(Object.assign({}, this.state.advanceTempValue));
    }

    advancePickerCancel() {
        this.setState({ advanceVisible: false, advanceTempValue: this.state.value });//直接传对象，因为adv控件不直接使用
    }

    advanceValueChange(value) {

        this.setState({ advanceTempValue: value });
    }

    getAdvancePickerDisplay() {
        const { value, onChange, maxDisplay, displayAll, style, placeholder, defaultValue, readonly, autoCollapse, selectionRange, ...others } = this.props;

        return this.state.advanceVisible ?
            <AkModal style={{ top: 30 }} visible={true} onOk={this.advancePickerOK.bind(this)}
                onCancel={() => {
                    // this.resetValue = true;
                    this.advancePickerCancel();
                }}>
                <AkIdentityPickerAdv value={this.state.advanceTempValue} fieldDataList={this.props.fieldDataList}
                    // resetValue={this.resetValue}
                    onChange={this.advanceValueChange.bind(this)} selectionRange={this.state.selectionRange} {...others}></AkIdentityPickerAdv>
            </AkModal> : undefined;
    }

    displayAdvancePicker() {
        let newValue = Object.assign({}, this.state.value);
        this.setState({ advanceVisible: true, advanceTempValue: newValue });
    }

    // getInputDisplay() {
    //     const {formatMessage} = this.props.intl;
    //     const {placeholder, readonly, hideInputOnMaxSelectReached} = this.props;
    //     const {searchResult, inputValue, hideInput, value, maxSelection} = this.state;
    //
    //     if (readonly || (hideInputOnMaxSelectReached && maxSelection > 0 && Object.keys(value).length >= maxSelection)) {
    //         //readonly or hide input on reached max selection
    //         return undefined;
    //     } else {
    //         return <AkAutoComplete
    //             value={inputValue}
    //             iconType="organization"
    //             iconClick={this.displayAdvancePicker.bind(this)}
    //             showSearch={true}
    //             placeholder={placeholder?placeholder:formatMessage({id:IdentityLocale.SimplePlaceholder})}
    //             dataSource={searchResult?searchResult.map(d=>{return {value: d.ID, text: d.Name}}):[]}
    //             onInputChange={value => this.handleInputChange(value)}
    //             onSelect={this.handleIdentitySelected.bind(this)}/>;
    //     }
    // }

    render() {
        const { formatMessage } = AkGlobal.intl;
        const { multiple, maxSelection, maxDisplay, displayAll, nameDisplayLength, placeholder, readonly, autoCollapse, hideInputOnMaxSelectReached } = this.props;
        const { initialized, value } = this.state;

        const className = classNames("ak-identity-picker-wrapper", { multiple: multiple });
        let _vale = IdentityAPI.identityDict2Array(value);
        return <div className={className}>
            <AkTagInput className={this.props.className} multiple={multiple} maxSelection={maxSelection}
                maxDisplay={maxDisplay} displayAll={displayAll} nameDisplayLength={nameDisplayLength}
                placeholder={placeholder ? placeholder : formatMessage({ id: IdentityLocale.SimplePlaceholder })}
                onChange={(arr, v) => this.changeValue(v)}
                onInputChange={value => this.inputChangeAsync(value)}
                readonly={readonly} autoCollapse={autoCollapse}
                hideInputOnMaxSelectReached={hideInputOnMaxSelectReached}
                advanceIcon="organization" advanceIconClick={this.displayAdvancePicker.bind(this)}
                value={_vale}
                showSpin={!initialized} valueDescriber={describer}
                dropdownMatchSelectWidth={!multiple} />
            {this.getAdvancePickerDisplay()}
        </div>;
    }
}