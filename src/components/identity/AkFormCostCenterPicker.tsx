import * as React from "react";
import { AkCostCenterPicker, AkCostCenterPickerAbstractProp } from "./AkCostCenterPicker";

export interface AkFormCostCenterPickerProp extends AkCostCenterPickerAbstractProp {
    onChange?: (obj: string | string[]) => any; //onchange事件，将选中dict输出
}

export class AkFormCostCenterPicker extends React.Component<AkFormCostCenterPickerProp, undefined> {
    constructor(props, context) {
        super(props, context);
    }

    handleUserChanges(value) {
        if (this.props.onChange) {
            if (!value) this.props.onChange(value);
            else if (Array.isArray(value)) this.props.onChange(value.map(i => i.ID));
            else this.props.onChange(value.ID);
        }
    }

    render() {
        const { onChange, ...other } = this.props;
        return <AkCostCenterPicker onChange={value => this.handleUserChanges(value)} {...other} />
    }
}