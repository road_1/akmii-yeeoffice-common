import * as React from "react";
import { Component } from "react";
import { IntlProps } from "../../util/";
import { AkSpin, AkSelect } from "../controls/";
import { AkPosition, AkIdentity } from "../../api/";
import { IdentityLocale } from "../../locales/localeid";
import { connect } from "react-redux";
import { AkGlobal } from "../../util/common";
import { IdentityAction } from "../../actions/index";

export interface AkPositionSelectProp {
    style?: React.CSSProperties;
    placeholder?: string;
    notFoundContent?: string;
    defaultValue?: string | AkIdentity;
    value?: string | AkIdentity;
    onChange?: (value: string, identity: AkPosition) => any; //onchange事件，将选中dict输出
    positions?: AkPosition[],
    positionLoaded?: boolean
}

export interface AkPositionSelectState {
    value?: string;
}

export class AkPositionSelect extends Component<AkPositionSelectProp,
    AkPositionSelectState> {

    static defaultProps: AkPositionSelectProp = {}

    constructor(props, context) {
        super(props, context);

        let value = "value" in props ? props.value : props.defaultValue;
        this.state = { value: this.parseValue(value) };
    }

    componentDidMount() {
        AkGlobal.store.dispatch(IdentityAction.requestPositions());
    }

    parseValue(value) {
        if (value && value.ID) {
            return value.ID;
        } else {
            return value;
        }
    }

    componentWillReceiveProps(nextProps) {
        if ("value" in nextProps && nextProps.value !== this.props.value) {
            this.setState({ value: this.parseValue(nextProps.value) });
        }
    }

    valueChanged(v) {
        if (!("value" in this.props)) {
            this.setState({ value: v });
        }

        if (this.props.onChange) {
            const { positions } = this.props;
            let identity;
            if (v && v.length > 0) {
                identity = positions.find(r => r.ID === v);
            }

            this.props.onChange(v, identity);
        }
    }

    render() {
        const { placeholder, notFoundContent, positionLoaded, positions, style } = this.props;
        const intl = AkGlobal.intl;

        let pl = placeholder ? placeholder : intl.formatMessage({ id: IdentityLocale.PositionPlaceholder });
        let value = this.state.value === null || this.state.value === "" ? undefined : this.state.value;
        return <AkSpin spinning={!positionLoaded}><AkSelect placeholder={pl} notFoundContent={notFoundContent}
            style={style}
            value={value}
            onChange={v => this.valueChanged(v)}>
            {positions.map(r => {
                return <AkSelect.Option key={r.ID} value={r.ID}>{r.Name}</AkSelect.Option>
            })}
        </AkSelect></AkSpin>
    }
}

const mapStateToProps = (state) => {
    return {
        positions: state.identity.positions,
        positionLoaded: state.identity.positionLoaded
    }
}
export default connect(mapStateToProps)(AkPositionSelect);
