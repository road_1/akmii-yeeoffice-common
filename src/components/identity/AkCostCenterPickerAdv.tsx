import * as React from "react";
import { connect } from "react-redux";
import {
    AkTab,
    AkTag,
    AkSearch,
    AkTreeSelect,
    AkRow,
    AkCol,
    AkTable,
    AkColumnProps,
    AkTableRowSelection
} from "../controls/";
import { IdentityAPI, AkOrganization, AkIdentity, AkIdentityType } from "../../api/";
import { IdentityLocale, CommonLocale } from "../../locales";
import { AkGlobal } from "../../util";
import { IdentityAction } from "../../actions";
import { AkCostCenter, SearchCostCenterRequest } from "../../index";

const AkTabPane=AkTab.TabPane;
export interface AkCostCenterPickerAdvProp {
    identityTypes?: AkIdentityType[];
    searchRowCount?: number;
    multiple?: boolean; //is allow multiple selection
    maxSelection?: number; //how many identity selection allowed
    nameDisplayLength?: number; //超出长度的名称会被截取
    style?: Object; //additional style apply to control
    defaultValue?: AkIdentity[]; //default selection
    value?: Object | AkIdentity[]; //和onChange一起结合使用，如果value赋值则defaultValue无效
    onChange?: (value: Object) => void;
    allowRowClickSelection?: boolean; //允许通过点击行选中
    treeDefaultExpandAll?: boolean; //树选择是否自动展开
    organizations?: AkOrganization[];
    organizationLoaded?: boolean;
    orgID?: string;
}

export interface AkCostCenterPickerAdvState {
    costcenterTabLoading?: boolean; //是否加载中状态
    costcenterTabIndex?: number; //成本中心页码
    costcenterTabTotal?: number; //总成本中心数
    costcenterSearchResult?: AkCostCenter[]; //成本中心搜索结果
    costcenterSelectedRowKeys?: string[]; // 成本中心选择结果key列表
    selectionDisabled?: boolean; //成本中心选择的checkbox是否禁用
    costCenterValue?: Object; //选中的costcenter identity; 
    disabledTree?: boolean;
    orgID?: string;
}

export class AkCostCneterTable extends AkTable<AkCostCenter> { }
const mapStateToProps = (state) => {
    return {
        organizations: state.identity.organizations,
        organizationLoaded: state.identity.organizationLoaded
    }
}
@connect(mapStateToProps)
export class AkCostCenterPickerAdv extends React.Component<AkCostCenterPickerAdvProp, AkCostCenterPickerAdvState> {
    static defaultProps: AkCostCenterPickerAdvProp = {
        identityTypes: [AkIdentityType.CostCenter],
        searchRowCount: 10,
        multiple: false,
        maxSelection: 200,
        defaultValue: [],
        allowRowClickSelection: true,
        treeDefaultExpandAll: false,
    }
    searchCostCenterRequest: SearchCostCenterRequest = {
        orgId: "0",
        keyword: "",
        pageSize: this.props.searchRowCount,
        pageIndex: 1
    };
    columns: AkColumnProps<AkCostCenter>[] = [
        {
            title: AkGlobal.intl.formatMessage({ id: CommonLocale.Code }),
            key: "Code",
            dataIndex: "Attr.Code",
            className: "name"
        },
        {
            title: AkGlobal.intl.formatMessage({ id: CommonLocale.Name }),
            key: "Name",
            dataIndex: "Name",
            className: "name"
        }
    ];

    constructor(props: AkCostCenterPickerAdvProp, context) {
        super(props, context);

        let value = ("value" in this.props)
            ? this.getDictFromValue(this.props.value)
            : this.getDictFromValue(this.props.defaultValue);
        let costcenterKeys = [];
        let costCenterValue = {};
        this.processValue(value, costcenterKeys, costCenterValue);
        this.state = {
            costcenterTabTotal: 0,
            costcenterTabLoading: true,
            costcenterTabIndex: 1,
            costcenterSearchResult: [],
            costCenterValue: costCenterValue,
            costcenterSelectedRowKeys: costcenterKeys,
            selectionDisabled: false,
            orgID: props.orgID,

        };
    }

    organizationDict: { [key: string]: AkOrganization } = {};

    componentDidMount() {
        this.searchCostCenterRequest.orgId = this.props.orgID;
        if (this.hasCostCenterTab()) {
            AkGlobal.store.dispatch(IdentityAction.requestOrganizations());
            this.searchCostCenter();
        }
    }

    /**
     * 根据传入的value，解析出costcenter、organization等对象
     * @param value
     * @param costcenterKeys
     * @param costCenterValue
     * @param orgKeys
     * @param orgValue
     */
    processValue(value: Object, costcenterKeys: string[], costCenterValue: Object) {
        Object
            .keys(value)
            .forEach(k => {
                let v: AkIdentity = value[k];
                switch (v.Type) {
                    case AkIdentityType.CostCenter:
                        costCenterValue[v.ID] = v;
                        costcenterKeys.push(v.ID);
                        break;
                    default:
                }
            });
    }

    ifValueAreEqual(v1, v2) {
        return v1 === v2 || JSON.stringify(v1) === JSON.stringify(v2);
    }

    componentWillReceiveProps(nextProps: AkCostCenterPickerAdvProp) {
        if ("value" in nextProps && !this.ifValueAreEqual(nextProps.value, this.props.value)) {
            let value = this.getDictFromValue(nextProps.value);
            let costcenterKeys = [];
            let costCenterValue = {};
            this.processValue(value, costcenterKeys, costCenterValue);
            this.setState({ costCenterValue: costCenterValue, costcenterSelectedRowKeys: costcenterKeys });
        }
    }

    /**
     * 根据传入参数适配专成Dict
     * @param data
     */
    getDictFromValue(data: AkIdentity[] | Object) {
        if (data) {
            if (data instanceof Array) {
                return IdentityAPI.identityArray2Dict(data);
            } else {
                return data;
            }
        } else {
            return {};
        }
    }

    hasTab(type: number) {
        if (this.props.identityTypes instanceof Array) {
            return this
                .props
                .identityTypes
                .indexOf(type) > -1;
        } else {
            return this.props.identityTypes === type;
        }
    }

    hasCostCenterTab() {
        return this.hasTab(AkIdentityType.CostCenter);
    }
    /**
     * 修改成本中心选中值及触发相应事件
     * @param value
     * @param rowKeys
     * @param selectionDisabled
     */
    changecostCenterValue(value: Object, rowKeys: string[], selectionDisabled: boolean) {

        const { costCenterValue } = this.state;

        this.setState({ selectionDisabled: selectionDisabled });

        if (!rowKeys) {
            rowKeys = Object.keys(value);
        }

        //如果传入value，则value由外部控制
        if (!("value" in this.props)) {
            this.setState({
                costcenterSelectedRowKeys: rowKeys,
                costCenterValue: value
            })
        }

        if (this.props.onChange) {
            this
                .props
                .onChange({ ...value });
        }
    }


    /**
     * 移除选中的identity
     * @param identity
     */
    tagClosed(identity: AkIdentity) {
        let value;
        let keys;
        let index;

        switch (identity.Type) {
            case AkIdentityType.CostCenter:
                value = this.state.costCenterValue;
                keys = this.state.costcenterSelectedRowKeys;

                delete value[identity.ID];
                index = keys.indexOf(identity.ID);
                if (index > -1) {
                    keys.splice(index, 1);
                }
                this.changecostCenterValue(value, keys, false);
                break;
            default:
        }

    }

    /**
     * 获取选中identity的Tag
     * @returns {any[]}
     */
    getTagDisplay() {
        const { costCenterValue } = this.state;

        let value = Object.assign({}, costCenterValue);

        return Object
            .keys(value)
            .map((v) => {
                let identity = value[v];
                let islong = identity.Name.length > this.props.nameDisplayLength;
                return <AkTag
                    closable={true}
                    key={identity.ID}
                    afterClose={() => this.tagClosed(identity)}>
                    {islong
                        ? identity
                            .Name
                            .slice(0, this.props.nameDisplayLength)
                        : identity.Name}
                </AkTag>;
            });
    }

    getOrgTree(organizations: AkOrganization[]) {

        return organizations.map(org => {
            this.organizationDict[org.ID] = org;
            if (org.ID == "0") {
                org.Name = AkGlobal.intl.formatMessage({ id: CommonLocale.All });
            }

            return { key: org.ID, value: org.ID, pId: org.Parent, label: org.Name }
        });
    }

    /**
     * 获取成本中心选择Tab
     * @returns {any}
     */
    getCostCenterTab() {
        const { treeDefaultExpandAll, orgID, organizations } = this.props;
        const intl = AkGlobal.intl;
        const treeDataSimpleMode = {
            id: 'key',
            rootPId: "-1"
        };
        let placeholder = intl.formatMessage({ id: CommonLocale.All });
        const organizationTree = this.getOrgTree(organizations);
        return <AkTabPane tab={intl.formatMessage({ id: IdentityLocale.CostCenter })} key="USER">
            <AkRow gutter={16}>
                <AkCol span={12}>
                    <AkTreeSelect
                        notFoundContent="loading..."
                        dropdownMatchSelectWidth={false}
                        placeholder={placeholder}
                        multiple={false}
                        treeCheckable={false}
                        treeNodeFilterProp='title'
                        showSearch={true}
                        allowClear={true}
                        value={this.state.orgID}
                        disabled={orgID != "0"}
                        treeData={organizationTree}
                        treeDataSimpleMode={treeDataSimpleMode}
                        onChange={v => this.organizationChanged(v)}
                        treeDefaultExpandedKeys={["0"]}
                        treeDefaultExpandAll={treeDefaultExpandAll} />
                </AkCol>
                <AkCol span={12}>
                    <AkSearch
                        placeholder={intl.formatMessage({ id: IdentityLocale.AdvCostCenterPlaceholder })}
                        onSearch={v => this.keywordChanged(v)} />
                </AkCol>
            </AkRow>
            <AkRow style={{
                marginTop: 5
            }}>
                {this.getCostCenterTabTable()}
            </AkRow>
        </AkTabPane>
    }

    /**
     * 获取user tab中的成本中心列表
     * @returns {any}
     */
    getCostCenterTabTable() {
        const { multiple, allowRowClickSelection } = this.props;

        let userTabPagination = {
            pageSize: this.props.searchRowCount,
            current: this.state.costcenterTabIndex,
            total: this.state.costcenterTabTotal,
            onChange: (page: number) => {
                this.setState({ costcenterTabLoading: true, costcenterTabIndex: page });
                this.searchCostCenterRequest.pageIndex = page;
                this.searchCostCenter();
            }
        }

        let rowSelection: AkTableRowSelection<AkCostCenter> = {
            type: multiple
                ? "checkbox"
                : "radio",
            selectedRowKeys: this.state.costcenterSelectedRowKeys,
            onChange: this
                .costCenterTabTableSelectChange
                .bind(this),
            getCheckboxProps: record => ({
                disabled: multiple ? this
                    .state
                    .costcenterSelectedRowKeys
                    .indexOf(record.ID) === -1 && this.state.selectionDisabled : false
            })
        };

        return <AkCostCneterTable
            rowSelection={rowSelection}
            onRowClick={allowRowClickSelection
                ? this
                    .costCenterTableRowOnClick
                    .bind(this)
                : undefined}
            size="small"
            rowKey="ID"
            columns={this.columns}
            loading={this.state.costcenterTabLoading}
            pagination={userTabPagination}
            dataSource={this.state.costcenterSearchResult} />;
    }

    /**
     * user table row click事件，更新check状态
     * @param record
     */
    costCenterTableRowOnClick(record: AkIdentity) {
        let { costCenterValue, costcenterSelectedRowKeys } = this.state;
        let { multiple, maxSelection } = this.props;

        let selectionDisabled = false;
        if (costCenterValue.hasOwnProperty(record.ID)) {
            delete costCenterValue[record.ID];
            costcenterSelectedRowKeys = Object.keys(costCenterValue);
            selectionDisabled = false;
        } else {
            if (multiple) {
                let count = Object
                    .keys(costCenterValue)
                    .length;
                if (maxSelection === 0 || count < maxSelection) {
                    costCenterValue[record.ID] = record;
                    costcenterSelectedRowKeys.push(record.ID);
                    count++;
                }
                selectionDisabled = maxSelection > 0 && count === maxSelection;
            } else {
                //单选中，直接切换选中的成本中心
                costCenterValue = {};
                costCenterValue[record.ID] = record;
                costcenterSelectedRowKeys = [record.ID];
                selectionDisabled = true;
            }
        }

        this.changecostCenterValue(costCenterValue, costcenterSelectedRowKeys, selectionDisabled);
    }

    /**
     * user table check变化后更新选中的value
     * @param selectedRowKeys
     */
    costCenterTabTableSelectChange(selectedRowKeys) {
        let value = this.state.costCenterValue;
        const { maxSelection } = this.props;

        //移除未选中的项目
        Object
            .keys(value)
            .forEach(key => {
                if (selectedRowKeys.indexOf(key) === -1) {
                    delete value[key];
                }
            });

        let selectionCount = Object
            .keys(value)
            .length;
        let rowKeys = selectedRowKeys.slice(0);

        //根据当前页的结果集，添加选中的user
        this
            .state
            .costcenterSearchResult
            .forEach(u => {
                let index = rowKeys.indexOf(u.ID);
                if (index > -1) {
                    if (!value.hasOwnProperty(u.ID)) {
                        if (maxSelection === 0 || selectionCount < maxSelection) {
                            value[u.ID] = u;
                            selectionCount++;
                        } else {
                            rowKeys.splice(index, 1);
                        }
                    }
                }
            });

        let selectionDisabled = maxSelection > 0 && selectionCount === maxSelection;

        this.changecostCenterValue(value, rowKeys, selectionDisabled);
    }

    //
    // /**
    //  * user table单行选中执行更新value，仅用于multiple为false
    //  * @param record  */ userTabTableRowClick(record: AkIdentity) {     let value
    // = {};     value[record.ID] = record;     this.setState({value: value}); }

    /**
     * 成本中心的组织变更事件
     * @param value
     */
    organizationChanged(value) {
        this.setState({ costcenterTabLoading: true, orgID: value, costcenterTabIndex: 1 });
        this.searchCostCenterRequest.pageIndex = 1;
        this.searchCostCenterRequest.orgId = value;
        this.searchCostCenter(true);
    }

    /**
     * 成本中心关键值搜索变更事件
     * @param value
     */
    keywordChanged(value) {
        this.setState({ costcenterTabLoading: true, costcenterTabIndex: 1 });
        this.searchCostCenterRequest.pageIndex = 1;
        this.searchCostCenterRequest.keyword = value;
        this.searchCostCenter(true);
    }

    /**
     * 执行成本中心搜索
     * @param reset //是否重置当前页面为第一页
     */
    searchCostCenter(reset?: boolean) {
        if (reset) {
            this.searchCostCenterRequest.pageIndex = 1;
        }

        IdentityAPI
            .searchCostCenter(this.searchCostCenterRequest)
            .then(rs => {
                let u = rs
                    .Data
                    .map(a => new AkIdentity(a));

                this.setState({ costcenterTabLoading: false, costcenterSearchResult: u, costcenterTabTotal: rs.TotalCount });
            });
    }

    // /**
    //  * 根据搜索结果和已经选择的内容，更新checked状态  */ findUserSelectedRowKeys(searchResult:
    // AkIdentity[]) {     //return Object.keys(this.state.value);     let rowKeys:
    // string[] = [];
    //
    //     if (this.props.multiple) {
    // Object.keys(this.state.value).forEach(field => {             let v =
    // this.state.value[field];             let fs = searchResult.find(sr => {
    //           return sr.ID === v.ID;             });             if (fs) {
    //          rowKeys.push(fs.ID);             }         });     }     return
    // rowKeys; }

    render() {
        const tabs = [];
        if (this.hasCostCenterTab()) tabs.push(this.getCostCenterTab());

        return <div className="ak-identity-picker-wrapper">
            <AkTab>{tabs}</AkTab>
            <div className="tag-container">
                {this.getTagDisplay()}
            </div>
        </div>
    }
}