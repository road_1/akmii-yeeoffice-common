import * as React from "react";
import { connect } from "react-redux";
import {
    AkTab,
    AkTag,
    AkSearch,
    AkTreeSimple,
    AkTreeSelect,
    AkRow,
    AkCol,
    AkTable,
    AkColumnProps,
    AkTableRowSelection,
    AkTreeData
} from "../controls/";
import { IdentityAPI, AkOrganization, SearchUserRequest, AkUser, AkIdentity, AkIdentityType, AkGroup, AkField } from "../../api/";
import { IdentityLocale, CommonLocale } from "../../locales";
import { AkGlobal } from "../../util";
import { IdentityAction } from "../../actions";
import { AkUtil } from '../../util/util';
import { AkIdentityPicker } from "../../index";
import { AkRowSelectionType } from "../controls/ak-table";

export interface AkIdentityPickerAdvProp {
    identityTypes?: AkIdentityType[];
    searchRowCount?: number;
    multiple?: boolean; //is allow multiple selection
    maxSelection?: number; //how many identity selection allowed
    nameDisplayLength?: number; //超出长度的名称会被截取
    style?: Object; //additional style apply to control
    defaultValue?: AkIdentity[]; //default selection
    value?: Object | AkIdentity[]; //和onChange一起结合使用，如果value赋值则defaultValue无效
    onChange?: (value: Object) => void;
    allowRowClickSelection?: boolean; //允许通过点击行选中
    treeDefaultExpandAll?: boolean; //树选择是否自动展开
    organizations?: AkOrganization[];
    organizationLoaded?: boolean;
    parentId?: string;
    treeSelect?: boolean;
    orgCheckedStrategy?: 'ALL' | 'PARENT' | 'CHILD';
    selectionRange?: AkIdentity[]; //可选择范围，赋值之后，用户或者组织等只可以从这个范围中选取
    disableValues?: string[];//disable的ID数组对应的checkbox;
    fieldDataList?: any[];
    // groups?:AkGroup[];
    // resetValue?:boolean;
}

export interface AkIdentityPickerAdvState {
    userTabLoading?: boolean; //是否加载中状态
    userTabIndex?: number; //用户页码
    userTabTotal?: number; //总用户数
    userSearchResult?: AkUser[]; //用户搜索结果
    userSelectedRowKeys?: string[]; // 用户选择结果key列表
    organizationCheckedKeys?: string[]; //Organization选中结果
    organizationTreeMax?: number; //organization对象最大的选择项
    selectionDisabled?: boolean; //用户选择的checkbox是否禁用
    userValue?: Object; //选中的user identity;
    orgValue?: Object; //选中的organization identity;

    groupValue?: Object; // 选中的group identity;
    fieldValue?: Object;
    groupSelectKeys?: string[];
    fieldSelectKeys?: string[];
    groupSearchResult?: AkGroup[];
    fieldSearchResult?: any[];
}

export class UserAkTable extends AkTable<AkUser> {
}

export class GroupAkTable extends AkTable<AkGroup>{
}

export class FieldAkTable extends AkTable<AkField> {

}

const mapStateToProps = (state) => {
    return {
        organizations: state.identity.organizations,
        organizationLoaded: state.identity.organizationLoaded,
        // groups:state.identity.groups,
    }
}
const AkTabPane = AkTab.TabPane;

@connect(mapStateToProps)
export class AkIdentityPickerAdv extends React.Component<AkIdentityPickerAdvProp, AkIdentityPickerAdvState> {
    static defaultProps: AkIdentityPickerAdvProp = {
        identityTypes: [AkIdentityType.User],
        searchRowCount: 10,
        multiple: false,
        maxSelection: 200,
        defaultValue: [],
        allowRowClickSelection: true,
        treeDefaultExpandAll: true,
        orgCheckedStrategy: 'PARENT',
        treeSelect: true,
    }

    searchUserRequest: SearchUserRequest = {
        orgId: "-1",
        pageSize: this.props.searchRowCount,
        pageIndex: 1
    };

    columns: AkColumnProps<AkUser>[] = [
        {
            title: AkGlobal.intl.formatMessage({ id: IdentityLocale.AdvUserColumnName }),
            key: "Name",
            dataIndex: "Name",
            className: "name"
        }, {
            title: AkGlobal.intl.formatMessage({ id: IdentityLocale.AdvUserColumnEmail }),
            key: "Email",
            dataIndex: "Attr.Email",
            className: "mail"
        }
    ];

    groupColumns: AkColumnProps<AkGroup>[] = [{
        title: AkGlobal.intl.formatMessage({ id: IdentityLocale.AdvGroupName }),
        key: "Name",
        dataIndex: "Name",
        className: "name"
    }
        // ,{
        //     title: AkGlobal.intl.formatMessage({ id: IdentityLocale.AdvGroupCode }),
        //     key: "Code",
        //     dataIndex: "Attr.Code",
        //     className: "mail"
        // }
    ];
    fieldColumns: AkColumnProps<AkField>[] = [{
        title: AkGlobal.intl.formatMessage({ id: IdentityLocale.ListFieldName }),
        key: "Name",
        dataIndex: "Name",
        className: "name"
    }]

    constructor(props: AkIdentityPickerAdvProp, context) {
        super(props, context);

        let value = ("value" in this.props)
            ? this.getDictFromValue(this.props.value)
            : this.getDictFromValue(this.props.defaultValue);

        let orgKeys = [];
        let userKeys = [];
        let groupKeys = [];
        let fieldKeys = [];

        let userValue = {};
        let orgValue = {};
        let groupValue = {};
        let fieldValue = {};

        this.processValue(value, userKeys, userValue, orgKeys, orgValue, groupKeys, groupValue, fieldKeys, fieldValue);

        this.state = {
            userTabTotal: 0,
            userTabLoading: true,
            userTabIndex: 1,
            userSearchResult: [],
            groupSearchResult: [],
            fieldSearchResult: this.getFiledmodel(props.fieldDataList),
            userValue: userValue,
            orgValue: orgValue,
            groupValue: groupValue,
            fieldValue: fieldValue,
            userSelectedRowKeys: userKeys,
            groupSelectKeys: groupKeys,
            fieldSelectKeys: fieldKeys,
            selectionDisabled: false,
            organizationCheckedKeys: orgKeys,
        };
    }

    /**
     * 获取最大可选项
     */
    getMaxSelection() {
        return this.props.multiple ? this.props.maxSelection : 1;
    }

    /**
     * 获取已选项数
     */
    getSelectedCount() {
        return this.state.userSelectedRowKeys.length + this.state.organizationCheckedKeys.length + this.state.groupSelectKeys.length + this.state.fieldSelectKeys.length;
    }

    organizationDict: { [key: string]: AkOrganization } = {};

    componentDidMount() {
        if (this.hasOrganizationTab() || this.hasUserTab()) {
            AkGlobal.store.dispatch(IdentityAction.requestOrganizations());
        }
        if (this.hasGroupTab()) {
            // AkGlobal.store.dispatch(IdentityAction.requestGroups());
            this.searchGroup();
        }
        if (this.hasUserTab()) {
            this.searchUser();
        }
    }

    /**
     * 根据传入的value，解析出user、organization等对象
     * @param value
     * @param userKeys
     * @param userValue
     * @param orgKeys
     * @param orgValue
     */
    processValue(value: Object, userKeys: string[], userValue: Object, orgKeys: string[], orgValue: Object, groupKeys: string[], groupValue: Object, fieldKeys: any[], fieldValue: Object) {
        Object
            .keys(value)
            .forEach(k => {
                let v: AkIdentity = value[k];
                switch (v.Type) {
                    case AkIdentityType.Organization:
                        orgValue[v.ID] = v;
                        orgKeys.push(v.ID);
                        break;
                    case AkIdentityType.User:
                        userValue[v.ID] = v;
                        userKeys.push(v.ID);
                        break;
                    case AkIdentityType.Group:
                        groupValue[v.ID] = v;
                        groupKeys.push(v.ID);
                        break;
                    case AkIdentityType.Field:
                        fieldValue[v.ID] = v;
                        fieldKeys.push(v.ID)
                    default:
                }
            });
    }

    ifValueAreEqual(v1, v2) {
        return v1 === v2 || JSON.stringify(v1) === JSON.stringify(v2);
    }

    componentWillReceiveProps(nextProps: AkIdentityPickerAdvProp) {
        if ("value" in nextProps && !this.ifValueAreEqual(nextProps.value, this.props.value)) {
            let value = this.getDictFromValue(nextProps.value);
            let orgKeys = [];
            let userKeys = [];
            let groupKeys = [];
            let fieldKeys = [];

            let userValue = {};
            let orgValue = {};
            let groupValue = {};
            let fieldValue = {};

            this.processValue(value, userKeys, userValue, orgKeys, orgValue, groupKeys, groupValue, fieldKeys, fieldValue);

            let maxSelection = this.getMaxSelection();
            let selectionDisabled = (userKeys.length + orgKeys.length) >= maxSelection;

            this.setState({
                userValue: userValue,
                userSelectedRowKeys: userKeys,
                orgValue: orgValue,
                groupValue: groupValue,
                groupSelectKeys: groupKeys,
                fieldSelectKeys: fieldKeys,
                organizationCheckedKeys: orgKeys,
                selectionDisabled: selectionDisabled,
                fieldSearchResult: this.getFiledmodel(nextProps.fieldDataList)
            });
        }
        // if("resetValue" in nextProps&&nextProps.resetValue){
        //     this.keywordChanged("");
        // }

        if ("selectionRange" in nextProps && nextProps.selectionRange !== this.props.selectionRange) {
            if (this.hasUserTab()) {
                this.searchUser(true, true, nextProps.selectionRange);
            }
        }
    }

    /**
     * 根据传入参数适配专成Dict
     * @param data
     */
    getDictFromValue(data: AkIdentity[] | Object) {
        if (data) {
            if (data instanceof Array) {
                return IdentityAPI.identityArray2Dict(data);
            } else {
                return data;
            }
        } else {
            return {};
        }
    }

    hasTab(type: number) {
        if (AkUtil.isArray(this.props.identityTypes)) {
            return this.props.identityTypes.indexOf(type) > -1;
        } else {
            return type === AkIdentityType.User;
        }
    }

    hasUserTab() {
        return this.hasTab(AkIdentityType.User);
    }

    hasOrganizationTab() {
        return this.hasTab(AkIdentityType.Organization);
    }
    hasGroupTab() {
        return this.hasTab(AkIdentityType.Group);
    }
    hasFieldTab() {
        return this.hasTab(AkIdentityType.Field)
    }
    /**
     * 修改用户选中值及触发相应事件
     * @param value
     * @param rowKeys
     * @param selectionDisabled
     */
    changeUserValue(value: Object, rowKeys: string[], selectionDisabled: boolean) {
        const { orgValue, groupValue, fieldValue } = this.state;
        this.setState({ selectionDisabled: selectionDisabled });

        if (!rowKeys) {
            rowKeys = Object.keys(value);
        }

        //如果传入value，则value由外部控制
        if (!("value" in this.props)) {
            this.setState({
                userSelectedRowKeys: rowKeys,
                userValue: value
            })
        }

        if (this.props.onChange) {
            this.props.onChange(Object.assign({}, orgValue, groupValue, fieldValue, value));
        }
    }
    /**
     * 修改组织选中值及触发相应事件
     * @param value
     * @param rowKeys
     * @param selectionDisabled
     */
    changeOrgValue(value: Object, rowKeys: string[], selectionDisabled: boolean) {
        const { userValue, groupValue, fieldValue } = this.state;
        this.setState({ selectionDisabled: selectionDisabled });

        if (!rowKeys) {
            rowKeys = Object.keys(value);
        }

        //如果传入value，则value由外部控制
        if (!("value" in this.props)) {
            this.setState({
                organizationCheckedKeys: rowKeys,
                orgValue: value
            })
        }

        if (this.props.onChange) {
            // let orgs = Object.keys(value).map(k => value[k]); let users =
            // Object.keys(userValue).map(k => userValue[k]);
            // this.props.onChange(orgs.concat(users));

            this
                .props
                .onChange(Object.assign({}, userValue, groupValue, fieldValue, value));
        }
    }

    changeGroupValue(value: Object, rowKeys: string[], selectionDisabled: boolean) {
        // console.log(value,rowKeys,selectionDisabled);
        const { orgValue, userValue } = this.state;

        this.setState({ selectionDisabled: selectionDisabled });

        if (!rowKeys) {
            rowKeys = Object.keys(value);
        }

        //如果传入value，则value由外部控制
        if (!("value" in this.props)) {
            this.setState({
                groupSelectKeys: rowKeys,
                groupValue: value
            })
        }

        if (this.props.onChange) {
            this.props.onChange(Object.assign({}, orgValue, userValue, value));
        }
    }
    changeFieldValue(value: Object, rowKeys: string[], selectionDisabled: boolean) {

        const { orgValue, userValue, groupValue } = this.state;

        this.setState({ selectionDisabled: selectionDisabled });

        if (!rowKeys) {
            rowKeys = Object.keys(value);
        }

        //如果传入value，则value由外部控制
        if (!("value" in this.props)) {
            this.setState({
                fieldSelectKeys: rowKeys,
                fieldValue: value as AkField
            })
        }

        if (this.props.onChange) {
            this.props.onChange(Object.assign({}, orgValue, userValue, groupValue, value));
        }
    }

    /**
     * 移除选中的identity
     * @param identity
     */
    tagClosed(identity: AkIdentity) {
        let value;
        let keys;
        let index;

        switch (identity.Type) {
            case AkIdentityType.User:
                value = this.state.userValue;
                keys = this.state.userSelectedRowKeys;

                delete value[identity.ID];
                index = keys.indexOf(identity.ID);
                if (index > -1) {
                    keys.splice(index, 1);
                }

                this.changeUserValue(value, keys, false);
                break;
            case AkIdentityType.Organization:
                value = this.state.orgValue;
                keys = this.state.organizationCheckedKeys;

                delete value[identity.ID];
                index = keys.indexOf(identity.ID);
                if (index > -1) {
                    keys.splice(index, 1);
                }

                this.changeOrgValue(value, keys, false);
                break;
            case AkIdentityType.Group:
                value = this.state.groupValue;
                keys = this.state.groupSelectKeys;

                delete value[identity.ID];
                index = keys.indexOf(identity.ID);
                if (index > -1) {
                    keys.splice(index, 1);
                }
                this.changeGroupValue(value, keys, false);
                break;
            default:
        }

    }

    /**
     * 获取选中identity的Tag
     * @returns {any[]}
     */
    getTagDisplay() {
        const { userValue, orgValue, groupValue, fieldValue } = this.state;
        const { disableValues } = this.props;
        let value = Object.assign({}, userValue, orgValue, groupValue, fieldValue);
        return Object
            .keys(value)
            .map((v) => {
                let identity = value[v];
                let islong = identity.Name.length > this.props.nameDisplayLength;
                return <AkTag
                    closable={disableValues ? disableValues.indexOf(identity.ID) === -1 : true}
                    key={identity.ID}
                    afterClose={() => this.tagClosed(identity)}>
                    {islong
                        ? identity
                            .Name
                            .slice(0, this.props.nameDisplayLength)
                        : identity.Name}
                </AkTag>;
            });
    }

    getOrgTree(organizations: AkOrganization[]) {
        const { parentId, treeSelect } = this.props;
        let tree = [];
        let pid = parentId ? parentId : "0";
        organizations.forEach(org => {
            if (treeSelect || (pid === org.Parent)) {
                let d = { key: org.ID, value: org.ID, pId: org.Parent, label: org.Name, };
                tree.push(d);
                this.organizationDict[org.ID] = org;
            }
        });


        // let tree = organizations.map(org => {
        //     this.organizationDict[org.ID] = org;
        //     return { key: org.ID, value: org.ID, pId: org.Parent, label: org.Name, }
        // });

        return tree;
        // return this.filterOrganizationTreeByParentId(tree);
    }

    // filterOrganizationTreeByParentId(tree) {
    //     let parentId = this.props.parentId;
    //     if (!parentId) return tree;

    //     let parentIds = new Set<String>([parentId]);
    //     return tree.filter(item => {
    //         if (item.key === parentId) {
    //             return false;
    //         }
    //         else if (parentIds.has(item.pId)) {
    //             if (item.pId === parentId) {
    //                 item.pId = "0";//将根节点的pId重置成0
    //             }
    //             parentIds.add(item.key);
    //             return true;
    //         }
    //         return false;
    //     });
    // }

    /**
     * 获取用户选择Tab
     * @returns {any}
     */
    getUserTab() {
        const { treeDefaultExpandAll, organizations } = this.props;
        const intl = AkGlobal.intl;

        const treeDataSimpleMode = {
            id: 'key',
            rootPId: "0"
        };
        let placeholder = intl.formatMessage({ id: IdentityLocale.Organization }) + " : " + intl.formatMessage({ id: CommonLocale.All });
        const organizationTree = this.getOrgTree(organizations);
        return <AkTabPane tab={intl.formatMessage({ id: IdentityLocale.User })} key="USER">
            <AkRow gutter={16}>
                <AkCol span={12}>
                    <AkTreeSelect
                        notFoundContent="loading..."
                        dropdownMatchSelectWidth={false}
                        placeholder={placeholder}
                        multiple={false}
                        treeCheckable={false}
                        treeNodeFilterProp='title'
                        showSearch={true}
                        allowClear={true}
                        treeData={organizationTree}
                        treeDataSimpleMode={treeDataSimpleMode}
                        onChange={v => this.organizationChanged(v)}
                        treeDefaultExpandAll={treeDefaultExpandAll} />
                </AkCol>
                <AkCol span={12}>
                    {/* //GO TO 无法清除上次的输入值  this.props.resetValue*/}
                    <AkSearch
                        placeholder={intl.formatMessage({ id: IdentityLocale.AdvUserPlaceholder })}
                        onSearch={v => this.keywordChanged(v)} />
                </AkCol>
            </AkRow>
            <AkRow style={{
                marginTop: 5
            }}>
                {this.getUserTabTable()}
            </AkRow>
        </AkTabPane>
    }
    getGroupTab() {
        const { formatMessage } = AkGlobal.intl;
        return <AkTabPane tab={formatMessage({ id: IdentityLocale.Group })} key="GROUP">
            <AkRow>
                <AkSearch onSearch={(value) => this.searchGroup(value)} placeholder={formatMessage({ id: IdentityLocale.AdvGroupPlaceholder })} />
            </AkRow>
            <AkRow style={{
                marginTop: 5
            }}>
                {this.getGroupTabTable()}
            </AkRow>
        </AkTabPane>
    }
    GetFieldTab() {
        const { formatMessage } = AkGlobal.intl;
        return <AkTabPane tab={formatMessage({ id: IdentityLocale.ListFieldTab })} key="FIELD">
            <AkRow style={{
                marginTop: 5
            }}>
                {this.getFieldTabTable()}
            </AkRow>
        </AkTabPane>
    }
    /**
     * 获取user tab中的用户列表
     * @returns {any}
     */
    getUserTabTable() {
        const { multiple, allowRowClickSelection, disableValues } = this.props;

        let userTabPagination = {
            pageSize: this.props.searchRowCount,
            current: this.state.userTabIndex,
            total: this.state.userTabTotal,
            onChange: (page: number) => {
                this.setState({ userTabLoading: true, userTabIndex: page });
                this.searchUserRequest.pageIndex = page;
                this.searchUser();
            }
        }

        let rowSelection: AkTableRowSelection<AkUser> = {
            type: multiple
                ? "checkbox"
                : "radio",
            selectedRowKeys: this.state.userSelectedRowKeys,
            onChange: this
                .userTabTableSelectChange
                .bind(this),
            getCheckboxProps: record => ({
                disabled: multiple ? (this
                    .state
                    .userSelectedRowKeys
                    .indexOf(record.ID) === -1 && this.state.selectionDisabled) ||
                    (disableValues && disableValues.indexOf(record.ID) > -1)
                    : false
            })
        };

        return <UserAkTable
            rowSelection={rowSelection}
            onRowClick={allowRowClickSelection
                ? this.userTableRowOnClick.bind(this)
                : undefined}
            size="small"
            rowKey="ID"
            columns={this.columns}
            loading={this.state.userTabLoading}
            pagination={userTabPagination}
            dataSource={this.state.userSearchResult} />;
    }
    getGroupTabTable() {
        const { multiple, allowRowClickSelection, disableValues } = this.props;

        let rowSelection: AkTableRowSelection<AkGroup> = {
            type: multiple ? "checkbox" : "radio",
            selectedRowKeys: this.state.groupSelectKeys,
            onChange: this.groupTabTableSelectChange.bind(this),
            getCheckboxProps: record => ({
                disabled: multiple
                    ?
                    (this.state.groupSelectKeys.indexOf(record.ID) === -1 && this.state.selectionDisabled)
                    ||
                    (disableValues && disableValues.indexOf(record.ID) > -1)
                    :
                    false
            })
        };
        let groupTabPagination = {
            pageSize: 10,
            total: this.state.groupSearchResult.length,
        }
        return <GroupAkTable
            rowKey="ID"
            size="small"
            onRowClick={allowRowClickSelection
                ? this.groupTableRowOnClick.bind(this)
                : undefined}
            rowSelection={rowSelection}
            columns={this.groupColumns}
            pagination={groupTabPagination}
            dataSource={this.state.groupSearchResult} />
    }
    getFiledmodel(fieldSearchResult) {
        let AkFieldList: AkField[] = [];
        if (fieldSearchResult) {
            fieldSearchResult.map((i) => {
                let t: AkField = {
                    ID: i.InternalName,
                    Type: 999,
                    Name: i.DisplayName,
                    DisplayName: i.DisplayName,
                    Attr: {}
                };
                AkFieldList.push(t);
            });
        }
        return AkFieldList;
    }
    getFieldTabTable() {
        const { multiple, allowRowClickSelection, disableValues } = this.props;
        let rowSelection = {
            type: multiple
                ? "checkbox" as AkRowSelectionType
                : "radio" as AkRowSelectionType,
            selectedRowKeys: this.state.fieldSelectKeys,
            onChange: this.FieldTabTableSelectChange.bind(this),
            getCheckboxProps: record => ({
                disabled: multiple
                    ?
                    (this.state.fieldSelectKeys.indexOf(record.ID) === -1 && this.state.selectionDisabled)
                    ||
                    (disableValues && disableValues.indexOf(record.ID) > -1)
                    :
                    false
            })
        }
        return <FieldAkTable
            rowKey="ID"
            size="small"
            onRowClick={allowRowClickSelection
                ? this.fieldTableRowOnClick.bind(this)
                : undefined
            }
            rowSelection={rowSelection}
            columns={this.fieldColumns}
            pagination={false}
            dataSource={this.state.fieldSearchResult}
        />
    }
    /**
     * user table row click事件，更新check状态
     * @param record
     */
    userTableRowOnClick(record: AkIdentity) {
        let { userValue, userSelectedRowKeys } = this.state;
        const { disableValues } = this.props;
        if (disableValues && disableValues.indexOf(record.ID) > -1) return;
        let maxSelection = this.getMaxSelection();

        let selectionDisabled = false;
        if (userValue.hasOwnProperty(record.ID)) {
            //如果已选中，移除选中项
            delete userValue[record.ID];
            userSelectedRowKeys = Object.keys(userValue);
            selectionDisabled = false;
            this.changeUserValue(userValue, userSelectedRowKeys, selectionDisabled);
        } else {
            let selectedCount = this.getSelectedCount();
            //未达到最大选择数
            if (selectedCount < maxSelection) {
                userValue[record.ID] = record;
                userSelectedRowKeys.push(record.ID);
                selectedCount++;
                selectionDisabled = selectedCount === maxSelection;
                this.changeUserValue(userValue, userSelectedRowKeys, selectionDisabled);
            }
        }
    }
    groupTableRowOnClick(record: AkIdentity) {
        let { groupValue, groupSelectKeys } = this.state;
        const { disableValues } = this.props;
        if (disableValues && disableValues.indexOf(record.ID) > -1) return;
        let maxSelection = this.getMaxSelection();
        let selectionDisabled = false;
        if (groupValue.hasOwnProperty(record.ID)) {
            //如果已选中，移除选中项
            delete groupValue[record.ID];
            groupSelectKeys = Object.keys(groupValue);
            selectionDisabled = false;
            this.changeGroupValue(groupValue, groupSelectKeys, selectionDisabled);
        } else {
            let selectedCount = this.getSelectedCount();
            //未达到最大选择数
            if (selectedCount < maxSelection) {
                groupValue[record.ID] = record;
                groupSelectKeys.push(record.ID);
                selectedCount++;
                selectionDisabled = selectedCount === maxSelection;
                this.changeGroupValue(groupValue, groupSelectKeys, selectionDisabled);
            }
        }
    }
    fieldTableRowOnClick(record: AkField) {
        let { fieldValue, fieldSelectKeys } = this.state;
        const { disableValues } = this.props;
        if (disableValues && disableValues.indexOf(record.ID) > -1) return;
        let maxSelection = this.getMaxSelection();
        let selectionDisabled = false;
        if (fieldValue.hasOwnProperty(record.ID)) {
            //如果已选中，移除选中项
            delete fieldValue[record.ID];
            fieldSelectKeys = Object.keys(fieldValue);
            selectionDisabled = false;
            this.changeGroupValue(fieldValue, fieldSelectKeys, selectionDisabled);
        } else {
            let selectedCount = this.getSelectedCount();
            //未达到最大选择数
            if (selectedCount < maxSelection) {
                fieldValue[record.ID] = record;
                fieldSelectKeys.push(record.ID);
                selectedCount++;
                selectionDisabled = selectedCount === maxSelection;
                this.changeFieldValue(fieldValue, fieldSelectKeys, selectionDisabled);
            }
        }
    }
    /**
     * user table check变化后更新选中的value
     * @param selectedRowKeys
     */
    userTabTableSelectChange(selectedRowKeys) {
        let value = this.state.userValue;
        let maxSelection = this.getMaxSelection();

        let selectedCount = this.getSelectedCount();
        //移除未选中的项目
        Object.keys(value).forEach(key => {
            if (selectedRowKeys.indexOf(key) === -1) {
                delete value[key];
                selectedCount--;
            }
        });

        let rowKeys = selectedRowKeys.slice(0);

        //根据当前页的结果集，添加选中的user，因为选中项多于当页结果
        for (var i = 0; i < this.state.userSearchResult.length; i++) {
            let u = this.state.userSearchResult[i];
            let index = rowKeys.indexOf(u.ID);
            if (index > -1) {
                if (!value.hasOwnProperty(u.ID)) {
                    if (selectedCount < maxSelection) {
                        value[u.ID] = u;
                        selectedCount++;
                    } else {
                        rowKeys.splice(index, 1);
                    }
                }
            }
        }
        let selectionDisabled = selectedCount >= maxSelection;

        this.changeUserValue(value, rowKeys, selectionDisabled);
    }
    groupTabTableSelectChange(selectedRowKeys) {
        let value = this.state.groupValue;
        let maxSelection = this.getMaxSelection();

        let selectedCount = this.getSelectedCount();
        //移除未选中的项目
        Object.keys(value).forEach(key => {
            if (selectedRowKeys.indexOf(key) === -1) {
                delete value[key];
                selectedCount--;
            }
        });

        let rowKeys = selectedRowKeys.slice(0);

        //根据当前页的结果集，添加选中的user，因为选中项多于当页结果
        for (var i = 0; i < this.state.groupSearchResult.length; i++) {
            let u = this.state.groupSearchResult[i];
            let index = rowKeys.indexOf(u.ID);
            if (index > -1) {
                if (!value.hasOwnProperty(u.ID)) {
                    if (selectedCount < maxSelection) {
                        value[u.ID] = u;
                        selectedCount++;
                    } else {
                        rowKeys.splice(index, 1);
                    }
                }
            }
        }
        let selectionDisabled = selectedCount >= maxSelection;

        this.changeGroupValue(value, rowKeys, selectionDisabled);
    }

    FieldTabTableSelectChange(selectedRowKeys) {
        let rowKeys = selectedRowKeys.slice(0);

        let value = this.state.fieldValue;
        let maxSelection = this.getMaxSelection();

        let selectedCount = this.getSelectedCount();
        Object.keys(value).forEach(key => {
            if (selectedRowKeys.indexOf(key) === -1) {
                delete value[key];
                selectedCount--;
            }
        });

        //根据当前页的结果集，添加选中的user，因为选中项多于当页结果
        for (var i = 0; i < this.state.fieldSearchResult.length; i++) {
            let u = this.state.fieldSearchResult[i];
            let index = rowKeys.indexOf(u.ID);
            if (index > -1) {
                if (!value.hasOwnProperty(u.ID)) {
                    if (selectedCount < maxSelection) {
                        value[u.ID] = u;
                        selectedCount++;
                    } else {
                        rowKeys.splice(index, 1);
                    }
                }
            }
        }
        let selectionDisabled = selectedCount >= maxSelection;
        this.changeFieldValue(value, rowKeys, selectionDisabled);
    }

    //
    // /**
    //  * user table单行选中执行更新value，仅用于multiple为false
    //  * @param record  */ userTabTableRowClick(record: AkIdentity) {     let value
    // = {};     value[record.ID] = record;     this.setState({value: value}); }

    /**
     * 用户的组织变更事件
     * @param value
     */

    organizationChanged(value) {
        this.setState({ userTabLoading: true, userTabIndex: 1 });
        this.searchUserRequest.pageIndex = 1;
        this.searchUserRequest.orgId = value;
        this.searchUser(true);
    }

    /**
     * 用户关键值搜索变更事件
     * @param value
     */
    keywordChanged(value) {
        this.setState({ userTabLoading: true, userTabIndex: 1 });
        this.searchUserRequest.pageIndex = 1;
        this.searchUserRequest.keyword = value;
        this.searchUser(true);
    }

    /**
     * 执行用户搜索
     * @param reset //是否重置当前页面为第一页
     */
    searchUser(reset?: boolean, useNewRange?: boolean, newSelectionRange?) {
        if (reset) {
            this.searchUserRequest.pageIndex = 1;
        }
        let selectionRange = useNewRange ? newSelectionRange : this.props.selectionRange;
        if (selectionRange) {
            let u;
            if (this.searchUserRequest.orgId && this.searchUserRequest.orgId !== "-1") {
                u = AkIdentityPicker.searchIdentitiesFromArray(this.searchUserRequest.keyword, selectionRange, AkIdentityType.User, this.searchUserRequest.orgId);
            } else {
                //有选择限制，从内容中查询
                u = AkIdentityPicker.searchIdentitiesFromArray(this.searchUserRequest.keyword, selectionRange, AkIdentityType.User);
            }
            this.setState({ userTabLoading: false, userSearchResult: u, userTabTotal: selectionRange.length });
        } else {
            IdentityAPI
                .searchUser(this.searchUserRequest)
                .then(rs => {
                    let u = rs.Data.map(a => new AkUser(a));

                    //已经查询的用户放入redux中
                    AkGlobal.store.dispatch(IdentityAction.identityUserLoaded(u));
                    // IdentityUtil.updateUserCacheDirectly(u);
                    this.setState({ userTabLoading: false, userSearchResult: u, userTabTotal: rs.TotalCount });
                });
        }
    }
    searchGroup(keyword?: string) {
        IdentityAPI.getGroups({ name: keyword || '' }).then(d => {
            if (d.Status === 0) {
                this.setState({
                    groupSearchResult: d.Data
                });
            }
        })
    }

    organizationDisableCheck(item: AkTreeData) {
        const { selectionRange } = this.props;
        if (selectionRange) {
            const index = selectionRange.findIndex(r => {
                return r.ID === item.key
            })
            return index < 0; //没有从选择范围种找到匹配的item，则返回true，disable选择
        }
        return false;
    }

    getOrganizationTab() {
        const { organizations, selectionRange } = this.props;
        const intl = AkGlobal.intl;
        let organizationTree = this.getOrgTree(organizations);
        // if (!this.props.treeSelect && this.props.treeSelect === false) {
        //     //仅显示顶层数据，顶层项pId===0
        //     organizationTree = organizationTree.filter(item => item.pId === "0");
        // }

        const customDisable = selectionRange;
        const simpletreemodal = {
            id: "key",
            pId: "pId",
            rootPId: this.props.parentId || "0",
            title: "label"
        }
        // let placeholder = intl.formatMessage({id: IdentityLocale.Organization}) + " :
        // " + intl.formatMessage({id: CommonLocale.All});
        return <AkTabPane
            tab={intl.formatMessage({ id: IdentityLocale.Organization })}
            key="ORGANIZATION">
            <div className="org-tree">
                <AkTreeSimple
                    disableCheck={this.state.selectionDisabled}
                    checkedKeys={this.state.organizationCheckedKeys}
                    onCheck={this
                        .organizationTreeChecked
                        .bind(this)}
                    checkable={this.getMaxSelection() > 1}
                    selectedKeys={this.getMaxSelection() > 1 ? [] : this.state.organizationCheckedKeys}
                    // onSelect={this.organizationTreeChecked.bind(this)}
                    maxChecking={this.getMaxSelection()}
                    defaultExpandAll
                    checkedStrategy={this.props.orgCheckedStrategy}
                    treeData={organizationTree}
                    simpleTreeDescriber={simpletreemodal}
                    disableItem={customDisable ? this.organizationDisableCheck.bind(this) : null}
                />
            </div>
        </AkTabPane>
    }

    organizationTreeSelected(selectedKeys: string[], treeEvent) {
        let maxSelection = this.getMaxSelection();
        if (maxSelection > 1) {
            if (treeEvent.node.props.disableCheckbox) {
                //控件已经被disable

            }
        } else {
            this.organizationTreeChecked(selectedKeys);
        }
    }

    /**
     * organizationTree 变化后更新事件
     * @param checkedKeys
     */
    organizationTreeChecked(checkedKeys: string[]) {
        const { userSelectedRowKeys } = this.state;
        let maxSelection = this.getMaxSelection();

        let disable = false;
        if (maxSelection > 0) {
            checkedKeys.splice(maxSelection - userSelectedRowKeys.length, checkedKeys.length);
            if (checkedKeys.length + userSelectedRowKeys.length >= maxSelection) {
                disable = true;
            }
        }
        let value = {};
        checkedKeys.forEach(k => {
            value[k] = this.organizationDict[k];
        });

        this.changeOrgValue(value, checkedKeys, disable);
    }

    // /**
    //  * 根据搜索结果和已经选择的内容，更新checked状态  */ findUserSelectedRowKeys(searchResult:
    // AkIdentity[]) {     //return Object.keys(this.state.value);     let rowKeys:
    // string[] = [];
    //
    //     if (this.props.multiple) {
    // Object.keys(this.state.value).forEach(field => {             let v =
    // this.state.value[field];             let fs = searchResult.find(sr => {
    //           return sr.ID === v.ID;             });             if (fs) {
    //          rowKeys.push(fs.ID);             }         });     }     return
    // rowKeys; }

    render() {
        const tabs = [];
        if (this.hasUserTab()) tabs.push(this.getUserTab());
        if (this.hasOrganizationTab()) tabs.push(this.getOrganizationTab());
        if (this.hasGroupTab()) tabs.push(this.getGroupTab());
        if (this.hasFieldTab()) tabs.push(this.GetFieldTab())
        return <div className="ak-identity-picker-wrapper">
            <AkTab >{tabs}</AkTab>
            <div className="tag-container">
                {this.getTagDisplay()}
            </div>
        </div>
    }
}