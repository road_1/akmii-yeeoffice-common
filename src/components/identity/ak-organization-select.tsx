import * as React from "react";
import { Component } from "react";
import { IntlProps } from "../../util";
import { AkTreeSelect, AkSpin } from "../controls/";
import { AkIdentity, AkOrganization } from "../../api/";
import { IdentityLocale } from "../../locales/localeid";
import { connect } from "react-redux";
import { AkGlobal } from "../../util/common";
import { IdentityAction } from "../../actions/index";

export interface AkOrganizationSelectProp {
    placeholder?: string;
    notFoundContent?: string;
    treeDefaultExpandAll?: boolean;
    defaultValue?: string | AkIdentity;
    value?: string | AkIdentity;
    onChange?: (value: string, identity: AkOrganization) => any; //onchange事件，将选中dict输出
    organizations?: AkOrganization[],
    organizationLoaded?: boolean,
    disabled?: boolean,
    showCheckedStrategy?: "SHOW_ALL" | "SHOW_PARENT" | "SHOW_CHILD"
}

export interface AkOrganizationSelectState {
    value?: string;
}

export class AkOrganizationSelect extends Component<AkOrganizationSelectProp,
    AkOrganizationSelectState> {

    static defaultProps: AkOrganizationSelectProp = {
        treeDefaultExpandAll: true
    }

    orgDict: { [key: string]: AkOrganization } = {};

    constructor(props, context) {
        super(props, context);
        let value = "value" in props ? props.value : props.defaultValue;
        this.state = { value: this.parseValue(value) }
    }
    componentDidMount() {
        AkGlobal.store.dispatch(IdentityAction.requestOrganizations());
    }
    parseValue(value) {
        if (value && value.ID) {
            return value.ID;
        } else {
            return value;
        }
    }

    getOrgTree(organizations: AkOrganization[]) {
        return organizations.map(org => {
            this.orgDict[org.ID] = org;
            return { key: org.ID, value: org.ID, pId: org.Parent, label: org.Name }
        });
    }

    componentWillReceiveProps(nextProps) {
        if ("value" in nextProps && nextProps.value !== this.props.value) {
            this.setState({ value: this.parseValue(nextProps.value) });
        }
    }

    valueChanged(v) {

        if (!("value" in this.props)) {
            this.setState({ value: v });
        }

        if (this.props.onChange) {
            let identity;
            if (v && v in this.orgDict) {
                identity = this.orgDict[v];
            }

            this.props.onChange(v, identity);
        }
    }

    render() {
        const { placeholder, notFoundContent, treeDefaultExpandAll, organizations, organizationLoaded, disabled, showCheckedStrategy } = this.props;
        const intl = AkGlobal.intl;

        let pl = placeholder ? placeholder : intl.formatMessage({ id: IdentityLocale.OrganizationPlaceholder });

        const treeDataSimpleMode = {
            id: 'key', rootPId: "0"
        }

        const organizationTree = this.getOrgTree(organizations);
        let value = this.state.value === null || this.state.value === "" ? undefined : this.state.value;
        return <AkSpin spinning={!organizationLoaded}><AkTreeSelect notFoundContent={notFoundContent}
            placeholder={pl} multiple={false}
            value={value}
            treeCheckable={false}
            treeNodeFilterProp='title' showSearch={true}
            allowClear={true}
            disabled={disabled}
            treeData={organizationTree}
            treeDataSimpleMode={treeDataSimpleMode}
            onChange={v => this.valueChanged(v)}
            treeDefaultExpandAll={treeDefaultExpandAll}
            showCheckedStrategy={showCheckedStrategy}
        /></AkSpin>
    }
}

const mapStateToProps = (state) => {
    return {
        organizations: state.identity.organizations,
        organizationLoaded: state.identity.organizationLoaded
    }
}
export default connect(mapStateToProps)(AkOrganizationSelect);
