import * as React from "react";
import { Component } from "react";
import { IntlProps } from "../../util";
import { AkSpin, AkSelect } from "../controls";
import { AkLocation, AkIdentity } from "../../api/";
import { IdentityLocale } from "../../locales/localeid";
import { AkGlobal } from "../../util/common";
import { connect } from "react-redux";
import { IdentityAction } from "../../actions/index";
import * as classNames from 'classnames';

export interface AkLocationSelectProp extends IntlProps {
    placeholder?: string;
    notFoundContent?: string;
    defaultValue?: string | AkIdentity;
    value?: string | AkIdentity;
    onChange?: (value: string, identity: AkLocation) => any; //onchange事件，将选中dict输出
    locations?: AkLocation[];
    locationLoaded?: boolean;
    allowClear?: boolean;
    disabled?: boolean;
    className?: string;
}

export interface AkLocationSelectState {
    value?: string;
}

export class AkLocationSelect extends Component<AkLocationSelectProp,
    AkLocationSelectState> {

    static defaultProps: AkLocationSelectProp = {}

    constructor(props, context) {
        super(props, context);

        let value = "value" in props ? props.value : props.defaultValue;
        this.state = { value: this.parseValue(value) }
    }
    componentDidMount() {
        AkGlobal.store.dispatch(IdentityAction.requestLocations());
    }
    parseValue(value) {
        let val = value;

        if (value && value.ID) {
            val = value.ID;
        }
        return val;
    }

    componentWillReceiveProps(nextProps) {
        if ("value" in nextProps && nextProps.value !== this.props.value) {
            this.setState({ value: this.parseValue(nextProps.value) });
        }
    }

    valueChanged(v) {
        if (!("value" in this.props)) {
            this.setState({ value: v });
        }

        if (this.props.onChange) {
            const { locations } = this.props;
            let identity;
            if (v && v.length > 0) {
                identity = locations.find(r => r.ID === v);
            }
            this.props.onChange(v, identity);
        }
    }

    render() {
        const { placeholder, notFoundContent, locations, locationLoaded, allowClear, disabled, className } = this.props;
        const intl = AkGlobal.intl;

        let pl = placeholder ? placeholder : intl.formatMessage({ id: IdentityLocale.LocationPlaceholder });
        let value = this.state.value === null || this.state.value === "" ? undefined : this.state.value;
        return <AkSpin spinning={!locationLoaded}><AkSelect placeholder={pl} notFoundContent={notFoundContent}
            value={value} allowClear={allowClear} disabled={disabled} className={className}
            onChange={v => this.valueChanged(v)}>
            {locations.map(r => {
                return <AkSelect.Option key={r.ID} value={r.ID}>{r.Name}</AkSelect.Option>
            })}
        </AkSelect></AkSpin>
    }
}

const mapStateToProps = (state) => {
    return {
        locations: state.identity.locations,
        locationLoaded: state.identity.locationLoaded,
    }
}

export default connect(mapStateToProps)(AkLocationSelect);
