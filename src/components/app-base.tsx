import * as React from "react";
import { Component, ReactNode } from 'react';
import { IntlProvider, injectIntl } from "react-intl";
import { getTheme } from "../themes/";
import { getLocale } from "../locales/";
import { AkLocalProvider, AkSpin } from "./controls/";
import { AppLocaleStatic, AkUtil } from "../util/";
import { AkGlobal, FuWenBenExpand } from "../util/common";
import { ReducersMapObject, createStore, applyMiddleware, Action } from "redux";
import createSagaMiddleware, { SagaMiddleware } from "redux-saga";
import { rootCombineReducers } from "../reducers/index";
import rootSaga from "../sagas";
import { Provider } from "react-redux";
import { MasterPage } from "./masterpage/masterpage";
import { MerchatAPI } from "../api/masterpage/masterpage";
import { Cookies } from "../util/cookie";
import { AkContext, AppCenterAction } from "../index";
import { CacheHelper } from "../util/cache";
import { YeeFlowMasterPage } from "./yeeflowmasterpage";
import YeeOfficeMasterPage from "./masterpage/yeeoffice-masterpage";

export interface AppBaseProps {
    /**静态资源的 CDN 地址 */
    cdnUrl?: string;
    /**
     * 自带masterPage
     */
    requireMasterpage?: boolean;
    showMasterHeader?: boolean;
    reducers?: ReducersMapObject;
    onLoaded?: (appLocale?: AppLocaleStatic, theme?: string) => Promise<any>;
    onSagaInit?: (saga?: SagaMiddleware<any>) => void;
    customMaster?: React.ReactNode;
    /**项目对应的AppKey,必填  (可以从 AppKeys 取到) */
    appKey: string;
    title?: string; //页面的title，默认是YeeOffice
    languages?: string[]; //系统支持的语言，默认['zh', 'en']
    renderActions?: React.ReactNode | React.ReactNode[];
    isHomePage?: boolean;
    hideSyStemActions?: boolean;//隐藏masterpage的聊天，系统设置，用户信息按钮
}
export interface AppBaseStates {
    appLocale?: AppLocaleStatic;
    reload?: boolean;//刷新masterpage
    isHomePage?: boolean;
    language?: string;

}

declare global {
    interface Window {
        akDispatch<A extends Action>(action: A);
    }
}

let sagaInitialized = false;

export class AppBase extends Component<AppBaseProps, AppBaseStates> {
    constructor(props, context) {
        super(props, context);
        this.state = {
            appLocale: null,
            reload: false,
            isHomePage: this.props.isHomePage ? AkUtil.isHomePage() : false,
            language: null
        };
        window["CDN"] = this.props.cdnUrl;
        window["CurrentAppKey"] = props.appKey;
        AkContext.setAppKey(props.appKey);
        this.initRedux();

        // Fix for IE Not support matches and closest
        if (!Element.prototype.matches)
            Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;

        if (!Element.prototype.closest)
            Element.prototype.closest = function (s) {
                var el = this;
                var ancestor = this;
                if (!document.documentElement.contains(el)) return null;
                do {
                    if (ancestor.matches(s)) return ancestor;
                    ancestor = ancestor.parentElement;
                } while (ancestor !== null);
                return null;
            };
    }

    initSaga() {
        const topthis = this;
        if (!sagaInitialized) {
            AkGlobal.saga.run(rootSaga);
            AkGlobal.reloadMasterpage = this.reloadMasterpage.bind(this);
            this.props.onSagaInit && this.props.onSagaInit(AkGlobal.saga);
            sagaInitialized = true;
        }
    }

    reloadMasterpage(language?: any) {
        this.setState({
            reload: !this.state.reload,
            isHomePage: this.props.isHomePage ? AkUtil.isHomePage() : false
        });
        if (language) {
            this.loadLanguage(language).then(() => {
                this.setState({ language });
            });
        }
    }

    initRedux() {
        AkGlobal.saga = createSagaMiddleware();
        AkGlobal.store = createStore(rootCombineReducers(this.props.reducers), applyMiddleware(AkGlobal.saga)); //创建store
        Window.prototype.akDispatch = (action) => AkGlobal.store.dispatch(action); //给window对象增加dispatch action方法

        this.initSaga();
        // if (!this.props.requireMasterpage) {
        //     this.initSaga();
        // }
    }
    /*富文本 图片预览模态框 */
    fwbImgViewModile(){
        return<div id="mobilefangdatupian" className="fuwenben-fangdamodile">	
            <div onClick={window["mobilefangdatupianClose"]} className="closeBtn">X</div>
            <div className="img-box">
            <img alt="" id="fangdatupian" src=""/>
            </div>
        </div>;
    }
    GlobalIntlInject = injectIntl((props, options) => {
        AkGlobal.intl = props.intl;
        if (this.props.requireMasterpage) {
            if (AkContext.isYeeFlow()) {
                //yeeflow的masterpage
                return <div className="ak-container">
                    <YeeFlowMasterPage
                        languages={this.props.languages}
                        showMasterHeader={this.props.showMasterHeader}
                        // onLoaded={() => {
                        //     this.initSaga();
                        // }}
                        renderActions={this.props.renderActions}
                        onChangeLanguage={(language) => {
                            AkContext.setLanguage(language);
                            // CacheHelper.setCooikes(AkContext.Keys.LanguageKey, language, {
                            //     path: "/"
                            // }, true);
                            this.loadLanguage(language);
                        }}
                    >
                        {this.props.children}
                    </YeeFlowMasterPage>
                    {this.fwbImgViewModile()}
                </div>
            } else {
                if (AkContext.isNewVersion() || this.state.isHomePage || this.props.hideSyStemActions) {
                    //yeeoffice的新masterpage
                    return <div className="ak-container">
                        <YeeOfficeMasterPage
                            languages={this.props.languages}
                            isHomePage={this.state.isHomePage}
                            hideSyStemActions={this.props.hideSyStemActions}
                            showMasterHeader={this.props.showMasterHeader}
                            // onLoaded={() => {
                            //     this.initSaga();
                            // }}
                            renderActions={this.props.renderActions}
                            onChangeLanguage={(language) => {
                                AkContext.setLanguage(language);
                                // CacheHelper.setCooikes(AkContext.Keys.LanguageKey, language, {
                                //     path: "/"
                                // }, true);
                                this.loadLanguage(language);
                            }}>
                            {this.props.children}
                        </YeeOfficeMasterPage>
                        {this.fwbImgViewModile()}
                    </div>
                } else {
                    return <div className="ak-container">
                        <MasterPage
                            title={this.props.title}
                            languages={this.props.languages}
                            showMasterHeader={this.props.showMasterHeader}
                            // onLoaded={() => {
                            //     this.initSaga();
                            // }}
                            renderActions={this.props.renderActions}
                            onChangeLanguage={(language) => {
                                AkContext.setLanguage(language);
                                // CacheHelper.setCooikes(AkContext.Keys.LanguageKey, language, {
                                //     path: "/"
                                // }, true);
                                this.loadLanguage(language);
                            }}>
                            {this.props.children}
                        </MasterPage>
                        {this.fwbImgViewModile()}
                    </div>;
                }
            }
        }
        else {
            return <div className="ak-container">{this.props.children}{this.fwbImgViewModile()}</div>;
        }
    });

    getDefaultLanguage() {
        const { languages } = this.props;
        if (languages && languages.length > 0) {
            return languages[0];
        } else {
            return 'zh';
        }
    }

    loadLanguage(language: string) {
        let theme;
        getTheme(theme);
        return new Promise((resolve, reject) => {
            getLocale(language ? language : this.getDefaultLanguage()).then(data => {
                if (this.props.onLoaded) {
                    this.props.onLoaded(data, theme).then(d => {
                        data.messages = { ...data.messages, ...d };
                        this.setState({ appLocale: data }, () => resolve());
                    });
                } else {
                    this.setState({ appLocale: data }, () => resolve());
                }
            });
        })
    }
    //默认从cookie加载个人设置的语言，没有cookie时，从企业的preferred语言加载。
    componentDidMount() {
        let topThis = this;
        let language = AkContext.getLanguage();
        topThis.loadLanguage(language);
        FuWenBenExpand.imgView();//富文本img 移动端缩放预览
    }

    render() {
        const { appLocale, language } = this.state;
        let content = appLocale && AkGlobal.store.getState().commonBaseInfo.baseInfo.isSuccess !== null ? <AkLocalProvider locale={appLocale.antd}>
            <IntlProvider
                key={appLocale.locale}
                locale={appLocale.locale}
                messages={appLocale.messages}>
                <this.GlobalIntlInject />
            </IntlProvider>
        </AkLocalProvider>
            : <div>
            </div>;
        if (AkContext.isYeeFlow() && !language) {
            content = <div></div>;
        }
        return <Provider store={AkGlobal.store}>
            {content}
        </Provider>;
    }
}