import "moment/locale/ja";
import * as Pagination from "rc-pagination/lib/locale/ja_JP";
import * as DatePicker from "antd/lib/date-picker/locale/ja_JP";
import * as TimePicker from "antd/lib/time-picker/locale/ja_JP";
import * as Calendar from "antd/lib/calendar/locale/ja_JP";

export default {
    locale: 'ja',
    Pagination,
    DatePicker,
    TimePicker,
    Calendar,
    Table: {
        filterTitle: 'メニューをフィルター',
        filterConfirm: '確定',
        filterReset: 'リセット',
        emptyText: 'データがありません',
        selectAll: 'すべてを選択',
        selectInvert: '選択を反転'
    },
    Modal: {
        okText: '確定',
        cancelText: 'キャンセル',
        justOkText: '確定'
    },
    Popconfirm: {
        okText: '確定',
        cancelText: 'キャンセル'
    },
    Transfer: {
        notFoundContent: '結果はありません',
        searchPlaceholder: 'ここを検索',
        itemUnit: 'アイテム',
        itemsUnit: 'アイテム'
    },
    Select: {
        notFoundContent: '結果はありません'
    },
    Upload: {
        uploading: 'アップロード中...',
        removeFile: 'ファイルを削除',
        uploadError: 'アップロードエラー',
        previewFile: 'ファイルをプレビュー'
    }
};