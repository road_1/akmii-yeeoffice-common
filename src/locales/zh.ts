import "moment/locale/zh-cn";
import * as Pagination from "rc-pagination/lib/locale/zh_CN";
import * as DatePicker from "antd/lib/date-picker/locale/zh_CN";
import * as TimePicker from "antd/lib/time-picker/locale/zh_CN";
import * as Calendar from "antd/lib/calendar/locale/zh_CN";

export default {
    locale: 'zh-cn',
    Pagination,
    DatePicker,
    TimePicker,
    Calendar,
    Table: {
        filterTitle: '筛选',
        filterConfirm: '确定',
        filterReset: '重置',
        emptyText: '暂无数据',
        selectAll: '全选当页',
        selectInvert: '反选当页',
    },
    Modal: {
        okText: '确定',
        cancelText: '取消',
        justOkText: '知道了',
    },
    Popconfirm: {
        okText: '确定',
        cancelText: '取消',
    },
    Transfer: {
        notFoundContent: '暂无数据',
        searchPlaceholder: '搜索',
        itemUnit: '项',
        itemsUnit: '项',
    },
    Select: {
        notFoundContent: '无匹配结果',
    },
    Upload: {
        uploading: '文件上传中',
        removeFile: '删除文件',
        uploadError: '上传错误',
        previewFile: '预览文件',
    }
};