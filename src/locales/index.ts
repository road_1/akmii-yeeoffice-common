import { addLocaleData } from "react-intl";
import * as en from "react-intl/locale-data/en";
import * as zh from "react-intl/locale-data/zh";
import * as ja from "react-intl/locale-data/ja";
import * as de from "react-intl/locale-data/de";

import antdEn from "./en";
import antdDe from "./de";
import antdTw from "./tw";
import antdZh from "./zh";
import antdJa from "./ja";
import * as moment from "moment";
import { AppLocaleStatic } from "../util";
import { CommonLocale, IdentityLocale, TaskLocale, ApplyContentLocale, MathLocale, KindEditorLocale, ListLookupLocale, UploadLocale, CardTypeLocal } from "./localeid";

export async function getLocale(language?: string): Promise<AppLocaleStatic> {
    if (!language) {
        //Default local_lanΩguage
        let DEFAULT_LOCALE = 'zh-CN';
        language = navigator.language || (navigator as any).browserLanguage || DEFAULT_LOCALE;
    }

    let languageWithoutRegionCode = language.toLowerCase().split(/[_-]+/)[0];

    if (language === "zh-tw") {
        languageWithoutRegionCode = language;
    }

    let appLocaleStatic: AppLocaleStatic;
    return new Promise<AppLocaleStatic>((resolve: (locale) => void, reject: (reason: any) => void) => {
        switch (languageWithoutRegionCode) {
            case 'en':
                moment.locale('en');
                addLocaleData([...en]);
                // require.ensure([], () => {
                appLocaleStatic = new AppLocaleStatic({
                    locale: languageWithoutRegionCode,
                    messages: appLocaleStatic,
                    antd: antdEn
                });
                appLocaleStatic.messages = require("./en.json");
                resolve(appLocaleStatic)
                // }, "en.json");
                break;
            case 'ja':
                moment.locale('ja');
                addLocaleData([...ja]);
                // require.ensure([], () => {
                appLocaleStatic = new AppLocaleStatic({
                    locale: languageWithoutRegionCode,
                    messages: appLocaleStatic,
                    antd: antdJa
                });
                appLocaleStatic.messages = require("./ja.json");
                resolve(appLocaleStatic)
                break;
            case 'de':
                moment.locale('de');
                addLocaleData([...de]);
                // require.ensure([], () => {
                appLocaleStatic = new AppLocaleStatic({
                    locale: languageWithoutRegionCode,
                    messages: appLocaleStatic,
                    antd: antdDe
                });
                appLocaleStatic.messages = require("./de.json");
                resolve(appLocaleStatic)
                break;
            case "zh-tw":
                moment.locale("zh-tw");
                addLocaleData([...zh]);
                // require.ensure([], () => {
                appLocaleStatic = new AppLocaleStatic({
                    locale: languageWithoutRegionCode,
                    messages: appLocaleStatic,
                    antd: antdTw
                });
                appLocaleStatic.messages = require("./tw.json");
                resolve(appLocaleStatic)
                break;
            case 'zh':
            default:
                moment.locale('zh-cn');
                addLocaleData([...zh]);
                // require.ensure([], () => {
                appLocaleStatic = new AppLocaleStatic({
                    locale: languageWithoutRegionCode,
                    messages: appLocaleStatic,
                    antd: antdZh
                });
                appLocaleStatic.messages = require("./zh.json");
                resolve(appLocaleStatic);
                // }, "zh.json");
                break;
        }
    });
}

export { CommonLocale, IdentityLocale, TaskLocale, ApplyContentLocale, MathLocale, KindEditorLocale, ListLookupLocale, UploadLocale, CardTypeLocal }