import "moment/locale/de";
import * as Pagination from "rc-pagination/lib/locale/de_DE";
import * as DatePicker from "antd/lib/date-picker/locale/de_DE";
import * as TimePicker from "antd/lib/time-picker/locale/de_DE";
import * as Calendar from "antd/lib/calendar/locale/de_DE";

export default {
    locale: 'de',
    Pagination,
    DatePicker,
    TimePicker,
    Calendar,
    Table: {
        filterTitle: 'Filter-Menü',
        filterConfirm: 'OK',
        filterReset: 'Zurücksetzen',
        emptyText: 'Keine Daten',
        selectAll: 'Selektiere Alle',
        selectInvert: 'Selektion Invertieren'
    },
    Modal: {
        okText: 'OK',
        cancelText: 'Abbrechen',
        justOkText: 'OK'
    },
    Popconfirm: {
        okText: 'OK',
        cancelText: 'Abbrechen'
    },
    Transfer: {
        notFoundContent: 'Nicht gefunden',
        searchPlaceholder: 'Suchen',
        itemUnit: 'Eintrag',
        itemsUnit: 'Einträge'
    },
    Select: {
        notFoundContent: 'Nicht gefunden'
    },
    Upload: {
        uploading: 'Hochladen...',
        removeFile: 'Datei entfernen',
        uploadError: 'Fehler beim Hochladen',
        previewFile: 'Dateivorschau'
    }
};