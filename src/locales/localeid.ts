export class CommonLocale {
    static All = "common.All";
    static Clear = "common.clear";
    static Close = "common.close";
    static Search = "common.search";

    static Save = "common.save";
    static Cancel = "common.cancel";
    static Edit = "common.edit";
    static Delete = "common.delete";
    static New = "common.New";
    static Insert = "common.insert";
    static Add = "common.Add";
    static Submit = "common.submit";
    static Exit = "common.exit";
    static OK = "common.ok";
    static ExitConfirm = "common.exit.confirm";
    static Deploy = "common.deploy";
    static Undo = "common.undo";
    static Redo = "common.redo";

    static Id = "common.id";
    static Name = "common.name";

    static Text = "common.text";
    static URL = "common.url";

    static more = "common.more";

    static Setting = "common.setting";
    static Settings = "common.settings";

    static Error = "common.error";
    static Warn = "common.warn";
    static Info = "common.info";
    static Notification = "common.notification";
    static Status = "common.status";
    static Code = "common.code";

    static Expression = "common.expression";
    static Function = "common.function";
    static Number = "common.number";
    /**固定电话 */
    static Telephone = "common.telephone";
    /**移动电话 */
    static Cellphone = "common.cellphone";
    /**邮箱 */
    static Email = "common.email";
    static True = "common.true";
    static False = "common.false";
    static On = "common.on";
    static Off = "common.off";

    static Order = "common.order";
    static Required = "common.required";
    static RequiredLabel = "common.required.label";

    static Ext = "common.ext";
    /**清除 */
    static Eliminate = "common.eliminate";

    static Copy = "common.copy";
    static Paste = "common.paste";
    static And = "common.and";
    static Or = "common.or";

    static AutoCompleteNoResult = "common.autocomplete.empty";
    static Value = "common.value";
    static Label = "common.label";
    static Reset = "common.reset";
    static Loading = "common.loading";
    static InlineLoading = "common.inlineloading";
    static KeywordPlaceHolder = "common.pl.keyword";

    static DeleteConfirm = "common.delete.confirm";
    static ResponseError = "common.response.error";
    static ResponseUnauthorized = "common.response.unauthorized";
    static SPLoginResponseUnauthorized = "common.response.SPLogin.unauthorized";
    static ResponseBadRequest = "common.response.badrequest";
    static ResponseNotFound = "common.response.notFound";
    static CacheDataBroken = "common.cache.data.broken";
    static FormValidatePleaseInput = "common.form.pleaseinput";
    static FormValidatePleaseSelect = "common.form.pleaseselect";
    static FormValidateDuplicated = "common.form.duplicated";
    static FormValidateDefaultValue = "common.form.defaultvalue";
    static FormValidateApplicant = "common.form.applicant";
    static FormRangeMessage = "common.form.rangemessage";
    static FormEqualsRule = "common.form.equals";
    static FormGreaterThanRule = "common.form.greaterthan";
    static FormGreaterOrEqualsRule = "common.form.greaterorequalsthan";
    static FormLessThanRule = "common.form.lessthan";
    static FormLessOrEqualsThanRule = "common.form.lessorequalsthan";
    static FormLength = "common.form.length";
    static FormInteger = "common.form.integer";
    static FormNumber = "common.form.number";
    static FormEmail = "common.form.email";
    static FormBankAccount = "common.form.bankaccount";
    static FormUrl = "common.form.url";
    static FormValidaMessage = "common.form.valida.message";
    /**格式不正确 */
    static FormIncorrectFormat = "common.form.Incorrect.format";

    static Metadata = "common.metadata";
    static Operation = "common.operation";
    static NotInstall = "common.not.install";


    /**提示 */
    static Tip: string = "common.tip";

    /**添加成功 */
    static AddSuccess: string = "common.add.success";
    /**新建成功 */
    static NewSuccess: string = "common.new.success";
    /**编辑成功 */
    static EditSuccess: string = "common.edit.success";
    /**删除成功 */
    static DeleteSuccess: string = "common.delete.success";
    /**添加失败 */
    static AddFail: string = "common.add.fail";
    /**新建失败 */
    static NewFail: string = "common.new.fail";
    /**编辑失败 */
    static EditFail: string = "common.edit.fail";
    /**删除失败 */
    static DeleteFail: string = "common.delete.fail";

    /**禁用成功 */
    static DisabledSuccess: string = "common.disabled.success";
    /**启用成功 */
    static EnabledSuccess: string = "common.enabled.success";
    /**禁用失败 */
    static DisabledFail: string = "common.disabled.fail";
    /**启用失败 */
    static EnabledFail: string = "common.enabled.fail";

    /**保存并编辑 */
    static BtnSaveAndEdit: string = "common.btn.save.and.edit";
    /**添加 */
    static BtnAdd: string = "common.btn.add";
    /**获取出错 */
    static GetInfoError: string = "common.getinfo.error";

    /**保存成功 */
    static TipSaveSuccess: string = "common.tip.save.success";
    /**保存失败 */
    static TipSaveFail: string = "common.tip.save.fail";
    /**操作成功 */
    static TipOperationSuccess: string = "common.tip.operation.success";
    /**操作失败 */
    static TipOperationFail: string = "common.tip.operation.fail";
    /**暂时没有内容 */
    static NoContent = "common.nocontent";
    /**选择颜色 */
    static ChooseColor = "common.choose.color";
    /**Tip 超过最大值 */
    static TipExceedMax = "common.exceed.max";
    /**Tip 小于最小值 */
    static TipLessMin = "common.less.min";

    static FormValidateMessage = "formcraft.form.validate.message";

    /**标签名 */
    static TagName = "common.tag.name";
    static DateDefaultPlaceholder = "data.picker.default.placeholder";
    static TimePickerDefaultPlaceholder = "time.picker.default.placeholder";

    /**图片剪裁提示语 */
    static ImageCropSubMit = "common.image.crop.submit";

    static NoPermission: string = "common.nopermission";
    static NoPermissionDes: string = "common.nopermission.describe";

    static AccessDenied: string = "common.AccessDenied";
    static AccessDeniedDes: string = "common.AccessDenied.describe";

    static NoSiteApps: string = "common.no.site.apps";

    static Customize = "common.customize";
}

export class MathLocale {
    static Plus = "math.+";
    static Minus = "math.-";
    static Multiply = "math.*";
    static Divide = "math./";
    static Modulo = "math.%";
    static OpenParen = "math.(";
    static CloseParen = "math.)";
    static Equal = "math.==";
    static NotEqual = "math.!=";
    static Greater = "math.>";
    static GreaterEqual = "math.>=";
    static Less = "math.<";
    static LessEqual = "math.<=";
}

export class IdentityLocale {
    static ID = "identity.id";
    static User = "identity.user";
    static Name = "identity.name";
    static Code = "identity.code";
    static Type = "identity.type";
    static Phone = "identity.phone";
    static Address = "identity.address";
    static CostCenter = "identity.costcenter";
    static Project = "identity.project";
    static Manager = "identity.manager";
    static SimplePlaceholder = "identity.simple.placeholder";
    static AdvUserPlaceholder = "identity.adv.user.placeholder";
    static AdvCostCenterPlaceholder = "identity.adv.costcenter.placeholder";
    static AdvUserColumnName = "identity.adv.user.column.name";
    static AdvUserColumnEmail = "identity.adv.user.column.email";
    static Organization = "identity.organization";
    static Location = "identity.location";
    static Group = "identity.group";
    static Position = "identity.position";
    static PositionPlaceholder = "identity.position.placeholder";
    static OrganizationPlaceholder = "identity.org.placeholder";
    static LocationPlaceholder = "identity.loc.placeholder";
    static GroupPlaceholder = "identity.group.placeholder";
    /**组名 */
    static AdvGroupName = "identity.name";
    static ListFieldName = "identity.field.name";
    static ListFieldTab = "identity.field.tab";
    /**编码 */
    static AdvGroupCode = "identity.code";

    static AdvGroupPlaceholder = "identity.adv.group.placeholder";
}

export class TaskLocale {
    static Code = "task.code";
    static Status = "task.status";
    static OutcomeCompleted = "task.outcome.Completed";
    static OutcomeApproved = "task.outcome.Approved";
    static OutcomeRejected = "task.outcome.Rejected";
    static OutcomeRevoked = "task.outcome.Revoked";
    static OutcomeCancelled = "task.outcome.Cancelled";
    static OutcomeTerminated = "task.outcome.Terminated";
    static OutcomeProcessing = "task.outcome.Processing";
}

export class UploadLocale {
    static UploadAttachment = "upload.attachment";
    static MsgIconSizeError = "upload.message.icon.size";
    static MsgIconTypeError = "upload.message.icon.type";
    static MsgIconMaxWidthAndHeight = "upload.message.icon.max.width.height";
    static MsgIconMaxHeight = "upload.message.icon.max.height";
    static MsgIconMaxWidth = "upload.message.icon.max.width";
    static MsgIconMinWidthAndHeight = "upload.message.icon.min.weight.height";
    static MsgIconMinWidth = "upload.message.icon.min.weight";
    static MsgIconMinHeight = "upload.message.icon.min.height";
    static MsgFileSizeError = "upload.message.file.size";
    static MsgFileCountError = "upload.message.file.count";
    static MsgFileTypeError = "upload.message.file.type";
    static MsgNoContentError = "upload.message.file.no.length";
    static MsgFileExists = "upload.message.exists";
    static MsgFileReadFail = "upload.message.read.fail";
    static MsgDeleteConfirm = "upload.message.delete.confirm";
    /*****文件名超过200字符！***/
    static FileNameMoreLarge = "upload.message.file.name.more.large";
    /*****文件名包含特殊字符串！***/
    static FileNameInvalidate = "upload.message.file.name.invalidate";
}

export class ListLocale {
    static Summary = "formcraft.list.summary";
    static SummaryTypePre = "formcraft.list.summary.";
    static ViewTable = "formcraft.list.view.table";
    static ViewForm = "formcraft.list.view.form";
    static ListEmpty = "formcraft.list.empty";
    static TableEmpty = "Table.list.empty";
}

export class ListLookupLocale {
    static SelectListPlaceholder = "listlookup.selectlistplaceholder";
    static SelectLibraryPlaceholder = "listlookup.selectlibraryplaceholder";
    static SelectFieldPlaceholder = "listlookup.selectfieldplaceholder";
    static SelectAppPlaceholder = "listlookup.selectappplaceholder";
    static SelectListTitle = "listlookup.selectlisttitle";
    static SearchListPlaceholder = "listlookup.searchlistplaceholder";
    static SearchLibraryListPlaceholder = "librarylookup.searchlistplaceholder";
    static TableColumnTitle = "listlookup.tablecolumntitle";
    static LibraryColumnTitle = "librarylistlookup.tablecolumntitle";
}
export class LookUpLocale {
    /**流程名称 */
    static FlowName = "lookup.flowname.name";
}
export class MetadataLocale {
    static MetadataCategoryPlaceholder = "metadata.category.placehoder";
    static MetadataCategoryEmpty = "metadata.category.empty";
}

/**
 * 申请单
 *
 * @export
 * @class ApplyContentLocale
 */
export class ApplyContentLocale {

    static StatusTip = "page.apply.column.process.status.tip";
    static PendingAssignees = "requisition.pendingassignees";

    static AttachmentFile = "requisition.attachment.title";
    static AttachmentAdd = "requisition.attachment.add";
    /**
     * 申请人信息
     * @memberOf YeeUserContentLocale
     */
    static ApplyUserTitle = "requisition.user.title";
    /**
     * 提交人
     *
     * @static
     *
     * @memberOf YeeUserContentLocale
     */
    static LabelSubmitUser = "requisition.user.label.submituser";
    /**
     * 提交时间
     *
     * @static
     *
     * @memberOf YeeUserContentLocale
     */
    static LabelSubmitDate = "requisition.user.label.submitdate";
    /**
     * 申请人
     *
     * @static
     *
     * @memberOf YeeUserContentLocale
     */
    static LabelApplyUser = "requisition.user.label.applyuser";
    /**
     * 员工编号
     *
     * @static
     *
     * @memberOf YeeUserContentLocale
     */
    static LabelEmployeeNo = "requisition.user.label.employeeno";
    /**
     * 职称
     *
     * @static
     *
     * @memberOf YeeUserContentLocale
     */
    static LabelJobTitle = "requisition.user.label.jobtitle";
    /**
     * 工作城市
     *
     * @static
     *
     * @memberOf YeeUserContentLocale
     */
    static LabelLocation = "requisition.user.label.location";
    /**
     * 汇报经理
     *
     * @static
     *
     * @memberOf YeeUserContentLocale
     */
    static LabelLineManager = "requisition.user.label.linemanager";
    /**
     * 部门
     *
     * @static
     *
     * @memberOf YeeUserContentLocale
     */
    static LabelOrg = "requisition.user.label.org";

    static LogTitle = "requisition.log.title";

    static ButtonSubmit = "requisition.button.submit";
    static ButtonSave = "requisition.button.save";
    static ButtonTerminate = "requisition.button.terminate";
    static ButtonClose = "requisition.button.close";
    static ButtonApprove = "requisition.button.approve";
    static ButtonReject = "requisition.button.reject";
    static ButtonRevoke = "requisition.button.revoke";
    static ButtonForward = "requisition.button.forward";
    static ButtonReceive = "requisition.button.receive";
    static ButtonClaim = "requisition.button.claim";
    static ButtonCompleted = "requisition.button.completed";
    static ButtonAddAssignee = "requisition.button.addassignee";
    static ButtonResign = "requisition.button.resign";
    static ButtonRecall = "requisition.button.recall";

    static ApprovalComments = "requisition.approval.comments";
    static ApprovalOpinion = "requisition.approval.opinion";
    static LogContentStatus = "requisition.logcontent.lable.status";
    static LogContentFlowchart = "requisition.logcontent.lable.flowchart";
    static LogContentFlowchartTitle = "requisition.logcontent.modal.flowcharttitle";
    static ClaimAssigneeTitle = "requisition.modal.claim.title";
    /**加签人 */
    static PropsAssignee = "requisition.modal.assignee";
    /**转办人 */
    static PropsForwardAssignee = "requisition.modal.forward.assignee";
    static AddAssigneeTitle = "requisition.modal.addassignee.title";
    static ReassignTitle = "requisition.modal.reassign.title";

    static CommentLengthCheck = "requisition.validation.comment";
    static CommentOnReject = "requisition.validation.comment.reject";
    static ReceiveConfirm = "requisition.confirm.receive";
    static Submit = "requisition.submit";
    static SizeCannot = "requisition.sizecannot";
    static UploadFail = "requisition.uploadfail";
    static Exists = "requisition.Exists";
    static UploadComplete = "requisition.uploadcomplete";
    static FileReadFail = "requisition.filereadfail";
    static Prompt = "requisition.prompt";
    static ConfirmDelete = "requisition.confirmdelete";
    static DeleteFileByOther = "requisition.deletefilebyother";
    static SelectApplicant = "requisition.selectapplicant";

    /** 加签人已存在 */
    static TipOperationExists: string = "common.tip.operation.exists";
    /**xxx 必须唯一 */
    static TipUniqueValue: string = "common.tip.unique.value";

    /**
     * 流程超预算
     */
    static BudgetExceed = "requisition.budget.exceed";

    /**任务已完成或不存在*/
    static RecallNotExit = "requisition.recall.notexit";
    /**任务运行中，不需要召回*/
    static RecallRuning = "requisition.recall.runing";
    /**只能召回自己的任务*/
    static RecallUnSelf = "requisition.recall.unself";
    /**领用任务不能召回*/
    static RecallRefuseReceive = "requisition.recall.refusereceive";
    /**该任务不能召回*/
    static RecallRefuse = "requisition.recall.refuse";
    /**已召回*/
    static LabelRecall = "requisition.recall.label";
    /**加签人 $人员名称 已在经办人列表 */
    static TipAssigneeExist = "requisition.tip.assignee.exist";
    /**任务将加签给 $人员名称，是否继续？ */
    static TipAssigneeUnExist = "requisition.tip.assignee.unexist";
    /**任务将转办给 $人员名称，是否继续？ */
    static TipReassignedTo = "requisition.tip.reassignedto";
    /**转办人不能为本人 */
    static TipReassignUnCurrent = "requisition.tip.reassign.uncurrent";
    /**转办人不能为经办人*/
    static TipReassignUnAssign = "requisition.tip.reassign.unassign";

    /**确定撤回申请 */
    static ModalIsRecallApply: string = "requisition.tip.isrecallapply";
    /**确定终止申请 */
    static ModalIsCancelApply: string = "requisition.tip.iscancelapply";

    /** 申请不满足撤回的条件*/
    static DontRecall: string = "requisition.tip.dont.recall";
    /** 申请不满足取消的条件*/
    static DontTerminate: string = "requisition.tip.dont.terminate";
}

export class MasterPageLocale {
    /**页面标题:易企办 */
    static PageTitle = "masterpage.title";
    /**
     * 切换用户
     */
    static ChangeUser = "masterpage.header.changeuser";
    /**
     * 退出
     */
    static Exit = "masterpage.header.exit";
    /**
     * 查看信息
     */
    static ViewDetail = "masterpage.header.viewdetail";
    /**
     * 中文
     */
    static LanguageZH = "masterpage.language.zh";
    /**
     * 中文（繁体）
     */
    static LanguageTW = "masterpage.language.TW";
    /**
     * 英文
     */
    static LanguageEN = "masterpage.language.en";
    /**
     * 日文
     */
    static LanguageJA = "masterpage.language.ja";
    /**
     * 德文
     */
    static LanguageDE = "masterpage.language.de";
    /**
     * 语言切换
     */
    static LanguageTrigger = "masterpage.language.trigger";

    /**
     * 网易页面Title
     */
    static MasterTitleNetease = "masterpage.title.netease";
    /**
     * 网易底部说明
     */
    static MasterFooterNetease = "masterpage.footer.netease";


    /**
   * Subscribe
   */
    static SubscribeLevelBuy = "masterpage.level.buy";

    /**
   * 试用状态
   */
    static SubscribeLevelNotBuy = "masterpage.level.notBuy";
    /**
    * Lite标题
    */
    static SubscribeLiteTitle = "masterpage.lite.title";
    /**
   * Enterprise标题
   */
    static SubscribeEnterpriseTitle = "masterpage.enterprise.title";
    /**
   * Standard标题
   */
    static SubscribeStandardTitle = "masterpage.standard.title";



    /**
    * 付费购买
    */
    static SubscribeBuy = "masterpage.subscribe.buy";
    /**
     * 订阅ID
     */
    static SubscribeSubscribeID = "masterpage.subscribe.subscribeID";
    /**
    * 订阅过期
    */
    static SubscribeEnds = "masterpage.subscribe.ends";
    /**
    * 订阅付费提示
    */
    static SubscribeUpgrade = "masterpage.subscribe.upgrade";
    /**
   * 订阅开始时间
   */
    static SubscribeCreated = "masterpage.subscribe.created";
    /**
      * 订阅过期时间
      */
    static SubscribeExpireDate = "masterpage.subscribe.expireDate";

    /**
   * 订阅过期时间
   */
    static SubscribeUserMaxCount = "masterpage.subscribe.userMaxCount";

    /**
  * 订阅时长(天)
  */
    static SubscribeSubscribeDay = "masterpage.subscribe.subscribeDay";
    /**
   * 订阅剩余时长(天)
   */
    static SubscribeRemainingDay = "masterpage.subscribe.remainingDay";

    /**修改密码 */
    static ChangePassword = "masterpage.change.password";

    /**修改密码 */
    static ChangePwd = "masterpage.change.pwd";

    /**当前密码 */
    static Current = "masterpage.current";

    /**当前密码提示 */
    static CurrentTip = "masterpage.current.tip";

    /**新密码 */
    static New = "masterpage.new";

    /**新密码提示 */
    static NewTip = "masterpage.new.tip";

    /**确认新密码 */
    static Confirm = "masterpage.confirm";

    /**确认新密码提示 */
    static ConfirmTip = "masterpage.confirm.tip";

    /**密码验证规则 */
    static PasswordRuleStrength = "masterpage.password.rule.strength";

    /**密码不一致 */
    static PasswordRuleEqual = "masterpage.password.rule.equal";

    /**密码修改成功 */
    static ChangePasswordSuccess = "masterpage.change.password.success";

    /**密码修改失败 */
    static ChangePasswordFail = "masterpage.change.password.fail";

    /**yeeflow语言切换 */
    static YeeFlowLanguageTrigger = "masterpage.yeeflow.language.trigger";

    /**语言 */
    static Language = "masterpage.yeeflow.language";

    /**语言设置 */
    static LanguageSettings = "masterpage.yeeflow.language.settings";

    /**版本信息 */
    static Subscription = "masterpage.yeeflow.subscription";

    /**剩余天数 */
    static ExpiresDays = "masterpage.yeeflow.expiresdays";

    /**需要帮助 */
    static NeedHelp = "masterpage.yeeflow.need.help";

    /**资源 */
    static Resources = "masterpage.yeeflow.resources";

    /**新闻室 */
    static NewsRoom = "masterpage.yeeflow.news.room";

    /**有什么新鲜事吗？ */
    static WhatNew = "masterpage.yeeflow.what.new";

    /**退出 */
    static SignOut = "masterpage.yeeflow.sign.out";

    /**试用 */
    static Trial = "masterpage.yeeflow.trial";

    static AppManage: string = "masterpage.yeeflow.homepage.app.manage";

    static SystemSettings: string = "masterpage.menu.YeeOfficeSettings";
    /**订阅 */
    static SubscriptionModal: string = "masterpage.yeeflow.subscription.modal";

    static SubscriptionModalTitle: string = "masterpage.yeeflow.subscription.modal.title";

    static SubscriptionModalRequireTip: string = "masterpage.yeeflow.subscription.modal.require.tip";

    static SubscriptionModalInputNumberTip: string = "masterpage.yeeflow.subscription.modal.input.number.tip";

    static SubscriptionModalEmailTip: string = "masterpage.yeeflow.subscription.modal.email.tip";

    static SubscriptionModalName: string = "masterpage.yeeflow.subscription.modal.name";

    static SubscriptionModalWorkEmail: string = "masterpage.yeeflow.subscription.modal.email.name";

    static SuSubscriptionModalJobTitle: string = "masterpage.yeeflow.subscription.modal.job.title";

    static SuSubscriptionModalPhoneNumber: string = "masterpage.yeeflow.subscription.modal.phone.number";

    static SuSubscriptionModalHowManyUsers: string = "masterpage.yeeflow.subscription.modal.how.many.users";

    static SuSubscriptionModalTermsService1: string = "masterpage.yeeflow.subscription.modal.terms.service1";
    static SuSubscriptionModalTermsService2: string = "masterpage.yeeflow.subscription.modal.terms.service2";
    static SuSubscriptionModalTermsService3: string = "masterpage.yeeflow.subscription.modal.terms.service3";
    static SuSubscriptionModalTermsService4: string = "masterpage.yeeflow.subscription.modal.terms.service4";

    static SuSubscriptionModalSubmit: string = "masterpage.yeeflow.subscription.modal.submit";

    static SuSubscriptionModalCompanyName: string = "masterpage.yeeflow.subscription.modal.company.name";

    static SuSubscriptionModalSubscriptionID: string = "masterpage.yeeflow.subscription.modal.Subscription.id";

    static SuSubscriptionCloseModalH3: string = "masterpage.yeeflow.subscription.close.modal.h3";

    static SuSubscriptionCloseModalThanksTip: string = "masterpage.yeeflow.subscription.close.modal.thanks.tip";

    static SuSubscriptionCloseModalBtn: string = "masterpage.yeeflow.subscription.close.modal.btn";

    static SuSubscriptionCloseModalSalesTip: string = "masterpage.yeeflow.subscription.close.modal.sales.tip";

    static SuSubscriptionRemindTitle: string = "masterpage.yeeflow.subscription.remind.title"
    static SuSubscriptionRemindDescription: string = "masterpage.yeeflow.subscription.remind.description"
    static SuSubscriptionRemindBtn: string = "masterpage.yeeflow.subscription.remind.Btn"
}

export class NoticeLocale {
    static Yesterday = "notice.chat.yesterday"
    static DeleteTitle = "notice.chat.deletetitle";
    static DeleteContent = "notice.chat.deletecontent";
    static QuitGroup = "notice.chat.quitgroup";
    static ChatDetail = "notice.chat.chatdetail";
    static MessageCenter = "notice.chat.messagecenter";
    static SuccessTip = "flow.revokeorcancel.success.tip";
}


export class AkOneUpLocale {
    static Title = "akoneup.title";
    static Name = "akoneup.name";
    static Type = "akoneup.type";
    static Date = "akoneup.date";
    static Dimensions = "akoneup.dimensions";
    static Size = "akoneup.size";
}
export class AkCommentsLocale {
    static Add = "akcomment.add";
    static Reply = "akcomment.reply";
    static RegContent = "akcomment.regcontent.reg";
    static MsgCommentSuccess = "akcomment.msgcomment.success";
    static DeleteSuccess = "akcomment.delete.success";
    static ClickMore = "akcomment.clickmore";

    static Comments = "akcomment.comments";
    static Send = "akcomment.send";
}

export class HistoryLogLocale {
    /** 处理人 */
    static HistoryLogActorUser = "historylog.actorUser";
    /** 子任务 */
    static HistoryLogSubtask = "historylog.subtask";
    /** 开始时间 */
    static HistoryLogStartTime = "historylog.startTime";
    /** 结束时间 */
    static HistoryLogEndTime = "historylog.endTime";
    /** 多人任务 */
    static HistoryLogMultiTask = "historylog.multi.task";
    /** 待领用 */
    static HistoryLogWaitReceive = "historylog.wait.receive";
    /**备注 */
    static HistoryLogRemark: string = "historylog.remark";
    /**原 */
    static HistoryLogOriginal: string = "historylog.original";
}

export class KindEditorLocale {
    /** 编辑工具栏 无工具栏 */
    static KindEditorTypeNobar = "kindeditor.type.nobar";
    /** 编辑工具栏 简单项 */
    static KindEditorTypeSmpale = "kindeditor.type.smpale";
    /** 编辑工具栏 自定义 */
    static KindEditorTypeCustom = "kindeditor.type.custom";
    /** 工具栏 粗体*/
    static ToolbarBold = "kindeditor.toolbar.bold";
    /** 工具栏 下划线*/
    static ToolbarUnderline = "kindeditor.toolbar.underline";
    /** 工具栏 斜体*/
    static ToolbarItalic = "kindeditor.toolbar.italic";
    /** 工具栏 删除线*/
    static ToolbarStrikethrough = "kindeditor.toolbar.strikethrough";
    /** 工具栏 文字颜色*/
    static ToolbarForecolor = "kindeditor.toolbar.forecolor";
    /** 工具栏 背景色*/
    static ToolbarBgcolor = "kindeditor.toolbar.bgcolor";
    /** 工具栏 标题*/
    static ToolbarHead = "kindeditor.toolbar.head";
    /** 工具栏 链接*/
    static ToolbarLink = "kindeditor.toolbar.link";
    /** 工具栏 表格*/
    static ToolbarTable = "kindeditor.toolbar.table";
    /** 工具栏 图片*/
    static ToolbarImg = "kindeditor.toolbar.img";
    /** 工具栏 插入代码*/
    static ToolbarInsertcode = "kindeditor.toolbar.insertcode";
    /** 工具栏 撤销*/
    static ToolbarUndo = "kindeditor.toolbar.undo";
    /** 工具栏 重复*/
    static ToolbarRedo = "kindeditor.toolbar.redo";
    /** 工具栏 列表*/
    static ToolbarList = "kindeditor.toolbar.list";
    /** 工具栏 对齐方式*/
    static ToolbarJustify = "kindeditor.toolbar.justify";
    /** 工具栏 引用*/
    static ToolbarQuote = "kindeditor.toolbar.quote";
    /** 工具栏 表情*/
    static ToolbarEmoticon = "kindeditor.toolbar.emoticon";
    /** 工具栏 插入视频*/
    static ToolbarVideo = "kindeditor.toolbar.video";
}
export class EditorLocale {
    /** 上传的不是图片*/
    static NotImage = "editor.verify.Not.image";
    /** 超出最大上传兆数*/
    static ExceedSize = "editor.verify.exceed.size";
    /** 上传的不是图片*/
    static UploadImageFail = "editor.verify.uploadimage.fail";
    /** 正文内容*/
    static EditorBody = "editor.menu.config.body";
    /** 超出上传数量*/
    static MaxCountImages = "editor.menu.max.countimage";
    /** 工具栏 标题*/
    static MenuTitle = "editor.menu.config.title";
    /** 工具栏 文字颜色*/
    static MenuWordColor = "editor.menu.config.wordcolor";
    /** 工具栏 背景色*/
    static MenuBgColor = "editor.menu.config.bgcolor";
    /** 工具栏 链接*/
    static MenuLink = "editor.menu.config.link";
    /** 工具栏 文字链接*/
    static MenuLinkText = "editor.menu.config.linktext";
    /** 工具栏 列表*/
    static MenuList = "editor.menu.config.list";
    /** 工具栏 有序列表*/
    static MenuOlList = "editor.menu.config.ollist";
    /** 工具栏 无序列表*/
    static MenuUlList = "editor.menu.config.ullist";
    /** 工具栏 对齐方式*/
    static MenuAlign = "editor.menu.config.align";
    /** 工具栏 靠右*/
    static MenuRight = "editor.menu.config.right";
    /** 工具栏 靠左*/
    static Menuleft = "editor.menu.config.left";
    /** 工具栏 居中*/
    static MenuCenter = "editor.menu.config.center";
    /** 工具栏 表情*/
    static MenuEmoji = "editor.menu.config.emoji";
    /** 工具栏 手势*/
    static MenuGesture = "editor.menu.config.gesture";
    /** 工具栏 上传图片*/
    static MenuUploadImage = "editor.menu.config.uploadimage";
    /** 工具栏 网络图片*/
    static MenuOnlinePicture = "editor.menu.config.onlinepicture";
    /** 工具栏 插入表格*/
    static MenuInsertTable = "editor.menu.config.inserttable";
    /** 工具栏 创建*/
    static MenuCreate = "editor.menu.config.create";
    /** 工具栏 行*/
    static MenuRow = "editor.menu.config.row";
    /** 工具栏 列*/
    static MenuColumn = "editor.menu.config.column";
    /** 工具栏 插入视频*/
    static MenuVideo = "editor.menu.config.video";
    /** 工具栏 格式*/
    static MenuFormat = "editor.menu.config.format";
    /** 工具栏 插入代码*/
    static MenuCode = "editor.menu.config.code";
    /** 工具栏 插入*/
    static MenuInsert = "editor.menu.config.insert";
    /** 工具栏 插入表格*/
    static MenuTable = "editor.menu.config.table";
}

/**证件类型 */
export class CardTypeLocal {
    /** 身份证*/
    static ID = "cardtype.id";
    /** 护照*/
    static Passport = "cardtype.passport";
    //  /** 军人身份证*/
    //  static ToolbarVideo = "cardtype.Military identity card";
    //  /** 社会保障卡*/
    //  static ToolbarVideo = "cardtype.SIN Card";
    //  /** 武装警察身份证件*/
    //  static ToolbarVideo = "cardtype.";
    //  /** 港澳通行证*/
    //  static ToolbarVideo = "cardtype.";
    //  /** 台湾居民来往大陆通行证*/
    //  static ToolbarVideo = "cardtype.";
    //  /** 户口簿*/
    //  static ToolbarVideo = "cardtype.";
    //  /** 临时居民身份证*/
    //  static ToolbarVideo = "cardtype.";
    //  /** 外国人永久居留证*/
    //  static ToolbarVideo = "cardtype.";
}

export class NotifyLocal {
    /** 获取历史消息 */
    static GetHistoricalMessage = "notify.button.get.historical.message";
    /** 发送 */
    static Send = "notify.button.send";
    /** 置顶 */
    static Stick = "notify.button.stick";
    /** 群聊名称 */
    static GroupNames = "notify.button.group.names";
    /** 请选择聊天人员 */
    static SelectPerson = "notify.button.select.person";
    /** 查看详情 */
    static ViewDetail = "notify.button.view.detail";
    /** 服务通知 */
    static Notification = "notify.title.notification";
    /** 不能发送空白信息 */
    static NoSendNullMessage = "notify.tip.send.null.message";
    /**查看全部群成员 */
    static ShowAllGroupUser = "notify.button.show.all.group.user";
    /**收起群成员 */
    static HiddenGroupUser = "notify.button.hidden.group.user";
    /**暂无消息 */
    static NoMessage = "notify.no.message";
    /**申请人 */
    static AppUser = "notify.content.appuser";
    /**申请时间 */
    static AppCreated = "notify.content.appcreated";
}

export class CommonControlsLocale {
    static Tag_Relation = "common.controls.tag.relation";
}

export class LibraryListLocal {
    static AddFolder = "content.libraty.list.title.add.folder";
    static AddFile = "content.libraty.list.title.add.file";
    static MutAddFile = "content.libraty.list.title.mut.add.file";
    static FolderRequireSelect = "content.libraty.list.floder.require.selector.mesage";
    static CopyRequestSuccess = "content.libraty.list.copy.request.success";
    static CopyRequestFail = "content.libraty.list.copy.request.fail";
    static FileExistedMessage = "content.libraty.list.file.or.folder.has.existed";
    static TypeIcon = "content.libraty.list.news.view.colums.type.icon";
    static CopyFolderTipMessage = "content.libraty.list.copy.folder.tip.message";
    static AddFilesSuccess = "content.libraty.list.add.files.success";
    static AddFolderFail = "content.libraty.list.add.folder.fail";
}

/**
 * 引导页用语
 */
export class FlowGuideModalTips {
    static GuideTitle = "page.flow.guide.modal.title";
    static GuideContent = "page.flow.guide.modal.content";

    static GuideCreateWorkflow = "page.flow.guide.modal.create.workflow";
    static GuideDesignSteps = "page.flow.guide.modal.design.steps";
    static GuideDesignForms = "page.flow.guide.modal.design.forms";
    static GuidePublishWorkflow = "page.flow.guide.modal.publish.workflow";

    static GuideCreateWorkflowContent = "page.flow.guide.modal.create.workflow.content";
    static GuideDesignStepsContent = "page.flow.guide.modal.design.steps.content";
    static GuideDesignFormsContent = "page.flow.guide.modal.design.forms.content";
    static GuidePublishWorkflowContent = "page.flow.guide.modal.publish.workflow.content";

    static GuideCreateWorkflowBtn = "page.flow.guide.modal.create.workflow.btn";

    static GuideSkipText = "page.flow.guide.modal.skip";
}

export class LocalizationLocale {
    static CountrySelectHd = "common.controls.country.placeholder";
}
/**剪切图片提示语 */
export class ProcModelPageLocale {
    /**上传图片要求提示语 */
    static PropsPlaceholderImgDescribe = "page.process.model.modal.palceholder.imgdescribe";
}
